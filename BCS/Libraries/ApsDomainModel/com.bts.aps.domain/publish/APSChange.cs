﻿using System.Xml.Serialization;
//using IBM.XMS;
using System.Xml;
using System.IO;
using System.Runtime;
using System;

using System.Collections.Generic;
using System.Diagnostics;


namespace com.bts.aps.domain.publish
{
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class rootDomainObject
    {

        private System.Xml.XmlElement[] anyField;

        private string classField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAnyElementAttribute()]
        public System.Xml.XmlElement[] Any
        {
            get
            {
                return this.anyField;
            }
            set
            {
                this.anyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string @class
        {
            get
            {
                return this.classField;
            }
            set
            {
                this.classField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(IsNullable = false)]
    public class domainObject
    {

        private string referenceField;

        private string classField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string @class
        {
            get
            {
                return this.classField;
            }
            set
            {
                this.classField = value;
            }
        }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(IsNullable = false)]
    public class details
    {

        private APSChangeDetail[] APSChangeDetailField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("com.bts.aps.domain.publish.APSChangeDetail")]
        public APSChangeDetail[] APSChangeDetail
        {
            get
            {
                return this.APSChangeDetailField;
            }
            set
            {
                this.APSChangeDetailField = value;
            }
        }
    }

    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("com.bts.aps.domain.publish.APSChange", IsNullable = false)]
    public class APSChange
    {
        public const string rootObject_XPath = "/com.bts.aps.domain.publish.APSChange/rootDomainObject";
        public const string rootClass_Agency = "com.bts.aps.domain.Agency";
        public const string rootClass_CompanyPersonnel = "com.bts.aps.domain.CompanyPersonnel";

        private long idField;

        private string companyAbbreviationField;

        private string effectiveDateField;

        private string userNameField;

        private string rootClassField;

        private long rootIdField;

        private rootDomainObject rootDomainObjectField;

        private APSChangeDetail[] detailsField;

        private object[] detailDomainObjectInstancesField = null;

        XmlDocument m_objMessage = null;

        private APSChange() { }

        //public static APSChange getInstance(IMessage apsChangeMessage)
        //{
        //    return getInstance(((IBM.XMS.ITextMessage)apsChangeMessage).Text, true);
        //}

        public static APSChange getInstance(String apsChangeMessage)
        {
            return getInstance(apsChangeMessage, true);
        }

        public static APSChange getInstance(String apsChangeMessage, bool enableLazyLoad)
        {
            XmlDocument objMessage = new XmlDocument();
            objMessage.LoadXml(apsChangeMessage);
            APSChange objApsChange = new APSChange();
            XmlSerializer objXmlSerializer = new XmlSerializer(objApsChange.GetType());
            objApsChange = (APSChange)objXmlSerializer.Deserialize(new StringReader(objMessage.InnerXml));

            ReorderDetails(objApsChange);

            if (enableLazyLoad)
                objApsChange.m_objMessage = objMessage;
            else
            {
                objApsChange.detailDomainObjectInstancesField = new object[objApsChange.details.Length];
                for (int i = 0; i < objApsChange.details.Length; i++)
                {
                    Type domObjType = System.Type.GetType(objApsChange.details[i].className, true);
                    objApsChange.detailDomainObjectInstancesField[i] = Activator.CreateInstance(domObjType);
                    objXmlSerializer = new XmlSerializer(domObjType);
                    String strXPath = objApsChange.details[i].domainObject.reference;
                    if (strXPath != null)
                    {
                        strXPath = "/com.bts.aps.domain.publish.APSChange" + strXPath.Substring(strXPath.IndexOf("/rootDomainObject"));
                    }
                    else
                    {
                        strXPath = "/com.bts.aps.domain.publish.APSChange/rootDomainObject";
                    }
                    //strXPath = "/com.bts.aps.domain.publish.APSChange" + strXPath.Substring(strXPath.IndexOf("/rootDomainObject"));
                    XmlNode objSelectedNode = objMessage.SelectSingleNode(strXPath);
                    //FindAndReplaceRefenceNodes(objSelectedNode);
                    String strOuterTag = objApsChange.details[i].className;
                    strOuterTag = strOuterTag.Substring(strOuterTag.LastIndexOf(".") + 1);
                    objApsChange.detailDomainObjectInstancesField[i] = objXmlSerializer.Deserialize(new StringReader("<" + strOuterTag + ">" + objSelectedNode.InnerXml + "</" + strOuterTag + ">"));
                }
            }

            return objApsChange;
        }

        private static void ReorderDetails(APSChange apsChange)
        {
            // for the moment, just bubble up agency
            List<APSChangeDetail> originalDetails = new List<APSChangeDetail>();
            List<APSChangeDetail> newDetails = new List<APSChangeDetail>();
            originalDetails.AddRange(apsChange.details);
            foreach (APSChangeDetail var in originalDetails)
            {
                if (typeof(Agency).FullName == var.className)
                {
                    newDetails.Add(var);
                    originalDetails.Remove(var);
                    break;
                }
            }
            foreach (APSChangeDetail var in originalDetails)
            {
                newDetails.Add(var);
            }
            apsChange.details = newDetails.ToArray();
        }

        public static void FindAndReplaceRefenceNodes(XmlNode startNode,
                                                      string className,
                                                      bool isRootAgency,
                                                      string xPath,
                                                      bool isRootCompanyPersonnel,
                                                      string companyPersonnelInnerXml)
        {
            switch (className)
            {

                case "com.bts.aps.domain.AgencyUWPersonRole":
                    #region AgencyUWPersonRole
                    // step 1 get nodes with reference
                    string expression = string.Format("{0}//*[@reference]", xPath);
                    string strAgency = "agency";
                    string strCompanyPersonnel = "companypersonnel";
                    string strCompanyPersonnelRole = "companypersonnelrole";
                    string strAgencyUnderwritingUnit = "agencyunderwritingunit";

                    bool removeAgency = false;
                    if (isRootAgency)
                    {
                        expression = string.Format("{0}//*[@reference]", xPath);
                        removeAgency = true;
                    }

                    // Storing referred nodes
                    XmlNodeList nodes = startNode.SelectNodes(expression);
                    List<XmlNode> referredNodes = new List<XmlNode>(nodes.Count);
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        XmlNode var = nodes[i];
                        string referencePath = var.Attributes["reference"].Value;
                        XmlNode referredNode = var.SelectSingleNode(referencePath);
                        referredNodes.Add(referredNode);
                    }

                    bool smallAgencyExists = false; //Shorter Agency
                    string smallAgencyInnerXml = string.Empty;
                    smallAgencyExists = DetermineIfSmallAgencyExists(xPath, startNode, nodes,
                                            referredNodes, out smallAgencyInnerXml);

                    bool smallCompanyPersonnelRoleExists = false;
                    string smallCompanyPersonnelRoleInnerXml = string.Empty;
                    smallCompanyPersonnelRoleExists =
                        DetermineIfSmallCompanyPersonnelRoleExists(xPath, startNode, nodes,
                            referredNodes, companyPersonnelInnerXml, out smallCompanyPersonnelRoleInnerXml);

                    bool smallAgencyUWUExists = false;
                    string smallAgencyUWUInnerXml = string.Empty;
                    smallAgencyUWUExists = DetermineIfAgencyUWUExists(xPath, startNode, nodes,
                                                                    referredNodes, out smallAgencyUWUInnerXml);


                    for (int i = 0; i < referredNodes.Count; i++)
                    {
                        if (referredNodes[i] != null)
                        {
                            if (removeAgency)
                            {
                                if (!nodes[i].Name.ToLower().Equals(strAgency))
                                {
                                    nodes[i].Attributes.Remove(nodes[i].Attributes["reference"]);
                                    nodes[i].InnerXml = referredNodes[i].InnerXml;
                                }
                            }
                            else
                            {
                                if (nodes[i].Name.ToLower().Equals(strAgency) && smallAgencyExists)
                                {
                                    nodes[i].Attributes.Remove(nodes[i].Attributes["reference"]);
                                    nodes[i].InnerXml = smallAgencyInnerXml; //referredNodes[i].InnerXml;
                                }
                                else if (nodes[i].Name.ToLower().Equals(strCompanyPersonnel) && isRootCompanyPersonnel
                                         && companyPersonnelInnerXml.Length > 0)
                                {
                                    nodes[i].Attributes.Remove(nodes[i].Attributes["reference"]);
                                    nodes[i].InnerXml = companyPersonnelInnerXml;
                                }
                                else if (nodes[i].Name.ToLower().Equals(strCompanyPersonnelRole) && isRootCompanyPersonnel
                                    && smallCompanyPersonnelRoleExists)
                                {
                                    nodes[i].Attributes.Remove(nodes[i].Attributes["reference"]);
                                    nodes[i].InnerXml = smallCompanyPersonnelRoleInnerXml;
                                }
                                else if (nodes[i].Name.ToLower().Equals(strAgencyUnderwritingUnit) && isRootCompanyPersonnel
                                       && smallAgencyUWUExists)
                                {
                                    nodes[i].Attributes.Remove(nodes[i].Attributes["reference"]);
                                    nodes[i].InnerXml = smallAgencyUWUInnerXml;
                                }
                                else
                                {
                                    nodes[i].Attributes.Remove(nodes[i].Attributes["reference"]);
                                    nodes[i].InnerXml = referredNodes[i].InnerXml;
                                }
                            }
                        }

                    }
                    #region SICK
                    /*
                   bool referredNodesExist = false;
                   int cntr = 0;                    
                   XmlNodeList newNodes = null; 

                   while (referredNodesExist)
                    {
                        for (int i = 0; i < referredNodes.Count; i++)
                        {
                            if (referredNodes[i] != null)
                            {
                                if (newNodes != null && newNodes.Count > 0)
                                {
                                    nodes = newNodes;
                                    if (removeAgency)
                                    {
                                        if (!nodes[i].Name.ToLower().Equals("agency"))
                                        {
                                            nodes[i].Attributes.Remove(nodes[i].Attributes["reference"]);
                                            nodes[i].InnerXml = referredNodes[i].InnerXml;
                                        }
                                    }
                                    else
                                    {
                                        if (cntr > 0 && nodes[i].Name.Equals("agency") &&
                                            referredNodes[i].Name.Equals("agency"))
                                        {
                                            nodes[i].Attributes.Remove(nodes[i].Attributes["reference"]);
                                            nodes[i].InnerXml = referredNodes[i].InnerXml;
                                        }
                                        //else
                                        //{
                                        //    newNodes[i].Attributes.Remove(newNodes[i].Attributes["reference"]);
                                        //    newNodes[i].InnerXml = referredNodes[i].InnerXml;
                                        //}
                                    }
                                }
                                else
                                {
                                    if (removeAgency)
                                    {
                                        if (!nodes[i].Name.ToLower().Equals("agency"))
                                        {
                                            nodes[i].Attributes.Remove(nodes[i].Attributes["reference"]);
                                            nodes[i].InnerXml = referredNodes[i].InnerXml;
                                        }
                                    }
                                    else
                                    {
                                        nodes[i].Attributes.Remove(nodes[i].Attributes["reference"]);
                                        nodes[i].InnerXml = referredNodes[i].InnerXml;
                                    }
                                }

                            }
                        }
                        string allExpression = string.Format("{0}//agency[@reference]", xPath);
                        newNodes = startNode.SelectNodes(allExpression);
                        if (newNodes.Count <= 0)
                        {
                            referredNodesExist = false;
                            cntr = 0;
                        }
                        else
                        {
                            referredNodes.Clear();
                            for (int i = 0; i < newNodes.Count; i++)
                            {
                                XmlNode var = newNodes[i];
                                string referencePath = var.Attributes["reference"].Value;
                                XmlNode referredNode = var.SelectSingleNode(referencePath);
                                referredNodes.Add(referredNode);
                                referredNodesExist = true;
                                
                            }
                            cntr++;
                        }
                    }*/
                    #endregion
                    #endregion
                    break;
                case "com.bts.aps.domain.CompanyPersonnelInfo":
                    #region CompanyPersonnelInfo
                    // step 1 get nodes with reference
                    string cpiExpression = string.Format("{0}//*[@reference]", xPath);
                    XmlNodeList cpiNodes = startNode.SelectNodes(cpiExpression);
                    List<XmlNode> cpiReferredNodes = new List<XmlNode>(cpiNodes.Count);

                    for (int i = 0; i < cpiNodes.Count; i++)
                    {
                        XmlNode var = cpiNodes[i];
                        string referencePath = var.Attributes["reference"].Value;
                        XmlNode referredNode = var.SelectSingleNode(referencePath);
                        cpiReferredNodes.Add(referredNode);
                    }

                    for (int i = 0; i < cpiReferredNodes.Count; i++)
                    {
                        if (cpiReferredNodes[i] != null)
                        {
                            cpiNodes[i].Attributes.Remove(cpiNodes[i].Attributes["reference"]);
                            cpiNodes[i].InnerXml = cpiReferredNodes[i].InnerXml;
                        }

                    }
                    #endregion
                    break;
                case "com.bts.aps.domain.AgencyPersonnel":
                    #region AgencyPersonnel
                    // step 1 get nodes with reference
                    string cpiExpression2 = string.Format("{0}//*[@reference]", xPath);
                    XmlNodeList cpiNodes2 = startNode.SelectNodes(cpiExpression2);
                    List<XmlNode> cpiReferredNodes2 = new List<XmlNode>(cpiNodes2.Count);

                    for (int i = 0; i < cpiNodes2.Count; i++)
                    {
                        XmlNode var = cpiNodes2[i];
                        string referencePath = var.Attributes["reference"].Value;
                        XmlNode referredNode = var.SelectSingleNode(referencePath).Clone();

                        if (referredNode.HasChildNodes)
                        {
                            for (int j = referredNode.ChildNodes.Count - 1; j > 0; j--)
                            {
                                if (referredNode.ChildNodes[j].HasChildNodes && referredNode.ChildNodes[j].ChildNodes.Count > 1)
                                    referredNode.RemoveChild(referredNode.ChildNodes[j]);
                            }
                        }

                        cpiReferredNodes2.Add(referredNode);
                    }

                    for (int i = 0; i < cpiReferredNodes2.Count; i++)
                    {
                        if (cpiReferredNodes2[i] != null)
                        {
                            cpiNodes2[i].Attributes.Remove(cpiNodes2[i].Attributes["reference"]);
                            cpiNodes2[i].InnerXml = cpiReferredNodes2[i].InnerXml;
                        }
                    }
                    break;
                    #endregion
                case "com.bts.aps.domain.Agency":
                    string aExp = string.Format("{0}//*[@reference]", xPath);
                    XmlNodeList aNodes = startNode.SelectNodes(aExp);
                    List<XmlNode> aReferredNodes = new List<XmlNode>();

                    for (int i = 0; i < aNodes.Count; i++)
                    {
                        XmlNode var = aNodes[i];
                        string referencePath = var.Attributes["reference"].Value;
                        XmlNode referredNode = var.SelectSingleNode(referencePath).Clone();

                        if (referredNode.HasChildNodes)
                        {
                            for (int j = referredNode.ChildNodes.Count - 1; j > 0; j--)
                            {
                                if (referredNode.ChildNodes[j].HasChildNodes && referredNode.ChildNodes[j].ChildNodes.Count > 1)
                                    referredNode.RemoveChild(referredNode.ChildNodes[j]);
                            }
                        }
                        aReferredNodes.Add(referredNode);
                    }

                    for (int i = 0; i < aReferredNodes.Count; i++)
                    {
                        if (aReferredNodes[i] != null)
                        {
                            aNodes[i].Attributes.Remove(aNodes[i].Attributes["reference"]);
                            aNodes[i].InnerXml = aReferredNodes[i].InnerXml;
                        }
                    }

                    break;


                default:
                    break;
            }
            return;
        }

        private static bool DetermineIfAgencyUWUExists(string xPath, XmlNode startNode,
                                                                    XmlNodeList nodes, List<XmlNode> referredNodes,
                                                                    out string smallAgencyUWUnitInnerXml)
        {
            bool smallAgencyUWUExists = false;
            smallAgencyUWUnitInnerXml = string.Empty;

            string strAgencyUnderwitingUnitNode = string.Format("{0}/agencyUnderwritingUnit", xPath);

            XmlNode lAgencyUWUnitNode = startNode.SelectSingleNode(strAgencyUnderwitingUnitNode);
            bool lAgencyUWUnitNodeExists = lAgencyUWUnitNode != null;
            bool lAgencyUWUnitRefAttrExists = lAgencyUWUnitNodeExists && lAgencyUWUnitNode.Attributes["reference"] != null &&
                                            lAgencyUWUnitNode.Attributes["reference"].Value != null;


            string strUWUNode = string.Format("{0}/underwritingUnit", strAgencyUnderwitingUnitNode);
            XmlNode lUWUnitNode = startNode.SelectSingleNode(strUWUNode);
            bool lUWUnitNodeExists = lUWUnitNode != null;
            bool lUWUnitRefAttrExists = lUWUnitNodeExists && lUWUnitNode.Attributes["reference"] != null &&
                                            lUWUnitNode.Attributes["reference"].Value != null;

            //// Underwriting Unit.
            string smallUWUNodeXml = string.Empty;
            bool blSmallUWUNodeExists = lUWUnitNodeExists && !lUWUnitRefAttrExists;
            if (blSmallUWUNodeExists)
            {
                smallUWUNodeXml = AddUWUSmallNodesToXml(lUWUnitNode);
            }
            else
            {
                for (int i = 0; i < referredNodes.Count; i++)
                {
                    if (referredNodes[i] != null && !blSmallUWUNodeExists)
                    {
                        if (nodes[i].Name.ToLower().Equals("agencyunderwritingunit"))
                        {
                            XmlNode tempUWUNode = referredNodes[i].SelectSingleNode("underwritingUnit");
                            XmlNode tempUWUDescNode = tempUWUNode.SelectSingleNode("description");
                            if (tempUWUDescNode != null && tempUWUDescNode.InnerText.Length > 0)
                            {
                                smallUWUNodeXml = AddUWUSmallNodesToXml(tempUWUNode);
                                blSmallUWUNodeExists = true;
                                break;
                            }
                        }
                    }
                }
            }
            //Complete the above

            string strSmallAgencyXml = string.Empty;
            bool blSmallAgencyExists = false;
            blSmallAgencyExists = DetermineIfSmallAgencyExists(xPath, startNode, nodes, referredNodes, out strSmallAgencyXml);

            if (blSmallAgencyExists && blSmallUWUNodeExists)
            {
                smallAgencyUWUnitInnerXml = AddAgencyUWUSmallNodesToXml(strSmallAgencyXml, smallUWUNodeXml);
                smallAgencyUWUExists = true;
            }

            return smallAgencyUWUExists;
        }


        private static bool DetermineIfSmallAgencyExists(string xPath, XmlNode startNode, XmlNodeList nodes, List<XmlNode> referredNodes, out string smallAgencyInnerXml)
        {
            bool smallAgencyExists = false;
            smallAgencyInnerXml = string.Empty;

            string lAgencyNode = string.Format("{0}/agencyUnderwritingUnit/agency", xPath);
            string lAgencyCodeNode = string.Format("{0}/agencyCode", lAgencyNode);

            XmlNode lnAgencyNode = startNode.SelectSingleNode(lAgencyNode);
            bool blAgencyNodeExists = (lnAgencyNode != null);
            bool lAgencyNodeRefAttrExists = blAgencyNodeExists && lnAgencyNode.Attributes["reference"] != null &&
                                            lnAgencyNode.Attributes["reference"].Value != null;


            smallAgencyInnerXml = string.Empty;
            smallAgencyExists = blAgencyNodeExists && !lAgencyNodeRefAttrExists;

            if (smallAgencyExists)
            {
                smallAgencyInnerXml = AddAgencySmallNodesToXml(lnAgencyNode);
            }
            else
            {
                for (int i = 0; i < referredNodes.Count; i++)
                {
                    if (referredNodes[i] != null && !smallAgencyExists)
                    {
                        if (nodes[i].Name.ToLower().Equals("agency"))
                        {
                            XmlNode tempAgencyNode = referredNodes[i].SelectSingleNode("agencyCode"); //check if need to be replaced by refererrednodes
                            if (tempAgencyNode != null && tempAgencyNode.InnerText.Length > 0)
                            {
                                smallAgencyInnerXml = AddAgencySmallNodesToXml(nodes[i]);
                                smallAgencyExists = true;
                                break;
                            }
                        }
                    }
                }
            }

            return smallAgencyExists;

        }


        private static bool DetermineIfSmallCompanyPersonnelRoleExists(
                                                            string xPath, XmlNode startNode,
                                                            XmlNodeList nodes, List<XmlNode> referredNodes,
                                                            string companyPersonnelInnerXml,
                                                            out string smallCompanyPersonnelRoleInnerXml)
        {
            bool smallCompanyPersonnelRoleExists = false;
            smallCompanyPersonnelRoleInnerXml = string.Empty;

            bool smallCompanyPersonnelRoleTypeExists = false;
            string smallCompanyPersonnelRoleTypeInnerXml = string.Empty;


            string strCompanyPersonnelRoleNode = string.Format("{0}/companyPersonnelRole", xPath);
            string strCompanyPersonnelRoleTypeNode = string.Format("{0}/companyPersonnelRoleType", strCompanyPersonnelRoleNode);

            //Company Personnel Role
            XmlNode lnCompanyPersonnelRoleNode = startNode.SelectSingleNode(strCompanyPersonnelRoleNode);
            bool blCompanyPersonnelRoleNodeExists = (lnCompanyPersonnelRoleNode != null);
            bool blCompanyPersonnelRoleNodeRefAttrExists = blCompanyPersonnelRoleNodeExists &&
                                                           lnCompanyPersonnelRoleNode.Attributes["reference"] != null &&
                                                           lnCompanyPersonnelRoleNode.Attributes["reference"].Value != null;

            //Company Personnel Role Type
            XmlNode lnCompanyPersonnelRoleTypeNode = startNode.SelectSingleNode(strCompanyPersonnelRoleTypeNode);
            bool blCompanyPersonnelRoleTypeNodeExists = (lnCompanyPersonnelRoleTypeNode != null);
            bool blCompanyPersonnelRoleTypeNodeRefAttrExists = blCompanyPersonnelRoleTypeNodeExists &&
                                                           lnCompanyPersonnelRoleTypeNode.Attributes["reference"] != null &&
                                                           lnCompanyPersonnelRoleTypeNode.Attributes["reference"].Value != null;

            smallCompanyPersonnelRoleTypeInnerXml = string.Empty;
            smallCompanyPersonnelRoleTypeExists = blCompanyPersonnelRoleTypeNodeExists && !blCompanyPersonnelRoleTypeNodeRefAttrExists;
            string smallCompanyPersonnelRoleTypeXml = string.Empty;

            if (smallCompanyPersonnelRoleTypeExists)
            {
                smallCompanyPersonnelRoleTypeXml = AddCompanyPersonnelRoleTypeSmallNodesToXml(lnCompanyPersonnelRoleTypeNode);
            }
            else
            {
                for (int i = 0; i < referredNodes.Count; i++)
                {
                    if (referredNodes[i] != null && !smallCompanyPersonnelRoleTypeExists)
                    {
                        if (nodes[i].Name.ToLower().Equals("companypersonnelroletype"))
                        {
                            XmlNode tempCompanyPersonnelRoleTypeLabelNode = referredNodes[i].SelectSingleNode("label");
                            if (tempCompanyPersonnelRoleTypeLabelNode != null && tempCompanyPersonnelRoleTypeLabelNode.InnerText.Length > 0)
                            {
                                smallCompanyPersonnelRoleTypeXml =
                                    AddCompanyPersonnelRoleTypeSmallNodesToXml(tempCompanyPersonnelRoleTypeLabelNode);
                                smallCompanyPersonnelRoleTypeExists = true;
                                break;
                            }
                        }
                    }
                }

            }

            smallCompanyPersonnelRoleInnerXml = string.Empty;
            smallCompanyPersonnelRoleExists = blCompanyPersonnelRoleNodeExists &&
                                              !blCompanyPersonnelRoleNodeRefAttrExists &&
                                              smallCompanyPersonnelRoleTypeExists;


            if (smallCompanyPersonnelRoleExists)
            {
                smallCompanyPersonnelRoleInnerXml = AddCompanyPersonnelRoleSmallNodesToXml(smallCompanyPersonnelRoleTypeXml, companyPersonnelInnerXml);
            }
            else
            {
                for (int i = 0; i < referredNodes.Count; i++)
                {
                    if (referredNodes[i] != null && !smallCompanyPersonnelRoleExists)
                    {
                        if (nodes[i].Name.ToLower().Equals("companypersonnelrole"))
                        {
                            smallCompanyPersonnelRoleInnerXml = AddCompanyPersonnelRoleSmallNodesToXml(
                                                                    smallCompanyPersonnelRoleTypeXml,
                                                                    companyPersonnelInnerXml);
                            smallCompanyPersonnelRoleExists = true;
                            break;

                        }
                    }
                }

            }

            return smallCompanyPersonnelRoleExists;

        }

        public static string AddCompanyPersonnelRoleTypeSmallNodesToXml(XmlNode companyPersonnelRoleTypeNode)
        {
            string label = companyPersonnelRoleTypeNode.InnerText.Trim();
            string strSmallCompanyPersonnelRoleTypeXml = string.Empty;
            strSmallCompanyPersonnelRoleTypeXml =
                string.Format("<companyPersonnelRoleType><label>{0}</label></companyPersonnelRoleType>", label);
            return strSmallCompanyPersonnelRoleTypeXml;
        }

        public static string AddUWUSmallNodesToXml(XmlNode uwuNode)
        {
            //<underwritingUnit>
            //<id>103164257063263889</id>
            //<description>Commercial</description>
            //<code>CL</code>
            //<agencyUnderwritingUnits class="set"/>
            //</underwritingUnit>
            string id = uwuNode.SelectSingleNode("id") != null ? uwuNode.SelectSingleNode("id").InnerText.Trim() : string.Empty;
            string desc = uwuNode.SelectSingleNode("description") != null ? uwuNode.SelectSingleNode("description").InnerText.Trim() : string.Empty;
            string code = uwuNode.SelectSingleNode("code") != null ? uwuNode.SelectSingleNode("code").InnerText.Trim() : string.Empty; ;

            string strSmallUWUXml = string.Empty;
            strSmallUWUXml = string.Format("<underwritingUnit><id>{0}</id><description>{1}</description>" +
                                            "<code>{2}</code></underwritingUnit>", id, desc, code);
            return strSmallUWUXml;
        }

        public static string AddCompanyPersonnelRoleSmallNodesToXml(string strSmallCompanyPersonnelRoleTypeXml,
                                                                    string compPersonnelInnerXml)
        {
            //USG: what if the RoleType is referred?

            //string label = fullCompanyPersonnelRoleNode.SelectSingleNode("companyPersonnelRoleType/label").InnerText.Trim();

            string strSmallCompanyPersonnelRole = string.Empty;
            if (strSmallCompanyPersonnelRoleTypeXml != null && strSmallCompanyPersonnelRoleTypeXml.Length > 0)
            {
                strSmallCompanyPersonnelRole = string.Format("{0}<companyPersonnel>{1}</companyPersonnel>"
                                                , strSmallCompanyPersonnelRoleTypeXml, compPersonnelInnerXml);
            }

            return strSmallCompanyPersonnelRole;
        }

        public static string AddAgencySmallNodesToXml(XmlNode fullAgencyNode)
        {
            string agencyCode = fullAgencyNode.SelectSingleNode("agencyCode").InnerText;
            long agencyApsId = long.Parse(fullAgencyNode.SelectSingleNode("id").InnerText);

            string strSmallAgency = string.Empty;
            if (agencyCode != null && agencyApsId > long.MinValue)
                strSmallAgency = string.Format("<id>{0}</id><agencyCode>{1}</agencyCode>", agencyApsId, agencyCode);

            return strSmallAgency;
        }

        public static string AddAgencyUWUSmallNodesToXml(string smallAgencyXml, string smallUWU)
        {
            string strSmallAgencyUWU = string.Empty;
            strSmallAgencyUWU = string.Format("<agency>{0}</agency>{1}", smallAgencyXml, smallUWU);
            return strSmallAgencyUWU;
        }

        #region APSChange Properties
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        public string companyAbbreviation
        {
            get
            {
                return this.companyAbbreviationField;
            }
            set
            {
                this.companyAbbreviationField = value;
            }
        }

        /// <remarks/>
        public string effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        public string userName
        {
            get
            {
                return this.userNameField;
            }
            set
            {
                this.userNameField = value;
            }
        }

        /// <remarks/>
        public string rootClass
        {
            get
            {
                return this.rootClassField;
            }
            set
            {
                this.rootClassField = value;
            }
        }

        public bool isRootClassAgency
        {
            get
            {
                return (rootClassField != null && rootClassField.Equals(rootClass_Agency));
            }
        }

        public bool isRootClassCompanyPersonnel
        {
            get
            {
                return (rootClassField != null && rootClassField.Equals(rootClass_CompanyPersonnel));
            }
        }

        public string companyPersonnel_FirstName
        {
            get
            {
                string firstName = string.Empty;
                if (isRootClassCompanyPersonnel)
                {
                    XmlNode _rootNode = m_objMessage.SelectSingleNode(rootObject_XPath);
                    XmlNode _firstNameCodeNode = _rootNode.SelectSingleNode("firstName");
                    if (_firstNameCodeNode == null || _rootNode == null)
                    {
                        throw new Exception("Could not find a node at path '" + rootObject_XPath + "/firstName'.");
                    }
                    firstName = _firstNameCodeNode.InnerText;

                }
                return firstName;
            }

        }

        public string companyPersonnelInnerXml
        {
            get
            {
                string innerXml = string.Empty;
                if (isRootClassCompanyPersonnel)
                {
                    innerXml = string.Format("<firstName>{0}</firstName><lastName>{1}</lastName><userId>{2}</userId>",
                                this.companyPersonnel_FirstName, this.companyPersonnel_LastName, this.companyPersonnel_UserId);

                }
                return innerXml;
            }
        }

        public string companyPersonnel_LastName
        {
            get
            {
                string lastName = string.Empty;
                if (isRootClassCompanyPersonnel)
                {
                    XmlNode _rootNode = m_objMessage.SelectSingleNode(rootObject_XPath);
                    XmlNode _lastNameCodeNode = _rootNode.SelectSingleNode("lastName");
                    if (_lastNameCodeNode == null || _rootNode == null)
                    {
                        //throw new Exception("Could not find a node at path '" + rootObject_XPath + "/lastName'.");
                        lastName = string.Empty;
                    }
                    else
                        lastName = _lastNameCodeNode.InnerText;
                }
                return lastName;
            }

        }
        public string companyPersonnel_UserId
        {
            get
            {
                string userid = string.Empty;
                if (isRootClassCompanyPersonnel)
                {
                    XmlNode _rootNode = m_objMessage.SelectSingleNode(rootObject_XPath);
                    XmlNode _useridCodeNode = _rootNode.SelectSingleNode("userId");
                    if (_useridCodeNode == null || _rootNode == null)
                    {
                        throw new Exception("Could not find a node at path '" + rootObject_XPath + "/userId'.");
                    }
                    userid = _useridCodeNode.InnerText;

                }
                return userid;
            }

        }

        public long companyPersonnel_APSID
        {
            get
            {
                long apsID = long.MinValue;
                if (isRootClassCompanyPersonnel)
                {
                    XmlNode _rootNode = m_objMessage.SelectSingleNode(rootObject_XPath);
                    XmlNode _apsIDCodeNode = _rootNode.SelectSingleNode("companyPersonnelInfo/id");
                    if (_apsIDCodeNode == null || _rootNode == null)
                    {
                        throw new Exception("Could not find a node at path '" + rootObject_XPath + "/companyPersonnelInfo/id'.");
                    }
                    apsID = long.Parse(_apsIDCodeNode.InnerText.Trim());

                }
                return apsID;
            }

        }


        /// <remarks/>
        public long rootId
        {
            get
            {
                return this.rootIdField;
            }
            set
            {
                this.rootIdField = value;
            }
        }

        /// <remarks/>
        public rootDomainObject rootDomainObject
        {
            get
            {
                return this.rootDomainObjectField;
            }
            set
            {
                this.rootDomainObjectField = value;
            }
        }

        /// <remarks/>
        public string rootAgencyCode
        {
            get
            {
                string rootAgencyCode = string.Empty;
                try
                {
                    if (rootClassField.Equals(rootClass_Agency))
                    {
                        XmlNode _rootNode = m_objMessage.SelectSingleNode(rootObject_XPath);
                        XmlNode _agencyCodeNode = _rootNode.SelectSingleNode("agencyCode");
                        if (_agencyCodeNode == null || _rootNode == null)
                        {
                            throw new Exception("Could not find a node at path '" + rootObject_XPath + "/agencyCode'.");
                        }
                        rootAgencyCode = _agencyCodeNode.InnerText;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return rootAgencyCode;
            }
        }


        /// <summary>
        /// returns the Group Type Id in the given xml file
        /// </summary>
        /// 

        public string rootAgencyGroupType
        {
            get
            {
                string rootGroupType = string.Empty;
                try
                {
                    if (rootClassField.Equals(rootClass_Agency))
                    {
                        XmlNode _rootNode = m_objMessage.SelectSingleNode(rootObject_XPath);
                        XmlNode _agencyGroupTypeNode = _rootNode.SelectSingleNode("agencyGroupType");
                        if (_agencyGroupTypeNode == null || _rootNode == null)
                        {
                            throw new Exception("Could not find a node at path '" + rootObject_XPath + "/agencyGroupType'.");
                        }
                        XmlNode rootGroupTypeNode = _agencyGroupTypeNode.FirstChild;
                        return rootGroupTypeNode.InnerText.ToString();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return string.Empty;
            }
        }
        public long rootAgencyApsId
        {
            get
            {
                long rootAgencyApsId = long.MinValue;
                try
                {
                    if (rootClassField.Equals(rootClass_Agency))
                    {
                        XmlNode _rootNode = m_objMessage.SelectSingleNode(rootObject_XPath);
                        XmlNode _idNode = _rootNode.SelectSingleNode("id");
                        if (_idNode == null || _rootNode == null)
                        {
                            throw new Exception("Could not find a node at path '" + rootObject_XPath + "/id'.");
                        }
                        string val = _idNode.InnerText;
                        rootAgencyApsId = long.Parse(val);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return rootAgencyApsId;
            }
        }
        #endregion

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("com.bts.aps.domain.publish.APSChangeDetail", IsNullable = false)]
        public APSChangeDetail[] details
        {
            get
            {
                return this.detailsField;
            }
            set
            {
                this.detailsField = value;
            }
        }

        public object detailDomainObjectInstance(Int32 Index)
        {
            object objDomainObjectInstance = null;

            if (detailDomainObjectInstancesField == null)
            {
                Type domObjType = System.Type.GetType(this.details[Index].className, true);
                objDomainObjectInstance = Activator.CreateInstance(domObjType);
                XmlSerializer objXmlSerializer = new XmlSerializer(domObjType);
                String strXPath = this.details[Index].domainObject.reference;
                //strXPath = "/com.bts.aps.domain.publish.APSChange";// +strXPath.Substring(strXPath.IndexOf("/rootDomainObject"));
                bool noDetailReference = false;
                if (strXPath != null)
                {
                    strXPath = "/com.bts.aps.domain.publish.APSChange" + strXPath.Substring(strXPath.IndexOf("/rootDomainObject"));
                }
                else
                {
                    strXPath = "/com.bts.aps.domain.publish.APSChange/rootDomainObject";
                    noDetailReference = true;
                }
                XmlNode objSelectedNode = null;
                long modType = this.details[Index].modificationType;
                bool changeInsertOrUpdate = modType == 1 || modType == 2;

                //Go into FindAndReplace only if there is a detail reference node
                if (!noDetailReference)
                {
                    objSelectedNode = m_objMessage.SelectSingleNode(strXPath);
                    FindAndReplaceRefenceNodes(objSelectedNode,
                                                this.details[Index].className,
                                                this.isRootClassAgency,
                                                strXPath,
                                                this.isRootClassCompanyPersonnel,
                                                this.companyPersonnelInnerXml);

                }
                else
                {
                    string strXPathAPSChangeDetail = string.Format("/com.bts.aps.domain.publish.APSChange/details" +
                                            "/com.bts.aps.domain.publish.APSChangeDetail[{0}]/domainObject", Index + 1);

                    objSelectedNode = m_objMessage.SelectSingleNode(strXPathAPSChangeDetail); ;
                    //BuildNodeDirectlyfromChangeDetail(objSelectedNode, this.details[Index].className);
                }
                String strOuterTag = this.details[Index].className;
                strOuterTag = strOuterTag.Substring(strOuterTag.LastIndexOf(".") + 1);
                objDomainObjectInstance = objXmlSerializer.Deserialize(new StringReader("<" + strOuterTag + ">" + objSelectedNode.InnerXml + "</" + strOuterTag + ">"));

            }
            else
                objDomainObjectInstance = detailDomainObjectInstancesField[Index];

            return objDomainObjectInstance;
        }
    }
}