using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain.publish
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute("com.bts.aps.domain.publish.APSChangeDetail", IsNullable = false)]
    public class APSChangeDetail
    {
        public const long MODIFICATION_TYPE_INSERT = 1;
        public const long MODIFICATION_TYPE_UPDATE = 2;
        public const long MODIFICATION_TYPE_DELETE = 3;

        private long idField;

        private string classNameField;

        private long objectIdField;

        private long modificationTypeField;

        private domainObject domainObjectField;

        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        public string className
        {
            get
            {
                return this.classNameField;
            }
            set
            {
                this.classNameField = value;
            }
        }

        public long objectId
        {
            get
            {
                return this.objectIdField;
            }
            set
            {
                this.objectIdField = value;
            }
        }

        public long modificationType
        {
            get
            {
                return this.modificationTypeField;
            }
            set
            {
                this.modificationTypeField = value;
            }
        }

        public domainObject domainObject
        {
            get
            {
                return this.domainObjectField;
            }
            set
            {
                this.domainObjectField = value;
            }
        }
    }
}
