using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyXRef
    {

        private AgencyXRefAssociationType[] agencyXRefAssociationTypesField;

        private Agency childAgencyField;

        private String effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private String effectiveDateNewField;

        private bool effectiveDateNewFieldSpecified;

        private String effectiveDateRenewalField;

        private bool effectiveDateRenewalFieldSpecified;

        private String expirationDateField;

        private bool expirationDateFieldSpecified;

        private String expirationDateNewField;

        private bool expirationDateNewFieldSpecified;

        private String expirationDateRenewalField;

        private bool expirationDateRenewalFieldSpecified;

        private long idField;

        private Agency parentAgencyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("com.bts.aps.domain.AgencyXRefAssociationType", IsNullable = false)]
        public AgencyXRefAssociationType[] agencyXRefAssociationTypes
        {
            get
            {
                return this.agencyXRefAssociationTypesField;
            }
            set
            {
                this.agencyXRefAssociationTypesField = value;
            }
        }

        /// <remarks/>
        public Agency childAgency
        {
            get
            {
                return this.childAgencyField;
            }
            set
            {
                this.childAgencyField = value;
            }
        }

        /// <remarks/>
        public String effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        public bool effectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String effectiveDateNew
        {
            get
            {
                return this.effectiveDateNewField;
            }
            set
            {
                this.effectiveDateNewField = value;
            }
        }

        /// <remarks/>
        public bool effectiveDateNewSpecified
        {
            get
            {
                return this.effectiveDateNewFieldSpecified;
            }
            set
            {
                this.effectiveDateNewFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String effectiveDateRenewal
        {
            get
            {
                return this.effectiveDateRenewalField;
            }
            set
            {
                this.effectiveDateRenewalField = value;
            }
        }

        /// <remarks/>
        public bool effectiveDateRenewalSpecified
        {
            get
            {
                return this.effectiveDateRenewalFieldSpecified;
            }
            set
            {
                this.effectiveDateRenewalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        public bool expirationDateSpecified
        {
            get
            {
                return this.expirationDateFieldSpecified;
            }
            set
            {
                this.expirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String expirationDateNew
        {
            get
            {
                return this.expirationDateNewField;
            }
            set
            {
                this.expirationDateNewField = value;
            }
        }

        /// <remarks/>
        public bool expirationDateNewSpecified
        {
            get
            {
                return this.expirationDateNewFieldSpecified;
            }
            set
            {
                this.expirationDateNewFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String expirationDateRenewal
        {
            get
            {
                return this.expirationDateRenewalField;
            }
            set
            {
                this.expirationDateRenewalField = value;
            }
        }

        /// <remarks/>
        public bool expirationDateRenewalSpecified
        {
            get
            {
                return this.expirationDateRenewalFieldSpecified;
            }
            set
            {
                this.expirationDateRenewalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public Agency parentAgency
        {
            get
            {
                return this.parentAgencyField;
            }
            set
            {
                this.parentAgencyField = value;
            }
        }
    }
}
