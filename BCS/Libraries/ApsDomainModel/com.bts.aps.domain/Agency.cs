using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class Agency
    {

        private AgencyAffiliation[] agencyAffiliationsField;

        private AgencyBranch[] agencyBranchesField;

        private AgencyCategory agencyCategoryField;

        private string agencyCodeField;

        private AgencyCustomValue[] agencyCustomValuesField;

        private AgencyEOInfo agencyEOInfoField;

        private AgencyGroupType agencyGroupTypeField;

        private AgencyLicense[] agencyLicensesField;

        private AgencyName[] agencyNamesField;

        private AgencyNote[] agencyNotesField;

        private AgencyPersonnel[] agencyPersonsField;

        private AgencyProgram[] agencyProgramsField;

        private AgencyRank agencyRankField;

        private AgencyStatementType agencyStatementTypeField;

        private AgencyStatus agencyStatusField;

        private AgencyTerritory agencyTerritoryField;

        private AgencyType agencyTypeField;

        private AgencyUnderwritingUnit[] agencyUnderwritingUnitsField;

        private AgencyXRef[] agencyXRefChildrenField;

        private AgencyXRef[] agencyXRefParentsField;

        private AnnualPremiumGoal[] annualPremiumGoalsField;

        private String appointmentDateField;

        private bool appointmentDateFieldSpecified;

        private Automation[] automationsField;

        private BillType billTypeField;

        private Branch branchField;

        private AgencyCancelReason cancelReasonField;

        private Competitor[] competitorsField;

        private string dbaNameField;

        private FinancialAccount[] financialAccountsField;

        private String foundingDateField;

        private bool foundingDateFieldSpecified;

        private string homeCityField;

        private USState homeStateField;

        private long idField;

        private string legalNameField;

        private Location[] locationsField;

        private String newCancelDateField;

        private bool newCancelDateFieldSpecified;

        private PreviousAppointment[] previousAppointmentsField;

        private string programNotesField;

        private String renewalCancelDateField;

        private bool renewalCancelDateFieldSpecified;

        private bool requires1099Field;

        private Company servicingCompanyField;

        private string shortNameField;

        private string taxIdField;

        private TotalPremiumBusinessMix totalPremiumBusinessMixField;

        private String transferDateField;

        private bool transferDateFieldSpecified;

        private String transferProcessDateField;

        private bool transferProcessDateFieldSpecified;

        private Agency transferredToAgencyField;

        private string webUrlField;

        public AgencyAffiliation[] agencyAffiliations
        {
            get
            {
                return this.agencyAffiliationsField;
            }
            set
            {
                this.agencyAffiliationsField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("com.bts.aps.domain.AgencyBranch", IsNullable = false)]
        public AgencyBranch[] agencyBranches
        {
            get
            {
                return this.agencyBranchesField;
            }
            set
            {
                this.agencyBranchesField = value;
            }
        }

        public AgencyCategory agencyCategory
        {
            get
            {
                return this.agencyCategoryField;
            }
            set
            {
                this.agencyCategoryField = value;
            }
        }

        public string agencyCode
        {
            get
            {
                return this.agencyCodeField;
            }
            set
            {
                this.agencyCodeField = value;
            }
        }

        public AgencyCustomValue[] agencyCustomValues
        {
            get
            {
                return this.agencyCustomValuesField;
            }
            set
            {
                this.agencyCustomValuesField = value;
            }
        }

        public AgencyEOInfo agencyEOInfo
        {
            get
            {
                return this.agencyEOInfoField;
            }
            set
            {
                this.agencyEOInfoField = value;
            }
        }

        public AgencyGroupType agencyGroupType
        {
            get
            {
                return this.agencyGroupTypeField;
            }
            set
            {
                this.agencyGroupTypeField = value;
            }
        }

        public AgencyLicense[] agencyLicenses
        {
            get
            {
                return this.agencyLicensesField;
            }
            set
            {
                this.agencyLicensesField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("com.bts.aps.domain.AgencyName", IsNullable = false)]
        public AgencyName[] agencyNames
        {
            get
            {
                return this.agencyNamesField;
            }
            set
            {
                this.agencyNamesField = value;
            }
        }

        public AgencyNote[] agencyNotes
        {
            get
            {
                return this.agencyNotesField;
            }
            set
            {
                this.agencyNotesField = value;
            }
        }

        public AgencyPersonnel[] agencyPersons
        {
            get
            {
                return this.agencyPersonsField;
            }
            set
            {
                this.agencyPersonsField = value;
            }
        }

        public AgencyProgram[] agencyPrograms
        {
            get
            {
                return this.agencyProgramsField;
            }
            set
            {
                this.agencyProgramsField = value;
            }
        }

        public AgencyRank agencyRank
        {
            get
            {
                return this.agencyRankField;
            }
            set
            {
                this.agencyRankField = value;
            }
        }

        public AgencyStatementType agencyStatementType
        {
            get
            {
                return this.agencyStatementTypeField;
            }
            set
            {
                this.agencyStatementTypeField = value;
            }
        }

        public AgencyStatus agencyStatus
        {
            get
            {
                return this.agencyStatusField;
            }
            set
            {
                this.agencyStatusField = value;
            }
        }

        public AgencyTerritory agencyTerritory
        {
            get
            {
                return this.agencyTerritoryField;
            }
            set
            {
                this.agencyTerritoryField = value;
            }
        }

        public AgencyType agencyType
        {
            get
            {
                return this.agencyTypeField;
            }
            set
            {
                this.agencyTypeField = value;
            }
        }

        public AgencyUnderwritingUnit[] agencyUnderwritingUnits
        {
            get
            {
                return this.agencyUnderwritingUnitsField;
            }
            set
            {
                this.agencyUnderwritingUnitsField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("com.bts.aps.domain.AgencyXRef", IsNullable = false)]
        public AgencyXRef[] agencyXRefChildren
        {
            get
            {
                return this.agencyXRefChildrenField;
            }
            set
            {
                this.agencyXRefChildrenField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("com.bts.aps.domain.AgencyXRef", IsNullable = false)]
        public AgencyXRef[] agencyXRefParents
        {
            get
            {
                return this.agencyXRefParentsField;
            }
            set
            {
                this.agencyXRefParentsField = value;
            }
        }

        public AnnualPremiumGoal[] annualPremiumGoals
        {
            get
            {
                return this.annualPremiumGoalsField;
            }
            set
            {
                this.annualPremiumGoalsField = value;
            }
        }

        public String appointmentDate
        {
            get
            {
                return this.appointmentDateField;
            }
            set
            {
                this.appointmentDateField = value;
            }
        }

        public bool appointmentDateSpecified
        {
            get
            {
                return this.appointmentDateFieldSpecified;
            }
            set
            {
                this.appointmentDateFieldSpecified = value;
            }
        }

        public Automation[] automations
        {
            get
            {
                return this.automationsField;
            }
            set
            {
                this.automationsField = value;
            }
        }

        public BillType billType
        {
            get
            {
                return this.billTypeField;
            }
            set
            {
                this.billTypeField = value;
            }
        }

        public Branch branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        public AgencyCancelReason cancelReason
        {
            get
            {
                return this.cancelReasonField;
            }
            set
            {
                this.cancelReasonField = value;
            }
        }

        public Competitor[] competitors
        {
            get
            {
                return this.competitorsField;
            }
            set
            {
                this.competitorsField = value;
            }
        }

        public string dbaName
        {
            get
            {
                return this.dbaNameField;
            }
            set
            {
                this.dbaNameField = value;
            }
        }

        public FinancialAccount[] financialAccounts
        {
            get
            {
                return this.financialAccountsField;
            }
            set
            {
                this.financialAccountsField = value;
            }
        }

        public String foundingDate
        {
            get
            {
                return this.foundingDateField;
            }
            set
            {
                this.foundingDateField = value;
            }
        }

        public bool foundingDateSpecified
        {
            get
            {
                return this.foundingDateFieldSpecified;
            }
            set
            {
                this.foundingDateFieldSpecified = value;
            }
        }

        public string homeCity
        {
            get
            {
                return this.homeCityField;
            }
            set
            {
                this.homeCityField = value;
            }
        }

        public USState homeState
        {
            get
            {
                return this.homeStateField;
            }
            set
            {
                this.homeStateField = value;
            }
        }

        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        public string legalName
        {
            get
            {
                return this.legalNameField;
            }
            set
            {
                this.legalNameField = value;
            }
        }

        [System.Xml.Serialization.XmlArrayItemAttribute("com.bts.aps.domain.Location", IsNullable = false)]
        public Location[] locations
        {
            get
            {
                return this.locationsField;
            }
            set
            {
                this.locationsField = value;
            }
        }

        public String newCancelDate
        {
            get
            {
                return this.newCancelDateField;
            }
            set
            {
                this.newCancelDateField = value;
            }
        }

        public bool newCancelDateSpecified
        {
            get
            {
                return this.newCancelDateFieldSpecified;
            }
            set
            {
                this.newCancelDateFieldSpecified = value;
            }
        }

        public PreviousAppointment[] previousAppointments
        {
            get
            {
                return this.previousAppointmentsField;
            }
            set
            {
                this.previousAppointmentsField = value;
            }
        }

        public string programNotes
        {
            get
            {
                return this.programNotesField;
            }
            set
            {
                this.programNotesField = value;
            }
        }

        public String renewalCancelDate
        {
            get
            {
                return this.renewalCancelDateField;
            }
            set
            {
                this.renewalCancelDateField = value;
            }
        }

        public bool renewalCancelDateSpecified
        {
            get
            {
                return this.renewalCancelDateFieldSpecified;
            }
            set
            {
                this.renewalCancelDateFieldSpecified = value;
            }
        }

        public bool requires1099
        {
            get
            {
                return this.requires1099Field;
            }
            set
            {
                this.requires1099Field = value;
            }
        }

        public Company servicingCompany
        {
            get
            {
                return this.servicingCompanyField;
            }
            set
            {
                this.servicingCompanyField = value;
            }
        }

        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }

        public string taxId
        {
            get
            {
                return this.taxIdField;
            }
            set
            {
                this.taxIdField = value;
            }
        }

        public TotalPremiumBusinessMix totalPremiumBusinessMix
        {
            get
            {
                return this.totalPremiumBusinessMixField;
            }
            set
            {
                this.totalPremiumBusinessMixField = value;
            }
        }

        public String transferDate
        {
            get
            {
                return this.transferDateField;
            }
            set
            {
                this.transferDateField = value;
            }
        }

        public bool transferDateSpecified
        {
            get
            {
                return this.transferDateFieldSpecified;
            }
            set
            {
                this.transferDateFieldSpecified = value;
            }
        }

        public String transferProcessDate
        {
            get
            {
                return this.transferProcessDateField;
            }
            set
            {
                this.transferProcessDateField = value;
            }
        }

        public bool transferProcessDateSpecified
        {
            get
            {
                return this.transferProcessDateFieldSpecified;
            }
            set
            {
                this.transferProcessDateFieldSpecified = value;
            }
        }

        public Agency transferredToAgency
        {
            get
            {
                return this.transferredToAgencyField;
            }
            set
            {
                this.transferredToAgencyField = value;
            }
        }

        public string webUrl
        {
            get
            {
                return this.webUrlField;
            }
            set
            {
                this.webUrlField = value;
            }
        }
    }
}
