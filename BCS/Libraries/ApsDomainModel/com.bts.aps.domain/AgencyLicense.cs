using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyLicense
    {

        private Agency agencyField;

        private string commentsField;

        private bool counterSignatureField;

        private long idField;

        private String licenseEffectiveDateField;

        private bool licenseEffectiveDateFieldSpecified;

        private String licenseExpirationDateField;

        private bool licenseExpirationDateFieldSpecified;

        private string licenseNumberField;

        private USState licenseStateField;

        private AgencyLicenseType licenseTypeField;

        private LicenseResidencyType residencyTypeField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public string comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        public bool counterSignature
        {
            get
            {
                return this.counterSignatureField;
            }
            set
            {
                this.counterSignatureField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public String licenseEffectiveDate
        {
            get
            {
                return this.licenseEffectiveDateField;
            }
            set
            {
                this.licenseEffectiveDateField = value;
            }
        }

        /// <remarks/>
        public bool licenseEffectiveDateSpecified
        {
            get
            {
                return this.licenseEffectiveDateFieldSpecified;
            }
            set
            {
                this.licenseEffectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String licenseExpirationDate
        {
            get
            {
                return this.licenseExpirationDateField;
            }
            set
            {
                this.licenseExpirationDateField = value;
            }
        }

        /// <remarks/>
        public bool licenseExpirationDateSpecified
        {
            get
            {
                return this.licenseExpirationDateFieldSpecified;
            }
            set
            {
                this.licenseExpirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string licenseNumber
        {
            get
            {
                return this.licenseNumberField;
            }
            set
            {
                this.licenseNumberField = value;
            }
        }

        /// <remarks/>
        public USState licenseState
        {
            get
            {
                return this.licenseStateField;
            }
            set
            {
                this.licenseStateField = value;
            }
        }

        /// <remarks/>
        public AgencyLicenseType licenseType
        {
            get
            {
                return this.licenseTypeField;
            }
            set
            {
                this.licenseTypeField = value;
            }
        }

        /// <remarks/>
        public LicenseResidencyType residencyType
        {
            get
            {
                return this.residencyTypeField;
            }
            set
            {
                this.residencyTypeField = value;
            }
        }
    }
}
