using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyPersonnelRole
    {

        private AgencyPersonnel agencyPersonnelField;

        private AgencyPersonnelRoleType agencyPersonnelRoleTypeField;

        private long idField;

        /// <remarks/>
        public AgencyPersonnel agencyPersonnel
        {
            get
            {
                return this.agencyPersonnelField;
            }
            set
            {
                this.agencyPersonnelField = value;
            }
        }

        /// <remarks/>
        public AgencyPersonnelRoleType agencyPersonnelRoleType
        {
            get
            {
                return this.agencyPersonnelRoleTypeField;
            }
            set
            {
                this.agencyPersonnelRoleTypeField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }
}
