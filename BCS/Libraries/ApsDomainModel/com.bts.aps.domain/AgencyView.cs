using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyView
    {

        private string agencyCodeField;

        private string agencyHomeCityField;

        private string agencyNameField;

        private string[] agencyNotesField;

        private string agencyStateField;

        private string agencyStatusField;

        private long idField;

        /// <remarks/>
        public string agencyCode
        {
            get
            {
                return this.agencyCodeField;
            }
            set
            {
                this.agencyCodeField = value;
            }
        }

        /// <remarks/>
        public string agencyHomeCity
        {
            get
            {
                return this.agencyHomeCityField;
            }
            set
            {
                this.agencyHomeCityField = value;
            }
        }

        /// <remarks/>
        public string agencyName
        {
            get
            {
                return this.agencyNameField;
            }
            set
            {
                this.agencyNameField = value;
            }
        }

        /// <remarks/>
        public string[] agencyNotes
        {
            get
            {
                return this.agencyNotesField;
            }
            set
            {
                this.agencyNotesField = value;
            }
        }

        /// <remarks/>
        public string agencyState
        {
            get
            {
                return this.agencyStateField;
            }
            set
            {
                this.agencyStateField = value;
            }
        }

        /// <remarks/>
        public string agencyStatus
        {
            get
            {
                return this.agencyStatusField;
            }
            set
            {
                this.agencyStatusField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }
}
