using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class LocationCommunication
    {

        private LocationCommunicationType communicationTypeField;

        private string communicationValueField;

        private long idField;

        private Location locationField;

        private bool primaryIndField;

        /// <remarks/>
        public LocationCommunicationType communicationType
        {
            get
            {
                return this.communicationTypeField;
            }
            set
            {
                this.communicationTypeField = value;
            }
        }

        /// <remarks/>
        public string communicationValue
        {
            get
            {
                return this.communicationValueField;
            }
            set
            {
                this.communicationValueField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public Location location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }

        /// <remarks/>
        public bool primaryInd
        {
            get
            {
                return this.primaryIndField;
            }
            set
            {
                this.primaryIndField = value;
            }
        }
    }
}
