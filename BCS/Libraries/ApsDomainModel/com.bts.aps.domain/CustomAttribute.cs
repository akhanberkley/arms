using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class CustomAttribute
    {

        private string attributeLabelField;

        private string attributeNameField;

        private string attributeTypeField;

        private CustomAttributeChoice[] customAttributeChoicesField;

        private CustomEntity customEntityField;

        private long idField;

        private bool requiredField;

        private long sortOrderField;

        private string widgetTypeField;

        /// <remarks/>
        public string attributeLabel
        {
            get
            {
                return this.attributeLabelField;
            }
            set
            {
                this.attributeLabelField = value;
            }
        }

        /// <remarks/>
        public string attributeName
        {
            get
            {
                return this.attributeNameField;
            }
            set
            {
                this.attributeNameField = value;
            }
        }

        /// <remarks/>
        public string attributeType
        {
            get
            {
                return this.attributeTypeField;
            }
            set
            {
                this.attributeTypeField = value;
            }
        }

        /// <remarks/>
        public CustomAttributeChoice[] customAttributeChoices
        {
            get
            {
                return this.customAttributeChoicesField;
            }
            set
            {
                this.customAttributeChoicesField = value;
            }
        }

        /// <remarks/>
        public CustomEntity customEntity
        {
            get
            {
                return this.customEntityField;
            }
            set
            {
                this.customEntityField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public bool required
        {
            get
            {
                return this.requiredField;
            }
            set
            {
                this.requiredField = value;
            }
        }

        /// <remarks/>
        public long sortOrder
        {
            get
            {
                return this.sortOrderField;
            }
            set
            {
                this.sortOrderField = value;
            }
        }

        /// <remarks/>
        public string widgetType
        {
            get
            {
                return this.widgetTypeField;
            }
            set
            {
                this.widgetTypeField = value;
            }
        }
    }
}
