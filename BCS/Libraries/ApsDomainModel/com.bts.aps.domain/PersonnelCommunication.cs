using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class PersonnelCommunication
    {

        private PersonnelCommunicationType communicationTypeField;

        private string communicationValueField;

        private long idField;

        private Personnel personnelField;

        private bool primaryIndField;

        /// <remarks/>
        public PersonnelCommunicationType communicationType
        {
            get
            {
                return this.communicationTypeField;
            }
            set
            {
                this.communicationTypeField = value;
            }
        }

        /// <remarks/>
        public string communicationValue
        {
            get
            {
                return this.communicationValueField;
            }
            set
            {
                this.communicationValueField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public Personnel personnel
        {
            get
            {
                return this.personnelField;
            }
            set
            {
                this.personnelField = value;
            }
        }

        /// <remarks/>
        public bool primaryInd
        {
            get
            {
                return this.primaryIndField;
            }
            set
            {
                this.primaryIndField = value;
            }
        }
    }
}
