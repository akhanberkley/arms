using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class FinancialAccount
    {

        private string abaRoutingCodeField;

        private string accountNumberField;

        private Agency agencyField;

        private string bankField;

        private String endDateField;

        private bool endDateFieldSpecified;

        private FinancialAccPurposeType financialAccPurposeTypeField;

        private FinancialAccountType financialAccountTypeField;

        private long idField;

        private String startDateField;

        private bool startDateFieldSpecified;

        /// <remarks/>
        public string abaRoutingCode
        {
            get
            {
                return this.abaRoutingCodeField;
            }
            set
            {
                this.abaRoutingCodeField = value;
            }
        }

        /// <remarks/>
        public string accountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public string bank
        {
            get
            {
                return this.bankField;
            }
            set
            {
                this.bankField = value;
            }
        }

        /// <remarks/>
        public String endDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }

        /// <remarks/>
        public bool endDateSpecified
        {
            get
            {
                return this.endDateFieldSpecified;
            }
            set
            {
                this.endDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public FinancialAccPurposeType financialAccPurposeType
        {
            get
            {
                return this.financialAccPurposeTypeField;
            }
            set
            {
                this.financialAccPurposeTypeField = value;
            }
        }

        /// <remarks/>
        public FinancialAccountType financialAccountType
        {
            get
            {
                return this.financialAccountTypeField;
            }
            set
            {
                this.financialAccountTypeField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public String startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public bool startDateSpecified
        {
            get
            {
                return this.startDateFieldSpecified;
            }
            set
            {
                this.startDateFieldSpecified = value;
            }
        }
    }
}
