using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyUWPersonRole
    {

        private AgencyUnderwritingUnit agencyUnderwritingUnitField;

        private CompanyPersonnelRole companyPersonnelRoleField;

        private String effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private String expirationDateField;

        private bool expirationDateFieldSpecified;

        private long idField;

        private bool primaryIndField;

        /// <remarks/>
        public AgencyUnderwritingUnit agencyUnderwritingUnit
        {
            get
            {
                return this.agencyUnderwritingUnitField;
            }
            set
            {
                this.agencyUnderwritingUnitField = value;
            }
        }

        /// <remarks/>
        public CompanyPersonnelRole companyPersonnelRole
        {
            get
            {
                return this.companyPersonnelRoleField;
            }
            set
            {
                this.companyPersonnelRoleField = value;
            }
        }

        /// <remarks/>
        public String effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        public bool effectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        public bool expirationDateSpecified
        {
            get
            {
                return this.expirationDateFieldSpecified;
            }
            set
            {
                this.expirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public bool primaryInd
        {
            get
            {
                return this.primaryIndField;
            }
            set
            {
                this.primaryIndField = value;
            }
        }
    }
}
