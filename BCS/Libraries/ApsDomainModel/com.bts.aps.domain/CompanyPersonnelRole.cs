using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class CompanyPersonnelRole
    {

        private AgencyUWPersonRole[] agencyPersonnelUWRolesField;

        private CompanyPersonnel companyPersonnelField;

        private CompanyPersonnelRoleBranch[] companyPersonnelRoleBranchesField;

        private CompanyPersonnelRoleType companyPersonnelRoleTypeField;

        private String effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private String expirationDateField;

        private bool expirationDateFieldSpecified;

        private long idField;

        private string userGuidField;

        /// <remarks/>
        public AgencyUWPersonRole[] agencyPersonnelUWRoles
        {
            get
            {
                return this.agencyPersonnelUWRolesField;
            }
            set
            {
                this.agencyPersonnelUWRolesField = value;
            }
        }

        /// <remarks/>
        public CompanyPersonnel companyPersonnel
        {
            get
            {
                return this.companyPersonnelField;
            }
            set
            {
                this.companyPersonnelField = value;
            }
        }

        /// <remarks/>
        public CompanyPersonnelRoleBranch[] companyPersonnelRoleBranches
        {
            get
            {
                return this.companyPersonnelRoleBranchesField;
            }
            set
            {
                this.companyPersonnelRoleBranchesField = value;
            }
        }

        /// <remarks/>
        public CompanyPersonnelRoleType companyPersonnelRoleType
        {
            get
            {
                return this.companyPersonnelRoleTypeField;
            }
            set
            {
                this.companyPersonnelRoleTypeField = value;
            }
        }

        /// <remarks/>
        public String effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        public bool effectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        public bool expirationDateSpecified
        {
            get
            {
                return this.expirationDateFieldSpecified;
            }
            set
            {
                this.expirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string userGuid
        {
            get
            {
                return this.userGuidField;
            }
            set
            {
                this.userGuidField = value;
            }
        }
    }
}
