using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class CustomAttributeChoice
    {

        private CustomAttribute customAttributeField;

        private long idField;

        private string labelField;

        private long sortOrderField;

        /// <remarks/>
        public CustomAttribute customAttribute
        {
            get
            {
                return this.customAttributeField;
            }
            set
            {
                this.customAttributeField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string label
        {
            get
            {
                return this.labelField;
            }
            set
            {
                this.labelField = value;
            }
        }

        /// <remarks/>
        public long sortOrder
        {
            get
            {
                return this.sortOrderField;
            }
            set
            {
                this.sortOrderField = value;
            }
        }
    }
}
