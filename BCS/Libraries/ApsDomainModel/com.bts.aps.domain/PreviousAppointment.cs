using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class PreviousAppointment
    {

        private Agency agencyField;

        private String appointmentDateField;

        private bool appointmentDateFieldSpecified;

        private String cancelDateField;

        private bool cancelDateFieldSpecified;

        private AgencyCancelReason cancelReasonField;

        private long idField;

        private String newCancelDateField;

        private bool newCancelDateFieldSpecified;

        private String renewalCancelDateField;

        private bool renewalCancelDateFieldSpecified;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public String appointmentDate
        {
            get
            {
                return this.appointmentDateField;
            }
            set
            {
                this.appointmentDateField = value;
            }
        }

        /// <remarks/>
        public bool appointmentDateSpecified
        {
            get
            {
                return this.appointmentDateFieldSpecified;
            }
            set
            {
                this.appointmentDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String cancelDate
        {
            get
            {
                return this.cancelDateField;
            }
            set
            {
                this.cancelDateField = value;
            }
        }

        /// <remarks/>
        public bool cancelDateSpecified
        {
            get
            {
                return this.cancelDateFieldSpecified;
            }
            set
            {
                this.cancelDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public AgencyCancelReason cancelReason
        {
            get
            {
                return this.cancelReasonField;
            }
            set
            {
                this.cancelReasonField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public String newCancelDate
        {
            get
            {
                return this.newCancelDateField;
            }
            set
            {
                this.newCancelDateField = value;
            }
        }

        /// <remarks/>
        public bool newCancelDateSpecified
        {
            get
            {
                return this.newCancelDateFieldSpecified;
            }
            set
            {
                this.newCancelDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String renewalCancelDate
        {
            get
            {
                return this.renewalCancelDateField;
            }
            set
            {
                this.renewalCancelDateField = value;
            }
        }

        /// <remarks/>
        public bool renewalCancelDateSpecified
        {
            get
            {
                return this.renewalCancelDateFieldSpecified;
            }
            set
            {
                this.renewalCancelDateFieldSpecified = value;
            }
        }
    }
}
