using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class PersonnelNote
    {

        private string authorField;

        private String dateEnteredField;

        private bool dateEnteredFieldSpecified;

        private string detailField;

        private long idField;

        private Personnel personnelField;

        private PersonnelNoteCatType personnelNoteCatTypeField;

        private string summaryField;

        /// <remarks/>
        public string author
        {
            get
            {
                return this.authorField;
            }
            set
            {
                this.authorField = value;
            }
        }

        /// <remarks/>
        public String dateEntered
        {
            get
            {
                return this.dateEnteredField;
            }
            set
            {
                this.dateEnteredField = value;
            }
        }

        /// <remarks/>
        public bool dateEnteredSpecified
        {
            get
            {
                return this.dateEnteredFieldSpecified;
            }
            set
            {
                this.dateEnteredFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string detail
        {
            get
            {
                return this.detailField;
            }
            set
            {
                this.detailField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public Personnel personnel
        {
            get
            {
                return this.personnelField;
            }
            set
            {
                this.personnelField = value;
            }
        }

        /// <remarks/>
        public PersonnelNoteCatType personnelNoteCatType
        {
            get
            {
                return this.personnelNoteCatTypeField;
            }
            set
            {
                this.personnelNoteCatTypeField = value;
            }
        }

        /// <remarks/>
        public string summary
        {
            get
            {
                return this.summaryField;
            }
            set
            {
                this.summaryField = value;
            }
        }
    }
}
