using System;
using System.Collections.Generic;
using System.Text;
using BCS.CWGLoad.Business;
using BCS.CWGLoad.Business.Objects;
using biz = BCS.Biz;
using BizObjects = BCS.Core.Clearance.BizObjects;
using BCS.Core.Clearance;
using XML = BCS.Core.XML;
using Structures = BTS.ClientCore.Wrapper.Structures;
using System.Globalization;
using BTS.LogFramework;

namespace BCS.CWGLoad
{
    class Program
    {
        static ClientProxy.Client clientProxy = new BCS.CWGLoad.ClientProxy.Client();

        static void Main(string[] args)
        {
            #region TESTING Agencies time out
            //for (int i = 0; i < 100; i++)
            //{
            //    string clResult = Common.GetClientProxy().GetClientById("40", "10002825");
            //    Console.Write(clResult);
            //    Console.WriteLine(Environment.NewLine);
            //    Console.WriteLine(i);
            //    Console.WriteLine(Environment.NewLine);
            //}
            //return;
            #endregion
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            Console.WriteLine("Started at {0}", DateTime.Now.ToString());
            
            #region test submissions get
            //Submission[] loadSubmissions = DataManager.GetSubmissionsToLoad();
            //Console.WriteLine(Properties.Settings.Default.SourceDSN);
            //Console.WriteLine(Properties.Settings.Default.TargetDSN);
            //return;
            #endregion
            #region test clients get
            //Client[] loadClients = DataManager.GetClientsToLoad();
            //Console.WriteLine(Properties.Settings.Default.SourceDSN);
            //Console.WriteLine(Properties.Settings.Default.TargetDSN);
            //Console.WriteLine(Properties.Settings.Default.ClientSysDSN);
            //Console.WriteLine("total client count - {0}", loadClients.Length);
            //int totalSubCount = 0;
            //foreach (Client client in loadClients)
            //{
            //    string clientId = string.Empty;

            //    foreach (Submission submission in client.Submissions)
            //    {
            //        if (submission.SendToCobra == true)
            //        {
            //            clientId = submission.Client_id;
            //            break;
            //        }
            //    }
            //    if (!string.IsNullOrEmpty(clientId))
            //        Console.WriteLine(clientId);

            //    //Console.WriteLine("info for cust_loc_rep_code='{0}' AND cust_id='{1}' , Address Count - {2}, Name  Count - {3}, Submission  Count - {4}",
            //    //    client.Submissions[0].CUST_LOC_REP_CODE, client.Submissions[0].CUST_ID, client.ClientAddresses.Length, client.ClientNames.Length, client.Submissions.Length);
            //    totalSubCount += client.Submissions.Length;
            //}
            //Console.WriteLine("total submission count - {0}", totalSubCount);
            //return;
            #endregion


            new Loader().Load();

            stopwatch.Stop();
            Console.WriteLine("Ended at {0}", DateTime.Now.ToString());

            Console.WriteLine("time taken {0} minutes", stopwatch.Elapsed.TotalMinutes);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class NameInBillingSystemComparer : System.Collections.IComparer
    {

        // Calls CaseInsensitiveComparer.Compare with the parameters reversed. bubbles up trues
        int System.Collections.IComparer.Compare(Object x, Object y)
        {
            if (!(x is ClientName) || !(y is ClientName))
                throw new ArgumentException("Invalid comparison");

            ClientName xName = x as ClientName;
            ClientName yName = y as ClientName;

            return (new System.Collections.CaseInsensitiveComparer().Compare(yName.InBillingSystem, xName.InBillingSystem));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AddressInBillingSystemComparer : System.Collections.IComparer
    {
        // Calls CaseInsensitiveComparer.Compare with the parameters reversed. bubbles up trues
        int System.Collections.IComparer.Compare(Object x, Object y)
        {
            if (!(x is ClientAddress) || !(y is ClientAddress))
                throw new ArgumentException("Invalid comparison");

            ClientAddress xAddress = x as ClientAddress;
            ClientAddress yAddress = y as ClientAddress;

            return (new System.Collections.CaseInsensitiveComparer().Compare(yAddress.InBillingSystem, xAddress.InBillingSystem));
        }
    }
}

