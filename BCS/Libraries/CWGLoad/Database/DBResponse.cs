#region Using
using System; 
#endregion

namespace BCS.CWGLoad.Database
{
    #region Public Enumerations
    /// <summary>
    /// Codes used to indicate the status of database response
    /// </summary>
    public enum ResponseCode
    {
        /// <summary>
        /// Unknown Failure
        /// </summary>
        Unknown,
        /// <summary>
        /// Primary key violation while adding a record to the database table
        /// </summary>
        PrimaryKeyViolation,

        /// <summary>
        /// Foreign key violation while deleting or updating a database table
        /// </summary>
        ForeignKeyViolation,

        /// <summary>
        /// No errors identified
        /// </summary>
        Success
    } 
    #endregion

    #region Class DBResponse

    /// <summary>
    /// Class representing the status of database operation
    /// </summary>
    public class DBResponse
    {
        private string _message = string.Empty;
        private ResponseCode _code = ResponseCode.Success;
        private object _value = null;

        /// <summary>
        /// The response code for this Response
        /// </summary>
        public ResponseCode ResponseCode
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        /// <summary>
        /// The Message for this DBResponse
        /// </summary>
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }

        /// <summary>
        /// The Value for this Response
        /// </summary>
        public object Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        /// <summary>
        /// Static method that helps in generating a failre database response
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="code">One of the response codes</param>
        /// <returns></returns>
        public static DBResponse GetFailureResponse(string message, ResponseCode code)
        {
            DBResponse res = new DBResponse();
            res.ResponseCode = code;
            res.Message = message;
            res.Value = null;
            return res;
        }

        /// <summary>
        /// Static method that helps in generating a failre database response with response code unknown failure
        /// </summary>
        /// <param name="message">Message</param>
        /// <returns></returns>
        public static DBResponse GetFailureResponse(string message)
        {
            DBResponse res = new DBResponse();
            res.ResponseCode = ResponseCode.Unknown;
            res.Message = message;
            res.Value = null;
            return res;
        }
        

        /// <summary>
        /// Static method that helps in generating a success response
        /// </summary>
        /// <param name="value">Value that was requested from the database</param>
        /// <returns></returns>
        public static DBResponse GetSuccessResponse(object value)
        {
            DBResponse res = new DBResponse();
            res.ResponseCode = ResponseCode.Success;
            res.Message = string.Empty;
            res.Value = value;
            return res;
        }
    } 
    #endregion
}

