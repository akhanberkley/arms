using System;
using System.Collections.Generic;
using System.Text;
using BCS.CWGLoad.Business.Objects;
using Structures = BTS.ClientCore.Wrapper.Structures;
using BizObjects = BCS.Core.Clearance.BizObjects;
using XML = BCS.Core.XML;
using biz = BCS.Biz;
using BCS.CWGLoad.Business;
using BTS.LogFramework;

namespace BCS.CWGLoad
{
    class Loader
    {
        static ClientProxy.Client clientProxy = new BCS.CWGLoad.ClientProxy.Client();

        /// <summary>
        /// loads all clients --> submissions
        /// </summary>
        public void Load()
        {
            Client[] loadClients = DataManager.GetClientsToLoad();

            new ThreadedLoader().Load(loadClients);

            //if (loadClients.Length > 0)
            //{
            //    // target DB lookups
            //    biz.SubmissionTypeCollection allTypes = DataManager.GetAllTypes(Properties.Settings.Default.CompanyNumber);
            //    biz.SubmissionStatusCollection allStatuses = DataManager.GetAllStatuses(Properties.Settings.Default.CompanyNumber);
            //    biz.SubmissionStatusReasonCollection allStatusReasons = DataManager.GetAllStatusReasons(Properties.Settings.Default.CompanyNumber);
            //    biz.LineOfBusinessCollection allUWs = DataManager.GetAllUnderwritingUnits(Properties.Settings.Default.CompanyNumber);
            //    biz.PersonCollection allPersons = DataManager.GetAllUnderwriters(Properties.Settings.Default.CompanyNumber);
            //    biz.CompanyNumberCodeCollection allCNCs = DataManager.GetAllCompanyNumberCodes(Properties.Settings.Default.CompanyNumber);
            //    biz.AgencyCollection allAgencies = DataManager.GetAllAgencies(Properties.Settings.Default.CompanyNumber);
            //    biz.AgentCollection allAgents = DataManager.GetAllAgents(Properties.Settings.Default.CompanyNumber);
            //    biz.AgentUnspecifiedReasonCollection allAgentReasons = DataManager.GetAllAgentUnspecifiedReasons(Properties.Settings.Default.CompanyNumber);
            //    biz.CompanyNumberLOBGridCollection allLOBGrids = DataManager.GetAllLOBGrids(Properties.Settings.Default.CompanyNumber);
                
            //    foreach (Client client in loadClients)
            //    {
            //        string clientId = string.Empty;

            //        foreach (Submission submission in client.Submissions)
            //        {
            //            if (submission.SendToCobra == true)
            //            {
            //                clientId = submission.Client_id;
            //                break;
            //            }
            //        }
            //        clientId = LoadClient(client, clientId);

            //        LoadClientSubmissions(clientId, client.Submissions, allAgencies, allAgentReasons, allAgents, allCNCs, ref allLOBGrids, allStatuses, allStatusReasons,
            //            allTypes, allUWs, allPersons);
            //    }
            //}
        }

        private static string LoadClient(Client client, string clientId)
        {
            Console.WriteLine("start loading client ");

            StringBuilder names = new StringBuilder();
            StringBuilder addresses = new StringBuilder();

            string taxid = string.Empty;
            // defaulting to FEIN since CWG load column name is FEIN
            string taxidType = "FEIN";

            ClientName[] refinedNames = Common.RefineBillingSysEntries(client.ClientNames);
            foreach (ClientName var in refinedNames)
            {
                Structures.Import.NameInfo aName = new BTS.ClientCore.Wrapper.Structures.Import.NameInfo();
                aName.BusinessName = var.Business_name;
                if (var.InsuredDBA_indicator == "I")
                    aName.ClearanceNameType = "Insured";
                if (var.InsuredDBA_indicator == "D")
                    aName.ClearanceNameType = "DBA";

                // note : we are not dealing with individual clients
                if (Common.IsNotNullOrEmpty(var.FIRSTNAME))
                {
                    aName.BusinessName += string.Format(" {0}", var.FIRSTNAME);
                }
                if (Common.IsNotNullOrEmpty(var.MIDDLE))
                {
                    aName.BusinessName += string.Format(" {0}", var.MIDDLE);
                }
                if (Common.IsNotNullOrEmpty(var.LASTNAME))
                {
                    aName.BusinessName += string.Format(" {0}", var.LASTNAME);
                }
                taxid = var.FEIN;

                string sName = BCS.Core.XML.Serializer.SerializeAsXml(aName);
                names.AppendFormat("{0}+-", sName);
            }

            ClientAddress[] refinedAddresses = Common.RefineBillingSysEntries(client.ClientAddresses);
            foreach (ClientAddress var in refinedAddresses)
            {
                Structures.Import.AddressInfo aAddress = new BTS.ClientCore.Wrapper.Structures.Import.AddressInfo();

                if (var.Mailing_physical_ind == "M")
                    aAddress.ClearanceAddressType = "Mailing";
                if (var.Mailing_physical_ind == "P")
                    aAddress.ClearanceAddressType = "Physical";
                if (var.Mailing_physical_ind == "B")
                    aAddress.ClearanceAddressType = "Both";



                aAddress.HouseNumber = var.ADDR_NUM;
                aAddress.ADDR1 = var.ADDR_STREET;
                aAddress.ADDR2 = var.ADDR_STREET2;
                aAddress.CityName = var.CITY;
                aAddress.County = var.COUNTY_NAME;
                aAddress.StateCode = var.STATE;
                aAddress.Zip = var.ZIP + var.ZIP_4;
                aAddress.Country = "USA";

                string sAddress = BCS.Core.XML.Serializer.SerializeAsXml(aAddress);
                addresses.AppendFormat("{0}+-", sAddress);
            }

            //Commenting out since CC interface has changed since this was developed apparently
            return "";
            //// meaning add a client
            //if (string.IsNullOrEmpty(clientId))
            //{
            //    string clientAddResult = clientProxy.AddClientWithImport(Properties.Settings.Default.CompanyNumber, string.Empty, string.Empty,
            //        DateTime.Now.ToString(), null, "A", "N", taxid, taxidType, 1, names.ToString(), addresses.ToString(), string.Empty, string.Empty, string.Empty,
            //        0, string.Empty, "CWG Load");

            //    Structures.Import.Vector addedClient =
            //        (Structures.Import.Vector)BCS.Core.XML.Deserializer.Deserialize(clientAddResult, typeof(Structures.Import.Vector));

            //    Common.SetLegacyFlags(addedClient.ClientId);

            //    Console.WriteLine("end loading client, id - {0}", addedClient.ClientId);

            //    return addedClient.ClientId;
            //}
            //else // meaning edit a client
            //{
            //    string clientEditResult = clientProxy.ModifyClient(clientId, Properties.Settings.Default.CompanyNumber, string.Empty, string.Empty, string.Empty,
            //       DateTime.Now.ToString(), null, "A", taxid, taxidType, names.ToString(), addresses.ToString(), string.Empty, string.Empty, string.Empty,
            //       0, string.Empty, "CWG Load");

            //    Structures.Import.Vector editedClient =
            //        (Structures.Import.Vector)BCS.Core.XML.Deserializer.Deserialize(clientEditResult, typeof(Structures.Import.Vector));

            //    Common.SetLegacyFlags(editedClient.ClientId);

            //    Console.WriteLine("end loading client, id - {0}", editedClient.ClientId);

            //    return editedClient.ClientId;
            //}
        }

        private static void LoadClientSubmissions(string clientId, Submission[] submissions, BCS.Biz.AgencyCollection allAgencies, BCS.Biz.AgentUnspecifiedReasonCollection allAgentReasons, BCS.Biz.AgentCollection allAgents, BCS.Biz.CompanyNumberCodeCollection allCNCs, ref BCS.Biz.CompanyNumberLOBGridCollection allLOBGrids, BCS.Biz.SubmissionStatusCollection allStatuses, BCS.Biz.SubmissionStatusReasonCollection allStatusReasons, BCS.Biz.SubmissionTypeCollection allTypes, BCS.Biz.LineOfBusinessCollection allUWs, BCS.Biz.PersonCollection allPersons)
        {
            string aclientstr = clientProxy.GetClientById(Properties.Settings.Default.CompanyNumber, clientId);
            BizObjects.ClearanceClient adata = (BizObjects.ClearanceClient)XML.Deserializer.Deserialize(aclientstr, typeof(BizObjects.ClearanceClient), "seq_nbr");

            #region client address ids
            List<string> AddressIdsList = new List<string>();
            foreach (Structures.Info.Address var in adata.ClientCoreClient.Addresses)
            {
                AddressIdsList.Add(var.AddressId);
            }
            string[] AddressIdsArray = new string[AddressIdsList.Count];
            AddressIdsList.CopyTo(AddressIdsArray);
            #endregion

            #region client names, ids, insured and dba
            string insuredNameString = string.Empty;
            string dbaString = string.Empty;
            List<string> NameIdsList = new List<string>();
            foreach (Structures.Info.Name var in adata.ClientCoreClient.Client.Names)
            {
                NameIdsList.Add(var.SequenceNumber);

                if (var.ClearanceNameType == "DBA")
                {
                    // the client's first name dba should be the submission's name dba
                    if (string.IsNullOrEmpty(dbaString))
                        dbaString = var.NameBusiness;
                }
                if (var.ClearanceNameType == "Insured")
                {
                    // the client's first name insured should be the submission's name insured
                    if (string.IsNullOrEmpty(insuredNameString))
                        insuredNameString = var.NameBusiness;
                }
            }
            string[] NameIdsArray = new string[NameIdsList.Count];
            NameIdsList.CopyTo(NameIdsArray);
            #endregion
            foreach (Submission submission in submissions)
            {
                Console.WriteLine("start loading submission LOC_REP_CODE - {0}, CUST_LOC_REP_CODE - {1}, APP_NUM - {2}, CUST_ID - {3}.",
                        submission.LOC_REP_CODE, submission.CUST_LOC_REP_CODE, submission.APP_NUM, submission.CUST_ID);

                biz.Agency agency = allAgencies.FindByAgencyNumber(submission.Agency_Number);
                if (agency == null)
                {
                    LogCentral.Current.LogWarn("Missing agency - " + submission.Agency_Number);
                    continue;
                }
              

                #region Company Number Code Ids
                string[] CodeIdsArray = new string[3];
                CodeIdsArray[0] =
                    allCNCs.FindByCode(submission.Policy_symbol) != null ?
                    allCNCs.FindByCode(submission.Policy_symbol).Id.ToString() : string.Empty;
                CodeIdsArray[1] =
                    allCNCs.FindById(submission.BCS_Service_Office) != null ?
                    allCNCs.FindById(submission.BCS_Service_Office).Id.ToString() : string.Empty;
                string howReceived = submission.BCS_How_Received;
                string[] howReceivedArr = howReceived.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < howReceivedArr.Length; i++)
                {
                    howReceivedArr[i] = howReceivedArr[i].Trim();
                }
                Biz.CompanyNumberCodeCollection filteredCNCs = allCNCs.FilterByCode(howReceivedArr[0]);
                filteredCNCs = filteredCNCs.FilterByDescription(howReceivedArr[1]);
                if (filteredCNCs.Count == 1)
                    CodeIdsArray[2] = filteredCNCs[0].Id.ToString();
                string codeids = string.Join("|", CodeIdsArray);
                #endregion

                #region agent unspecified reasons
                string[] AgentReasonArray = new string[2];
                AgentReasonArray[0] = submission.No_Agent == "1" ? allAgentReasons.FindByReason("Not on File").Id.ToString() : "";
                AgentReasonArray[1] = submission.No_Signature == "1" ? allAgentReasons.FindByReason("No Signature").Id.ToString() : "";
                string AgentReasonsString = string.Join("|", AgentReasonArray);
                #endregion

                #region comments
                string[] commentsArray = new string[submission.Comments.Length];
                for (int i = 0; i < submission.Comments.Length; i++)
                {
                    commentsArray[i] = string.Format("[{0} {1} {2}]\r\n{3}", submission.Comments[i].Comment_Time, submission.Comments[i].Comment_Date, submission.Comments[i].Comment_user, submission.Comments[i].Comments);
                }
                string comments = string.Join("|", commentsArray);
                #endregion

                #region LOB Children
                string[] lOBChildrenArray = new string[submission.AppLOBs.Length];
                for (int i = 0; i < submission.AppLOBs.Length; i++)
                {
                    string[] lOBChildrenArrayElements = new string[5];

                    // id = 0, since this is a new submission
                    lOBChildrenArrayElements[0] = "0";

                    // lob
                    lOBChildrenArrayElements[1] =
                        allLOBGrids.FindByMulitLOBCode(submission.AppLOBs[i].BCS_LOB_CODE) != null ?
                        allLOBGrids.FindByMulitLOBCode(submission.AppLOBs[i].BCS_LOB_CODE).Id.ToString() : "0";

                    // status
                    lOBChildrenArrayElements[2] =
                        allStatuses.FindByStatusCode(submission.AppLOBs[i].BCS_STATUS_CODE) != null ?
                        allStatuses.FindByStatusCode(submission.AppLOBs[i].BCS_STATUS_CODE).Id.ToString() : "0";

                    // status reason
                    lOBChildrenArrayElements[3] = submission.BCS_STATUS_CODE_REASON_ID;

                    // is this deleted
                    lOBChildrenArrayElements[4] = "false";

                    string lobChildElement = string.Join("=", lOBChildrenArrayElements);

                    lOBChildrenArray[i] = lobChildElement;
                }
                string clobChilrenString = string.Join("|", lOBChildrenArray);
                #endregion

                string policyNumber = submission.Policy_NUM;
                // adding prospect will depend upon SendToCobra flag, this is handled in core
                if (submission.SendToCobra)
                    policyNumber += "-" + submission.SendToCobra;

                SubmissionProxy.Submission sproxy = new BCS.CWGLoad.SubmissionProxy.Submission();
                string addResult = sproxy.AddSubmissionByIds(
                    clientId, // client id
                    string.Join("|", AddressIdsArray), // client address ids
                    string.Join("|", NameIdsArray), // client name ids
                    adata.ClientCoreClient.Client.PortfolioId, // client portfolio id
                    "CWG Load", // submission by
                    submission.Submission_Date, // submission date
                    allTypes.FindByTypeName(submission.Submission_Type) != null ? allTypes.FindByTypeName(submission.Submission_Type).Id : 0, // submission type
                    agency.Id, // submission agency
                    allAgencies.FindByAgencyNumber(submission.Agency_Number).Agents.FindByBrokerNo(submission.Agent_Number.ToString()) != null ? allAgencies.FindByAgencyNumber(submission.Agency_Number).Agents.FindByBrokerNo(submission.Agent_Number.ToString()).Id : 0, // submission agent
                    AgentReasonsString, // agent unspecified reasons
                    0, // contact id
                    Properties.Settings.Default.CompanyNumber, // company number
                    allStatuses.FindByStatusCode(submission.BCS_STATUS_CODE) != null ? allStatuses.FindByStatusCode(submission.BCS_STATUS_CODE).Id : 0, // status
                    submission.STATUS_DATE, // status date
                    Convert.ToInt32(submission.BCS_STATUS_CODE_REASON_ID),
                    "", // status reason other
                    policyNumber, // policy number
                    codeids, // company number codes
                    "", // attributes
                    0, // premium
                    submission.APP_EFF_DATE, // effective date
                    "", // expiration date
                    allPersons.FindByInitials(submission.UW_CODE) != null ? allPersons.FindByInitials(submission.UW_CODE).Id : 0, // underwriter id
                    0, // analyst id
                    0, // policy system id
                    comments, // comments
                    allUWs.FindByCode(submission.BCS_UW_UNIT) != null ? allUWs.FindByCode(submission.BCS_UW_UNIT).Id : 0,
                    clobChilrenString, // lob children
                    insuredNameString, // insured name
                    dbaString, // dba
                    "", // submission number
                    0, // other carrier id
                    0 // other carrier premium
                    );

                Console.WriteLine("end loading submission");
            }
        }




        //private static void Load()
        //{
        //    // get all submissions to load
        //    Submission[] submissions = DataManager.GetSubmissionsToLoad();

        //    // only process if we have something to 
        //    if (submissions.Length > 0)
        //    {
        //        // target DB lookups
        //        biz.SubmissionTypeCollection allTypes = DataManager.GetAllTypes(Properties.Settings.Default.CompanyNumber);
        //        biz.SubmissionStatusCollection allStatuses = DataManager.GetAllStatuses(Properties.Settings.Default.CompanyNumber);
        //        biz.SubmissionStatusReasonCollection allStatusReasons = DataManager.GetAllStatusReasons(Properties.Settings.Default.CompanyNumber);
        //        biz.LineOfBusinessCollection allUWs = DataManager.GetAllUnderwritingUnits(Properties.Settings.Default.CompanyNumber);
        //        biz.PersonCollection allPersons = DataManager.GetAllUnderwriters(Properties.Settings.Default.CompanyNumber);
        //        biz.CompanyNumberCodeCollection allCNCs = DataManager.GetAllCompanyNumberCodes(Properties.Settings.Default.CompanyNumber);
        //        biz.AgencyCollection allAgencies = DataManager.GetAllAgencies(Properties.Settings.Default.CompanyNumber);
        //        biz.AgentCollection allAgents = DataManager.GetAllAgents(Properties.Settings.Default.CompanyNumber);
        //        biz.AgentUnspecifiedReasonCollection allAgentReasons = DataManager.GetAllAgentUnspecifiedReasons(Properties.Settings.Default.CompanyNumber);
        //        biz.CompanyNumberLOBGridCollection allLOBGrids = DataManager.GetAllLOBGrids(Properties.Settings.Default.CompanyNumber);

        //        foreach (Submission submission in submissions)
        //        {
        //            LoadSubmission(submission, allAgencies, allAgentReasons, allAgents, allCNCs, ref allLOBGrids, allStatuses, allStatusReasons,
        //                allTypes, allUWs, allPersons);
        //        }
        //    }
        //}

        //private static void LoadSubmission(Submission submission, BCS.Biz.AgencyCollection allAgencies, BCS.Biz.AgentUnspecifiedReasonCollection allAgentReasons, BCS.Biz.AgentCollection allAgents, BCS.Biz.CompanyNumberCodeCollection allCNCs, ref BCS.Biz.CompanyNumberLOBGridCollection allLOBGrids, BCS.Biz.SubmissionStatusCollection allStatuses, BCS.Biz.SubmissionStatusReasonCollection allStatusReasons, BCS.Biz.SubmissionTypeCollection allTypes, BCS.Biz.LineOfBusinessCollection allUWs, BCS.Biz.PersonCollection allPersons)
        //{
        //    Console.WriteLine("start loading submission LOC_REP_CODE - {0}, CUST_LOC_REP_CODE - {1}, APP_NUM - {2}, CUST_ID - {3}.",
        //        submission.LOC_REP_CODE, submission.CUST_LOC_REP_CODE, submission.APP_NUM, submission.CUST_ID);
        //    #region client id
        //    string clientId = string.Empty;
        //    if (Common.IsNotNullOrEmpty(submission.Client_id.Trim()))
        //    {
        //        // client already exists
        //        clientId = submission.Client_id;
        //    }
        //    else
        //    {
        //        // new client
        //        clientId = LoadClient(submission);
        //    }
        //    #endregion

        //    string aclientstr = clientProxy.GetClientById(Properties.Settings.Default.CompanyNumber, clientId);
        //    BizObjects.ClearanceClient adata = (BizObjects.ClearanceClient)XML.Deserializer.Deserialize(aclientstr, typeof(BizObjects.ClearanceClient), "seq_nbr");

        //    #region client address ids
        //    List<string> AddressIdsList = new List<string>();
        //    foreach (Structures.Info.Address var in adata.ClientCoreClient.Addresses)
        //    {
        //        AddressIdsList.Add(var.AddressId);
        //    }
        //    string[] AddressIdsArray = new string[AddressIdsList.Count];
        //    AddressIdsList.CopyTo(AddressIdsArray);
        //    #endregion

        //    #region client names, ids, insured and dba
        //    string insuredNameString = string.Empty;
        //    string dbaString = string.Empty;
        //    List<string> NameIdsList = new List<string>();
        //    foreach (Structures.Info.Name var in adata.ClientCoreClient.Client.Names)
        //    {
        //        NameIdsList.Add(var.SequenceNumber);

        //        if (var.ClearanceNameType == "DBA")
        //        {
        //            // the client's first name dba should be the submission's name dba
        //            if (string.IsNullOrEmpty(dbaString))
        //                dbaString = var.NameBusiness;
        //        }
        //        if (var.ClearanceNameType == "Insured")
        //        {
        //            // the client's first name insured should be the submission's name insured
        //            if (string.IsNullOrEmpty(insuredNameString))
        //                insuredNameString = var.NameBusiness;
        //        }
        //    }
        //    string[] NameIdsArray = new string[NameIdsList.Count];
        //    NameIdsList.CopyTo(NameIdsArray);
        //    #endregion

        //    #region Company Number Code Ids
        //    string[] CodeIdsArray = new string[3];
        //    CodeIdsArray[0] =
        //        allCNCs.FindByCode(submission.Policy_symbol) != null ?
        //        allCNCs.FindByCode(submission.Policy_symbol).Id.ToString() : string.Empty;
        //    CodeIdsArray[1] =
        //        allCNCs.FindById(submission.BCS_Service_Office) != null ?
        //        allCNCs.FindById(submission.BCS_Service_Office).Id.ToString() : string.Empty;
        //    string howReceived = submission.BCS_How_Received;
        //    string[] howReceivedArr = howReceived.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        //    for (int i = 0; i < howReceivedArr.Length; i++)
        //    {
        //        howReceivedArr[i] = howReceivedArr[i].Trim();
        //    }
        //    Biz.CompanyNumberCodeCollection filteredCNCs = allCNCs.FilterByCode(howReceivedArr[0]);
        //    filteredCNCs = filteredCNCs.FilterByDescription(howReceivedArr[1]);
        //    if (filteredCNCs.Count == 1)
        //        CodeIdsArray[2] = filteredCNCs[0].Id.ToString();
        //    string codeids = string.Join("|", CodeIdsArray);
        //    #endregion

        //    #region agent unspecified reasons
        //    string[] AgentReasonArray = new string[2];
        //    AgentReasonArray[0] = submission.No_Agent == "1" ? allAgentReasons.FindByReason("Not on File").Id.ToString() : "";
        //    AgentReasonArray[1] = submission.No_Signature == "1" ? allAgentReasons.FindByReason("No Signature").Id.ToString() : "";
        //    string AgentReasonsString = string.Join("|", AgentReasonArray);
        //    #endregion

        //    #region comments
        //    string[] commentsArray = new string[submission.Comments.Length];
        //    for (int i = 0; i < submission.Comments.Length; i++)
        //    {
        //        commentsArray[i] = string.Format("[{0} {1} {2}]\r\n{3}", submission.Comments[i].Comment_Time, submission.Comments[i].Comment_Date, submission.Comments[i].Comment_user, submission.Comments[i].Comments);
        //    }
        //    string comments = string.Join("|", commentsArray);
        //    #endregion

        //    #region LOB Children
        //    string[] lOBChildrenArray = new string[submission.AppLOBs.Length];
        //    for (int i = 0; i < submission.AppLOBs.Length; i++)
        //    {
        //        string[] lOBChildrenArrayElements = new string[5];

        //        // id = 0, since this is a new submission
        //        lOBChildrenArrayElements[0] = "0";


        //        // lob is required in clearance, so if not already in there add it
        //        if (allLOBGrids.FindByMultiLOBDesc(submission.AppLOBs[i].BCS_LOB_CODE) == null)
        //        {
        //            allLOBGrids = DataManager.AddLOB(submission.AppLOBs[i].LOB_CODE, submission.AppLOBs[i].BCS_LOB_CODE);
        //        }

        //        // lob
        //        lOBChildrenArrayElements[1] =
        //            allLOBGrids.FindByMultiLOBDesc(submission.AppLOBs[i].BCS_LOB_CODE) != null ?
        //            allLOBGrids.FindByMultiLOBDesc(submission.AppLOBs[i].BCS_LOB_CODE).Id.ToString() : "0";

        //        // status
        //        lOBChildrenArrayElements[2] =
        //            allStatuses.FindByStatusCode(submission.AppLOBs[i].BCS_STATUS_CODE) != null ?
        //            allStatuses.FindByStatusCode(submission.AppLOBs[i].BCS_STATUS_CODE).Id.ToString() : "0";

        //        // status reason
        //        lOBChildrenArrayElements[3] = submission.BCS_STATUS_CODE_REASON_ID;

        //        // is this deleted
        //        lOBChildrenArrayElements[4] = "false";

        //        string lobChildElement = string.Join("=", lOBChildrenArrayElements);

        //        lOBChildrenArray[i] = lobChildElement;
        //    }
        //    string clobChilrenString = string.Join("|", lOBChildrenArray);
        //    #endregion



        //    SubmissionProxy.Submission sproxy = new BCS.CWGLoad.SubmissionProxy.Submission();
        //    string addResult = sproxy.AddSubmissionByIds(
        //        clientId, // client id
        //        string.Join("|", AddressIdsArray), // client address ids
        //        string.Join("|", NameIdsArray), // client name ids
        //        adata.ClientCoreClient.Client.PortfolioId, // client portfolio id
        //        "CWG Load", // submission by
        //        DateTime.Now.ToString(), // submission date
        //        allTypes.FindByTypeName(submission.Submission_Type) != null ? allTypes.FindByTypeName(submission.Submission_Type).Id : 0, // submission type
        //        allAgencies.FindByAgencyNumber(submission.Agency_Number) != null ? allAgencies.FindByAgencyNumber(submission.Agency_Number).Id : 0, // submission agency
        //        allAgencies.FindByAgencyNumber(submission.Agency_Number).Agents.FindByBrokerNo(submission.Agent_Number.ToString()) != null ? allAgencies.FindByAgencyNumber(submission.Agency_Number).Agents.FindByBrokerNo(submission.Agent_Number.ToString()).Id : 0, // submission agent
        //        AgentReasonsString, // agent unspecified reasons
        //        0, // contact id
        //        Properties.Settings.Default.CompanyNumber, // company number
        //        allStatuses.FindByStatusCode(submission.BCS_STATUS_CODE) != null ? allStatuses.FindByStatusCode(submission.BCS_STATUS_CODE).Id : 0, // status
        //        submission.STATUS_DATE, // status date
        //        Convert.ToInt32(submission.BCS_STATUS_CODE_REASON_ID),
        //        "", // status reason other
        //        submission.Policy_NUM, // policy number
        //        codeids, // company number codes
        //        "", // attributes
        //        0, // premium
        //        submission.APP_EFF_DATE, // effective date
        //        "", // expiration date
        //        allPersons.FindByInitials(submission.UW_CODE) != null ? allPersons.FindByInitials(submission.UW_CODE).Id : 0, // underwriter id
        //        0, // analyst id
        //        0, // policy system id
        //        comments, // comments
        //        allUWs.FindByCode(submission.BCS_UW_UNIT) != null ? allUWs.FindByCode(submission.BCS_UW_UNIT).Id : 0,
        //        clobChilrenString, // lob children
        //        insuredNameString, // insured name
        //        dbaString, // dba
        //        "", // submission number
        //        0, // other carrier id
        //        0 // other carrier premium
        //        );

        //    Console.WriteLine("end loading submission");
        //}

        //private static string LoadClient(Submission submission)
        //{
        //    Console.WriteLine("start loading client ");

        //    StringBuilder names = new StringBuilder();
        //    StringBuilder addresses = new StringBuilder();

        //    string taxid = string.Empty;
        //    // defaulting to FEIN since CWG load column name is FEIN
        //    string taxidType = "FEIN";

        //    ClientName[] refinedNames = Common.RefineBillingSysEntries(submission.Client.ClientNames);
        //    foreach (ClientName var in refinedNames)
        //    {
        //        Structures.Import.NameInfo aName = new BTS.ClientCore.Wrapper.Structures.Import.NameInfo();
        //        aName.BusinessName = var.Business_name;
        //        if (var.InsuredDBA_indicator == "I")
        //            aName.ClearanceNameType = "Insured";
        //        if (var.InsuredDBA_indicator == "D")
        //            aName.ClearanceNameType = "DBA";

        //        // note : we are not dealing with individual clients
        //        if (Common.IsNotNullOrEmpty(var.FIRSTNAME))
        //        {
        //            aName.BusinessName += string.Format(" {0}", var.FIRSTNAME);
        //        }
        //        if (Common.IsNotNullOrEmpty(var.MIDDLE))
        //        {
        //            aName.BusinessName += string.Format(" {0}", var.MIDDLE);
        //        }
        //        if (Common.IsNotNullOrEmpty(var.LASTNAME))
        //        {
        //            aName.BusinessName += string.Format(" {0}", var.LASTNAME);
        //        }
        //        taxid = var.FEIN;

        //        string sName = BCS.Core.XML.Serializer.SerializeAsXml(aName);
        //        names.AppendFormat("{0}+-", sName);
        //    }

        //    ClientAddress[] refinedAddresses = Common.RefineBillingSysEntries(submission.Client.ClientAddresses);
        //    foreach (ClientAddress var in refinedAddresses)
        //    {
        //        Structures.Import.AddressInfo aAddress = new BTS.ClientCore.Wrapper.Structures.Import.AddressInfo();

        //        if (var.Mailing_physical_ind == "M")
        //            aAddress.ClearanceAddressType = "Mailing";
        //        if (var.Mailing_physical_ind == "P")
        //            aAddress.ClearanceAddressType = "Physical";
        //        if (var.Mailing_physical_ind == "B")
        //            aAddress.ClearanceAddressType = "Both";



        //        aAddress.HouseNumber = var.ADDR_NUM;
        //        aAddress.ADDR1 = var.ADDR_STREET;
        //        aAddress.ADDR2 = var.ADDR_STREET2;
        //        aAddress.CityName = var.CITY;
        //        aAddress.County = var.COUNTY_NAME;
        //        aAddress.StateCode = var.STATE;
        //        aAddress.Zip = var.ZIP + var.ZIP_4;
        //        aAddress.Country = "USA";

        //        string sAddress = BCS.Core.XML.Serializer.SerializeAsXml(aAddress);
        //        addresses.AppendFormat("{0}+-", sAddress);
        //    }

        //    string clientAddResult = clientProxy.AddClientWithImport(Properties.Settings.Default.CompanyNumber, string.Empty, string.Empty,
        //        DateTime.Now.ToString(), null, "A", "N", taxid, taxidType, 1, names.ToString(), addresses.ToString(), string.Empty, string.Empty, string.Empty,
        //        0, string.Empty, "CWG Load");

        //    Structures.Import.Vector addedClient =
        //        (Structures.Import.Vector)BCS.Core.XML.Deserializer.Deserialize(clientAddResult, typeof(Structures.Import.Vector));

        //    Common.SetLegacyFlags(addedClient.ClientId);

        //    Console.WriteLine("end loading client, id - {0}", addedClient.ClientId);

        //    return addedClient.ClientId;
        //}

    }
}
