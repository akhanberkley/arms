namespace BCS.CWGLoad.Business.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// client name
    /// </summary>
    public class ClientName
    {
        /// <summary>
        /// private fields
        /// </summary>
        private int cust_LOC_REP_CODE, cUST_ID;

        /// <summary>
        /// private fields
        /// </summary>
        private string lASTNAME, fIRSTNAME, mIDDLE, business_name, fEIN, insuredDBA_indicator, inBillingSystemIndicator;
        private bool inBillingSystem;

        /// <summary>
        /// Gets whether in billing system.
        /// </summary>
        public bool InBillingSystem
        {
            get { return inBillingSystem; }
        }

        /// <summary>
        /// Gets or sets whether in billing system as string value. Y implies in billing system.
        /// </summary>
        public string InBillingSystemIndicator
        {
            get { return inBillingSystemIndicator; }
            set
            {
                if (value != "Y")
                    inBillingSystem = false;
                else
                    inBillingSystem = true;
                
                inBillingSystemIndicator = value;
            }
        }

        /// <summary>
        /// Gets or sets cust id
        /// </summary>
        public int CUST_ID
        {
            get { return this.cUST_ID; }
            set { this.cUST_ID = value; }
        }

        /// <summary>
        /// Gets or sets cust loc rep code
        /// </summary>
        public int Cust_LOC_REP_CODE
        {
            get { return this.cust_LOC_REP_CODE; }
            set { this.cust_LOC_REP_CODE = value; }
        }

        /// <summary>
        /// Gets or sets insured or dba indicator
        /// </summary>
        public string InsuredDBA_indicator
        {
            get { return this.insuredDBA_indicator; }
            set { this.insuredDBA_indicator = value; }
        }

        /// <summary>
        /// Gets or sets fein
        /// </summary>
        public string FEIN
        {
            get { return this.fEIN; }
            set { this.fEIN = value; }
        }

        /// <summary>
        /// Gets or sets business name
        /// </summary>
        public string Business_name
        {
            get { return this.business_name; }
            set { this.business_name = value; }
        }

        /// <summary>
        /// Gets or sets middle
        /// </summary>
        public string MIDDLE
        {
            get { return this.mIDDLE; }
            set { this.mIDDLE = value; }
        }

        /// <summary>
        /// Gets or sets first name
        /// </summary>
        public string FIRSTNAME
        {
            get { return this.fIRSTNAME; }
            set { this.fIRSTNAME = value; }
        }

        /// <summary>
        /// Gets or sets last name
        /// </summary>
        public string LASTNAME
        {
            get { return this.lASTNAME; }
            set { this.lASTNAME = value; }
        }
    }
}
