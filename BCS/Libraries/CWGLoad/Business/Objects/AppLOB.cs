using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.CWGLoad.Business.Objects
{
    /// <summary>
    /// Class representing lob children of submission
    /// </summary>
    public class AppLOB
    {
        private int lOC_REP_CODE;
        private string aPP_NUM, pOLICY_NUM, lOB_CODE, sTATUS_CODE, sTATUS_REASON;

        /// <summary>
        /// fields for clearance
        /// </summary>
        private string bCS_STATUS_CODE, bCS_STATUS_CODE_REASON, bCS_LOB_CODE, bCS_STATUS_CODE_REASON_ID;

        /// <summary>
        /// 
        /// </summary>
        public string BCS_STATUS_CODE_REASON_ID
        {
            get { return bCS_STATUS_CODE_REASON_ID; }
            set { bCS_STATUS_CODE_REASON_ID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string BCS_STATUS_CODE_REASON
        {
            get { return bCS_STATUS_CODE_REASON; }
            set { bCS_STATUS_CODE_REASON = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string BCS_STATUS_CODE
        {
            get { return bCS_STATUS_CODE; }
            set { bCS_STATUS_CODE = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string BCS_LOB_CODE
        {
            get { return bCS_LOB_CODE; }
            set { bCS_LOB_CODE = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int LOC_REP_CODE
        {
            get { return lOC_REP_CODE; }
            set { lOC_REP_CODE = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string STATUS_CODE
        {
            get { return sTATUS_CODE; }
            set { sTATUS_CODE = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string STATUS_REASON
        {
            get { return sTATUS_REASON; }
            set { sTATUS_REASON = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string LOB_CODE
        {
            get { return lOB_CODE; }
            set { lOB_CODE = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string POLICY_NUM
        {
            get { return pOLICY_NUM; }
            set { pOLICY_NUM = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string APP_NUM
        {
            get { return aPP_NUM; }
            set { aPP_NUM = value; }
        }
    }
}
