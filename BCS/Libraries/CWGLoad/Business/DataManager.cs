namespace BCS.CWGLoad.Business
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using BCS.CWGLoad.Business.Objects;
    using biz = BCS.Biz;

    /// <summary>
    /// Class DataManager
    /// </summary>
    public class DataManager
    {

        static readonly string key1 = " CUST_LOC_REP_CODE = {0} AND CUST_ID = '{1}' ";
        static readonly string key2 = " LOC_REP_CODE = {0} AND APP_NUM = '{1}' ";

        #region Submission Methods
        /// <summary>
        /// gets the submissions to load from the source DB and any associations like submissions, names etc.
        /// </summary>
        /// <remarks>This is not used anymore since we start from clients now.</remarks>
        /// <returns>an array of submissions</returns>
        internal static Submission[] GetSubmissionsToLoad()
        {
            Database.DBAccess.ConnectionString = Common.GetSourceDSN();

            // get data
            DataTable submissionTable = Database.DBAccess.GetDataTable("conv_submission");
            DataTable commentsTable = Database.DBAccess.GetDataTable("CONV_COMMENTS");
            DataTable appLOBsTable = Database.DBAccess.GetDataTable("conv_applob");
            DataTable clientNamesTable = Database.DBAccess.GetDataTable("conv_Names");
            DataTable clientAddressesTable = Database.DBAccess.GetDataTable("conv_Addresses");
            DataTable lookupStatuses = Database.DBAccess.GetDataTable("conv_Statuses");
            DataTable lookupStatusReasons = Database.DBAccess.GetDataTable("conv_StatusReason");
            DataTable lookupUWUnits = Database.DBAccess.GetDataTable("conv_UWUnit");
            DataTable lookupLOBCodes = Database.DBAccess.GetDataTable("conv_LOBCodes");
            DataTable lookupPolicySymbols = Database.DBAccess.GetDataTable("conv_PolicySymbol");
            DataTable lookupServiceOffices = Database.DBAccess.GetDataTable("conv_ServiceOffice");


            List<Submission> lstSubmissions = new List<Submission>();
            foreach (DataRow dataRow in submissionTable.Rows)
            {
                #region Submission
                Submission submissionVar = new Submission();
                submissionVar.Agency_Number = Common.GetNullSafeString(dataRow["Agency_Number"]);
                submissionVar.Agent_Number = dataRow["Agent_Number"] == DBNull.Value ? 0 : (int)dataRow["Agent_Number"];
                submissionVar.APP_EFF_DATE = Common.GetNullSafeString(dataRow["APP_EFF_DATE"]);
                submissionVar.APP_NUM = Common.GetNullSafeString(dataRow["APP_NUM"]);
                submissionVar.Client_id = Common.GetNullSafeString(dataRow["Client_id"]);
                submissionVar.CUST_ID = dataRow["CUST_ID"] == DBNull.Value ? 0 : (int)dataRow["CUST_ID"];
                submissionVar.CUST_LOC_REP_CODE = dataRow["CUST_LOC_REP_CODE"] == DBNull.Value ? 0 : (int)dataRow["CUST_LOC_REP_CODE"];
                submissionVar.LOC_REP_CODE = dataRow["LOC_REP_CODE"] == DBNull.Value ? 0 : (int)dataRow["LOC_REP_CODE"];
                submissionVar.No_Agent = Common.GetNullSafeString(dataRow["No_Agent"]);
                submissionVar.No_Signature = Common.GetNullSafeString(dataRow["No_Signature"]);
                submissionVar.Policy_NUM = Common.GetNullSafeString(dataRow["Policy_NUM"]);
                submissionVar.Policy_symbol = Common.GetNullSafeString(dataRow["Policy_symbol"]);
                submissionVar.Service_Office = Common.GetNullSafeString(dataRow["Service_Office"]);
                submissionVar.STATUS_CODE = Common.GetNullSafeString(dataRow["STATUS_CODE"]);                
                submissionVar.STATUS_DATE = Common.GetNullSafeString(dataRow["STATUS_DATE"]);
                submissionVar.STATUS_REASON = Common.GetNullSafeString(dataRow["STATUS_REASON"]);
                submissionVar.Submission_Type = Common.GetNullSafeString(dataRow["Submission_Type"]);
                submissionVar.UW_CODE = Common.GetNullSafeString(dataRow["UW_CODE"]);
                submissionVar.UW_UNIT = Common.GetNullSafeString(dataRow["UW_UNIT"]);
                submissionVar.BCS_How_Received = Common.GetNullSafeString(dataRow["HowReceived"]);

                try
                {
                    submissionVar.BCS_STATUS_CODE = lookupStatuses.Select(string.Format(" StatusCode = '{0}' ", submissionVar.STATUS_CODE))[0]["StatusDescription"].ToString();
                }
                catch (Exception)
                {
                    submissionVar.BCS_STATUS_CODE = string.Empty;
                }
                try
                {
                    submissionVar.BCS_STATUS_CODE_REASON = lookupStatusReasons.Select(string.Format(" StatusReasonCode = '{0}' ", submissionVar.STATUS_REASON))[0]["StatusReasonDescription"].ToString();
                }
                catch (Exception)
                {
                    submissionVar.BCS_STATUS_CODE_REASON = string.Empty;
                }
                try
                {
                    submissionVar.BCS_STATUS_CODE_REASON_ID = lookupStatusReasons.Select(string.Format(" StatusReasonCode = '{0}' ", submissionVar.STATUS_REASON))[0]["BCSStatusReasonID"].ToString();
                }
                catch (Exception)
                {
                    submissionVar.BCS_STATUS_CODE_REASON_ID = "0";
                }
                try
                {
                    submissionVar.BCS_UW_UNIT = lookupUWUnits.Select(string.Format(" UWUnitCode = '{0}' ", submissionVar.UW_UNIT))[0]["UWUnitCodeDescription"].ToString();
                }
                catch (Exception)
                {
                    submissionVar.BCS_UW_UNIT = string.Empty;
                }
                try
                {
                    submissionVar.BCS_Policy_symbol = lookupPolicySymbols.Select(string.Format(" PolicySymbol = '{0}' ", submissionVar.Policy_symbol))[0]["PolicySymbolDescription"].ToString();
                }
                catch (Exception)
                {
                    submissionVar.BCS_Policy_symbol = string.Empty;
                }
                try
                {
                    submissionVar.BCS_Service_Office = lookupServiceOffices.Select(string.Format(" ServiceOfficeCode = '{0}' ", submissionVar.Service_Office))[0]["BCSCompanyNumberCodeID"].ToString();
                }
                catch (Exception)
                {
                    submissionVar.BCS_Service_Office = string.Empty;
                }

                #endregion

                #region Submission Comments
                DataRow[] rowsComments = commentsTable.Select(string.Format(" LOC_REP_CODE = {0} AND APP_NUM = '{1}' ", submissionVar.LOC_REP_CODE, submissionVar.APP_NUM));
                List<Comment> lstComments = new List<Comment>();
                foreach (DataRow rowComment in rowsComments)
                {
                    Comment commentVar = new Comment();
                    commentVar.APP_NUM = Common.GetNullSafeString(rowComment["APP_NUM"]);
                    commentVar.Comment_Date = Common.GetNullSafeString(rowComment["Comment_Date"]);
                    commentVar.Comment_Time = Common.GetNullSafeString(rowComment["Comment_Time"]);
                    commentVar.Comment_user = Common.GetNullSafeString(rowComment["Comment_user"]);
                    commentVar.Comments = Common.GetNullSafeString(rowComment["Comments"]);
                    commentVar.LOC_REP_CODE = rowComment["LOC_REP_CODE"] == DBNull.Value ? 0 : (int)rowComment["LOC_REP_CODE"];
                    commentVar.POLICY_Num = Common.GetNullSafeString(rowComment["POLICY_Num"]);

                    lstComments.Add(commentVar);
                }

                submissionVar.Comments = new Comment[lstComments.Count];
                lstComments.CopyTo(submissionVar.Comments); 
                #endregion

                #region Submission LOB Children
                DataRow[] rowsAppLOBs = appLOBsTable.Select(string.Format(" LOC_REP_CODE = {0} AND APP_NUM = '{1}' ", submissionVar.LOC_REP_CODE, submissionVar.APP_NUM));
                List<AppLOB> lstAppLOBs = new List<AppLOB>();
                foreach (DataRow rowAppLOB in rowsAppLOBs)
                {
                    AppLOB appLOBVar = new AppLOB();
                    appLOBVar.APP_NUM = Common.GetNullSafeString(rowAppLOB["APP_NUM"]);
                    appLOBVar.LOB_CODE = Common.GetNullSafeString(rowAppLOB["LOB_CODE"]);
                    appLOBVar.LOC_REP_CODE = rowAppLOB["LOC_REP_CODE"] == DBNull.Value ? 0 : (int)rowAppLOB["LOC_REP_CODE"];
                    appLOBVar.POLICY_NUM = Common.GetNullSafeString(rowAppLOB["POLICY_NUM"]);
                    appLOBVar.STATUS_CODE = Common.GetNullSafeString(rowAppLOB["STATUS_CODE"]);
                    appLOBVar.STATUS_REASON = Common.GetNullSafeString(rowAppLOB["STATUS_REASON"]);

                    try
                    {
                        appLOBVar.BCS_STATUS_CODE = lookupStatuses.Select(string.Format(" StatusCode = '{0}' ", submissionVar.STATUS_CODE))[0]["StatusDescription"].ToString();
                    }
                    catch (Exception)
                    {
                        appLOBVar.BCS_STATUS_CODE = string.Empty;
                    }
                    try
                    {
                        appLOBVar.BCS_STATUS_CODE_REASON = lookupStatuses.Select(string.Format(" StatusReasonCode = '{0}' ", submissionVar.STATUS_REASON))[0]["StatusReasonDescription"].ToString();
                    }
                    catch (Exception)
                    {
                        appLOBVar.BCS_STATUS_CODE_REASON = string.Empty;
                    }
                    try
                    {
                        appLOBVar.BCS_STATUS_CODE_REASON_ID = lookupStatusReasons.Select(string.Format(" StatusReasonCode = '{0}' ", submissionVar.STATUS_REASON))[0]["BCSStatusReasonID"].ToString();
                    }
                    catch (Exception)
                    {
                        appLOBVar.BCS_STATUS_CODE_REASON_ID = "0";
                    }
                    try
                    {
                        appLOBVar.BCS_LOB_CODE = lookupLOBCodes.Select(string.Format(" LOBCode = '{0}' ", appLOBVar.LOB_CODE))[0]["LOBDescription"].ToString();
                    }
                    catch (Exception)
                    {
                        appLOBVar.BCS_LOB_CODE = string.Empty;
                    }

                    lstAppLOBs.Add(appLOBVar);
                }

                submissionVar.AppLOBs = new AppLOB[lstAppLOBs.Count];
                lstAppLOBs.CopyTo(submissionVar.AppLOBs); 
                #endregion

                #region Submission Client

                #region Names
                DataRow[] rowsClientNames = clientNamesTable.Select(string.Format(" CUST_LOC_REP_CODE = {0} AND CUST_ID = '{1}' ", submissionVar.CUST_LOC_REP_CODE, submissionVar.CUST_ID));
                List<ClientName> lstClientNames = new List<ClientName>();
                foreach (DataRow rowClientName in rowsClientNames)
                {
                    ClientName clientNameVar = new ClientName();
                    clientNameVar.Business_name = Common.GetNullSafeString(rowClientName["Business_name"]);
                    clientNameVar.CUST_ID = rowClientName["CUST_ID"] == DBNull.Value ? 0 : (int)rowClientName["CUST_ID"];
                    clientNameVar.Cust_LOC_REP_CODE = rowClientName["CUST_ID"] == DBNull.Value ? 0 : (int)rowClientName["Cust_LOC_REP_CODE"];
                    clientNameVar.FEIN = Common.GetNullSafeString(rowClientName["FEIN"]);
                    clientNameVar.FIRSTNAME = Common.GetNullSafeString(rowClientName["FIRSTNAME"]);
                    clientNameVar.InsuredDBA_indicator = string.IsNullOrEmpty(Common.GetNullSafeString(rowClientName["InsuredDBA_indicator"])) ?
                        "I" : Common.GetNullSafeString(rowClientName["InsuredDBA_indicator"]);
                    clientNameVar.LASTNAME = Common.GetNullSafeString(rowClientName["LASTNAME"]);
                    clientNameVar.MIDDLE = Common.GetNullSafeString(rowClientName["MIDDLE"]);
                    clientNameVar.InBillingSystemIndicator = Common.GetNullSafeString(rowClientName["InBillingSystem"]);

                    lstClientNames.Add(clientNameVar);
                }
                #endregion

                #region Addresses
                DataRow[] rowsClientAddresses = clientAddressesTable.Select(string.Format(" CUST_LOC_REP_CODE = {0} AND CUST_ID = '{1}' ", submissionVar.CUST_LOC_REP_CODE, submissionVar.CUST_ID));
                List<ClientAddress> lstClientAddresses = new List<ClientAddress>();
                foreach (DataRow rowClientAddress in rowsClientAddresses)
                {
                    ClientAddress clientAddressVar = new ClientAddress();
                    clientAddressVar.ADDR_NUM = Common.GetNullSafeString(rowClientAddress["ADDR_NUM"]);
                    clientAddressVar.ADDR_STREET = Common.GetNullSafeString(rowClientAddress["ADDR_STREET"]);
                    clientAddressVar.ADDR_STREET2 = Common.GetNullSafeString(rowClientAddress["ADDR_STREET2"]);
                    clientAddressVar.CITY = Common.GetNullSafeString(rowClientAddress["CITY"]);
                    clientAddressVar.COUNTY_NAME = Common.GetNullSafeString(rowClientAddress["COUNTY_NAME"]);
                    clientAddressVar.Cust_id = rowClientAddress["Cust_id"] == DBNull.Value ? 0 : (int)rowClientAddress["Cust_id"];
                    clientAddressVar.Cust_loc_rep_code = rowClientAddress["Cust_loc_rep_code"] == DBNull.Value ? 0 : (int)rowClientAddress["Cust_loc_rep_code"];
                    clientAddressVar.Mailing_physical_ind = Common.GetNullSafeString(rowClientAddress["Mailing_physical_ind"]);
                    clientAddressVar.STATE = Common.GetNullSafeString(rowClientAddress["STATE"]);
                    clientAddressVar.ZIP = Common.GetNullSafeString(rowClientAddress["ZIP"]);
                    clientAddressVar.ZIP_4 = Common.GetNullSafeString(rowClientAddress["ZIP_4"]);
                    clientAddressVar.InBillingSystemIndicator = Common.GetNullSafeString(rowClientAddress["InBillingSystem"]);

                    lstClientAddresses.Add(clientAddressVar);
                }
                #endregion 

                submissionVar.Client = new Client();
                submissionVar.Client.ClientAddresses = new ClientAddress[lstClientAddresses.Count];
                lstClientAddresses.CopyTo(submissionVar.Client.ClientAddresses);
                submissionVar.Client.ClientNames = new ClientName[lstClientNames.Count];
                lstClientNames.CopyTo(submissionVar.Client.ClientNames);

                #endregion

                lstSubmissions.Add(submissionVar);
            }

            Submission[] returnSubmissions = new Submission[lstSubmissions.Count];
            lstSubmissions.CopyTo(returnSubmissions);

            return returnSubmissions;
        } 
        #endregion

        #region Lookup Methods
        /// <summary>
        /// gets all statuses for the company number from the target DB.
        /// </summary>
        /// <param name="companyNumber">company number</param>
        /// <returns>a collection of statuses</returns>
        internal static biz.SubmissionStatusCollection GetAllStatuses(string companyNumber)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(
                biz.JoinPath.SubmissionStatus.CompanyNumberSubmissionStatus.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber);
            return dm.GetSubmissionStatusCollection();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        internal static BCS.Biz.SubmissionTypeCollection GetAllTypes(string companyNumber)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(
                biz.JoinPath.SubmissionType.CompanyNumberSubmissionType.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber);
            return dm.GetSubmissionTypeCollection();
        }

        /// <summary>
        /// gets all status reasons for the company number from the target DB.
        /// </summary>
        /// <param name="companyNumber">company number</param>
        /// <returns>a collection of status reasons</returns>
        internal static biz.SubmissionStatusReasonCollection GetAllStatusReasons(string companyNumber)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(
                biz.JoinPath.SubmissionStatusReason.SubmissionStatusSubmissionStatusReason.SubmissionStatus.CompanyNumberSubmissionStatus.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber);
            return dm.GetSubmissionStatusReasonCollection();
        }

        /// <summary>
        /// gets all underwriting units for the company number from the target DB
        /// </summary>
        /// <param name="companyNumber">company number</param>
        /// <returns>a collection of underwriting units</returns>
        internal static biz.LineOfBusinessCollection GetAllUnderwritingUnits(string companyNumber)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(
                biz.JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber);
            return dm.GetLineOfBusinessCollection();
        }

        /// <summary>
        /// gets all underwriters for the company number from the target DB
        /// </summary>
        /// <param name="companyNumber">company number</param>
        /// <returns>a collection of persons</returns>
        internal static biz.PersonCollection GetAllUnderwriters(string companyNumber)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(
                biz.JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber);
            return dm.GetPersonCollection();
        }

        /// <summary>
        /// gets all company number codes for the company number from the target DB.
        /// </summary>
        /// <param name="companyNumber">company number</param>
        /// <returns>a collection of company number codes</returns>
        internal static biz.CompanyNumberCodeCollection GetAllCompanyNumberCodes(string companyNumber)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(
                biz.JoinPath.CompanyNumberCode.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber);
            return dm.GetCompanyNumberCodeCollection();
        }

        internal static BCS.Biz.AgencyCollection GetAllAgencies(string companyNumber)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(
                biz.JoinPath.Agency.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber);
            return dm.GetAgencyCollection();
        }

        internal static BCS.Biz.AgentCollection GetAllAgents(string companyNumber)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(biz.JoinPath.Agent.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            return dm.GetAgentCollection();
        }

        internal static BCS.Biz.AgentUnspecifiedReasonCollection GetAllAgentUnspecifiedReasons(string companyNumber)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(
                biz.JoinPath.AgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReason.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber);
            return dm.GetAgentUnspecifiedReasonCollection();
        }

        internal static BCS.Biz.CompanyNumberLOBGridCollection GetAllLOBGrids(string companyNumber)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(
                biz.JoinPath.CompanyNumberLOBGrid.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            return dm.GetCompanyNumberLOBGridCollection();
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lobCode"></param>
        /// <param name="lobDescription"></param>
        /// <returns></returns>
        public static biz.CompanyNumberLOBGridCollection AddLOB(string lobCode, string lobDescription)
        {
            biz.DataManager dm = GetDataManager();
            dm.QueryCriteria.And(biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, Properties.Settings.Default.CompanyNumber);
            biz.CompanyNumber cnumber = dm.GetCompanyNumber();
            biz.CompanyNumberLOBGrid newLOB= cnumber.NewCompanyNumberLOBGrid();
            newLOB.MulitLOBCode = lobCode;
            newLOB.MultiLOBDesc = lobDescription;

            dm.CommitAll();

            return GetAllLOBGrids(Properties.Settings.Default.CompanyNumber);

        }

        /// <summary>
        /// gets the DataManager Instance
        /// </summary>
        /// <returns>a BCS.Biz.DataManager object</returns>
        private static BCS.Biz.DataManager GetDataManager()
        {
            biz.DataManager dm = new biz.DataManager(Common.GetTargetDSN());
            dm.CommandTimeout = 200;
            return dm;
        }

        /// <summary>
        /// gets the clients and any associations like submissions, names etc.
        /// </summary>
        internal static Client[] GetClientsToLoad()
        {
            Database.DBAccess.ConnectionString = Common.GetSourceDSN();
                       
            #region get data

            // get distinct clients
            DataRowCollection distinctClientRows = Database.DBAccess.GetDistinctRows("conv_submission", "CUST_LOC_REP_CODE", "CUST_ID");            

            DataTable submissionTable = Database.DBAccess.GetDataTable("conv_submission");
            DataTable commentsTable = Database.DBAccess.GetDataTable("CONV_COMMENTS");
            DataTable appLOBsTable = Database.DBAccess.GetDataTable("conv_applob");
            DataTable clientNamesTable = Database.DBAccess.GetDataTable("conv_Names");
            DataTable clientAddressesTable = Database.DBAccess.GetDataTable("conv_Addresses");
            DataTable lookupStatuses = Database.DBAccess.GetDataTable("conv_Statuses");
            DataTable lookupStatusReasons = Database.DBAccess.GetDataTable("conv_StatusReason");
            DataTable lookupUWUnits = Database.DBAccess.GetDataTable("conv_UWUnit");
            DataTable lookupLOBCodes = Database.DBAccess.GetDataTable("conv_LOBCodes");
            DataTable lookupPolicySymbols = Database.DBAccess.GetDataTable("conv_PolicySymbol");
            DataTable lookupServiceOffices = Database.DBAccess.GetDataTable("conv_ServiceOffice"); 
            #endregion

            List<Client> clientsList = new List<Client>();
            foreach (DataRow dataRow in distinctClientRows)
            {
                Client clientVar = new Client();

                #region Client

                #region Names
                DataRow[] clientNameRows = clientNamesTable.Select(string.Format(key1, dataRow["CUST_LOC_REP_CODE"], dataRow["CUST_ID"]));
                List<ClientName> lstClientNames = new List<ClientName>();
                foreach (DataRow rowClientName in clientNameRows)
                {
                    ClientName clientNameVar = new ClientName();
                    clientNameVar.Business_name = Common.GetNullSafeString(rowClientName["Business_name"]);
                    clientNameVar.CUST_ID = rowClientName["CUST_ID"] == DBNull.Value ? 0 : (int)rowClientName["CUST_ID"];
                    clientNameVar.Cust_LOC_REP_CODE = rowClientName["CUST_ID"] == DBNull.Value ? 0 : (int)rowClientName["Cust_LOC_REP_CODE"];
                    clientNameVar.FEIN = Common.GetNullSafeString(rowClientName["FEIN"]);
                    clientNameVar.FIRSTNAME = Common.GetNullSafeString(rowClientName["FIRSTNAME"]);
                    clientNameVar.InsuredDBA_indicator = string.IsNullOrEmpty(Common.GetNullSafeString(rowClientName["InsuredDBA_indicator"])) ?
                        "I" : Common.GetNullSafeString(rowClientName["InsuredDBA_indicator"]);
                    clientNameVar.LASTNAME = Common.GetNullSafeString(rowClientName["LASTNAME"]);
                    clientNameVar.MIDDLE = Common.GetNullSafeString(rowClientName["MIDDLE"]);
                    clientNameVar.InBillingSystemIndicator = Common.GetNullSafeString(rowClientName["InBillingSystem"]);

                    lstClientNames.Add(clientNameVar);
                }
                #endregion

                #region Addresses
                DataRow[] clientAddressRows = clientAddressesTable.Select(string.Format(key1, dataRow["CUST_LOC_REP_CODE"], dataRow["CUST_ID"]));
                List<ClientAddress> lstClientAddresses = new List<ClientAddress>();
                foreach (DataRow rowClientAddress in clientAddressRows)
                {
                    ClientAddress clientAddressVar = new ClientAddress();
                    clientAddressVar.ADDR_NUM = Common.GetNullSafeString(rowClientAddress["ADDR_NUM"]);
                    clientAddressVar.ADDR_STREET = Common.GetNullSafeString(rowClientAddress["ADDR_STREET"]);
                    clientAddressVar.ADDR_STREET2 = Common.GetNullSafeString(rowClientAddress["ADDR_STREET2"]);
                    clientAddressVar.CITY = Common.GetNullSafeString(rowClientAddress["CITY"]);
                    clientAddressVar.COUNTY_NAME = Common.GetNullSafeString(rowClientAddress["COUNTY_NAME"]);
                    clientAddressVar.Cust_id = rowClientAddress["Cust_id"] == DBNull.Value ? 0 : (int)rowClientAddress["Cust_id"];
                    clientAddressVar.Cust_loc_rep_code = rowClientAddress["Cust_loc_rep_code"] == DBNull.Value ? 0 : (int)rowClientAddress["Cust_loc_rep_code"];
                    clientAddressVar.Mailing_physical_ind = Common.GetNullSafeString(rowClientAddress["Mailing_physical_ind"]);
                    clientAddressVar.STATE = Common.GetNullSafeString(rowClientAddress["STATE"]);
                    clientAddressVar.ZIP = Common.GetNullSafeString(rowClientAddress["ZIP"]);
                    clientAddressVar.ZIP_4 = Common.GetNullSafeString(rowClientAddress["ZIP_4"]);
                    clientAddressVar.InBillingSystemIndicator = Common.GetNullSafeString(rowClientAddress["InBillingSystem"]);

                    lstClientAddresses.Add(clientAddressVar);
                }
                #endregion

                clientVar.ClientAddresses = lstClientAddresses.ToArray();
                clientVar.ClientNames = lstClientNames.ToArray();

                #endregion

                #region Client Submissions

                List<Submission> clientSubmissionList = new List<Submission>();

                DataRow[] submissionRows = submissionTable.Select(string.Format(key1, dataRow["CUST_LOC_REP_CODE"], dataRow["CUST_ID"]));
                foreach (DataRow submissionRow in submissionRows)
                {
                    #region Submission
                    Submission submissionVar = new Submission();
                    submissionVar.Agency_Number = Common.GetNullSafeString(submissionRow["Agency_Number"]);
                    submissionVar.Agent_Number = submissionRow["Agent_Number"] == DBNull.Value ? 0 : (int)submissionRow["Agent_Number"];
                    submissionVar.APP_EFF_DATE = Common.GetNullSafeString(submissionRow["APP_EFF_DATE"]);
                    submissionVar.APP_NUM = Common.GetNullSafeString(submissionRow["APP_NUM"]);
                    submissionVar.Client_id = Common.GetNullSafeString(submissionRow["Client_id"]);
                    submissionVar.CUST_ID = submissionRow["CUST_ID"] == DBNull.Value ? 0 : (int)submissionRow["CUST_ID"];
                    submissionVar.CUST_LOC_REP_CODE = submissionRow["CUST_LOC_REP_CODE"] == DBNull.Value ? 0 : (int)submissionRow["CUST_LOC_REP_CODE"];
                    submissionVar.LOC_REP_CODE = submissionRow["LOC_REP_CODE"] == DBNull.Value ? 0 : (int)submissionRow["LOC_REP_CODE"];
                    submissionVar.No_Agent = Common.GetNullSafeString(submissionRow["No_Agent"]);
                    submissionVar.No_Signature = Common.GetNullSafeString(submissionRow["No_Signature"]);
                    submissionVar.Policy_NUM = Common.GetNullSafeString(submissionRow["Policy_NUM"]);
                    submissionVar.Policy_symbol = Common.GetNullSafeString(submissionRow["Policy_symbol"]);
                    submissionVar.Service_Office = Common.GetNullSafeString(submissionRow["Service_Office"]);
                    submissionVar.STATUS_CODE = Common.GetNullSafeString(submissionRow["STATUS_CODE"]);
                    submissionVar.STATUS_DATE = Common.GetNullSafeString(submissionRow["STATUS_DATE"]);
                    submissionVar.STATUS_REASON = Common.GetNullSafeString(submissionRow["STATUS_REASON"]);
                    submissionVar.Submission_Type = Common.GetNullSafeString(submissionRow["Submission_Type"]);
                    submissionVar.UW_CODE = Common.GetNullSafeString(submissionRow["UW_CODE"]);
                    submissionVar.UW_UNIT = Common.GetNullSafeString(submissionRow["UW_UNIT"]);
                    submissionVar.BCS_How_Received = Common.GetNullSafeString(submissionRow["HowReceived"]);
                    submissionVar.Submission_Date = Common.GetNullSafeDateTimeString(submissionRow["Submission_Date"]);
                    submissionVar.SendToCobra = Common.GetNullSafeBool(submissionRow["SendToCobra"]);                    

                    try
                    {
                        submissionVar.BCS_STATUS_CODE = lookupStatuses.Select(string.Format(" StatusCode = '{0}' ", submissionVar.STATUS_CODE))[0]["StatusDescription"].ToString();
                    }
                    catch (Exception)
                    {
                        submissionVar.BCS_STATUS_CODE = string.Empty;
                    }
                    try
                    {
                        submissionVar.BCS_STATUS_CODE_REASON = lookupStatusReasons.Select(string.Format(" StatusReasonCode = '{0}' ", submissionVar.STATUS_REASON))[0]["StatusReasonDescription"].ToString();
                    }
                    catch (Exception)
                    {
                        submissionVar.BCS_STATUS_CODE_REASON = string.Empty;
                    }
                    try
                    {
                        submissionVar.BCS_STATUS_CODE_REASON_ID = lookupStatusReasons.Select(string.Format(" StatusReasonCode = '{0}' ", submissionVar.STATUS_REASON))[0]["BCSStatusReasonID"].ToString();
                    }
                    catch (Exception)
                    {
                        submissionVar.BCS_STATUS_CODE_REASON_ID = "0";
                    }
                    try
                    {
                        submissionVar.BCS_UW_UNIT = lookupUWUnits.Select(string.Format(" UWUnitCode = '{0}' ", submissionVar.UW_UNIT))[0]["UWUnitCodeDescription"].ToString();
                    }
                    catch (Exception)
                    {
                        submissionVar.BCS_UW_UNIT = string.Empty;
                    }
                    try
                    {
                        submissionVar.BCS_Policy_symbol = lookupPolicySymbols.Select(string.Format(" PolicySymbol = '{0}' ", submissionVar.Policy_symbol))[0]["PolicySymbolDescription"].ToString();
                    }
                    catch (Exception)
                    {
                        submissionVar.BCS_Policy_symbol = string.Empty;
                    }
                    try
                    {
                        submissionVar.BCS_Service_Office = lookupServiceOffices.Select(string.Format(" ServiceOfficeCode = '{0}' ", submissionVar.Service_Office))[0]["BCSCompanyNumberCodeID"].ToString();
                    }
                    catch (Exception)
                    {
                        submissionVar.BCS_Service_Office = string.Empty;
                    } 
                    #endregion

                    #region Submission Comments
                    DataRow[] CommentRows = commentsTable.Select(string.Format(key2, submissionVar.LOC_REP_CODE, submissionVar.APP_NUM));
                    List<Comment> commentsList = new List<Comment>();
                    foreach (DataRow rowComment in CommentRows)
                    {
                        Comment commentVar = new Comment();
                        commentVar.APP_NUM = Common.GetNullSafeString(rowComment["APP_NUM"]);
                        commentVar.Comment_Date = Common.GetNullSafeString(rowComment["Comment_Date"]);
                        commentVar.Comment_Time = Common.GetNullSafeString(rowComment["Comment_Time"]);
                        commentVar.Comment_user = Common.GetNullSafeString(rowComment["Comment_user"]);
                        commentVar.Comments = Common.GetNullSafeString(rowComment["Comments"]);
                        commentVar.LOC_REP_CODE = rowComment["LOC_REP_CODE"] == DBNull.Value ? 0 : (int)rowComment["LOC_REP_CODE"];
                        commentVar.POLICY_Num = Common.GetNullSafeString(rowComment["POLICY_Num"]);

                        commentsList.Add(commentVar);
                    }

                    submissionVar.Comments = commentsList.ToArray();
                    #endregion

                    #region Submission LOB Children
                    DataRow[] appLOBRows = appLOBsTable.Select(string.Format(key2, submissionVar.LOC_REP_CODE, submissionVar.APP_NUM));
                    List<AppLOB> appLOBList = new List<AppLOB>();
                    foreach (DataRow rowAppLOB in appLOBRows)
                    {
                        AppLOB appLOBVar = new AppLOB();
                        appLOBVar.APP_NUM = Common.GetNullSafeString(rowAppLOB["APP_NUM"]);
                        appLOBVar.LOB_CODE = Common.GetNullSafeString(rowAppLOB["LOB_CODE"]);
                        appLOBVar.LOC_REP_CODE = rowAppLOB["LOC_REP_CODE"] == DBNull.Value ? 0 : (int)rowAppLOB["LOC_REP_CODE"];
                        appLOBVar.POLICY_NUM = Common.GetNullSafeString(rowAppLOB["POLICY_NUM"]);
                        appLOBVar.STATUS_CODE = Common.GetNullSafeString(rowAppLOB["STATUS_CODE"]);
                        appLOBVar.STATUS_REASON = Common.GetNullSafeString(rowAppLOB["STATUS_REASON"]);

                        try
                        {
                            appLOBVar.BCS_STATUS_CODE = lookupStatuses.Select(string.Format(" StatusCode = '{0}' ", submissionVar.STATUS_CODE))[0]["StatusDescription"].ToString();
                        }
                        catch (Exception)
                        {
                            appLOBVar.BCS_STATUS_CODE = string.Empty;
                        }
                        try
                        {
                            appLOBVar.BCS_STATUS_CODE_REASON = lookupStatuses.Select(string.Format(" StatusReasonCode = '{0}' ", submissionVar.STATUS_REASON))[0]["StatusReasonDescription"].ToString();
                        }
                        catch (Exception)
                        {
                            appLOBVar.BCS_STATUS_CODE_REASON = string.Empty;
                        }
                        try
                        {
                            appLOBVar.BCS_STATUS_CODE_REASON_ID = lookupStatusReasons.Select(string.Format(" StatusReasonCode = '{0}' ", submissionVar.STATUS_REASON))[0]["BCSStatusReasonID"].ToString();
                        }
                        catch (Exception)
                        {
                            appLOBVar.BCS_STATUS_CODE_REASON_ID = "0";
                        }
                        try
                        {
                            appLOBVar.BCS_LOB_CODE = lookupLOBCodes.Select(string.Format(" LOBCode = '{0}' ", appLOBVar.LOB_CODE))[0]["BCSMultiLOBCode"].ToString();
                        }
                        catch (Exception)
                        {
                            appLOBVar.BCS_LOB_CODE = string.Empty;
                        }

                        appLOBList.Add(appLOBVar);
                    }

                    submissionVar.AppLOBs = appLOBList.ToArray();
                    #endregion

                    clientSubmissionList.Add(submissionVar);
                }
                clientVar.Submissions = clientSubmissionList.ToArray();

                #endregion

                clientsList.Add(clientVar);
            }
            clientsList.TrimExcess();
            return clientsList.ToArray();
        }
    }
}
