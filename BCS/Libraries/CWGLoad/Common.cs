using System;
using System.Collections.Generic;
using System.Text;
using BTS.LogFramework;
using BCS.CWGLoad.Business.Objects;
using System.Net;

namespace BCS.CWGLoad
{
    enum ClientTypes
    {
        BusinessOnly = 0,
        All
    }

    class Common
    {
        internal static string GetTargetDSN()
        {
            return BCS.CWGLoad.Properties.Settings.Default.SourceDSN;
        }

        internal static string GetSourceDSN()
        {
            return BCS.CWGLoad.Properties.Settings.Default.TargetDSN;
        }

        internal static bool IsNotNullOrEmpty(string p)
        {
            return !string.IsNullOrEmpty(p);
        }

        internal static string GetNullSafeString(object p)
        {
            if (p == null || p == DBNull.Value)
                return string.Empty;
            else
                return p.ToString();
        }
        internal static string GetNullSafeDateTimeString(object p)
        {
            try
            {
                return Convert.ToDateTime(GetNullSafeString(p)).ToString();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        internal static bool GetNullSafeBool(object p)
        {
            string val = GetNullSafeString(p);
            if (val.Equals("Y", StringComparison.CurrentCultureIgnoreCase) || val.Equals("True", StringComparison.CurrentCultureIgnoreCase))
                return true;
            return false;
        }

        internal static void SetLegacyFlags(string clientId)
        {
            BCS.ClientSys.Biz.DataManager dm = new BCS.ClientSys.Biz.DataManager(Properties.Settings.Default.ClientSysDSN);

            // names
            dm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Client.Columns.ClientCoreClientId, clientId);
            BCS.ClientSys.Biz.ClientNameCollection names = dm.GetClientNameCollection();
            foreach (BCS.ClientSys.Biz.ClientName var in names)
            {
                var.FromLegacySystem = true;
            }

            // addresses
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientAddress.Client.Columns.ClientCoreClientId, clientId);
            BCS.ClientSys.Biz.ClientAddressCollection addresses = dm.GetClientAddressCollection();
            foreach (BCS.ClientSys.Biz.ClientAddress var in addresses)
            {
                var.FromLegacySystem = true;
            }

            try
            {
                dm.CommitAll();
            }
            catch (Exception)
            {
                LogCentral.Current.LogWarn("Unable to set the legacy flag of client with id " + clientId);
            }
        }



        internal static ClientName[] RefineBillingSysEntries(ClientName[] clientNameArray)
        {
            int inBSCount = 0;
            foreach (ClientName var in clientNameArray)
            {
                if (var.InBillingSystem)
                    inBSCount++;
            }

            // bubble up in billing system entries if more than one found
            if (inBSCount > 1)
            {
                Array.Sort(clientNameArray, new NameInBillingSystemComparer());
            }

            // log if no in billing system entries
            if (inBSCount == 0)
            {
                // log entry to be used as in billing system
                LogCentral.Current.LogWarn(
                    string.Format("No 'In Billing System' Names were found. Treating CUST_LOC_REP_CODE - '{0}', CUST_ID - '{1}', LASTNAME - '{2}', FIRSTNAME - '{3}', MIDDLE - '{4}', Business_name - '{5}', FEIN - '{6}', insured dba indicator - '{7}', in billing sytem indicator - '{8}' as 'In Billing System' entry.",
                    clientNameArray[0].Cust_LOC_REP_CODE, clientNameArray[0].CUST_ID, clientNameArray[0].LASTNAME, clientNameArray[0].FIRSTNAME, clientNameArray[0].MIDDLE, clientNameArray[0].Business_name, clientNameArray[0].FEIN, clientNameArray[0].InsuredDBA_indicator, clientNameArray[0].InBillingSystemIndicator));
            }

            // log if multiple in billing system entries
            if (inBSCount > 1)
            {
                // log entry to be used as in billing system
                LogCentral.Current.LogWarn(
                    string.Format("Multiple({0}) 'In Billing System' Names were found. Treating CUST_LOC_REP_CODE - '{1}', CUST_ID - '{2}', LASTNAME - '{3}', FIRSTNAME - '{4}', MIDDLE - '{5}', Business_name - '{6}', FEIN - '{7}', insured dba indicator - '{8}', in billing sytem indicator - '{9}' as 'In Billing System' entry.",
                    inBSCount, clientNameArray[0].Cust_LOC_REP_CODE, clientNameArray[0].CUST_ID, clientNameArray[0].LASTNAME, clientNameArray[0].FIRSTNAME, clientNameArray[0].MIDDLE, clientNameArray[0].Business_name, clientNameArray[0].FEIN, clientNameArray[0].InsuredDBA_indicator, clientNameArray[0].InBillingSystemIndicator));
            }
            return clientNameArray;
        }
        internal static ClientAddress[] RefineBillingSysEntries(ClientAddress[] clientAddressArray)
        {
            int inBSCount = 0;
            foreach (ClientAddress var in clientAddressArray)
            {
                if (var.InBillingSystem)
                    inBSCount++;
            }
            // bubble up in billing system entries if more than one found
            if (inBSCount > 1)
            {
                Array.Sort(clientAddressArray, new AddressInBillingSystemComparer());
            }

            // log if no in billing system entries
            if (inBSCount == 0)
            {
                // log entry to be used as in billing system
                LogCentral.Current.LogWarn(
                    string.Format("No 'In Billing System' Addresses were found. Treating Cust_loc_rep_code - '{0}', Cust_id - '{1}', ADDR_NUM - '{2}', ADDR_STREET - '{3}', ADDR_STREET - '{4}', CITY - '{5}', COUNTY_NAME - '{6}', STATE - '{7}', ZIP - '{8}', ZIP_4 - '{9}', Mailing_physical_ind - '{10}', InBillingSystemIndicator - '{11}' as 'In Billing System' entry.",
                    clientAddressArray[0].Cust_loc_rep_code, clientAddressArray[0].Cust_id, clientAddressArray[0].ADDR_NUM, clientAddressArray[0].ADDR_STREET, clientAddressArray[0].ADDR_STREET, clientAddressArray[0].CITY, clientAddressArray[0].COUNTY_NAME, clientAddressArray[0].STATE, clientAddressArray[0].ZIP, clientAddressArray[0].ZIP_4, clientAddressArray[0].Mailing_physical_ind, clientAddressArray[0].InBillingSystemIndicator));
            }

            // log if multiple in billing system entries
            if (inBSCount > 1)
            {
                // log entry to be used as in billing system
                LogCentral.Current.LogWarn(
                    string.Format("Multiple({0}) 'In Billing System' Addresses were found. Treating Cust_loc_rep_code - '{1}', Cust_id - '{2}', ADDR_NUM - '{3}', ADDR_STREET - '{4}', ADDR_STREET - '{5}', CITY - '{6}', COUNTY_NAME - '{7}', STATE - '{8}', ZIP - '{9}', ZIP_4 - '{10}', Mailing_physical_ind - '{11}', InBillingSystemIndicator - '{12}' as 'In Billing System' entry.",
                    inBSCount, clientAddressArray[0].Cust_loc_rep_code, clientAddressArray[0].Cust_id, clientAddressArray[0].ADDR_NUM, clientAddressArray[0].ADDR_STREET, clientAddressArray[0].ADDR_STREET, clientAddressArray[0].CITY, clientAddressArray[0].COUNTY_NAME, clientAddressArray[0].STATE, clientAddressArray[0].ZIP, clientAddressArray[0].ZIP_4, clientAddressArray[0].Mailing_physical_ind, clientAddressArray[0].InBillingSystemIndicator));
            }

            return clientAddressArray;
        }
        public static ClientProxy.Client GetClientProxy()
        {
            return new ClientHelper();
            //return _clientProxy;
        }

        public static SubmissionProxy.Submission GetSubmissionProxy()
        {
            return new SubmissionHelper();
            //return _submissionProxy;
        }
    }

    public class ClientHelper : ClientProxy.Client
    {
        protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
        {
            //return base.GetWebRequest(uri);
            System.Net.WebRequest request = base.GetWebRequest(uri);
            (request as System.Net.HttpWebRequest).KeepAlive = false;
            return request;
        }
    }

    public class SubmissionHelper : SubmissionProxy.Submission
    {
        protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
        {
            //return base.GetWebRequest(uri);
            WebRequest request = base.GetWebRequest(uri);
            (request as System.Net.HttpWebRequest).KeepAlive = false;
            return request;
        }
    }
}
