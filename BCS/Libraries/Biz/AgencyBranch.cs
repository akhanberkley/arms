/* (ORM.NET)
 * AccessorName:AgencyLineOfBusiness
 * This is a one time generated class skeleton by ORM.NET.
 * Please add your business logic for the class here.
 * Please do not remove these comments as they are required by ORM.NET to function correctly.
 */
 
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace BCS.Biz
{
	public class AgencyBranch : AgencyBranchOrmTemplate
	{
        internal AgencyBranch(DataManager dataContext, DataRow ROW)
            : base(dataContext, ROW)
		{
			row = ROW;
		}
	}
}
