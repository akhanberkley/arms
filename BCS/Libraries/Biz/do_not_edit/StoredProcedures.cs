using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Reflection;

using OrmLib;

namespace BCS.Biz
{
	/// <summary>
	/// Wraps stored proceedure calls
	/// </summary>
	public class StoredProcedures 
	{
		private	static DataManager dm;
		/// <summary>
		/// Wraps stored proceedure calls
		/// </summary>
		static StoredProcedures() { dm = new DataManager(Config.Dsn);}
	
		/// <summary>
		/// Calls DeleteSubmissionById, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet DeleteSubmissionById(  System.Int32 Id )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramId = new SqlParameter( "@Id", Id);
			paramId.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramId.Direction = ParameterDirection.Input;
			paramId.Size = 4;
			arrayParams.Add( paramId );


			DataSet ds = dm.ExecuteProcedure("DeleteSubmissionById", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls DeleteAPSSubmissionById, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet DeleteAPSSubmissionById(  System.Int64 Id )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramId = new SqlParameter( "@Id", Id);
			paramId.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "bigint", true);
			paramId.Direction = ParameterDirection.Input;
			paramId.Size = 8;
			arrayParams.Add( paramId );


			DataSet ds = dm.ExecuteProcedure("DeleteAPSSubmissionById", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls GetAgencies, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet GetAgencies(  System.Int32 pageSize,
  System.Int32 pageNumber,
  System.Int32 companyNumberId,
  System.String sortField,
  System.String agencyNumber,
  System.String agencyName,
  System.String city,
  System.String state,
  System.String zip )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter parampageSize = new SqlParameter( "@pageSize", pageSize);
			parampageSize.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			parampageSize.Direction = ParameterDirection.Input;
			parampageSize.Size = 4;
			arrayParams.Add( parampageSize );

			SqlParameter parampageNumber = new SqlParameter( "@pageNumber", pageNumber);
			parampageNumber.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			parampageNumber.Direction = ParameterDirection.Input;
			parampageNumber.Size = 4;
			arrayParams.Add( parampageNumber );

			SqlParameter paramcompanyNumberId = new SqlParameter( "@companyNumberId", companyNumberId);
			paramcompanyNumberId.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramcompanyNumberId.Direction = ParameterDirection.Input;
			paramcompanyNumberId.Size = 4;
			arrayParams.Add( paramcompanyNumberId );

			SqlParameter paramsortField = new SqlParameter( "@sortField", sortField);
			paramsortField.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramsortField.Direction = ParameterDirection.Input;
			paramsortField.Size = 100;
			arrayParams.Add( paramsortField );

			SqlParameter paramagencyNumber = new SqlParameter( "@agencyNumber", agencyNumber);
			paramagencyNumber.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramagencyNumber.Direction = ParameterDirection.Input;
			paramagencyNumber.Size = 15;
			arrayParams.Add( paramagencyNumber );

			SqlParameter paramagencyName = new SqlParameter( "@agencyName", agencyName);
			paramagencyName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramagencyName.Direction = ParameterDirection.Input;
			paramagencyName.Size = 255;
			arrayParams.Add( paramagencyName );

			SqlParameter paramcity = new SqlParameter( "@city", city);
			paramcity.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramcity.Direction = ParameterDirection.Input;
			paramcity.Size = 50;
			arrayParams.Add( paramcity );

			SqlParameter paramstate = new SqlParameter( "@state", state);
			paramstate.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramstate.Direction = ParameterDirection.Input;
			paramstate.Size = 2;
			arrayParams.Add( paramstate );

			SqlParameter paramzip = new SqlParameter( "@zip", zip);
			paramzip.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramzip.Direction = ParameterDirection.Input;
			paramzip.Size = 10;
			arrayParams.Add( paramzip );


			DataSet ds = dm.ExecuteProcedure("GetAgencies", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_generateansiname, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_generateansiname(  ref System.String name )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramname = new SqlParameter( "@name", name);
			paramname.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramname.Direction = ParameterDirection.Output;
			paramname.Size = 255;
			arrayParams.Add( paramname );


			DataSet ds = dm.ExecuteProcedure("dt_generateansiname", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			name = (System.String) paramname.Value;

			return ds;
		}

		/// <summary>
		/// Calls dt_adduserobject, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_adduserobject(  )
		{
			ArrayList arrayParams = new ArrayList();
	

			DataSet ds = dm.ExecuteProcedure("dt_adduserobject", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_setpropertybyid, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_setpropertybyid(  System.Int32 id,
  System.String propertyParam,
  System.String value,
  System.Byte[] lvalue )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramid = new SqlParameter( "@id", id);
			paramid.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramid.Direction = ParameterDirection.Input;
			paramid.Size = 4;
			arrayParams.Add( paramid );

			SqlParameter paramproperty = new SqlParameter( "@property", propertyParam);
			paramproperty.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramproperty.Direction = ParameterDirection.Input;
			paramproperty.Size = 64;
			arrayParams.Add( paramproperty );

			SqlParameter paramvalue = new SqlParameter( "@value", value);
			paramvalue.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvalue.Direction = ParameterDirection.Input;
			paramvalue.Size = 255;
			arrayParams.Add( paramvalue );

			SqlParameter paramlvalue = new SqlParameter( "@lvalue", lvalue);
			paramlvalue.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "image", true);
			paramlvalue.Direction = ParameterDirection.Input;
			paramlvalue.Size = 16;
			arrayParams.Add( paramlvalue );


			DataSet ds = dm.ExecuteProcedure("dt_setpropertybyid", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_getobjwithprop, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_getobjwithprop(  System.String propertyParam,
  System.String value )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramproperty = new SqlParameter( "@property", propertyParam);
			paramproperty.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramproperty.Direction = ParameterDirection.Input;
			paramproperty.Size = 30;
			arrayParams.Add( paramproperty );

			SqlParameter paramvalue = new SqlParameter( "@value", value);
			paramvalue.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvalue.Direction = ParameterDirection.Input;
			paramvalue.Size = 255;
			arrayParams.Add( paramvalue );


			DataSet ds = dm.ExecuteProcedure("dt_getobjwithprop", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_getpropertiesbyid, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_getpropertiesbyid(  System.Int32 id,
  System.String propertyParam )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramid = new SqlParameter( "@id", id);
			paramid.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramid.Direction = ParameterDirection.Input;
			paramid.Size = 4;
			arrayParams.Add( paramid );

			SqlParameter paramproperty = new SqlParameter( "@property", propertyParam);
			paramproperty.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramproperty.Direction = ParameterDirection.Input;
			paramproperty.Size = 64;
			arrayParams.Add( paramproperty );


			DataSet ds = dm.ExecuteProcedure("dt_getpropertiesbyid", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_setpropertybyid_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_setpropertybyid_u(  System.Int32 id,
  System.String propertyParam,
  System.String uvalue,
  System.Byte[] lvalue )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramid = new SqlParameter( "@id", id);
			paramid.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramid.Direction = ParameterDirection.Input;
			paramid.Size = 4;
			arrayParams.Add( paramid );

			SqlParameter paramproperty = new SqlParameter( "@property", propertyParam);
			paramproperty.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramproperty.Direction = ParameterDirection.Input;
			paramproperty.Size = 64;
			arrayParams.Add( paramproperty );

			SqlParameter paramuvalue = new SqlParameter( "@uvalue", uvalue);
			paramuvalue.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramuvalue.Direction = ParameterDirection.Input;
			paramuvalue.Size = 510;
			arrayParams.Add( paramuvalue );

			SqlParameter paramlvalue = new SqlParameter( "@lvalue", lvalue);
			paramlvalue.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "image", true);
			paramlvalue.Direction = ParameterDirection.Input;
			paramlvalue.Size = 16;
			arrayParams.Add( paramlvalue );


			DataSet ds = dm.ExecuteProcedure("dt_setpropertybyid_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_getobjwithprop_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_getobjwithprop_u(  System.String propertyParam,
  System.String uvalue )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramproperty = new SqlParameter( "@property", propertyParam);
			paramproperty.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramproperty.Direction = ParameterDirection.Input;
			paramproperty.Size = 30;
			arrayParams.Add( paramproperty );

			SqlParameter paramuvalue = new SqlParameter( "@uvalue", uvalue);
			paramuvalue.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramuvalue.Direction = ParameterDirection.Input;
			paramuvalue.Size = 510;
			arrayParams.Add( paramuvalue );


			DataSet ds = dm.ExecuteProcedure("dt_getobjwithprop_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_getpropertiesbyid_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_getpropertiesbyid_u(  System.Int32 id,
  System.String propertyParam )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramid = new SqlParameter( "@id", id);
			paramid.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramid.Direction = ParameterDirection.Input;
			paramid.Size = 4;
			arrayParams.Add( paramid );

			SqlParameter paramproperty = new SqlParameter( "@property", propertyParam);
			paramproperty.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramproperty.Direction = ParameterDirection.Input;
			paramproperty.Size = 64;
			arrayParams.Add( paramproperty );


			DataSet ds = dm.ExecuteProcedure("dt_getpropertiesbyid_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_dropuserobjectbyid, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_dropuserobjectbyid(  System.Int32 id )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramid = new SqlParameter( "@id", id);
			paramid.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramid.Direction = ParameterDirection.Input;
			paramid.Size = 4;
			arrayParams.Add( paramid );


			DataSet ds = dm.ExecuteProcedure("dt_dropuserobjectbyid", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_droppropertiesbyid, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_droppropertiesbyid(  System.Int32 id,
  System.String propertyParam )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramid = new SqlParameter( "@id", id);
			paramid.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramid.Direction = ParameterDirection.Input;
			paramid.Size = 4;
			arrayParams.Add( paramid );

			SqlParameter paramproperty = new SqlParameter( "@property", propertyParam);
			paramproperty.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramproperty.Direction = ParameterDirection.Input;
			paramproperty.Size = 64;
			arrayParams.Add( paramproperty );


			DataSet ds = dm.ExecuteProcedure("dt_droppropertiesbyid", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_verstamp006, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_verstamp006(  )
		{
			ArrayList arrayParams = new ArrayList();
	

			DataSet ds = dm.ExecuteProcedure("dt_verstamp006", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_verstamp007, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_verstamp007(  )
		{
			ArrayList arrayParams = new ArrayList();
	

			DataSet ds = dm.ExecuteProcedure("dt_verstamp007", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_getpropertiesbyid_vcs, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_getpropertiesbyid_vcs(  System.Int32 id,
  System.String propertyParam,
  ref System.String value )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramid = new SqlParameter( "@id", id);
			paramid.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramid.Direction = ParameterDirection.Input;
			paramid.Size = 4;
			arrayParams.Add( paramid );

			SqlParameter paramproperty = new SqlParameter( "@property", propertyParam);
			paramproperty.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramproperty.Direction = ParameterDirection.Input;
			paramproperty.Size = 64;
			arrayParams.Add( paramproperty );

			SqlParameter paramvalue = new SqlParameter( "@value", value);
			paramvalue.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvalue.Direction = ParameterDirection.Output;
			paramvalue.Size = 255;
			arrayParams.Add( paramvalue );


			DataSet ds = dm.ExecuteProcedure("dt_getpropertiesbyid_vcs", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			value = (System.String) paramvalue.Value;

			return ds;
		}

		/// <summary>
		/// Calls dt_displayoaerror, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_displayoaerror(  System.Int32 iObject,
  System.Int32 iresult )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramiObject = new SqlParameter( "@iObject", iObject);
			paramiObject.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiObject.Direction = ParameterDirection.Input;
			paramiObject.Size = 4;
			arrayParams.Add( paramiObject );

			SqlParameter paramiresult = new SqlParameter( "@iresult", iresult);
			paramiresult.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiresult.Direction = ParameterDirection.Input;
			paramiresult.Size = 4;
			arrayParams.Add( paramiresult );


			DataSet ds = dm.ExecuteProcedure("dt_displayoaerror", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_adduserobject_vcs, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_adduserobject_vcs(  System.String vchProperty )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramvchProperty = new SqlParameter( "@vchProperty", vchProperty);
			paramvchProperty.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchProperty.Direction = ParameterDirection.Input;
			paramvchProperty.Size = 64;
			arrayParams.Add( paramvchProperty );


			DataSet ds = dm.ExecuteProcedure("dt_adduserobject_vcs", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_addtosourcecontrol, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_addtosourcecontrol(  System.String vchSourceSafeINI,
  System.String vchProjectName,
  System.String vchComment,
  System.String vchLoginName,
  System.String vchPassword )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramvchSourceSafeINI = new SqlParameter( "@vchSourceSafeINI", vchSourceSafeINI);
			paramvchSourceSafeINI.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchSourceSafeINI.Direction = ParameterDirection.Input;
			paramvchSourceSafeINI.Size = 255;
			arrayParams.Add( paramvchSourceSafeINI );

			SqlParameter paramvchProjectName = new SqlParameter( "@vchProjectName", vchProjectName);
			paramvchProjectName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchProjectName.Direction = ParameterDirection.Input;
			paramvchProjectName.Size = 255;
			arrayParams.Add( paramvchProjectName );

			SqlParameter paramvchComment = new SqlParameter( "@vchComment", vchComment);
			paramvchComment.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchComment.Direction = ParameterDirection.Input;
			paramvchComment.Size = 255;
			arrayParams.Add( paramvchComment );

			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 255;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 255;
			arrayParams.Add( paramvchPassword );


			DataSet ds = dm.ExecuteProcedure("dt_addtosourcecontrol", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_checkinobject, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_checkinobject(  System.String chObjectType,
  System.String vchObjectName,
  System.String vchComment,
  System.String vchLoginName,
  System.String vchPassword,
  System.Int32 iVCSFlags,
  System.Int32 iActionFlag,
  System.String txStream1,
  System.String txStream2,
  System.String txStream3 )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramchObjectType = new SqlParameter( "@chObjectType", chObjectType);
			paramchObjectType.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "char", true);
			paramchObjectType.Direction = ParameterDirection.Input;
			paramchObjectType.Size = 4;
			arrayParams.Add( paramchObjectType );

			SqlParameter paramvchObjectName = new SqlParameter( "@vchObjectName", vchObjectName);
			paramvchObjectName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchObjectName.Direction = ParameterDirection.Input;
			paramvchObjectName.Size = 255;
			arrayParams.Add( paramvchObjectName );

			SqlParameter paramvchComment = new SqlParameter( "@vchComment", vchComment);
			paramvchComment.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchComment.Direction = ParameterDirection.Input;
			paramvchComment.Size = 255;
			arrayParams.Add( paramvchComment );

			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 255;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 255;
			arrayParams.Add( paramvchPassword );

			SqlParameter paramiVCSFlags = new SqlParameter( "@iVCSFlags", iVCSFlags);
			paramiVCSFlags.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiVCSFlags.Direction = ParameterDirection.Input;
			paramiVCSFlags.Size = 4;
			arrayParams.Add( paramiVCSFlags );

			SqlParameter paramiActionFlag = new SqlParameter( "@iActionFlag", iActionFlag);
			paramiActionFlag.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiActionFlag.Direction = ParameterDirection.Input;
			paramiActionFlag.Size = 4;
			arrayParams.Add( paramiActionFlag );

			SqlParameter paramtxStream1 = new SqlParameter( "@txStream1", txStream1);
			paramtxStream1.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "text", true);
			paramtxStream1.Direction = ParameterDirection.Input;
			paramtxStream1.Size = 16;
			arrayParams.Add( paramtxStream1 );

			SqlParameter paramtxStream2 = new SqlParameter( "@txStream2", txStream2);
			paramtxStream2.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "text", true);
			paramtxStream2.Direction = ParameterDirection.Input;
			paramtxStream2.Size = 16;
			arrayParams.Add( paramtxStream2 );

			SqlParameter paramtxStream3 = new SqlParameter( "@txStream3", txStream3);
			paramtxStream3.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "text", true);
			paramtxStream3.Direction = ParameterDirection.Input;
			paramtxStream3.Size = 16;
			arrayParams.Add( paramtxStream3 );


			DataSet ds = dm.ExecuteProcedure("dt_checkinobject", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_checkoutobject, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_checkoutobject(  System.String chObjectType,
  System.String vchObjectName,
  System.String vchComment,
  System.String vchLoginName,
  System.String vchPassword,
  System.Int32 iVCSFlags,
  System.Int32 iActionFlag )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramchObjectType = new SqlParameter( "@chObjectType", chObjectType);
			paramchObjectType.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "char", true);
			paramchObjectType.Direction = ParameterDirection.Input;
			paramchObjectType.Size = 4;
			arrayParams.Add( paramchObjectType );

			SqlParameter paramvchObjectName = new SqlParameter( "@vchObjectName", vchObjectName);
			paramvchObjectName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchObjectName.Direction = ParameterDirection.Input;
			paramvchObjectName.Size = 255;
			arrayParams.Add( paramvchObjectName );

			SqlParameter paramvchComment = new SqlParameter( "@vchComment", vchComment);
			paramvchComment.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchComment.Direction = ParameterDirection.Input;
			paramvchComment.Size = 255;
			arrayParams.Add( paramvchComment );

			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 255;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 255;
			arrayParams.Add( paramvchPassword );

			SqlParameter paramiVCSFlags = new SqlParameter( "@iVCSFlags", iVCSFlags);
			paramiVCSFlags.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiVCSFlags.Direction = ParameterDirection.Input;
			paramiVCSFlags.Size = 4;
			arrayParams.Add( paramiVCSFlags );

			SqlParameter paramiActionFlag = new SqlParameter( "@iActionFlag", iActionFlag);
			paramiActionFlag.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiActionFlag.Direction = ParameterDirection.Input;
			paramiActionFlag.Size = 4;
			arrayParams.Add( paramiActionFlag );


			DataSet ds = dm.ExecuteProcedure("dt_checkoutobject", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_isundersourcecontrol, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_isundersourcecontrol(  System.String vchLoginName,
  System.String vchPassword,
  System.Int32 iWhoToo )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 255;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 255;
			arrayParams.Add( paramvchPassword );

			SqlParameter paramiWhoToo = new SqlParameter( "@iWhoToo", iWhoToo);
			paramiWhoToo.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiWhoToo.Direction = ParameterDirection.Input;
			paramiWhoToo.Size = 4;
			arrayParams.Add( paramiWhoToo );


			DataSet ds = dm.ExecuteProcedure("dt_isundersourcecontrol", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_removefromsourcecontrol, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_removefromsourcecontrol(  )
		{
			ArrayList arrayParams = new ArrayList();
	

			DataSet ds = dm.ExecuteProcedure("dt_removefromsourcecontrol", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_validateloginparams, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_validateloginparams(  System.String vchLoginName,
  System.String vchPassword )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 255;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 255;
			arrayParams.Add( paramvchPassword );


			DataSet ds = dm.ExecuteProcedure("dt_validateloginparams", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_vcsenabled, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_vcsenabled(  )
		{
			ArrayList arrayParams = new ArrayList();
	

			DataSet ds = dm.ExecuteProcedure("dt_vcsenabled", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_whocheckedout, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_whocheckedout(  System.String chObjectType,
  System.String vchObjectName,
  System.String vchLoginName,
  System.String vchPassword )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramchObjectType = new SqlParameter( "@chObjectType", chObjectType);
			paramchObjectType.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "char", true);
			paramchObjectType.Direction = ParameterDirection.Input;
			paramchObjectType.Size = 4;
			arrayParams.Add( paramchObjectType );

			SqlParameter paramvchObjectName = new SqlParameter( "@vchObjectName", vchObjectName);
			paramvchObjectName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchObjectName.Direction = ParameterDirection.Input;
			paramvchObjectName.Size = 255;
			arrayParams.Add( paramvchObjectName );

			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 255;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 255;
			arrayParams.Add( paramvchPassword );


			DataSet ds = dm.ExecuteProcedure("dt_whocheckedout", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_getpropertiesbyid_vcs_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_getpropertiesbyid_vcs_u(  System.Int32 id,
  System.String propertyParam,
  ref System.String value )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramid = new SqlParameter( "@id", id);
			paramid.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramid.Direction = ParameterDirection.Input;
			paramid.Size = 4;
			arrayParams.Add( paramid );

			SqlParameter paramproperty = new SqlParameter( "@property", propertyParam);
			paramproperty.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "varchar", true);
			paramproperty.Direction = ParameterDirection.Input;
			paramproperty.Size = 64;
			arrayParams.Add( paramproperty );

			SqlParameter paramvalue = new SqlParameter( "@value", value);
			paramvalue.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvalue.Direction = ParameterDirection.Output;
			paramvalue.Size = 510;
			arrayParams.Add( paramvalue );


			DataSet ds = dm.ExecuteProcedure("dt_getpropertiesbyid_vcs_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			value = (System.String) paramvalue.Value;

			return ds;
		}

		/// <summary>
		/// Calls dt_displayoaerror_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_displayoaerror_u(  System.Int32 iObject,
  System.Int32 iresult )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramiObject = new SqlParameter( "@iObject", iObject);
			paramiObject.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiObject.Direction = ParameterDirection.Input;
			paramiObject.Size = 4;
			arrayParams.Add( paramiObject );

			SqlParameter paramiresult = new SqlParameter( "@iresult", iresult);
			paramiresult.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiresult.Direction = ParameterDirection.Input;
			paramiresult.Size = 4;
			arrayParams.Add( paramiresult );


			DataSet ds = dm.ExecuteProcedure("dt_displayoaerror_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_addtosourcecontrol_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_addtosourcecontrol_u(  System.String vchSourceSafeINI,
  System.String vchProjectName,
  System.String vchComment,
  System.String vchLoginName,
  System.String vchPassword )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramvchSourceSafeINI = new SqlParameter( "@vchSourceSafeINI", vchSourceSafeINI);
			paramvchSourceSafeINI.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchSourceSafeINI.Direction = ParameterDirection.Input;
			paramvchSourceSafeINI.Size = 510;
			arrayParams.Add( paramvchSourceSafeINI );

			SqlParameter paramvchProjectName = new SqlParameter( "@vchProjectName", vchProjectName);
			paramvchProjectName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchProjectName.Direction = ParameterDirection.Input;
			paramvchProjectName.Size = 510;
			arrayParams.Add( paramvchProjectName );

			SqlParameter paramvchComment = new SqlParameter( "@vchComment", vchComment);
			paramvchComment.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchComment.Direction = ParameterDirection.Input;
			paramvchComment.Size = 510;
			arrayParams.Add( paramvchComment );

			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 510;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 510;
			arrayParams.Add( paramvchPassword );


			DataSet ds = dm.ExecuteProcedure("dt_addtosourcecontrol_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_checkinobject_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_checkinobject_u(  System.String chObjectType,
  System.String vchObjectName,
  System.String vchComment,
  System.String vchLoginName,
  System.String vchPassword,
  System.Int32 iVCSFlags,
  System.Int32 iActionFlag,
  System.String txStream1,
  System.String txStream2,
  System.String txStream3 )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramchObjectType = new SqlParameter( "@chObjectType", chObjectType);
			paramchObjectType.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "char", true);
			paramchObjectType.Direction = ParameterDirection.Input;
			paramchObjectType.Size = 4;
			arrayParams.Add( paramchObjectType );

			SqlParameter paramvchObjectName = new SqlParameter( "@vchObjectName", vchObjectName);
			paramvchObjectName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchObjectName.Direction = ParameterDirection.Input;
			paramvchObjectName.Size = 510;
			arrayParams.Add( paramvchObjectName );

			SqlParameter paramvchComment = new SqlParameter( "@vchComment", vchComment);
			paramvchComment.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchComment.Direction = ParameterDirection.Input;
			paramvchComment.Size = 510;
			arrayParams.Add( paramvchComment );

			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 510;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 510;
			arrayParams.Add( paramvchPassword );

			SqlParameter paramiVCSFlags = new SqlParameter( "@iVCSFlags", iVCSFlags);
			paramiVCSFlags.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiVCSFlags.Direction = ParameterDirection.Input;
			paramiVCSFlags.Size = 4;
			arrayParams.Add( paramiVCSFlags );

			SqlParameter paramiActionFlag = new SqlParameter( "@iActionFlag", iActionFlag);
			paramiActionFlag.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiActionFlag.Direction = ParameterDirection.Input;
			paramiActionFlag.Size = 4;
			arrayParams.Add( paramiActionFlag );

			SqlParameter paramtxStream1 = new SqlParameter( "@txStream1", txStream1);
			paramtxStream1.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "text", true);
			paramtxStream1.Direction = ParameterDirection.Input;
			paramtxStream1.Size = 16;
			arrayParams.Add( paramtxStream1 );

			SqlParameter paramtxStream2 = new SqlParameter( "@txStream2", txStream2);
			paramtxStream2.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "text", true);
			paramtxStream2.Direction = ParameterDirection.Input;
			paramtxStream2.Size = 16;
			arrayParams.Add( paramtxStream2 );

			SqlParameter paramtxStream3 = new SqlParameter( "@txStream3", txStream3);
			paramtxStream3.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "text", true);
			paramtxStream3.Direction = ParameterDirection.Input;
			paramtxStream3.Size = 16;
			arrayParams.Add( paramtxStream3 );


			DataSet ds = dm.ExecuteProcedure("dt_checkinobject_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_checkoutobject_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_checkoutobject_u(  System.String chObjectType,
  System.String vchObjectName,
  System.String vchComment,
  System.String vchLoginName,
  System.String vchPassword,
  System.Int32 iVCSFlags,
  System.Int32 iActionFlag )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramchObjectType = new SqlParameter( "@chObjectType", chObjectType);
			paramchObjectType.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "char", true);
			paramchObjectType.Direction = ParameterDirection.Input;
			paramchObjectType.Size = 4;
			arrayParams.Add( paramchObjectType );

			SqlParameter paramvchObjectName = new SqlParameter( "@vchObjectName", vchObjectName);
			paramvchObjectName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchObjectName.Direction = ParameterDirection.Input;
			paramvchObjectName.Size = 510;
			arrayParams.Add( paramvchObjectName );

			SqlParameter paramvchComment = new SqlParameter( "@vchComment", vchComment);
			paramvchComment.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchComment.Direction = ParameterDirection.Input;
			paramvchComment.Size = 510;
			arrayParams.Add( paramvchComment );

			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 510;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 510;
			arrayParams.Add( paramvchPassword );

			SqlParameter paramiVCSFlags = new SqlParameter( "@iVCSFlags", iVCSFlags);
			paramiVCSFlags.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiVCSFlags.Direction = ParameterDirection.Input;
			paramiVCSFlags.Size = 4;
			arrayParams.Add( paramiVCSFlags );

			SqlParameter paramiActionFlag = new SqlParameter( "@iActionFlag", iActionFlag);
			paramiActionFlag.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiActionFlag.Direction = ParameterDirection.Input;
			paramiActionFlag.Size = 4;
			arrayParams.Add( paramiActionFlag );


			DataSet ds = dm.ExecuteProcedure("dt_checkoutobject_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_isundersourcecontrol_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_isundersourcecontrol_u(  System.String vchLoginName,
  System.String vchPassword,
  System.Int32 iWhoToo )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 510;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 510;
			arrayParams.Add( paramvchPassword );

			SqlParameter paramiWhoToo = new SqlParameter( "@iWhoToo", iWhoToo);
			paramiWhoToo.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "int", true);
			paramiWhoToo.Direction = ParameterDirection.Input;
			paramiWhoToo.Size = 4;
			arrayParams.Add( paramiWhoToo );


			DataSet ds = dm.ExecuteProcedure("dt_isundersourcecontrol_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_validateloginparams_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_validateloginparams_u(  System.String vchLoginName,
  System.String vchPassword )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 510;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 510;
			arrayParams.Add( paramvchPassword );


			DataSet ds = dm.ExecuteProcedure("dt_validateloginparams_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}

		/// <summary>
		/// Calls dt_whocheckedout_u, returning a DataSet of any rows returned 
		/// from the stored proceedure.
		/// </summary>
		/// <returns>DataSet</returns>
		public static DataSet dt_whocheckedout_u(  System.String chObjectType,
  System.String vchObjectName,
  System.String vchLoginName,
  System.String vchPassword )
		{
			ArrayList arrayParams = new ArrayList();
	
			SqlParameter paramchObjectType = new SqlParameter( "@chObjectType", chObjectType);
			paramchObjectType.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "char", true);
			paramchObjectType.Direction = ParameterDirection.Input;
			paramchObjectType.Size = 4;
			arrayParams.Add( paramchObjectType );

			SqlParameter paramvchObjectName = new SqlParameter( "@vchObjectName", vchObjectName);
			paramvchObjectName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchObjectName.Direction = ParameterDirection.Input;
			paramvchObjectName.Size = 510;
			arrayParams.Add( paramvchObjectName );

			SqlParameter paramvchLoginName = new SqlParameter( "@vchLoginName", vchLoginName);
			paramvchLoginName.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchLoginName.Direction = ParameterDirection.Input;
			paramvchLoginName.Size = 510;
			arrayParams.Add( paramvchLoginName );

			SqlParameter paramvchPassword = new SqlParameter( "@vchPassword", vchPassword);
			paramvchPassword.SqlDbType = (SqlDbType) Enum.Parse( typeof(SqlDbType), "nvarchar", true);
			paramvchPassword.Direction = ParameterDirection.Input;
			paramvchPassword.Size = 510;
			arrayParams.Add( paramvchPassword );


			DataSet ds = dm.ExecuteProcedure("dt_whocheckedout_u", (SqlParameter[]) arrayParams.ToArray(typeof(SqlParameter)));
			
			return ds;
		}


	}
}
