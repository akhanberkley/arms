using System;
using System.Data;
using System.Data.SqlTypes;
using System.Collections;
using System.ComponentModel;

namespace BCS.ClientSys.Biz
{


	/// <summary>
	/// Wraps a datarow, exposing it's richest features as properties and methods.
	/// </summary>
	public abstract class ClientOrmTemplate : Business
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="dataContext">The DataManager this object should perform it's operations with</param>
		/// <param name="ROW">The underlying row that this object will wrap</param>
		internal ClientOrmTemplate( DataManager dataContext, DataRow ROW) : base(dataContext) 
		{ 
			row = ROW; 
		}


		/// <summary>
		/// Gets and sets the property at the underlying row index.
		/// </summary>
		public object this[int index]
		{
			get
			{
				if ( this.row.IsNull(index) )
					return null;
				else
					return this.row[index];
			}
			set
			{
				if ( value == null )
					this.row[index] = DBNull.Value;
				else
					this.row[index] = value;
			}
		}

		/// <summary>
		/// Gets and sets the property by its name.
		/// </summary>
		/// <remarks>
		/// Do not use the underlying column name if it is different.
		/// </remarks>
		public object this[string property]
		{
			get
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanRead )
					return pi.GetValue(this, null);

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Get" + property,new System.Type[]{});
								
				if ( mi != null )
					return mi.Invoke(this, null);

				return null;
			}
			set
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanWrite )
				{
					pi.SetValue(this, value, null);
					return;
				}

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Set" + property,new System.Type[]{value.GetType()});
								
				if ( mi != null )
					mi.Invoke(this, new object[]{value});
			}
		}		

		
		/// <summary>
		/// Returns true if the underlying DataRow.RowState is marked as DataRowState.Deleted
		/// </summary>
		/// <returns>true if deleted</returns>
		public virtual bool IsDeleted()
		{
			return row.RowState == DataRowState.Deleted;
		}

		/// <summary>
		/// Gets the Id.
		/// </summary>
		/// <value>
		/// The underlying rows Id cell
		/// </value>
		public virtual System.Int32 Id 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return (System.Int32) row["Id"]; 
				else
					return (System.Int32) row["Id", DataRowVersion.Original];
				
				//System.Int32 
			} 
		} 
		/// <summary>
		/// Gets and Sets the CompanyId.
		/// </summary>
		/// <value>
		/// The underlying rows CompanyId cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlInt32 CompanyId 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("CompanyId"))
						return System.Data.SqlTypes.SqlInt32.Null;
					else
						return new System.Data.SqlTypes.SqlInt32((System.Int32)row["CompanyId"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["CompanyId"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlInt32.Null;
					else
						return new System.Data.SqlTypes.SqlInt32((System.Int32)row["CompanyId", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["CompanyId"] = DBNull.Value;
				else
					row["CompanyId"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the ClientCoreClientId.
		/// </summary>
		/// <value>
		/// The underlying rows ClientCoreClientId cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlInt64 ClientCoreClientId 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("ClientCoreClientId"))
						return System.Data.SqlTypes.SqlInt64.Null;
					else
						return new System.Data.SqlTypes.SqlInt64((System.Int64)row["ClientCoreClientId"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["ClientCoreClientId"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlInt64.Null;
					else
						return new System.Data.SqlTypes.SqlInt64((System.Int64)row["ClientCoreClientId", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["ClientCoreClientId"] = DBNull.Value;
				else
					row["ClientCoreClientId"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the TaxNumber.
		/// </summary>
		/// <value>
		/// The underlying rows TaxNumber cell
		/// </value>
		public virtual System.String TaxNumber 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("TaxNumber") ? null : (System.String) row["TaxNumber"]; 
				else
					return row.IsNull(row.Table.Columns["TaxNumber"], DataRowVersion.Original) ? null : (System.String) row["TaxNumber", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["TaxNumber"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the TaxTypeCode.
		/// </summary>
		/// <value>
		/// The underlying rows TaxTypeCode cell
		/// </value>
		public virtual System.String TaxTypeCode 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("TaxTypeCode") ? null : (System.String) row["TaxTypeCode"]; 
				else
					return row.IsNull(row.Table.Columns["TaxTypeCode"], DataRowVersion.Original) ? null : (System.String) row["TaxTypeCode", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["TaxTypeCode"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the PortfolioId.
		/// </summary>
		/// <value>
		/// The underlying rows PortfolioId cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlInt64 PortfolioId 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("PortfolioId"))
						return System.Data.SqlTypes.SqlInt64.Null;
					else
						return new System.Data.SqlTypes.SqlInt64((System.Int64)row["PortfolioId"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["PortfolioId"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlInt64.Null;
					else
						return new System.Data.SqlTypes.SqlInt64((System.Int64)row["PortfolioId", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["PortfolioId"] = DBNull.Value;
				else
					row["PortfolioId"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the EntryDt.
		/// </summary>
		/// <value>
		/// The underlying rows EntryDt cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlDateTime EntryDt 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("EntryDt"))
						return System.Data.SqlTypes.SqlDateTime.Null;
					else
						return new System.Data.SqlTypes.SqlDateTime((System.DateTime)row["EntryDt"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["EntryDt"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlDateTime.Null;
					else
						return new System.Data.SqlTypes.SqlDateTime((System.DateTime)row["EntryDt", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["EntryDt"] = DBNull.Value;
				else
					row["EntryDt"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the Obsolete.
		/// </summary>
		/// <value>
		/// The underlying rows Obsolete cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlBoolean Obsolete 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("Obsolete"))
						return System.Data.SqlTypes.SqlBoolean.Null;
					else
						return new System.Data.SqlTypes.SqlBoolean((System.Boolean)row["Obsolete"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["Obsolete"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlBoolean.Null;
					else
						return new System.Data.SqlTypes.SqlBoolean((System.Boolean)row["Obsolete", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["Obsolete"] = DBNull.Value;
				else
					row["Obsolete"] = value.Value; 
 
			} 
		}
		 
		public virtual BCS.ClientSys.Biz.TaxType TaxType
		{
			get
			{
				if (row.IsNull("TaxTypeCode"))
					return null;
				else
					return DataContext.GetLookups().TaxTypes.FindByCode( (System.String) row["TaxTypeCode"] );
			}
			set
			{
				row["TaxTypeCode"] = value.Code;
			}
		}

		 
		public virtual BCS.ClientSys.Biz.Company Company
		{
			get
			{
				if (row.IsNull("CompanyId"))
					return null;
				else
					return DataContext.GetLookups().Companys.FindById( (System.Int32) row["CompanyId"] );
			}
			set
			{
				row["CompanyId"] = value.Id;
			}
		}

		private BCS.ClientSys.Biz.ClientAddressCollection _ClientAddresss = null;
	
		/// <summary>
		/// Refreshes the collection of ClientAddresss from the underlying dataset
		/// </summary>
		internal void refreshClientAddresss()
		{
			if (_ClientAddresss == null) _ClientAddresss = new BCS.ClientSys.Biz.ClientAddressCollection();
			
			((IList)_ClientAddresss).Clear();

			DataRow[] cr = row.GetChildRows("ClientIDClientAddress");
			foreach( DataRow chld in cr)
			{
				BCS.ClientSys.Biz.ClientAddress obj = new BCS.ClientSys.Biz.ClientAddress(base.DataContext, chld);
				_ClientAddresss.Add( obj );
			}
			
			// add after, so that events wont be fired
			_ClientAddresss.Parent = this;
		}
				
		/// <summary>
		/// Exposes the collection of child BCS.ClientSys.Biz.ClientAddresss.
		/// </summary>
		public virtual BCS.ClientSys.Biz.ClientAddressCollection ClientAddresss
		{
			get 
			{ 
				if (_ClientAddresss == null) refreshClientAddresss();

				return _ClientAddresss;
			}
		}


			/// <summary>
			/// Adds a BCS.ClientSys.Biz.ClientAddress to the collection.
			/// </summary>
			public virtual int AddClientAddress(BCS.ClientSys.Biz.ClientAddress newClientAddress)
			{
				if ( _ClientAddresss == null ) refreshClientAddresss();

				if ( newClientAddress.row.GetParentRow(base.DataSet.Relations["ClientIDClientAddress"]) == row )
					return _ClientAddresss.IndexOf( newClientAddress );

				newClientAddress.row.SetParentRow(row,base.DataSet.Relations["ClientIDClientAddress"]);

				int index = _ClientAddresss.Add( newClientAddress );

				return index;

			}

			/// <summary>
			/// Creates a new BCS.ClientSys.Biz.ClientAddress, adding it to the collection.
			/// </summary>
			/// <remarks>
			/// CommitAll() must be called to persist this to the database.
			/// </remarks>
			/// <returns>A new ClientAddress object</returns>
			public virtual BCS.ClientSys.Biz.ClientAddress NewClientAddress()
			{
				if ( _ClientAddresss == null ) refreshClientAddresss();

				BCS.ClientSys.Biz.ClientAddress newClientAddress = new BCS.ClientSys.Biz.ClientAddress(base.DataContext, base.DataSet.Tables["ClientAddress"].NewRow());
				base.DataSet.Tables["ClientAddress"].Rows.Add(newClientAddress.row);
				
				this.AddClientAddress(newClientAddress);

				return newClientAddress;
			}
	
		private BCS.ClientSys.Biz.ClientNameCollection _ClientNames = null;
	
		/// <summary>
		/// Refreshes the collection of ClientNames from the underlying dataset
		/// </summary>
		internal void refreshClientNames()
		{
			if (_ClientNames == null) _ClientNames = new BCS.ClientSys.Biz.ClientNameCollection();
			
			((IList)_ClientNames).Clear();

			DataRow[] cr = row.GetChildRows("ClientIDClientName");
			foreach( DataRow chld in cr)
			{
				BCS.ClientSys.Biz.ClientName obj = new BCS.ClientSys.Biz.ClientName(base.DataContext, chld);
				_ClientNames.Add( obj );
			}
			
			// add after, so that events wont be fired
			_ClientNames.Parent = this;
		}
				
		/// <summary>
		/// Exposes the collection of child BCS.ClientSys.Biz.ClientNames.
		/// </summary>
		public virtual BCS.ClientSys.Biz.ClientNameCollection ClientNames
		{
			get 
			{ 
				if (_ClientNames == null) refreshClientNames();

				return _ClientNames;
			}
		}


			/// <summary>
			/// Adds a BCS.ClientSys.Biz.ClientName to the collection.
			/// </summary>
			public virtual int AddClientName(BCS.ClientSys.Biz.ClientName newClientName)
			{
				if ( _ClientNames == null ) refreshClientNames();

				if ( newClientName.row.GetParentRow(base.DataSet.Relations["ClientIDClientName"]) == row )
					return _ClientNames.IndexOf( newClientName );

				newClientName.row.SetParentRow(row,base.DataSet.Relations["ClientIDClientName"]);

				int index = _ClientNames.Add( newClientName );

				return index;

			}

			/// <summary>
			/// Creates a new BCS.ClientSys.Biz.ClientName, adding it to the collection.
			/// </summary>
			/// <remarks>
			/// CommitAll() must be called to persist this to the database.
			/// </remarks>
			/// <returns>A new ClientName object</returns>
			public virtual BCS.ClientSys.Biz.ClientName NewClientName()
			{
				if ( _ClientNames == null ) refreshClientNames();

				BCS.ClientSys.Biz.ClientName newClientName = new BCS.ClientSys.Biz.ClientName(base.DataContext, base.DataSet.Tables["ClientName"].NewRow());
				base.DataSet.Tables["ClientName"].Rows.Add(newClientName.row);
				
				this.AddClientName(newClientName);

				return newClientName;
			}
	

	}



	/// <summary>
	/// Wraps a datarow, exposing it's richest features as properties and methods.
	/// </summary>
	public abstract class ClientAddressOrmTemplate : Business
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="dataContext">The DataManager this object should perform it's operations with</param>
		/// <param name="ROW">The underlying row that this object will wrap</param>
		internal ClientAddressOrmTemplate( DataManager dataContext, DataRow ROW) : base(dataContext) 
		{ 
			row = ROW; 
		}


		/// <summary>
		/// Gets and sets the property at the underlying row index.
		/// </summary>
		public object this[int index]
		{
			get
			{
				if ( this.row.IsNull(index) )
					return null;
				else
					return this.row[index];
			}
			set
			{
				if ( value == null )
					this.row[index] = DBNull.Value;
				else
					this.row[index] = value;
			}
		}

		/// <summary>
		/// Gets and sets the property by its name.
		/// </summary>
		/// <remarks>
		/// Do not use the underlying column name if it is different.
		/// </remarks>
		public object this[string property]
		{
			get
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanRead )
					return pi.GetValue(this, null);

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Get" + property,new System.Type[]{});
								
				if ( mi != null )
					return mi.Invoke(this, null);

				return null;
			}
			set
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanWrite )
				{
					pi.SetValue(this, value, null);
					return;
				}

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Set" + property,new System.Type[]{value.GetType()});
								
				if ( mi != null )
					mi.Invoke(this, new object[]{value});
			}
		}		

		
		/// <summary>
		/// Returns true if the underlying DataRow.RowState is marked as DataRowState.Deleted
		/// </summary>
		/// <returns>true if deleted</returns>
		public virtual bool IsDeleted()
		{
			return row.RowState == DataRowState.Deleted;
		}

		/// <summary>
		/// Gets the Id.
		/// </summary>
		/// <value>
		/// The underlying rows Id cell
		/// </value>
		public virtual System.Int32 Id 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return (System.Int32) row["Id"]; 
				else
					return (System.Int32) row["Id", DataRowVersion.Original];
				
				//System.Int32 
			} 
		} 
		/// <summary>
		/// Gets and Sets the ClientID.
		/// </summary>
		/// <value>
		/// The underlying rows ClientID cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlInt32 ClientID 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("ClientID"))
						return System.Data.SqlTypes.SqlInt32.Null;
					else
						return new System.Data.SqlTypes.SqlInt32((System.Int32)row["ClientID"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["ClientID"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlInt32.Null;
					else
						return new System.Data.SqlTypes.SqlInt32((System.Int32)row["ClientID", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["ClientID"] = DBNull.Value;
				else
					row["ClientID"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the StreetNumber.
		/// </summary>
		/// <value>
		/// The underlying rows StreetNumber cell
		/// </value>
		public virtual System.String StreetNumber 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("StreetNumber") ? null : (System.String) row["StreetNumber"]; 
				else
					return row.IsNull(row.Table.Columns["StreetNumber"], DataRowVersion.Original) ? null : (System.String) row["StreetNumber", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["StreetNumber"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the StreetName.
		/// </summary>
		/// <value>
		/// The underlying rows StreetName cell
		/// </value>
		public virtual System.String StreetName 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("StreetName") ? null : (System.String) row["StreetName"]; 
				else
					return row.IsNull(row.Table.Columns["StreetName"], DataRowVersion.Original) ? null : (System.String) row["StreetName", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["StreetName"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the StreetLine2.
		/// </summary>
		/// <value>
		/// The underlying rows StreetLine2 cell
		/// </value>
		public virtual System.String StreetLine2 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("StreetLine2") ? null : (System.String) row["StreetLine2"]; 
				else
					return row.IsNull(row.Table.Columns["StreetLine2"], DataRowVersion.Original) ? null : (System.String) row["StreetLine2", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["StreetLine2"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the City.
		/// </summary>
		/// <value>
		/// The underlying rows City cell
		/// </value>
		public virtual System.String City 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("City") ? null : (System.String) row["City"]; 
				else
					return row.IsNull(row.Table.Columns["City"], DataRowVersion.Original) ? null : (System.String) row["City", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["City"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the StateCode.
		/// </summary>
		/// <value>
		/// The underlying rows StateCode cell
		/// </value>
		public virtual System.String StateCode 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("StateCode") ? null : (System.String) row["StateCode"]; 
				else
					return row.IsNull(row.Table.Columns["StateCode"], DataRowVersion.Original) ? null : (System.String) row["StateCode", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["StateCode"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the ZipCode.
		/// </summary>
		/// <value>
		/// The underlying rows ZipCode cell
		/// </value>
		public virtual System.String ZipCode 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("ZipCode") ? null : (System.String) row["ZipCode"]; 
				else
					return row.IsNull(row.Table.Columns["ZipCode"], DataRowVersion.Original) ? null : (System.String) row["ZipCode", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["ZipCode"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the County.
		/// </summary>
		/// <value>
		/// The underlying rows County cell
		/// </value>
		public virtual System.String County 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("County") ? null : (System.String) row["County"]; 
				else
					return row.IsNull(row.Table.Columns["County"], DataRowVersion.Original) ? null : (System.String) row["County", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["County"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the CountryCode.
		/// </summary>
		/// <value>
		/// The underlying rows CountryCode cell
		/// </value>
		public virtual System.String CountryCode 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("CountryCode") ? null : (System.String) row["CountryCode"]; 
				else
					return row.IsNull(row.Table.Columns["CountryCode"], DataRowVersion.Original) ? null : (System.String) row["CountryCode", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["CountryCode"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the InBillingSystem.
		/// </summary>
		/// <value>
		/// The underlying rows InBillingSystem cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlBoolean InBillingSystem 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("InBillingSystem"))
						return System.Data.SqlTypes.SqlBoolean.Null;
					else
						return new System.Data.SqlTypes.SqlBoolean((System.Boolean)row["InBillingSystem"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["InBillingSystem"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlBoolean.Null;
					else
						return new System.Data.SqlTypes.SqlBoolean((System.Boolean)row["InBillingSystem", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["InBillingSystem"] = DBNull.Value;
				else
					row["InBillingSystem"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the FromLegacySystem.
		/// </summary>
		/// <value>
		/// The underlying rows FromLegacySystem cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlBoolean FromLegacySystem 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("FromLegacySystem"))
						return System.Data.SqlTypes.SqlBoolean.Null;
					else
						return new System.Data.SqlTypes.SqlBoolean((System.Boolean)row["FromLegacySystem"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["FromLegacySystem"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlBoolean.Null;
					else
						return new System.Data.SqlTypes.SqlBoolean((System.Boolean)row["FromLegacySystem", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["FromLegacySystem"] = DBNull.Value;
				else
					row["FromLegacySystem"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the OrderIndex.
		/// </summary>
		/// <value>
		/// The underlying rows OrderIndex cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlInt32 OrderIndex 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("OrderIndex"))
						return System.Data.SqlTypes.SqlInt32.Null;
					else
						return new System.Data.SqlTypes.SqlInt32((System.Int32)row["OrderIndex"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["OrderIndex"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlInt32.Null;
					else
						return new System.Data.SqlTypes.SqlInt32((System.Int32)row["OrderIndex", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["OrderIndex"] = DBNull.Value;
				else
					row["OrderIndex"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the EntryDt.
		/// </summary>
		/// <value>
		/// The underlying rows EntryDt cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlDateTime EntryDt 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("EntryDt"))
						return System.Data.SqlTypes.SqlDateTime.Null;
					else
						return new System.Data.SqlTypes.SqlDateTime((System.DateTime)row["EntryDt"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["EntryDt"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlDateTime.Null;
					else
						return new System.Data.SqlTypes.SqlDateTime((System.DateTime)row["EntryDt", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["EntryDt"] = DBNull.Value;
				else
					row["EntryDt"] = value.Value; 
 
			} 
		}
		
		/// <summary>
		/// Gets and Sets the parent BCS.ClientSys.Biz.Client.
		/// </summary>
		 
		public virtual BCS.ClientSys.Biz.Client Client
		{
			get
			{
				if (row.GetParentRow(base.DataSet.Relations["ClientIDClientAddress"]) != null)
					return new BCS.ClientSys.Biz.Client( DataContext, row.GetParentRow("ClientIDClientAddress"));
				else
					return null;
			}
			set
			{
				if ( value == null )
				{
					row.SetParentRow(null, base.DataSet.Relations["ClientIDClientAddress"] );
				}
				else
					row.SetParentRow( value.row, base.DataSet.Relations["ClientIDClientAddress"] );			
			}
		}
	
		 
		public virtual BCS.ClientSys.Biz.State State
		{
			get
			{
				if (row.IsNull("StateCode"))
					return null;
				else
					return DataContext.GetLookups().States.FindByCode( (System.String) row["StateCode"] );
			}
			set
			{
				row["StateCode"] = value.Code;
			}
		}

		 
		public virtual BCS.ClientSys.Biz.Country Country
		{
			get
			{
				if (row.IsNull("CountryCode"))
					return null;
				else
					return DataContext.GetLookups().Countrys.FindByCode( (System.String) row["CountryCode"] );
			}
			set
			{
				row["CountryCode"] = value.Code;
			}
		}


	}



	/// <summary>
	/// Wraps a datarow, exposing it's richest features as properties and methods.
	/// </summary>
	public abstract class ClientNameOrmTemplate : Business
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="dataContext">The DataManager this object should perform it's operations with</param>
		/// <param name="ROW">The underlying row that this object will wrap</param>
		internal ClientNameOrmTemplate( DataManager dataContext, DataRow ROW) : base(dataContext) 
		{ 
			row = ROW; 
		}


		/// <summary>
		/// Gets and sets the property at the underlying row index.
		/// </summary>
		public object this[int index]
		{
			get
			{
				if ( this.row.IsNull(index) )
					return null;
				else
					return this.row[index];
			}
			set
			{
				if ( value == null )
					this.row[index] = DBNull.Value;
				else
					this.row[index] = value;
			}
		}

		/// <summary>
		/// Gets and sets the property by its name.
		/// </summary>
		/// <remarks>
		/// Do not use the underlying column name if it is different.
		/// </remarks>
		public object this[string property]
		{
			get
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanRead )
					return pi.GetValue(this, null);

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Get" + property,new System.Type[]{});
								
				if ( mi != null )
					return mi.Invoke(this, null);

				return null;
			}
			set
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanWrite )
				{
					pi.SetValue(this, value, null);
					return;
				}

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Set" + property,new System.Type[]{value.GetType()});
								
				if ( mi != null )
					mi.Invoke(this, new object[]{value});
			}
		}		

		
		/// <summary>
		/// Returns true if the underlying DataRow.RowState is marked as DataRowState.Deleted
		/// </summary>
		/// <returns>true if deleted</returns>
		public virtual bool IsDeleted()
		{
			return row.RowState == DataRowState.Deleted;
		}

		/// <summary>
		/// Gets the Id.
		/// </summary>
		/// <value>
		/// The underlying rows Id cell
		/// </value>
		public virtual System.Int32 Id 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return (System.Int32) row["Id"]; 
				else
					return (System.Int32) row["Id", DataRowVersion.Original];
				
				//System.Int32 
			} 
		} 
		/// <summary>
		/// Gets and Sets the ClientID.
		/// </summary>
		/// <value>
		/// The underlying rows ClientID cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlInt32 ClientID 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("ClientID"))
						return System.Data.SqlTypes.SqlInt32.Null;
					else
						return new System.Data.SqlTypes.SqlInt32((System.Int32)row["ClientID"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["ClientID"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlInt32.Null;
					else
						return new System.Data.SqlTypes.SqlInt32((System.Int32)row["ClientID", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["ClientID"] = DBNull.Value;
				else
					row["ClientID"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the BusinessName.
		/// </summary>
		/// <value>
		/// The underlying rows BusinessName cell
		/// </value>
		public virtual System.String BusinessName 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("BusinessName") ? null : (System.String) row["BusinessName"]; 
				else
					return row.IsNull(row.Table.Columns["BusinessName"], DataRowVersion.Original) ? null : (System.String) row["BusinessName", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["BusinessName"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the InBillingSystem.
		/// </summary>
		/// <value>
		/// The underlying rows InBillingSystem cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlBoolean InBillingSystem 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("InBillingSystem"))
						return System.Data.SqlTypes.SqlBoolean.Null;
					else
						return new System.Data.SqlTypes.SqlBoolean((System.Boolean)row["InBillingSystem"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["InBillingSystem"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlBoolean.Null;
					else
						return new System.Data.SqlTypes.SqlBoolean((System.Boolean)row["InBillingSystem", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["InBillingSystem"] = DBNull.Value;
				else
					row["InBillingSystem"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the FromLegacySystem.
		/// </summary>
		/// <value>
		/// The underlying rows FromLegacySystem cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlBoolean FromLegacySystem 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("FromLegacySystem"))
						return System.Data.SqlTypes.SqlBoolean.Null;
					else
						return new System.Data.SqlTypes.SqlBoolean((System.Boolean)row["FromLegacySystem"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["FromLegacySystem"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlBoolean.Null;
					else
						return new System.Data.SqlTypes.SqlBoolean((System.Boolean)row["FromLegacySystem", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["FromLegacySystem"] = DBNull.Value;
				else
					row["FromLegacySystem"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the OrderIndex.
		/// </summary>
		/// <value>
		/// The underlying rows OrderIndex cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlInt32 OrderIndex 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("OrderIndex"))
						return System.Data.SqlTypes.SqlInt32.Null;
					else
						return new System.Data.SqlTypes.SqlInt32((System.Int32)row["OrderIndex"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["OrderIndex"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlInt32.Null;
					else
						return new System.Data.SqlTypes.SqlInt32((System.Int32)row["OrderIndex", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["OrderIndex"] = DBNull.Value;
				else
					row["OrderIndex"] = value.Value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the EntryDt.
		/// </summary>
		/// <value>
		/// The underlying rows EntryDt cell
		/// </value>
		public virtual System.Data.SqlTypes.SqlDateTime EntryDt 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
				{
					if (row.IsNull("EntryDt"))
						return System.Data.SqlTypes.SqlDateTime.Null;
					else
						return new System.Data.SqlTypes.SqlDateTime((System.DateTime)row["EntryDt"]);
				}
				else
				{
					if (row.IsNull(row.Table.Columns["EntryDt"], DataRowVersion.Original))
						return System.Data.SqlTypes.SqlDateTime.Null;
					else
						return new System.Data.SqlTypes.SqlDateTime((System.DateTime)row["EntryDt", DataRowVersion.Original]);
				}
 
			} 
			set
			{ 
				if ( value.IsNull ) 
					row["EntryDt"] = DBNull.Value;
				else
					row["EntryDt"] = value.Value; 
 
			} 
		}
		
		/// <summary>
		/// Gets and Sets the parent BCS.ClientSys.Biz.Client.
		/// </summary>
		 
		public virtual BCS.ClientSys.Biz.Client Client
		{
			get
			{
				if (row.GetParentRow(base.DataSet.Relations["ClientIDClientName"]) != null)
					return new BCS.ClientSys.Biz.Client( DataContext, row.GetParentRow("ClientIDClientName"));
				else
					return null;
			}
			set
			{
				if ( value == null )
				{
					row.SetParentRow(null, base.DataSet.Relations["ClientIDClientName"] );
				}
				else
					row.SetParentRow( value.row, base.DataSet.Relations["ClientIDClientName"] );			
			}
		}
	

	}



	/// <summary>
	/// Wraps a datarow, exposing it's richest features as properties and methods.
	/// </summary>
	public abstract class CompanyOrmTemplate : Business
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="dataContext">The DataManager this object should perform it's operations with</param>
		/// <param name="ROW">The underlying row that this object will wrap</param>
		internal CompanyOrmTemplate( DataManager dataContext, DataRow ROW) : base(dataContext) 
		{ 
			row = ROW; 
		}


		/// <summary>
		/// Gets and sets the property at the underlying row index.
		/// </summary>
		public object this[int index]
		{
			get
			{
				if ( this.row.IsNull(index) )
					return null;
				else
					return this.row[index];
			}
			set
			{
				if ( value == null )
					this.row[index] = DBNull.Value;
				else
					this.row[index] = value;
			}
		}

		/// <summary>
		/// Gets and sets the property by its name.
		/// </summary>
		/// <remarks>
		/// Do not use the underlying column name if it is different.
		/// </remarks>
		public object this[string property]
		{
			get
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanRead )
					return pi.GetValue(this, null);

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Get" + property,new System.Type[]{});
								
				if ( mi != null )
					return mi.Invoke(this, null);

				return null;
			}
			set
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanWrite )
				{
					pi.SetValue(this, value, null);
					return;
				}

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Set" + property,new System.Type[]{value.GetType()});
								
				if ( mi != null )
					mi.Invoke(this, new object[]{value});
			}
		}		

		
		/// <summary>
		/// Returns true if the underlying DataRow.RowState is marked as DataRowState.Deleted
		/// </summary>
		/// <returns>true if deleted</returns>
		public virtual bool IsDeleted()
		{
			return row.RowState == DataRowState.Deleted;
		}

		/// <summary>
		/// Gets the Id.
		/// </summary>
		/// <value>
		/// The underlying rows Id cell
		/// </value>
		public virtual System.Int32 Id 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return (System.Int32) row["Id"]; 
				else
					return (System.Int32) row["Id", DataRowVersion.Original];
				
				//System.Int32 
			} 
		} 
		/// <summary>
		/// Gets and Sets the CompanyName.
		/// </summary>
		/// <value>
		/// The underlying rows CompanyName cell
		/// </value>
		public virtual System.String CompanyName 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("CompanyName") ? null : (System.String) row["CompanyName"]; 
				else
					return row.IsNull(row.Table.Columns["CompanyName"], DataRowVersion.Original) ? null : (System.String) row["CompanyName", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["CompanyName"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the Initials.
		/// </summary>
		/// <value>
		/// The underlying rows Initials cell
		/// </value>
		public virtual System.String Initials 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("Initials") ? null : (System.String) row["Initials"]; 
				else
					return row.IsNull(row.Table.Columns["Initials"], DataRowVersion.Original) ? null : (System.String) row["Initials", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["Initials"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the CompanyNumber.
		/// </summary>
		/// <value>
		/// The underlying rows CompanyNumber cell
		/// </value>
		public virtual System.String CompanyNumber 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("CompanyNumber") ? null : (System.String) row["CompanyNumber"]; 
				else
					return row.IsNull(row.Table.Columns["CompanyNumber"], DataRowVersion.Original) ? null : (System.String) row["CompanyNumber", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["CompanyNumber"] = value; 
 
			} 
		}
		private BCS.ClientSys.Biz.ClientCollection _Clients = null;
	
		/// <summary>
		/// Refreshes the collection of Clients from the underlying dataset
		/// </summary>
		internal void refreshClients()
		{
			if (_Clients == null) _Clients = new BCS.ClientSys.Biz.ClientCollection();
			
			((IList)_Clients).Clear();

			DataRow[] cr = row.GetChildRows("CompanyIdClient");
			foreach( DataRow chld in cr)
			{
				BCS.ClientSys.Biz.Client obj = new BCS.ClientSys.Biz.Client(base.DataContext, chld);
				_Clients.Add( obj );
			}
			
			// add after, so that events wont be fired
			_Clients.Parent = this;
		}
		
	}



	/// <summary>
	/// Wraps a datarow, exposing it's richest features as properties and methods.
	/// </summary>
	public abstract class CountryOrmTemplate : Business
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="dataContext">The DataManager this object should perform it's operations with</param>
		/// <param name="ROW">The underlying row that this object will wrap</param>
		internal CountryOrmTemplate( DataManager dataContext, DataRow ROW) : base(dataContext) 
		{ 
			row = ROW; 
		}


		/// <summary>
		/// Gets and sets the property at the underlying row index.
		/// </summary>
		public object this[int index]
		{
			get
			{
				if ( this.row.IsNull(index) )
					return null;
				else
					return this.row[index];
			}
			set
			{
				if ( value == null )
					this.row[index] = DBNull.Value;
				else
					this.row[index] = value;
			}
		}

		/// <summary>
		/// Gets and sets the property by its name.
		/// </summary>
		/// <remarks>
		/// Do not use the underlying column name if it is different.
		/// </remarks>
		public object this[string property]
		{
			get
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanRead )
					return pi.GetValue(this, null);

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Get" + property,new System.Type[]{});
								
				if ( mi != null )
					return mi.Invoke(this, null);

				return null;
			}
			set
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanWrite )
				{
					pi.SetValue(this, value, null);
					return;
				}

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Set" + property,new System.Type[]{value.GetType()});
								
				if ( mi != null )
					mi.Invoke(this, new object[]{value});
			}
		}		

		
		/// <summary>
		/// Returns true if the underlying DataRow.RowState is marked as DataRowState.Deleted
		/// </summary>
		/// <returns>true if deleted</returns>
		public virtual bool IsDeleted()
		{
			return row.RowState == DataRowState.Deleted;
		}

		/// <summary>
		/// Gets and Sets the Code.
		/// </summary>
		/// <value>
		/// The underlying rows Code cell
		/// </value>
		public virtual System.String Code 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("Code") ? null : (System.String) row["Code"]; 
				else
					return row.IsNull(row.Table.Columns["Code"], DataRowVersion.Original) ? null : (System.String) row["Code", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["Code"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the CountryName.
		/// </summary>
		/// <value>
		/// The underlying rows CountryName cell
		/// </value>
		public virtual System.String CountryName 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("CountryName") ? null : (System.String) row["CountryName"]; 
				else
					return row.IsNull(row.Table.Columns["CountryName"], DataRowVersion.Original) ? null : (System.String) row["CountryName", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["CountryName"] = value; 
 
			} 
		}
		private BCS.ClientSys.Biz.ClientAddressCollection _ClientAddresss = null;
	
		/// <summary>
		/// Refreshes the collection of ClientAddresss from the underlying dataset
		/// </summary>
		internal void refreshClientAddresss()
		{
			if (_ClientAddresss == null) _ClientAddresss = new BCS.ClientSys.Biz.ClientAddressCollection();
			
			((IList)_ClientAddresss).Clear();

			DataRow[] cr = row.GetChildRows("CountryCodeClientAddress");
			foreach( DataRow chld in cr)
			{
				BCS.ClientSys.Biz.ClientAddress obj = new BCS.ClientSys.Biz.ClientAddress(base.DataContext, chld);
				_ClientAddresss.Add( obj );
			}
			
			// add after, so that events wont be fired
			_ClientAddresss.Parent = this;
		}
		
	}



	/// <summary>
	/// Wraps a datarow, exposing it's richest features as properties and methods.
	/// </summary>
	public abstract class StateOrmTemplate : Business
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="dataContext">The DataManager this object should perform it's operations with</param>
		/// <param name="ROW">The underlying row that this object will wrap</param>
		internal StateOrmTemplate( DataManager dataContext, DataRow ROW) : base(dataContext) 
		{ 
			row = ROW; 
		}


		/// <summary>
		/// Gets and sets the property at the underlying row index.
		/// </summary>
		public object this[int index]
		{
			get
			{
				if ( this.row.IsNull(index) )
					return null;
				else
					return this.row[index];
			}
			set
			{
				if ( value == null )
					this.row[index] = DBNull.Value;
				else
					this.row[index] = value;
			}
		}

		/// <summary>
		/// Gets and sets the property by its name.
		/// </summary>
		/// <remarks>
		/// Do not use the underlying column name if it is different.
		/// </remarks>
		public object this[string property]
		{
			get
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanRead )
					return pi.GetValue(this, null);

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Get" + property,new System.Type[]{});
								
				if ( mi != null )
					return mi.Invoke(this, null);

				return null;
			}
			set
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanWrite )
				{
					pi.SetValue(this, value, null);
					return;
				}

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Set" + property,new System.Type[]{value.GetType()});
								
				if ( mi != null )
					mi.Invoke(this, new object[]{value});
			}
		}		

		
		/// <summary>
		/// Returns true if the underlying DataRow.RowState is marked as DataRowState.Deleted
		/// </summary>
		/// <returns>true if deleted</returns>
		public virtual bool IsDeleted()
		{
			return row.RowState == DataRowState.Deleted;
		}

		/// <summary>
		/// Gets and Sets the Code.
		/// </summary>
		/// <value>
		/// The underlying rows Code cell
		/// </value>
		public virtual System.String Code 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("Code") ? null : (System.String) row["Code"]; 
				else
					return row.IsNull(row.Table.Columns["Code"], DataRowVersion.Original) ? null : (System.String) row["Code", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["Code"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the StateName.
		/// </summary>
		/// <value>
		/// The underlying rows StateName cell
		/// </value>
		public virtual System.String StateName 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("StateName") ? null : (System.String) row["StateName"]; 
				else
					return row.IsNull(row.Table.Columns["StateName"], DataRowVersion.Original) ? null : (System.String) row["StateName", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["StateName"] = value; 
 
			} 
		}
		private BCS.ClientSys.Biz.ClientAddressCollection _ClientAddresss = null;
	
		/// <summary>
		/// Refreshes the collection of ClientAddresss from the underlying dataset
		/// </summary>
		internal void refreshClientAddresss()
		{
			if (_ClientAddresss == null) _ClientAddresss = new BCS.ClientSys.Biz.ClientAddressCollection();
			
			((IList)_ClientAddresss).Clear();

			DataRow[] cr = row.GetChildRows("StateCodeClientAddress");
			foreach( DataRow chld in cr)
			{
				BCS.ClientSys.Biz.ClientAddress obj = new BCS.ClientSys.Biz.ClientAddress(base.DataContext, chld);
				_ClientAddresss.Add( obj );
			}
			
			// add after, so that events wont be fired
			_ClientAddresss.Parent = this;
		}
		
	}



	/// <summary>
	/// Wraps a datarow, exposing it's richest features as properties and methods.
	/// </summary>
	public abstract class TaxTypeOrmTemplate : Business
	{
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="dataContext">The DataManager this object should perform it's operations with</param>
		/// <param name="ROW">The underlying row that this object will wrap</param>
		internal TaxTypeOrmTemplate( DataManager dataContext, DataRow ROW) : base(dataContext) 
		{ 
			row = ROW; 
		}


		/// <summary>
		/// Gets and sets the property at the underlying row index.
		/// </summary>
		public object this[int index]
		{
			get
			{
				if ( this.row.IsNull(index) )
					return null;
				else
					return this.row[index];
			}
			set
			{
				if ( value == null )
					this.row[index] = DBNull.Value;
				else
					this.row[index] = value;
			}
		}

		/// <summary>
		/// Gets and sets the property by its name.
		/// </summary>
		/// <remarks>
		/// Do not use the underlying column name if it is different.
		/// </remarks>
		public object this[string property]
		{
			get
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanRead )
					return pi.GetValue(this, null);

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Get" + property,new System.Type[]{});
								
				if ( mi != null )
					return mi.Invoke(this, null);

				return null;
			}
			set
			{
				System.Reflection.PropertyInfo pi = this.GetType().GetProperty(property);
				
				if ( pi != null && pi.CanWrite )
				{
					pi.SetValue(this, value, null);
					return;
				}

				System.Reflection.MethodInfo mi = this.GetType().GetMethod("Set" + property,new System.Type[]{value.GetType()});
								
				if ( mi != null )
					mi.Invoke(this, new object[]{value});
			}
		}		

		
		/// <summary>
		/// Returns true if the underlying DataRow.RowState is marked as DataRowState.Deleted
		/// </summary>
		/// <returns>true if deleted</returns>
		public virtual bool IsDeleted()
		{
			return row.RowState == DataRowState.Deleted;
		}

		/// <summary>
		/// Gets and Sets the Code.
		/// </summary>
		/// <value>
		/// The underlying rows Code cell
		/// </value>
		public virtual System.String Code 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("Code") ? null : (System.String) row["Code"]; 
				else
					return row.IsNull(row.Table.Columns["Code"], DataRowVersion.Original) ? null : (System.String) row["Code", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["Code"] = value; 
 
			} 
		}
		/// <summary>
		/// Gets and Sets the TaxTypeDescription.
		/// </summary>
		/// <value>
		/// The underlying rows TaxTypeDescription cell
		/// </value>
		public virtual System.String TaxTypeDescription 
		{ 
			get
			{ 
				if (row.RowState != DataRowState.Deleted)
					return row.IsNull("TaxTypeDescription") ? null : (System.String) row["TaxTypeDescription"]; 
				else
					return row.IsNull(row.Table.Columns["TaxTypeDescription"], DataRowVersion.Original) ? null : (System.String) row["TaxTypeDescription", DataRowVersion.Original]; 

				//System.String 
			} 
			set
			{ 
				row["TaxTypeDescription"] = value; 
 
			} 
		}
		private BCS.ClientSys.Biz.ClientCollection _Clients = null;
	
		/// <summary>
		/// Refreshes the collection of Clients from the underlying dataset
		/// </summary>
		internal void refreshClients()
		{
			if (_Clients == null) _Clients = new BCS.ClientSys.Biz.ClientCollection();
			
			((IList)_Clients).Clear();

			DataRow[] cr = row.GetChildRows("TaxTypeCodeClient");
			foreach( DataRow chld in cr)
			{
				BCS.ClientSys.Biz.Client obj = new BCS.ClientSys.Biz.Client(base.DataContext, chld);
				_Clients.Add( obj );
			}
			
			// add after, so that events wont be fired
			_Clients.Parent = this;
		}
		
	}


}