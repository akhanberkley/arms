using System;

namespace BCS.Core
{
	/// <summary>
	/// Summary description for Core.
	/// </summary>
	public class Core
	{
		private Core() {}

		/// <summary>
		/// The key used for a TSHAK.Component.SecureQueryString
		/// </summary>
		/// <remarks>
		/// WARNING: Any sensitive information, including this key, 
		/// should be stored in a config file that is properly ACL'd
		/// and protected with EFS (Encrypted File System) or DPAPI 
		/// (Data Protection API) or stored in another secure location.
		/// </remarks>
		public static readonly byte[] Key = new byte[] {0,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6};

		/// <summary>
		/// Convert Stream to String
		/// </summary>
		/// <param name="theStream"></param>
		/// <returns></returns>
		public static string ConvertStreamToString(System.IO.Stream theStream) 
		{
			string theString="";
			theStream.Position=0;

			using (System.IO.StreamReader sr = new System.IO.StreamReader(theStream) ) 
			{
				theString = sr.ReadToEnd();
				// Close and clean up the StreamReader
				sr.Close();
			}
            
			return theString;
		}
	}
}
