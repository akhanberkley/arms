using System.Collections.Generic;
using System.Linq;
using BCS.DAL;
using BOA.Util.Extensions;
using System.Data.Objects;

namespace BCS.Core.Clearance
{
    /// <summary>
    /// 
    /// </summary>
    public class LookupsEF
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public string GetPolicySystems(string companyNumber)
        {
            List<DAL.PolicySystem> nc = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                nc = bc.PolicySystem.Where(p => p.CompanyNumber.CompanyNumber1 == companyNumber).ToList();
            }


            BizObjects.PolicySystemList stl = new BCS.Core.Clearance.BizObjects.PolicySystemList();
            BizObjects.PolicySystem[] list = new BCS.Core.Clearance.BizObjects.PolicySystem[nc.Count];

            for (int i = 0; i < nc.Count; i++)
            {
                BizObjects.PolicySystem st = new BCS.Core.Clearance.BizObjects.PolicySystem();
                st.Abbreviation = nc[i].Abbreviation;
                st.AssociatedCompanyNumberId = nc[i].AssociatedCompanyNumberId;
                st.Id = nc[i].Id;
                st.PropertyPolicySystem = nc[i].PolicySystem1;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public string GetPolicySystem(int id)
        {           
            DAL.PolicySystem nc = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                nc = bc.PolicySystem.Where(p => p.Id == id).SingleOrDefault();
            }
            if (nc == null)
                return null;

            BizObjects.PolicySystem st = new BCS.Core.Clearance.BizObjects.PolicySystem();
            st.Abbreviation = nc.Abbreviation;
            st.AssociatedCompanyNumberId = nc.AssociatedCompanyNumberId;
            st.Id = nc.Id;
            st.PropertyPolicySystem = nc.PolicySystem1;

            return XML.Serializer.SerializeAsXml(st);
        }

        /// <summary>
        /// 
        /// </summary>        
        public static string GetCompanyCodeTypesByCompanyNumber(string companyNo)
        {
            BizObjects.CodeTypeList sd = new BCS.Core.Clearance.BizObjects.CodeTypeList();
            //sd.CompanyNumber = companyNo;

            //// get companies by company number
            //Biz.DataManager dm = Common.GetDataManager();
            //dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNo);
            //Biz.CompanyNumberCollection cnums = dm.GetCompanyNumberCollection();

            List<CompanyNumberCodeType> ctypes = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                ctypes = bc.CompanyNumberCodeType.Where(c => c.CompanyNumber.CompanyNumber1 == companyNo).ToList();
            }

            BizObjects.CodeType[] list = new BCS.Core.Clearance.BizObjects.CodeType[ctypes.Count];
            for (int i = 0; i < ctypes.Count; i++)
            {
                BizObjects.CodeType type = new BCS.Core.Clearance.BizObjects.CodeType();
                type.CodeName = ctypes[i].CodeName;
                type.CompanyNumberId = ctypes[i].CompanyNumberId;
                type.DefaultCompanyNumberCodeId = ctypes[i].DefaultCompanyNumberCodeId.ToInt();
                type.DisplayOrder = ctypes[i].DisplayOrder;
                type.Id = ctypes[i].Id;
                type.ClientSpecific = ctypes[i].ClientSpecific;
                list[i] = type;
            }

            sd.List = list;
            
            string xml = XML.Serializer.SerializeAsXml(sd);
            return xml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNoId"></param>
        /// <param name="clientSpecific"></param>
        /// <returns></returns>       
        public static BizObjects.CodeTypeList GetCompanyCodeTypesByCompanyNumberId(string companyNoId, bool clientSpecific)
        {
            BizObjects.CodeTypeList sd = new BCS.Core.Clearance.BizObjects.CodeTypeList();


            List<DAL.CompanyNumberCodeType> ctypes = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                ctypes = bc.CompanyNumberCodeType.Where(c => c.CompanyNumber.CompanyId == companyNoId.ToInt() && c.ClientSpecific == clientSpecific).ToList();
            }

            if (ctypes.Count > 0)
            {
                BizObjects.CodeType[] list = new BCS.Core.Clearance.BizObjects.CodeType[ctypes.Count];
                for (int i = 0; i < ctypes.Count; i++)
                {
                    BizObjects.CodeType type = new BCS.Core.Clearance.BizObjects.CodeType();
                    type.CodeName = ctypes[i].CodeName;
                    type.CompanyNumberId = ctypes[i].CompanyNumberId;
                    type.DefaultCompanyNumberCodeId = ctypes[i].DefaultCompanyNumberCodeId.ToInt();
                    type.DisplayOrder = ctypes[i].DisplayOrder;
                    type.Id = ctypes[i].Id;
                    type.ClientSpecific = ctypes[i].ClientSpecific;
                    type.Required = ctypes[i].Required;
                    list[i] = type;
                }

                sd.List = list;
            }
            return sd;
        }

        /// <summary>
        /// 
        /// </summary>        
        public static string GetCompanyCodesByCompanyCodeType(string codeName, string companyNo)
        {
            BizObjects.CompanyNumberCodeList sd = new BCS.Core.Clearance.BizObjects.CompanyNumberCodeList();


            List<DAL.CompanyNumberCode> codes = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                codes = bc.CompanyNumberCode.Where(c => c.CompanyNumberCodeType.CodeName == codeName && c.CompanyNumber.CompanyNumber1 == companyNo).ToList();
            }

           
            BizObjects.CompanyNumberCode[] list = new BCS.Core.Clearance.BizObjects.CompanyNumberCode[codes.Count];
            for (int i = 0; i < codes.Count; i++)
            {
                BizObjects.CompanyNumberCode code = new BCS.Core.Clearance.BizObjects.CompanyNumberCode();
                code.Code = codes[i].Code;
                code.CompanyCodeTypeId = codes[i].CompanyCodeTypeId;
                code.CompanyNumberId = codes[i].CompanyNumberId;
                code.Description = codes[i].Description;
                code.Id = codes[i].Id;
                list[i] = code;
            }

            sd.List = list;
        

            sd.CodeName = codeName;
            sd.CompanyNumber = companyNo;

            string xml = XML.Serializer.SerializeAsXml(sd);
            return xml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codeName"></param>
        /// <param name="companyNoId"></param>
        /// <param name="clientSpecific"></param>
        /// <returns></returns>
        public static BizObjects.CompanyNumberCodeList GetCompanyCodesByCompanyCodeTypeByCompanyNumberId(string codeName, string companyNoId, bool clientSpecific)
        {
            BizObjects.CompanyNumberCodeList sd = new BCS.Core.Clearance.BizObjects.CompanyNumberCodeList();
            // get code type ids by code name
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.CodeName, codeName,
                OrmLib.MatchType.Exact).And(Biz.JoinPath.CompanyNumberCodeType.CompanyNumber.Columns.Id, companyNoId);
            if (clientSpecific)
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.ClientSpecific, clientSpecific);
            else
            {
                Biz.DataManager.CriteriaGroup clientspecific = new OrmLib.DataManagerBase.CriteriaGroup();
                clientspecific.And(Biz.JoinPath.CompanyNumberCodeType.Columns.ClientSpecific, false).Or(
                                Biz.JoinPath.CompanyNumberCodeType.Columns.ClientSpecific, null);
                dm.QueryCriteria.And(clientspecific);
            }
            Biz.CompanyNumberCodeTypeCollection ctypes = dm.GetCompanyNumberCodeTypeCollection().SortByDisplayOrder(OrmLib.SortDirection.Ascending);

            if (ctypes.Count > 0)
            {
                // get company codes by code types retrieved above
                dm.QueryCriteria.Clear();
                OrmLib.DataManagerBase.CriteriaGroup criteria = new OrmLib.DataManagerBase.CriteriaGroup();
                foreach (Biz.CompanyNumberCodeType ctype in ctypes)
                {
                    criteria.Or(Biz.JoinPath.CompanyNumberCode.Columns.CompanyCodeTypeId, ctype.Id);
                }
                dm.QueryCriteria = criteria;
                Biz.CompanyNumberCodeCollection codes = dm.GetCompanyNumberCodeCollection();                
                BizObjects.CompanyNumberCode[] list = new BCS.Core.Clearance.BizObjects.CompanyNumberCode[codes.Count];
                for (int i = 0; i < codes.Count; i++)
                {
                    BizObjects.CompanyNumberCode code = new BCS.Core.Clearance.BizObjects.CompanyNumberCode();
                    code.Code = codes[i].Code;
                    code.CompanyCodeTypeId = codes[i].CompanyCodeTypeId.Value;
                    code.CompanyNumberId = codes[i].CompanyNumberId.Value;                    
                    code.Description = codes[i].Description;
                    code.Id = codes[i].Id;
                    list[i] = code;
                }

                sd.List = list;
            }

            sd.CodeName = codeName;
            
            return sd;
        }

        /// <summary>
        /// method returns available submission types
        /// </summary>
        /// <returns></returns>
        public static string GetSubmissionTypes()
        {
            Biz.DataManager dm = Common.GetDataManager();
            Biz.SubmissionTypeCollection types = dm.GetSubmissionTypeCollection();

            BizObjects.SubmissionTypeList stl = new BCS.Core.Clearance.BizObjects.SubmissionTypeList();
            BizObjects.SubmissionType[] list = new BCS.Core.Clearance.BizObjects.SubmissionType[types.Count];

            for (int i = 0; i < types.Count; i++)
            {
                BizObjects.SubmissionType type = new BCS.Core.Clearance.BizObjects.SubmissionType();
                type.Abbreviation = types[i].Abbreviation;
                type.Id = types[i].Id;
                type.TypeName = types[i].TypeName;
                list[i] = type;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// method returns available submission types by company number
        /// </summary>
        /// <returns></returns>
        public static string GetSubmissionTypes(string companyNumber)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.SubmissionType.CompanyNumberSubmissionType.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.SubmissionTypeCollection types = dm.GetSubmissionTypeCollection();

            BizObjects.SubmissionTypeList stl = new BCS.Core.Clearance.BizObjects.SubmissionTypeList();
            BizObjects.SubmissionType[] list = new BCS.Core.Clearance.BizObjects.SubmissionType[types.Count];

            for (int i = 0; i < types.Count; i++)
            {
                BizObjects.SubmissionType type = new BCS.Core.Clearance.BizObjects.SubmissionType();
                type.Abbreviation = types[i].Abbreviation;
                type.Id = types[i].Id;
                type.TypeName = types[i].TypeName;
                list[i] = type;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// method returns available submission statuses
        /// </summary>
        /// <returns></returns>
        public static string GetSubmissionStatuses()
        {
            Biz.DataManager dm = Common.GetDataManager();
            Biz.SubmissionStatusCollection ss = dm.GetSubmissionStatusCollection();

            BizObjects.SubmissionStatusList stl = new BCS.Core.Clearance.BizObjects.SubmissionStatusList();
            BizObjects.SubmissionStatus[] list = new BCS.Core.Clearance.BizObjects.SubmissionStatus[ss.Count];

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.SubmissionStatus st = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
                st.Id = ss[i].Id;
                st.StatusCode = ss[i].StatusCode;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static string GetSubmissionStatuses(string companyNumber)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatus.CompanyNumberSubmissionStatus.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.SubmissionStatusCollection ss = dm.GetSubmissionStatusCollection();

            BizObjects.SubmissionStatusList stl = new BCS.Core.Clearance.BizObjects.SubmissionStatusList();
            BizObjects.SubmissionStatus[] list = new BCS.Core.Clearance.BizObjects.SubmissionStatus[ss.Count];

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.SubmissionStatus st = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
                st.Id = ss[i].Id;
                st.StatusCode = ss[i].StatusCode;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetStates()
        {
            Biz.DataManager dm = Common.GetDataManager();
            Biz.StateCollection ss = dm.GetStateCollection();

            BizObjects.StateList stl = new BCS.Core.Clearance.BizObjects.StateList();
            BizObjects.State[] list = new BCS.Core.Clearance.BizObjects.State[ss.Count];

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.State st = new BCS.Core.Clearance.BizObjects.State();
                st.Id = ss[i].Id;
                st.StateName = ss[i].StateName;
                st.Abbreviation = ss[i].Abbreviation;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetSuffixes()
        {
            Biz.DataManager dm = Common.GetDataManager();
            Biz.NameSuffixTitleCollection ss = dm.GetNameSuffixTitleCollection();

            BizObjects.SuffixList stl = new BCS.Core.Clearance.BizObjects.SuffixList();
            BizObjects.Suffix[] list = new BCS.Core.Clearance.BizObjects.Suffix[ss.Count];

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.Suffix st = new BCS.Core.Clearance.BizObjects.Suffix();
                st.Id = ss[i].Id;
                st.SuffixTitle = ss[i].SuffixTitle;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetPrefixes()
        {
            Biz.DataManager dm = Common.GetDataManager();
            Biz.NamePrefixCollection ss = dm.GetNamePrefixCollection();

            BizObjects.PrefixList stl = new BCS.Core.Clearance.BizObjects.PrefixList();
            BizObjects.Prefix[] list = new BCS.Core.Clearance.BizObjects.Prefix[ss.Count];

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.Prefix st = new BCS.Core.Clearance.BizObjects.Prefix();
                st.Id = ss[i].Id;
                st.PrefixTitle = ss[i].Prefix;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetAddressTypes()
        {
            Biz.Lookups lookups = Common.GetLookupManager();
            Biz.AddressTypeCollection ats = lookups.AddressTypes;

            BizObjects.AddressTypeList stl = new BCS.Core.Clearance.BizObjects.AddressTypeList();
            BizObjects.AddressType[] list = new BCS.Core.Clearance.BizObjects.AddressType[ats.Count];

            for (int i = 0; i < ats.Count; i++)
            {
                BizObjects.AddressType st = new BCS.Core.Clearance.BizObjects.AddressType();
                st.Id = ats[i].Id;
                st.Type = ats[i].PropertyAddressType;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static string GetAgencies(string companyNumber)
        {
            BizObjects.AgencyList stl = new BCS.Core.Clearance.BizObjects.AgencyList();
            stl.CompanyNumber = companyNumber;

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();
            if (cnum != null)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.CompanyNumberId, cnum.Id);

                Biz.AgencyCollection ss = dm.GetAgencyCollection(Biz.FetchPath.Agency.AgencyLicensedState.State);

                if (ss.Count > 0)
                {
                    BizObjects.Agency[] list = new BCS.Core.Clearance.BizObjects.Agency[ss.Count];

                    for (int i = 0; i < ss.Count; i++)
                    {
                        BizObjects.Agency st = new BCS.Core.Clearance.BizObjects.Agency();
                        st.Active = ss[i].Active.Value;
                        st.Address = ss[i].Address;
                        st.AgencyName = ss[i].AgencyName;
                        st.AgencyNumber = ss[i].AgencyNumber;
                        st.BranchId = ss[i].BranchId;
                        st.City = ss[i].City;
                        st.CompanyNumberId = ss[i].CompanyNumberId.Value;
                        st.ContactPerson = ss[i].ContactPerson;
                        st.Email = ss[i].Email;
                        st.EntryBy = ss[i].EntryBy;
                        st.EntryDate = ss[i].EntryDt.Value;
                        st.Fax = ss[i].Fax.IsNull ? decimal.Zero : ss[i].Fax.Value;
                        st.FEIN = ss[i].FEIN;
                        st.Id = ss[i].Id;
                        st.ImageFax = ss[i].ImageFax;
                        st.ModifiedBy = ss[i].ModifiedBy;
                        st.ModifiedDt = ss[i].ModifiedDt.IsNull ? System.DateTime.MinValue : ss[i].ModifiedDt.Value;
                        st.Phone = ss[i].Phone.IsNull ? decimal.Zero : ss[i].Phone.Value;
                        st.ReferralAgencyNumber = ss[i].ReferralAgencyNumber;
                        st.ReferralDate = ss[i].ReferralDate.IsNull ? System.DateTime.MinValue : ss[i].ReferralDate.Value;
                        st.State = ss[i].State;
                        st.Zip = ss[i].Zip;
                        st.CaptiveBroker = ss[i].CaptiveBroker;
                        
                        if (ss[i].AgencyLicensedStates.Count > 0)
                        {
                            st.LicensedStatesList = new BCS.Core.Clearance.BizObjects.StateList();
                            st.LicensedStatesList.List = new BCS.Core.Clearance.BizObjects.State[ss[i].AgencyLicensedStates.Count];
                            for (int j = 0; j < ss[i].AgencyLicensedStates.Count; j++)
                            {
                                st.LicensedStatesList.List[j] = new BCS.Core.Clearance.BizObjects.State();
                                st.LicensedStatesList.List[j].Abbreviation = ss[i].AgencyLicensedStates[j].State.Abbreviation;
                                st.LicensedStatesList.List[j].Id = ss[i].AgencyLicensedStates[j].StateId.Value;
                                st.LicensedStatesList.List[j].StateName = ss[i].AgencyLicensedStates[j].State.StateName;
                            }
                        }

                        list[i] = st;
                    }
                    stl.List = list;
                }
            }

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <param name="cancelDate"></param>
        /// <returns></returns>
        public static string GetAgencies(int companyNumberId, System.DateTime cancelDate)
        {
            BizObjects.AgencyList stl = new BCS.Core.Clearance.BizObjects.AgencyList();
            
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.CompanyNumber.Columns.Id, companyNumberId);
            dm = AddCancelCriteria(dm, cancelDate);

            Biz.AgencyCollection ss = dm.GetAgencyCollection(Biz.FetchPath.Agency.CompanyNumber, Biz.FetchPath.Agency.AgencyLicensedState.State);
            
            if (ss.Count > 0)
            {
                stl.CompanyNumber = ss[0].CompanyNumber.PropertyCompanyNumber;
                BizObjects.Agency[] list = new BCS.Core.Clearance.BizObjects.Agency[ss.Count];

                for (int i = 0; i < ss.Count; i++)
                {
                    BizObjects.Agency st = new BCS.Core.Clearance.BizObjects.Agency();
                    st.Active = ss[i].Active.Value;
                    st.Address = ss[i].Address;
                    st.AgencyName = ss[i].AgencyName;
                    st.AgencyNumber = ss[i].AgencyNumber;
                    st.BranchId = ss[i].BranchId;
                    st.City = ss[i].City;
                    st.CompanyNumberId = ss[i].CompanyNumberId.Value;
                    st.ContactPerson = ss[i].ContactPerson;
                    st.Email = ss[i].Email;
                    st.EntryBy = ss[i].EntryBy;
                    st.EntryDate = ss[i].EntryDt.Value;
                    st.Fax = ss[i].Fax.IsNull ? decimal.Zero : ss[i].Fax.Value;
                    st.FEIN = ss[i].FEIN;
                    st.Id = ss[i].Id;
                    st.ImageFax = ss[i].ImageFax;
                    st.ModifiedBy = ss[i].ModifiedBy;
                    st.ModifiedDt = ss[i].ModifiedDt.IsNull ? System.DateTime.MinValue : ss[i].ModifiedDt.Value;
                    st.Phone = ss[i].Phone.IsNull ? decimal.Zero : ss[i].Phone.Value;
                    st.ReferralAgencyNumber = ss[i].ReferralAgencyNumber;
                    st.ReferralDate = ss[i].ReferralDate.IsNull ? System.DateTime.MinValue : ss[i].ReferralDate.Value;
                    st.State = ss[i].State;
                    st.Zip = ss[i].Zip;

                    if (ss[i].AgencyLicensedStates.Count > 0)
                    {
                        st.LicensedStatesList = new BCS.Core.Clearance.BizObjects.StateList();
                        st.LicensedStatesList.List = new BCS.Core.Clearance.BizObjects.State[ss[i].AgencyLicensedStates.Count];
                        for (int j = 0; j < ss[i].AgencyLicensedStates.Count; j++)
                        {
                            st.LicensedStatesList.List[j] = new BCS.Core.Clearance.BizObjects.State();
                            st.LicensedStatesList.List[j].Abbreviation = ss[i].AgencyLicensedStates[j].State.Abbreviation;
                            st.LicensedStatesList.List[j].Id = ss[i].AgencyLicensedStates[j].StateId.Value;
                            st.LicensedStatesList.List[j].StateName = ss[i].AgencyLicensedStates[j].State.StateName;
                        }
                    }

                    list[i] = st;
                }
                stl.List = list;
            }

            return XML.Serializer.SerializeAsXml(stl);
        }
        private static Biz.DataManager AddCancelCriteria(Biz.DataManager dm, System.DateTime cancelDate)
        {
            if (cancelDate == System.DateTime.MinValue)
                cancelDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            Biz.DataManager.CriteriaGroup c = new OrmLib.DataManagerBase.CriteriaGroup();
            c.And(Biz.JoinPath.Agency.Columns.CancelDate, null).Or(Biz.JoinPath.Agency.Columns.CancelDate, cancelDate, OrmLib.MatchType.Greater);
            dm.QueryCriteria.And(c);
            return dm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static string GetAgents(string companyNumber)
        {
            BizObjects.AgentList stl = new BCS.Core.Clearance.BizObjects.AgentList();
            stl.CompanyNumber = companyNumber;

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agent.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.AgentCollection ss = dm.GetAgentCollection();

            if (ss.Count > 0)
            {
                BizObjects.Agent[] list = new BCS.Core.Clearance.BizObjects.Agent[ss.Count];

                for (int i = 0; i < ss.Count; i++)
                {
                    BizObjects.Agent st = new BCS.Core.Clearance.BizObjects.Agent();
                    st.Active = ss[i].Active.IsNull ? false : ss[i].Active.Value;
                    st.AddressId = ss[i].AddressId.IsNull ? 0 : ss[i].AddressId.Value;

                    st.AgencyId = ss[i].AgencyId.IsNull ? 0 : ss[i].AgencyId.Value;
                    st.BrokerNo = ss[i].BrokerNo;
                    st.CancelDate = ss[i].CancelDate.IsNull ? System.DateTime.MinValue : ss[i].CancelDate.Value;
                    st.DepartmentName = ss[i].DepartmentName;
                    st.DOB = ss[i].DOB.IsNull ? System.DateTime.MinValue : ss[i].DOB.Value;
                    st.EmailAddress = ss[i].EmailAddress;
                    st.EntryBy = ss[i].EntryBy;
                    st.EntryDate = ss[i].EntryDt.IsNull ? System.DateTime.MinValue : ss[i].EntryDt.Value;
                    st.Fax = ss[i].Fax;
                    st.FaxAreaCode = ss[i].FaxAreaCode;
                    st.FirstName = ss[i].FirstName;
                    st.FunctionalTitle = ss[i].FunctionalTitle;
                    st.Id = ss[i].Id;
                    st.MailStopCode = ss[i].MailStopCode;
                    st.Middle = ss[i].Middle;
                    st.ModifiedBy = ss[i].ModifiedBy;
                    st.ModifiedDt = ss[i].ModifiedDt.IsNull ? System.DateTime.MinValue : ss[i].ModifiedDt.Value;
                    st.Prefix = ss[i].Prefix;
                    st.PrimaryPhone = ss[i].PrimaryPhone;
                    st.PrimaryPhoneAreaCode = ss[i].PrimaryPhoneAreaCode;
                    st.ProfessionalTitle = ss[i].ProfessionalTitle;
                    st.SecondaryPhone = ss[i].SecondaryPhone;
                    st.SecondaryPhoneAreaCode = ss[i].SecondaryPhoneAreaCode;
                    st.SuffixTitle = ss[i].SuffixTitle;
                    st.Surname = ss[i].Surname;
                    st.TaxId = ss[i].TaxId;
                    st.UserId = ss[i].UserId.IsNull ? 0 : ss[i].UserId.Value;
                    list[i] = st;
                }
                stl.List = list;
            }

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="agencyNumber"></param>
        /// <returns></returns>
        public static string GetAgentsByAgencyNumber(string companyNumber, string agencyNumber)
        {
            BizObjects.AgentList stl = new BCS.Core.Clearance.BizObjects.AgentList();
            stl.CompanyNumber = companyNumber;

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agent.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.Agent.Agency.Columns.AgencyNumber, agencyNumber);
            Biz.AgentCollection ss = dm.GetAgentCollection();

            if (ss.Count > 0)
            {
                BizObjects.Agent[] list = new BCS.Core.Clearance.BizObjects.Agent[ss.Count];

                for (int i = 0; i < ss.Count; i++)
                {
                    BizObjects.Agent st = new BCS.Core.Clearance.BizObjects.Agent();
                    st.Active = ss[i].Active.IsNull ? false : ss[i].Active.Value;
                    st.AddressId = ss[i].AddressId.IsNull ? 0 : ss[i].AddressId.Value;

                    st.AgencyId = ss[i].AgencyId.IsNull ? 0 : ss[i].AgencyId.Value;
                    st.BrokerNo = ss[i].BrokerNo;
                    st.CancelDate = ss[i].CancelDate.IsNull ? System.DateTime.MinValue : ss[i].CancelDate.Value;
                    st.DepartmentName = ss[i].DepartmentName;
                    st.DOB = ss[i].DOB.IsNull ? System.DateTime.MinValue : ss[i].DOB.Value;
                    st.EmailAddress = ss[i].EmailAddress;
                    st.EntryBy = ss[i].EntryBy;
                    st.EntryDate = ss[i].EntryDt.IsNull ? System.DateTime.MinValue : ss[i].EntryDt.Value;
                    st.Fax = ss[i].Fax;
                    st.FaxAreaCode = ss[i].FaxAreaCode;
                    st.FirstName = ss[i].FirstName;
                    st.FunctionalTitle = ss[i].FunctionalTitle;
                    st.Id = ss[i].Id;
                    st.MailStopCode = ss[i].MailStopCode;
                    st.Middle = ss[i].Middle;
                    st.ModifiedBy = ss[i].ModifiedBy;
                    st.ModifiedDt = ss[i].ModifiedDt.IsNull ? System.DateTime.MinValue : ss[i].ModifiedDt.Value;
                    st.Prefix = ss[i].Prefix;
                    st.PrimaryPhone = ss[i].PrimaryPhone;
                    st.PrimaryPhoneAreaCode = ss[i].PrimaryPhoneAreaCode;
                    st.ProfessionalTitle = ss[i].ProfessionalTitle;
                    st.SecondaryPhone = ss[i].SecondaryPhone;
                    st.SecondaryPhoneAreaCode = ss[i].SecondaryPhoneAreaCode;
                    st.SuffixTitle = ss[i].SuffixTitle;
                    st.Surname = ss[i].Surname;
                    st.TaxId = ss[i].TaxId;
                    st.UserId = ss[i].UserId.IsNull ? 0 : ss[i].UserId.Value;
                    list[i] = st;
                }
                stl.List = list;
            }

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="agencyNumber"></param>
        /// <returns></returns>
        public static string GetContactsByAgencyNumber(string companyNumber, string agencyNumber)
        {
            BizObjects.ContactList stl = new BCS.Core.Clearance.BizObjects.ContactList();

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Contact.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.Contact.Agency.Columns.AgencyNumber, agencyNumber);
            Biz.ContactCollection ss = dm.GetContactCollection();

            if (ss.Count > 0)
            {
                BizObjects.Contact[] list = new BCS.Core.Clearance.BizObjects.Contact[ss.Count];

                for (int i = 0; i < ss.Count; i++)
                {
                    BizObjects.Contact st = new BCS.Core.Clearance.BizObjects.Contact();
                    st.Id = ss[i].Id;
                    st.AgencyId = ss[i].AgencyId.IsNull ? 0 : ss[i].AgencyId.Value;
                    st.Name = ss[i].Name;
                    st.DateOfBirth = ss[i].DateOfBirth.IsNull ? System.DateTime.MinValue : ss[i].DateOfBirth.Value;
                    st.PrimaryPhone = ss[i].PrimaryPhone;
                    st.PrimaryPhoneExtension = ss[i].PrimaryPhoneExtension;
                    st.SecondaryPhone = ss[i].SecondaryPhone;
                    st.SecondaryPhoneExtension = ss[i].SecondaryPhoneExtension;
                    st.Fax = ss[i].Fax;
                    st.Email = ss[i].Email;
                    st.RetirementDate = ss[i].RetirementDate.IsNull ? System.DateTime.MinValue : ss[i].RetirementDate.Value;
                    list[i] = st;
                }
                stl.List = list;
            }

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string GetAgentById(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agent.Columns.Id, id);
            Biz.Agent a = dm.GetAgent();

            if (a != null)
            {
                BizObjects.Agent st = new BCS.Core.Clearance.BizObjects.Agent();
                st.Active = a.Active.IsNull ? false : a.Active.Value;
                st.AddressId = a.AddressId.IsNull ? 0 : a.AddressId.Value;

                st.AgencyId = a.AgencyId.IsNull ? 0 : a.AgencyId.Value;
                st.BrokerNo = a.BrokerNo;
                st.CancelDate = a.CancelDate.IsNull ? System.DateTime.MinValue : a.CancelDate.Value;
                st.DepartmentName = a.DepartmentName;
                st.DOB = a.DOB.IsNull ? System.DateTime.MinValue : a.DOB.Value;
                st.EmailAddress = a.EmailAddress;
                st.EntryBy = a.EntryBy;
                st.EntryDate = a.EntryDt.IsNull ? System.DateTime.MinValue : a.EntryDt.Value;
                st.Fax = a.Fax;
                st.FaxAreaCode = a.FaxAreaCode;
                st.FirstName = a.FirstName;
                st.FunctionalTitle = a.FunctionalTitle;
                st.Id = a.Id;
                st.MailStopCode = a.MailStopCode;
                st.Middle = a.Middle;
                st.ModifiedBy = a.ModifiedBy;
                st.ModifiedDt = a.ModifiedDt.IsNull ? System.DateTime.MinValue : a.ModifiedDt.Value;
                st.Prefix = a.Prefix;
                st.PrimaryPhone = a.PrimaryPhone;
                st.PrimaryPhoneAreaCode = a.PrimaryPhoneAreaCode;
                st.ProfessionalTitle = a.ProfessionalTitle;
                st.SecondaryPhone = a.SecondaryPhone;
                st.SecondaryPhoneAreaCode = a.SecondaryPhoneAreaCode;
                st.SuffixTitle = a.SuffixTitle;
                st.Surname = a.Surname;
                st.TaxId = a.TaxId;
                st.UserId = a.UserId.IsNull ? 0 : a.UserId.Value;

                return XML.Serializer.SerializeAsXml(st);
            }
            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetClientNameTypes()
        {
            Biz.Lookups lookups = Common.GetLookupManager();
            Biz.ClientNameTypeCollection ats = lookups.ClientNameTypes;

            BizObjects.ClientNameTypeList stl = new BCS.Core.Clearance.BizObjects.ClientNameTypeList();
            BizObjects.ClientNameType[] list = new BCS.Core.Clearance.BizObjects.ClientNameType[ats.Count];

            for (int i = 0; i < ats.Count; i++)
            {
                BizObjects.ClientNameType st = new BCS.Core.Clearance.BizObjects.ClientNameType();
                st.Id = ats[i].Id;
                st.NameType = ats[i].NameType;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetGeographicDirectional()
        {
            Biz.DataManager dm = Common.GetDataManager();
            Biz.GeographicDirectionalCollection ss = dm.GetGeographicDirectionalCollection();

            BizObjects.GeographicDirectionalList stl = new BCS.Core.Clearance.BizObjects.GeographicDirectionalList();
            BizObjects.GeographicDirectional[] list = new BCS.Core.Clearance.BizObjects.GeographicDirectional[ss.Count];

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.GeographicDirectional st = new BCS.Core.Clearance.BizObjects.GeographicDirectional();
                st.Id = ss[i].Id;
                st.GDirectional = ss[i].PropertyGeographicDirectional;
                st.Abbreviation = ss[i].Abbreviation;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static string GetLOBsByCompanyNumber(string companyNumber)
        {
            Biz.DataManager dm = Common.GetDataManager();            
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.LineOfBusinessCollection lobs = dm.GetLineOfBusinessCollection();


            BizObjects.LineOfBusinessList stl = new BCS.Core.Clearance.BizObjects.LineOfBusinessList();
            BizObjects.LineOfBusiness[] list = new BCS.Core.Clearance.BizObjects.LineOfBusiness[lobs.Count];

            for (int i = 0; i < lobs.Count; i++)
            {
                BizObjects.LineOfBusiness st = new BCS.Core.Clearance.BizObjects.LineOfBusiness();
                st.Code = lobs[i].Code;
                st.Description = lobs[i].Description;
                st.Id = lobs[i].Id;
                st.Line = lobs[i].Line;
                list[i] = st;
            }
            stl.CompanyNumber = companyNumber;
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetStreetSuffixes()
        {
            Biz.DataManager dm = Common.GetDataManager();
            Biz.StreetSuffixCollection lobs = dm.GetStreetSuffixCollection();


            BizObjects.StreetSuffixList stl = new BCS.Core.Clearance.BizObjects.StreetSuffixList();
            BizObjects.StreetSuffix[] list = new BCS.Core.Clearance.BizObjects.StreetSuffix[lobs.Count];

            for (int i = 0; i < lobs.Count; i++)
            {
                BizObjects.StreetSuffix st = new BCS.Core.Clearance.BizObjects.StreetSuffix();
                st.ApprovedAbbreviation = lobs[i].ApprovedAbbreviation;
                st.CommonAbbreviation = lobs[i].CommonAbbreviation;
                st.Id = lobs[i].Id;
                st.Suffix = lobs[i].PropertyStreetSuffix;
                list[i] = st;
            }            
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static string GetAgencyLOBsByAgency(int agencyId)
        {
            BizObjects.LineOfBusinessList stl = new BCS.Core.Clearance.BizObjects.LineOfBusinessList();
            stl.AgencyId = agencyId;

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.AgencyLineOfBusiness.Columns.AgencyId, agencyId);

            Biz.LineOfBusinessCollection cns = dm.GetLineOfBusinessCollection();
            if (cns.Count > 0)
            {
                BizObjects.LineOfBusiness[] list = new BCS.Core.Clearance.BizObjects.LineOfBusiness[cns.Count];

                for (int i = 0; i < cns.Count; i++)
                {
                    BizObjects.LineOfBusiness st = new BCS.Core.Clearance.BizObjects.LineOfBusiness();
                    st.Code = cns[i].Code;
                    st.Id = cns[i].Id;
                    st.Description = cns[i].Description;
                    st.Line = cns[i].Line;
                    list[i] = st;
                }
                stl.List = list;
            }
            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string GetCompanyNumberCodeByCompanyNumberCodeId(int id)
        {
            BizObjects.CompanyNumberCode cncode = new BCS.Core.Clearance.BizObjects.CompanyNumberCode();
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Id, id);
            Biz.CompanyNumberCode cnc = dm.GetCompanyNumberCode();
            if (cnc != null)
            {
                cncode.Code = cnc.Code;
                cncode.CompanyCodeTypeId = cnc.CompanyCodeTypeId.Value;
                cncode.CompanyNumberId = cnc.CompanyNumberId.Value;
                cncode.Description = cnc.Description;
                cncode.Id = cnc.Id;

                return XML.Serializer.SerializeAsXml(cncode);
            }

            return null;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="agencyId"></param>
        ///// <param name="LobId"></param>
        ///// <returns></returns>
        //public static string GetPersonsByAgencyLOB(int agencyId, int LobId)
        //{
        //    BizObjects.PersonList stl = new BCS.Core.Clearance.BizObjects.PersonList();
        //    stl.AgencyId = agencyId;
        //    stl.LineOfBusinessId = LobId;

        //    Biz.DataManager dm = Common.GetDataManager();
        //    dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPerson.Columns.AgencyId, agencyId).And(
        //        Biz.JoinPath.Person.AgencyLineOfBusinessPerson.Columns.LineOfBusinessId, LobId);

        //    Biz.PersonCollection cns = dm.GetPersonCollection();
        //    if (cns.Count > 0)
        //    {
        //        BizObjects.Person[] list = new BCS.Core.Clearance.BizObjects.Person[cns.Count];

        //        for (int i = 0; i < cns.Count; i++)
        //        {
        //            BizObjects.Person st = new BCS.Core.Clearance.BizObjects.Person();
        //            st.CompanyNumberId = cns[i].CompanyNumberId.Value;
        //            st.Id = cns[i].Id;
        //            st.EmailAddress = cns[i].EmailAddress;
        //            st.FullName = cns[i].FullName;
        //            st.UserName = cns[i].UserName;
        //            list[i] = st;
        //        }
        //        stl.List = list;
        //    }
        //    return XML.Serializer.SerializeAsXml(stl);
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetCompanies()
        {
            BizObjects.CompanyList stl = new BCS.Core.Clearance.BizObjects.CompanyList();
           
            Biz.DataManager dm = Common.GetDataManager();

            Biz.CompanyCollection cns = dm.GetCompanyCollection();
            if (cns.Count > 0)
            {
                BizObjects.Company[] list = new BCS.Core.Clearance.BizObjects.Company[cns.Count];

                for (int i = 0; i < cns.Count; i++)
                {
                    BizObjects.Company st = new BCS.Core.Clearance.BizObjects.Company();
                    st.AllowSubmissionsOnError = cns[i].AllowSubmissionsOnError.IsNull ? false : cns[i].AllowSubmissionsOnError.Value;
                    st.AnalystRoleId = cns[i].AnalystRoleId.IsNull ? 0 : cns[i].AnalystRoleId.Value;
                    st.ClientCoreClientType = cns[i].ClientCoreClientType;
                    st.ClientCoreCorpCode = cns[i].ClientCoreCorpCode; 
                    st.ClientCoreCorpId = cns[i].ClientCoreCorpId; 
                    st.ClientCoreEndPointUrl = cns[i].ClientCoreEndPointUrl;
                    st.ClientCoreLocalId = cns[i].ClientCoreLocalId;
                    st.ClientCorePassword = cns[i].ClientCorePassword;
                    st.ClientCoreSystemCd = cns[i].ClientCoreSystemCd;
                    st.ClientCoreUsername = cns[i].ClientCoreUsername;
                    st.ClientCoreOwner = cns[i].ClientCoreOwner;
                    st.CompanyInitials = cns[i].CompanyInitials;
                    st.CompanyName = cns[i].CompanyName;
                    st.Id = cns[i].Id;
                    st.LogoUrl = cns[i].LogoUrl;
                    st.ThemeStyleSheet = cns[i].ThemeStyleSheet;
                    st.UnderwriterRoleId = cns[i].UnderwriterRoleId.IsNull ? 0 : cns[i].UnderwriterRoleId.Value;
                    list[i] = st;
                }
                stl.List = list;
            }
            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetCompanyNumbers()
        {
            BizObjects.CompanyNumberList stl = new BCS.Core.Clearance.BizObjects.CompanyNumberList();

            Biz.DataManager dm = Common.GetDataManager();

            Biz.CompanyNumberCollection cns = dm.GetCompanyNumberCollection();
            if (cns.Count > 0)
            {
                BizObjects.CompanyNumber[] list = new BCS.Core.Clearance.BizObjects.CompanyNumber[cns.Count];

                for (int i = 0; i < cns.Count; i++)
                {
                    BizObjects.CompanyNumber st = new BCS.Core.Clearance.BizObjects.CompanyNumber();
                    st.CompanyId = cns[i].CompanyId.Value;
                    st.Id = cns[i].Id;
                    st.PropertyCompanyNumber = cns[i].PropertyCompanyNumber;
                    list[i] = st;
                }
                stl.List = list;
            }
            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyName"></param>
        /// <returns></returns>
        public static string GetCompanyNumbers(string companyName)
        {
            BizObjects.CompanyNumberList stl = new BCS.Core.Clearance.BizObjects.CompanyNumberList();
            stl.CompanyName = companyName;

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Company.Columns.CompanyName, companyName);

            Biz.CompanyNumberCollection cns = dm.GetCompanyNumberCollection();
            if (cns.Count > 0)
            {
                BizObjects.CompanyNumber[] list = new BCS.Core.Clearance.BizObjects.CompanyNumber[cns.Count];

                for (int i = 0; i < cns.Count; i++)
                {
                    BizObjects.CompanyNumber st = new BCS.Core.Clearance.BizObjects.CompanyNumber();
                    st.CompanyId = cns[i].CompanyId.Value;
                    st.Id = cns[i].Id;
                    st.PropertyCompanyNumber = cns[i].PropertyCompanyNumber;
                    list[i] = st;
                }
                stl.List = list;
            }
            return XML.Serializer.SerializeAsXml(stl);
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="companyId"></param>
       /// <returns></returns>
        public static string GetCompanyNumbers(int companyId)
        {
            BizObjects.CompanyNumberList stl = new BCS.Core.Clearance.BizObjects.CompanyNumberList();            

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Company.Columns.Id, companyId);

            Biz.CompanyNumberCollection cns = dm.GetCompanyNumberCollection();
            if (cns.Count > 0)
            {
                BizObjects.CompanyNumber[] list = new BCS.Core.Clearance.BizObjects.CompanyNumber[cns.Count];

                for (int i = 0; i < cns.Count; i++)
                {
                    BizObjects.CompanyNumber st = new BCS.Core.Clearance.BizObjects.CompanyNumber();
                    st.CompanyId = cns[i].CompanyId.Value;
                    st.Id = cns[i].Id;
                    st.PropertyCompanyNumber = cns[i].PropertyCompanyNumber;
                    list[i] = st;
                }
                stl.List = list;
            }
            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static BizObjects.CompanyNumberList GetCompanyNumbersInternal(int companyId)
        {
            BizObjects.CompanyNumberList stl = new BCS.Core.Clearance.BizObjects.CompanyNumberList();

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Company.Columns.Id, companyId);

            Biz.CompanyNumberCollection cns = dm.GetCompanyNumberCollection();
            if (cns.Count > 0)
            {
                BizObjects.CompanyNumber[] list = new BCS.Core.Clearance.BizObjects.CompanyNumber[cns.Count];

                for (int i = 0; i < cns.Count; i++)
                {
                    BizObjects.CompanyNumber st = new BCS.Core.Clearance.BizObjects.CompanyNumber();
                    st.CompanyId = cns[i].CompanyId.Value;
                    st.Id = cns[i].Id;
                    st.PropertyCompanyNumber = cns[i].PropertyCompanyNumber;
                    list[i] = st;
                }
                stl.List = list;
            }
            return stl;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetUnderwritingRoles()
        {
            BizObjects.UnderwritingRoleList stl = new BCS.Core.Clearance.BizObjects.UnderwritingRoleList();            

            Biz.DataManager dm = Common.GetDataManager();            

            Biz.UnderwritingRoleCollection cns = dm.GetUnderwritingRoleCollection();
            if (cns.Count > 0)
            {
                BizObjects.UnderwritingRole[] list = new BCS.Core.Clearance.BizObjects.UnderwritingRole[cns.Count];

                for (int i = 0; i < cns.Count; i++)
                {
                    BizObjects.UnderwritingRole st = new BCS.Core.Clearance.BizObjects.UnderwritingRole();
                    st.Id = cns[i].Id;
                    st.RoleCode = cns[i].RoleCode;
                    st.RoleName = cns[i].RoleName;

                    list[i] = st;
                }
                stl.List = list;
            }
            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="LobId"></param>
        /// <param name="uroleId"></param>
        /// <returns></returns>
        public static string GetPersonsByAgencyLOBUnderwritingRole(int agencyId, int lobId, int uroleId)
        {
            BizObjects.PersonList stl = new BCS.Core.Clearance.BizObjects.PersonList();
            stl.AgencyId = agencyId;
            stl.LineOfBusinessId = lobId;
            stl.UnderwriterRoleId = uroleId;

            Biz.DataManager dm = Common.GetDataManager();
            //dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPerson.Columns.AgencyId, agencyId).And(
            //    Biz.JoinPath.Person.AgencyLineOfBusinessPerson.Columns.LineOfBusinessId, LobId);

            dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agencyId).And(
                Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.LineOfBusinessId, lobId).And(
                Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.UnderwritingRole.Columns.Id, uroleId);
            
            Biz.PersonCollection cns = dm.GetPersonCollection(Biz.FetchPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson);
            if (cns.Count > 0)
            {
                BizObjects.Person[] list = new BCS.Core.Clearance.BizObjects.Person[cns.Count];

                for (int i = 0; i < cns.Count; i++)
                {
                    Biz.AgencyLineOfBusinessPersonUnderwritingRolePersonCollection ccccc = cns[i].AgencyLineOfBusinessPersonUnderwritingRolePersons.FilterByAgencyId(
                        agencyId).FilterByLineOfBusinessId(
                        lobId).FilterByUnderwritingRoleId(
                        uroleId);
                    BizObjects.Person st = new BCS.Core.Clearance.BizObjects.Person();
                    st.CompanyNumberId = cns[i].CompanyNumberId.Value;
                    st.Id = cns[i].Id;
                    st.EmailAddress = cns[i].EmailAddress;
                    st.FullName = cns[i].FullName;
                    st.UserName = cns[i].UserName;
                    st.IsPrimary = ccccc[0].Primary.Value;
                    list[i] = st;
                }
                stl.List = list;
            }
            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string GetAgencyById(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, id);
            Biz.Agency a = dm.GetAgency(Biz.FetchPath.Agency.AgencyLicensedState.State);

            
            if (a != null)
            {
                BizObjects.Agency oa = new BCS.Core.Clearance.BizObjects.Agency();

                oa.Active = a.Active.Value;
                oa.Address = a.Address;
                oa.AgencyName = a.AgencyName;
                oa.AgencyNumber = a.AgencyNumber;
                oa.BranchId = a.BranchId;
                oa.City = a.City;
                oa.Comments = a.Comments;
                oa.CompanyNumberId = a.CompanyNumberId.Value;
                oa.ContactPerson = a.ContactPerson;
                oa.Email = a.Email;
                oa.EntryBy = a.EntryBy;
                oa.EntryDate = a.EntryDt.Value;
                oa.Fax = a.Fax.IsNull ? decimal.Zero : a.Fax.Value;
                oa.FEIN = a.FEIN;
                oa.Id = a.Id;
                oa.ImageFax = a.ImageFax;
                oa.ModifiedBy = a.ModifiedBy;
                oa.ModifiedDt = a.ModifiedDt.IsNull ? System.DateTime.MinValue : a.ModifiedDt.Value;
                oa.Phone = a.Phone.IsNull ? decimal.Zero : a.Phone.Value;
                oa.ReferralAgencyNumber = a.ReferralAgencyNumber;
                oa.ReferralDate = a.ReferralDate.IsNull ? System.DateTime.MinValue : a.ReferralDate.Value;
                oa.State = a.State;
                oa.Zip = a.Zip;
                oa.CaptiveBroker = a.CaptiveBroker;

                if (a.AgencyLicensedStates.Count > 0)
                {
                    oa.LicensedStatesList = new BCS.Core.Clearance.BizObjects.StateList();
                    oa.LicensedStatesList.List = new BCS.Core.Clearance.BizObjects.State[a.AgencyLicensedStates.Count];
                    for (int j = 0; j < a.AgencyLicensedStates.Count; j++)
                    {
                        oa.LicensedStatesList.List[j] = new BCS.Core.Clearance.BizObjects.State();
                        oa.LicensedStatesList.List[j].Abbreviation = a.AgencyLicensedStates[j].State.Abbreviation;
                        oa.LicensedStatesList.List[j].Id = a.AgencyLicensedStates[j].StateId.Value;
                        oa.LicensedStatesList.List[j].StateName = a.AgencyLicensedStates[j].State.StateName;
                    }
                }

                return XML.Serializer.SerializeAsXml(oa);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string GetCompanyById(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Company.Columns.Id, id);
            Biz.Company a = dm.GetCompany();
            
            if (a != null)
            {
                BizObjects.Company oa = new BCS.Core.Clearance.BizObjects.Company();
                oa.AllowSubmissionsOnError = a.AllowSubmissionsOnError.IsNull ? false : a.AllowSubmissionsOnError.Value;
                oa.AnalystRoleId = a.AnalystRoleId.IsNull ? 0 : a.AnalystRoleId.Value;
                oa.ClientCoreCorpCode = a.ClientCoreCorpCode;
                oa.ClientCoreCorpId = a.ClientCoreCorpId;
                oa.ClientCoreEndPointUrl = a.ClientCoreEndPointUrl;
                oa.ClientCoreLocalId = a.ClientCoreLocalId;
                oa.ClientCorePassword = a.ClientCorePassword;
                oa.ClientCoreSystemCd = a.ClientCoreSystemCd;
                oa.ClientCoreUsername = a.ClientCoreUsername;
                oa.ClientCoreOwner = a.ClientCoreOwner;
                oa.CompanyInitials = a.CompanyInitials;
                oa.CompanyName = a.CompanyName;
                oa.Id = a.Id;
                oa.LogoUrl = a.LogoUrl;
                oa.ThemeStyleSheet = a.ThemeStyleSheet;
                oa.UnderwriterRoleId = a.UnderwriterRoleId.IsNull ? 0 : a.UnderwriterRoleId.Value;
                return XML.Serializer.SerializeAsXml(oa);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="LobId"></param>
        /// <returns></returns>
        public static string GetPersonsByAgencyLOBUnderwritingRole(int agencyId, int lobId)
        {
            BizObjects.PersonList stl = new BCS.Core.Clearance.BizObjects.PersonList();
            stl.AgencyId = agencyId;
            stl.LineOfBusinessId = lobId;

            Biz.DataManager dm = Common.GetDataManager();
            //dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPerson.Columns.AgencyId, agencyId).And(
            //    Biz.JoinPath.Person.AgencyLineOfBusinessPerson.Columns.LineOfBusinessId, LobId);

            dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agencyId).And(
                Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.LineOfBusinessId, lobId);

            Biz.PersonCollection cns = dm.GetPersonCollection();
            if (cns.Count > 0)
            {
                BizObjects.Person[] list = new BCS.Core.Clearance.BizObjects.Person[cns.Count];

                for (int i = 0; i < cns.Count; i++)
                {
                    BizObjects.Person st = new BCS.Core.Clearance.BizObjects.Person();
                    st.CompanyNumberId = cns[i].CompanyNumberId.Value;
                    st.Id = cns[i].Id;
                    st.EmailAddress = cns[i].EmailAddress;
                    st.FullName = cns[i].FullName;
                    st.UserName = cns[i].UserName;
                    list[i] = st;
                }
                stl.List = list;
            }
            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="initials"></param>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static string GetPersonsByInitialsAndCompanyNumber(string initials, string companyNumber)
        {
            BizObjects.PersonList stl = new BCS.Core.Clearance.BizObjects.PersonList();

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber).And(
                Biz.JoinPath.Person.Columns.Initials, initials);

            Biz.PersonCollection cns = dm.GetPersonCollection();
            if (cns.Count > 0)
            {
                BizObjects.Person[] list = new BCS.Core.Clearance.BizObjects.Person[cns.Count];

                for (int i = 0; i < cns.Count; i++)
                {
                    BizObjects.Person st = new BCS.Core.Clearance.BizObjects.Person();
                    st.CompanyNumberId = cns[i].CompanyNumberId.Value;
                    st.Id = cns[i].Id;
                    st.EmailAddress = cns[i].EmailAddress;
                    st.FullName = cns[i].FullName;
                    st.UserName = cns[i].UserName;
                    list[i] = st;
                }
                stl.List = list;
            }
            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static int GetPersonsByUsernameAndCompanyNumber(string username, string companyNumber)
        {
            //BizObjects.PersonList stl = new BCS.Core.Clearance.BizObjects.PersonList();

            //Biz.DataManager dm = Common.GetDataManager();
            //dm.QueryCriteria.And(Biz.JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber).And(
            //    Biz.JoinPath.Person.Columns.Initials, initials);

            //Biz.PersonCollection cns = dm.GetPersonCollection();
            //if (cns.Count > 0)
            //{
            //    BizObjects.Person[] list = new BCS.Core.Clearance.BizObjects.Person[cns.Count];

            //    for (int i = 0; i < cns.Count; i++)
            //    {
            //        BizObjects.Person st = new BCS.Core.Clearance.BizObjects.Person();
            //        st.CompanyNumberId = cns[i].CompanyNumberId.Value;
            //        st.Id = cns[i].Id;
            //        st.EmailAddress = cns[i].EmailAddress;
            //        st.FullName = cns[i].FullName;
            //        st.UserName = cns[i].UserName;
            //        list[i] = st;
            //    }
            //    stl.List = list;
            //}
            //return XML.Serializer.SerializeAsXml(stl);


            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber).And(
                Biz.JoinPath.Person.Columns.UserName, username);

            Biz.PersonCollection cns = dm.GetPersonCollection();
            if (cns.Count != 1)
                return 0;
            else
                return cns[0].Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string GetPersonById(int id)
        {
            BizObjects.Person st = new BCS.Core.Clearance.BizObjects.Person();
            
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.Id, id);

            Biz.Person person = dm.GetPerson();

            if (person != null)
            {
                //BizObjects.Person st = new BCS.Core.Clearance.BizObjects.Person();
                st.CompanyNumberId = person.CompanyNumberId.Value;
                st.Id = person.Id;
                st.EmailAddress = person.EmailAddress;
                st.FullName = person.FullName;
                st.UserName = person.UserName;
                return XML.Serializer.SerializeAsXml(st);
            }
            //return XML.Serializer.SerializeAsXml(st);
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string GetCompanyNumberCodeTypeByCodeTypeId(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.Id, id);
            Biz.CompanyNumberCodeType cnct = dm.GetCompanyNumberCodeType();

            if (cnct != null)
            {
                BizObjects.CodeType type = new BCS.Core.Clearance.BizObjects.CodeType();
                type.CodeName = cnct.CodeName;
                type.CompanyNumberId = cnct.CompanyNumberId.Value;
                type.DefaultCompanyNumberCodeId = cnct.DefaultCompanyNumberCodeId.IsNull ? 0 : cnct.DefaultCompanyNumberCodeId.Value;
                type.DisplayOrder = cnct.DisplayOrder.Value;
                type.Id = cnct.Id;
                type.ClientSpecific = cnct.ClientSpecific.IsNull ? false : cnct.ClientSpecific.Value;

                string xml = XML.Serializer.SerializeAsXml(type);
                return xml;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <param name="clientSpecific"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberAttributeCollection GetCompanyNumberAttributesByCompanyNumberId(string companyNumberId, bool clientSpecific)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.Columns.CompanyNumberId, companyNumberId);            
            //dm.GetCompanyNumberAttributeCollection(Biz.FetchPath.CompanyNumberAttribute.AttributeDataType);

            if (clientSpecific)
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.Columns.ClientSpecific, clientSpecific);
            else
            {
                Biz.DataManager.CriteriaGroup clientspecific = new OrmLib.DataManagerBase.CriteriaGroup();
                clientspecific.And(Biz.JoinPath.CompanyNumberAttribute.Columns.ClientSpecific, false).Or(
                                Biz.JoinPath.CompanyNumberAttribute.Columns.ClientSpecific, null);
                dm.QueryCriteria.And(clientspecific);
            }

            Biz.CompanyNumberAttributeCollection attributes = dm.GetCompanyNumberAttributeCollection(Biz.FetchPath.CompanyNumberAttribute.AttributeDataType);
            return attributes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string GetLOBById(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, id);
            Biz.LineOfBusiness blob = dm.GetLineOfBusiness();

            if (blob != null)
            {
                BizObjects.LineOfBusiness lob = new BCS.Core.Clearance.BizObjects.LineOfBusiness();
                lob.Code = blob.Code;
                lob.Description = blob.Description;
                lob.Id = blob.Id;
                lob.Line = blob.Line;

                string xml = XML.Serializer.SerializeAsXml(lob);
                return xml;
            }
            return null;
        }

        /// <summary>
        /// gets reasons for a submission status
        /// </summary>
        /// <param name="statusCode">the status code of the submission status for which reasons are requested. passing null returns all the reasons</param>
        /// <returns>reasons</returns>
        public static string GetSubmissionStatusReasons(string statusCode)
        {
            Biz.DataManager dm = Common.GetDataManager();
            if(statusCode != null)
                dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatusReason.SubmissionStatusSubmissionStatusReason.SubmissionStatus.Columns.StatusCode, statusCode);
            Biz.SubmissionStatusReasonCollection ss = dm.GetSubmissionStatusReasonCollection();

            BizObjects.SubmissionStatusReasonList stl = new BCS.Core.Clearance.BizObjects.SubmissionStatusReasonList();
            BizObjects.SubmissionStatusReason[] list = new BCS.Core.Clearance.BizObjects.SubmissionStatusReason[ss.Count];

            stl.SubmissionStatusCode = statusCode;

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.SubmissionStatusReason st = new BCS.Core.Clearance.BizObjects.SubmissionStatusReason();
                st.Id = ss[i].Id;
                st.ReasonCode = ss[i].ReasonCode;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public static string GetSubmissionStatusReasonsByCompanyNumberAndStatusCode(string companyNumber, string statusCode)
        {
            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.Company company = dm.GetCompany(Biz.FetchPath.Company.CompanyParameter);

            Biz.CompanyParameter parameter = null;
            if (company != null && company.CompanyParameters != null)
            {
                parameter = company.CompanyParameters[0];
            }

            dm.QueryCriteria.Clear();

            dm.QueryCriteria.And(
                Biz.JoinPath.SubmissionStatusReason.SubmissionStatusSubmissionStatusReason.SubmissionStatus.CompanyNumberSubmissionStatus.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatusReason.SubmissionStatusSubmissionStatusReason.SubmissionStatus.Columns.StatusCode, statusCode);

            if (parameter != null && parameter.FilterInactiveStatusReasons)
            {
                dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatusReason.SubmissionStatusSubmissionStatusReason.Columns.Active, true);
            }

            Biz.SubmissionStatusReasonCollection ss = dm.GetSubmissionStatusReasonCollection(Biz.FetchPath.SubmissionStatusReason.SubmissionStatusSubmissionStatusReason);


            BizObjects.SubmissionStatusReasonList stl = new BCS.Core.Clearance.BizObjects.SubmissionStatusReasonList();
            BizObjects.SubmissionStatusReason[] list = new BCS.Core.Clearance.BizObjects.SubmissionStatusReason[ss.Count];

            stl.SubmissionStatusCode = statusCode;

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.SubmissionStatusReason st = new BCS.Core.Clearance.BizObjects.SubmissionStatusReason();
                st.Id = ss[i].Id;
                st.ReasonCode = ss[i].ReasonCode;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static string GetAttributesByCompanyNumber(string companyNumber)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.GetCompanyNumberAttributeCollection(Biz.FetchPath.CompanyNumberAttribute.AttributeDataType);

            Biz.CompanyNumberAttributeCollection attributes = dm.GetCompanyNumberAttributeCollection();

            BizObjects.CompanyNumberAttributeList stl = new BCS.Core.Clearance.BizObjects.CompanyNumberAttributeList();
            BizObjects.CompanyNumberAttribute[] list = new BCS.Core.Clearance.BizObjects.CompanyNumberAttribute[attributes.Count];
            
            if (!string.IsNullOrEmpty(companyNumber))
                stl.CompanyNumber = companyNumber;

            for (int i = 0; i < attributes.Count; i++)
            {
                BizObjects.CompanyNumberAttribute st = new BCS.Core.Clearance.BizObjects.CompanyNumberAttribute();
                st.AttributeId = attributes[i].AttributeDataTypeId.IsNull ? 0 : attributes[i].AttributeDataTypeId.Value;
                st.ClientSpecific = attributes[i].ClientSpecific.IsNull ? false : attributes[i].ClientSpecific.Value;
                st.DataTypeName = attributes[i].AttributeDataType.DataTypeName;
                st.DisplayOrder = attributes[i].DisplayOrder.Value;
                st.Id = attributes[i].Id;
                st.Label = attributes[i].Label;
                st.ReadOnly = attributes[i].ReadOnlyProperty.Value;
                st.UserControl = attributes[i].AttributeDataType.UserControl;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static BizObjects.Agency GetAgency(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, id);
            Biz.Agency a = dm.GetAgency(Biz.FetchPath.Agency.AgencyLicensedState.State);

            if (a != null)
            {
                BizObjects.Agency oa = new BCS.Core.Clearance.BizObjects.Agency();

                oa.Active = a.Active.Value;
                oa.Address = a.Address;
                oa.AgencyName = a.AgencyName;
                oa.AgencyNumber = a.AgencyNumber;
                oa.BranchId = a.BranchId;
                oa.City = a.City;
                oa.Comments = a.Comments;
                oa.CompanyNumberId = a.CompanyNumberId.Value;
                oa.ContactPerson = a.ContactPerson;
                oa.Email = a.Email;
                oa.EntryBy = a.EntryBy;
                oa.EntryDate = a.EntryDt.Value;
                oa.Fax = a.Fax.IsNull ? decimal.Zero : a.Fax.Value;
                oa.FEIN = a.FEIN;
                oa.Id = a.Id;
                oa.ImageFax = a.ImageFax;
                oa.ModifiedBy = a.ModifiedBy;
                oa.ModifiedDt = a.ModifiedDt.IsNull ? System.DateTime.MinValue : a.ModifiedDt.Value;
                oa.Phone = a.Phone.IsNull ? decimal.Zero : a.Phone.Value;
                oa.ReferralAgencyNumber = a.ReferralAgencyNumber;
                oa.ReferralDate = a.ReferralDate.IsNull ? System.DateTime.MinValue : a.ReferralDate.Value;
                oa.State = a.State;
                oa.Zip = a.Zip;

                if (a.AgencyLicensedStates.Count > 0)
                {
                    oa.LicensedStatesList = new BCS.Core.Clearance.BizObjects.StateList();
                    oa.LicensedStatesList.List = new BCS.Core.Clearance.BizObjects.State[a.AgencyLicensedStates.Count];
                    for (int j = 0; j < a.AgencyLicensedStates.Count; j++)
                    {
                        oa.LicensedStatesList.List[j] = new BCS.Core.Clearance.BizObjects.State();
                        oa.LicensedStatesList.List[j].Abbreviation = a.AgencyLicensedStates[j].State.Abbreviation;
                        oa.LicensedStatesList.List[j].Id = a.AgencyLicensedStates[j].StateId.Value;
                        oa.LicensedStatesList.List[j].StateName = a.AgencyLicensedStates[j].State.StateName;
                    }
                }

                if (!string.IsNullOrEmpty(oa.ReferralAgencyNumber))
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyNumber, oa.ReferralAgencyNumber);
                    Biz.Agency refAgency = dm.GetAgency();
                    oa.ReferralAgencyId = refAgency.Id;
                }
                return oa;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public string GetNumberControlTypesByCompanyNumber(string companyNumber)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.NumberControl.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.NumberControlCollection nc = dm.GetNumberControlCollection(Biz.FetchPath.NumberControl.NumberControlType);

            BizObjects.NumberControlList stl = new BCS.Core.Clearance.BizObjects.NumberControlList();
            BizObjects.NumberControl[] list = new BCS.Core.Clearance.BizObjects.NumberControl[nc.Count];

            for (int i = 0; i < nc.Count; i++)
            {
                BizObjects.NumberControl st = new BCS.Core.Clearance.BizObjects.NumberControl();
                st.CompanyNumberId = nc[i].CompanyNumberId.IsNull ? 0 : nc[i].CompanyNumberId.Value;
                st.ControlNumber = nc[i].ControlNumber.IsNull ? 0 : nc[i].ControlNumber.Value; ;                
                st.Id = nc[i].Id;
                st.NumberControlTypeId = nc[i].NumberControlTypeId.IsNull ? 0 : nc[i].NumberControlTypeId.Value;
                st.NumberControlTypeName = nc[i].NumberControlType.TypeName;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
            
        }

        /// <summary>
        /// 
        /// </summary>        
        public string GetNextNumberControlValueByCompanyNumberAndTypeName(string companyNumber, string typeName, int incrementBy)
        {
            return NumberGenerator.GetNextNumberControlValueByCompanyNumberAndTypeName(companyNumber, typeName, incrementBy);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string GetSicCodes(int code)
        {
            Biz.DataManager dm = Common.GetDataManager();
            if (code != 0)
                dm.QueryCriteria.And(Biz.JoinPath.SicCodeList.Columns.Code, code);
            Biz.SicCodeListCollection ss = dm.GetSicCodeListCollection();

            BizObjects.SicCodeList stl = new BCS.Core.Clearance.BizObjects.SicCodeList();
            BizObjects.SicCode[] list = new BCS.Core.Clearance.BizObjects.SicCode[ss.Count];

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.SicCode st = new BCS.Core.Clearance.BizObjects.SicCode();
                st.Id = ss[i].Id;
                st.Code = ss[i].Code;
                st.CodeDescription = ss[i].CodeDescription;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="codeValue"></param>
        /// <returns></returns>
        public string GetClassCodeByCodeValueAndCompanyNumber(string companyNumber, string codeValue)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            if (Common.IsNotNullOrEmpty(codeValue))
                dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CodeValue, codeValue);
            Biz.ClassCodeCollection ss = dm.GetClassCodeCollection();

            BizObjects.ClassCodeList stl = new BCS.Core.Clearance.BizObjects.ClassCodeList();
            BizObjects.ClassCode[] list = new BCS.Core.Clearance.BizObjects.ClassCode[ss.Count];

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.ClassCode st = new BCS.Core.Clearance.BizObjects.ClassCode();
                st.Id = ss[i].Id;
                st.CodeValue = ss[i].CodeValue;
                st.CompanyNumberId = ss[i].CompanyNumberId.Value;
                st.Description = ss[i].Description;
                st.SicCodeId = ss[i].SicCodeId.IsNull ? 0 : ss[i].SicCodeId.Value;
                list[i] = st;
            }
            stl.List = list;

            return XML.Serializer.SerializeAsXml(stl);
        }

        public int GetCompanyIdbyCompanyNumber(string companyNumber)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.CompanyNumber cNumber = dm.GetCompanyNumber();
            if (cNumber == null)
                return 0;
            else
                return cNumber.CompanyId.Value;
        }

        public int GetAgencyIdByCompanyNumberAgencyNumber(string companyNumber, string agencyNumber)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyNumber, agencyNumber);
            Biz.Agency agency = dm.GetAgency();
            if (agency == null)
                return 0;
            else
                return agency.Id;
        }

        public int GetAgencyIdByCompanyNumberIdAgencyNumber(int companyNumberId, string agencyNumber)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.CompanyNumberId, companyNumberId);
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyNumber, agencyNumber);
            Biz.Agency agency = dm.GetAgency();
            if (agency == null)
                return 0;
            else
                return agency.Id;
        }

        public string GetAssignment(string companyNumber, string agencyNumber, string underwritingUnitCode, string roleCode, bool isPrimary)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Agency.Columns.AgencyNumber, agencyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.LineOfBusiness.Columns.Code, underwritingUnitCode);
            dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.UnderwritingRole.Columns.RoleCode, roleCode);

            if (isPrimary)
            {
                dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.Primary, isPrimary);
            }
            
            Biz.PersonCollection persons = dm.GetPersonCollection(Biz.FetchPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson);

            if (persons != null && persons.Count > 0)
            {
                foreach (Biz.Person person in persons)
                {
                    //Return the first non-retired person record found
                    if (person.RetirementDate.IsNull || person.RetirementDate > System.DateTime.Now)
                    {
                        BizObjects.Person st = new BCS.Core.Clearance.BizObjects.Person();
                        st.CompanyNumberId = person.CompanyNumberId.Value;
                        st.Id = person.Id;
                        st.EmailAddress = person.EmailAddress;
                        st.FullName = person.FullName;
                        st.UserName = person.UserName;
                        st.Active = true;
                        st.RetirementDate = person.RetirementDate.IsNull ? System.DateTime.MinValue : person.RetirementDate.Value;
                        return XML.Serializer.SerializeAsXml(st);
                    }
                }
            }

            return null;
        }

        public string GetHazardGradesByClassCode(string companyNumber, string classCode)
        {
            BizObjects.ClassCode result = null;
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CodeValue, classCode);
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);

            Biz.ClassCode code = dm.GetClassCode(Biz.FetchPath.ClassCode.CompanyNumberClassCodeHazardGradeValue.HazardGradeType,
                Biz.FetchPath.ClassCode.ClassCodeCompanyNumberCode.CompanyNumberCode.CompanyNumberCodeType);

            if (code != null)
            {
                result = new BizObjects.ClassCode();
                result.CodeValue = code.CodeValue;
                result.Description = code.Description;
                result.SicCodeId = (int)code.SicCodeId;
                if (code.CompanyNumberClassCodeHazardGradeValues != null)
                {
                    foreach (Biz.CompanyNumberClassCodeHazardGradeValue item in code.CompanyNumberClassCodeHazardGradeValues)
                    {
                        result.HazardGrades.Add(new BizObjects.HazardGrade(){HazardGradeTypeCode=item.HazardGradeType.TypeName, HazardGradeValue=item.HazardGradeValue });
                    }
                }
                if (code.ClassCodeCompanyNumberCodes != null)
                {
                    foreach (Biz.ClassCodeCompanyNumberCode cc in code.ClassCodeCompanyNumberCodes)
                    {
                        result.ClassCodeCompanyNumberCodes.Add(new BizObjects.ClassCodeCompanyNumberCode() {  CodeTypeName = cc.CompanyNumberCode.CompanyNumberCodeType.CodeName, Code = cc.CompanyNumberCode.Code, CodeDescription = cc.CompanyNumberCode.Description });
                    }
                }
            }

            return XML.Serializer.SerializeAsXml(result);
        }

        public string GetOtherCarriersByCompanyNumber(string companyNumber)
        {
            if (string.IsNullOrWhiteSpace(companyNumber))
                return string.Empty;

            List<BizObjects.OtherCarrier> result = new List<BizObjects.OtherCarrier>();

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.OtherCarrier.CompanyNumberOtherCarrier.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);

            Biz.OtherCarrierCollection otherCarriers = dm.GetOtherCarrierCollection();

            if (otherCarriers == null || otherCarriers.Count < 1)
                return string.Empty;

            foreach (Biz.OtherCarrier carrier in otherCarriers)
            {
                result.Add(new BizObjects.OtherCarrier() { Code = carrier.Code, Description = carrier.Description, Id = carrier.Id });
            }

            return XML.Serializer.SerializeAsXml(result); 
        }
    }
}
