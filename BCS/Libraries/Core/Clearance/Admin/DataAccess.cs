using System;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using BTS.LogFramework;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace BCS.Core.Clearance.Admin
{
    /// <summary>
    /// 
    /// </summary>
    public class DataAccess
    {
        /// <summary>
        /// 
        /// </summary>
        public DataAccess()
        { }

        #region Agencies

        private static BCS.Biz.DataManager AddAgencyCancelCriteria(BCS.Biz.DataManager dm)
        {
            return AddAgencyCancelCriteria(dm, DateTime.Now);
        }

        private static BCS.Biz.DataManager AddAgencyCancelCriteria(BCS.Biz.DataManager dm, DateTime date)
        {
            if (date < SqlDateTime.MinValue.Value)
                date = SqlDateTime.MinValue.Value;
            if (date > SqlDateTime.MaxValue.Value)
                date = SqlDateTime.MaxValue.Value;
            Biz.DataManager.CriteriaGroup c1 = new OrmLib.DataManagerBase.CriteriaGroup();
            c1.And(Biz.JoinPath.Agency.Columns.CancelDate, null).Or(Biz.JoinPath.Agency.Columns.CancelDate, date, OrmLib.MatchType.GreaterOrEqual);
            dm.QueryCriteria.And(c1);
            Biz.DataManager.CriteriaGroup c2 = new OrmLib.DataManagerBase.CriteriaGroup();
            c2.And(Biz.JoinPath.Agency.Columns.EffectiveDate, null).Or(Biz.JoinPath.Agency.Columns.EffectiveDate, date, OrmLib.MatchType.LesserOrEqual);
            dm.QueryCriteria.And(c2);
            return dm;
        }

        private static BCS.Biz.DataManager AddAgencyCancelCriteria(BCS.Biz.DataManager dm, DateTime date, bool isRenewalCancelDate)
        {
            if (date < SqlDateTime.MinValue.Value)
                date = SqlDateTime.MinValue.Value;
            if (date > SqlDateTime.MaxValue.Value)
                date = SqlDateTime.MaxValue.Value;
            Biz.DataManager.CriteriaGroup c1 = new OrmLib.DataManagerBase.CriteriaGroup();
            c1.And(Biz.JoinPath.Agency.Columns.RenewalCancelDate, null).Or(Biz.JoinPath.Agency.Columns.RenewalCancelDate, date, OrmLib.MatchType.GreaterOrEqual);
            dm.QueryCriteria.And(c1);
            Biz.DataManager.CriteriaGroup c2 = new OrmLib.DataManagerBase.CriteriaGroup();
            c2.And(Biz.JoinPath.Agency.Columns.EffectiveDate, null).Or(Biz.JoinPath.Agency.Columns.EffectiveDate, date, OrmLib.MatchType.LesserOrEqual);
            dm.QueryCriteria.And(c2);
            return dm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgencies(int companyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.CompanyNumber.Columns.CompanyId, companyId);
            Biz.AgencyCollection ac = dm.GetAgencyCollection(Biz.FetchPath.Agency.CompanyNumber);
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
                foreach (Biz.Agency agency in ac)
                {
                    agency.AgencyName = string.Format("{0} ({1})", agency.AgencyName, agency.AgencyNumber);
                }
            }
            return ac;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="excludeCanceled"></param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgencies(int companyId, bool excludeCanceled)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.CompanyNumber.Columns.CompanyId, companyId);
            if (excludeCanceled)
                dm = AddAgencyCancelCriteria(dm);
            Biz.AgencyCollection ac = dm.GetAgencyCollection(Biz.FetchPath.Agency.CompanyNumber);
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
                foreach (Biz.Agency agency in ac)
                {
                    agency.AgencyName = string.Format("{0} ({1})", agency.AgencyName, agency.AgencyNumber);
                }
            }
            return ac;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgencies(string filter, int companyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            if (filter == null || filter.Length == 0)
                return GetAgencies(companyId);
            Biz.AgencyCollection ac = GetAgencies(companyId).FilterByAgencyNumber(filter, OrmLib.CompareType.Contains);
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
            }
            return ac;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <param name="date"></param>
        /// <param name="agencyNumber"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="masterAgencyId">ignored if -1</param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgenciesByNameNumberCityStateAnd(int companyNumberId,
            DateTime date, string agencyName, string agencyNumber, string city, string state, int masterAgencyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.CompanyNumberId, companyNumberId);
            if (masterAgencyId != -1)
            {
                Biz.DataManager.CriteriaGroup masterCriteria = new OrmLib.DataManagerBase.CriteriaGroup();
                masterCriteria.And(Biz.JoinPath.Agency.Columns.MasterAgencyId, masterAgencyId).Or(
                    Biz.JoinPath.Agency.Columns.Id, masterAgencyId);
                dm.QueryCriteria.And(masterCriteria);
            }
            dm = AddAgencyCancelCriteria(dm, date);
            if (Common.IsNotNullOrEmpty(agencyName))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyName, agencyName, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(agencyNumber))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyNumber, agencyNumber, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(city))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.City, city, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(state))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.State, state, OrmLib.MatchType.Partial);

            Biz.AgencyCollection ac = dm.GetAgencyCollection();
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
            }
            return ac;
        }

        public static Biz.AgencyCollection GetAgenciesByNameNumberCityStateAnd(int companyNumberId,
            DateTime date, string agencyName, string agencyNumber, string city, string state, int masterAgencyId, int extra)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.CompanyNumberId, companyNumberId);
            if (masterAgencyId != -1)
            {
                Biz.DataManager.CriteriaGroup masterCriteria = new OrmLib.DataManagerBase.CriteriaGroup();
                masterCriteria.And(Biz.JoinPath.Agency.Columns.MasterAgencyId, masterAgencyId).Or(
                    Biz.JoinPath.Agency.Columns.Id, masterAgencyId);
                dm.QueryCriteria.And(masterCriteria);
            }
            dm = AddAgencyCancelCriteria(dm, date);
            if (Common.IsNotNullOrEmpty(agencyName))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyName, agencyName, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(agencyNumber))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyNumber, agencyNumber, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(city))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.City, city, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(state))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.State, state, OrmLib.MatchType.Partial);

            if (extra != 0)
            {
                OrmLib.DataManagerBase.CriteriaGroup cg = new OrmLib.DataManagerBase.CriteriaGroup();
                cg.And(Biz.JoinPath.Agency.Columns.Id, extra);
                dm.QueryCriteria.Or(cg);
            }

            Biz.AgencyCollection ac = dm.GetAgencyCollection();
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
            }
            return ac;
        }

        public static Biz.AgencyCollection GetAgenciesByNameNumberCityStateAndUseRenewalCancelDate(int companyNumberId,
           DateTime date, string agencyName, string agencyNumber, string city, string state, int masterAgencyId, int extra, bool useRenewalCancelDate)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.CompanyNumberId, companyNumberId);
            if (masterAgencyId != -1)
            {
                Biz.DataManager.CriteriaGroup masterCriteria = new OrmLib.DataManagerBase.CriteriaGroup();
                masterCriteria.And(Biz.JoinPath.Agency.Columns.MasterAgencyId, masterAgencyId).Or(
                    Biz.JoinPath.Agency.Columns.Id, masterAgencyId);
                dm.QueryCriteria.And(masterCriteria);
            }
            if (useRenewalCancelDate)
                dm = AddAgencyCancelCriteria(dm, date, useRenewalCancelDate);
            else
                dm = AddAgencyCancelCriteria(dm, date);
            if (Common.IsNotNullOrEmpty(agencyName))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyName, agencyName, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(agencyNumber))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyNumber, agencyNumber, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(city))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.City, city, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(state))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.State, state, OrmLib.MatchType.Partial);


            Biz.AgencyCollection ac = dm.GetAgencyCollection(BCS.Biz.FetchPath.Agency.AgencyBranch);
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
            }
            return ac;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterAgencyNumber"></param>
        /// <param name="filterAgencyName"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgencies(string filterAgencyNumber, string filterAgencyName, int companyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.CompanyNumber.Columns.CompanyId, companyId);
            if (Common.IsNotNullOrEmpty(filterAgencyName))
            {
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyName, filterAgencyName, OrmLib.MatchType.Partial);
            }
            if (Common.IsNotNullOrEmpty(filterAgencyNumber))
            {
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyNumber, filterAgencyNumber, OrmLib.MatchType.Partial);
            }

            Biz.AgencyCollection ac = dm.GetAgencyCollection(Biz.FetchPath.Agency.CompanyNumber);
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
            }
            return ac;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="companyId"></param>
        /// <param name="excludeCanceled"></param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgencies(string filter, int companyId, bool excludeCanceled)
        {
            if (filter == null || filter.Length == 0)
                return GetAgencies(companyId, excludeCanceled);
            Biz.DataManager dm = Common.GetDataManager();
            if (excludeCanceled)
                dm = AddAgencyCancelCriteria(dm);
            Biz.AgencyCollection ac = GetAgencies(companyId).FilterByAgencyNumber(filter, OrmLib.CompareType.Contains);
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
                foreach (Biz.Agency agency in ac)
                {
                    agency.AgencyName = string.Format("{0} ({1})", agency.AgencyName, agency.AgencyNumber);
                }
            }
            return ac;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyName"></param>
        /// <param name="companyId"></param>
        /// <param name="excludeCanceled"></param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgenciesByAgencyName(string agencyName, int companyId, bool excludeCanceled)
        {
            if (agencyName == null || agencyName.Length == 0)
                return GetAgencies(companyId, excludeCanceled);
            Biz.DataManager dm = Common.GetDataManager();
            if (excludeCanceled)
                dm = AddAgencyCancelCriteria(dm);
            Biz.AgencyCollection ac = GetAgencies(companyId).FilterByAgencyName(agencyName, OrmLib.CompareType.Contains);
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
                foreach (Biz.Agency agency in ac)
                {
                    agency.AgencyName = string.Format("{0} ({1})", agency.AgencyName, agency.AgencyNumber);
                }
            }
            return ac;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterAgencyNumber"></param>
        /// <param name="filterLOBId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgencies(string filterAgencyNumber, int filterLOBId, int companyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.CompanyNumber.Columns.CompanyId, companyId);
            if (filterLOBId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.Agency.AgencyLineOfBusiness.Columns.LineOfBusinessId, filterLOBId);
            Biz.AgencyCollection ac = dm.GetAgencyCollection(
                Biz.FetchPath.Agency.CompanyNumber, Biz.FetchPath.Agency.AgencyLineOfBusiness);
            if (filterAgencyNumber != null && filterAgencyNumber.Length > 0)
                ac = GetAgencies(companyId).FilterByAgencyNumber(filterAgencyNumber, OrmLib.CompareType.Contains);
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
                foreach (Biz.Agency agency in ac)
                {
                    agency.AgencyName = string.Format("{0} ({1})", agency.AgencyName, agency.AgencyNumber);
                }
            }
            return ac;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterAgencyNumber"></param>
        /// <param name="filterLOBId"></param>
        /// <param name="unassignedOnly"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgencies(string filterAgencyNumber, int filterLOBId, bool unassignedOnly, int companyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.CompanyNumber.Columns.CompanyId, companyId);
            if (filterLOBId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.Agency.AgencyLineOfBusiness.Columns.LineOfBusinessId, filterLOBId);
            if (Common.IsNotNullOrEmpty(filterAgencyNumber))
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyNumber, filterAgencyNumber, OrmLib.MatchType.Partial);
            Biz.AgencyCollection ac = dm.GetAgencyCollection(
                Biz.FetchPath.Agency.CompanyNumber, Biz.FetchPath.Agency.AgencyLineOfBusiness);
            //if (Common.IsNotNullOrEmpty(filterAgencyNumber))
            //    ac = GetAgencies(companyId).FilterByAgencyNumber(filterAgencyNumber, OrmLib.CompareType.Contains);

            if (unassignedOnly)
            {
                Biz.AgencyCollection unassignedAgencies = new BCS.Biz.AgencyCollection();
                foreach (Biz.Agency agency in ac)
                {
                    if (agency.AgencyLineOfBusinesss.Count == 0)
                        unassignedAgencies.Add(agency);
                }
                ac = unassignedAgencies;
            }
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
                //foreach (Biz.Agency agency in ac)
                //{
                //    agency.AgencyName = string.Format("{0} ({1})", agency.AgencyName, agency.AgencyNumber);
                //}
            }
            return ac;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterAgencyNumber"></param>
        /// <param name="filterLOBId"></param>
        /// <param name="companyId"></param>
        /// <param name="excludeCanceled"></param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgencies(string filterAgencyNumber, int filterLOBId, int companyId, bool excludeCanceled)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.CompanyNumber.Columns.CompanyId, companyId);
            if (filterLOBId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.Agency.AgencyLineOfBusiness.Columns.LineOfBusinessId, filterLOBId);
            if (excludeCanceled)
                dm = AddAgencyCancelCriteria(dm);
            Biz.AgencyCollection ac = dm.GetAgencyCollection(
                Biz.FetchPath.Agency.CompanyNumber, Biz.FetchPath.Agency.AgencyLineOfBusiness);
            if (filterAgencyNumber != null && filterAgencyNumber.Length > 0)
                ac = GetAgencies(companyId).FilterByAgencyNumber(filterAgencyNumber, OrmLib.CompareType.Contains);
            if (ac != null)
            {
                ac = ac.SortByAgencyName(OrmLib.SortDirection.Ascending);
                foreach (Biz.Agency agency in ac)
                {
                    agency.AgencyName = string.Format("{0} ({1})", agency.AgencyName, agency.AgencyNumber);
                }
            }
            return ac;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.Agency GetAgency(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, id);
            Biz.Agency agency = dm.GetAgency(Biz.FetchPath.Agency.CompanyNumber, Biz.FetchPath.Agency.ParentRelationAgency,
                Biz.FetchPath.Agency.ChildRelationAgency, Biz.FetchPath.Agency.AgencyLicensedState.State);
            //if (agency != null && agency.State == null)
            //    agency.State = string.Empty;            
            return agency;
        }

        public static string GetLicensedStates(int agencyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.AgencyLicensedState.Columns.AgencyId, agencyId);
            Biz.AgencyLicensedStateCollection agencyLicensedStates = dm.GetAgencyLicensedStateCollection(Biz.FetchPath.AgencyLicensedState.State);
            if (null == agencyLicensedStates || 0 == agencyLicensedStates.Count)
                return string.Empty;

            List<string> listStates = new List<string>();
            foreach (Biz.AgencyLicensedState var in agencyLicensedStates)
            {
                listStates.Add(var.State.Abbreviation);
            }
            return string.Join(",", listStates.ToArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string DeleteAgency(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, id);
            dm.GetAgency().Delete();
            try
            {
                string s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulAgencyDeleteFK;
                }
                return "There was a problem deleting the agency.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the agency.";
            }

            return "Agency was successfully deleted.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agency"></param>
        /// <returns></returns>
        public static string AddAgency(BizObjects.Agency agency)
        {
            if (agency == null)
                return "Input agency was null.";
            if (agency.ModifiedBy == null)
                return "The ModifiedBy property cannot be null. Cannot proceed with add.";

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, agency.CompanyNumberId);
            Biz.CompanyNumber cn = dm.GetCompanyNumber();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyNumber, agency.AgencyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.CompanyNumberId, agency.CompanyNumberId);
            Biz.AgencyCollection duplicates = dm.GetAgencyCollection();
            if (duplicates != null && duplicates.Count > 1)
            {
                return "The Agency already exists. Please enter a different Agency Name or Number.";
            }

            dm.QueryCriteria.Clear();
            Biz.Agency newAgency = dm.NewAgency(agency.AgencyNumber, agency.AgencyName, agency.Active, agency.ModifiedBy,
                DateTime.Now, cn);
            newAgency.Address = agency.Address;
            newAgency.Address2 = agency.Address2;
            newAgency.BranchId = agency.BranchId;
            newAgency.CancelDate = agency.CancelDate == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : agency.CancelDate;
            newAgency.City = agency.City;
            newAgency.Comments = agency.Comments;
            newAgency.ContactPerson = agency.ContactPerson;
            newAgency.ContactPersonEmail = agency.ContactPersonEmail;
            newAgency.ContactPersonExtension = agency.ContactPersonExtension;
            newAgency.ContactPersonFax = agency.ContactPersonFax;
            newAgency.ContactPersonPhone = agency.ContactPersonPhone;
            newAgency.EffectiveDate = agency.EffectiveDate == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : agency.EffectiveDate;
            newAgency.Email = agency.Email;
            newAgency.Fax = agency.Fax == 0 ? System.Data.SqlTypes.SqlDecimal.Null : agency.Fax;
            newAgency.FEIN = agency.FEIN;
            newAgency.ImageFax = agency.ImageFax;
            newAgency.IsMaster = agency.IsMaster;
            newAgency.MasterAgencyId = agency.MasterAgencyId == 0 ? System.Data.SqlTypes.SqlInt32.Null : agency.MasterAgencyId;
            newAgency.ModifiedBy = agency.EntryBy;
            newAgency.ModifiedDt = DateTime.Now;
            newAgency.Phone = agency.Phone == 0 ? System.Data.SqlTypes.SqlDecimal.Null : agency.Phone;
            newAgency.ReferralAgencyNumber = agency.ReferralAgencyNumber;
            newAgency.ReferralDate = agency.ReferralDate == DateTime.MinValue ? SqlDateTime.Null : agency.ReferralDate;
            newAgency.State = agency.State;
            newAgency.StateOfIncorporation = agency.StateOfIncorporation;
            newAgency.StateTaxId = agency.StateTaxId;
            newAgency.Zip = agency.Zip;

            foreach (BizObjects.State state in agency.LicensedStatesList.List)
            {
                Biz.AgencyLicensedState aState = newAgency.AgencyLicensedStates.FindByStateId(state.Id);
                if (aState == null)
                    aState = newAgency.NewAgencyLicensedState();
                aState.StateId = state.Id;
            }

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    LogCentral.Current.LogWarn(ex.Message, ex);
                    return "The Agency already exists. Please enter a different Agency Name or Number.";
                }
                throw;
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the agency."; }

            return "Agency was successfully added.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agency"></param>
        /// <returns></returns>
        public static string UpdateAgency(BizObjects.Agency agency)
        {
            if (agency == null)
                return "Input agency was null.";
            if (agency.ModifiedBy == null)
                return "The ModifiedBy property cannot be null. Cannot proceed with update.";

            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agency.Id);
            Biz.Agency newAgency = dm.GetAgency(Biz.FetchPath.Agency.AgencyLicensedState);
            newAgency.Active = agency.Active;
            newAgency.Address = agency.Address;
            newAgency.Address2 = agency.Address2;
            newAgency.AgencyName = agency.AgencyName;
            newAgency.AgencyNumber = agency.AgencyNumber;
            newAgency.BranchId = agency.BranchId;
            newAgency.CancelDate = agency.CancelDate == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : agency.CancelDate;
            newAgency.City = agency.City;
            newAgency.Comments = agency.Comments;
            newAgency.CompanyNumberId = agency.CompanyNumberId;
            newAgency.ContactPerson = agency.ContactPerson;
            newAgency.ContactPersonEmail = agency.ContactPersonEmail;
            newAgency.ContactPersonExtension = agency.ContactPersonExtension;
            newAgency.ContactPersonFax = agency.ContactPersonFax;
            newAgency.ContactPersonPhone = agency.ContactPersonPhone;
            newAgency.EffectiveDate = agency.EffectiveDate == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : agency.EffectiveDate;
            newAgency.Email = agency.Email;
            newAgency.Fax = agency.Fax == 0 ? System.Data.SqlTypes.SqlDecimal.Null : agency.Fax;
            newAgency.FEIN = agency.FEIN;
            newAgency.ImageFax = agency.ImageFax;
            newAgency.IsMaster = agency.IsMaster;
            newAgency.MasterAgencyId = agency.MasterAgencyId == 0 ? System.Data.SqlTypes.SqlInt32.Null : agency.MasterAgencyId;
            newAgency.ModifiedBy = agency.ModifiedBy;
            newAgency.ModifiedDt = DateTime.Now;
            newAgency.Phone = agency.Phone == 0 ? System.Data.SqlTypes.SqlDecimal.Null : agency.Phone;
            newAgency.ReferralAgencyNumber = agency.ReferralAgencyNumber;
            newAgency.ReferralDate = agency.ReferralDate == DateTime.MinValue ? SqlDateTime.Null : agency.ReferralDate;
            newAgency.State = agency.State;
            newAgency.StateOfIncorporation = agency.StateOfIncorporation;
            newAgency.StateTaxId = agency.StateTaxId;
            newAgency.Zip = agency.Zip;

            List<int> toRemove = new List<int>();
            foreach (Biz.AgencyLicensedState var in newAgency.AgencyLicensedStates)
            {
                bool removable = true;
                // find licensed states not contained in new 
                if (agency.LicensedStatesList != null && agency.LicensedStatesList.List != null)
                {
                    foreach (BizObjects.State bovar in agency.LicensedStatesList.List)
                    {
                        if (bovar.Id == var.StateId)
                        {
                            removable = false;
                            break;
                        }
                    }
                }
                if (removable)
                    toRemove.Add(var.StateId.Value);
            }
            foreach (int var in toRemove)
            {
                newAgency.AgencyLicensedStates.FindByStateId(var).Delete();
            }

            foreach (BizObjects.State state in agency.LicensedStatesList.List)
            {
                Biz.AgencyLicensedState aState = newAgency.AgencyLicensedStates.FindByStateId(state.Id);
                if (aState == null)
                    aState = newAgency.NewAgencyLicensedState();
                aState.StateId = state.Id;
            }

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulAgencyUpdateFK;
                }
                if (ex.Number == 2627)
                {
                    LogCentral.Current.LogWarn(ex.Message, ex);
                    return "The Agency already exists. Please enter a different Agency Name or Number.";
                }
                throw;
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the agency."; }

            return "Agency was successfully updated.";
        }

        #endregion

        #region Agents

        private Biz.DataManager AddAgentCancelCriteria(Biz.DataManager dm)
        {
            Biz.DataManager.CriteriaGroup c = new OrmLib.DataManagerBase.CriteriaGroup();
            c.And(Biz.JoinPath.Agent.Columns.CancelDate, null).Or(Biz.JoinPath.Agent.Columns.CancelDate, DateTime.Now, OrmLib.MatchType.Greater);
            dm.QueryCriteria.And(c);
            return dm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterAgencyNumber"></param>
        /// <param name="filterAgencyName"></param>
        /// <param name="filterAgentName"></param>
        /// <param name="filterAgentNumber"></param>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        //public static Biz.AgentCollection GetAgents(string filterAgencyNumber, string filterAgencyName, string filterAgentName,
        //        string filterAgentNumber, string companyNumber)
        //{
        //    Biz.DataManager dm = Common.GetDataManager();
        //    dm.QueryCriteria.And(Biz.JoinPath.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
        //    if (Common.IsNotNullOrEmpty(filterAgencyName))
        //        dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyName, filterAgencyName, OrmLib.MatchType.Partial);
        //    if (Common.IsNotNullOrEmpty(filterAgencyNumber))
        //        dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.AgencyNumber, filterAgencyNumber, OrmLib.MatchType.Partial);
        //    if (Common.IsNotNullOrEmpty(filterAgentNumber))
        //        dm.QueryCriteria.And(Biz.JoinPath.Agency.Agent.Columns.BrokerNo, filterAgentNumber, OrmLib.MatchType.Partial);
        //    if (Common.IsNotNullOrEmpty(filterAgentName))
        //    {
        //        OrmLib.DataManagerBase.CriteriaGroup cg = new OrmLib.DataManagerBase.CriteriaGroup();
        //        cg.Or(Biz.JoinPath.Agency.Agent.Columns.FirstName, filterAgentName, OrmLib.MatchType.Partial).Or(
        //            Biz.JoinPath.Agency.Agent.Columns.Surname, filterAgentName, OrmLib.MatchType.Partial);
        //        dm.QueryCriteria.And(cg);
        //    }

        //    Biz.AgencyCollection agencies = dm.GetAgencyCollection(Biz.FetchPath.Agency.Agent);
        //    agencies = agencies.SortByAgencyNumber(OrmLib.SortDirection.Ascending);

        //    Biz.AgentCollection returnAgents = new Biz.AgentCollection();
        //    foreach (Biz.Agency agency in agencies)
        //    {
        //        Biz.AgentCollection tempAgents = agency.Agents.SortBySurname(OrmLib.SortDirection.Ascending);
        //        foreach (Biz.Agent agent in tempAgents)
        //        {
        //            returnAgents.Add(agent);
        //        }
        //    }

        //    return returnAgents;
        //}
        public static DataTable GetAgents(string filterAgencyNumber, string filterAgencyName, string filterAgentName,
                string filterAgentNumber, string companyNumber)
        {
            using (SqlConnection conn = new SqlConnection(Common.DSN))
            {
                string commandText = "SELECT * FROM dbo.Agent INNER JOIN dbo.Agency ON dbo.Agent.AgencyId = dbo.Agency.Id INNER JOIN dbo.CompanyNumber ON dbo.Agency.CompanyNumberId = dbo.CompanyNumber.Id WHERE (dbo.CompanyNumber.CompanyNumber = '{0}') {1} ORDER BY dbo.Agency.AgencyNumber, dbo.Agent.Surname ";

                StringBuilder sb = new StringBuilder();
                if (Common.IsNotNullOrEmpty(filterAgencyNumber))
                    sb.AppendFormat(" AND (dbo.Agency.AgencyNumber LIKE '%{0}%') ", filterAgencyNumber);
                if (Common.IsNotNullOrEmpty(filterAgencyName))
                    sb.AppendFormat(" AND (dbo.Agency.AgencyName LIKE '%{0}%') ", filterAgencyName);
                if (Common.IsNotNullOrEmpty(filterAgentName))
                    sb.AppendFormat(" AND (dbo.Agent.Surname LIKE '%{0}%' OR dbo.Agent.FirstName LIKE '%{0}%') ", filterAgentName);
                if (Common.IsNotNullOrEmpty(filterAgentNumber))
                    sb.AppendFormat(" AND (dbo.Agent.BrokerNo LIKE '%{0}%') ", filterAgentNumber);

                SqlCommand command = new SqlCommand();
                command.Connection = conn;
                command.CommandText = string.Format(commandText, companyNumber, sb.ToString());

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataSet ds = new DataSet();
                adapter.Fill(ds, "Agent");

                return ds.Tables[0];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.Agent GetAgent(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agent.Columns.Id, id);
            Biz.Agent agent = dm.GetAgent(Biz.FetchPath.Agent.Agency, Biz.FetchPath.Agent.User);
            if (agent != null)
            {
                if (agent.UserId.IsNull)
                    agent.UserId = 0;
                agent.PrimaryPhone = agent.PrimaryPhoneAreaCode + agent.PrimaryPhone;
                agent.SecondaryPhone = agent.SecondaryPhoneAreaCode + agent.SecondaryPhone;
                agent.Fax = agent.FaxAreaCode + agent.Fax;
                if (agent.Agency != null)
                {
                    agent.Agency.AgencyName = string.Format("{0} ({1})", agent.Agency.AgencyName, agent.Agency.AgencyNumber);
                }
            }
            return agent;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string DeleteAgent(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agent.Columns.Id, id);
            dm.GetAgent().Delete();
            try
            {
                string s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulAgentDeleteFK;
                }
                return "There was a problem deleting the agent.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the agent.";
            }

            return "Agent was successfully deleted.";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="agent"></param>
        /// <returns></returns>
        public static string AddAgent(BizObjects.Agent agent)
        {
            if (agent == null)
                return "Input agent was null.";
            if (agent.ModifiedBy == null)
                return "The ModifiedBy property cannot be null. Cannot proceed with add.";

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agent.AgencyId);
            Biz.Agency ag = dm.GetAgency();

            dm.QueryCriteria.Clear();
            Biz.Agent newAgent = dm.NewAgent("0", agent.FirstName, agent.Surname, agent.Active,
                agent.ModifiedBy, DateTime.Now, ag);
            newAgent.AddressId = agent.AddressId == 0 ? System.Data.SqlTypes.SqlInt32.Null : agent.AddressId;
            newAgent.BrokerNo = string.IsNullOrEmpty(agent.BrokerNo) ? "0" : agent.BrokerNo;
            newAgent.CancelDate = agent.CancelDate == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : agent.CancelDate;
            newAgent.DOB = agent.DOB == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : agent.DOB;
            newAgent.DepartmentName = agent.DepartmentName;
            newAgent.EffectiveDate = agent.EffectiveDate == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : agent.EffectiveDate;
            newAgent.EmailAddress = agent.EmailAddress;
            newAgent.Fax = agent.Fax;
            newAgent.FaxAreaCode = agent.FaxAreaCode;
            newAgent.FunctionalTitle = agent.FunctionalTitle;
            newAgent.MailStopCode = agent.MailStopCode;
            newAgent.Middle = agent.Middle;
            newAgent.ModifiedBy = agent.ModifiedBy;
            newAgent.ModifiedDt = DateTime.Now;
            newAgent.Prefix = agent.Prefix;
            newAgent.PrimaryPhone = agent.PrimaryPhone;
            newAgent.PrimaryPhoneAreaCode = agent.PrimaryPhoneAreaCode;
            newAgent.ProfessionalTitle = agent.ProfessionalTitle;
            newAgent.SecondaryPhone = agent.SecondaryPhone;
            newAgent.SecondaryPhoneAreaCode = agent.SecondaryPhoneAreaCode;
            newAgent.SuffixTitle = agent.SuffixTitle;
            newAgent.TaxId = agent.TaxId;
            newAgent.UserId = agent.UserId == 0 ? System.Data.SqlTypes.SqlInt32.Null : agent.UserId;

            try
            { dm.CommitAll(); }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the agent."; }

            return "Agent was successfully added.";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="agent"></param>
        /// <returns></returns>
        public static string UpdateAgent(BizObjects.Agent agent)
        {
            if (agent == null)
                return "Input agent was null.";
            if (agent.ModifiedBy == null)
                return "The ModifiedBy property cannot be null. Cannot proceed with update.";

            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Agent.Columns.Id, agent.Id);
            Biz.Agent newAgent = dm.GetAgent();
            newAgent.Active = agent.Active;
            newAgent.AddressId = agent.AddressId == 0 ? System.Data.SqlTypes.SqlInt32.Null : agent.AddressId;
            newAgent.AgencyId = agent.AgencyId;
            newAgent.BrokerNo = string.IsNullOrEmpty(agent.BrokerNo) ? "0" : agent.BrokerNo;
            newAgent.CancelDate = agent.CancelDate == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : agent.CancelDate;
            newAgent.DepartmentName = agent.DepartmentName;
            newAgent.DOB = agent.DOB == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : agent.DOB;
            newAgent.EffectiveDate = agent.EffectiveDate == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : agent.EffectiveDate;
            newAgent.EmailAddress = agent.EmailAddress;
            newAgent.EntryBy = agent.ModifiedBy;
            newAgent.EntryDt = DateTime.Now;
            newAgent.Fax = agent.Fax;
            newAgent.FaxAreaCode = agent.FaxAreaCode;
            newAgent.FirstName = agent.FirstName;
            newAgent.FunctionalTitle = agent.FunctionalTitle;
            newAgent.MailStopCode = agent.MailStopCode;
            newAgent.Middle = agent.Middle;
            newAgent.ModifiedBy = agent.ModifiedBy;
            newAgent.ModifiedDt = DateTime.Now;
            newAgent.Prefix = agent.Prefix;
            newAgent.PrimaryPhone = agent.PrimaryPhone;
            newAgent.PrimaryPhoneAreaCode = agent.PrimaryPhoneAreaCode;
            newAgent.ProfessionalTitle = agent.ProfessionalTitle;
            newAgent.SecondaryPhone = agent.SecondaryPhone;
            newAgent.SecondaryPhoneAreaCode = agent.SecondaryPhoneAreaCode;
            newAgent.SuffixTitle = agent.SuffixTitle;
            newAgent.Surname = agent.Surname;
            newAgent.TaxId = agent.TaxId;
            newAgent.UserId = agent.UserId == 0 ? System.Data.SqlTypes.SqlInt32.Null : agent.UserId;

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulAgentUpdateFK;
                }
                return "There was a problem updating the agent.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the agent."; }

            return "Agent was successfully updated.";
        }
        #endregion

        #region Line of Business by Agency and Company Number

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        //public static Biz.LineOfBusinessCollection GetLineOfBusinesses(int companyId)
        //{
        //    Biz.DataManager dm = Common.GetDataManager();
        //    if (companyId != 0)
        //        dm.QueryCriteria.And(
        //        Biz.JoinPath.LineOfBusiness.AgencyLineOfBusiness.Agency.CompanyNumber.Columns.CompanyId,
        //        companyId);
        //    return dm.GetLineOfBusinessCollection();
        //}

        public static Biz.LineOfBusinessCollection GetLineOfBusinesses(int companynumberid)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.Columns.CompanyNumberId, companynumberid);
            return dm.GetLineOfBusinessCollection();
        }

        public static Biz.LineOfBusiness GetLineOfBusiness(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, id);
            return dm.GetLineOfBusiness();
        }

        public static string AddCompanyNumberLOB(int companyNumberId, string code, string description, string line)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, companyNumberId);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();

            Biz.LineOfBusiness newLob = dm.NewLineOfBusiness(code, description, line);
            Biz.CompanyNumberLineOfBusiness newCompanyNumberLineOfBusiness = cnum.NewCompanyNumberLineOfBusiness();
            newCompanyNumberLineOfBusiness.LineOfBusiness = newLob;

            try
            { dm.CommitAll(); }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the company number line of business."; }

            return "Company Number line of business was successfully added.";
        }

        public static string UpdateCompanyNumberLOB(int id, string code, string description, string line)
        {
            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, id);
            Biz.LineOfBusiness newLOB = dm.GetLineOfBusiness();

            newLOB.Code = code;
            newLOB.Description = description;
            newLOB.Line = line;

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return "There was a problem updating the company number line of business.";
                }
                return "There was a problem updating the company number line of business.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the company number line of business."; }

            return "Company Number line of business was successfully updated.";
        }
        public static string DeleteCompanyNumberLOB(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, id);
            try
            {
                Biz.LineOfBusiness lineOfBusiness = dm.GetLineOfBusiness(Biz.FetchPath.LineOfBusiness.CompanyNumberLineOfBusiness);
                lineOfBusiness.CompanyNumberLineOfBusinesss.FindByLineOfBusinessId(id).Delete();
                lineOfBusiness.Delete();
                string s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return "There was a problem deleting the company number line of business.";
                }
                return "There was a problem deleting the company number line of business.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the company number line of business.";
            }

            return "Company Number line of business was successfully deleted.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static Biz.LineOfBusinessCollection GetLineOfBusinessesByAgencyId(int companyId, int agencyId)
        {
            if (agencyId == 0)
                return null;
            Biz.DataManager dm = Common.GetDataManager();
            if (companyId != 0)
                dm.QueryCriteria.And(
                    Biz.JoinPath.LineOfBusiness.AgencyLineOfBusiness.Agency.CompanyNumber.Columns.CompanyId,
                    companyId);

            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.AgencyLineOfBusiness.Columns.AgencyId, agencyId);
            return dm.GetLineOfBusinessCollection();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static Biz.LineOfBusinessCollection GetLineOfBusinessesNotByAgencyId(int companyId, int agencyId)
        {
            if (agencyId == 0)
                return null;
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Company.Columns.Id, companyId);
            Biz.LineOfBusinessCollection coll = dm.GetLineOfBusinessCollection();
            Biz.LineOfBusinessCollection ac = GetLineOfBusinessesByAgencyId(companyId, agencyId);

            Biz.LineOfBusinessCollection newcoll = new BCS.Biz.LineOfBusinessCollection();
            foreach (Biz.LineOfBusiness lob in coll)
            {
                if (ac.FindById(lob.Id) == null)
                {
                    newcoll.Add(lob);
                }
            }
            return newcoll;
        }


        private static Biz.LineOfBusinessCollection GetLineOfBusinessesByAgencyIds(int companyId, int personId, int[] agencyIds)
        {
            Biz.DataManager dm = Common.GetDataManager();
            if (companyId != 0)
                dm.QueryCriteria.And(
                    Biz.JoinPath.LineOfBusiness.AgencyLineOfBusiness.Agency.CompanyNumber.Columns.CompanyId,
                    companyId);
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.AgencyLineOfBusinessPersonUnderwritingRolePerson.Person.Columns.Id, personId);
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agencyIds, OrmLib.MatchType.In);
            return dm.GetLineOfBusinessCollection();
        }

        private static Biz.LineOfBusinessCollection GetLineOfBusinessesNotByAgencyIds(int companyId, int personId, int[] agencyIds)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.AgencyLineOfBusiness.Agency.Columns.Id, agencyIds, OrmLib.MatchType.In);
            Biz.LineOfBusinessCollection coll = dm.GetLineOfBusinessCollection();
            Biz.LineOfBusinessCollection ac = GetLineOfBusinessesByAgencyIds(companyId, personId, agencyIds);

            Biz.LineOfBusinessCollection newcoll = new BCS.Biz.LineOfBusinessCollection();

            foreach (Biz.LineOfBusiness lob in coll)
            {
                if (ac.FindById(lob.Id) == null)
                {
                    newcoll.Add(lob);
                }
            }
            return newcoll;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="personId"></param>
        /// <param name="agencyIds"></param>
        /// <returns></returns>
        public static Biz.LineOfBusinessCollection GetLineOfBusinessesByAgencyIds(int companyId, int personId, string agencyIds)
        {
            if (agencyIds == null)
                return new Biz.LineOfBusinessCollection();
            string[] saids = agencyIds.Split(",".ToCharArray());
            int[] aids = new int[saids.Length];
            for (int i = 0; i < saids.Length; i++)
            {
                aids[i] = Convert.ToInt32(saids[i]);
            }
            return GetLineOfBusinessesByAgencyIds(companyId, personId, aids);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="personId"></param>
        /// <param name="agencyIds"></param>
        /// <returns></returns>
        public static Biz.LineOfBusinessCollection GetLineOfBusinessesNotByAgencyIds(int companyId, int personId, string agencyIds)
        {
            if (agencyIds == null)
                return new BCS.Biz.LineOfBusinessCollection();
            string[] saids = agencyIds.Split(",".ToCharArray());
            int[] aids = new int[saids.Length];
            for (int i = 0; i < saids.Length; i++)
            {
                aids[i] = Convert.ToInt32(saids[i]);
            }
            return GetLineOfBusinessesNotByAgencyIds(companyId, personId, aids);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public static Biz.LineOfBusinessCollection GetLineOfBusinessesByCompanyNumberId(int companyNumberId)
        {
            if (companyNumberId == 0)
                return new BCS.Biz.LineOfBusinessCollection();
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.Columns.CompanyNumberId, companyNumberId);
            return dm.GetLineOfBusinessCollection();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public static Biz.LineOfBusinessCollection GetLineOfBusinessesNotByCompanyNumberId(int companyNumberId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            Biz.LineOfBusinessCollection coll = dm.GetLineOfBusinessCollection();
            Biz.LineOfBusinessCollection ac = GetLineOfBusinessesByCompanyNumberId(companyNumberId);

            Biz.LineOfBusinessCollection newcoll = new BCS.Biz.LineOfBusinessCollection();

            foreach (Biz.LineOfBusiness lob in coll)
            {
                if (ac.FindById(lob.Id) == null)
                {
                    newcoll.Add(lob);
                }
            }
            return newcoll;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.LineOfBusiness GetLineOfBusinessById(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, id);
            return dm.GetLineOfBusiness();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <param name="lobIds"></param>
        /// <returns></returns>
        public static string UpdateAgencyLineOfBusiness(int agencyId, int[] lobIds)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.AgencyLineOfBusiness.Agency.Columns.Id, agencyId);
            Biz.AgencyLineOfBusinessCollection alobc = dm.GetAgencyLineOfBusinessCollection();

            int le = alobc.Count;
            for (int i = 0; i < le; i++)
            {
                alobc[(le - i) - 1].Delete();
            }
            dm.CommitAll();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agencyId);
            Biz.Agency agency = dm.GetAgency();

            foreach (int i in lobIds)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, i);
                Biz.LineOfBusiness lob = dm.GetLineOfBusiness();

                dm.NewAgencyLineOfBusiness(agency, lob);
            }

            try
            {
                dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return "This Agency or Line of Business is currently associated with other data and cannot be updated at this time.";
                }
                return "There was a problem updating the Agency Line of Business.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem updating the Agency Line of Business.";
            }


            return "Agency Line of Business successfully updated.";

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <param name="lobIds"></param>
        /// <returns></returns>
        public static string UpdateCompanyNumberLineOfBusiness(int companyNumberId, int[] lobIds)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberLineOfBusiness.Columns.CompanyNumberId, companyNumberId);
            Biz.CompanyNumberLineOfBusinessCollection alobc = dm.GetCompanyNumberLineOfBusinessCollection();

            int le = alobc.Count;
            for (int i = 0; i < le; i++)
            {
                alobc[(le - i) - 1].Delete();
            }
            dm.CommitAll();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, companyNumberId);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();

            foreach (int i in lobIds)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, i);
                Biz.LineOfBusiness lob = dm.GetLineOfBusiness();

                dm.NewCompanyNumberLineOfBusiness(cnum, lob);
            }

            try
            {
                dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return "This Company Number or Line of Business is currently associated with other data and cannot be updated at this time.";
                }
                return "There was a problem updating the Company Number Line of Business.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem updating the Company Number Line of Business.";
            }


            return "Company Number Line of Business successfully updated.";

        }
        #endregion

        #region CompanyNumberCodeTypes
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberCodeTypeCollection GetCompanyNumberCodeTypes(int companyNumberId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.CompanyNumberId, companyNumberId);
            return dm.GetCompanyNumberCodeTypeCollection(Biz.FetchPath.CompanyNumberCodeType.All);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <param name="companyDependent"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberCodeTypeCollection GetCompanyNumberCodeTypes(int companyNumberId, bool companyDependent)
        {
            Biz.DataManager dm = Common.GetDataManager();

            if (companyNumberId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.CompanyNumberId, companyNumberId);
            else
            {
                if (companyDependent)
                {
                    return new BCS.Biz.CompanyNumberCodeTypeCollection();
                }
            }

            // note: somehow .All does not work, so adding all the fetch paths individually probably because of circular dependency            
            //return dm.GetCompanyNumberCodeTypeCollection(Biz.FetchPath.CompanyNumberCodeType.All);
            return dm.GetCompanyNumberCodeTypeCollection(Biz.FetchPath.CompanyNumberCodeType.CompanyNumber,
                Biz.FetchPath.CompanyNumberCodeType.CompanyNumberCode,
                Biz.FetchPath.CompanyNumberCodeType.CompanyNumberCode_by_DefaultCompanyNumberCodeId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberCodeTypeCollection GetCompanyNumberCodeTypes(int companyId, int companyNumberId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            if (companyId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.CompanyNumber.Columns.CompanyId, companyId);
            if (companyNumberId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.CompanyNumberId, companyNumberId);
            // note: somehow .All does not work, so adding all the fetch paths individually probably because of circular dependency
            //return dm.GetCompanyNumberCodeTypeCollection(Biz.FetchPath.CompanyNumberCodeType.All);
            return dm.GetCompanyNumberCodeTypeCollection(Biz.FetchPath.CompanyNumberCodeType.CompanyNumber,
                Biz.FetchPath.CompanyNumberCodeType.CompanyNumberCode,
                Biz.FetchPath.CompanyNumberCodeType.CompanyNumberCode_by_DefaultCompanyNumberCodeId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberCodeType GetCompanyNumberCodeType(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.Id, id);
            // note: somehow .All does not work, so adding all the fetch paths individually probably because of circular dependency
            //return dm.GetCompanyNumberCodeType(Biz.FetchPath.CompanyNumberCodeType.All);
            return dm.GetCompanyNumberCodeType(Biz.FetchPath.CompanyNumberCodeType.CompanyNumber,
                Biz.FetchPath.CompanyNumberCodeType.CompanyNumberCode,
                Biz.FetchPath.CompanyNumberCodeType.CompanyNumberCode_by_DefaultCompanyNumberCodeId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string AddCompanyNumberCodeType(BizObjects.CodeType type)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Code, type.CodeName).And(
                Biz.JoinPath.CompanyNumberCode.Columns.CompanyCodeTypeId, type.CompanyNumberId);
            if (dm.GetCompanyNumberCode() != null)
            {
                return "This code type already exists for the company number.";
            }

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, type.CompanyNumberId);
            Biz.CompanyNumber number = dm.GetCompanyNumber();

            dm.QueryCriteria.Clear();
            Biz.CompanyNumberCodeType newType = dm.NewCompanyNumberCodeType(type.CodeName, number);
            newType.ClientSpecific = type.ClientSpecific;
            newType.DefaultCompanyNumberCodeId = type.DefaultCompanyNumberCodeId == 0 ? SqlInt32.Null : type.DefaultCompanyNumberCodeId;
            newType.DisplayOrder = type.DisplayOrder;
            newType.Required = type.Required;
            newType.VisibleInLOBGridSection = type.VisibleInLOBGridSection;
            newType.ClassCodeSpecific = type.ClassCodeSpecific;
            try
            { dm.CommitAll(); }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the Company Number Code Type."; }

            return "Company Number Code Type was successfully added.";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string UpdateCompanyNumberCodeType(BizObjects.CodeType type)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Id, type.Id);
            Biz.CompanyNumberCodeType cntype = dm.GetCompanyNumberCodeType();
            cntype.CodeName = type.CodeName;
            cntype.CompanyNumberId = type.CompanyNumberId;
            cntype.ClientSpecific = type.ClientSpecific;
            //cntype.ClassCodeSpecific = type.ClassCodeSpecific;
            cntype.DefaultCompanyNumberCodeId = type.DefaultCompanyNumberCodeId == 0 ? SqlInt32.Null : type.DefaultCompanyNumberCodeId;
            cntype.DisplayOrder = type.DisplayOrder;
            cntype.Required = type.Required;
            cntype.VisibleInLOBGridSection = type.VisibleInLOBGridSection;

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulCompanyNumberCodeTypeUpdateFK;
                }
                return "There was a problem updating the Company Number Code Type.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the Company Number Code Type."; }

            return "Company Number Code Type was successfully updated.";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string DeleteCompanyNumberCodeType(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.Id, id);
            Biz.CompanyNumberCodeType type = dm.GetCompanyNumberCodeType();
            type.Delete();
            try
            {
                string s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulCompanyNumberCodeTypeDeleteFK;
                }
                return "There was a problem deleting the Company Number Code Type.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the Company Number Code Type.";
            }

            return "Company Number Code Type was successfully deleted.";
        }
        #endregion

        #region CompanyNumberCodes
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="filterCompanyNumberId"></param>
        /// <param name="filterCodeTypeId"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberCodeCollection GetCompanyNumberCodes(int companyId,
            int filterCompanyNumberId, int filterCodeTypeId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            if (companyId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.CompanyNumber.Columns.CompanyId, companyId);
            if (filterCompanyNumberId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.CompanyNumberId, filterCompanyNumberId);
            if (filterCodeTypeId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.CompanyCodeTypeId, filterCodeTypeId);

            return dm.GetCompanyNumberCodeCollection(Biz.FetchPath.CompanyNumberCode.AllParents);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="filterCodeTypeCodeName"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberCodeCollection GetCompanyNumberCodes(string companyNumber, string filterCodeTypeCodeName)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.CompanyNumberCodeType.Columns.CodeName, filterCodeTypeCodeName);

            return dm.GetCompanyNumberCodeCollection().SortByCode(OrmLib.SortDirection.Ascending);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberCode GetCompanyNumberCode(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Id, id);
            return dm.GetCompanyNumberCode(Biz.FetchPath.CompanyNumberCode.All);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string AddCompanyNumberCode(BizObjects.CompanyNumberCode code)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Code, code.Code).And(
                Biz.JoinPath.CompanyNumberCode.Columns.CompanyCodeTypeId, code.CompanyCodeTypeId).And(
                Biz.JoinPath.CompanyNumberCode.Columns.CompanyNumberId, code.CompanyNumberId).And(
                Biz.JoinPath.CompanyNumberCode.Columns.Description, code.Description);
            if (dm.GetCompanyNumberCode() != null)
            {
                return "This code already exists for the company number.";
            }

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.Id, code.CompanyCodeTypeId);
            Biz.CompanyNumberCodeType type = dm.GetCompanyNumberCodeType();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, code.CompanyNumberId);
            Biz.CompanyNumber number = dm.GetCompanyNumber();

            dm.QueryCriteria.Clear();
            Biz.CompanyNumberCode newCode = dm.NewCompanyNumberCode(type, number);
            newCode.Description = code.Description;
            newCode.Code = code.Code;
            newCode.ExpirationDate = code.ExpirationDate == DateTime.MinValue ? SqlDateTime.Null : code.ExpirationDate;
            try
            { dm.CommitAll(); }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the Company Number Code."; }

            return "Company Number Code was successfully added.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string UpdateCompanyNumberCode(BizObjects.CompanyNumberCode code)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Id, code.Id);
            Biz.CompanyNumberCode cncode = dm.GetCompanyNumberCode();
            cncode.Code = code.Code;
            cncode.CompanyCodeTypeId = code.CompanyCodeTypeId;
            cncode.CompanyNumberId = code.CompanyNumberId;
            cncode.Description = code.Description;
            cncode.ExpirationDate = code.ExpirationDate == DateTime.MinValue ? SqlDateTime.Null : code.ExpirationDate;

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulCompanyNumberCodeUpdateFK;
                }
                return "There was a problem updating the Company Number Code.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the Company Number Code."; }

            return "Company Number Code was successfully updated.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string DeleteCompanyNumberCode(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Id, id);
            Biz.CompanyNumberCode code = dm.GetCompanyNumberCode();
            code.Delete();

            try
            {
                string s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulCompanyNumberCodeDeleteFK;
                }
                return "There was a problem deleting the Company Number Code.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the Company Number Code.";
            }

            return "Company Number Code was successfully deleted.";
        }
        #endregion

        #region Users
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public static Biz.UserCollection GetUsers(int companyId, string filterUserName)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.User.UserCompany.Columns.CompanyId, companyId);
            if (Common.IsNotNullOrEmpty(filterUserName))
                dm.QueryCriteria.And(Biz.JoinPath.User.Columns.Username, filterUserName, OrmLib.MatchType.Partial);
            Biz.UserCollection Users = dm.GetUserCollection(Biz.FetchPath.User.UserCompany, Biz.FetchPath.User.UserRole);
            if (Users != null)
            {
                Users = Users.SortByUsername(OrmLib.SortDirection.Ascending);
            }
            return Users;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.User GetUser(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.User.Columns.Id, id);
            return dm.GetUser(Biz.FetchPath.User.UserCompany, Biz.FetchPath.User.UserRole);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static string AddUser(BizObjects.User user)
        {
            if (user == null)
                return "Input user was null.";

            Biz.DataManager dm = Common.GetDataManager();
            Biz.User newUser = dm.NewUser(user.Username);
            newUser.Active = user.Active;
            newUser.AgencyId = user.AgencyId == 0 ? SqlInt32.Null : user.AgencyId;
            newUser.Notes = user.Notes;
            newUser.Segmentation = user.Segmentation;
            newUser.Password = null;
            newUser.PrimaryCompanyId = user.PrimaryCompanyId == 0 ? SqlInt32.Null : user.PrimaryCompanyId;
            Biz.UserCompany userCompany = newUser.NewUserCompany();
            userCompany.CompanyId = user.PrimaryCompanyId;

            foreach (BizObjects.Role state in user.RoleList.List)
            {
                Biz.UserRole aRole = newUser.UserRoles.FindByRoldId(state.Id);
                if (aRole == null)
                    aRole = newUser.NewUserRole();
                aRole.RoldId = state.Id;
            }

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    return "Same username already exists.";
                }
                return "There was a problem adding the User.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the User."; }

            return Messages.SuccessfulAddUser;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public static string UpdateUser(BizObjects.User user)
        {
            if (user == null)
                return "Input user was null.";

            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Agent.Columns.Id, user.Id);
            Biz.User newUser = dm.GetUser(Biz.FetchPath.User.UserRole);
            newUser.Active = user.Active;
            newUser.AgencyId = user.AgencyId == 0 ? SqlInt32.Null : user.AgencyId;
            newUser.Notes = user.Notes;
            newUser.Segmentation = user.Segmentation;
            newUser.Password = null;
            newUser.PrimaryCompanyId = user.PrimaryCompanyId == 0 ? SqlInt32.Null : user.PrimaryCompanyId;
            newUser.Username = user.Username;

            List<int> toRemove = new List<int>();
            foreach (Biz.UserRole var in newUser.UserRoles)
            {
                bool removable = true;
                // find roles not contained in new 
                if (user.RoleList != null && user.RoleList.List != null)
                {
                    foreach (BizObjects.Role bovar in user.RoleList.List)
                    {
                        if (bovar.Id == var.RoldId)
                        {
                            removable = false;
                            break;
                        }
                    }
                }
                if (removable)
                    toRemove.Add(var.RoldId);
            }
            foreach (int var in toRemove)
            {
                newUser.UserRoles.FindByRoldId(var).Delete();
            }

            foreach (BizObjects.Role role in user.RoleList.List)
            {
                Biz.UserRole aRole = newUser.UserRoles.FindByRoldId(role.Id);
                if (aRole == null)
                    aRole = newUser.NewUserRole();
                aRole.RoldId = role.Id;
            }

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return "The user cannot be updated because its associated with one or more companies.";
                }
                if (ex.Number == 2627)
                {
                    return "Same username already exists.";
                }
                return "There was a problem updating the User.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the User."; }

            return Messages.SuccessfulEditUser;
        }
        #endregion
        #region Links
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public static Biz.LinkCollection GetLinks(int companyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Link.Columns.CompanyId, companyId);
            Biz.LinkCollection links = dm.GetLinkCollection();
            if (links != null)
            {
                links = links.SortByDisplayOrder(OrmLib.SortDirection.Ascending);
            }
            return links;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.Link GetLink(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Link.Columns.Id, id);
            return dm.GetLink();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Link"></param>
        /// <returns></returns>
        public static string AddLink(BizObjects.Link link)
        {
            if (link == null)
                return "Input Link was null.";

            Biz.DataManager dm = Common.GetDataManager();
            Biz.Link newLink = dm.NewLink(link.CompanyId, link.Url);
            newLink.Description = link.Description;
            newLink.DisplayOrder = link.DisplayOrder;

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    return "Same Linkname already exists.";
                }
                return "There was a problem adding the Link.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the Link."; }

            return "Link was successfully added.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Link"></param>
        /// <returns></returns>
        public static string UpdateLink(BizObjects.Link Link)
        {
            if (Link == null)
                return "Input Link was null.";

            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Agent.Columns.Id, Link.Id);
            Biz.Link newLink = dm.GetLink();
            newLink.URL = Link.Url;
            newLink.Description = Link.Description;
            newLink.DisplayOrder = Link.DisplayOrder;

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return "The Link cannot be updated because its associated with one or more companies.";
                }
                if (ex.Number == 2627)
                {
                    return "Same Linkname already exists.";
                }
                return "There was a problem updating the Link.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the Link."; }

            return "Link was successfully updated.";
        }
        public static string DeleteLink(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Link.Columns.Id, id);
            dm.GetLink().Delete();
            try
            {
                string s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                return "There was a problem deleting the Link.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the Link.";
            }

            return "Link was successfully deleted.";
        }
        #endregion
        #region Persons
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public static Biz.PersonCollection GetPersons(int companyId, int companyNumberId, string username, string fullname)
        {
            Biz.DataManager dm = Common.GetDataManager();
            if (companyId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.Person.CompanyNumber.Columns.CompanyId, companyId);
            if (companyNumberId != 0)
                dm.QueryCriteria.And(Biz.JoinPath.Person.CompanyNumber.Columns.Id, companyNumberId);
            if (Common.IsNotNullOrEmpty(username))
                dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.UserName, username, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(fullname))
                dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.FullName, fullname, OrmLib.MatchType.Partial);
            Biz.PersonCollection persons = dm.GetPersonCollection(Biz.FetchPath.Person.CompanyNumber);
            if (persons != null)
            {
                persons = persons.SortByFullName(OrmLib.SortDirection.Ascending);
            }
            return persons;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.Person GetPerson(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.Id, id);
            return dm.GetPerson(Biz.FetchPath.Person.CompanyNumber);
        }

        public static Biz.Person GetPersonByAPSId(Int64 apsId, int companyNumberId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.APSId, apsId);
            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.CompanyNumberId, companyNumberId);
            return dm.GetPerson(Biz.FetchPath.Person.CompanyNumber);
        }

        public static int GetPersonIdByAPSId(Int64 apsId, int companyNumberId)
        {
            Biz.Person person = GetPersonByAPSId(apsId, companyNumberId);
            if (person != null)
                return person.Id;

            return 0;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static string AddPerson(BizObjects.Person person)
        {
            if (person == null)
                return "Input person was null.";

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, person.CompanyNumberId);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();

            dm.QueryCriteria.Clear();
            Biz.Person newPerson = dm.NewPerson(person.UserName, cnum);
            newPerson.FullName = person.FullName;
            newPerson.EmailAddress = person.EmailAddress;
            newPerson.Initials = person.Initials;
            newPerson.Active = person.Active;
            newPerson.RetirementDate = person.RetirementDate == DateTime.MinValue ? SqlDateTime.Null : person.RetirementDate;

            try
            { dm.CommitAll(); }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the person."; }

            return "Person was successfully added.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static string UpdatePerson(BizObjects.Person person)
        {
            if (person == null)
                return "Input person was null.";

            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Agent.Columns.Id, person.Id);
            Biz.Person newPerson = dm.GetPerson();
            newPerson.CompanyNumberId = person.CompanyNumberId;
            newPerson.EmailAddress = person.EmailAddress;
            newPerson.FullName = person.FullName;
            newPerson.UserName = person.UserName;
            newPerson.Initials = person.Initials;
            newPerson.Active = person.Active;
            newPerson.RetirementDate = person.RetirementDate == DateTime.MinValue ? SqlDateTime.Null : person.RetirementDate;

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulPersonUpdateFK;
                }
                return "There was a problem updating the person.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the person."; }

            return "Person was successfully updated.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string DeletePerson(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.Id, id);
            dm.GetPerson().Delete();
            try
            {
                string s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulPersonDeleteFK;
                }
                return "There was a problem deleting the person.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the person.";
            }

            return "Person was successfully deleted.";
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="personid"></param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgenciesOfPerson(int personid)
        {
            if (personid == 0)
                return new Biz.AgencyCollection();

            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.And(Biz.JoinPath.Agency.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, personid);
            Biz.AgencyCollection acoll = dm.GetAgencyCollection();

            if (acoll != null)
            {
                acoll = acoll.SortByAgencyName(OrmLib.SortDirection.Ascending);
            }
            foreach (Biz.Agency agency in acoll)
            {
                agency.AgencyName = string.Format("{0} ({1}){2}", agency.AgencyName, agency.AgencyNumber,
                    (agency.CancelDate.IsNull || (!agency.CancelDate.IsNull && agency.CancelDate.Value > DateTime.Today)) ? string.Empty : " (inactive)");
            }
            return acoll;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="personid"></param>
        /// <returns></returns>
        public static Biz.AgencyCollection GetAgenciesNotOfPerson(int personid)
        {
            if (personid == 0)
                return new BCS.Biz.AgencyCollection();
            Biz.AgencyCollection personAgencies = GetAgenciesOfPerson(personid);

            List<int> listAgencyIds = new List<int>();
            foreach (Biz.Agency var in personAgencies)
            {
                listAgencyIds.Add(var.Id);
            }

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.Id, personid);
            Biz.Person person = dm.GetPerson();
            int companyNumberId = person == null || person.CompanyNumberId.IsNull ? 0 : person.CompanyNumberId.Value;

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(
                Biz.JoinPath.Agency.Columns.CompanyNumberId,
                companyNumberId);
            // since there were no agencies already associated with the person, we need all agencies
            if (listAgencyIds.Count > 0)
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, listAgencyIds, OrmLib.MatchType.NotIn);
            dm = AddAgencyCancelCriteria(dm);
            Biz.AgencyCollection all = dm.GetAgencyCollection();

            //foreach (Biz.Agency i in personAgencies)
            //{
            //    all = all.FilterById(i.Id, OrmLib.CompareType.Not);
            //}
            foreach (Biz.Agency agency in all)
            {
                agency.AgencyName = string.Format("{0} ({1})", agency.AgencyName, agency.AgencyNumber);
            }
            if (all != null)
            {
                all = all.SortByAgencyName(OrmLib.SortDirection.Ascending);
            }
            return all;
        }

        public static Biz.AgencyCollection GetAgenciesNotOfPerson(int personid, string personAgencyIds)
        {
            if (personid == 0)
                return new BCS.Biz.AgencyCollection();

            if (personAgencyIds == null)
                personAgencyIds = string.Empty;

            string[] agencyids = personAgencyIds.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.Id, personid);
            Biz.Person person = dm.GetPerson();
            int companyNumberId = person == null || person.CompanyNumberId.IsNull ? 0 : person.CompanyNumberId.Value;

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(
                Biz.JoinPath.Agency.Columns.CompanyNumberId,
                companyNumberId);
            // since there were no agencies already associated with the person, we need all agencies
            if (agencyids.Length > 0)
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agencyids, OrmLib.MatchType.NotIn);
            dm = AddAgencyCancelCriteria(dm);
            Biz.AgencyCollection all = dm.GetAgencyCollection();

            //foreach (Biz.Agency i in personAgencies)
            //{
            //    all = all.FilterById(i.Id, OrmLib.CompareType.Not);
            //}
            foreach (Biz.Agency agency in all)
            {
                agency.AgencyName = string.Format("{0} ({1})", agency.AgencyName, agency.AgencyNumber);
            }
            if (all != null)
            {
                all = all.SortByAgencyName(OrmLib.SortDirection.Ascending);
            }
            return all;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="personid"></param>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static Biz.UnderwritingRoleCollection GetRolesOfPerson(int personid, int agencyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(
                Biz.JoinPath.UnderwritingRole.AgencyLineOfBusinessPersonUnderwritingRolePerson.Person.Columns.Id,
                personid);
            dm.QueryCriteria.And(
                Biz.JoinPath.UnderwritingRole.AgencyLineOfBusinessPersonUnderwritingRolePerson.Agency.Columns.Id,
                agencyId);
            return dm.GetUnderwritingRoleCollection();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="personid"></param>
        /// <param name="agencyId"></param>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public static Biz.UnderwritingRoleCollection GetRolesNotOfPerson(int personid, int agencyId, int companyNumberId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            //dm.QueryCriteria.And(
            //    Biz.JoinPath.UnderwritingRole.AgencyLineOfBusinessPersonUnderwritingRolePerson.Person.Columns.Id,
            //    personid);
            Biz.UnderwritingRoleCollection roles = GetRolesOfPerson(personid, agencyId);
            if (roles == null || roles.Count == 0)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.UnderwritingRole.CompanyNumberUnderwritingRole.Columns.CompanyNumberId, companyNumberId);
                return dm.GetUnderwritingRoleCollection();
            }

            List<int> al = new List<int>();
            foreach (Biz.UnderwritingRole role in roles)
            {
                al.Add(role.Id);
            }
            int[] rids = al.ToArray();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.UnderwritingRole.Columns.Id, rids, OrmLib.MatchType.NotIn);
            dm.QueryCriteria.And(Biz.JoinPath.UnderwritingRole.CompanyNumberUnderwritingRole.Columns.CompanyNumberId, companyNumberId);
            Biz.UnderwritingRoleCollection nonroles = dm.GetUnderwritingRoleCollection();
            return nonroles;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Biz.CompanyCollection GetCompanies()
        {
            Biz.DataManager dm = Common.GetDataManager();
            return dm.GetCompanyCollection().SortByCompanyName(OrmLib.SortDirection.Ascending);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="companyDependent"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberCollection GetCompanyNumbers(int companyId, bool companyDependent)
        {
            if (companyDependent && companyId == 0)
                return new BCS.Biz.CompanyNumberCollection();
            Biz.DataManager dm = Common.GetDataManager();
            if (companyDependent)
            {
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Company.Columns.Id, companyId);
                return dm.GetCompanyNumberCollection();
            }
            else
            {
                if (companyId != 0)
                    dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Company.Columns.Id, companyId);
                return dm.GetCompanyNumberCollection();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static BizObjects.CompanyNumberList GetCompanyNumbers(int companyId)
        {
            return Lookups.GetCompanyNumbersInternal(companyId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static BizObjects.UserList GetUsers(int companyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            Biz.UserCollection ss = dm.GetUserCollection();

            BizObjects.UserList stl = new BCS.Core.Clearance.BizObjects.UserList();
            BizObjects.User[] list = new BCS.Core.Clearance.BizObjects.User[ss.Count];

            for (int i = 0; i < ss.Count; i++)
            {
                BizObjects.User st = new BCS.Core.Clearance.BizObjects.User();
                st.Id = ss[i].Id;
                st.Username = ss[i].Username;
                list[i] = st;
            }
            stl.List = list;
            BizObjects.User user = new BCS.Core.Clearance.BizObjects.User();
            user.Username = string.Empty;
            user.Id = 0;
            stl.Insert(0, user);

            return stl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.CompanyNumber GetCompanyNumber(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, id);
            return dm.GetCompanyNumber();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public static int GetCompanyOfCompanyNumber(int companyNumberId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, companyNumberId);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();
            if (cnum == null)
                return 0;
            return cnum.CompanyId.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="agencyIds"></param>
        /// <param name="lobIds"></param>
        /// <param name="uroleIds"></param>
        /// <param name="primaryIds"></param>
        /// <param name="isPrimary"></param>
        /// <returns></returns>
        public static string UpdateAgencyLOBURPerson(int personId, int[] agencyIds, int[] lobIds, int[] uroleIds, string primaryIds, bool isPrimary)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, personId);
            dm.QueryCriteria.And(Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agencyIds, OrmLib.MatchType.In);
            Biz.AgencyLineOfBusinessPersonUnderwritingRolePersonCollection existing =
                dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection().FilterByPersonId(personId);

            // section 1 restoring the existing primaries for combination other than input primary ids
            System.Data.DataTable dt = dm.DataSet.Tables["AgencyLineOfBusinessPersonUnderwritingRolePerson"].Clone();
            foreach (Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson exi in existing)
            {
                dt.Rows.Add(new object[] { exi.AgencyId, exi.LineOfBusinessId, exi.UnderwritingRoleId, exi.PersonId, exi.Primary.Value });
            }
            // end section 1 restoring the existing primaries for combination other than input primary ids

            int le = existing.Count;

            for (int i = 0; i < le; i++)
            {
                existing[(le - i) - 1].Delete();
            }
            try
            {
                string commitstring = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return "This Agency Line of Business Underwriting Person Roles is currently associated with other data and cannot be updated at this time.";
                }
                return "There was a problem updating the Agency Line of Business Underwriting Person Roles.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the Agency Line of Business Underwriting Person Roles."; }



            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.Id, personId);
            Biz.Person person = dm.GetPerson();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agencyIds, OrmLib.MatchType.In);
            Biz.AgencyCollection agencies = dm.GetAgencyCollection();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, lobIds, OrmLib.MatchType.In);
            Biz.LineOfBusinessCollection lineofbusinesses = dm.GetLineOfBusinessCollection();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.UnderwritingRole.Columns.Id, uroleIds, OrmLib.MatchType.In);
            Biz.UnderwritingRoleCollection roles = dm.GetUnderwritingRoleCollection();

            dm.QueryCriteria.Clear();
            foreach (Biz.Agency agency in agencies)
            {
                foreach (Biz.LineOfBusiness lineofbusiness in lineofbusinesses)
                {
                    foreach (Biz.UnderwritingRole role in roles)
                    {
                        //dm.NewAgencyLineOfBusinessPersonUnderwritingRolePerson(lineofbusiness, person,
                        //    role, agency);

                        // section 2 restoring the existing primaries for combination other than input primary ids
                        Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson alobpup = dm.NewAgencyLineOfBusinessPersonUnderwritingRolePerson(
                             agency, lineofbusiness, person, role);

                        object[] findTheseVals = new object[4];
                        // Set the values of the keys to find.
                        findTheseVals[0] = agency.Id;
                        findTheseVals[1] = lineofbusiness.Id;
                        findTheseVals[2] = role.Id;
                        findTheseVals[3] = person.Id;

                        System.Data.DataRow row = dt.Rows.Find(findTheseVals);
                        if (row != null)
                        {
                            object preprimary = row["Primary"];
                            alobpup.Primary = Convert.ToBoolean(row["Primary"]);
                        }

                        // end section 2 restoring the existing primaries for combination other than input primary ids
                    }
                }
            }
            try
            {
                string commitsstring = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return "This Agency, Line of Business, Underwriting, Person, or Roles is currently associated with other data and cannot be updated at this time.";
                }
                return "There was a problem updating the This Agency Line of Business Underwriting Person Roles.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the This Agency Line of Business Underwriting Person Roles."; }


            if (primaryIds.Length > 0)
            {
                string[] aaa = primaryIds.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (aaa.Length == 3 && isPrimary)
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, aaa[0]);
                    dm.QueryCriteria.And(Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.LineOfBusinessId, aaa[1]);
                    dm.QueryCriteria.And(Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, aaa[2]);
                    Biz.AgencyLineOfBusinessPersonUnderwritingRolePersonCollection varcoll =
                        dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection();
                    varcoll = varcoll.FilterByLineOfBusinessId(aaa[1]);
                    varcoll = varcoll.FilterByUnderwritingRoleId(aaa[2]);
                    foreach (Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson var in varcoll)
                    {
                        var.Primary = personId == var.PersonId && isPrimary;
                    }
                }
                else
                {
                    BTS.LogFramework.LogCentral.Current.LogWarn(string.Empty,
                        new ArgumentException("Invalid value passed.", "primaryIds", null));
                }
            }
            try
            {
                string commitstring = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return "This Agency, Line of Business, Underwriting, Person, or Roles is currently associated with other data and cannot be updated at this time.";
                }
                return "There was a problem updating the This Agency Line of Business Underwriting Person Roles.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the This Agency Line of Business Underwriting Person Roles."; }

            return "Agency Line of Business Underwriting Person Roles were successfully updated.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fpersonId"></param>
        /// <param name="tpersonId"></param>
        /// <returns></returns>
        public static string CopyAgencyLOBURPerson(int fpersonId, int tpersonId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(
                Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, tpersonId);
            Biz.AgencyLineOfBusinessPersonUnderwritingRolePersonCollection coll =
                dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection(
                Biz.FetchPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.All).FilterByPersonId(tpersonId);

            int le = coll.Count;

            for (int i = 0; i < le; i++)
            {
                coll[(le - i) - 1].Delete();
            }
            try
            {
                string commitstring = dm.CommitAll();
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem copying This Agency Line of Business Underwriting Person Roles.";
            }

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(
                Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, fpersonId);
            coll = dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection(
                Biz.FetchPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.All).FilterByPersonId(fpersonId);

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.Id, tpersonId);
            Biz.Person person = dm.GetPerson();

            dm.QueryCriteria.Clear();
            foreach (Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson item in coll)
            {
                Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson pper =
                dm.NewAgencyLineOfBusinessPersonUnderwritingRolePerson(item.Agency, item.LineOfBusiness, person, item.UnderwritingRole);
                // note : uncomment line below to copy/move primary assignments too. OR can be based on new method parameter.
                //pper.Primary = item.Primary;
            }

            try
            {
                string commitstring = dm.CommitAll();
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem copying This Agency Line of Business Underwriting Person Roles.";
            }
            return "Agency Line of Business Underwriting Person Roles were successfully copied.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fpersonId"></param>
        /// <param name="tpersonId"></param>
        /// <returns></returns>
        public static string MoveAgencyLOBURPerson(int fpersonId, int tpersonId)
        {
            string result = CopyAgencyLOBURPerson(fpersonId, tpersonId);
            if (result != "Agency Line of Business Underwriting Person Roles were successfully copied.")
            {
                return "There was a problem moving This Agency Line of Business Underwriting Person Roles.";
            }

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(
                Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, fpersonId);
            Biz.AgencyLineOfBusinessPersonUnderwritingRolePersonCollection coll =
                dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection(
                Biz.FetchPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.All).FilterByPersonId(fpersonId);

            int le = coll.Count;

            for (int i = 0; i < le; i++)
            {
                coll[(le - i) - 1].Delete();
            }
            try
            {
                string commitstring = dm.CommitAll();
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem moving This Agency Line of Business Underwriting Person Roles.";
            }
            return "Agency Line of Business Underwriting Person Roles were successfully moved.";
        }

        /// <summary>
        /// determines if the combination of the parameters is primary combination
        /// </summary>
        /// <param name="personId">Id of the person</param>
        /// <param name="agencyId">Id of the Agency</param>
        /// <param name="LobId">Id of the Line of business</param>
        /// <param name="roleId">Id of the Underwriting role</param>
        /// <returns>primary</returns>
        public static bool IsPrimaryPerson(int personId, int agencyId, int lobId, int roleId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            // TODO: investigate - for some reason adding the criteria of agencyid, personid, lobid and roleid does not work (only agencyid condition is satisfied)
            dm.QueryCriteria.And(Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agencyId);

            Biz.AgencyLineOfBusinessPersonUnderwritingRolePersonCollection vars = dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection();
            vars = vars.FilterByPersonId(personId);
            vars = vars.FilterByLineOfBusinessId(lobId);
            vars = vars.FilterByUnderwritingRoleId(roleId);

            if (vars.Count > 1)
                throw new Exception("Inconsistent Data: There cannot be more than one row with same person, agency, lob and role.");

            if (vars.Count > 0)
            {
                Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson var = vars[0];
                if (var != null)
                {
                    if (!var.Primary.IsNull)
                    {
                        return var.Primary.Value;
                    }
                }
            }
            return false;
        }

        #region Company Number Attributes

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Biz.AttributeDataTypeCollection GetAttributeDataTypes()
        {
            Biz.DataManager dm = Common.GetDataManager();
            Biz.AttributeDataTypeCollection attributes = dm.GetAttributeDataTypeCollection();
            return attributes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberAttributeCollection GetCompanyNumberAttributes(int companyNumberId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.Columns.CompanyNumberId, companyNumberId);
            Biz.CompanyNumberAttributeCollection attributes = dm.GetCompanyNumberAttributeCollection(Biz.FetchPath.CompanyNumberAttribute.AllParents);
            return attributes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.CompanyNumberAttribute GetCompanyNumberAttribute(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.Columns.Id, id);
            return dm.GetCompanyNumberAttribute(Biz.FetchPath.CompanyNumberAttribute.AllParents);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <param name="attributeDataTypeId"></param>
        /// <param name="label"></param>
        /// <param name="clientspecific"></param>
        /// <param name="required"></param>
        /// <param name="readOnlyProperty"></param>
        /// <param name="displayOrder"></param>
        /// <returns></returns>
        public static string AddCompanyNumberAttribute(int companyNumberId, int attributeDataTypeId, string label, bool clientspecific,
            bool required, bool readOnlyProperty, short displayOrder, string uiAttributes)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, companyNumberId);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.AttributeDataType.Columns.Id, attributeDataTypeId);
            Biz.AttributeDataType adt = dm.GetAttributeDataType();

            dm.QueryCriteria.Clear();
            Biz.CompanyNumberAttribute newCompanyNumberAttribute = dm.NewCompanyNumberAttribute(label, adt, cnum);
            newCompanyNumberAttribute.ClientSpecific = clientspecific;
            newCompanyNumberAttribute.ReadOnlyProperty = readOnlyProperty;
            newCompanyNumberAttribute.DisplayOrder = displayOrder;
            newCompanyNumberAttribute.Required = required;
            newCompanyNumberAttribute.UIAttributes = uiAttributes;

            try
            { dm.CommitAll(); }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the company number attribute."; }

            return "Company Number Attribute was successfully added.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="companyNumberId"></param>
        /// <param name="attributeDataTypeId"></param>
        /// <param name="label"></param>
        /// <param name="clientspecific"></param>
        /// <param name="required"></param>
        /// <param name="readOnlyProperty"></param>
        /// <param name="displayOrder"></param>
        /// <returns></returns>
        public static string UpdateCompanyNumberAttribute(int id, int companyNumberId, int attributeDataTypeId, string label, bool clientspecific,
            bool required, bool readOnlyProperty, short displayOrder, string uiAttributes)
        {
            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.Columns.Id, id);
            Biz.CompanyNumberAttribute newCompanyNumberAttribute = dm.GetCompanyNumberAttribute();

            newCompanyNumberAttribute.AttributeDataTypeId = attributeDataTypeId;
            newCompanyNumberAttribute.CompanyNumberId = companyNumberId;
            newCompanyNumberAttribute.Label = label;
            newCompanyNumberAttribute.ClientSpecific = clientspecific;
            newCompanyNumberAttribute.ReadOnlyProperty = readOnlyProperty;
            newCompanyNumberAttribute.DisplayOrder = displayOrder;
            newCompanyNumberAttribute.Required = required;
            newCompanyNumberAttribute.UIAttributes = uiAttributes;

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulCompanyNumberAttributeUpdateFK;
                }
                return "There was a problem updating the company number attribute.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the company number attribute."; }

            return "Company Number Attribute was successfully updated.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string DeleteCompanyNumberAttribute(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.Columns.Id, id);
            dm.GetCompanyNumberAttribute().Delete();
            try
            {
                string s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulCompanyNumberAttributeDeleteFK;
                }
                return "There was a problem deleting the company number attribute.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the company number attribute.";
            }

            return "Company Number Attribute was successfully deleted.";
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static Biz.PersonCollection GetPersonsOfAgency(int agencyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agencyId);
            return dm.GetPersonCollection();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="agencyId"></param>
        /// <returns></returns>
        public static Biz.PersonCollection GetPersonsNotOfAgency(int agencyId)
        {
            Biz.PersonCollection assignedPersons = GetPersonsOfAgency(agencyId);

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agencyId);
            Biz.Agency agency = dm.GetAgency();
            int companyNumberId = agency == null || agency.CompanyNumberId.IsNull ? 0 : agency.CompanyNumberId.Value;

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.CompanyNumberId, companyNumberId);
            Biz.PersonCollection all = dm.GetPersonCollection();

            foreach (Biz.Person person in assignedPersons)
            {
                all = all.FilterById(person.Id, OrmLib.CompareType.Not);
            }
            return all;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fagencyId"></param>
        /// <param name="tagencyId"></param>
        /// <returns></returns>
        public static string CopyPersonLOBURAgency(int fagencyId, int tagencyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(
                Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, tagencyId);
            Biz.AgencyLineOfBusinessPersonUnderwritingRolePersonCollection coll =
                dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection(
                Biz.FetchPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.All).FilterByAgencyId(tagencyId);

            int le = coll.Count;

            for (int i = 0; i < le; i++)
            {
                coll[(le - i) - 1].Delete();
            }
            try
            {
                string commitstring = dm.CommitAll();
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem copying This Agency Line of Business Underwriting Person Roles.";
            }

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(
                Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, fagencyId);
            coll = dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection(
                Biz.FetchPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.All).FilterByAgencyId(fagencyId);

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, tagencyId);
            Biz.Agency agency = dm.GetAgency();

            dm.QueryCriteria.Clear();
            foreach (Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson item in coll)
            {
                Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson pper =
                dm.NewAgencyLineOfBusinessPersonUnderwritingRolePerson(agency, item.LineOfBusiness, item.Person, item.UnderwritingRole);
                //pper.Primary = item.Primary;
            }

            try
            {
                string commitstring = dm.CommitAll();
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem copying This Agency Line of Business Underwriting Person Roles.";
            }
            return "Agency Line of Business Underwriting Person Roles were successfully copied.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fagencyId"></param>
        /// <param name="tagencyId"></param>
        /// <returns></returns>
        public static string MovePersonLOBURAgency(int fagencyId, int tagencyId)
        {
            string result = CopyPersonLOBURAgency(fagencyId, tagencyId);
            if (result != "Agency Line of Business Underwriting Person Roles were successfully copied.")
            {
                return "There was a problem moving This Agency Line of Business Underwriting Person Roles.";
            }

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(
                Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, fagencyId);
            Biz.AgencyLineOfBusinessPersonUnderwritingRolePersonCollection coll =
                dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection(
                Biz.FetchPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.All).FilterByAgencyId(fagencyId);

            int le = coll.Count;

            for (int i = 0; i < le; i++)
            {
                coll[(le - i) - 1].Delete();
            }
            try
            {
                string commitstring = dm.CommitAll();
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem moving This Agency Line of Business Underwriting Person Roles.";
            }
            return "Agency Line of Business Underwriting Person Roles were successfully moved.";
        }

        #region Class Codes

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterCodeValue"></param>
        /// <param name="companyNumberId"></param>
        /// <returns></returns>
        public Biz.ClassCodeCollection GetClassCodes(string filterCodeValue, string filterDesc, int companyNumberId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CompanyNumberId, companyNumberId);
            if (Common.IsNotNullOrEmpty(filterCodeValue))
                dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CodeValue, filterCodeValue, OrmLib.MatchType.Partial);
            if (Common.IsNotNullOrEmpty(filterDesc))
                dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Description, filterDesc, OrmLib.MatchType.Partial);
            Biz.ClassCodeCollection clcc = dm.GetClassCodeCollection(Biz.FetchPath.ClassCode.SicCodeList, Biz.FetchPath.ClassCode.NaicsCodeList);
            return clcc.SortByCodeValue(OrmLib.SortDirection.Ascending);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Biz.ClassCode GetClassCode(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, id);
            Biz.ClassCode classcode = dm.GetClassCode(Biz.FetchPath.ClassCode.SicCodeList, Biz.FetchPath.ClassCode.NaicsCodeList);
            return classcode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public string DeleteClassCode(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            try
            {
                //for some reason just trying to get the exact CompanyNumberClassCode does not work. so get the collection and filter.
                //dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberClassCode.Columns.ClassCodeId, Id);
                //dm.GetCompanyNumberClassCode().Delete();
                //Biz.CompanyNumberClassCodeCollection cccc = dm.GetCompanyNumberClassCodeCollection();
                //Biz.CompanyNumberClassCode ccc = cccc.FilterByClassCodeId(Id).FilterByCompanyNumberId(companyNumberId)[0];

                DeleteClassCodeCompanyNumberCode(id);

                dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, id);
                Biz.ClassCode ccc = dm.GetClassCode();

                ccc.Delete();
                string s = dm.CommitAll();

                // TODO: determine if we need to also delete the class code entry after deleting the association from company number
                //dm.QueryCriteria.Clear();
                //dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, Id);
                //dm.GetClassCode().Delete();
                //s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulClassCodeDeleteFK;
                }
                return "There was a problem deleting the Class Code.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the Class Code.";
            }

            return "Class Code was successfully deleted.";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="classcode"></param>
        /// <returns></returns>
        public string AddClassCode(BizObjects.ClassCode classcode)
        {
            if (classcode == null)
                return "Input Class Code was null.";

            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, classcode.CompanyNumberId);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();

            Biz.ClassCode newClassCode = dm.NewClassCode(classcode.CodeValue, cnum);
            newClassCode.SicCodeId = classcode.SicCodeId == 0 ? System.Data.SqlTypes.SqlInt32.Null : classcode.SicCodeId;
            newClassCode.NaicsCodeId = classcode.NaicsCodeId == 0 ? System.Data.SqlTypes.SqlInt32.Null : classcode.NaicsCodeId;
            newClassCode.Description = classcode.Description;

            try
            {
                dm.CommitAll();

                //add class code attributes
                if (classcode.ClassCodeCompanyNumberCodes != null && classcode.ClassCodeCompanyNumberCodes.Count > 0)
                {
                    foreach (BizObjects.ClassCodeCompanyNumberCode code in classcode.ClassCodeCompanyNumberCodes)
                    {
                        AddClassCodeCompanyNumberCode(newClassCode.Id, code.CodeTypeId, code.CodeId);
                    }
                }
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    LogCentral.Current.LogWarn(ex.Message, ex);
                    return "The class code already exists. Please select a different code value or sic code.";
                }
                throw;
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the Class Code."; }

            return "Class Code was successfully added.";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="classcode"></param>
        /// <returns></returns>
        public static string UpdateClassCode(BizObjects.ClassCode classcode)
        {
            if (classcode == null)
                return "Input Class Code was null.";

            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, classcode.Id);
            Biz.ClassCode newClassCode = dm.GetClassCode();

            newClassCode.SicCodeId = classcode.SicCodeId == 0 ? System.Data.SqlTypes.SqlInt32.Null : classcode.SicCodeId;
            newClassCode.NaicsCodeId = classcode.NaicsCodeId == 0 ? System.Data.SqlTypes.SqlInt32.Null : classcode.NaicsCodeId;
            newClassCode.CodeValue = classcode.CodeValue;
            newClassCode.Description = classcode.Description;

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulClassCodeUpdateFK;
                }
                if (ex.Number == 2627)
                {
                    LogCentral.Current.LogWarn(ex.Message, ex);
                    return "The class code already exists. Please select a different code value or sic code.";
                }
                return "There was a problem updating the Class Code.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the Class Code."; }

            return "Class Code was successfully updated.";
        }

        #endregion

        public static Biz.ClientClassCodeCollection GetClassCodesHVOfAClient(int companyNumberId, long clientCoreClientId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.Client.Columns.ClientCoreClientId, clientCoreClientId);
            dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.Client.Columns.CompanyNumberId, companyNumberId);
            Biz.ClientClassCodeCollection clientClassCodes = dm.GetClientClassCodeCollection(
                Biz.FetchPath.ClientClassCode.ClassCode,
                Biz.FetchPath.ClientClassCode.ClassCode.SicCodeList,
                Biz.FetchPath.ClientClassCode.ClassCode.NaicsCodeList,
                Biz.FetchPath.ClientClassCode.ClassCode.CompanyNumberClassCodeHazardGradeValue);

            return clientClassCodes;
        }
        public static string DeleteClassCodeHVOfAClient(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.Columns.Id, id);
            Biz.ClientClassCode ccc = dm.GetClientClassCode();

            try
            {
                ccc.Delete();
                dm.CommitAll();
            }
            catch (Exception)
            {
                // TODO enhance with logging etc

                return "There was a problem deleting the class code.";
            }
            return "Class Code successfully deleted.";
        }

        public static string AddClassCodeHVOfAClient(int companyNumberId, long clientCoreClientId, int classCodeId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Client.Columns.ClientCoreClientId, clientCoreClientId);
            dm.QueryCriteria.And(Biz.JoinPath.Client.Columns.CompanyNumberId, companyNumberId);
            Biz.Client client = dm.GetClient();

            if (client == null)
            {
                // the client is in client core but not in clearance

                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, companyNumberId);
                Biz.CompanyNumber cnum = dm.GetCompanyNumber();

                client = dm.NewClient(clientCoreClientId, cnum);
            }

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, classCodeId);
            Biz.ClassCode classCode = dm.GetClassCode();

            dm.QueryCriteria.Clear();
            dm.NewClientClassCode(classCode, client);

            try
            {
                dm.CommitAll();
            }
            catch (Exception)
            {
                // TODO: enhance here 
                return "There was a problem adding the Class Code.";
            }

            return "Class Code was successfully added.";
        }

        public static string UpdateClassCodeHVOfAClient(int id, int newClassCodeId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.Columns.Id, id);
            Biz.ClientClassCode ccc = dm.GetClientClassCode();
            ccc.ClassCodeId = newClassCodeId;

            try
            {
                dm.CommitAll();
            }
            catch (Exception)
            {
                // TODO: enhance here 
                return "There was a problem updating the Class Code.";
            }

            return "Class Code was successfully updated.";
        }

        #region Class Codes Hazard Grade Values

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <param name="filterCodeValue"></param>
        /// <returns></returns>
        public static Biz.ClassCodeCollection GetClassCodesHV(int companyNumberId, string filterCodeValue)
        {
            Biz.DataManager dm = Common.GetDataManager();
            if (Common.IsNotNullOrEmpty(filterCodeValue))
                dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CodeValue, filterCodeValue, OrmLib.MatchType.Partial);
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CompanyNumberId, companyNumberId);
            Biz.ClassCodeCollection ac = dm.GetClassCodeCollection(Biz.FetchPath.ClassCode.SicCodeList,
                Biz.FetchPath.ClassCode.NaicsCodeList,
                Biz.FetchPath.ClassCode.CompanyNumberClassCodeHazardGradeValue);

            Biz.ClassCodeCollection retColl = new BCS.Biz.ClassCodeCollection();
            foreach (Biz.ClassCode cc in ac)
            {
                if (cc.CompanyNumberClassCodeHazardGradeValues.Count > 0)
                    retColl.Add(cc);
            }
            retColl = retColl.SortByCodeValue(OrmLib.SortDirection.Ascending);

            return retColl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <param name="classCodeId"></param>
        /// <returns></returns>
        public static string DeleteClassCodeHV(int companyNumberId, int classCodeId)
        {
            Biz.DataManager dm = Common.GetDataManager();

            #region delete CompanyNumberClassCodeHazardGradeValue entries
            dm.QueryCriteria.And(
                Biz.JoinPath.CompanyNumberClassCodeHazardGradeValue.Columns.CompanyNumberId, companyNumberId).And(
                Biz.JoinPath.CompanyNumberClassCodeHazardGradeValue.Columns.ClassCodeId, classCodeId);
            Biz.CompanyNumberClassCodeHazardGradeValueCollection coll = dm.GetCompanyNumberClassCodeHazardGradeValueCollection();
            coll = coll.FilterByClassCodeId(classCodeId);

            int le = coll.Count;

            for (int i = 0; i < le; i++)
            {
                coll[(le - i) - 1].Delete();
            }
            try
            {
                string s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return "This Company Number Class Code Hazard grade values can not be deleted because it is currently associated with one or more clients.";
                }
                return "There was a problem deleting the Company Number Class Code Hazard grade values";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the Company Number Class Code Hazard grade values.";
            }
            #endregion

            return "The Company Number Class Code Hazard grade values was successfully deleted.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <param name="classCodeId"></param>
        /// <param name="hazardIdValues"></param>
        /// <returns></returns>
        public static string UpdateClassCodeHV(int companyNumberId, int classCodeId, System.Collections.Specialized.HybridDictionary hazardIdValues)
        {
            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, companyNumberId);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, classCodeId);
            Biz.ClassCode classCode = dm.GetClassCode();

            dm.QueryCriteria.Clear();
            Biz.HazardGradeTypeCollection hgts = dm.GetHazardGradeTypeCollection();

            dm.QueryCriteria.Clear();
            Biz.CompanyNumberClassCodeHazardGradeValueCollection cncchgts = dm.GetCompanyNumberClassCodeHazardGradeValueCollection();
            cncchgts = cncchgts.FilterByCompanyNumberId(companyNumberId);
            cncchgts = cncchgts.FilterByClassCodeId(classCodeId);

            foreach (System.Collections.DictionaryEntry o in hazardIdValues)
            {
                Biz.CompanyNumberClassCodeHazardGradeValue aEntry =
                    cncchgts.FindByHazardGradeTypeId(o.Key.ToString());

                if (aEntry != null)
                    aEntry.HazardGradeValue = o.Value.ToString();
                else
                {
                    aEntry = dm.NewCompanyNumberClassCodeHazardGradeValue(classCode, cnum, hgts.FindById(o.Key.ToString()));
                    aEntry.HazardGradeValue = o.Value.ToString();
                }
            }
            try
            {
                dm.CommitAll();
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the Company Number Class Code Hazard grade values.";
            }

            return "Company Number Class Code Hazard grade values were successfully updated.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumberId"></param>
        /// <param name="classCodeId"></param>
        /// <param name="hazardIdValues"></param>
        /// <returns></returns>
        public static string AddClassCodeHV(int companyNumberId, int classCodeId, System.Collections.Specialized.HybridDictionary hazardIdValues)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, companyNumberId);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, classCodeId);
            Biz.ClassCode classCode = dm.GetClassCode();

            #region adding rows to CompanyNumberClassCodeHazardGradeValue

            dm.QueryCriteria.Clear();
            Biz.HazardGradeTypeCollection hgts = dm.GetHazardGradeTypeCollection();

            foreach (System.Collections.DictionaryEntry o in hazardIdValues)
            {
                Biz.CompanyNumberClassCodeHazardGradeValue newEntry = dm.NewCompanyNumberClassCodeHazardGradeValue(classCode, cnum, hgts.FindById(o.Key.ToString()));
                newEntry.HazardGradeValue = o.Value.ToString();
            }
            try
            {
                dm.CommitAll();
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    LogCentral.Current.LogWarn(ex.Message, ex);
                    return "The classcode hazardgradevalues already exists. Please select a different class code.";
                }
                throw;
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the companynumber classcode hazardgradevalues.";
            }



            #endregion

            return "Company Number Class Code Hazard grade values were successfully added.";
        }

        #endregion

        #region ClientClassCodeCompanyCodes

        public static string AddClassCodeCompanyNumberCode(int classCodeId, int companyNumberCodeTypeId, int companyNumberCodeId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, classCodeId);

            Biz.ClassCode classCode = dm.GetClassCode();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Id, companyNumberCodeId);

            Biz.CompanyNumberCode companyNumberCode = dm.GetCompanyNumberCode();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.Id, companyNumberCodeTypeId);

            Biz.CompanyNumberCodeType companyNumberCodeType = dm.GetCompanyNumberCodeType();

            try
            {

                dm.NewClassCodeCompanyNumberCode(companyNumberCode, classCode, companyNumberCodeType);
                dm.CommitAll();
            }
            catch (Exception)
            {
                return "There was a problem adding the Class Code.";
            }
            return "Class Code was successfully added.";
        }

        //public static string AddClassCodeCompanyNumberCode(long clientCoreClientId, int classCodeId,int companyNumberid, int companyNumberCodeId)
        //{
        //    Biz.DataManager dm = Common.GetDataManager();
        //    dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.Client.Columns.ClientCoreClientId, clientCoreClientId);
        //    dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.Client.Columns.CompanyNumberId, companyNumberid);
        //    dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.ClassCode.Columns.Id, classCodeId);

        //    Biz.ClientClassCode clientClassCode = dm.GetClientClassCode();

        //    dm.QueryCriteria.Clear();
        //    dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Id, companyNumberCodeId);

        //    Biz.CompanyNumberCode companyNumberCode = dm.GetCompanyNumberCode();

        //    try
        //    {
        //        dm.NewClientClassCodeCompanyNumberCode(clientClassCode, companyNumberCode);
        //        dm.CommitAll();
        //    }
        //    catch (Exception)
        //    {
        //        return "There was a problem adding the Class Code.";
        //    }
        //    return "Class Code was successfully added.";
        //}

        public static string DeleteClassCodeCompanyNumberCode(int classCodeId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCodeCompanyNumberCode.Columns.ClassCodeId, classCodeId);

            Biz.ClassCodeCompanyNumberCodeCollection ccs = dm.GetClassCodeCompanyNumberCodeCollection();

            try
            {
                for (int i = ccs.Count; i > 0; i--)
                {
                    ccs[i - 1].Delete();
                    dm.CommitAll();
                }
            }
            catch (Exception)
            {
                return "There was a problem updating the Class Code.";
            }
            return "Class Code was successfully updated.";
        }

        public static string DeleteClassCodeCompanyNumberCode(int classCodeId, int companyNumberCodeTypeId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCodeCompanyNumberCode.Columns.ClassCodeId, classCodeId);
            Biz.ClassCodeCompanyNumberCodeCollection ccs = dm.GetClassCodeCompanyNumberCodeCollection();

            Biz.ClassCodeCompanyNumberCode cc = null;

            if (ccs != null && ccs.Count > 0)
                cc = ccs.FindByCompanyNumberCodeTypeId(companyNumberCodeTypeId);

            if (cc != null)
            {
                try
                {
                    cc.Delete();
                    dm.CommitAll();
                }
                catch (Exception)
                {
                    return "There was a problem updating the Class Code.";
                }
            }
            return "Class Code was successfully updated.";
        }

        public static string UpdateClassCodeCompanyNumberCode(int classCodeId, int companyNumberCodeTypeId, int companyNumberCodeId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.ClassCodeCompanyNumberCode.Columns.ClassCodeId, classCodeId);
            Biz.ClassCodeCompanyNumberCodeCollection ccs = dm.GetClassCodeCompanyNumberCodeCollection();

            Biz.ClassCodeCompanyNumberCode cc = null;

            if (ccs != null && ccs.Count > 0)
                cc = ccs.FindByCompanyNumberCodeTypeId(companyNumberCodeTypeId);

            if (cc == null)
                return AddClassCodeCompanyNumberCode(classCodeId, companyNumberCodeTypeId, companyNumberCodeId);

            try
            {
                cc.CompanyNumberCodeId = companyNumberCodeId;
                dm.CommitAll();
            }
            catch (Exception)
            {
                return "There was a problem updating the Class Code.";
            }
            return "Class Code was successfully updated.";
        }

        #endregion

        #region Contacts

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterAgencyNumber"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static Biz.ContactCollection GetContacts(string filterAgencyNumber, int companyId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Contact.Agency.CompanyNumber.Company.Columns.Id, companyId);
            if (filterAgencyNumber != null && filterAgencyNumber.Length > 0)
                dm.QueryCriteria.And(Biz.JoinPath.Contact.Agency.Columns.AgencyNumber, filterAgencyNumber, OrmLib.MatchType.Partial);
            Biz.ContactCollection ac = dm.GetContactCollection(
                Biz.FetchPath.Contact.Agency);
            return ac;
        }

        public static Biz.ContactCollection GetContactsSort(string filterAgencyNumber, int companyId)
        {
            //Definitely not the best way to go about this...           
            Biz.ContactCollection ac = GetContacts(filterAgencyNumber, companyId);

            BCS.Biz.ContactCollection tempContacts = new BCS.Biz.ContactCollection();

            DataTable dt = new DataTable();
            dt.Columns.Add("Id", typeof(int));
            dt.Columns.Add("AgencyName", typeof(string));
            dt.Columns.Add("Name", typeof(string));

            for (int i = 0; i < ac.Count; i++)
            {
                dt.Rows.Add(ac[i].Id, ac[i].Agency.AgencyName, ac[i].Name);
            }

            DataView dv = dt.DefaultView;
            dv.Sort = "AgencyName,Name";
            dt = dv.ToTable();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int id = (int)dt.Rows[i][0];
                tempContacts.Add(ac.FindById(id));
            }

            return tempContacts;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Biz.Contact GetContact(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Contact.Columns.Id, id);
            Biz.Contact contact = dm.GetContact(Biz.FetchPath.Contact.Agency);
            if (contact != null)
            {
                if (contact.Agency != null)
                {
                    contact.Agency.AgencyName = string.Format("{0} ({1})", contact.Agency.AgencyName, contact.Agency.AgencyNumber);
                }
            }
            return contact;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string DeleteContact(int id)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Contact.Columns.Id, id);
            dm.GetContact().Delete();
            try
            {
                string s = dm.CommitAll();
            }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulContactDeleteFK;
                }
                return "There was a problem deleting the Contact.";
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem deleting the Contact.";
            }

            return "Contact was successfully deleted.";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        public static string AddContact(BizObjects.Contact contact)
        {
            if (contact == null)
                return "Input Contact was null.";

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, contact.AgencyId);
            Biz.Agency ag = dm.GetAgency();

            dm.QueryCriteria.Clear();
            Biz.Contact newContact = dm.NewContact(contact.Name, ag);
            newContact.DateOfBirth = contact.DateOfBirth == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : contact.DateOfBirth;
            newContact.Email = contact.Email;
            newContact.Fax = contact.Fax;
            newContact.PrimaryPhone = contact.PrimaryPhone;
            newContact.PrimaryPhoneExtension = contact.PrimaryPhoneExtension;
            newContact.RetirementDate = contact.RetirementDate == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : contact.RetirementDate;
            newContact.SecondaryPhone = contact.SecondaryPhone;
            newContact.SecondaryPhoneExtension = contact.SecondaryPhoneExtension;

            try
            { dm.CommitAll(); }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem adding the Contact."; }

            return "Contact was successfully added.";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        public static string UpdateContact(BizObjects.Contact contact)
        {
            if (contact == null)
                return "Input Contact was null.";

            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Contact.Columns.Id, contact.Id);
            Biz.Contact newContact = dm.GetContact();
            newContact.AgencyId = contact.AgencyId;
            newContact.DateOfBirth = contact.DateOfBirth == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : contact.DateOfBirth;
            newContact.Email = contact.Email; ;
            newContact.Fax = contact.Fax;
            newContact.Name = contact.Name;
            newContact.PrimaryPhone = contact.PrimaryPhone;
            newContact.PrimaryPhoneExtension = contact.PrimaryPhoneExtension;
            newContact.RetirementDate = contact.RetirementDate == DateTime.MinValue ? System.Data.SqlTypes.SqlDateTime.Null : contact.RetirementDate;
            newContact.SecondaryPhone = contact.SecondaryPhone;
            newContact.SecondaryPhoneExtension = contact.SecondaryPhoneExtension;

            try
            { dm.CommitAll(); }
            catch (SqlException ex)
            {
                LogCentral.Current.LogWarn(ex.Message, ex);
                if (ex.Number == 547)
                {
                    return Messages.UnsuccessfulContactUpdateFK;
                }
                return "There was a problem updating the Contact.";
            }
            catch (Exception ex)
            { LogCentral.Current.LogFatal(ex.Message, ex); return "There was a problem updating the Contact."; }

            return "Contact was successfully updated.";
        }
        #endregion

        #region Portfolios
        /// <summary>
        /// TODO: to be implemented
        /// </summary>
        /// <param name="code"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static List<Portfolio> GetPortfolios(string code, string description)
        {
            return new List<Portfolio>();
        }
        /// <summary>
        /// TODO: to be implemented
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Portfolio GetPortfolio(int id)
        {
            if (id == 0)
                return null;
            return new Portfolio();
        }

        /// <summary>
        /// TODO: to be implemented
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static string DeletePortfolio(int id)
        {
            throw new NotImplementedException("Not implemented yet");
        }
        /// <summary>
        /// TODO: to be implemented
        /// </summary>
        /// <param name="portfolio"></param>
        /// <returns></returns>
        public static string AddPortfolio(Portfolio portfolio)
        {
            throw new NotImplementedException("Not implemented yet");
        }
        /// <summary>
        /// TODO: to be implemented
        /// </summary>
        /// <param name="portfolio"></param>
        /// <returns></returns>
        public static string UpdatePortfolio(Portfolio portfolio)
        {
            throw new NotImplementedException("Not implemented yet");
        }
        #endregion

        /// <summary>
        /// Re sequences submission number and/or sequence
        /// </summary>
        /// <param name="submissionId">Submission Id of the submission to be re assigned.</param>
        /// <param name="numNewSubNo">The new submission number. 0 indicates system generated submission number.</param>
        /// <param name="numNewSeq">0 for the submission number or the sequence number indicates system generated sequence.</param>
        /// <param name="user">User updating the submission. Required.</param>
        /// <returns>Status message.</returns>
        public static string ReSequenceSubmission(int submissionId, long newSubNo, int newSeq, string user)
        {
            if (string.IsNullOrEmpty(user))
                return "User is required.";
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, submissionId);
            Biz.Submission submission = dm.GetSubmission(Biz.FetchPath.Submission.CompanyNumber);
            if (null == submission)
                return "Unable to get the submission.";
            // nothing to update
            if (submission.SubmissionNumber.Value == newSubNo && submission.Sequence.Value == newSeq)
                return Messages.SuccessfulReSequenceSubmission;

            // if none provided, generate a new sequence number
            if (newSubNo == 0 || newSeq == 0)
            {
                newSubNo = SubmissionNumberGenerator.GetSubmissionControlNumber(newSubNo, submission.CompanyNumber.PropertyCompanyNumber, out newSeq);

            }
            else // make sure there are no duplicates
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.CompanyNumberId, submission.CompanyNumberId);
                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.SubmissionNumber, newSubNo);
                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Sequence, newSeq);
                Biz.Submission dupSubmission = dm.GetSubmission(BCS.Biz.FetchPath.Submission.CompanyNumber);
                if (dupSubmission != null)
                    return "Another submission with the same submission number and sequence exists. Please try again.";

            }
            submission.SubmissionNumber = newSubNo;
            submission.Sequence = newSeq;
            submission.UpdatedBy = user;
            submission.UpdatedDt = DateTime.Now;

            dm.CommitAll();

            return Messages.SuccessfulReSequenceSubmission;
        }
    }

    /// temp class representing a portfolio
    public class Portfolio
    {
        private int id;
        private string code, description;

        /// <summary>
        /// 
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Code
        {
            get { return code; }
            set { code = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }


    }
}
