using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.Core.Clearance
{
    internal class SubmissionNumberGenerator
    {
        private static object aLock = new object();

        private static long GetCurrentSubmissionControlNumberByCompanyNumber(string companyNo)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNo);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.SubmissionNumberControl.Columns.CompanyNumberId, cnum.Id);
            Biz.SubmissionNumberControlCollection snc = dm.GetSubmissionNumberControlCollection();
            if (snc.Count == 0)
                return 0;
            snc.SortByControlNumber(OrmLib.SortDirection.Descending);
            return snc[0].ControlNumber.Value;
        }

        private static long IncrementSubmissionControlNumberByCompanyNumber(string companyNo)
        {
            long current = GetCurrentSubmissionControlNumberByCompanyNumber(companyNo);
            return ++current;
        }

        internal static long GetSubmissionControlNumber(long submissionNo , string companyNo, out int sequence)
        {
            lock (aLock)
            {
                Biz.DataManager dm = Common.GetDataManager();
                dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNo);
                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.SubmissionNumber, submissionNo);
                Biz.SubmissionCollection scoll = dm.GetSubmissionCollection();
                if (scoll == null || scoll.Count == 0)
                {
                    sequence = 1;
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.SubmissionNumberControl.CompanyNumber.Columns.PropertyCompanyNumber, companyNo);

                    long controlNo = IncrementSubmissionControlNumberByCompanyNumber(companyNo);

                    // increment and save here itself
                    Biz.SubmissionNumberControl snc = dm.GetSubmissionNumberControl();
                    if (snc != null)
                    {
                        if (sequence == 1)
                            snc.ControlNumber = Common.ConvertToSqlInt64(controlNo);
                    }
                    else
                    {
                        //dm.NewSubmissionNumberControl(10000, cnum);
                        dm.QueryCriteria.Clear();
                        dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNo);
                        
                        dm.NewSubmissionNumberControl(10000, dm.GetCompanyNumber());
                    }
                    dm.CommitAll();

                    return controlNo;
                }
                scoll = scoll.SortBySequence(OrmLib.SortDirection.Descending);

                sequence = scoll[0].Sequence.IsNull ? 1 : scoll[0].Sequence.Value + 1;
                return scoll[0].SubmissionNumber.Value;
            }
        }
    }
}
