//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Web.UI;
//using BCS.Biz;
//using System.Data;
//using System.Data.SqlTypes;
//using System.Data.SqlClient;
//using BTS.LogFramework;

//namespace BCS.Core.Clearance.APS
//{
//    public class Submissions
//    {
//        public static BizObjects.APS.Submission GetBlankSubmission()
//        {
//            return new BCS.Core.Clearance.BizObjects.APS.Submission();
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="submissionNumber"></param>
//        /// <param name="companyNumber"></param>
//        /// <returns></returns>
//        public string GetSubmissionBySubmissionNumberAndCompanyNumber(string submissionNumber, string companyNumber)
//        {
//            APSSubmissionCollection scoll = GetSubmissions(
//                new Triplet(JoinPath.APSSubmission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber, OrmLib.MatchType.Exact),
//                new Triplet(JoinPath.APSSubmission.Columns.SubmissionNumber, submissionNumber, OrmLib.MatchType.Exact));

//            scoll = scoll.SortBySequence(OrmLib.SortDirection.Descending);

//            if (scoll != null && scoll.Count > 0)
//            {
//                BizObjects.APS.Submission s =null;
//                return SerializeSubmission(scoll[0], out s);
//            }
//            return null;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="submissionNumber"></param>
//        /// <param name="companyNumber"></param>
//        /// <param name="sequence"></param>
//        /// <returns></returns>
//        public string GetSubmissionBySubmissionNumberAndCompanyNumber(
//            string submissionNumber, string companyNumber, string sequence)
//        {
//            APSSubmissionCollection scoll = GetSubmissions(
//                new Triplet(JoinPath.APSSubmission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber, OrmLib.MatchType.Exact),
//                new Triplet(JoinPath.APSSubmission.Columns.SubmissionNumber, submissionNumber, OrmLib.MatchType.Exact),
//                new Triplet(JoinPath.APSSubmission.Columns.Sequence, sequence, OrmLib.MatchType.Exact));

//            scoll = scoll.SortBySequence(OrmLib.SortDirection.Descending);

//            if (scoll != null && scoll.Count > 0)
//            {
//                BizObjects.APS.Submission s = null;
//                return SerializeSubmission(scoll[0], out s);
//            }
//            return null;
//        }

//        public string GetSubmissions(string companyNumber, params Triplet[] criteria)
//        {
//            Triplet cnTriplet = new Triplet(BCS.Biz.JoinPath.APSSubmission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber, OrmLib.MatchType.Exact);
//            List<Triplet> list = new List<Triplet>(criteria.Length + 1);
//            list.Add(cnTriplet);
//            list.AddRange(criteria);
//            return SerializeSubmissionList(GetSubmissions(list.ToArray()));
//        }

//        public string GetSubmissionById(long id)
//        {
//            BizObjects.APS.Submission s = null;
//            return GetSubmissionById(id, out s);
//        }

//        public string GetSubmissionById(long id, out BizObjects.APS.Submission submission)
//        {
//            Triplet t = new Triplet(BCS.Biz.JoinPath.APSSubmission.Columns.Id, id, OrmLib.MatchType.Exact);
//            BizObjects.APS.SubmissionList list = null;            
//            string serialized = SerializeSubmissionList(GetSubmissions(t), out list);
//            submission = list.List[0];
//            return serialized;
//        }

//        public string UpdateClientId(string companyNumber, string oldClientId, string newClientId, string insuredName, string dba, string user)
//        {
//            SqlTransaction transaction = null;

//            DataManager dm = Common.GetDataManager(out transaction);
//            dm.QueryCriteria.And(JoinPath.APSSubmission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
//            dm.QueryCriteria.And(JoinPath.APSSubmission.Columns.ClientCoreClientId, oldClientId);

//            // get submissions
//            APSSubmissionCollection submissions = dm.GetAPSSubmissionCollection(
//                FetchPath.APSSubmission.CompanyNumber,
//                FetchPath.APSSubmission.APSSubmissionClientCoreClientAddress,
//                FetchPath.APSSubmission.APSSubmissionClientCoreClientName);
//            if (null == submissions || 0 == submissions.Count)
//                return string.Format("No submissions were found for the client {0}.", oldClientId);

//            // get the new client
//            BizObjects.ClearanceClient client = Clients.GetClientByIdAsObject(companyNumber, newClientId.ToString(), true,false);

//            foreach (APSSubmission submission in submissions)
//            {
//                #region delete old address assocs
//                int addressCount = submission.APSSubmissionClientCoreClientAddresss.Count;

//                for (int i = 0; i < addressCount; i++)
//                {
//                    submission.APSSubmissionClientCoreClientAddresss[(addressCount - i) - 1].Delete();
//                }
//                #endregion
//                #region delete old name assocs
//                int nameCount = submission.APSSubmissionClientCoreClientNames.Count;

//                for (int i = 0; i < nameCount; i++)
//                {
//                    submission.APSSubmissionClientCoreClientNames[(nameCount - i) - 1].Delete();
//                }
//                #endregion 
//            }
//            dm.CommitAll();

//            foreach (APSSubmission submission in submissions)
//            {
//                #region Insured Name
//                // update only if something is passed
//                if (Common.IsNotNullOrEmpty(insuredName))
//                {
//                    submission.InsuredName = insuredName;
//                }
//                #endregion
//                #region DBA
//                // update only if something is passed
//                if (Common.IsNotNullOrEmpty(dba))
//                {
//                    submission.Dba = dba;
//                } 
//                #endregion
//                if (null != client.ClientCoreClient.Addresses)
//                {
//                    foreach (BTS.ClientCore.Wrapper.Structures.Info.Address var in client.ClientCoreClient.Addresses)
//                    {
//                        APSSubmissionClientCoreClientAddress newAddr = submission.NewAPSSubmissionClientCoreClientAddress();
//                        newAddr.ClientCoreClientAddressId = Convert.ToInt32(var.AddressId);
//                    }
//                }
//                if (null != client.ClientCoreClient.Client.Names)
//                {
//                    foreach (BTS.ClientCore.Wrapper.Structures.Info.Name var in client.ClientCoreClient.Client.Names)
//                    {
//                        APSSubmissionClientCoreClientName newAddr = submission.NewAPSSubmissionClientCoreClientName();
//                        newAddr.ClientCoreNameId = Convert.ToInt32(var.SequenceNumber);
//                    }
//                }
//                submission.ClientCoreClientId = Convert.ToInt64(client.ClientCoreClient.Client.ClientId);
//                submission.ClientCorePortfolioId = Convert.ToInt64(client.ClientCoreClient.Client.PortfolioId);
//                submission.UpdatedBy = user;
//                submission.UpdatedDt = DateTime.Now;
//            }

//            try
//            {
//                dm.CommitAll();
//                transaction.Commit();
//            }
//            catch (Exception ex)
//            {
//                transaction.Rollback();
//                LogCentral.Current.LogFatal(
//                    string.Format(
//                    "There was a problem reassigning the client id for " +
//                    "company number : '{0}', fromClientId : '{1}', toClientId : '{2}', insuredName : '{3}',  dba : '{4}'",
//                    companyNumber, oldClientId, newClientId, insuredName, dba), ex);
//                return "There was a problem reassigning the client.";
//            }

//            // assumed success if reached here
//            return "Submissions were successfully reassigned.";
//        }

//        protected APSSubmissionCollection GetSubmissions(params Triplet[] criteria)
//        {
//            bool useCache = false;
//            // TODO: where should the value come from to cache or not.
//            if (!useCache)
//            {
//                Biz.DataManager dm = Common.GetDataManager();
//                foreach (System.Web.UI.Triplet var in criteria)
//                {
//                    if (var.Third == null)
//                        var.Third = OrmLib.MatchType.Exact;
//                    dm.QueryCriteria.And((OrmLib.DataManagerBase.JoinPathRelation)var.First, var.Second, (OrmLib.MatchType)var.Third);
//                }
//                return dm.GetAPSSubmissionCollection(GetSubmissionFetchPath());
//            }

//            string dependency = string.Empty;
//            foreach (System.Web.UI.Triplet triplet in criteria)
//            {
//                dependency += string.Format("{0}{1}{2}", triplet.First, triplet.Second, triplet.Third);
//            }

//            if (System.Web.HttpContext.Current.Cache["SearchedCacheDependency"] != null)
//            {
//                if (System.Web.HttpContext.Current.Cache["SearchedCacheDependency"].ToString() != dependency)
//                { System.Web.HttpContext.Current.Cache["SearchedCacheDependency"] = dependency; }
//            }
//            else
//            { System.Web.HttpContext.Current.Cache["SearchedCacheDependency"] = dependency; }

//            Biz.APSSubmissionCollection submissions;
//            if (System.Web.HttpContext.Current.Cache["SearchedSubmissions"] == null)
//            {
//                Biz.DataManager dm = Common.GetDataManager();
//                foreach (System.Web.UI.Triplet var in criteria)
//                {
//                    dm.QueryCriteria.And((OrmLib.DataManagerBase.JoinPathRelation)var.First, var.Second, (OrmLib.MatchType)var.Third);
//                }
//                submissions = dm.GetAPSSubmissionCollection(GetSubmissionFetchPath());

//                onRemove = new System.Web.Caching.CacheItemRemovedCallback(RemovedCallback);
//                System.Web.HttpContext.Current.Cache.Insert("SearchedSubmissions", submissions,
//                    new System.Web.Caching.CacheDependency(null, new string[] { "SearchedCacheDependency" }),
//                    System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 20, 0),
//                    System.Web.Caching.CacheItemPriority.NotRemovable, onRemove);

//                return submissions;
//            }
//            else
//            {
//                submissions = System.Web.HttpContext.Current.Cache["SearchedSubmissions"] as Biz.APSSubmissionCollection;
//            }

//            return submissions;
//        }

//        private static OrmLib.DataManagerBase.FetchPathRelation[] GetSubmissionFetchPath()
//        {
//            return new OrmLib.DataManagerBase.FetchPathRelation[]
//            {
//                Biz.FetchPath.APSSubmission.All,
//                Biz.FetchPath.APSSubmission.APSSubmissionCompanyNumberCode.CompanyNumberCode,
//                Biz.FetchPath.APSSubmission.APSSubmissionCompanyNumberCode.CompanyNumberCode.CompanyNumberCodeType,
//                Biz.FetchPath.APSSubmission.APSSubmissionCompanyNumberAttributeValue.CompanyNumberAttribute,
//                Biz.FetchPath.APSSubmission.APSSubmissionCompanyNumberAttributeValue.CompanyNumberAttribute.AttributeDataType,
//                Biz.FetchPath.APSSubmission.APSSubmissionCompanyNumberAgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReason,
//                Biz.FetchPath.APSSubmission.APSSubmissionCompanyNumberAgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason,
//                Biz.FetchPath.APSSubmission.APSSubmissionLineOfBusinessChildren.CompanyNumberLOBGrid,
//                // TODO: Biz.FetchPath.APSSubmission.APSSubmissionHistory
//            };
//        }

//        private string SerializeSubmission(BCS.Biz.APSSubmission bizSubmission, out BizObjects.APS.Submission boSubmission)
//        {
//            BCS.Biz.APSSubmissionCollection bizSubmissions = new APSSubmissionCollection();
//            bizSubmissions.Add(bizSubmission);
//            BizObjects.APS.SubmissionList list = ConvertToBizObject(bizSubmissions);
//            boSubmission = list.List[0];
//            string serialized = XML.Serializer.SerializeAsXml(boSubmission);
//            return serialized;
//        }
        
//        private string SerializeSubmissionList(APSSubmissionCollection aPSSubmissionCollection)
//        {
//            BizObjects.APS.SubmissionList list = null;
//            return SerializeSubmissionList(aPSSubmissionCollection, out list);
//        }
//        private string SerializeSubmissionAsList(BCS.Biz.APSSubmission bizSubmission, out BizObjects.APS.Submission boSubmission)
//        {
//            BCS.Biz.APSSubmissionCollection bizSubmissions = new APSSubmissionCollection();
//            bizSubmissions.Add(bizSubmission);
//            BizObjects.APS.SubmissionList list = null;
//            string serialized = SerializeSubmissionList(bizSubmissions, out list);
//            boSubmission = list.List[0];
//            return serialized;
//        }
//        private string SerializeSubmissionList(BCS.Biz.APSSubmissionCollection bizSubmissions, out BizObjects.APS.SubmissionList list)
//        {
//             BizObjects.APS.SubmissionList sList = ConvertToBizObject(bizSubmissions);
//             list = sList;
//             return XML.Serializer.SerializeAsXml(sList);
//         }
//         public static APSSubmissionStatusHistoryCollection GetSubmissionStatusHistory(long submissionId)
//         {
//             if (submissionId <= 0)
//                 return null;
//             Biz.DataManager dm = Common.GetDataManager();
//             dm.QueryCriteria.And(Biz.JoinPath.APSSubmissionStatusHistory.Columns.SubmissionId, submissionId);
//             APSSubmissionStatusHistoryCollection coll = dm.GetAPSSubmissionStatusHistoryCollection(FetchPath.APSSubmissionStatusHistory.SubmissionStatus);
//             if (coll != null)
//                 coll = coll.SortByStatusChangeDt(OrmLib.SortDirection.Descending);
//             return coll;
//         }
//         public static string ToggleSubmissionStatusHistoryActive(int statusId, string user)
//         {
//             int d = 0;
//             return ToggleSubmissionStatusHistoryActive(statusId, user, ref d);
//         }
//         public static string ToggleSubmissionStatusHistoryActive(int statusId, string user, ref int newSubmissionStatusId)
//         {
//             DateTime changeDate = DateTime.Now;
//             DataManager dm = Common.GetDataManager();
//             dm.QueryCriteria.And(JoinPath.APSSubmission.APSSubmissionStatusHistory.Columns.Id, statusId);
//             APSSubmission submission = dm.GetAPSSubmission(FetchPath.APSSubmission.APSSubmissionStatusHistory);
//             if (submission == null)
//                 return "Invalid history Id provided.";
//             APSSubmissionStatusHistory history = submission.APSSubmissionStatusHistorys.FindById(statusId);
//             if (history == null)
//                 return "Invalid history Id provided.";

//             bool deactivating = history.Active.IsTrue;
//             newSubmissionStatusId = submission.SubmissionStatusId.IsNull ? 0 : submission.SubmissionStatusId.Value;

//             // update the history
//             history.Active = !history.Active;
//             history.StatusChangeDt = changeDate;
//             history.StatusChangeBy = user;

//             if (!history.PreviousSubmissionStatusId.IsNull)
//             {
//                 // we only update the submission status, when it is the same status that is getting de-activated on the history
//                 if (deactivating && submission.SubmissionStatusId == history.PreviousSubmissionStatusId)
//                 {
//                     APSSubmissionStatusHistoryCollection histories = submission.APSSubmissionStatusHistorys.FilterById(statusId, OrmLib.CompareType.Not);
//                     histories = histories.FilterByActive(true);
//                     histories = histories.SortByStatusChangeDt(OrmLib.SortDirection.Descending);
//                     if (histories != null && histories.Count > 0)
//                     {
//                         submission.UpdatedBy = user;
//                         submission.UpdatedDt = changeDate;
//                         submission.SubmissionStatusId = histories[0].PreviousSubmissionStatusId;
//                         newSubmissionStatusId = histories[0].PreviousSubmissionStatusId.Value;
//                     }
//                 }
//             }
//             try
//             {
//                 dm.CommitAll();
//                 // remove the cache dependency used to cache submissions for page
//                 System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
//             }

//             catch (Exception)
//             {
//                 return "There was a problem updating the status history.";
//             }
//             return string.Format("Status flag successfully {0}.", history.Active ? "activated" : "de-activated");
//         }

//        private BizObjects.APS.SubmissionList ConvertToBizObject(APSSubmissionCollection bizSubmissions)
//        {
//            List<BizObjects.APS.Submission> gList = new List<BCS.Core.Clearance.BizObjects.APS.Submission>(bizSubmissions.Count);
//            foreach (Biz.APSSubmission varBiz in bizSubmissions)
//            {
//                BizObjects.APS.Submission boVar = new BCS.Core.Clearance.BizObjects.APS.Submission();
//                // TODO:  boVar.Agency = null;
//                boVar.AgencyId = varBiz.AgencyId.Value;
//                // TODO:  boVar.AgencyName = null;
//                // TODO:  boVar.AgencyNumber = null;
//                // TODO:  boVar.Agent = null;
//                boVar.AgentId = varBiz.AgentId;
//                // TODO:  boVar.AgentUnspecifiedReasonList = null;
//                #region Agent unspecified region
//                if (varBiz.APSSubmissionCompanyNumberAgentUnspecifiedReasons != null && varBiz.APSSubmissionCompanyNumberAgentUnspecifiedReasons.Count > 0)
//                {
//                    boVar.AgentUnspecifiedReasonList = new BCS.Core.Clearance.BizObjects.APS.SubmissionAgentUnspecifiedReason[varBiz.APSSubmissionCompanyNumberAgentUnspecifiedReasons.Count];
//                    for (int j = 0; j < varBiz.APSSubmissionCompanyNumberAgentUnspecifiedReasons.Count; j++)
//                    {
//                        boVar.AgentUnspecifiedReasonList[j] = new BCS.Core.Clearance.BizObjects.APS.SubmissionAgentUnspecifiedReason();
//                        boVar.AgentUnspecifiedReasonList[j].CompanyNumberAgentUnspecifiedReasonId =
//                            varBiz.APSSubmissionCompanyNumberAgentUnspecifiedReasons[j].CompanyNumberAgentUnspecifiedReasonId;
//                        boVar.AgentUnspecifiedReasonList[j].Reason =
//                                varBiz.APSSubmissionCompanyNumberAgentUnspecifiedReasons[j].CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason.Reason;
//                    }
//                } 
//                #endregion
//                // TODO:  boVar.AnalystFullName = null;
//                boVar.AnalystId = varBiz.AnalystPersonId;
//                // TODO:  boVar.AnalystName = null;
//                // TODO:  boVar.AttributeList = null;                

//                #region client core client address ids
//                List<int> ccids = new List<int>();
//                foreach (Biz.APSSubmissionClientCoreClientAddress var in varBiz.APSSubmissionClientCoreClientAddresss)
//                {
//                    ccids.Add(var.ClientCoreClientAddressId);
//                }
//                boVar.ClientCoreAddressIds = new int[ccids.Count];
//                ccids.CopyTo(boVar.ClientCoreAddressIds);
//                // TODO: for the moment, we will have old element populated too to easily identify submissions with a address
//                // on the default.aspx page.
//                string[] strccids = Array.ConvertAll<int, string>(boVar.ClientCoreAddressIds, new Converter<int, string>(Convert.ToString));
//                boVar.ClientCoreAddressIdsString = string.Join(",", strccids);
//                #region TODO: stop gap for other apps to catch up
//                if (ccids.Count > 0)
//                {
//                    boVar.ClientCoreAddressId = ccids[0].ToString();
//                }
//                else
//                {
//                    boVar.ClientCoreAddressId = "0";
//                }
//                #endregion

//                #endregion
//                #region client core client name ids
//                ccids = new List<int>();
//                foreach (Biz.APSSubmissionClientCoreClientName var in varBiz.APSSubmissionClientCoreClientNames)
//                {
//                    ccids.Add(var.ClientCoreNameId);
//                }
//                boVar.ClientCoreNameIds = new int[ccids.Count];
//                ccids.CopyTo(boVar.ClientCoreNameIds);

//                #endregion
//                boVar.ClientCoreClientId = varBiz.ClientCoreClientId.Value;
//                if (!varBiz.ClientCorePortfolioId.IsNull)
//                    boVar.ClientCorePortfolioId = varBiz.ClientCorePortfolioId.Value;
//                #region Submission Comments

//                if (varBiz.APSSubmissionComments.Count > 0)
//                {

//                    // bubble up and latest comment as comment for easy display in grids
//                    boVar.Comments = varBiz.APSSubmissionComments.SortByEntryDt(OrmLib.SortDirection.Descending)[0].Comment;

//                    boVar.SubmissionComments = new BCS.Core.Clearance.BizObjects.APS.SubmissionComment[varBiz.APSSubmissionComments.Count];
//                    for (int j = 0; j < varBiz.APSSubmissionComments.Count; j++)
//                    {
//                        boVar.SubmissionComments[j] = new BCS.Core.Clearance.BizObjects.APS.SubmissionComment();

//                        boVar.SubmissionComments[j].Comment = varBiz.APSSubmissionComments[j].Comment;
//                        boVar.SubmissionComments[j].EntryBy = varBiz.APSSubmissionComments[j].EntryBy;
//                        boVar.SubmissionComments[j].EntryDt = varBiz.APSSubmissionComments[j].EntryDt.Value;
//                        boVar.SubmissionComments[j].Id = varBiz.APSSubmissionComments[j].Id;
//                        boVar.SubmissionComments[j].ModifiedBy = varBiz.APSSubmissionComments[j].ModifiedBy;
//                        boVar.SubmissionComments[j].ModifiedDt =
//                            varBiz.APSSubmissionComments[j].ModifiedDt.IsNull ?
//                            DateTime.MinValue : varBiz.APSSubmissionComments[j].ModifiedDt.Value;
//                        boVar.SubmissionComments[j].SubmissionId = varBiz.APSSubmissionComments[j].SubmissionId.Value;
//                    }
//                }
//                else
//                {
//                    boVar.SubmissionComments = new BCS.Core.Clearance.BizObjects.APS.SubmissionComment[0];
//                }
//                #endregion
//                boVar.CompanyId = varBiz.CompanyNumber.CompanyId.Value;
//                boVar.CompanyNumberId = varBiz.CompanyNumberId.Value;
//                // TODO:  boVar.Contact = null;
//                boVar.ContactId = varBiz.ContactId;
//                boVar.Dba = varBiz.Dba;
//                if (!varBiz.EffectiveDate.IsNull)
//                    boVar.EffectiveDate = varBiz.EffectiveDate.Value;
//                if (!varBiz.ExpirationDate.IsNull)
//                    boVar.ExpirationDate = varBiz.ExpirationDate.Value;
//                if(!varBiz.EstimatedWrittenPremium.IsNull)
//                    boVar.EstimatedWrittenPremium = varBiz.EstimatedWrittenPremium.Value;
//                // TODO: boVar.FirstSubmissionLineOfBusinessChild = null;
//                boVar.Id = varBiz.Id;
//                boVar.InsuredName = varBiz.InsuredName;
//                // TODO:  boVar.LineOfBusiness = null;
//                #region SubmissionLineOfBusinessChildren

//                if (varBiz.APSSubmissionLineOfBusinessChildrens.Count > 0)
//                {
//                    boVar.LineOfBusinessChildList = new BCS.Core.Clearance.BizObjects.APS.SubmissionLineOfBusinessChild[varBiz.APSSubmissionLineOfBusinessChildrens.Count];
//                    for (int j = 0; j < varBiz.APSSubmissionLineOfBusinessChildrens.Count; j++)
//                    {
//                        boVar.LineOfBusinessChildList[j] = new BCS.Core.Clearance.BizObjects.APS.SubmissionLineOfBusinessChild();

//                        boVar.LineOfBusinessChildList[j].Id = varBiz.APSSubmissionLineOfBusinessChildrens[j].Id;
//                        boVar.LineOfBusinessChildList[j].CompanyNumberLOBGridId = varBiz.APSSubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGridId.Value;
//                        boVar.LineOfBusinessChildList[j].SubmissionStatusId =
//                            varBiz.APSSubmissionLineOfBusinessChildrens[j].SubmissionStatusId.IsNull ?
//                            0 : varBiz.APSSubmissionLineOfBusinessChildrens[j].SubmissionStatusId.Value;
//                        boVar.LineOfBusinessChildList[j].SubmissionStatusReasonId =
//                            varBiz.APSSubmissionLineOfBusinessChildrens[j].SubmissionStatusReasonId.IsNull ?
//                            0 : varBiz.APSSubmissionLineOfBusinessChildrens[j].SubmissionStatusReasonId.Value;

//                        if (varBiz.APSSubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGrid != null)
//                        {
//                            boVar.LineOfBusinessChildList[j].CompanyNumberLOBGrid = new BCS.Core.Clearance.BizObjects.CompanyNumberLOBGrid();
//                            boVar.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Code = varBiz.APSSubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGrid.MulitLOBCode;
//                            boVar.LineOfBusinessChildList[j].CompanyNumberLOBGrid.CompanyNumberId = varBiz.APSSubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGrid.CompanyNumberId.Value;
//                            boVar.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Description = varBiz.APSSubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGrid.MultiLOBDesc;
//                            boVar.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Id = varBiz.APSSubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGrid.Id;
//                        }

//                        if (varBiz.APSSubmissionLineOfBusinessChildrens[j].SubmissionStatus != null)
//                        {
//                            boVar.LineOfBusinessChildList[j].SubmissionStatus = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
//                            boVar.LineOfBusinessChildList[j].SubmissionStatus.Id = varBiz.APSSubmissionLineOfBusinessChildrens[j].SubmissionStatus.Id;
//                            boVar.LineOfBusinessChildList[j].SubmissionStatus.StatusCode = varBiz.APSSubmissionLineOfBusinessChildrens[j].SubmissionStatus.StatusCode;
//                        }

//                        if (varBiz.APSSubmissionLineOfBusinessChildrens[j].SubmissionStatusReason != null)
//                        {
//                            boVar.LineOfBusinessChildList[j].SubmissionStatusReason = new BCS.Core.Clearance.BizObjects.SubmissionStatusReason();
//                            boVar.LineOfBusinessChildList[j].SubmissionStatusReason.Id = varBiz.APSSubmissionLineOfBusinessChildrens[j].SubmissionStatusReason.Id;
//                            boVar.LineOfBusinessChildList[j].SubmissionStatusReason.ReasonCode = varBiz.APSSubmissionLineOfBusinessChildrens[j].SubmissionStatusReason.ReasonCode;
//                        }
//                    }
//                    #region first lob child
//                    int index0 = 0;
//                    {
//                        boVar.FirstSubmissionLineOfBusinessChild = new BCS.Core.Clearance.BizObjects.APS.FirstSubmissionLineOfBusinessChild();
//                        boVar.FirstSubmissionLineOfBusinessChild.Id = varBiz.APSSubmissionLineOfBusinessChildrens[index0].Id;
//                        boVar.FirstSubmissionLineOfBusinessChild.LobId = varBiz.APSSubmissionLineOfBusinessChildrens[index0].CompanyNumberLOBGridId.Value;

//                        if (varBiz.APSSubmissionLineOfBusinessChildrens[index0].CompanyNumberLOBGrid != null)
//                        {
//                            boVar.FirstSubmissionLineOfBusinessChild.LobCode = varBiz.APSSubmissionLineOfBusinessChildrens[index0].CompanyNumberLOBGrid.MulitLOBCode;
//                            boVar.FirstSubmissionLineOfBusinessChild.LobDescription = varBiz.APSSubmissionLineOfBusinessChildrens[index0].CompanyNumberLOBGrid.MultiLOBDesc;
//                        }
//                    }
//                    #endregion
//                }
//                else
//                {
//                    boVar.LineOfBusinessChildList = new BCS.Core.Clearance.BizObjects.APS.SubmissionLineOfBusinessChild[0];
//                }
//                #endregion
//                boVar.LineOfBusinessId = varBiz.UnderwritingUnitId.Value;
//                // TODO:  boVar.LineOfBusinessList = null;
//                // TODO:  boVar.OtherCarrier = null;
//                if(!varBiz.OtherCarrierId.IsNull)
//                    boVar.OtherCarrierId = varBiz.OtherCarrierId.Value;
//                if (!varBiz.OtherCarrierPremium.IsNull)
//                    boVar.OtherCarrierPremium = varBiz.OtherCarrierPremium.Value;
//                boVar.PolicyNumber = varBiz.PolicyNumber;                
//                if (!varBiz.PolicySystemId.IsNull)
//                    boVar.PolicySystemId = varBiz.PolicySystemId.Value;
//                // TODO: boVar.PreviousUnderwriterFullName = null;
//                #region previous underwriter
//                // TODO: analyze if it is better to add a foreign key and get histories in one go
//                DataManager dm = Common.GetDataManager();
//                dm.QueryCriteria.And(JoinPath.APSSubmissionHistory.Columns.SubmissionId, varBiz.Id);
//                APSSubmissionHistoryCollection histories = dm.GetAPSSubmissionHistoryCollection();
//                histories = histories.FilterByUnderwriterPersonId(null, OrmLib.CompareType.Not);
//                histories = histories.FilterByUnderwriterPersonId(boVar.UnderwriterId, OrmLib.CompareType.Not);
//                histories = histories.SortByHistoryDt(OrmLib.SortDirection.Descending);
//                if (histories.Count > 0)
//                {
//                    boVar.PreviousUnderwriterId = histories[0].UnderwriterPersonId;
//                } 
//                #endregion
//                // TODO:  boVar.PreviousUnderwriterName = null;
//                boVar.SubmissionBy = varBiz.SubmissionBy;
//                #region Company number codes
//                if (varBiz.APSSubmissionCompanyNumberCodes != null && varBiz.APSSubmissionCompanyNumberCodes.Count > 0)
//                {
//                    boVar.SubmissionCodeTypess = new BCS.Core.Clearance.BizObjects.APS.SubmissionCodeType[varBiz.APSSubmissionCompanyNumberCodes.Count];
//                    for (int j = 0; j < varBiz.APSSubmissionCompanyNumberCodes.Count; j++)
//                    {
//                        boVar.SubmissionCodeTypess[j] = new BCS.Core.Clearance.BizObjects.APS.SubmissionCodeType();
//                        boVar.SubmissionCodeTypess[j].CodeName = varBiz.APSSubmissionCompanyNumberCodes[j].CompanyNumberCode.CompanyNumberCodeType.CodeName;
//                        boVar.SubmissionCodeTypess[j].CodeValue = varBiz.APSSubmissionCompanyNumberCodes[j].CompanyNumberCode.Code;
//                        boVar.SubmissionCodeTypess[j].CodeId = varBiz.APSSubmissionCompanyNumberCodes[j].CompanyNumberCode.Id;
//                        boVar.SubmissionCodeTypess[j].CodeDescription = varBiz.APSSubmissionCompanyNumberCodes[j].CompanyNumberCode.Description;
//                        boVar.SubmissionCodeTypess[j].CodeTypeId = varBiz.APSSubmissionCompanyNumberCodes[j].CompanyNumberCode.CompanyNumberCodeType.Id;
//                    }
//                } 
//                #endregion
//                boVar.SubmissionDt = varBiz.SubmissionDt.Value;
//                boVar.SubmissionNumber = varBiz.SubmissionNumber.Value;
//                boVar.SubmissionNumberSequence = varBiz.Sequence.Value;

//                // status
//                if (!varBiz.SubmissionStatusId.IsNull)
//                    boVar.SubmissionStatusId = varBiz.SubmissionStatusId.Value;
//                if (varBiz.SubmissionStatus != null)
//                {
//                    boVar.SubmissionStatus = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
//                    boVar.SubmissionStatus.Id = varBiz.SubmissionStatus.Id;
//                    boVar.SubmissionStatus.StatusCode = varBiz.SubmissionStatus.StatusCode;
//                }
//                // status reason
//                if (!varBiz.SubmissionStatusReasonId.IsNull)
//                {
//                    boVar.SubmissionStatusReasonId = varBiz.SubmissionStatusReasonId.Value;
//                    boVar.SubmissionStatusReason = varBiz.SubmissionStatusReason.ReasonCode;
//                }
//                if (!varBiz.SubmissionStatusDate.IsNull)
//                    boVar.SubmissionStatusDate = varBiz.SubmissionStatusDate.Value;
//                boVar.SubmissionStatusReasonOther = varBiz.SubmissionStatusReasonOther;

//                // type
//                boVar.SubmissionTypeId = varBiz.SubmissionTypeId.Value;
//                if (varBiz.SubmissionType != null)
//                {
//                    boVar.SubmissionType = new BCS.Core.Clearance.BizObjects.SubmissionType();
//                    boVar.SubmissionType.Abbreviation = varBiz.SubmissionType.Abbreviation;
//                    boVar.SubmissionType.Id = varBiz.SubmissionType.Id;
//                    boVar.SubmissionType.TypeName = varBiz.SubmissionType.TypeName;
//                }

//                boVar.UnderwriterId = varBiz.UnderwriterPersonId;
//                // TODO: boVar.UnderwriterName = null;
//                boVar.UpdatedBy = varBiz.UpdatedBy;
//                if (!varBiz.UpdatedDt.IsNull)
//                    boVar.UpdatedDt = varBiz.UpdatedDt.Value;
                
//                // finally add to the generic list
//                gList.Add(boVar);
//            }

//            BizObjects.APS.SubmissionList sList = new BCS.Core.Clearance.BizObjects.APS.SubmissionList();
//            sList.List = gList.ToArray();
//            return sList;
//        }

//        public string AddSubmission(string clientid, string clientaddressids, string clientnameids, string clientportfolioid,
//            string submissionBy, string submissionDate, int typeid, long agencyid, string agentid, string agentreasonids,
//            string contactid, string companynumber, int statusid, string statusdate, int statusreasonid, string statusreasonother,
//            string policynumber, decimal premium, string cocodeids, string attributes, string effective, string expiration,
//            string underwriterid, string analystid, string technicianid, int policysystemid, string comments, long lobid, string lobchildren, string insuredname,
//            string dba, string subno, int othercarrierid, decimal othercarrierpremium)
//        {
//            #region Input Validations
//            string failedStatus = "Failure";
//            string requiredMessage = "Required :-{0}.";
//            BizObjects.APS.Submission s = new BCS.Core.Clearance.BizObjects.APS.Submission();
//            #region submission by
//            if (string.IsNullOrEmpty(submissionBy))
//            {
//                s.Status = failedStatus;
//                s.Message = string.Format(requiredMessage, "Submission By");
//                return XML.Serializer.SerializeAsXml(s);
//            } 
//            #endregion
//            #region client id
//            if (string.IsNullOrEmpty(clientid))
//            {
//                s.Status = failedStatus;
//                s.Message = string.Format(requiredMessage, "Client Id");
//                return XML.Serializer.SerializeAsXml(s);
//            }
//            long clientIdInt=0;
//            if (!Int64.TryParse(clientid, out clientIdInt))
//            {
//                s.Status = failedStatus;
//                s.Message = string.Format(requiredMessage, "Valid Client Id");
//                return XML.Serializer.SerializeAsXml(s);
//            }
//            #endregion
//            #region client address ids
//            if (string.IsNullOrEmpty(clientaddressids))
//            {
//                s.Status = failedStatus;
//                s.Message = string.Format(requiredMessage, "Client Address Id");
//                return XML.Serializer.SerializeAsXml(s);
//            }
//            string[] arrClientAddressIds = clientaddressids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
//            int[] arrIntClientAddressIds;
//            try
//            {
//                arrIntClientAddressIds = Array.ConvertAll<string, int>(arrClientAddressIds, new Converter<string, int>(Convert.ToInt32));
//            }
//            catch (FormatException)
//            {
//                s.Status = failedStatus;
//                s.Message = string.Format(requiredMessage, "Valid Client Address Id");
//                return XML.Serializer.SerializeAsXml(s);
//            }

//            #endregion
//            #region client name ids, not required
//            int[] arrIntClientNameIds = new int[0];
//            if (Common.IsNotNullOrEmpty(clientnameids))
//            {
//                string[] arrClientNameIds = clientnameids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
//                try
//                {
//                    arrIntClientNameIds = Array.ConvertAll<string, int>(arrClientNameIds, new Converter<string, int>(Convert.ToInt32));
//                }
//                catch (FormatException)
//                {
//                    s.Status = failedStatus;
//                    s.Message = string.Format(requiredMessage, "Valid Client Name Ids");
//                    return XML.Serializer.SerializeAsXml(s);
//                }
//            }

//            #endregion
//            #region agency
//            if (agencyid <= 0)
//            {
//                s.Status = failedStatus;
//                s.Message = string.Format(requiredMessage, "Valid Agency");
//                return XML.Serializer.SerializeAsXml(s);
//            } 
//            #endregion
//            #region type
//            if (typeid <= 0)
//            {
//                s.Status = failedStatus;
//                s.Message = string.Format(requiredMessage, "Valid Type.");
//                return XML.Serializer.SerializeAsXml(s);
//            }
//            #endregion
//            #region uw unit
//            if (lobid <= 0)
//            {
//                s.Status = failedStatus;
//                s.Message = string.Format(requiredMessage, "Valid Underwriting Unit.");
//                return XML.Serializer.SerializeAsXml(s);
//            }
//            #endregion

//            #endregion
//            #region Data Validations
//            #region company number
//            DataManager dm = Common.GetDataManager();
//            // get company number
//            dm.QueryCriteria.And(JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companynumber);
//            BCS.Biz.CompanyNumber cNumber = dm.GetCompanyNumber();
//            if (cNumber == null)
//            {
//                s.Status = failedStatus;
//                s.Message = string.Format(requiredMessage, "Valid Company Number");
//                return XML.Serializer.SerializeAsXml(s);
//            }
//        #endregion

//            // TODO: Do we do validations if above and their associations are DB valid?
//            #endregion

//            DateTime dtNow = DateTime.Now;

//            BizObjects.Submission boSubmission = new BizObjects.Submission();
//            APSSubmission biSubmission =  cNumber.NewAPSSubmission();
//            biSubmission.ClientCoreClientId = clientIdInt;

//            #region Client Addresses
//            foreach (int clientaddressid in arrIntClientAddressIds)
//            {
//                APSSubmissionClientCoreClientAddress newAddress = biSubmission.NewAPSSubmissionClientCoreClientAddress();
//                newAddress.ClientCoreClientAddressId = clientaddressid;

//            }
//            #endregion
//            #region Client Names
//            foreach (int clientnameid in arrIntClientNameIds)
//            {
//                APSSubmissionClientCoreClientName newName = biSubmission.NewAPSSubmissionClientCoreClientName();
//                newName.ClientCoreNameId = clientnameid;
//            }
//            #endregion
//            biSubmission.ClientCorePortfolioId = Convert.ToInt64(clientportfolioid);
//            biSubmission.SubmissionBy = submissionBy;
//            biSubmission.SubmissionDt = string.IsNullOrEmpty(submissionDate) ? dtNow : Convert.ToDateTime(submissionDate);
//            biSubmission.UpdatedBy = submissionBy;
//            biSubmission.UpdatedDt = string.IsNullOrEmpty(submissionDate) ? dtNow : Convert.ToDateTime(submissionDate);
//            biSubmission.SubmissionTypeId = typeid;
//            biSubmission.AgencyId = agencyid;
//            biSubmission.AgentId = agentid;

//            // TODO: should they be added only if agent is not specified?
//            #region Agent Unspecified reasons
//            if (Common.IsNotNullOrEmpty(agentreasonids))
//            {
//                string[] tagentreasonids = agentreasonids.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
//                foreach (string tarids in tagentreasonids)
//                {
//                    APSSubmissionCompanyNumberAgentUnspecifiedReason newSubmissionAgentUnspecifiedReason =
//                        biSubmission.NewAPSSubmissionCompanyNumberAgentUnspecifiedReason();
//                    newSubmissionAgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReasonId = Convert.ToInt32(tarids);
//                }
//            } 
//            #endregion
//            biSubmission.ContactId = contactid;
//            biSubmission.SubmissionStatusId = statusid == 0 ? SqlInt32.Null : statusid;
//            biSubmission.SubmissionStatusDate = Common.ConvertToSqlDateTime(statusdate);
//            #region Status History
//            APSSubmissionStatusHistory submissionStatusHistory = biSubmission.NewAPSSubmissionStatusHistory();
//            submissionStatusHistory.PreviousSubmissionStatusId = statusid == 0 ? System.Data.SqlTypes.SqlInt32.Null : statusid;
//            submissionStatusHistory.SubmissionStatusDate = Common.IsNotNullOrEmpty(statusdate) ? Common.ConvertToSqlDateTime(statusdate) :
//                System.Data.SqlTypes.SqlDateTime.Null;
//            submissionStatusHistory.StatusChangeBy = submissionBy; 
//            #endregion
//            biSubmission.SubmissionStatusReasonId = statusreasonid == 0 ? SqlInt32.Null : statusreasonid;
//            biSubmission.SubmissionStatusReasonOther = statusreasonother;
//            biSubmission.PolicyNumber = policynumber;
//            biSubmission.EstimatedWrittenPremium = premium;
//            #region company codes
//            if (Common.IsNotNullOrEmpty(cocodeids))
//            {
//                string[] codeids = cocodeids.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
//                foreach (string codeid in codeids)
//                {
//                    if (codeid != "0")
//                    {
//                        APSSubmissionCompanyNumberCode newCode = biSubmission.NewAPSSubmissionCompanyNumberCode();
//                        newCode.CompanyNumberCodeId = Convert.ToInt32(codeid);
//                    }
//                }
//            }
//            #endregion
//            #region attributes, format : id1=value1|id2=value2
//            if (Common.IsNotNullOrEmpty(attributes))
//            {
//                string[] attributeIdValues = attributes.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
//                foreach (string attributeIdValue in attributeIdValues)
//                {
//                    string[] splitIdValue = attributeIdValue.Split("=".ToCharArray()); // RHS may be an empty string
//                    APSSubmissionCompanyNumberAttributeValue newAttr = biSubmission.NewAPSSubmissionCompanyNumberAttributeValue();
//                    newAttr.CompanyNumberAttributeId = Convert.ToInt32(splitIdValue[0]);
//                    newAttr.AttributeValue = splitIdValue[1];

//                }
//            }

//            #endregion
//            biSubmission.EffectiveDate = Common.ConvertToSqlDateTime(effective);
//            biSubmission.ExpirationDate = Common.ConvertToSqlDateTime(expiration);
//            biSubmission.UnderwriterPersonId = underwriterid;
//            biSubmission.AnalystPersonId = analystid;
//            biSubmission.TechnicianPersonId = technicianid;
//            biSubmission.PolicySystemId = policysystemid == 0 ? SqlInt32.Null : policysystemid;
//            biSubmission.UnderwritingUnitId = lobid;

//            #region Submission Line Of Business Children. format : lobid1=statusid1=reasonid1=deleteflag1|lobid2=statusid2=reasonid2=deleteflag2
//            if (Common.IsNotNullOrEmpty(lobchildren))
//            {
//                string[] lobchildrenarray = lobchildren.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
//                foreach (string lobchild in lobchildrenarray)
//                {
//                    string[] lobchildparts = lobchild.Split("=".ToCharArray(), StringSplitOptions.None);

//                    // index 0 contains Id, index length-1 contains deleted, since this is a submission add there cannot be a delete flag, but checking neverthless
//                    if (lobchildparts[0] == "0" && lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
//                        continue;

//                    APSSubmissionLineOfBusinessChildren nslobc = biSubmission.NewAPSSubmissionLineOfBusinessChildren();
//                    nslobc.CompanyNumberLOBGridId = Convert.ToInt32(lobchildparts[1]);
//                    nslobc.SubmissionStatusId = lobchildparts[2] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[2]);
//                    nslobc.SubmissionStatusReasonId = lobchildparts[3] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[3]);
//                } 
//            }


//            #endregion
//            #region Submission Comments

//            if (comments == null)
//                comments = string.Empty;
//            string[] lobcommentarray = comments.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
//            foreach (string lobchild in lobcommentarray)
//            {
//                string[] lobchildparts = lobchild.Split(new string[] { "=====" }, StringSplitOptions.None);

//                // index 0 contains Id, index length-1 contains deleted
//                if (lobchildparts[0] == "0" && lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
//                    continue;

//                Biz.APSSubmissionComment nslobc = biSubmission.NewAPSSubmissionComment();
//                nslobc.Comment = lobchildparts[1];
//                nslobc.EntryBy = lobchildparts[2];
//                nslobc.EntryDt = Convert.ToDateTime(lobchildparts[3]);
//            }


//            #endregion

//            biSubmission.InsuredName = insuredname;
//            biSubmission.Dba = dba;            
//            #region Submission Number and sequence
//            int sequence = 0;
//            long submissioNo = 0;
//            Int64.TryParse(subno, out submissioNo);
//            long controlNo =
//            SubmissionNumberGenerator.GetSubmissionControlNumber(submissioNo, companynumber, out sequence);
//            biSubmission.SubmissionNumber = controlNo;
//            biSubmission.Sequence = sequence;
//            #endregion

//            biSubmission.OtherCarrierId = othercarrierid == 0 ? SqlInt32.Null : othercarrierid;
//            biSubmission.OtherCarrierPremium = othercarrierpremium;


//            biSubmission.SystemDt = dtNow;

//            string result = dm.CommitAll();
//            if (biSubmission != null)
//            {
//                BizObjects.APS.Submission resultSubmission = null;
//                string str = GetSubmissionById(biSubmission.Id, out resultSubmission);
//                resultSubmission.Message += Messages.SuccessfulAddSubmission;
//                resultSubmission.Status = "Success";
//                System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
//                return XML.Serializer.SerializeAsXml(resultSubmission);
//            }
//            else
//            {
//                s.Message += Messages.UnsuccessfulAddSubmission;
//                s.Status = "Failure";
//                return XML.Serializer.SerializeAsXml(s);
//            }

//        }

//        public string ModifySubmission(string id, long clientid, string clientaddressids, string clientnameids,
//            long clientportfolioid, int typeid, long agencyid, string agentid, string agentreasonids, string contactid, int companynumberid,
//            int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string cocodeids, string attributes, decimal premium,
//            string effective, string expiration, string underwriterid, string analystid, string technicianid, int policysystemid, string comments, string updatedby,
//            long lineofbusinessesid, string lobchildren, string insuredname, string dba, int othercarrierid, decimal othercarrierpremium)
//        {
//            // TODO: User transaction, since there are intermediate CommitAlls

//            SqlTransaction transaction = null;

//            DateTime dtNow = DateTime.Now;
//            #region Input Validations
//            string failedStatus = "Failure";
//            string requiredMessage = "Required :-{0}.";
//            if (!Common.IsNotNullOrEmpty(id))
//            {
//                BizObjects.APS.Submission failedSub = new BizObjects.APS.Submission();
//                failedSub.Message = string.Format(requiredMessage, "Id");
//                failedSub.Status = failedStatus;
//                return XML.Serializer.SerializeAsXml(failedSub);
//            }

//            if (!Common.IsNotNullOrEmpty(updatedby))
//            {
//                BizObjects.APS.Submission failedSub = new BizObjects.APS.Submission();
//                failedSub.Message = string.Format(requiredMessage, "updated by");
//                failedSub.Status = failedStatus;
//                return XML.Serializer.SerializeAsXml(failedSub);
//            }
//            if (companynumberid == 0)
//            {
//                BizObjects.APS.Submission failedSub = new BizObjects.APS.Submission();
//                failedSub.Message = string.Format(requiredMessage, "Company Number Id");
//                failedSub.Status = failedStatus;
//                return XML.Serializer.SerializeAsXml(failedSub);
//            }
//            if (agencyid == 0)
//            {
//                BizObjects.APS.Submission failedSub = new BizObjects.APS.Submission();
//                failedSub.Message = string.Format(requiredMessage, "Agency");
//                failedSub.Status = failedStatus;
//                return XML.Serializer.SerializeAsXml(failedSub);
//            }
//            if (lineofbusinessesid == 0)
//            {
//                BizObjects.APS.Submission failedSub = new BizObjects.APS.Submission();
//                failedSub.Message = string.Format(requiredMessage, "Underwriting Unit");
//                failedSub.Status = failedStatus;
//                return XML.Serializer.SerializeAsXml(failedSub);
//            }
//            #endregion

//            #region Data validations
//            DataManager dm = Common.GetDataManager(out transaction);
//            dm.QueryCriteria.And(JoinPath.APSSubmission.Columns.Id, id);
//            APSSubmission submission = dm.GetAPSSubmission(GetSubmissionFetchPath());

//            if (submission == null)
//            {
//                BizObjects.Submission failedSub = new BCS.Core.Clearance.BizObjects.Submission();
//                failedSub.Message = string.Format("Submission with Id {0} does not exist.", id);
//                failedSub.Status = "Failure";
//                return XML.Serializer.SerializeAsXml(failedSub);
//            }
//            // TODO: Do we do validations if above and their associations are DB valid?
//            #endregion

//            // required by db schema, validated above
//            submission.CompanyNumberId = companynumberid;
//            // required by db schema, validated above
//            submission.AgencyId = agencyid;
//            // required by db schema, validated above
//            submission.UnderwritingUnitId = lineofbusinessesid;
//            // required by db schema, validated above
//            submission.SubmissionTypeId = typeid;

//            submission.AgentId = agentid;
//            submission.ContactId = contactid;

//            if (clientid != 0)
//                submission.ClientCoreClientId = Common.ConvertToSqlInt64(clientid);
//            else
//                submission.ClientCoreClientId = SqlInt64.Null;

//            submission.EffectiveDate = Common.ConvertToSqlDateTime(effective);

//            submission.EstimatedWrittenPremium = premium;
//            submission.OtherCarrierPremium = othercarrierpremium;

//            if (Common.IsNotNullOrEmpty(expiration))
//                submission.ExpirationDate = Common.ConvertToSqlDateTime(expiration);
//            else
//                submission.ExpirationDate = SqlDateTime.Null;

//            submission.PolicyNumber = policynumber.Trim();

//            submission.InsuredName = insuredname;

//            submission.Dba = dba;

//            if (clientportfolioid != 0)
//                submission.ClientCorePortfolioId = Common.ConvertToSqlInt64(clientportfolioid);
//            else
//                submission.ClientCorePortfolioId = submission.ClientCoreClientId;

//            #region Status and StatusHistory update
//            System.Data.SqlTypes.SqlInt32 prevStatus = submission.SubmissionStatusId;
//            if ((prevStatus.IsNull && statusid == 0) && (submission.SubmissionStatusDate == Common.ConvertToSqlDateTime(statusdate)))
//            { }
//            else
//            {
//                int prevstatus = prevStatus.IsNull ? 0 : prevStatus.Value;
//                if (prevstatus != statusid || (submission.SubmissionStatusDate.IsNull) || submission.SubmissionStatusDate != Common.ConvertToSqlDateTime(statusdate))
//                {
//                    dm.QueryCriteria.Clear();
//                    dm.QueryCriteria.And(JoinPath.SubmissionStatus.Columns.Id, statusid);
//                    SubmissionStatus submissionStatus = dm.GetSubmissionStatus();

//                    if (submissionStatus != null)
//                    {
//                        APSSubmissionStatusHistory newAPSSubmissionStatusHistory = submission.NewAPSSubmissionStatusHistory();
//                        // required, note made not required on db schema due to changes but made required in dal. name is misleading, it actually refers to current status
//                        newAPSSubmissionStatusHistory.PreviousSubmissionStatusId = statusid;
//                        newAPSSubmissionStatusHistory.StatusChangeBy = updatedby;
//                        newAPSSubmissionStatusHistory.SubmissionStatusDate = Common.ConvertToSqlDateTime(statusdate);
//                    }
//                }
//            }
//            if (statusid != 0)
//                submission.SubmissionStatusId = statusid;
//            else
//                submission.SubmissionStatusId = SqlInt32.Null;
//            if (Common.IsNotNullOrEmpty(statusdate))
//                submission.SubmissionStatusDate = Common.ConvertToSqlDateTime(statusdate);
//            else
//                submission.SubmissionStatusDate = SqlDateTime.Null;

//            if (statusreasonid != 0)
//                submission.SubmissionStatusReasonId = statusreasonid;
//            else
//                submission.SubmissionStatusReasonId = System.Data.SqlTypes.SqlInt32.Null;

//            submission.SubmissionStatusReasonOther = statusreasonother;
//            #endregion

//            #region Submission Attributes
//            if (Common.IsNotNullOrEmpty(attributes))
//            {
//                string[] sa = attributes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
//                foreach (string ts in sa)
//                {
//                    string[] ta = ts.Split("=".ToCharArray());
//                    APSSubmissionCompanyNumberAttributeValue var = submission.APSSubmissionCompanyNumberAttributeValues.FindByCompanyNumberAttributeId(ta[0]);
//                    if (var != null)
//                    {
//                        if (var.AttributeValue == ta[1])
//                            continue;
//                        else
//                        {
//                            var.AttributeValue = ta[1];
//                        }
//                    }
//                    else
//                    {
//                        // TODO: do we validate company number attribute?
//                        APSSubmissionCompanyNumberAttributeValue newAPSSubmissionCompanyNumberAttributeValue = submission.NewAPSSubmissionCompanyNumberAttributeValue();
//                        newAPSSubmissionCompanyNumberAttributeValue.CompanyNumberAttributeId = Convert.ToInt32(ta[0]);
//                        newAPSSubmissionCompanyNumberAttributeValue.AttributeValue = ta[1];
//                    }
//                }
//            } 
//            #endregion

//            submission.UnderwriterPersonId = underwriterid;
//            submission.AnalystPersonId = analystid;
//            submission.TechnicianPersonId = technicianid;

//            if (othercarrierid != 0)
//                submission.OtherCarrierId = othercarrierid;
//            else
//                submission.OtherCarrierId = SqlInt32.Null;

//            if (policysystemid != 0)
//                submission.PolicySystemId = policysystemid;
//            else
//                submission.PolicySystemId = SqlInt32.Null;

//            submission.UpdatedBy = updatedby;
//            submission.UpdatedDt = dtNow;

//            #region New Address handling
//            MarkDelete(submission.APSSubmissionClientCoreClientAddresss);
//            #region TODO: analyzing the method above
//            //int le = submission.APSSubmissionClientCoreClientAddresss.Count;

//            //for (int i = 0; i < le; i++)
//            //{
//            //    submission.APSSubmissionClientCoreClientAddresss[(le - i) - 1].Delete();
//            //} 
//            #endregion

//            dm.CommitAll();

//            if (Common.IsNotNullOrEmpty(clientaddressids))
//            {
//                string[] arrAddressids = clientaddressids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
//                foreach (string anAddressId in arrAddressids)
//                {
//                    submission.NewAPSSubmissionClientCoreClientAddress().ClientCoreClientAddressId = Convert.ToInt32(anAddressId);
//                }
//            }

//            #endregion
//            #region New Name handling
//            MarkDelete(submission.APSSubmissionClientCoreClientNames);
//            #region TODO: analyzing the method above
//            //le = submission.APSSubmissionClientCoreClientNames.Count;

//            //for (int i = 0; i < le; i++)
//            //{
//            //    submission.APSSubmissionClientCoreClientNames[(le - i) - 1].Delete();
//            //}
//            #endregion

//            dm.CommitAll();

//            if (Common.IsNotNullOrEmpty(clientnameids))
//            {
//                string[] arrNameids = clientnameids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
//                foreach (string aNameId in arrNameids)
//                {
//                    submission.NewAPSSubmissionClientCoreClientName().ClientCoreNameId = Convert.ToInt32(aNameId);
//                }
//            }

//            #endregion


//            ModifySubmissionCompanyNumberCodes(submission, cocodeids);
           
//            #region Submission Line Of Business Children

//            if (Common.IsNotNullOrEmpty(lobchildren))
//            {
//                string[] lobchildrenarray = lobchildren.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
//                foreach (string lobchild in lobchildrenarray)
//                {
//                    string[] lobchildparts = lobchild.Split("=".ToCharArray(), StringSplitOptions.None);

//                    // index 0 contains Id, index length-1 contains deleted
//                    if (lobchildparts[0] == "0")
//                    {
//                        if (lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
//                            continue;
//                        else
//                        {
//                            APSSubmissionLineOfBusinessChildren child = submission.NewAPSSubmissionLineOfBusinessChildren();
//                            child.CompanyNumberLOBGridId = Convert.ToInt32(lobchildparts[1]);
//                            child.SubmissionStatusId = lobchildparts[2] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[2]);
//                            child.SubmissionStatusReasonId = lobchildparts[3] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[3]);
//                        }
//                    }
//                    else
//                    {
//                        if (lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
//                        {
//                            submission.APSSubmissionLineOfBusinessChildrens.FindById(lobchildparts[0]).Delete();
//                        }
//                        else
//                        {
//                            APSSubmissionLineOfBusinessChildren child = submission.APSSubmissionLineOfBusinessChildrens.FindById(lobchildparts[0]);
//                            child.CompanyNumberLOBGridId = Convert.ToInt32(lobchildparts[1]);
//                            child.SubmissionStatusId = lobchildparts[2] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[2]);
//                            child.SubmissionStatusReasonId = lobchildparts[3] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[3]);
//                        }
//                    }
//                }
//            }


//            #endregion
//            #region Submission Comment

//            if (Common.IsNotNullOrEmpty(comments))
//            {
//                string[] lobcommentrenarray = comments.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
//                foreach (string lobcomment in lobcommentrenarray)
//                {
//                    string[] lobcommentparts = lobcomment.Split(new string[] { "=====" }, StringSplitOptions.None);

//                    // index 0 contains Id, index length-1 contains deleted
//                    if (lobcommentparts[0] == "0")
//                    {
//                        if (lobcommentparts[lobcommentparts.Length - 1].ToLower() == "true")
//                            continue;
//                        else
//                        {
//                            Biz.APSSubmissionComment comment = submission.NewAPSSubmissionComment();
//                            comment.Comment = lobcommentparts[1];
//                            comment.EntryBy = lobcommentparts[2];
//                            comment.EntryDt = Convert.ToDateTime(lobcommentparts[3]);
//                        }
//                    }
//                    else
//                    {
//                        if (lobcommentparts[lobcommentparts.Length - 1].ToLower() == "true")
//                        {
//                            submission.APSSubmissionComments.FindById(lobcommentparts[0]).Delete();
//                        }
//                        else
//                        {
//                            Biz.APSSubmissionComment comment = submission.APSSubmissionComments.FindById(lobcommentparts[0]);
//                            if (comment.Comment != lobcommentparts[1])
//                            {
//                                comment.Comment = lobcommentparts[1];
//                                comment.ModifiedBy = lobcommentparts[2];
//                                comment.ModifiedDt = Convert.ToDateTime(lobcommentparts[3]);
//                            }
//                        }
//                    }
//                }
//            }
//            #endregion

//            #region Agent Unspecified reasons
//            Dictionary<int, int> oldReasons = new Dictionary<int, int>();
//            foreach (APSSubmissionCompanyNumberAgentUnspecifiedReason bizreason in submission.APSSubmissionCompanyNumberAgentUnspecifiedReasons)
//            {
//                oldReasons.Add(bizreason.CompanyNumberAgentUnspecifiedReasonId, bizreason.CompanyNumberAgentUnspecifiedReasonId);
//            }
//            Dictionary<int, int> newReasons = new Dictionary<int, int>();
//            if (agentreasonids == null)
//                agentreasonids = string.Empty;
//            string[] tagentreasonids = agentreasonids.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
//            foreach (string st in tagentreasonids)
//            {
//                newReasons.Add(Convert.ToInt32(st), Convert.ToInt32(st));
//            }
//            List<int> reasonsToBeInserted = new List<int>();
//            List<int> reasonsToBeDeleted = new List<int>();


//            foreach (int intVar in oldReasons.Keys)
//            {
//                if (newReasons.ContainsKey(intVar))
//                {
                    
//                }
//                else
//                    reasonsToBeDeleted.Add(oldReasons[intVar]);
//            }
//            foreach (int intVar in newReasons.Keys)
//            {
//                if (oldReasons.ContainsKey(intVar))
//                { /* we have already taken care of this condition in previous loop */ }
//                else
//                {
//                    reasonsToBeInserted.Add(newReasons[intVar]);
//                }
//            }
//            foreach (int intVar in reasonsToBeInserted)
//            {
//                APSSubmissionCompanyNumberAgentUnspecifiedReason submissionCompanyNumberAgentUnspecifiedReason =
//                    submission.NewAPSSubmissionCompanyNumberAgentUnspecifiedReason();
//                submissionCompanyNumberAgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReasonId = intVar;
//            }
//            foreach (int intVar in reasonsToBeDeleted)
//            {
//                submission.APSSubmissionCompanyNumberAgentUnspecifiedReasons.FindByCompanyNumberAgentUnspecifiedReasonId(intVar).Delete();
//            }
//            #endregion


//            try
//            {
//                dm.CommitAll();
//                transaction.Commit();
//            }
//            catch (Exception ex)
//            {
//                LogCentral.Current.LogFatal("Unable to modify submission.", ex);
//                BizObjects.APS.Submission failedSubmission = new BCS.Core.Clearance.BizObjects.APS.Submission();
//                failedSubmission.Status = failedStatus;
//                failedSubmission.Message = "Unable to modify submission.";
//                return XML.Serializer.SerializeAsXml(failedSubmission);                
//            }

//            // TODO: handle like add submission
//            dm = Common.GetDataManager();
//            dm.QueryCriteria.And(JoinPath.APSSubmission.Columns.Id, id);
//            APSSubmission submission1 = dm.GetAPSSubmission(GetSubmissionFetchPath());
//            BizObjects.APS.Submission s = null;
//            string resultant = SerializeSubmissionAsList(submission1, out s);
//            s.Message += Messages.SuccessfulEditSubmission;
//            s.Status = "Success";
//            // remove the cache dependency used to cache submissions for page
//            System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
//            return XML.Serializer.SerializeAsXml(s);
//        }

//        private static void MarkDelete(APSSubmissionClientCoreClientAddressCollection collection)
//        {
//            int le = collection.Count;

//            for (int i = 0; i < le; i++)
//            {
//                collection[(le - i) - 1].Delete();
//            }
//        }
//        private static void MarkDelete(APSSubmissionClientCoreClientNameCollection collection)
//        {
//            int le = collection.Count;

//            for (int i = 0; i < le; i++)
//            {
//                collection[(le - i) - 1].Delete();
//            }
//        }
//        private static void ModifySubmissionCompanyNumberCodes(BCS.Biz.APSSubmission submission, string codeids)
//        {
//            Dictionary<int, int> oldValues = new Dictionary<int, int>();
//            foreach (APSSubmissionCompanyNumberCode cnc in submission.APSSubmissionCompanyNumberCodes)
//            {
//                oldValues.Add(cnc.CompanyNumberCode.CompanyCodeTypeId.Value, cnc.CompanyNumberCodeId);
//            }

//            Dictionary<int, int> newValues = new Dictionary<int, int>();

//            DataManager dm = Common.GetDataManager();
//            dm.QueryCriteria.And(
//                JoinPath.CompanyNumberCode.Columns.Id,
//                codeids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries),
//                OrmLib.MatchType.In);
//            CompanyNumberCodeCollection coll = dm.GetCompanyNumberCodeCollection();
//            foreach (CompanyNumberCode cnc in coll)
//            {
//                newValues.Add(cnc.CompanyCodeTypeId.Value, cnc.Id);
//            }

//            List<int> toBeInserted = new List<int>();
//            List<int> toBeDeleted = new List<int>();
//            Dictionary<int, int[]> toBeUpdated = new Dictionary<int, int[]>();


//            foreach (int intVar in oldValues.Keys)
//            {
//                if (newValues.ContainsKey(intVar))
//                {
//                    if (newValues[intVar] == oldValues[intVar])
//                    {
//                        /* leave alone -  */
//                    }
//                    else
//                    {
//                        toBeUpdated.Add(intVar, new int[] { oldValues[intVar], newValues[intVar] });
//                    }
//                }
//                else
//                    toBeDeleted.Add(oldValues[intVar]);
//            }
//            foreach (int intVar in newValues.Keys)
//            {
//                if (oldValues.ContainsKey(intVar))
//                { /* we have already taken care of this condition in previous loop */ }
//                else
//                {
//                    toBeInserted.Add(newValues[intVar]);
//                }
//            }

//            foreach (int[] intVar in toBeUpdated.Values)
//            {
//                submission.APSSubmissionCompanyNumberCodes.FindByCompanyNumberCodeId(intVar[0]).CompanyNumberCodeId = intVar[1];
//            }
//            foreach (int intVar in toBeInserted)
//            {
//                APSSubmissionCompanyNumberCode sCode = submission.NewAPSSubmissionCompanyNumberCode();
//                sCode.CompanyNumberCodeId = intVar;
//            }
//            foreach (int intVar in toBeDeleted)
//            {
//                submission.APSSubmissionCompanyNumberCodes.FindByCompanyNumberCodeId(intVar).Delete();
//            }
//        }
//        static bool itemRemoved = false;
//        static System.Web.Caching.CacheItemRemovedReason reason;
//        static System.Web.Caching.CacheItemRemovedCallback onRemove = null;
//        public static void RemovedCallback(String k, Object v, System.Web.Caching.CacheItemRemovedReason r)
//        {
//            itemRemoved = true;
//            reason = r;
//        }

//        internal string SearchByClientCoreClientIdAndAgencyId(string companyNumber, long clientid, long agencyid, int pageSize, int pageNo)
//        {
//            if (agencyid >= 0)
//            {
//                BCS.APS.agency[] agencies = new BCS.APS.DataSources().GetAgencyWithSlaves(companyNumber, agencyid);

//                if (null == agencies)
//                    return SerializeSubmissionList(null);
//                long[] agencyIds = new long[agencies.Length + 1];
//                for (int i = 0; i < agencies.Length; i++)
//                {
//                    agencyIds[i] = agencies[i].Id;
//                }
//                agencyIds[agencies.Length] = agencyid;

//                System.Web.UI.Triplet[] triplets = new System.Web.UI.Triplet[]{
//                    new Triplet(JoinPath.APSSubmission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber),
//                new System.Web.UI.Triplet(Biz.JoinPath.APSSubmission.Columns.ClientCoreClientId, clientid.ToString(), OrmLib.MatchType.Exact),
//                new System.Web.UI.Triplet(Biz.JoinPath.APSSubmission.Columns.AgencyId, agencyIds, OrmLib.MatchType.In)};
//                Biz.APSSubmissionCollection submissions = GetSubmissions(triplets);

//                return SerializeSubmissionList(submissions);
//            }
//            else
//            {
//                System.Web.UI.Triplet[] triplets = new System.Web.UI.Triplet[]{
//                    new Triplet(JoinPath.APSSubmission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber),
//                new System.Web.UI.Triplet(Biz.JoinPath.APSSubmission.Columns.ClientCoreClientId, clientid.ToString(), OrmLib.MatchType.Exact)};
//                Biz.APSSubmissionCollection submissions = GetSubmissions(triplets);

//                return SerializeSubmissionList(submissions);
//            }
//        }

//        public string UpdatePersonAssignments(string[] submissionIds, string personid, string rolecolumn, string user)
//        {
//            Type t = typeof(APSSubmission);
//            System.Reflection.PropertyInfo pi = t.GetProperty(rolecolumn);

//            DataManager dm = Common.GetDataManager();
//            dm.QueryCriteria.And(JoinPath.APSSubmission.Columns.Id, submissionIds, OrmLib.MatchType.In);
//            APSSubmissionCollection ss = dm.GetAPSSubmissionCollection();
//            foreach (APSSubmission var in ss)
//            {
//                pi.SetValue(var, personid, null);

//                var.UpdatedBy = user;
//                var.UpdatedDt = Common.ConvertToSqlDateTime(DateTime.Now);
//            }

//            try
//            {
//                dm.CommitAll();
//                System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
//            }
//            catch (Exception ex)
//            {
//                BTS.LogFramework.LogCentral.Current.LogFatal(ex.Message, ex);
//                return "There was a problem updating the submissions.";
//            }
//            return "Submissions successfully updated.";
//        }

//        /// <summary>
//        /// deletes existing addresses and adds new ones provided
//        /// </summary>
//        /// <param name="submissionId"></param>
//        /// <param name="addressids"></param>
//        /// <returns></returns>
//        public static string UpdateSubmissionClientAddresses(long submissionId, string clientaddressids)
//        {
//            DataManager dm = Common.GetDataManager();
//            dm.QueryCriteria.And(JoinPath.APSSubmission.Columns.Id, submissionId);

//            APSSubmission submission = dm.GetAPSSubmission();

//            int le = submission.APSSubmissionClientCoreClientAddresss.Count;

//            for (int i = 0; i < le; i++)
//            {
//                submission.APSSubmissionClientCoreClientAddresss[(le - i) - 1].Delete();
//            }

//            try
//            {
//                dm.CommitAll();
//            }
//            catch (Exception)
//            {
//                return "There was a problem updating the client core addresses.";
//            }

//            if (Common.IsNotNullOrEmpty(clientaddressids))
//            {
//                string[] arrAddressids = clientaddressids.Split("|".ToCharArray());
//                foreach (string anAddressId in arrAddressids)
//                {
//                    submission.NewAPSSubmissionClientCoreClientAddress().ClientCoreClientAddressId = Convert.ToInt32(anAddressId);
//                }
//            }
//            try
//            {
//                dm.CommitAll();
//            }
//            catch (Exception)
//            {
//                return "There was a problem updating the client core addresses.";
//            }
//            System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
//            return "Addresses were successfully updated.";
//        }

//        internal string GetSubmissionsByPolicyNumberAndCompanyNumber(string policyNumber, string companyNumber)
//        {
//            Biz.DataManager dm = new DataManager(Common.DSN);
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.APSSubmission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber).And(
//                Biz.JoinPath.APSSubmission.Columns.PolicyNumber, policyNumber);
//            Biz.APSSubmissionCollection scoll = dm.GetAPSSubmissionCollection(GetSubmissionFetchPath());
//            if (scoll != null)
//                scoll = scoll.SortBySequence(OrmLib.SortDirection.Descending);

//            string xml = SerializeSubmissionList(scoll);
//            return xml;
//        }
//    }
//}
