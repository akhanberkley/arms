using System;
using System.Xml;
using System.Data;

using structures = BTS.ClientCore.Wrapper.Structures;
using logs = BTS.LogFramework;
using System.Collections.Generic;
using BTS.ClientCore.WS.SF;
using BTS.ClientCore.WS;


namespace BCS.Core.Clearance
{
    /// <summary>
    /// 
    /// </summary>
    public static class ClientsEF
    {
        #region Get Client Methods

        /// <summary>
        /// method gets the client by Id
        /// </summary>
        public static string GetClientByTaxId(string companyNumber, string taxid)
        {
            //BizObjects.Company ucompany = Common.BuildCompany(companyNumber);
            structures.Search.SearchClient client = new BTS.ClientCore.Wrapper.Structures.Search.SearchClient();
            //structures.Search.ClientInfo info = new BTS.ClientCore.Wrapper.Structures.Search.ClientInfo();

            //client.TaxId = taxid.Replace("-", "*") + "*";
            taxid = taxid.Replace("-","");
            string newid = string.Empty;
            foreach (char c in taxid)
                newid += c + "*";

            client.TaxId = newid;       
            client.TaxIdType = "";

            #region Old Perform Search CC Call
            //BTS.ClientCore.WS.ClientCoreWebServiceService ccws = Common.GetClientCoreWebServiceService(companyNumber);
            //string resXML = ccws.performClientSearch(Common.GetCommonBasicXml(ucompany), Common.BuildClientSearchXml(client));
            #endregion

            #region New Perform Search CC Call
            string resXML = Common.InvokeperformClientSearch(companyNumber, client);
            #endregion

            XmlDocument doc = new XmlDocument();

            doc.LoadXml(resXML);
            XmlNode node = Common.RemoveTypeAttributes(doc);

            resXML = node.InnerXml;

            // serialize the resXML and return as one of the client structures            
            try
            {
                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                structures.Search.Vector o = (structures.Search.Vector)XML.Deserializer.Deserialize(resXML, typeof(structures.Search.Vector));
                if (Common.IsNotNullOrEmpty(o.Error))
                    logs.LogCentral.Current.LogInfo(string.Format("Search Failed, Reason : '{0}', criteria - taxid : '{1}'", o.Error, taxid));
                if (Common.IsNotNullOrEmpty(o.Message))
                    logs.LogCentral.Current.LogInfo(string.Format("Search Failed, Reason : '{0}', criteria - taxid : '{1}'", o.Message, taxid));

                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// method gets business clients by name
        /// </summary>
        public static string GetClientByBusinessName(string companyNumber, string businessName)
        {
            //BizObjects.Company ucompany = Common.BuildCompany(companyNumber);

            structures.Search.SearchClient client = new BTS.ClientCore.Wrapper.Structures.Search.SearchClient();

            client.BusinessName = companyNumber == "40" ? businessName : "*" + businessName + "*";
            //client.BusinessName = "*" + businessName + "*";
            client.ClientType = string.Empty;
            client.SearchType = string.Empty;

            #region Old Perform Search CC Call
            //BTS.ClientCore.WS.ClientCoreWebServiceService ccws = Common.GetClientCoreWebServiceService(companyNumber);
            //string resXML = ccws.performClientSearch(Common.GetCommonBasicXml(ucompany), Common.BuildClientSearchXml(client));
            #endregion
            #region New Perform Search CC Call
            string resXML = Common.InvokeperformClientSearch(companyNumber, client);
            #endregion

            // serialize the resXML and return as one of the client structures
            XmlDocument doc = new XmlDocument();

            doc.LoadXml(resXML);
            XmlNode node = Common.RemoveTypeAttributes(doc);

            resXML = node.InnerXml;

            // serialize the resXML and return as one of the client structures            
            try
            {
                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                structures.Search.Vector o = (structures.Search.Vector)XML.Deserializer.Deserialize(resXML, typeof(structures.Search.Vector));
                if (Common.IsNotNullOrEmpty(o.Error))
                    logs.LogCentral.Current.LogInfo(string.Format("Search Failed, Reason : '{0}', criteria - businessName: '{1}'", o.Error, businessName));
                if (Common.IsNotNullOrEmpty(o.Message))
                    logs.LogCentral.Current.LogInfo(string.Format("Search Failed, Reason : '{0}', criteria - businessName: '{1}'", o.Message, businessName));

                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// method gets individual clients by name
        /// </summary>
        public static string GetClientsByName(string companyNumber, string firstName, string lastName)
        {
            BizObjects.Company ucompany = Common.BuildCompany(companyNumber);
            structures.Search.SearchClient client = new BTS.ClientCore.Wrapper.Structures.Search.SearchClient();
            //structures.Search.ClientName name = new BTS.ClientCore.Wrapper.Structures.Search.ClientName();
            client.FirstName = firstName + "*";
            client.LastName = lastName + "*";
            //client.Names = new BTS.ClientCore.Wrapper.Structures.Search.ClientName[1] { name };

            #region Old Perform Search CC Call
            //BTS.ClientCore.WS.ClientCoreWebServiceService ccws = Common.GetClientCoreWebServiceService(companyNumber);
            //string resXML = ccws.performClientSearch(Common.GetCommonBasicXml(ucompany), Common.BuildClientSearchXml(client)); 
            #endregion

            #region New Perform Search CC Call
            string resXML = Common.InvokeperformClientSearch(companyNumber, client);
            #endregion

            XmlDocument doc = new XmlDocument();

            doc.LoadXml(resXML);
            XmlNode node = Common.RemoveTypeAttributes(doc);

            resXML = node.InnerXml;

            // serialize the resXML and return as one of the client structures            
            try
            {
                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                structures.Search.Vector o = (structures.Search.Vector)XML.Deserializer.Deserialize(resXML, typeof(structures.Search.Vector));
                if (Common.IsNotNullOrEmpty(o.Error))
                    logs.LogCentral.Current.LogInfo(string.Format("Search Failed, Reason : '{0}', criteria - lastName, firstname : '{1}, {2}'", o.Error, lastName, firstName));
                if (Common.IsNotNullOrEmpty(o.Message))
                    logs.LogCentral.Current.LogInfo(string.Format("Search Failed, Reason : '{0}', criteria - lastName, firstname : '{1}, {2}'", o.Message, lastName, firstName));
                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// method gets individual clients by name
        /// </summary>
        public static string GetClientsByName(string companyNumber, string firstName, string lastName, string middlename,
            string suffix, string prefix)
        {
            BizObjects.Company ucompany = Common.BuildCompany(companyNumber);
            structures.Search.SearchClient client = new BTS.ClientCore.Wrapper.Structures.Search.SearchClient();
            //structures.Search.ClientName name = new BTS.ClientCore.Wrapper.Structures.Search.ClientName();
            client.FirstName = firstName;
            client.LastName = lastName;
            client.MiddleName = middlename;
            //client. = prefix;
            //client.NameSuffix = suffix;
            //client.Names = new BTS.ClientCore.Wrapper.Structures.Search.ClientName[1] { name };

            #region Old Perform Search CC Call
            //BTS.ClientCore.WS.ClientCoreWebServiceService ccws = Common.GetClientCoreWebServiceService(companyNumber);
            //string resXML = ccws.performClientSearch(Common.GetCommonBasicXml(ucompany), Common.BuildClientSearchXml(client)); 
            #endregion

            #region New Perform Search CC Call
            string resXML = Common.InvokeperformClientSearch(companyNumber, client);
            #endregion

            XmlDocument doc = new XmlDocument();

            doc.LoadXml(resXML);
            XmlNode node = Common.RemoveTypeAttributes(doc);

            resXML = node.InnerXml;

            // serialize the resXML and return as one of the client structures            
            try
            {
                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                object o = XML.Deserializer.Deserialize(resXML, typeof(structures.Search.Vector));
                //return (structures.Vector)o;
                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// method gets clients by addresses
        /// </summary>
        public static string GetClientsByAddress(string companyNumber, string houseNo, string address1, string address2,
            string address3, string address4, string city, string state, string county, string country,
            string zip)
        {
            BizObjects.Company ucompany = Common.BuildCompany(companyNumber);

            structures.Search.SearchClient client = new BTS.ClientCore.Wrapper.Structures.Search.SearchClient();
            client.SearchType = "Client";
            client.ClientType = string.Empty;

            //structures.Search.AddressVersion av = new BTS.ClientCore.Wrapper.Structures.Search.AddressVersion();
            client.HouseNumber = houseNo;
            client.ZipCode = zip;
            client.StateName = state;
            client.CountryName = country;
            client.CityName = city + "*";
            client.Address1 = address1 + "*";
            client.CountyName = county;
            //client.Addresses = new BTS.ClientCore.Wrapper.Structures.Search.AddressVersion[1] { av };

            #region Old Perform Search CC Call
            //BTS.ClientCore.WS.ClientCoreWebServiceService ccws = Common.GetClientCoreWebServiceService(companyNumber);
            //string resXML = ccws.performClientSearch(Common.GetCommonBasicXml(ucompany), Common.BuildClientSearchXml(client));
            #endregion
            #region New Perform Search CC Call
            string resXML = Common.InvokeperformClientSearch(companyNumber, client);
            #endregion

            XmlDocument doc = new XmlDocument();

            doc.LoadXml(resXML);
            XmlNode node = Common.RemoveTypeAttributes(doc);

            resXML = node.InnerXml;

            // serialize the resXML and return as one of the client structures            
            try
            {
                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                structures.Search.Vector o = (structures.Search.Vector)XML.Deserializer.Deserialize(resXML, typeof(structures.Search.Vector));
                if (Common.IsNotNullOrEmpty(o.Error))
                    logs.LogCentral.Current.LogInfo(string.Format("Search Failed, Reason : '{0}', criteria - address : '{1} {2} {3} {4} {5} {6}'", o.Error, houseNo, address1, address2, city, country, zip));
                if (Common.IsNotNullOrEmpty(o.Message))
                    logs.LogCentral.Current.LogInfo(string.Format("Search Failed, Reason : '{0}', criteria - address : '{1} {2} {3} {4} {5} {6}'", o.Message, houseNo, address1, address2, city, country, zip));

                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// method gets clients
        /// </summary>
        private static string GetClient(string companyNumber, structures.Search.SearchClient client)
        {
            #region Old Perform Search CC Call
            //BizObjects.Company ucompany = Common.BuildCompany(companyNumber);
            //BTS.ClientCore.WS.ClientCoreWebServiceService ws = Common.GetClientCoreWebServiceService(companyNumber);
            //string resXML = ws.performClientSearch(Common.GetCommonBasicXml(ucompany), Common.BuildClientSearchXml(client));            
            #endregion


            #region New Perform Search CC Call
            string resXML = Common.InvokeperformClientSearch(companyNumber, client);
            #endregion

            // serialize the resXML and return as one of the client structures
            XmlDocument doc = new XmlDocument();

            doc.LoadXml(resXML);
            XmlNode node = Common.RemoveTypeAttributes(doc);

            resXML = node.InnerXml;

            // serialize the resXML and return as one of the client structures            
            try
            {
                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                object o = XML.Deserializer.Deserialize(resXML, typeof(structures.Search.Vector));
                //return (structures.Vector)o;
                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// method gets client by Id.
        /// </summary>        
        public static string GetClientById(string companyNumber, string id, bool includeClientCoreInfo, bool isSearchScreen)
        {
            BizObjects.ClearanceClient clientObj = GetClientByIdAsObject(companyNumber, id, includeClientCoreInfo, isSearchScreen);
            return XML.Serializer.SerializeAsXml(clientObj);
        }
        /// <summary>
        /// method gets client by Id.
        /// </summary>        
        public static BizObjects.ClearanceClient GetClientByIdAsObject(string companyNumber, string id, bool includeClientCoreInfo, bool isSearchScreen)
        {

            BizObjects.ClearanceClient client = new BCS.Core.Clearance.BizObjects.ClearanceClient();
            structures.Info.ClientInfo ccclient;
            string resXML = string.Empty;
            try
            {
                if (includeClientCoreInfo)
                {
                    #region Old get Client Info call
                    //BizObjects.Company ucompany = Common.BuildCompany(companyNumber);
                    //BTS.ClientCore.WS.ClientCoreWebServiceService ccws = Common.GetClientCoreWebServiceService(companyNumber);
                    //resXML = ccws.getClientInfo(Common.GetCommonBasicXml(ucompany), Common.BuildClientSearchByIdXml(Id, ucompany)); 
                    #endregion

                    resXML = Common.InvokegetClientInfo(companyNumber, id, isSearchScreen);

                    ccclient =
                        (structures.Info.ClientInfo)BCS.Core.XML.Deserializer.Deserialize(
                        resXML, typeof(structures.Info.ClientInfo), "seq_nbr");

                    client.ClientCoreClient = ccclient;
                }
            }
            catch (Exception ex)
            {
                logs.LogCentral.Current.LogWarn(string.Format("The Client associated with the submission either does not exist or is inaccessible. Id = {0}", id), ex);
                return new BizObjects.ClearanceClient();
            }

            BCS.Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.And(Biz.JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.Company company = dm.GetCompany(Biz.FetchPath.Company.ClientSystemType);

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Client.Columns.ClientCoreClientId, id);
            dm.QueryCriteria.And(Biz.JoinPath.Client.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            
            // TODO: optimize the fetch path
            List<OrmLib.DataManagerBase.FetchPathRelation> clientFetches = new List<OrmLib.DataManagerBase.FetchPathRelation>();
            clientFetches.Add(Biz.FetchPath.Client.All);
            if (company.SupportsClassCodes.IsTrue)
            {
                clientFetches.Add(Biz.FetchPath.Client.ClientClassCode.ClassCode);
                clientFetches.Add(Biz.FetchPath.Client.ClientClassCode.ClassCode.SicCodeList);
            }
            Biz.Client bizclient = dm.GetClient(
                clientFetches.ToArray());
            if (bizclient != null)
            {
                client.CompanyNumberId = bizclient.CompanyNumberId.IsNull ? 0 : bizclient.CompanyNumberId.Value;
                client.BusinessDescription = bizclient.BusinessDescription;
                client.ClientCoreClientId = bizclient.ClientCoreClientId.IsNull ? 0 : bizclient.ClientCoreClientId.Value;
                client.Id = bizclient.Id;
                client.SICCodeId = bizclient.SicCodeId.IsNull ? 0 : bizclient.SicCodeId.Value;
                if (bizclient.SicCodeList != null)
                {
                    client.SICCode = bizclient.SicCodeList.Code;
                    client.SICCodeDescription = bizclient.SicCodeList.CodeDescription;
                }

                #region populate company number attributes
                client.CompanyNumberAttributes = new BCS.Core.Clearance.BizObjects.ClientCompanyNumberAttributeValue[bizclient.ClientCompanyNumberAttributeValues.Count];
                for (int i = 0; i < bizclient.ClientCompanyNumberAttributeValues.Count; i++)
                {
                    BizObjects.ClientCompanyNumberAttributeValue attr = new BCS.Core.Clearance.BizObjects.ClientCompanyNumberAttributeValue();
                    attr.AttributeId = bizclient.ClientCompanyNumberAttributeValues[i].CompanyNumberAttributeId;
                    attr.AttributeValue = bizclient.ClientCompanyNumberAttributeValues[i].AttributeValue;

                    client.CompanyNumberAttributes[i] = attr;
                }
                #endregion

                #region populate company number codes
                client.CompanyNumberCodes = new BCS.Core.Clearance.BizObjects.ClientCompanyNumberCode[bizclient.ClientCompanyNumberCodes.Count];
                for (int i = 0; i < bizclient.ClientCompanyNumberCodes.Count; i++)
                {
                    BizObjects.ClientCompanyNumberCode code = new BCS.Core.Clearance.BizObjects.ClientCompanyNumberCode();
                    code.CompanyNumberCodeId = bizclient.ClientCompanyNumberCodes[i].CompanyNumberCodeId;

                    client.CompanyNumberCodes[i] = code;
                }
                #endregion

                #region populate client class codes
                if (company.SupportsClassCodes.IsTrue)
                {
                    client.ClientClassCodes = new BCS.Core.Clearance.BizObjects.ClientClassCode[bizclient.ClientClassCodes.Count];
                    for (int i = 0; i < bizclient.ClientClassCodes.Count; i++)
                    {
                        BizObjects.ClientClassCode code = new BCS.Core.Clearance.BizObjects.ClientClassCode();
                        code.ClassCodeId = bizclient.ClientClassCodes[i].ClassCodeId.IsNull ? 0 : bizclient.ClientClassCodes[i].ClassCodeId.Value;
                        code.ClientId = bizclient.ClientClassCodes[i].ClientId.IsNull ? 0 : bizclient.ClientClassCodes[i].ClientId.Value;
                        code.Id = bizclient.ClientClassCodes[i].Id;
                        code.Description = bizclient.ClientClassCodes[i].ClassCode.Description;

                        if (bizclient.ClientClassCodes[i].ClassCode != null)
                        {
                            code.ClassCode = new BCS.Core.Clearance.BizObjects.ClassCode();
                            code.ClassCode.CodeValue = bizclient.ClientClassCodes[i].ClassCode.CodeValue;
                            code.ClassCode.CompanyNumberId = bizclient.ClientClassCodes[i].ClassCode.CompanyNumberId.Value;
                            code.ClassCode.Description = bizclient.ClientClassCodes[i].ClassCode.Description;
                            code.ClassCode.Id = bizclient.ClientClassCodes[i].ClassCode.Id;
                            code.ClassCode.SicCodeId =
                                bizclient.ClientClassCodes[i].ClassCode.SicCodeId.IsNull ? 0 : bizclient.ClientClassCodes[i].ClassCode.SicCodeId.Value;
                        }

                        client.ClientClassCodes[i] = code;
                    }
                }
                #endregion

                #region populate name and address types
                if (includeClientCoreInfo)
                {
                    for (int i = 0; i < client.ClientCoreClient.Addresses.Length; i++)
                    {
                        if (client.ClientCoreClient.Addresses[i].AddressId != null)
                        {
                            Biz.ClientCoreAddressAddressType oType = bizclient.ClientCoreAddressAddressTypes.FindByClientCoreAddressId(client.ClientCoreClient.Addresses[i].AddressId);
                            if (null != oType)
                                client.ClientCoreClient.Addresses[i].ClearanceAddressType = oType.AddressType.PropertyAddressType;
                        }
                    }

                    for (   int i = 0; i < client.ClientCoreClient.Client.Names.Length; i++)
                    {
                        if (client.ClientCoreClient.Client.Names[i].SequenceNumber != null)
                        {
                            Biz.ClientCoreNameClientNameType oType = bizclient.ClientCoreNameClientNameTypes.FindByClientCoreNameId(client.ClientCoreClient.Client.Names[i].SequenceNumber);
                            if (null != oType)
                                client.ClientCoreClient.Client.Names[i].ClearanceNameType = oType.ClientNameType.NameType;
                        }
                    }
                }
                #endregion

                #region populate county
                bool isCobra = false;
                isCobra = company.ClientSystemType.Code == "Cobra";

                if (isCobra)
                {
                    for (int i = 0; i < client.ClientCoreClient.Addresses.Length; i++)
                    {
                        if (client.ClientCoreClient.Addresses[i].AddressId != null)
                        {
                            Biz.CobraAddressCounty oCounty = bizclient.CobraAddressCountys.FindByClientAddressId(client.ClientCoreClient.Addresses[i].AddressId);
                            if (null != oCounty)
                                client.ClientCoreClient.Addresses[i].County = oCounty.County;
                        }
                    }
                }
                #endregion
            }

            //resXML = XML.Serializer.SerializeAsXml(client);

            return client;
        }

        /// <summary>
        /// TODO: to be implemented
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public static string GetClientsByIds(string companyNumber, string[] ids, bool isSearchScreen)
        {
            #region Till ClientCORE provides this method
            Dictionary<string, BizObjects.ClearanceClient> dctClients = new Dictionary<string, BCS.Core.Clearance.BizObjects.ClearanceClient>();
            foreach (string id in ids)
            {
                BizObjects.ClearanceClient idClient = GetClientByIdAsObject(companyNumber, id, true, isSearchScreen);
                if (Common.IsNotNullOrEmpty(idClient.ClientCoreClient.Client.ClientId))
                {
                    if (!dctClients.ContainsKey(idClient.ClientCoreClient.Client.ClientId))
                    {
                        dctClients.Add(idClient.ClientCoreClient.Client.ClientId, idClient);
                    }
                }
            }
            // TODO: at this time just converting only fields needed by agency port the fields/properties, since anyhow this is just a 
            // simulation of the client core method until its provided.
            Dictionary<string, structures.Search.Client> dctSearchClients = new Dictionary<string, BTS.ClientCore.Wrapper.Structures.Search.Client>();
            foreach (BizObjects.ClearanceClient var in dctClients.Values)
            {
                structures.Search.Client varClient = new BTS.ClientCore.Wrapper.Structures.Search.Client();
                #region Fill Addresses
                List<structures.Search.AddressVersion> addrVs = new List<BTS.ClientCore.Wrapper.Structures.Search.AddressVersion>();
                foreach (structures.Info.Address infoAddr in var.ClientCoreClient.Addresses)
                {
                    structures.Search.AddressVersion searchAddr = new BTS.ClientCore.Wrapper.Structures.Search.AddressVersion();
                    searchAddr.Address1 = infoAddr.Address1;
                    searchAddr.Address2 = infoAddr.Address2;
                    searchAddr.Address3 = infoAddr.Address3;
                    searchAddr.Address4 = infoAddr.Address4;
                    searchAddr.AddressId = infoAddr.AddressId;
                    searchAddr.CountryId = infoAddr.CountryId;
                    searchAddr.HouseNumber = infoAddr.HouseNumber;
                    searchAddr.City = infoAddr.City;
                    searchAddr.County = infoAddr.County;
                    searchAddr.StateProvCode = infoAddr.StateProvinceId;
                    searchAddr.PostalCode = infoAddr.PostalCode;

                    addrVs.Add(searchAddr);
                }
                structures.Search.AddressVersion[] arrAddresses = new BTS.ClientCore.Wrapper.Structures.Search.AddressVersion[addrVs.Count];
                addrVs.CopyTo(arrAddresses);
                varClient.Addresses = arrAddresses;
                #endregion

                #region Fill Names
                List<structures.Search.ClientName> nameVs = new List<BTS.ClientCore.Wrapper.Structures.Search.ClientName>();
                foreach (structures.Info.Name infoName in var.ClientCoreClient.Client.Names)
                {
                    structures.Search.ClientName searchName = new BTS.ClientCore.Wrapper.Structures.Search.ClientName();
                    searchName.SequenceNumber = infoName.SequenceNumber;
                    searchName.PrimaryNameFlag = infoName.PrimaryNameFlag;
                    searchName.NameBusiness = infoName.NameBusiness;

                    nameVs.Add(searchName);
                }
                structures.Search.ClientName[] arrNames = new BTS.ClientCore.Wrapper.Structures.Search.ClientName[nameVs.Count];
                nameVs.CopyTo(arrNames);
                varClient.Names = arrNames;
                #endregion

                #region Client Info
                varClient.Info.ClientId = var.ClientCoreClient.Client.ClientId;
                #endregion

                dctSearchClients.Add(var.ClientCoreClient.Client.ClientId.ToString(), varClient);
            }
            #endregion
            structures.Search.Vector v = new BTS.ClientCore.Wrapper.Structures.Search.Vector();
            v.Clients = new BTS.ClientCore.Wrapper.Structures.Search.Client[dctSearchClients.Count];
            dctSearchClients.Values.CopyTo(v.Clients, 0);

            return XML.Serializer.SerializeAsXml(v);
        }
        /// <summary>
        /// 
        /// </summary>
        public static string GetClient(string companyNumber, string taxid, string houseNo, string address1, string city,
            string state, string zip, string firstname, string lastname, string businessname)
        {
            //BizObjects.Company ucompany = Common.BuildCompany(companyNumber);

            structures.Search.SearchClient client = new BTS.ClientCore.Wrapper.Structures.Search.SearchClient();
            //client.ClientAddresses[0].Addresses[0].AddressVersions[0].
            //structures.Search.ClientInfo info = new BTS.ClientCore.Wrapper.Structures.Search.ClientInfo();
            if (Common.IsNotNullOrEmpty(taxid))
                client.TaxId = taxid;
            //client.Info = info ;

            if (businessname.Length > 0)
            {
                //structures.Search.ClientName name = new BTS.ClientCore.Wrapper.Structures.Search.ClientName();
                client.BusinessName = "*" + businessname + "*";
                //client.Names = new BTS.ClientCore.Wrapper.Structures.Search.ClientName[1] { name };
            }
            else
            {
                //structures.Search.ClientName name = new BTS.ClientCore.Wrapper.Structures.Search.ClientName();
                client.FirstName = firstname;
                client.LastName = lastname;
                //client.Names = new BTS.ClientCore.Wrapper.Structures.Search.ClientName[1] { name };
            }

            //structures.Search.AddressVersion av = new BTS.ClientCore.Wrapper.Structures.Search.AddressVersion();
            if (Common.IsNotNullOrEmpty(houseNo))
                client.HouseNumber = houseNo;
            if (Common.IsNotNullOrEmpty(zip))
                client.ZipCode = zip;
            if (Common.IsNotNullOrEmpty(state))
                client.StateName = state;
            if (Common.IsNotNullOrEmpty(city))
                client.CityName = "*" + city + "*";
            if (Common.IsNotNullOrEmpty(address1))
                client.Address1 = address1;
            //client.Addresses = new BTS.ClientCore.Wrapper.Structures.Search.AddressVersion[1] { av };

            #region Old Perform Search CC Call
            //BTS.ClientCore.WS.ClientCoreWebServiceService ccws = Common.GetClientCoreWebServiceService(companyNumber);
            //string resXML = ccws.performClientSearch(Common.GetCommonBasicXml(ucompany), Common.BuildClientSearchXml(client)); 
            #endregion

            #region New Perform Search CC Call
            string resXML = Common.InvokeperformClientSearch(companyNumber, client);
            #endregion

            XmlDocument doc = new XmlDocument();

            doc.LoadXml(resXML);
            XmlNode node = Common.RemoveTypeAttributes(doc);

            resXML = node.InnerXml;

            // serialize the resXML and return as one of the client structures            
            try
            {
                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                structures.Search.Vector o = (structures.Search.Vector)XML.Deserializer.Deserialize(resXML, typeof(structures.Search.Vector));

                if (Common.IsNotNullOrEmpty(o.Error))
                    logs.LogCentral.Current.LogInfo(
                        string.Format("Search Failed, Reason : '{0}', criteria - tax Id : '{1}', number : '{2}', street : '{3}', city : '{4}', state : '{5}', zip : '{6}', first name : '{7}', last name : '{8}', business name : '{9}'",
                        o.Error,
                        Common.IsNotNullOrEmpty(taxid) ? taxid : "NULL",
                        Common.IsNotNullOrEmpty(houseNo) ? houseNo : "NULL",
                        Common.IsNotNullOrEmpty(address1) ? address1 : "NULL",
                        Common.IsNotNullOrEmpty(city) ? city : "NULL",
                        Common.IsNotNullOrEmpty(state) ? state : "NULL",
                        Common.IsNotNullOrEmpty(zip) ? zip : "NULL",
                        Common.IsNotNullOrEmpty(firstname) ? firstname : "NULL",
                        Common.IsNotNullOrEmpty(lastname) ? lastname : "NULL",
                        Common.IsNotNullOrEmpty(businessname) ? businessname : "NULL"));
                if (Common.IsNotNullOrEmpty(o.Message))
                    logs.LogCentral.Current.LogInfo(
                        string.Format("Search Failed, Reason : '{0}', criteria - tax Id : '{1}', number : '{2}', street : '{3}', city : '{4}', state : '{5}', zip : '{6}', first name : '{7}', last name : '{8}', business name : '{9}'",
                        o.Message,
                        Common.IsNotNullOrEmpty(taxid) ? taxid : "NULL",
                        Common.IsNotNullOrEmpty(houseNo) ? houseNo : "NULL",
                        Common.IsNotNullOrEmpty(address1) ? address1 : "NULL",
                        Common.IsNotNullOrEmpty(city) ? city : "NULL",
                        Common.IsNotNullOrEmpty(state) ? state : "NULL",
                        Common.IsNotNullOrEmpty(zip) ? zip : "NULL",
                        Common.IsNotNullOrEmpty(firstname) ? firstname : "NULL",
                        Common.IsNotNullOrEmpty(lastname) ? lastname : "NULL",
                        Common.IsNotNullOrEmpty(businessname) ? businessname : "NULL"));

                //return (structures.Vector)o;
                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetClientsByPortfolioId(string companyNumber, string portfolioId)
        {
            structures.Search.SearchPortfolio client = new BTS.ClientCore.Wrapper.Structures.Search.SearchPortfolio();
            client.Id = portfolioId;
            client.EffectiveDate = DateTime.Today.ToShortDateString();

            #region Perform Search CC Call
            string resXML = Common.InvokeperformPortfolioSearch(companyNumber, client);
            #endregion

            // serialize the resXML and return as one of the client structures
            XmlDocument doc = new XmlDocument();

            doc.LoadXml(resXML);
            XmlNode node = Common.RemoveTypeAttributes(doc);

            resXML = node.InnerXml;

            // serialize the resXML and return as one of the client structures            
            try
            {
                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                structures.Search.Vector o = (structures.Search.Vector)XML.Deserializer.Deserialize(resXML, typeof(structures.Search.Vector));
                if (Common.IsNotNullOrEmpty(o.Error))
                    logs.LogCentral.Current.LogInfo(string.Format("Search Failed, Reason : '{0}', criteria - portfolioId: '{1}'", o.Error, portfolioId));
                if (Common.IsNotNullOrEmpty(o.Message))
                    logs.LogCentral.Current.LogInfo(string.Format("Search Failed, Reason : '{0}', criteria - portfolioId: '{1}'", o.Message, portfolioId));

                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetClientsByPortfolioName(string companyNumber, string portfolioName)
        {
            structures.Search.SearchPortfolio client = new BTS.ClientCore.Wrapper.Structures.Search.SearchPortfolio();
            client.Name = portfolioName;
            client.EffectiveDate = DateTime.Today.ToShortDateString();

            #region Perform Search CC Call
            string resXML = Common.InvokeperformPortfolioSearch(companyNumber, client);
            #endregion

            XmlDocument doc = new XmlDocument();

            doc.LoadXml(resXML);            
            XmlNode node = Common.RemoveTypeAttributes(doc);            
            resXML = node.InnerXml;

            // serialize the resXML and return as one of the client structures            
            try
            {

                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                structures.Search.Vector o = (structures.Search.Vector)XML.Deserializer.Deserialize(resXML, typeof(structures.Search.Vector));
                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Add Client Methods
        // method lets add a client with a single address, single name, single communication info, single group owner.
        // i.e. client type is one of segind or unsegind
        // to add more than one of the info types use other overloads of AddClient
        /// <summary>
        /// 
        /// </summary>
        public static string AddClient(string companyNumber,
            DateTime effectiveDate,
            string corpId,
            string taxId,
            string taxIdType,
            string membershipGroup,
            string membershipFlag,
            string category,
            string status,
            string type,
            string maritalstatus,
            string driverLicenseNo,
            string driverLicenseCountry,
            string driverLicenseState,
            string gender,
            string creditRating,
            DateTime birthDate,
            string communicationType, string communicationValue,
            string nameFirst, string nameLast, string nameMiddle, string namePrefix, string namePrefixCode,
            string nameSuffix, string nameSuffixCode, string nameType, bool primaryName,
            string houseNo, string address1, string address2, string address3, string address4,
            string countryCode, string stateProvCode, string city, string county, int postalCode,
            string groupAccessKeyId, bool matches)
        {
            BizObjects.Company company = Common.BuildCompany(companyNumber);
            string m = !matches ? "<checkForMatchesOption>1</checkForMatchesOption>" : "<checkForMatchesOption>0</checkForMatchesOption>";

            structures.Add.TFGClientXMLDO client = new BTS.ClientCore.Wrapper.Structures.Add.TFGClientXMLDO();
            client.BirthDate = Common.ConvertToClientCoreDateTime(birthDate).ToString();
            client.Category = category;
            client.ClientType = type;
            client.CorpId = corpId;
            client.CreditRating = creditRating;
            client.DriverLicenseCountry = driverLicenseCountry;
            client.DriverLicenseNumber = driverLicenseNo;
            client.DriverLicenseState = driverLicenseState;
            client.EffectiveDate = Common.ConvertToClientCoreDateTime(effectiveDate).ToString();
            client.Gender = gender;
            client.MaritalStatus = maritalstatus;
            client.MembershipFlag = membershipFlag;
            client.MembershipGroup = membershipGroup;
            client.Status = status;
            client.TaxId = taxId;
            client.TaxIdType = taxIdType;
            //client.ICC_End_NBR = "23";
            //client.StartDate = effectiveDate.ToString(Globals.DateFormat);
            //client.InterStateId = "2";
            //client.IntraStateId = "3";
            //client.MC_Docket_Nbr = "";
            //client.NCCI_Nbr = "22";


            //structures.Communication communication = new BTS.ClientCore.Wrapper.Structures.Communication();
            //communication.TypeCode = communicationType;
            //communication.Value = communicationValue;

            //structures.GroupOwnerInfoDO group = new BTS.ClientCore.Wrapper.Structures.GroupOwnerInfoDO();
            //group.AccessKeyID = groupAccessKeyId;
            //group.GroupCode = string.Empty;
            //group.GroupDescription = string.Empty;
            //group.InactiveDate = string.Empty;

            structures.Add.ClientName name = new BTS.ClientCore.Wrapper.Structures.Add.ClientName();
            name.NameFirst = nameFirst;
            name.NameLast = nameLast;
            name.NameMiddle = nameMiddle;
            name.NamePrefix = namePrefix;
            name.NamePrefixCode = namePrefixCode;
            name.NameSuffix = nameSuffix;
            name.NameSuffixCode = nameSuffixCode;
            name.NameType = nameType;
            name.PrimaryNameFlag = primaryName ? "Y" : "N";

            structures.Add.AddressVersion av = new BTS.ClientCore.Wrapper.Structures.Add.AddressVersion();
            av.Address1 = address1;
            av.Address2 = address2;
            av.Address3 = address3;
            av.Address4 = address4;
            av.City = city;
            av.CorpId = corpId;
            av.CountryCode = countryCode;
            av.County = county;
            av.HouseNumber = houseNo;
            av.PostalCode = postalCode.ToString();
            av.StateProvCode = stateProvCode;
            av.EffectiveDate = Common.ConvertToClientCoreDateTime(effectiveDate);

            client.Addresses = new BTS.ClientCore.Wrapper.Structures.Add.AddressVersion[1] { av };
            client.Names = new BTS.ClientCore.Wrapper.Structures.Add.ClientName[1] { name };
            //client.Groups = new BTS.ClientCore.Wrapper.Structures.GroupOwnerInfoDO[1] { group };
            //client.Communications = new BTS.ClientCore.Wrapper.Structures.Communication[1] { communication };

            BTS.ClientCore.WS.ClientCoreWebServiceService cc = Common.GetClientCoreWebServiceService(company.ClientCoreEndPointUrl);
            string resXML = cc.clientQuickAdd(Common.GetCommonBasicXml(company), Common.BuildAddClientXml(client), m);

            try
            {
                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                //object o = Common.DeSerialize(resXML, typeof(structures.Vector));
                //return (structures.Vector)o;
                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="clientXml"></param>
        /// <param name="matches"></param>
        /// <returns></returns>        
        public static string AddClient(string companyNumber, string clientXml, bool matches)
        {
            BizObjects.Company company = Common.BuildCompany(companyNumber);
            string m = !matches ? "<checkForMatchesOption>1</checkForMatchesOption>" : "<checkForMatchesOption>0</checkForMatchesOption>";
            try
            {
                structures.Add.TFGClientXMLDO client = (structures.Add.TFGClientXMLDO)XML.Deserializer.Deserialize(
                    clientXml, typeof(structures.Add.TFGClientXMLDO));
                client.CorpId = company.ClientCoreCorpId;
                client.EffectiveDate = Common.ConvertToClientCoreDateTime(DateTime.Now);
                //client.AllowMultipleNames = "Y";

                if (client.Addresses != null)
                {
                    for (int i = 0; i < client.Addresses.Length; i++)
                    {
                        client.Addresses[i].EffectiveDate = client.EffectiveDate;
                    }
                }

                clientXml = BCS.Core.XML.Serializer.SerializeAsXml(client);

                BTS.ClientCore.WS.ClientCoreWebServiceService ccws = Common.GetClientCoreWebServiceService(company.ClientCoreEndPointUrl);
                string resXML = ccws.clientQuickAdd(Common.GetCommonBasicXml(Common.BuildCompany(companyNumber)), clientXml, m);

                return resXML;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// adds a business client.
        /// 
        /// </summary>       
        public static string AddBusinessClient(string companyNumber, DateTime effectiveDate, string corpId, string taxId, string taxIdType,
            string membershipGroup, string membershipFlag, string category, string status, string type,
            string maritalstatus, string driverLicenseNo, string driverLicenseCountry, string driverLicenseState,
            string gender, string creditRating, DateTime birthDate, string communicationType, string communicationValue,
            string nameBusiness, string nameType, bool primaryName, string houseNo, string address1, string address2,
            string address3, string address4, string countryCode, string stateProvCode, string city, string county,
            int postalCode, string groupAccessKeyId, bool matches)
        {
            BizObjects.Company company = Common.BuildCompany(companyNumber);
            string m = !matches ? "<checkForMatchesOption>1</checkForMatchesOption>" : "<checkForMatchesOption>0</checkForMatchesOption>";

            structures.Add.TFGClientXMLDO client = new BTS.ClientCore.Wrapper.Structures.Add.TFGClientXMLDO();
            client.BirthDate = Common.ConvertToClientCoreDateTime(birthDate).ToString();
            client.Category = category;
            client.ClientType = type;
            client.CorpId = corpId;
            client.CreditRating = creditRating;
            client.DriverLicenseCountry = driverLicenseCountry;
            client.DriverLicenseNumber = driverLicenseNo;
            client.DriverLicenseState = driverLicenseState;
            client.EffectiveDate = Common.ConvertToClientCoreDateTime(effectiveDate).ToString();
            client.Gender = gender;
            client.MaritalStatus = maritalstatus;
            client.MembershipFlag = membershipFlag;
            client.MembershipGroup = membershipGroup;
            client.Status = status;
            client.TaxId = taxId;
            client.TaxIdType = taxIdType;

            structures.Add.ClientName name = new BTS.ClientCore.Wrapper.Structures.Add.ClientName();
            name.NameBusiness = nameBusiness;
            name.NameType = nameType;
            name.PrimaryNameFlag = primaryName ? "Y" : "N";

            structures.Add.AddressVersion av = new BTS.ClientCore.Wrapper.Structures.Add.AddressVersion();
            av.Address1 = address1;
            av.Address2 = address2;
            av.Address3 = address3;
            av.Address4 = address4;
            av.City = city;
            av.CorpId = corpId;
            av.CountryCode = countryCode;
            av.County = county;
            av.HouseNumber = houseNo;
            av.PostalCode = postalCode.ToString();
            av.StateProvCode = stateProvCode;
            av.EffectiveDate = Common.ConvertToClientCoreDateTime(effectiveDate).ToString();


            client.Addresses = new BTS.ClientCore.Wrapper.Structures.Add.AddressVersion[1] { av };
            client.Names = new BTS.ClientCore.Wrapper.Structures.Add.ClientName[1] { name };

            BTS.ClientCore.WS.ClientCoreWebServiceService cc = Common.GetClientCoreWebServiceService(company.ClientCoreEndPointUrl);
            string resXML = cc.clientQuickAdd(Common.GetCommonBasicXml(company), Common.BuildAddClientXml(client), m);

            try
            {
                if (!resXML.Contains("vector"))
                    resXML = string.Concat("<vector>", resXML, "</vector>");
                //object o = Common.DeSerialize(resXML, typeof(structures.Vector));
                //return (structures.Vector)o;
                return resXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Modify Client Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="category"></param>
        /// <param name="clientType"></param>
        /// <param name="effectiveDate"></param>
        /// 
        /// <param name="status"></param>
        /// <param name="vendorFlag"></param>
        /// <param name="taxid"></param>
        /// <param name="taxidtype"></param>
        /// <param name="dupliateClientOption"></param>
        /// <param name="nameversions">seperated by '+-' string</param>
        /// <param name="addressversions">seperated by '+-' string</param>
        /// <param name="cocodeids"></param>
        /// <param name="attributes"></param>
        /// <param name="classCodes"></param>
        /// <param name="siccodeid"></param>
        /// <param name="businessdesc"></param>
        /// <param name="by"></param>
        /// <returns></returns>
        public static string AddClientWithImport(string companyNumber, string category, 
            string effectiveDate, string status, string vendorFlag, string taxid, 
            string taxidtype, int dupliateClientOption, string nameversions, string addressversions, string cocodeids, string attributes,
            string classCodes, int siccodeid, string businessdesc, string by, bool isSearchScreen)
        {
            BizObjects.Company company = Common.BuildCompany(companyNumber);

            string[] names = nameversions.Split(new string[] { "+-" }, StringSplitOptions.RemoveEmptyEntries);
            string[] addresses = addressversions.Split(new string[] { "+-" }, StringSplitOptions.RemoveEmptyEntries);

            structures.Import.AddressInfo[] ainfos = new BTS.ClientCore.Wrapper.Structures.Import.AddressInfo[addresses.Length];
            structures.Import.NameInfo[] ninfos = new BTS.ClientCore.Wrapper.Structures.Import.NameInfo[names.Length];

            try
            {
                for (int i = 0; i < addresses.Length; i++)
                {
                    ainfos[i] = (structures.Import.AddressInfo)XML.Deserializer.Deserialize(
                        addresses[i], typeof(structures.Import.AddressInfo));
                    ainfos[i].EffectiveDate = Common.ConvertToClientCoreDateTime(ainfos[i].EffectiveDate);
                }
                ainfos[0].Primary = "Y";

                for (int i = 0; i < names.Length; i++)
                {
                    ninfos[i] = (structures.Import.NameInfo)XML.Deserializer.Deserialize(
                        names[i], typeof(structures.Import.NameInfo));
                }
                ninfos[0].DisplayFlag = "Y";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //structures.AddressInfo[] ainfos = Common.ConvertAddressVersionsToImport(avs);
            //structures.NameInfo[] ninfos = Common.ConvertNamesToImport(cns);

            structures.Import.ClientImportData idata = new BTS.ClientCore.Wrapper.Structures.Import.ClientImportData();
            idata.Signature = new BTS.ClientCore.Wrapper.Structures.Import.Signature("CLIENT IMPORT", "1.0", "en_US");

            idata.Client = new BTS.ClientCore.Wrapper.Structures.Import.ImportClient();
            idata.Client.AllowMultipleNames = "Y";
            //idata.Client.CCClientId = clientid;
            idata.Client.ClientType = company.ClientCoreClientType;
            idata.Client.EffectiveDate = Common.ConvertToClientCoreDateTime(effectiveDate);
            idata.Client.ClientCategory = category;
            idata.Client.CorporationCode = company.ClientCoreCorpCode;
            idata.Client.DuplicateClientOption = dupliateClientOption;
            idata.Client.Owner = company.ClientCoreOwner;
            idata.Client.Status = status;
            idata.Client.VendorFlag = vendorFlag;
            idata.Client.AddressInfos = ainfos;
            idata.Client.NameInfos = ninfos;

            idata.Client.PersonalInfo = new BTS.ClientCore.Wrapper.Structures.Import.PersonalInfo();
            idata.Client.PersonalInfo.TaxId = taxid;

            idata.Client.PersonalInfo.TaxIdTypeCode = taxidtype;

            string importXML = XML.Serializer.SerializeAsXml(idata);

            #region Old import Client call
            //BTS.ClientCore.WS.ClientCoreWebServiceService ccws = Common.GetClientCoreWebServiceService(companyNumber);
            //string resXML = ccws.importClient(Common.GetCommonBasicXml(company), importXML);
            #endregion
            #region New import Client call
            string resXML = Common.InvokeimportClient(companyNumber, importXML);
            #endregion

            #region clearance client info add
            // add client related info in clearance. TODO: analyze if its better to update before updating info in client core or
            // client core call can be avoided.

            structures.Import.Vector addedclient = (structures.Import.Vector)BCS.Core.XML.Deserializer.Deserialize(resXML, typeof(structures.Import.Vector));

            Biz.DataManager dm = Common.GetDataManager();
            Biz.Client bizclient = dm.NewClient(Convert.ToInt64(addedclient.ClientId), Common.VerifyCompanyNumber(dm, companyNumber));
            bizclient.SicCodeId = siccodeid == 0 ? System.Data.SqlTypes.SqlInt32.Null : siccodeid;
            bizclient.BusinessDescription = businessdesc;
            bizclient.EntryBy = by;

            string[] tattributes = attributes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string ta in tattributes)
            {
                string[] ts = ta.Split("=".ToCharArray());
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.Columns.Id, ts[0]);
                Biz.CompanyNumberAttribute cnattr = dm.GetCompanyNumberAttribute();

                dm.NewClientCompanyNumberAttributeValue(bizclient, cnattr).AttributeValue = ts[1];
            }


            string[] scompanycodeids = cocodeids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            int[] companycodeids = new int[scompanycodeids.Length];
            for (int i = 0; i < scompanycodeids.Length; i++)
            {
                companycodeids[i] = Convert.ToInt32(scompanycodeids[i]);
            }
            foreach (int iid in companycodeids)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Id, iid);
                Biz.CompanyNumberCode ncnc = dm.GetCompanyNumberCode();
                dm.NewClientCompanyNumberCode(bizclient, ncnc);
            }

            dm.CommitAll();

            #region Class Codes add
            string[] clientClassCodes = classCodes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string clientClassCode in clientClassCodes)
            {
                string[] ss = clientClassCode.Split("=".ToCharArray());
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, ss[1]);
                dm.NewClientClassCode(dm.GetClassCode(), bizclient);
            }
            #endregion

            dm.CommitAll();
            #endregion

            #region update name and address TYPES on clearance side

            //string aclientstr = GetClientById(companyNumber, addedclient.ClientId, true);
            //BizObjects.ClearanceClient adata = (BizObjects.ClearanceClient)XML.Deserializer.Deserialize(aclientstr, typeof(BizObjects.ClearanceClient), "seq_nbr");
            BizObjects.ClearanceClient adata = GetClientByIdAsObject(companyNumber, addedclient.ClientId, true, isSearchScreen);

            bool isCobraForCounty = false;
            bool isCobraOrRelatedForTypes = false; // for address & name types
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.Company currentCompany = dm.GetCompany(Biz.FetchPath.Company.ClientSystemType);
            isCobraForCounty = currentCompany.ClientSystemType.Code.Equals("Cobra", StringComparison.CurrentCultureIgnoreCase);
            isCobraOrRelatedForTypes = isCobraForCounty || currentCompany.ClientSystemType.Code.Equals("BCSasClientCore", StringComparison.CurrentCultureIgnoreCase);

            // Important: Assuming the client core returns names and address in the same order that we send them. This is needed to overcome the fact
            // a client can have more than one exact same name and address.
            Biz.Lookups lookup = Common.GetLookupManager();
            //foreach (structures.Info.Address newAddr in adata.ClientCoreClient.Addresses)
            for (int i = 0; i < adata.ClientCoreClient.Addresses.Length; i++)
            {
                structures.Info.Address newAddr = adata.ClientCoreClient.Addresses[i];
                //foreach (structures.Import.AddressInfo ipAddrInfo in idata.Client.AddressInfos)
                {
                    structures.Import.AddressInfo ipAddrInfo = idata.Client.AddressInfos[i];
                    // cobra pads the zip to 9 digits, so addresses will not be equal
                    if (isCobraOrRelatedForTypes)
                    {
                        while (ipAddrInfo.Zip.Length < 9)
                        {
                            ipAddrInfo.Zip += "0";
                        }
                    }
                    if (AddressIsEqual(newAddr, ipAddrInfo))
                    {
                        if (ipAddrInfo.ClearanceAddressType != null)
                            dm.NewClientCoreAddressAddressType(Convert.ToInt32(newAddr.AddressId), lookup.AddressTypes.FindByPropertyAddressType(ipAddrInfo.ClearanceAddressType), bizclient);

                        if (isCobraForCounty)
                        {
                            if (Common.IsNotNullOrEmpty(ipAddrInfo.County))
                            {
                                Biz.CobraAddressCounty cobraAddressCounty = dm.NewCobraAddressCounty(Convert.ToInt32(newAddr.AddressId), bizclient);
                                cobraAddressCounty.County = ipAddrInfo.County;
                            }
                        }
                        //break;
                    }
                }
            }
            //foreach (structures.Info.Name newName in adata.ClientCoreClient.Client.Names)
            for (int i = 0; i < adata.ClientCoreClient.Client.Names.Length; i++)
            {
                structures.Info.Name newName = adata.ClientCoreClient.Client.Names[i];
                //foreach (structures.Import.NameInfo ipNameInfo in idata.Client.NameInfos)
                {
                    structures.Import.NameInfo ipNameInfo = idata.Client.NameInfos[i];
                    if (NameIsEqual(newName, ipNameInfo))
                    {
                        if (ipNameInfo.ClearanceNameType != null)
                        {
                            Biz.ClientNameType temp = lookup.ClientNameTypes.FindByNameType(ipNameInfo.ClearanceNameType);
                            dm.NewClientCoreNameClientNameType(Convert.ToInt32(newName.SequenceNumber), bizclient, temp);
                        }
                        //break;
                    }
                }
            }
            dm.CommitAll();

            #endregion

            return resXML;
        }

        private static bool NameIsEqual(BTS.ClientCore.Wrapper.Structures.Info.Name newName, BTS.ClientCore.Wrapper.Structures.Import.NameInfo ipNameInfo)
        {
            bool b = false;

            if (newName.NameBusiness != null)
            {
                if (newName.NameBusiness.Trim() == ipNameInfo.BusinessName.Trim())
                    b = true;
            }
            else
            {
                if (newName.NameFirst == ipNameInfo.FirstName && newName.NameLast == ipNameInfo.LastName && newName.NameMiddle == ipNameInfo.MiddleName &&
                     newName.NamePrefix == ipNameInfo.NamePrefix && newName.NameSuffix == ipNameInfo.NameSuffix)
                {
                    //return true; 
                    b = true;
                }
            }
            return b;
        }

        private static bool AddressIsEqual(BTS.ClientCore.Wrapper.Structures.Info.Address newAddr, BTS.ClientCore.Wrapper.Structures.Import.AddressInfo ipAddrInfo)
        {
            bool b = false;


            if (newAddr.Address1 == null)
                newAddr.Address1 = string.Empty;
            if (newAddr.Address2 == null)
                newAddr.Address2 = string.Empty;
            if (newAddr.City == null)
                newAddr.City = string.Empty;
            if (newAddr.HouseNumber == null)
                newAddr.HouseNumber = string.Empty;
            if (newAddr.StateProvinceId == null)
                newAddr.StateProvinceId = string.Empty;
            if (newAddr.PostalCode == null)
                newAddr.PostalCode = string.Empty;


            if (ipAddrInfo.ADDR1 == null)
                ipAddrInfo.ADDR1 = string.Empty;
            if (ipAddrInfo.ADDR2 == null)
                ipAddrInfo.ADDR2 = string.Empty;
            if (ipAddrInfo.CityName == null)
                ipAddrInfo.CityName = string.Empty;
            if (ipAddrInfo.HouseNumber == null)
                ipAddrInfo.HouseNumber = string.Empty;
            if (ipAddrInfo.StateCode == null)
                ipAddrInfo.StateCode = string.Empty;
            if (ipAddrInfo.Zip == null)
                ipAddrInfo.Zip = string.Empty;



            if (newAddr.Address1.Trim() == ipAddrInfo.ADDR1.Trim() &&
                newAddr.Address2.Trim() == ipAddrInfo.ADDR2.Trim() &&
                newAddr.City.Trim() == ipAddrInfo.CityName.Trim() &&
                newAddr.HouseNumber.Trim() == ipAddrInfo.HouseNumber.Trim() &&
                newAddr.StateProvinceId.Trim() == ipAddrInfo.StateCode.Trim() &&
                newAddr.PostalCode.Trim() == ipAddrInfo.Zip.Trim())
            {
                //return true; 
                b = true;
            }
            return b;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientid"></param>
        /// <param name="companyNumber"></param>
        /// <param name="portfolioid"></param>
        /// <param name="category"></param>
        /// <param name="clientType"></param>
        /// <param name="effectiveDate"></param>
        /// <param name="owner"></param>
        /// <param name="status"></param>
        /// <param name="vendorFlag"></param>
        /// <param name="taxid"></param>
        /// <param name="taxidtype"></param>
        /// <param name="dupliateClientOption"></param>
        /// <param name="nameversions">seperated by '+-' string</param>
        /// <param name="addressversions">seperated by '+-' string</param>
        /// <param name="cocodeids"></param>
        /// <param name="attributes"></param>
        /// <param name="classCodes"></param>
        /// <param name="siccodeid"></param>
        /// <param name="businessdesc"></param>
        /// <param name="by"></param>
        /// <returns></returns>
        public static string ModifyClient(string clientid, string companyNumber, string portfolioid, string category, string clientType,
            string effectiveDate, string owner, string status, string vendorFlag, string taxid,
            string taxidtype, int dupliateClientOption, string nameversions, string addressversions, string cocodeids, string attributes,
            string classCodes, int siccodeid, string businessdesc, string by,bool isSearchScreen)
        {
            bool IsBusiness = false;
            BizObjects.Company company = Common.BuildCompany(companyNumber);

            string[] names = nameversions.Split(new string[] { "+-" }, StringSplitOptions.RemoveEmptyEntries);
            string[] addresses = addressversions.Split(new string[] { "+-" }, StringSplitOptions.RemoveEmptyEntries);

            structures.Import.AddressInfo[] ainfos = new BTS.ClientCore.Wrapper.Structures.Import.AddressInfo[addresses.Length];
            structures.Import.NameInfo[] ninfos = new BTS.ClientCore.Wrapper.Structures.Import.NameInfo[names.Length];

            try
            {
                for (int i = 0; i < addresses.Length; i++)
                {
                    ainfos[i] = (structures.Import.AddressInfo)XML.Deserializer.Deserialize(
                        addresses[i], typeof(structures.Import.AddressInfo));
                    ainfos[i].EffectiveDate = Common.ConvertToClientCoreDateTime(ainfos[i].EffectiveDate);
                }

                for (int i = 0; i < names.Length; i++)
                {
                    ninfos[i] = (structures.Import.NameInfo)XML.Deserializer.Deserialize(
                        names[i], typeof(structures.Import.NameInfo));
                    if (ninfos[i].BusinessName != null && ninfos[i].BusinessName.Length > 0)
                    {
                        IsBusiness = true;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //structures.AddressInfo[] ainfos = Common.ConvertAddressVersionsToImport(avs);
            //structures.NameInfo[] ninfos = Common.ConvertNamesToImport(cns);

            structures.Import.ClientImportData idata = new BTS.ClientCore.Wrapper.Structures.Import.ClientImportData();
            idata.Signature = new BTS.ClientCore.Wrapper.Structures.Import.Signature("CLIENT IMPORT", "1.0", "en_US");

            idata.Client = new BTS.ClientCore.Wrapper.Structures.Import.ImportClient();
            idata.Client.AllowMultipleNames = "Y";
            idata.Client.CCClientId = clientid;
            idata.Client.ClientType = clientType;
            idata.Client.EffectiveDate = Common.ConvertToClientCoreDateTime(effectiveDate);
            idata.Client.ClientCategory = category;
            idata.Client.CorporationCode = company.ClientCoreCorpCode;
            idata.Client.DuplicateClientOption = dupliateClientOption;
            idata.Client.Owner = owner;
            idata.Client.Status = status;
            idata.Client.VendorFlag = vendorFlag;
            idata.Client.AddressInfos = ainfos;
            idata.Client.NameInfos = ninfos;
            idata.Client.PortfolioId = portfolioid;

            idata.Client.PersonalInfo = new BTS.ClientCore.Wrapper.Structures.Import.PersonalInfo();


            if (taxid != null && taxid.Length > 0)
            {

                idata.Client.PersonalInfo.TaxId = taxid;
                idata.Client.PersonalInfo.TaxIdTypeCode = taxidtype;
                //if (taxidtype == null || taxidtype.Length == 0)
                //{
                //    taxidtype = "SSN";
                //    if (IsBusiness)
                //        taxidtype = "FEIN";
                //    idata.Client.PersonalInfo.TaxIdTypeCode = taxidtype;
                //}
            }
            else
            {
                idata.Client.PersonalInfo.TaxIdTypeCode = null;
            }

            string importXML = XML.Serializer.SerializeAsXml(idata);



            // add client related info in clearance. TODO: anaylize if its better to update before updating info in client core or
            // client core call can be avoided.
            structures.Import.Vector addedclient;

            // TODO: can be enhanced here, if there is nothing to update on the client core side
            if (idata.Client.AddressInfos.Length == 0 && idata.Client.NameInfos.Length == 0 && idata.Client.PersonalInfo.TaxId == null)
            {
                addedclient.ClientId = clientid;
            }
            else
            {
                #region Old import Client call
                //BTS.ClientCore.WS.ClientCoreWebServiceService ccws = Common.GetClientCoreWebServiceService(companyNumber);
                //string resXML = ccws.importClient(Common.GetCommonBasicXml(company), importXML); 
                #endregion
                #region New import Client call
                string resXML = Common.InvokeimportClient(companyNumber, importXML);
                #endregion

                try
                {
                    addedclient = (structures.Import.Vector)BCS.Core.XML.Deserializer.Deserialize(resXML, typeof(structures.Import.Vector));

                }
                catch (Exception ex)
                {
                    logs.LogCentral.Current.LogInfo(string.Format(" modify client failed at client core : client Id - {0}.", clientid));
                    throw ex;
                }
            }
            #region clearance client info update

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Client.Columns.ClientCoreClientId, addedclient.ClientId);
            dm.QueryCriteria.And(Biz.JoinPath.Client.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.Client bizclient = dm.GetClient(Biz.FetchPath.Client.AllChildren);
            if (bizclient == null)
            {
                bizclient = dm.NewClient(Convert.ToInt64(addedclient.ClientId), Common.VerifyCompanyNumber(dm, companyNumber));
                bizclient.SicCodeId = siccodeid == 0 ? System.Data.SqlTypes.SqlInt32.Null : siccodeid;
                bizclient.BusinessDescription = businessdesc;
                bizclient.EntryBy = by;
            }
            else
            {
                bizclient.SicCodeId = siccodeid == 0 ? System.Data.SqlTypes.SqlInt32.Null : siccodeid;
                bizclient.BusinessDescription = businessdesc;
                bizclient.ModifiedBy = by;
                bizclient.ModifiedDt = DateTime.Now;
            }

            string[] sa = attributes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string ts in sa)
            {
                string[] ta = ts.Split("=".ToCharArray());
                Biz.ClientCompanyNumberAttributeValue var = bizclient.ClientCompanyNumberAttributeValues.FindByCompanyNumberAttributeId(ta[0]);
                if (var != null)
                {
                    if (var.AttributeValue == ta[1])
                        continue;
                    else
                        var.AttributeValue = ta[1];
                }
                else
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.Columns.Id, ta[0]);
                    Biz.CompanyNumberAttribute cna = dm.GetCompanyNumberAttribute();
                    dm.NewClientCompanyNumberAttributeValue(bizclient, cna).AttributeValue = ta[1];
                }
            }

            int le = bizclient.ClientCompanyNumberCodes.Count;

            for (int i = 0; i < le; i++)
            {
                bizclient.ClientCompanyNumberCodes[(le - i) - 1].Delete();
            }

            dm.CommitAll();

            string[] scompanycodeids = cocodeids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            int[] companycodeids = new int[scompanycodeids.Length];
            for (int i = 0; i < scompanycodeids.Length; i++)
            {
                companycodeids[i] = Convert.ToInt32(scompanycodeids[i]);
            }
            foreach (int iid in companycodeids)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Id, iid);
                Biz.CompanyNumberCode ncnc = dm.GetCompanyNumberCode();
                dm.NewClientCompanyNumberCode(bizclient, ncnc);
            }

            #region Class Codes update

            string[] clientClassCodes = classCodes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string clientClassCode in clientClassCodes)
            {
                string[] ss = clientClassCode.Split("=".ToCharArray());
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.Columns.Id, ss[0]);
                Biz.ClientClassCode aclasscode = dm.GetClientClassCode();
                if (aclasscode == null)
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, ss[1]);
                    aclasscode = dm.NewClientClassCode(dm.GetClassCode(), bizclient);
                }
                else
                {
                    aclasscode.ClassCodeId = Common.ConvertToSqlInt32(ss[1]);
                }
            }
            #endregion

            #region Old Address Types and Name Types handling
            List<structures.Import.AddressInfo> addrList4new = new List<structures.Import.AddressInfo>();
            List<structures.Import.NameInfo> nameList4new = new List<structures.Import.NameInfo>();
            List<structures.Import.AddressInfo> addressList4County = new List<BTS.ClientCore.Wrapper.Structures.Import.AddressInfo>();


            bool isCobraForCounty = false;
            bool isCobraOrRelatedForTypes = false; // for address & name types
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.Company currentCompany = dm.GetCompany(Biz.FetchPath.Company.ClientSystemType);
            isCobraForCounty = currentCompany.ClientSystemType.Code.Equals("Cobra", StringComparison.CurrentCultureIgnoreCase);
            isCobraOrRelatedForTypes = isCobraForCounty || currentCompany.ClientSystemType.Code.Equals("BCSasClientCore", StringComparison.CurrentCultureIgnoreCase);


            Biz.Lookups lookup = Common.GetLookupManager();

            // cobra pads the zip to 9 digits, so addresses will not be equal
            if (isCobraOrRelatedForTypes)
            {
                for (int i = 0; i < idata.Client.AddressInfos.Length; i++)
                {

                    while (idata.Client.AddressInfos[i].Zip.Length < 9)
                    {
                        idata.Client.AddressInfos[i].Zip += "0";
                    }
                }
            }
            foreach (structures.Import.AddressInfo var in idata.Client.AddressInfos)
            {
                if (isCobraForCounty)
                {
                    if (!string.IsNullOrEmpty(var.CCAddressId))
                    {
                        Biz.CobraAddressCounty oCounty = bizclient.CobraAddressCountys.FindByClientAddressId(var.CCAddressId);
                        if (null == oCounty)
                        {
                            Biz.CobraAddressCounty cobraAddressCounty = bizclient.NewCobraAddressCounty();
                            cobraAddressCounty.ClientAddressId = Convert.ToInt32(var.CCAddressId);
                            cobraAddressCounty.County = var.County;
                        }
                        else
                        {
                            oCounty.County = var.County;
                        }
                    }
                    else
                    {
                        addressList4County.Add(var);
                    }
                }

                // no address type, we need not do anything on clearance side
                if (null == var.ClearanceAddressType)
                    continue;

                // no address Id, new address for an existing client. we will look at them after the ones with ids
                if (var.CCAddressId == null || var.CCAddressId.Length == 0)
                {
                    addrList4new.Add(var);
                    continue;
                }

                Biz.AddressType addrType = lookup.AddressTypes.FindByPropertyAddressType(var.ClearanceAddressType);
                Biz.ClientCoreAddressAddressType oType = bizclient.ClientCoreAddressAddressTypes.FindByClientCoreAddressId(var.CCAddressId);
                if (null == oType)
                {
                    Biz.ClientCoreAddressAddressType newClientCoreAddressAddressType = bizclient.NewClientCoreAddressAddressType();
                    newClientCoreAddressAddressType.ClientCoreAddressId = Convert.ToInt32(var.CCAddressId);
                    newClientCoreAddressAddressType.AddressType = addrType;
                }
                else
                {
                    oType.AddressType = addrType;
                }
            }

            foreach (structures.Import.NameInfo var in idata.Client.NameInfos)
            {
                if (null == var.ClearanceNameType)
                    continue;
                if (var.NameSeqNum == null || var.NameSeqNum.Length == 0)
                {
                    nameList4new.Add(var);
                    continue;
                }
                Biz.ClientNameType nameType = lookup.ClientNameTypes.FindByNameType(var.ClearanceNameType);
                Biz.ClientCoreNameClientNameType oType = bizclient.ClientCoreNameClientNameTypes.FindByClientCoreNameId(var.NameSeqNum);
                if (null == oType)
                {
                    Biz.ClientCoreNameClientNameType newClientCoreNameClientNameType = bizclient.NewClientCoreNameClientNameType();
                    newClientCoreNameClientNameType.ClientCoreNameId = Convert.ToInt32(var.NameSeqNum);
                    newClientCoreNameClientNameType.ClientNameType = nameType;
                }
                else
                {
                    oType.ClientNameType = nameType;
                }
            }

            if (addrList4new.Count > 0 || nameList4new.Count > 0 || addressList4County.Count > 0)
            {
                string aclientstr = GetClientById(companyNumber, addedclient.ClientId, true, isSearchScreen);
                BizObjects.ClearanceClient adata = (BizObjects.ClearanceClient)XML.Deserializer.Deserialize(aclientstr, typeof(BizObjects.ClearanceClient), "seq_nbr");

                foreach (structures.Import.AddressInfo var in addrList4new)
                {
                    foreach (structures.Info.Address avar in adata.ClientCoreClient.Addresses)
                    {
                        if (AddressIsEqual(avar, var))
                        {
                            if (var.ClearanceAddressType != null)
                            {
                                Biz.AddressType temp = lookup.AddressTypes.FindByPropertyAddressType(var.ClearanceAddressType);
                                dm.NewClientCoreAddressAddressType(Convert.ToInt32(avar.AddressId), temp, bizclient);

                            }
                            break;
                        }
                    }
                }

                foreach (structures.Import.NameInfo var in nameList4new)
                {
                    foreach (structures.Info.Name avar in adata.ClientCoreClient.Client.Names)
                    {
                        if (NameIsEqual(avar, var))
                        {
                            if (var.ClearanceNameType != null)
                            {
                                Biz.ClientNameType temp = lookup.ClientNameTypes.FindByNameType(var.ClearanceNameType);
                                dm.NewClientCoreNameClientNameType(Convert.ToInt32(avar.SequenceNumber), bizclient, temp);
                            }
                            break;
                        }
                    }
                }

                foreach (structures.Import.AddressInfo var in addressList4County)
                {
                    foreach (structures.Info.Address avar in adata.ClientCoreClient.Addresses)
                    {
                        if (AddressIsEqual(avar, var))
                        {
                            dm.NewCobraAddressCounty(Convert.ToInt32(avar.AddressId), bizclient).County = var.County;
                        }
                    }
                }

            }
            #endregion


            #region New Address Types and Name Types handling
            //string aclientstr = GetClientById(companyNumber, addedclient.ClientId, true);
            //BizObjects.ClearanceClient adata = (BizObjects.ClearanceClient)XML.Deserializer.Deserialize(aclientstr, typeof(BizObjects.ClearanceClient), "seq_nbr");

            //Biz.Lookups lookup = Common.GetLookupManager();
            //// Important: Assuming the client core returns names and address in the same order that we send them. This is needed to overcome the fact
            //// a client can have more than one exact same name and address.
            //#region Addresses
            ////foreach (structures.Info.Address newAddr in adata.ClientCoreClient.Addresses)
            //for (int i = 0; i < adata.ClientCoreClient.Addresses.Length; i++)
            //{
            //    structures.Info.Address newAddr = adata.ClientCoreClient.Addresses[i];
            //    structures.Import.AddressInfo ipAddrInfo = idata.Client.AddressInfos[i];

            //    // TODO: analyze, based on the assumption, should not be true, but never bad to check again
            //    if (!AddressIsEqual(newAddr, ipAddrInfo))
            //    {
            //        // address type is not updated.
            //        continue;
            //    }
            //    // regardless of its a new address or existing address of a client, do not process further since there is not address type associated.
            //    if (string.IsNullOrEmpty(ipAddrInfo.ClearanceAddressType))
            //    {
            //        continue;
            //    }

            //    Biz.AddressType addrType = lookup.AddressTypes.FindByPropertyAddressType(ipAddrInfo.ClearanceAddressType);

            //    //foreach (structures.Import.AddressInfo ipAddrInfo in idata.Client.AddressInfos)
            //    {
            //        // there is an address Id, meaning was existing address of a client
            //        if (Common.IsNotNullOrEmpty(ipAddrInfo.CCAddressId))
            //        {
            //            Biz.ClientCoreAddressAddressType oType = bizclient.ClientCoreAddressAddressTypes.FindByClientCoreAddressId(newAddr.AddressId);
            //            // if it did not already had an address type associated, create one
            //            if (null == oType)
            //            {
            //                Biz.ClientCoreAddressAddressType newClientCoreAddressAddressType = bizclient.NewClientCoreAddressAddressType();
            //                newClientCoreAddressAddressType.ClientCoreAddressId = Convert.ToInt32(newAddr.AddressId);
            //                newClientCoreAddressAddressType.AddressType = addrType;
            //            }
            //            else // it already had had an address type associated, update old value
            //            {
            //                oType.AddressType = addrType;
            //            }
            //        }
            //        else // there is no address Id, meaning is a new address of an existing client
            //        {
            //            Biz.ClientCoreAddressAddressType newClientCoreAddressAddressType = bizclient.NewClientCoreAddressAddressType();
            //            newClientCoreAddressAddressType.ClientCoreAddressId = Convert.ToInt32(newAddr.AddressId);
            //            newClientCoreAddressAddressType.AddressType = addrType;
            //        }
            //    }
            //} 
            //#endregion


            //#region Names
            ////foreach (structures.Info.Name newName in adata.ClientCoreClient.Client.Names)
            //for (int i = 0; i < adata.ClientCoreClient.Client.Names.Length; i++)
            //{
            //    structures.Info.Name newName = adata.ClientCoreClient.Client.Names[i];
            //    structures.Import.NameInfo ipNameInfo = idata.Client.NameInfos[i];

            //    // based on the assumption, should not be true, but never bad to check again
            //    if (!NameIsEqual(newName, ipNameInfo))
            //    {
            //        // name type is not updated.
            //        continue;
            //    }
            //    // regardless of its a new name or existing name of a client, do not process further since there is not name type associated.
            //    if (string.IsNullOrEmpty(ipNameInfo.ClearanceNameType))
            //    {
            //        continue;
            //    }

            //    Biz.ClientNameType nameType = lookup.ClientNameTypes.FindByNameType(ipNameInfo.ClearanceNameType);

            //    //foreach (structures.Import.NameInfo ipNameInfo in idata.Client.NameInfos)
            //    {
            //        // there is an name Id, meaning was existing name of a client
            //        if (Common.IsNotNullOrEmpty(ipNameInfo.NameSeqNum))
            //        {
            //            Biz.ClientCoreNameClientNameType oType = bizclient.ClientCoreNameClientNameTypes.FindByClientCoreNameId(newName.SequenceNumber);
            //            // if it did not already had an name type associated, create one
            //            if (null == oType)
            //            {
            //                Biz.ClientCoreNameClientNameType newClientCoreNameClientNameType = bizclient.NewClientCoreNameClientNameType();
            //                newClientCoreNameClientNameType.ClientCoreNameId = Convert.ToInt32(newName.SequenceNumber);
            //                newClientCoreNameClientNameType.ClientNameType = nameType;
            //            }
            //            else // it already had had an name type associated, update old value
            //            {
            //                oType.ClientNameType = nameType;
            //            }
            //        }
            //        else // there is no name Id, meaning is a new name of an existing client
            //        {
            //            Biz.ClientCoreNameClientNameType newClientCoreNameClientNameType = bizclient.NewClientCoreNameClientNameType();
            //            newClientCoreNameClientNameType.ClientCoreNameId = Convert.ToInt32(newName.SequenceNumber);
            //            newClientCoreNameClientNameType.ClientNameType = nameType;
            //        }
            //    }
            //} 
            //#endregion

            #endregion

            dm.CommitAll();

            #endregion

            return XML.Serializer.SerializeAsXml(addedclient);
        }

        #endregion

        #region Other Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public static string GetMaxHazardGrade(string companyNumber, string clientId)
        {
            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.Client.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.Client.Columns.ClientCoreClientId, clientId);
            Biz.ClientClassCodeCollection clientClassCodes = dm.GetClientClassCodeCollection(Biz.FetchPath.ClientClassCode.ClassCode);
            System.Collections.Generic.List<int> classcodeids = new System.Collections.Generic.List<int>();
            foreach (Biz.ClientClassCode clientclasscode in clientClassCodes)
                classcodeids.Add(clientclasscode.ClassCodeId.Value);
            int[] arrids = new int[classcodeids.Count];
            classcodeids.CopyTo(arrids);

            dm = Common.GetDataManager();
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberClassCodeHazardGradeValue.Columns.ClassCodeId, arrids, OrmLib.MatchType.In);
            Biz.CompanyNumberClassCodeHazardGradeValueCollection basehgValues = dm.GetCompanyNumberClassCodeHazardGradeValueCollection();

            // somehow the above query criteria does not work. so we loop, filter and add to another collection
            Biz.CompanyNumberClassCodeHazardGradeValueCollection hgValues = new BCS.Biz.CompanyNumberClassCodeHazardGradeValueCollection();
            foreach (Biz.CompanyNumberClassCodeHazardGradeValue var in basehgValues)
            {
                foreach (int classcodeId in arrids)
                    if (var.ClassCodeId == classcodeId)
                        hgValues.Add(var);
            }


            if (hgValues.Count == 0)
                return "0";

            DataTable dt = new DataTable();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberHazardGradeType.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Biz.CompanyNumberHazardGradeTypeCollection cnHgts = dm.GetCompanyNumberHazardGradeTypeCollection();
            foreach (Biz.CompanyNumberHazardGradeType var in cnHgts)
            {
                dt.Columns.Add(new DataColumn(var.HazardGradeTypeId.ToString(), typeof(int)));
            }

            int handledClassCodeId = 0;

            foreach (Biz.CompanyNumberClassCodeHazardGradeValue var in hgValues)
            {
                if (var.ClassCodeId == handledClassCodeId)
                    continue;
                handledClassCodeId = var.ClassCodeId;
                Biz.CompanyNumberClassCodeHazardGradeValueCollection temp = hgValues.FilterByClassCodeId(var.ClassCodeId);
                temp = temp.SortByHazardGradeTypeId(OrmLib.SortDirection.Ascending);
                List<string> al = new List<string>();

                for (int i = 0; i < cnHgts.Count; i++)
                {
                    Biz.CompanyNumberClassCodeHazardGradeValueCollection temp1 = temp.FilterByClassCodeId(var.ClassCodeId);
                    // there should be only one.
                    Biz.CompanyNumberClassCodeHazardGradeValue temp2 = temp1.FindByHazardGradeTypeId(cnHgts[i].HazardGradeTypeId);
                    if (temp2 == null)
                        al.Add("0");
                    else
                        al.Add(temp2.HazardGradeValue);
                }
                string[] objs = al.ToArray();
                dt.Rows.Add(objs);
            }
            dt.AcceptChanges();

            // bubble max for each column into a new row
            DataRow dr = dt.NewRow();
            foreach (DataColumn dc in dt.Columns)
            {
                dr[dc] = dt.Select("", dc.ColumnName + " DESC")[0][dc];
            }


            string[] retArray = Array.ConvertAll<object, string>(dr.ItemArray, new Converter<object, string>(Convert.ToString));
            string retStr = string.Join(",", retArray);

            return retStr;
        }

        #endregion

        public static string UpdatePortFolio(string companyNumber, string id, string newName)
        {
            string retVal = Common.InvokeupdatePortfolio(companyNumber, id, newName);
            return retVal;
        }

        public static void UpdateClientNameAddressTypes(int companyNumber,long clientID, string addressTypes, string nameTypes)
        {
            if (string.IsNullOrEmpty(addressTypes) && string.IsNullOrEmpty(nameTypes))
                return;

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.Clear();

            dm.QueryCriteria.And(Biz.JoinPath.Client.Columns.CompanyNumberId, companyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.Client.Columns.ClientCoreClientId, clientID);

            Biz.Client client = dm.GetClient(Biz.FetchPath.Client.ClientCoreAddressAddressType, Biz.FetchPath.Client.ClientCoreNameClientNameType);
            dm.QueryCriteria.Clear();

            if (client == null)
            {
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, companyNumber);
                Biz.CompanyNumber cn = dm.GetCompanyNumber();
                client = dm.NewClient(clientID, cn);
            }

            Biz.Lookups lookups = Common.GetLookupManager();

            Biz.ClientNameTypeCollection clientNameTypes = lookups.ClientNameTypes;
            Biz.AddressTypeCollection clientAddressTypes = lookups.AddressTypes;

            //Clear existing
            if (!string.IsNullOrEmpty(addressTypes) && client.ClientCoreAddressAddressTypes !=null)
            {
                while (client.ClientCoreAddressAddressTypes.Count>0)
                {
                    client.ClientCoreAddressAddressTypes[0].Delete();
                }
            }

            if (!string.IsNullOrEmpty(nameTypes) && client.ClientCoreNameClientNameTypes != null)
            {
                while (client.ClientCoreNameClientNameTypes.Count > 0)
                {
                    client.ClientCoreNameClientNameTypes[0].Delete();
                }
            }

            dm.CommitAll();

            string[] nTypes = nameTypes.Split('|');
            string[] aTypes = addressTypes.Split('|');

            //add the client name types
            foreach (string item in nTypes)
            {
                if (string.IsNullOrEmpty(item))
                    continue;

                string[] idAndType = item.Split('*');
                if (idAndType.Length != 2)
                    continue;

                Biz.ClientNameType type = clientNameTypes.FindByNameType(idAndType[1]);

                if (type != null)
                {
                    Biz.ClientCoreNameClientNameType nameType = client.NewClientCoreNameClientNameType();
                    nameType.ClientNameType = type;
                    nameType.ClientCoreNameId = int.Parse(idAndType[0]);
                }
            }

            //add the client address types
            foreach (string item in aTypes)
            {
                if (string.IsNullOrEmpty(item))
                    continue;

                string[] idAndType = item.Split('*');
                if (idAndType.Length != 2)
                    continue;

                Biz.AddressType type = clientAddressTypes.FindByPropertyAddressType(idAndType[1]);

                if (type != null)
                {
                    Biz.ClientCoreAddressAddressType addressType = client.NewClientCoreAddressAddressType();
                    addressType.AddressType = type;
                    addressType.ClientCoreAddressId = int.Parse(idAndType[0]);
                }
            }

            dm.CommitAll();
        }

        public static string ModifyClientMembershipInfo(string companyNumber, string clientId, string membershipGroup)
        {
            string result = string.Empty;

            BizObjects.ClearanceClient client = GetClientByIdAsObject(companyNumber, clientId, true, false);

            BizObjects.Company company = Common.BuildCompany(companyNumber);

             structures.Import.ClientImportData idata = new BTS.ClientCore.Wrapper.Structures.Import.ClientImportData();
            idata.Signature = new BTS.ClientCore.Wrapper.Structures.Import.Signature("CLIENT IMPORT", "1.0", "en_US");

            idata.Client = new BTS.ClientCore.Wrapper.Structures.Import.ImportClient();

            idata.Client.ClientType = client.ClientCoreClient.Client.ClientType;
            idata.Client.CorporationCode = company.ClientCoreCorpCode;
            idata.Client.Owner = company.ClientCoreOwner;
            idata.Client.CCClientId = clientId;
            idata.Client.EffectiveDate = Common.ConvertToClientCoreDateTime(DateTime.Now);
            idata.Client.DuplicateClientOption = 1;
            idata.Client.PortfolioNames = new string[] { client.ClientCoreClient.Client.Portfolio.Name };            
            
            idata.Client.MembershipInfo.MembershipGroup = membershipGroup;

            List<structures.Import.NameInfo> nameInfos = new List<structures.Import.NameInfo>();
            if (client.ClientCoreClient.Client.Names != null && client.ClientCoreClient.Client.Names.Length > 0)
            {
                nameInfos.Add(new structures.Import.NameInfo() { BusinessName = client.ClientCoreClient.Client.Names[0].NameBusiness });
            }

            idata.Client.NameInfos = nameInfos.ToArray();

            string importXML = XML.Serializer.SerializeAsXml(idata);

            result = Common.InvokeimportClient(companyNumber, importXML);

            return result;
        }

        public static string GetClientProductRelation(string companyNumber, string clientId)
        {
            string result = string.Empty;

            clientProductRelation[] relation = Common.InvokeGetProductRelation(companyNumber, clientId);

            result = XML.Serializer.SerializeAsXml(relation);

            return result;
        }

        public static bool ClientMarkedAsDeleted(int companyNumberId, long clientId)
        {
            Biz.DataManager dm = Common.GetDataManager();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Client.Columns.CompanyNumberId, companyNumberId);
            dm.QueryCriteria.And(Biz.JoinPath.Client.Columns.ClientCoreClientId, clientId);

            Biz.Client client = dm.GetClient();

            return client != null && client.RequestToDelete.IsTrue;
        }

    }
}
