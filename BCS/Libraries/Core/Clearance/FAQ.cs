﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BCS.DAL;

namespace BCS.Core.Clearance
{
    public static class FAQManager
    {
        public static List<BCS.DAL.FAQ> GetFAQsForCompany(int companyNumberId)
        {
            List<BCS.DAL.FAQ> faqs = null;
            using (BCS.DAL.BCSContext bc = DataAccess.NewBCSContext())
            {
                faqs = bc.FAQ.Include("Role").Include("CompanyNumber").Where(f => f.CompanyNumberId == companyNumberId && f.Active).ToList();
            }

            return faqs;
        }

        public static List<User> GetUserForRoles(FAQ faq)
        {
            List<User> users = null;
            List<int> roleIds = faq.Role.Select(r => r.Id).ToList();

            using (BCS.DAL.BCSContext bc = DataAccess.NewBCSContext())
            {
                users = bc.User.Where(u => u.Role.Select(r1=> r1.Id).Where(r1 => roleIds.Contains(r1)).Count() > 0
                    && u.Company.Select(c=>c.Id).Contains(faq.CompanyNumber.CompanyId)
                    && u.Company.Count == 1
                    && !u.Role.Select(r=> r.Id).Contains(1)
                    && u.Active
                    ).OrderBy(u=>u.Username).ToList();
            }

            return users;
        }
    }
}
