using System;
using System.Data.SqlTypes;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Reflection;
using structures = BTS.ClientCore.Wrapper.Structures;
using BCS.Biz;
using System.Data.SqlClient;
using BTS.LogFramework;
using System.Web.UI;
using System.Linq;
using System.Web.Services;
using System.Web.Services.Protocols;


namespace BCS.Core.Clearance
{
    /// <summary>
    /// class handles Submissions Data Access 
    /// </summary>
    public class Submissions
    {
        #region Get Submissions

        public string GetSubmissionById(long id)
        {
            {
                ListDictionary values = new ListDictionary();
                values.Add("Id", id);

                string result = GetSubmissions(string.Empty, values);
                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string GetSubmissions(string companyNumber, ListDictionary dict)
        {
            OrmLib.DataManagerBase.CriteriaGroup criteria = new OrmLib.DataManagerBase.CriteriaGroup();
            BizObjects.SubmissionList list;
            if (dict.Contains("Id"))
            { criteria.And(Biz.JoinPath.Submission.Columns.Id, dict["Id"]); }
            else if (dict.Contains("ClientId"))
            { criteria.And(Biz.JoinPath.Submission.Columns.ClientCoreClientId, dict["ClientId"]); }
            else if (dict.Contains("ClientIds"))
            {
                string[] clientids = dict["ClientIds"].ToString().Split(",".ToCharArray());
                foreach (string id in clientids)
                    criteria.Or(Biz.JoinPath.Submission.Columns.ClientCoreClientId, id);
            }
            else if (dict.Contains("FEIN"))
            { criteria.And(Biz.JoinPath.Submission.Agency.Columns.FEIN, dict["FEIN"]); }
            else if (dict.Contains("AgencyId"))
            { criteria.And(Biz.JoinPath.Submission.Columns.AgencyId, dict["AgencyId"]); }
            else if (dict.Contains("AgentId"))
            { criteria.Or(Biz.JoinPath.Submission.Columns.AgentId, dict["AgentId"]); }
            else
            {
                // get clients from client core
                structures.Search.Vector result;

                if (dict.Contains("ClientTaxId"))
                {
                    result = (structures.Search.Vector)XML.Deserializer.Deserialize(
                        Clients.GetClientByTaxId(companyNumber, dict["ClientTaxId"].ToString()), typeof(structures.Search.Vector));
                }
                else if (dict.Contains("NameFirst"))
                {
                    if (dict.Contains("NameBusiness").ToString().Length == 0)
                    {
                        result = (structures.Search.Vector)XML.Deserializer.Deserialize(
                            Clients.GetClientsByName(companyNumber,
                            dict["NameFirst"].ToString(), dict["NameLast"].ToString()), typeof(structures.Search.Vector));

                    }
                    else
                    {
                        result = (structures.Search.Vector)XML.Deserializer.Deserialize(
                        Clients.GetClientByBusinessName(companyNumber, dict["NameBusiness"].ToString()), typeof(structures.Search.Vector));
                    }
                }
                else if (dict.Contains("HouseNo"))
                {
                    string temp = Clients.GetClientsByAddress(companyNumber, dict["HouseNo"].ToString(), dict["Address1"].ToString(),
                        dict["Address2"].ToString(),
                        dict["Address3"].ToString(), dict["Address4"].ToString(), dict["City"].ToString(),
                        dict["State"].ToString(), dict["County"].ToString(), dict["Country"].ToString(),
                        dict["Zip"].ToString());

                    result = (structures.Search.Vector)XML.Deserializer.Deserialize(temp, typeof(structures.Search.Vector));
                }
                else
                {
                    result = new BTS.ClientCore.Wrapper.Structures.Search.Vector();
                    result.Error = "No values were supplied.";
                }
                if (result.Error == null && result.Clients != null)
                {
                    foreach (structures.Search.Client client in result.Clients)
                    {
                        criteria.Or(Biz.JoinPath.Submission.Columns.ClientCoreClientId, client.Info.ClientId);
                    }
                }
                if (result.Clients == null)
                {
                    list = new BCS.Core.Clearance.BizObjects.SubmissionList();
                    list.Message = "No clients were found with the search criteria specified.";
                    string x = XML.Serializer.SerializeAsXml(list);
                    return x;
                }
            }


            Biz.DataManager dm = Common.GetDataManager();

            if (!dict.Contains("Id"))
            {
                criteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            }

            dm.QueryCriteria = criteria;
            Biz.SubmissionCollection scoll = dm.GetSubmissionCollection(SubmissionFetchPath());
            list = BuildSubmissionList(scoll, new string[] { }, new string[] { }, null, 0, 0);

            // return the string as serialized Response object
            string xml = XML.Serializer.SerializeAsXml(list);
            return xml;
        }

        private static OrmLib.DataManagerBase.FetchPathRelation[] SubmissionFetchPath()
        {
            return new OrmLib.DataManagerBase.FetchPathRelation[]
            {
                Biz.FetchPath.Submission.All,
                Biz.FetchPath.Submission.SubmissionCompanyNumberCode.CompanyNumberCode,
                Biz.FetchPath.Submission.SubmissionCompanyNumberCode.CompanyNumberCode.CompanyNumberCodeType,
                Biz.FetchPath.Submission.SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute,
                Biz.FetchPath.Submission.SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute.AttributeDataType,
                Biz.FetchPath.Submission.SubmissionLineOfBusiness.LineOfBusiness,
                Biz.FetchPath.Submission.SubmissionCompanyNumberAgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReason,
                Biz.FetchPath.Submission.SubmissionCompanyNumberAgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason,
                Biz.FetchPath.Submission.SubmissionLineOfBusinessChildren.CompanyNumberLOBGrid,
                Biz.FetchPath.Submission.SubmissionHistory
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="criteria">first - OrmLib.DataManagerBase.JoinPathRelation, second - data, third - OrmLib.MatchType </param>
        /// <returns></returns>
        public static string GetSubmissions(string companyNumber, bool includeDeleted, params System.Web.UI.Triplet[] criteria)
        {
            Biz.SubmissionCollection scoll = SearchSubmissions(companyNumber, includeDeleted, criteria);
            BizObjects.SubmissionList list = BuildSubmissionList(scoll, new string[] { }, new string[] { }, null, 0, 0);

            // return the string as serialized Response object
            string xml = XML.Serializer.SerializeAsXml(list);
            return xml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="criteria">first - OrmLib.DataManagerBase.JoinPathRelation, second - data, third - OrmLib.MatchType </param>
        /// <returns></returns>
        public static string GetSubmissions(string companyNumber, bool includeDeleted, System.Web.UI.Triplet[] criteria, Dictionary<int, string> attributes)
        {
            Biz.SubmissionCollection scoll = SearchSubmissions(companyNumber, includeDeleted, criteria);
            BizObjects.SubmissionList list = BuildSubmissionList(scoll, new string[] { }, new string[] { }, null, 0, 0);

            if (attributes != null && attributes.Count > 0)
            {
                List<BizObjects.Submission> filteredList = new List<BizObjects.Submission>();
                foreach (var s in list.List.Where(s => s.AttributeList != null))
                {
                    bool valid = true;
                    foreach (var attr in attributes)
                    {
                        var submissionAttr = s.AttributeList.FirstOrDefault(a => a.CompanyNumberAttributeId == attr.Key);
                        if (submissionAttr == null || submissionAttr.AttributeValue.IndexOf(attr.Value, StringComparison.CurrentCultureIgnoreCase) < 0)
                        {
                            valid = false;
                            break;
                        }
                    }

                    if (valid)
                        filteredList.Add(s);
                }

                list.List = filteredList.ToArray();
            }

            // return the string as serialized Response object
            string xml = XML.Serializer.SerializeAsXml(list);
            return xml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="policyNumber"></param>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static string GetSubmissionsByPolicyNumberAndCompanyNumber(string policyNumber, string companyNumber)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Company company = dm.GetCompany();
            //if (company.UsesAPS)
            //    return new APS.Submissions().GetSubmissionsByPolicyNumberAndCompanyNumber(policyNumber, companyNumber);

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber).And(
                Biz.JoinPath.Submission.Columns.PolicyNumber, policyNumber);
            Biz.SubmissionCollection scoll = dm.GetSubmissionCollection(SubmissionFetchPath());
            if (scoll != null)
                scoll = scoll.SortBySequence(OrmLib.SortDirection.Descending).SortByEffectiveDate(OrmLib.SortDirection.Descending);

            BizObjects.SubmissionList list = BuildSubmissionList(scoll, new string[] { }, new string[] { }, null, 0, 0);

            if (list != null && list.List != null)
                list.List = list.List.OrderByDescending(e => e.EffectiveDate).ThenByDescending(s => s.SubmissionNumberSequence).ToArray();

            string xml = XML.Serializer.SerializeAsXml(list);
            return xml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="submissionNumber"></param>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static string GetSubmissionBySubmissionNumberAndCompanyNumber(string submissionNumber, string companyNumber)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Company company = dm.GetCompany();
            //if (company.UsesAPS)
            //    return new APS.Submissions().GetSubmissionBySubmissionNumberAndCompanyNumber(submissionNumber, companyNumber);

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber).And(
                Biz.JoinPath.Submission.Columns.SubmissionNumber, submissionNumber);
            Biz.SubmissionCollection scoll = dm.GetSubmissionCollection(SubmissionFetchPath());
            if (scoll != null && scoll.Count > 0)
            {
                scoll = scoll.SortBySequence(OrmLib.SortDirection.Descending);
                BizObjects.Submission list = BuildSubmission(scoll[0]);
                string xml = XML.Serializer.SerializeAsXml(list);
                return xml;
            }
            return null;
        }

        public static string GetSubmissionbyEffectiveCompanyNumberClientAgencyUnit(DateTime effectiveDate, string companyNumber, int clientID
                                                           , string agencyNumber, string underwriterUnitCode)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.Clear();

            if (effectiveDate != DateTime.MinValue)
                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.EffectiveDate, effectiveDate);
            if (!string.IsNullOrEmpty(companyNumber))
                dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            if (clientID > 0)
                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.ClientCoreClientId, clientID);
            if (!string.IsNullOrEmpty(agencyNumber))
                dm.QueryCriteria.And(Biz.JoinPath.Submission.Agency.Columns.AgencyNumber, agencyNumber);
            if (!string.IsNullOrEmpty(underwriterUnitCode))
                dm.QueryCriteria.And(Biz.JoinPath.Submission.SubmissionLineOfBusiness.LineOfBusiness.Columns.Code, underwriterUnitCode);
            Biz.SubmissionCollection scoll = dm.GetSubmissionCollection(SubmissionFetchPath());
            if (scoll != null)
                scoll = scoll.SortBySequence(OrmLib.SortDirection.Descending);

            BizObjects.SubmissionList list = BuildSubmissionList(scoll, new string[] { }, new string[] { }, null, 0, 0);
            string xml = XML.Serializer.SerializeAsXml(list);
            return xml;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="submissionNumber"></param>
        /// <param name="companyNumber"></param>
        /// <param name="sequence"></param>
        /// <returns></returns>
        public static string GetSubmissionBySubmissionNumberAndCompanyNumber(
            string submissionNumber, string companyNumber, string sequence)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Company company = dm.GetCompany();
            //if (company.UsesAPS)
            //return new APS.Submissions().GetSubmissionBySubmissionNumberAndCompanyNumber(submissionNumber, companyNumber, sequence);

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(
                Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber).And(
                Biz.JoinPath.Submission.Columns.SubmissionNumber, submissionNumber).And(
                Biz.JoinPath.Submission.Columns.Sequence, sequence);
            Biz.Submission scoll = dm.GetSubmission(SubmissionFetchPath());
            if (scoll != null)
            {
                BizObjects.Submission list = BuildSubmission(scoll);
                string xml = XML.Serializer.SerializeAsXml(list);
                return xml;
            }
            return null;
        }


        #endregion

        #region Add Submissions

        public static string AddSubmission(string clientid, string clientaddressids, string clientnameids, string clientportfolioid,
            string submissionBy, string submissionDate, int typeid, int agencyid,
            int agentid, string agentreasonids, int contactid, string companynumber, int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber,
            decimal premium, string cocodeids, string attributes, string effective, string expiration,
            int underwriterid, int analystid, int technicianid, int policysystemid, string comments, string lobids, string lobchildren, string insuredname,
            string dba, string subno, int othercarrierid, decimal othercarrierpremium)
        {
            return AddSubmission(clientid, clientaddressids, clientnameids, clientportfolioid,
             submissionBy, submissionDate, typeid, agencyid,
             agentid, agentreasonids, contactid, companynumber, statusid, statusdate, statusreasonid, statusreasonother, policynumber,
             premium, cocodeids, attributes, effective, expiration,
             underwriterid, analystid, technicianid, policysystemid, comments, lobids, lobchildren, insuredname,
             dba, subno, othercarrierid, othercarrierpremium, null, null);
        }


        /// <summary>
        /// method to add a submission by mostly ids
        /// </summary>
        public static string AddSubmission(string clientid, string clientaddressids, string clientnameids, string clientportfolioid,
            string submissionBy, string submissionDate, int typeid, int agencyid,
            int agentid, string agentreasonids, int contactid, string companynumber, int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber,
            decimal premium, string cocodeids, string attributes, string effective, string expiration,
            int underwriterid, int analystid, int technicianid, int policysystemid, string comments, string lobids, string lobchildren, string insuredname,
            string dba, string subno, int othercarrierid, decimal othercarrierpremium, string clientAddressIDsAndTypes, string clientNameIdsAndTypes)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companynumber);
            Company company = dm.GetCompany(Biz.FetchPath.Company.CompanyParameter);
            //if(company.UsesAPS)
            //{
            //    BizObjects.Submission callAPS = new BCS.Core.Clearance.BizObjects.Submission();
            //    callAPS.Status = "Failure";
            //    callAPS.Message = "The company is setup as using APS. Use AddAPSSubmissionByIds method.";
            //    return XML.Serializer.SerializeAsXml(callAPS);
            //}

            BizObjects.Submission s = new BCS.Core.Clearance.BizObjects.Submission();

            //if (Common.IsNotNullOrEmpty(policynumber))
            //{
            //    // if sending to cobra then indicator is expected in the form of <poilcynumber>-{something}
            //    string[] polSend = policynumber.Split("-".ToCharArray(), StringSplitOptions.None);

            //    // we already checked if its null or empty, so after split the first elem should be the policy number
            //    policynumber = polSend[0];

            //    if (polSend.Length == 2)
            //    {
            //        if (Common.IsNotNullOrEmpty(polSend[0]) && Common.IsNotNullOrEmpty(polSend[1]))
            //        {
            //            dm.QueryCriteria.Clear();
            //            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companynumber);
            //            CompanyNumber cnum1 = dm.GetCompanyNumber(Biz.FetchPath.CompanyNumber.Company, Biz.FetchPath.CompanyNumber.Company.ClientSystemType);

            //            if (cnum1.Company.ClientSystemType.Code.Equals("BCSasClientCore", StringComparison.CurrentCultureIgnoreCase))
            //            {
            //                string message = Common.InvokeaddProspect(companynumber, clientid, policynumber);
            //                if (message == "policy already exists.")
            //                {
            //                    s.Message = message;
            //                    s.Status = "Failure";
            //                    return XML.Serializer.SerializeAsXml(s);
            //                }
            //            }
            //        }
            //    }
            //}

            string requiredMessagePrefix = "Required :-";

            if (cocodeids == null)
                cocodeids = string.Empty;
            if (attributes == null)
                attributes = string.Empty;
            string[] scompanycodeids = cocodeids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            int[] companycodeids = new int[scompanycodeids.Length];
            for (int i = 0; i < scompanycodeids.Length; i++)
            {
                companycodeids[i] = Convert.ToInt32(scompanycodeids[i]);
            }


            if (!Common.IsNotNullOrEmpty(submissionBy))
            {
                s.Message += requiredMessagePrefix + "Submission by.";
                s.Status = "Failure";
                return XML.Serializer.SerializeAsXml(s);
            }
            if (!Common.IsNotNullOrEmpty(clientid))
            {
                s.Message += requiredMessagePrefix + "Client Id.";
                s.Status = "Failure";
                return XML.Serializer.SerializeAsXml(s);
            }
            if (!Common.IsNotNullOrEmpty(clientaddressids))
            {
                s.Message += requiredMessagePrefix + "Client Address Id.";
                s.Status = "Failure";
                return XML.Serializer.SerializeAsXml(s);
            }

            // verify if agency exists
            Biz.Agency agency = Common.VerifyAgency(dm, agencyid);
            if (agency == null)
            {
                s.Message += requiredMessagePrefix + Messages.NoSuchAgency;
                s.Status = "Failure";
                return XML.Serializer.SerializeAsXml(s);
            }
            // verify if agent exists
            Biz.Agent agent = Common.VerifyAgent(dm, agentid);
            if (agent == null)
            {
                s.Message += Messages.NoSuchAgent;
                //return XML.Serializer.SerializeAsXml(s);
            }

            // verify if agent exists
            Biz.Contact contact = Common.VerifyContact(dm, contactid);
            if (contact == null)
            {
                s.Message += Messages.NoSuchContact;
                //return XML.Serializer.SerializeAsXml(s);
            }
            // verify company exists
            Biz.CompanyNumber cnum = Common.VerifyCompanyNumber(dm, companynumber);
            if (cnum == null)
            {
                s.Message += requiredMessagePrefix + Messages.NoSuchCompanyNumber;
                s.Status = "Failure";
                return XML.Serializer.SerializeAsXml(s);
            }
            // verify status exists
            Biz.SubmissionStatus status = Common.VerifySubmissionStatus(dm, statusid);
            if (status == null)
            {
                s.Message += Messages.NoSuchSubmissionStatus;
                s.Status = "Failure";
                //return XML.Serializer.SerializeAsXml(s);
            }

            //Ensure Default Submission Status
            if (company.CompanyParameters != null && company.CompanyParameters.Count > 0 && company.CompanyParameters[0].EnsureDefaultStatusOnAdd)
            {
                using (BCS.DAL.BCSContext bc = BCS.DAL.DataAccess.NewBCSContext())
                {
                    int? defaultStatus = (from ss in bc.CompanyNumberSubmissionStatus
                                          where ss.DefaultSelection == true
                                          && ss.CompanyNumber.CompanyNumber1 == companynumber
                                          select ss.SubmissionStatusId).FirstOrDefault();

                    if (defaultStatus != null && defaultStatus != statusid)
                        statusid = Convert.ToInt32(defaultStatus);
                }
            }


            // verify status reason exists
            Biz.SubmissionStatusReason statusreason = Common.VerifySubmissionStatusReason(dm, statusreasonid);
            if (statusreason == null)
            {
                s.Message += Messages.NoSuchSubmissionStatusReason;
                //return XML.Serializer.SerializeAsXml(s);
            }

            // verify token exists
            //Biz.SubmissionToken token = null;
            //Biz.SubmissionToken token = Common.VerifySubmissionToken(dm, tokenid);
            //if (token == null)
            //{
            //    s.Message += Messages.NoSuchSubmissionToken;
            //    return XML.Serializer.SerializeAsXml(s);
            //}
            // verify type exists
            Biz.SubmissionType type = Common.VerifySubmissionType(dm, typeid);
            if (type == null)
            {
                s.Message += requiredMessagePrefix + Messages.NoSuchSubmissionType;
                s.Status = "Failure";
                return XML.Serializer.SerializeAsXml(s);
            }

            //verify underwriter            
            Biz.Person uperson = Common.VerifyUnderWriter(dm, underwriterid);
            if (uperson == null)
            {
                s.Message += Messages.NoSuchUnderwriter;
                //return XML.Serializer.SerializeAsXml(s);
            }

            //verify analyst, since underwriter and analyst are basically persons, same method is used
            Biz.Person aperson = Common.VerifyUnderWriter(dm, analystid);
            if (aperson == null)
            {
                s.Message += Messages.NoSuchAnalyst;
                //return XML.Serializer.SerializeAsXml(s);
            }

            //verify technician, since underwriter and technician are basically persons, same method is used
            Biz.Person tperson = Common.VerifyUnderWriter(dm, technicianid);
            if (tperson == null)
            {
                s.Message += Messages.NoSuchTechnician;
                //return XML.Serializer.SerializeAsXml(s);
            }

            //verify Policy System
            Biz.PolicySystem apolicysystem = Common.VerifyPolicySystem(dm, policysystemid);
            if (apolicysystem == null)
            {
                s.Message += Messages.NoSuchPolicySystem;
                //return XML.Serializer.SerializeAsXml(s);
            }

            // verify line of business
            //Biz.LineOfBusiness lob = Common.VerifyLineOfBusiness(dm, lineofbusiness);
            //if (lob == null)
            //{
            //    s.Message += requiredMessagePrefix + "Line of business specified was not found.";
            //    s.Status = "Failure";
            //    return XML.Serializer.SerializeAsXml(s);
            //}

            // verify company number code 
            Biz.CompanyNumberCodeCollection cnc = Common.VerifyCompanyNumberCode(dm, companycodeids);
            if (cnc == null)
            {
                s.Message += Messages.NoSuchCompanyNumberCode;
                //return XML.Serializer.SerializeAsXml(s);
            }

            DateTime dtNow = DateTime.Now;

            int sequence = 0;
            long submissioNo = 0;
            Int64.TryParse(subno, out submissioNo);
            long controlNo =
            SubmissionNumberGenerator.GetSubmissionControlNumber(submissioNo, companynumber, out sequence);

            Biz.Submission submission = dm.NewSubmission(Common.ConvertToSqlInt64(clientid),
                Common.ConvertToSqlInt64(controlNo),
                submissionBy,
                Common.IsNotNullOrEmpty(submissionDate) ? Common.ConvertToSqlDateTime(submissionDate) : Common.ConvertToSqlDateTime(dtNow),
                 agency, cnum, type);

            submission.AgentId = agent != null ? agentid : System.Data.SqlTypes.SqlInt32.Null;
            submission.ContactId = contact != null ? contactid : System.Data.SqlTypes.SqlInt32.Null;
            submission.Sequence = sequence;
            submission.EffectiveDate = Common.ConvertToSqlDateTime(effective);
            submission.EstimatedWrittenPremium = premium;
            submission.OtherCarrierPremium = othercarrierpremium;
            submission.ExpirationDate = Common.ConvertToSqlDateTime(expiration);
            submission.PolicyNumber = policynumber != null ? policynumber.Trim() : policynumber;
            submission.UnderwriterPersonId = uperson != null ? underwriterid : System.Data.SqlTypes.SqlInt32.Null;
            submission.AnalystPersonId = aperson != null ? analystid : System.Data.SqlTypes.SqlInt32.Null;
            submission.TechnicianPersonId = tperson != null ? technicianid : System.Data.SqlTypes.SqlInt32.Null;
            submission.PolicySystemId = apolicysystem != null ? policysystemid : System.Data.SqlTypes.SqlInt32.Null;
            submission.OtherCarrierId = othercarrierid != 0 ? othercarrierid : SqlInt32.Null;
            submission.UpdatedBy = submissionBy;
            submission.UpdatedDt = Common.ConvertToSqlDateTime(dtNow);
            submission.InsuredName = insuredname;
            submission.Dba = dba;
            if (clientportfolioid != "0")
                submission.ClientCorePortfolioId = Common.ConvertToSqlInt64(clientportfolioid);
            else
                submission.ClientCorePortfolioId = Common.ConvertToSqlInt64(clientid);
            submission.SubmissionStatusId = status != null ? statusid : System.Data.SqlTypes.SqlInt32.Null;
            submission.SubmissionStatusDate = Common.ConvertToSqlDateTime(statusdate);
            submission.SubmissionStatusReason = statusreason;
            submission.SubmissionStatusReasonOther = statusreasonother;

            BCS.Biz.SubmissionStatusHistory submissionStatusHistory = submission.NewSubmissionStatusHistory();
            submissionStatusHistory.PreviousSubmissionStatusId = status == null ? System.Data.SqlTypes.SqlInt32.Null : status.Id;
            submissionStatusHistory.SubmissionStatusDate = Common.IsNotNullOrEmpty(statusdate) ? Common.ConvertToSqlDateTime(statusdate) :
                System.Data.SqlTypes.SqlDateTime.Null;
            submissionStatusHistory.StatusChangeBy = submissionBy;


            //if (submission.ExpirationDate >= agency.CancelDate)
            //{
            //    s.Message = "A submission cannot expire after the cancel agency's cancel date.";
            //    return XML.Serializer.SerializeAsXml(s);
            //}

            dm.QueryCriteria.Clear();
            string[] arrClientAddressIds = clientaddressids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string clientaddressid in arrClientAddressIds)
            {
                dm.NewSubmissionClientCoreClientAddress(Convert.ToInt32(clientaddressid), submission);
            }

            if (clientnameids != null)
            {
                string[] arrClientNameIds = clientnameids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string clientnameid in arrClientNameIds)
                {
                    dm.NewSubmissionClientCoreClientName(Convert.ToInt32(clientnameid), submission);
                }
            }

            //Call to update the name and adderss types for a client. 
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companynumber);
            Biz.CompanyNumber cn = dm.GetCompanyNumber();

            int companyNumberId;
            int.TryParse(cn.Id.ToString(), out companyNumberId);
            long longClient;
            long.TryParse(clientid, out longClient);
            BCS.Core.Clearance.Clients.UpdateClientNameAddressTypes(companyNumberId, longClient, clientAddressIDsAndTypes, clientNameIdsAndTypes);

            //Biz.SubmissionLineOfBusiness slob = submission.NewSubmissionLineOfBusiness();
            //slob.LineOfBusinessId = lineofbusiness;            

            string[] lineofbusinesses = lobids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, lineofbusinesses, OrmLib.MatchType.In);
            Biz.LineOfBusinessCollection nlobs = dm.GetLineOfBusinessCollection();
            foreach (Biz.LineOfBusiness nlob in nlobs)
            {
                dm.NewSubmissionLineOfBusiness(nlob, submission);
            }

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Id, companycodeids, OrmLib.MatchType.In);
            Biz.CompanyNumberCodeCollection ncnc = dm.GetCompanyNumberCodeCollection();
            foreach (Biz.CompanyNumberCode iid in ncnc)
            {
                dm.NewSubmissionCompanyNumberCode(iid, submission);
            }

            //dm.QueryCriteria.Clear();
            //dm.QueryCriteria.And(Biz.JoinPath.SubmissionNumberControl.Columns.CompanyNumberId,cnum.Id);
            //Biz.SubmissionNumberControl snc = dm.GetSubmissionNumberControl();
            //if (snc != null)
            //{
            //    if (sequence == 1)
            //        snc.ControlNumber = Common.ConvertToSqlInt64(controlNo);
            //}
            //else
            //    dm.NewSubmissionNumberControl(10000, cnum);

            string[] tattributes = attributes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string ta in tattributes)
            {
                string[] ts = ta.Split("=".ToCharArray());
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.Columns.Id, ts[0]);
                dm.NewSubmissionCompanyNumberAttributeValue(dm.GetCompanyNumberAttribute(), submission).AttributeValue = ts[1];
            }

            #region Submission Line Of Business Children

            if (lobchildren == null)
                lobchildren = string.Empty;
            string[] lobchildrenarray = lobchildren.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string lobchild in lobchildrenarray)
            {
                string[] lobchildparts = lobchild.Split("=".ToCharArray(), StringSplitOptions.None);

                // index 0 contains Id, index length-1 contains deleted
                if (lobchildparts[0] == "0" && lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
                    continue;

                Biz.SubmissionLineOfBusinessChildren nslobc = submission.NewSubmissionLineOfBusinessChildren();
                nslobc.CompanyNumberLOBGridId = Convert.ToInt32(lobchildparts[1]);
                nslobc.SubmissionStatusId = lobchildparts[2] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[2]);
                nslobc.SubmissionStatusReasonId = lobchildparts[3] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[3]);
            }


            #endregion

            #region Submission Comments

            if (comments == null)
                comments = string.Empty;
            string[] lobcommentarray = comments.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string lobchild in lobcommentarray)
            {
                string[] lobchildparts = lobchild.Split(new string[] { "=====" }, StringSplitOptions.None);

                // index 0 contains Id, index length-1 contains deleted
                if (lobchildparts[0] == "0" && lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
                    continue;

                Biz.SubmissionComment nslobc = submission.NewSubmissionComment();
                nslobc.Comment = lobchildparts[1];
                nslobc.EntryBy = lobchildparts[2];
                nslobc.EntryDt = Convert.ToDateTime(lobchildparts[3]);
            }


            #endregion

            if (Common.IsNotNullOrEmpty(agentreasonids))
            {
                string[] tagentreasonids = agentreasonids.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string tarids in tagentreasonids)
                {
                    SubmissionCompanyNumberAgentUnspecifiedReason tSubmissionAgentUnspecifiedReason =
                        submission.NewSubmissionCompanyNumberAgentUnspecifiedReason();
                    tSubmissionAgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReasonId = Convert.ToInt32(tarids);
                }
            }

            submission.SystemDt = dtNow;

            string result = dm.CommitAll();

            if (submission != null)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, submission.Id);
                submission = dm.GetSubmission(SubmissionFetchPath());

                s = BuildSubmission(submission);
                s.Message += Messages.SuccessfulAddSubmission;
                s.Status = "Success";

                // do the OFAC Check here 
                PerformOFACCheck(submission.SubmissionNumber.ToString(), companynumber, company.Id, clientid);
                // remove the cache dependency used to cache submissions for page
                System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
                return XML.Serializer.SerializeAsXml(s);
            }
            else
            {
                s.Message += Messages.UnsuccessfulAddSubmission;
                s.Status = "Failure";
                return XML.Serializer.SerializeAsXml(s);
            }
        }


        public static string PerformOFACCheck(string submissionNumber, string companyNumber, int companyId, string clientId)
        {
            string clientNameValue = "";
            string address = "";
            string zipCode = "";
            string city = "";
            string country = "";
            string state = "";


            string clientxml = BCS.Core.Clearance.Clients.GetClientById(companyNumber, clientId, true, false);
            BCS.Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                    (BCS.Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                    clientxml, typeof(BCS.Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

            try
            {
                if (!string.IsNullOrEmpty(clearanceclient.ClientCoreClient.Client.ClientId))
                {
                    BTS.ClientCore.Wrapper.Structures.Info.ClientInfo _client = clearanceclient.ClientCoreClient;
                    if (_client.Addresses != null && _client.Client.Names != null)
                    {
                        BTS.ClientCore.Wrapper.Structures.Info.Name name = _client.Client.Names[0];
                        BTS.ClientCore.Wrapper.Structures.Info.Address addr = _client.Addresses[0];

                        clientNameValue = name.NameBusiness;
                        address = addr.HouseNumber + addr.Address1 + addr.Address2;
                        city = addr.City;
                        state = addr.StateProvinceId;
                        country = addr.Country;
                        zipCode = addr.PostalCode;

                        AdvClientSearch.advClientSearchServiceClient advcs = new AdvClientSearch.advClientSearchServiceClient();

                        //AdvClientSearch.AdvClientSearchWebServicePublicService advSearch = new AdvClientSearch.AdvClientSearchWebServicePublicService();

                        BCS.DAL.CompanyClientAdvancedSearch advSrchAttributes = BCS.Core.Clearance.SubmissionsEF.GetAdvSearchAttributesByCompany(companyId);

                        if (advSrchAttributes != null)
                        {
                            System.ServiceModel.EndpointAddress uri = new System.ServiceModel.EndpointAddress(advSrchAttributes.ClientAdvSearchEndPointURL);
                            advcs.Endpoint.Address = uri;

                            AdvClientSearch.Security advs = new AdvClientSearch.Security();

                            AdvClientSearch.UsernameTokenType utoken = new AdvClientSearch.UsernameTokenType();

                            AdvClientSearch.AttributedString atts = new AdvClientSearch.AttributedString();
                            atts.Value = advSrchAttributes.Username;

                            utoken.Username = atts;

                            advs.UsernameToken = utoken;
                            advs.dsik = advSrchAttributes.DSIK;

                            AdvClientSearch.performOFACSearch ofacchk = new AdvClientSearch.performOFACSearch();
                            ofacchk.name = clientNameValue;
                            ofacchk.address = address;
                            ofacchk.city = city;
                            ofacchk.state = state;
                            ofacchk.zip = zipCode;
                            ofacchk.country = country;
                            ofacchk.function = string.Format("Submission # :{0}", submissionNumber);

                            AdvClientSearch.AdvClientSearchRslt_Type result = advcs.performOFACSearch(advs, ofacchk);
                        }



                    }
                }

            }
            catch (Exception ex)
            {
                LogCentral.Current.LogWarn(string.Format("Could not do an OFAC check on Submission Number  = {0} for ClientId = {1}", submissionNumber, clearanceclient.ClientCoreClientId, ex));

            }

            return string.Empty;
        }
        //public static string AddSubmission(string clientid, string clientaddressid, string clientportfolioid,
        //    string by, int companynumber,
        //     string typeabbr, int agentid, string statuscode, string policynumber, decimal premium, string codename,
        //     string effective, string expiration, int underwriterid, int analystid, string comments, string updatedby,
        //     string lineofbusinesscode, string insuredname, string dba, string subno)
        //{
        //    BizObjects.Submission s = new BCS.Core.Clearance.BizObjects.Submission();

        //    Biz.DataManager dm = Common.GetDataManager();

        //    // get the company
        //    dm.QueryCriteria.Clear();
        //    dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companynumber);
        //    Biz.CompanyNumberCollection bizc = dm.GetCompanyNumberCollection();
        //    if (bizc == null || bizc.Count == 0)
        //    {
        //        s.Message += Messages.NoSuchCompanyNumber;
        //        return XML.Serializer.SerializeAsXml(s);
        //    }
        //    else
        //    {
        //        if (bizc.Count > 1)
        //        {
        //            s.Message += string.Format(Messages.MultipleCompaniesFound, "Company Number", companynumber);
        //            return XML.Serializer.SerializeAsXml(s);
        //        }

        //    }
        //    int companyid = bizc[0].CompanyId.Value;
        //    int companynumberid = bizc[0].Id;

        //    dm.QueryCriteria.Clear();
        //    dm.QueryCriteria.And(Biz.JoinPath.SubmissionType.Columns.Abbreviation, typeabbr);
        //    Biz.SubmissionTypeCollection bizt = dm.GetSubmissionTypeCollection();
        //    if (bizt == null || bizt.Count == 0)
        //    {
        //        s.Message += Messages.NoSuchSubmissionType;
        //        return XML.Serializer.SerializeAsXml(s);
        //    }
        //    else
        //    {
        //        if (bizt.Count > 1)
        //        {
        //            s.Message += string.Format(Messages.MultipleSubmissionTypesFound, "Submission Type", typeabbr);
        //            return XML.Serializer.SerializeAsXml(s);
        //        }

        //    }
        //    int typeid = bizt[0].Id;

        //    dm.QueryCriteria.Clear();
        //    dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.CompanyNumberId, companyid);
        //    Biz.AgencyCollection biza = dm.GetAgencyCollection();
        //    if (biza == null || biza.Count == 0)
        //    {
        //        s.Message += Messages.NoSuchAgency;
        //        return XML.Serializer.SerializeAsXml(s);
        //    }
        //    else
        //    {
        //        if (biza.Count > 1)
        //        {
        //            s.Message += string.Format(Messages.MultipleAgenciesFound, "Company Number", companynumber);
        //            return XML.Serializer.SerializeAsXml(s);
        //        }

        //    }
        //    int agencyid = biza[0].Id;

        //    int statusid = 0;
        //    dm.QueryCriteria.Clear();
        //    dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatus.Columns.StatusCode, statuscode);
        //    Biz.SubmissionStatusCollection bizs = dm.GetSubmissionStatusCollection();
        //    if (bizs == null || bizs.Count == 0)
        //    {
        //        s.Message += Messages.NoSuchSubmissionStatus;
        //        //return XML.Serializer.SerializeAsXml(s);
        //    }
        //    else
        //    {
        //        if (bizs.Count > 1)
        //        {
        //            s.Message += string.Format(Messages.MultipleSubmissionStatusesFound, "Status Code", statuscode);
        //            return XML.Serializer.SerializeAsXml(s);
        //        }
        //        statusid = bizs[0].Id;
        //    }


        //    dm.QueryCriteria.Clear();
        //    dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Code, lineofbusinesscode);
        //    Biz.LineOfBusinessCollection bizl = dm.GetLineOfBusinessCollection();
        //    if (bizl == null || bizl.Count == 0)
        //    {
        //        s.Message += Messages.NoSuchAgency;
        //        return XML.Serializer.SerializeAsXml(s);
        //    }
        //    else
        //    {
        //        if (bizl.Count > 1)
        //        {
        //            s.Message += string.Format(Messages.MultipleLineOfBusinessFound, "Line of Business Code", lineofbusinesscode);
        //            return XML.Serializer.SerializeAsXml(s);
        //        }

        //    }
        //    int lineid = bizl[0].Id;

        //    dm.QueryCriteria.Clear();
        //    Biz.SubmissionToken ntoken
        //        = dm.NewSubmissionToken(Common.GetUserId(), Common.ConvertToSqlDateTime(DateTime.Now));
        //    if (ntoken == null)
        //    {
        //        s.Message += Messages.UnsuccessfulSubmissionToken;
        //        return XML.Serializer.SerializeAsXml(s);
        //    }



        //    dm.QueryCriteria.Clear();
        //    dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.CompanyNumberCodeType.Columns.CodeName, codename);
        //    Biz.CompanyNumberCodeCollection bizct = dm.GetCompanyNumberCodeCollection();
        //    if (bizct == null || bizct.Count == 0)
        //    {
        //        s.Message += Messages.NoSuchCompanyNumberCode;
        //        return XML.Serializer.SerializeAsXml(s);
        //    }
        //    else
        //    {
        //        if (bizct.Count > 1)
        //        {
        //            s.Message += string.Format(Messages.MultipleCompanyNumberCodes, "Line of Business Code", lineofbusinesscode);
        //            return XML.Serializer.SerializeAsXml(s);
        //        }

        //    }
        //    int companynumbercodeid = bizct[0].Id;

        //    dm.CommitAll();

        //    int tokenid = ntoken.Id;


        //    return AddSubmission(clientid, clientaddressid, clientportfolioid, by, companyid, typeid, agencyid,
        //        agentid, companynumber, statusid, policynumber, premium, companynumbercodeid.ToString(),
        //        effective, expiration, underwriterid, analystid,
        //        comments, updatedby, lineid, insuredname,dba, subno);
        //}




        #endregion

        #region Modify Submissions

        public static string ModifySubmission(string id, long clientid, string clientaddressids, string clientnameids,
           long clientportfolioid, int companyid, int typeid, int agencyid, int agentid, string agentreasonids, int contactid, int companynumberid,
           int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string cocodeids, string attributes, decimal premium,
           string effective, string expiration, int underwriterid, int analystid, int technicianid, int policysystemid, string comments, string updatedby,
           string lineofbusinessesids, string lobchildren, string insuredname, string dba, int othercarrierid, decimal othercarrierpremium)
        {
            return ModifySubmission(id, clientid, clientaddressids, clientnameids,
             clientportfolioid, companyid, typeid, agencyid, agentid, agentreasonids, contactid, companynumberid,
             statusid, statusdate, statusreasonid, statusreasonother, policynumber, cocodeids, attributes, premium,
             effective, expiration, underwriterid, analystid, technicianid, policysystemid, comments, updatedby,
             lineofbusinessesids, lobchildren, insuredname, dba, othercarrierid, othercarrierpremium
            , null, null);
        }

        /// <summary>
        /// most parameters by Id
        /// </summary>       
        public static string ModifySubmission(string id, long clientid, string clientaddressids, string clientnameids,
            long clientportfolioid, int companyid, int typeid, int agencyid, int agentid, string agentreasonids, int contactid, int companynumberid,
            int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string cocodeids, string attributes, decimal premium,
            string effective, string expiration, int underwriterid, int analystid, int technicianid, int policysystemid, string comments, string updatedby,
            string lineofbusinessesids, string lobchildren, string insuredname, string dba, int othercarrierid, decimal othercarrierpremium
            , string clientAddressIDsAndTypes, string clientNameIdsAndTypes)
        {
            string requiredMessagePrefix = "Required :-";

            if (!Common.IsNotNullOrEmpty(id))
            {
                BizObjects.Submission _submission = new BCS.Core.Clearance.BizObjects.Submission();
                _submission.Message = requiredMessagePrefix + "Id.";
                _submission.Status = "Failure";
                return XML.Serializer.SerializeAsXml(_submission);
            }

            if (!Common.IsNotNullOrEmpty(updatedby))
            {
                BizObjects.Submission _submission = new BCS.Core.Clearance.BizObjects.Submission();
                _submission.Message = requiredMessagePrefix + "updated by.";
                _submission.Status = "Failure";
                return XML.Serializer.SerializeAsXml(_submission);
            }
            if (agencyid == 0)
            {
                BizObjects.Submission _submission = new BCS.Core.Clearance.BizObjects.Submission();
                _submission.Message = requiredMessagePrefix + "Agency.";
                _submission.Status = "Failure";
                return XML.Serializer.SerializeAsXml(_submission);
            }

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, id);
            Biz.Submission submission = dm.GetSubmission(SubmissionFetchPath());

            if (submission == null)
            {
                BizObjects.Submission _submission = new BCS.Core.Clearance.BizObjects.Submission();
                _submission.Message = string.Format("Submission with Id {0} does not exist.", id);
                _submission.Status = "Failure";
                return XML.Serializer.SerializeAsXml(_submission);
            }

            // required by db schema
            if (agencyid != 0)
                submission.AgencyId = agencyid;
            if (agentid != 0)
                submission.AgentId = agentid;
            else
                submission.AgentId = SqlInt32.Null;

            if (contactid != 0)
                submission.ContactId = contactid;
            else
                submission.ContactId = SqlInt32.Null;

            if (clientid != 0)
                submission.ClientCoreClientId = Common.ConvertToSqlInt64(clientid);
            else
                submission.ClientCoreClientId = SqlInt64.Null;

            // required by db schema
            if (companynumberid != 0)
                submission.CompanyNumberId = companynumberid;

            submission.EffectiveDate = Common.ConvertToSqlDateTime(effective);

            submission.EstimatedWrittenPremium = premium;
            submission.OtherCarrierPremium = othercarrierpremium;

            if (Common.IsNotNullOrEmpty(expiration))
                submission.ExpirationDate = Common.ConvertToSqlDateTime(expiration);
            else
                submission.ExpirationDate = SqlDateTime.Null;

            submission.PolicyNumber = policynumber.Trim();

            submission.InsuredName = insuredname;

            submission.Dba = dba;

            if (clientportfolioid != 0)
                submission.ClientCorePortfolioId = Common.ConvertToSqlInt64(clientportfolioid);
            else
                submission.ClientCorePortfolioId = submission.ClientCoreClientId;

            #region Status and StatusHistory update
            // TODO: analyze #790 #952
            System.Data.SqlTypes.SqlInt32 prevStatus = submission.SubmissionStatusId;
            if ((prevStatus.IsNull && statusid == 0) && (submission.SubmissionStatusDate == Common.ConvertToSqlDateTime(statusdate)))
            { }
            else
            {
                int prevstatus = prevStatus.IsNull ? 0 : prevStatus.Value;
                if (prevstatus != statusid || (submission.SubmissionStatusDate.IsNull) || submission.SubmissionStatusDate != Common.ConvertToSqlDateTime(statusdate))
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatus.Columns.Id, statusid);
                    BCS.Biz.SubmissionStatus submissionStatus = dm.GetSubmissionStatus();

                    if (submissionStatus != null)
                    {
                        dm.NewSubmissionStatusHistory(statusid, updatedby, submission).SubmissionStatusDate = Common.ConvertToSqlDateTime(statusdate); ;
                    }
                }
            }
            if (statusid != 0)
                submission.SubmissionStatusId = statusid;
            else
                submission.SubmissionStatusId = SqlInt32.Null;
            if (Common.IsNotNullOrEmpty(statusdate))
                submission.SubmissionStatusDate = Common.ConvertToSqlDateTime(statusdate);
            else
                submission.SubmissionStatusDate = SqlDateTime.Null;

            if (statusreasonid != 0)
                submission.SubmissionStatusReasonId = statusreasonid;
            else
                submission.SubmissionStatusReasonId = System.Data.SqlTypes.SqlInt32.Null;

            submission.SubmissionStatusReasonOther = statusreasonother;
            #endregion

            if (Common.IsNotNullOrEmpty(attributes))
            {
                string[] sa = attributes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string ts in sa)
                {
                    string[] ta = ts.Split("=".ToCharArray());
                    Biz.SubmissionCompanyNumberAttributeValue var = submission.SubmissionCompanyNumberAttributeValues.FindByCompanyNumberAttributeId(ta[0]);
                    if (var != null)
                    {
                        if (var.AttributeValue == ta[1])
                            continue;
                        else
                        {
                            var.AttributeValue = ta[1];
                        }
                    }
                    else
                    {
                        dm.QueryCriteria.Clear();
                        dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberAttribute.Columns.Id, ta[0]);
                        dm.NewSubmissionCompanyNumberAttributeValue(dm.GetCompanyNumberAttribute(), submission).AttributeValue = ta[1];
                    }
                }
            }
            // required by schema
            if (typeid != 0)
                submission.SubmissionTypeId = typeid;

            if (underwriterid != 0)
                submission.UnderwriterPersonId = underwriterid;
            else
                submission.UnderwriterPersonId = SqlInt32.Null;
            if (analystid != 0)
                submission.AnalystPersonId = analystid;
            else
                submission.AnalystPersonId = SqlInt32.Null;

            if (technicianid != 0)
                submission.TechnicianPersonId = technicianid;
            else
                submission.TechnicianPersonId = SqlInt32.Null;

            if (othercarrierid != 0)
                submission.OtherCarrierId = othercarrierid;
            else
                submission.OtherCarrierId = SqlInt32.Null;

            if (policysystemid != 0)
                submission.PolicySystemId = policysystemid;
            else
                submission.PolicySystemId = SqlInt32.Null;

            submission.UpdatedBy = updatedby;

            submission.UpdatedDt = DateTime.Now;

            // if submission.attribute is not empty and parameter is, then don't update the submission.attribute            
            //if (lineofbusiness != 0)
            //{
            //    Biz.SubmissionLineOfBusiness slob = submission.SubmissionLineOfBusiness;
            //    if (slob != null)
            //    {
            //        slob.LineOfBusinessId = lineofbusiness;
            //    }
            //    else
            //    {
            //        // if submission.attribute is not empty and parameter is, then don't update the submission.attribute
            //        slob = submission.NewSubmissionLineOfBusiness();
            //        slob.LineOfBusinessId = lineofbusiness;
            //    }
            //}
            if (Common.IsNotNullOrEmpty(lineofbusinessesids))
            {
                int lei = submission.SubmissionLineOfBusinesss.Count;

                for (int i = 0; i < lei; i++)
                {
                    submission.SubmissionLineOfBusinesss[(lei - i) - 1].Delete();
                }

                dm.CommitAll();

                string[] slineofbusinessesids = lineofbusinessesids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int[] ilineofbusinessesids = new int[slineofbusinessesids.Length];
                for (int i = 0; i < slineofbusinessesids.Length; i++)
                {
                    ilineofbusinessesids[i] = Convert.ToInt32(slineofbusinessesids[i]);
                }

                dm.QueryCriteria.Clear();

                dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, ilineofbusinessesids, OrmLib.MatchType.In);
                Biz.LineOfBusinessCollection lobs = dm.GetLineOfBusinessCollection();

                foreach (Biz.LineOfBusiness lob in lobs)
                    dm.NewSubmissionLineOfBusiness(lob, submission);
            }



            #region Old Address Handling
            //Biz.SubmissionClientCoreClientAddress scliaddr = submission.SubmissionClientCoreClientAddress;
            //if (scliaddr == null)
            //{
            //    // if submission.attribute is not empty and parameter is, then don't update the submission.attribute
            //    if (clientaddressid != 0)
            //        dm.NewSubmissionClientCoreClientAddress(clientaddressid, submission);
            //}
            //else
            //{
            //    if (clientaddressid != 0)
            //        scliaddr.ClientCoreClientAddressId = clientaddressid;
            //    else
            //        scliaddr.Delete();
            //} 
            #endregion

            #region New Address handling
            int le = submission.SubmissionClientCoreClientAddresss.Count;

            for (int i = 0; i < le; i++)
            {
                submission.SubmissionClientCoreClientAddresss[(le - i) - 1].Delete();
            }

            dm.CommitAll();

            if (Common.IsNotNullOrEmpty(clientaddressids))
            {
                string[] arrAddressids = clientaddressids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string anAddressId in arrAddressids)
                {
                    submission.NewSubmissionClientCoreClientAddress().ClientCoreClientAddressId = Convert.ToInt32(anAddressId);
                }
            }

            #endregion
            #region New Name handling
            le = submission.SubmissionClientCoreClientNames.Count;

            for (int i = 0; i < le; i++)
            {
                submission.SubmissionClientCoreClientNames[(le - i) - 1].Delete();
            }

            dm.CommitAll();

            if (Common.IsNotNullOrEmpty(clientnameids))
            {
                string[] arrNameids = clientnameids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string aNameId in arrNameids)
                {
                    submission.NewSubmissionClientCoreClientName().ClientCoreNameId = Convert.ToInt32(aNameId);
                }
            }

            #endregion

            ModifySubmissionCompanyNumberCodes(submission, cocodeids);

            //Call to update the name and adderss types for a client. 
            BCS.Core.Clearance.Clients.UpdateClientNameAddressTypes(companynumberid, clientid, clientAddressIDsAndTypes, clientNameIdsAndTypes);

            #region Submission Line Of Business Children

            if (Common.IsNotNullOrEmpty(lobchildren))
            {
                string[] lobchildrenarray = lobchildren.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string lobchild in lobchildrenarray)
                {
                    string[] lobchildparts = lobchild.Split("=".ToCharArray(), StringSplitOptions.None);

                    // index 0 contains Id, index length-1 contains deleted
                    if (lobchildparts[0] == "0")
                    {
                        if (lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
                            continue;
                        else
                        {
                            Biz.SubmissionLineOfBusinessChildren child = submission.NewSubmissionLineOfBusinessChildren();
                            child.CompanyNumberLOBGridId = Convert.ToInt32(lobchildparts[1]);
                            child.SubmissionStatusId = lobchildparts[2] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[2]);
                            child.SubmissionStatusReasonId = lobchildparts[3] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[3]);
                        }
                    }
                    else
                    {
                        if (lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
                        {
                            submission.SubmissionLineOfBusinessChildrens.FindById(lobchildparts[0]).Delete();
                        }
                        else
                        {
                            Biz.SubmissionLineOfBusinessChildren child = submission.SubmissionLineOfBusinessChildrens.FindById(lobchildparts[0]);
                            child.CompanyNumberLOBGridId = Convert.ToInt32(lobchildparts[1]);
                            child.SubmissionStatusId = lobchildparts[2] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[2]);
                            child.SubmissionStatusReasonId = lobchildparts[3] == "0" ? SqlInt32.Null : Convert.ToInt32(lobchildparts[3]);
                        }
                    }
                }
            }
            #endregion

            #region Submission Comment

            if (Common.IsNotNullOrEmpty(comments))
            {
                string[] lobcommentrenarray = comments.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string lobcomment in lobcommentrenarray)
                {
                    string[] lobcommentparts = lobcomment.Split(new string[] { "=====" }, StringSplitOptions.None);

                    // index 0 contains Id, index length-1 contains deleted
                    if (lobcommentparts[0] == "0")
                    {
                        if (lobcommentparts[lobcommentparts.Length - 1].ToLower() == "true")
                            continue;
                        else
                        {
                            Biz.SubmissionComment comment = submission.NewSubmissionComment();
                            comment.Comment = lobcommentparts[1];
                            comment.EntryBy = lobcommentparts[2];
                            comment.EntryDt = Convert.ToDateTime(lobcommentparts[3]);
                        }
                    }
                    else
                    {
                        if (lobcommentparts[lobcommentparts.Length - 1].ToLower() == "true")
                        {
                            submission.SubmissionComments.FindById(lobcommentparts[0]).Delete();
                        }
                        else
                        {
                            Biz.SubmissionComment comment = submission.SubmissionComments.FindById(lobcommentparts[0]);
                            if (comment.Comment != lobcommentparts[1])
                            {
                                comment.Comment = lobcommentparts[1];
                                comment.ModifiedBy = lobcommentparts[2];
                                comment.ModifiedDt = Convert.ToDateTime(lobcommentparts[3]);
                            }
                        }
                    }
                }
            }
            #endregion

            #region Agent Unspecified reasons
            Dictionary<int, int> oldReasons = new Dictionary<int, int>();
            foreach (Biz.SubmissionCompanyNumberAgentUnspecifiedReason bizreason in submission.SubmissionCompanyNumberAgentUnspecifiedReasons)
            {
                oldReasons.Add(bizreason.CompanyNumberAgentUnspecifiedReasonId, bizreason.CompanyNumberAgentUnspecifiedReasonId);
            }
            Dictionary<int, int> newReasons = new Dictionary<int, int>();
            if (agentreasonids == null)
                agentreasonids = string.Empty;
            string[] tagentreasonids = agentreasonids.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string st in tagentreasonids)
            {
                newReasons.Add(Convert.ToInt32(st), Convert.ToInt32(st));
            }
            List<int> reasonsToBeInserted = new List<int>();
            List<int> reasonsToBeDeleted = new List<int>();
            //Dictionary<int, int[]> reasonsToBeUpdated = new Dictionary<int, int[]>();


            foreach (int intVar in oldReasons.Keys)
            {
                if (newReasons.ContainsKey(intVar))
                {
                    //if (newValues[intVar] == oldValues[intVar])
                    //{
                    //    /* leave alone -  */
                    //}
                    //else
                    //{
                    //    reasonsToBeUpdated.Add(intVar, new int[] { oldValues[intVar], newValues[intVar] });
                    //}
                }
                else
                    reasonsToBeDeleted.Add(oldReasons[intVar]);
            }
            foreach (int intVar in newReasons.Keys)
            {
                if (oldReasons.ContainsKey(intVar))
                { /* we have already taken care of this condition in previous loop */ }
                else
                {
                    reasonsToBeInserted.Add(newReasons[intVar]);
                }
            }
            foreach (int intVar in reasonsToBeInserted)
            {
                Biz.SubmissionCompanyNumberAgentUnspecifiedReason submissionCompanyNumberAgentUnspecifiedReason =
                    submission.NewSubmissionCompanyNumberAgentUnspecifiedReason();
                submissionCompanyNumberAgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReasonId = intVar;
            }
            foreach (int intVar in reasonsToBeDeleted)
            {
                submission.SubmissionCompanyNumberAgentUnspecifiedReasons.FindByCompanyNumberAgentUnspecifiedReasonId(intVar).Delete();
            }
            #endregion



            dm.CommitAll();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, id);
            submission = dm.GetSubmission(SubmissionFetchPath());
            BizObjects.Submission s = BuildSubmission(submission);
            s.Message += Messages.SuccessfulEditSubmission;
            s.Status = "Success";
            // remove the cache dependency used to cache submissions for page
            System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
            return XML.Serializer.SerializeAsXml(s);

        }

        public static void CopySubmissionStatus(int srcSubmissionId, int tgtSubmissionId)
        {
            Biz.DataManager dm = Common.GetDataManager();


            dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, srcSubmissionId);
            Submission srcSub = dm.GetSubmission(Biz.FetchPath.Submission.SubmissionStatusHistory);

            dm.QueryCriteria.Clear();

            dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, tgtSubmissionId);
            Submission tgtSub = dm.GetSubmission(Biz.FetchPath.Submission.SubmissionStatusHistory);

            while (tgtSub.SubmissionStatusHistorys.Count > 0)
            {
                tgtSub.SubmissionStatusHistorys[0].Delete();
            }

            SubmissionStatusHistoryCollection histories = srcSub.SubmissionStatusHistorys.SortByStatusChangeDt(OrmLib.SortDirection.Descending);
            double i = 1.0;
            foreach (SubmissionStatusHistory item in histories)
            {
                if (item.Active)
                {
                    SubmissionStatusHistory copiedHis = dm.NewSubmissionStatusHistory(item.PreviousSubmissionStatusId, tgtSub.SubmissionBy, tgtSub);
                    copiedHis.Active = item.Active;
                    copiedHis.StatusChangeDt = DateTime.Now.AddMinutes(i * -1);
                    copiedHis.SubmissionStatusDate = DateTime.Now.Date;
                    i++;
                }
            }

            tgtSub.SubmissionStatusId = srcSub.SubmissionStatusId;

            tgtSub.SubmissionStatusReasonId = srcSub.SubmissionStatusReasonId;

            dm.CommitAll();
        }

        private static void ModifySubmissionCompanyNumberCodes(BCS.Biz.Submission submission, string codeids)
        {
            Dictionary<int, int> oldValues = new Dictionary<int, int>();
            foreach (Biz.SubmissionCompanyNumberCode cnc in submission.SubmissionCompanyNumberCodes)
            {
                oldValues.Add(cnc.CompanyNumberCode.CompanyCodeTypeId.Value, cnc.CompanyNumberCodeId);
            }

            Dictionary<int, int> newValues = new Dictionary<int, int>();

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(
                Biz.JoinPath.CompanyNumberCode.Columns.Id,
                codeids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries),
                OrmLib.MatchType.In);
            Biz.CompanyNumberCodeCollection coll = dm.GetCompanyNumberCodeCollection();
            foreach (Biz.CompanyNumberCode cnc in coll)
            {
                newValues.Add(cnc.CompanyCodeTypeId.Value, cnc.Id);
            }

            List<int> toBeInserted = new List<int>();
            List<int> toBeDeleted = new List<int>();
            Dictionary<int, int[]> toBeUpdated = new Dictionary<int, int[]>();


            foreach (int intVar in oldValues.Keys)
            {
                if (newValues.ContainsKey(intVar))
                {
                    if (newValues[intVar] == oldValues[intVar])
                    {
                        /* leave alone -  */
                    }
                    else
                    {
                        toBeUpdated.Add(intVar, new int[] { oldValues[intVar], newValues[intVar] });
                    }
                }
                else
                    toBeDeleted.Add(oldValues[intVar]);
            }
            foreach (int intVar in newValues.Keys)
            {
                if (oldValues.ContainsKey(intVar))
                { /* we have already taken care of this condition in previous loop */ }
                else
                {
                    toBeInserted.Add(newValues[intVar]);
                }
            }

            foreach (int[] intVar in toBeUpdated.Values)
            {
                submission.SubmissionCompanyNumberCodes.FindByCompanyNumberCodeId(intVar[0]).CompanyNumberCodeId = intVar[1];
            }
            foreach (int intVar in toBeInserted)
            {
                Biz.SubmissionCompanyNumberCode sCode = submission.NewSubmissionCompanyNumberCode();
                sCode.CompanyNumberCodeId = intVar;
            }
            foreach (int intVar in toBeDeleted)
            {
                submission.SubmissionCompanyNumberCodes.FindByCompanyNumberCodeId(intVar).Delete();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientid"></param>
        /// <param name="portfoilioid"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string UpdatePortfolioId(string companyNumber, long clientid, long portfoilioid, string user)
        {
            if (portfoilioid == 0)
                return string.Empty;
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.ClientCoreClientId, clientid);
            Biz.SubmissionCollection submissions = dm.GetSubmissionCollection();

            foreach (Biz.Submission submission in submissions)
            {
                if (portfoilioid == 0 || (!submission.ClientCorePortfolioId.IsNull && submission.ClientCorePortfolioId.Value == portfoilioid))
                    continue;
                submission.ClientCorePortfolioId = portfoilioid == 0 ?
                    System.Data.SqlTypes.SqlInt64.Null : portfoilioid;
                submission.UpdatedDt = DateTime.Now;
                submission.UpdatedBy = user;
            }
            string result = dm.CommitAll();
            return string.Empty;
        }

        /// <summary>
        /// deletes existing addresses and adds new ones provided
        /// </summary>
        /// <param name="submissionId"></param>
        /// <param name="addressids"></param>
        /// <returns></returns>
        public static string UpdateSubmissionClientAddresses(int submissionId, string clientaddressids)
        {
            DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(JoinPath.Submission.Columns.Id, submissionId);

            Submission submission = dm.GetSubmission();

            int le = submission.SubmissionClientCoreClientAddresss.Count;

            for (int i = 0; i < le; i++)
            {
                submission.SubmissionClientCoreClientAddresss[(le - i) - 1].Delete();
            }

            try
            {
                dm.CommitAll();
            }
            catch (Exception)
            {
                return "There was a problem updating the client core addresses.";
            }

            if (Common.IsNotNullOrEmpty(clientaddressids))
            {
                string[] arrAddressids = clientaddressids.Split("|".ToCharArray());
                foreach (string anAddressId in arrAddressids)
                {
                    submission.NewSubmissionClientCoreClientAddress().ClientCoreClientAddressId = Convert.ToInt32(anAddressId);
                }
            }
            try
            {
                dm.CommitAll();
            }
            catch (Exception)
            {
                return "There was a problem updating the client core addresses.";
            }
            System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
            return "Addresses were successfully updated.";
        }

        /// <summary>
        /// switches all sequences of multiple submission's to a new client
        /// </summary>
        /// <param name="companyNumber">the company number which the submissions belong</param>
        /// <param name="numbers">submission or policy numbers</param>
        /// <param name="clientInfo">: delimited client information. clientid:nameids:addressids:insuredname:dba. insuredname and dba are not updated if empty</param>
        /// <param name="numberType">SubmissionNumber or PolicyNumber</param>
        /// <returns>serialized response</returns>
        public string SwitchClient(string companyNumber, string numbers, string clientInfo, string numberType)
        {
            if (string.IsNullOrEmpty(companyNumber))
            {
                BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
                fResponse.Message = "Company Number is required.";
                fResponse.Status = "Failure";
                return XML.Serializer.SerializeAsXml(fResponse);
            }
            if (string.IsNullOrEmpty(numbers) || string.IsNullOrEmpty(clientInfo))
            {
                BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
                fResponse.Message = string.IsNullOrEmpty(numbers) ? "No submission numbers were passed. Unable to process." : "No client ids were passed. Unable to process.";
                fResponse.Status = "Failure";
                return XML.Serializer.SerializeAsXml(fResponse);
            }
            string[] numbersStringArray = numbers.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            long[] numbersArray = new long[numbersStringArray.Length];
            switch (numberType)
            {
                case "SubmissionNumber":
                    {
                        try
                        {
                            numbersArray = Array.ConvertAll<string, long>(numbersStringArray, new Converter<string, long>(Convert.ToInt64));
                        }
                        catch (Exception)
                        {
                            BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
                            fResponse.Message = "Invalid Submission number(s).";
                            fResponse.Status = "Failure";
                            return XML.Serializer.SerializeAsXml(fResponse);
                        }
                        break;
                    }
                default:
                    break;
            }

            SqlTransaction transaction = null;
            Biz.SubmissionCollection bizSubmissions = new SubmissionCollection();
            Biz.DataManager dm = Common.GetDataManager(out transaction);
            dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            switch (numberType)
            {
                case "SubmissionNumber":
                    {
                        dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.SubmissionNumber, numbersStringArray, OrmLib.MatchType.In);
                        break;
                    }
                case "PolicyNumber":
                    {
                        dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.PolicyNumber, numbersStringArray, OrmLib.MatchType.In);
                        break;
                    }
                default:
                    break;
            }
            try
            {
                bizSubmissions = dm.GetSubmissionCollection(Biz.FetchPath.Submission.SubmissionClientCoreClientAddress,
                        Biz.FetchPath.Submission.SubmissionClientCoreClientName);
            }
            catch (Exception)
            {
                BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
                fResponse.Message = "There was a problem retrieving the submissions. Please try again.";
                fResponse.Status = "Failure";
                return XML.Serializer.SerializeAsXml(fResponse);
            }

            List<string> notFoundList = new List<string>();

            for (int i = 0; i < numbersStringArray.Length; i++)
            {
                MethodInfo mi = typeof(BCS.Biz.SubmissionCollection).GetMethod("FilterBy" + numberType, new Type[] { typeof(string) });
                Biz.SubmissionCollection filtered = (Biz.SubmissionCollection)mi.Invoke(bizSubmissions, new object[] { numbersStringArray[i] });

                if (null == filtered || 0 == filtered.Count)
                {
                    notFoundList.Add(numbersStringArray[i]);
                    continue;
                }
                string clientInfoString = clientInfo.Trim(',');
                {
                    string[] clientInfoArray = clientInfoString.Split(":".ToCharArray(), StringSplitOptions.None);
                    if (clientInfoArray.Length != 5) // client id, name ids, address ids, insured name, dba
                    {
                        BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
                        fResponse.Message = "Invalid client information passed.";
                        fResponse.Status = "Failure";
                        return XML.Serializer.SerializeAsXml(fResponse);
                    }
                    long clientIdLong = 0;
                    try
                    {
                        // validate client id
                        clientIdLong = Convert.ToInt64(clientInfoArray[0]);
                    }
                    catch (FormatException)
                    {
                        BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
                        fResponse.Message = "Invalid Client Id(s).";
                        fResponse.Status = "Failure";
                        return XML.Serializer.SerializeAsXml(fResponse);
                    }
                    foreach (Biz.Submission bizSubmission in filtered)
                    {
                        bizSubmission.ClientCoreClientId = clientIdLong;
                    }
                    #region name handling
                    string[] clientNameIdArray = clientInfoArray[1].Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    int[] clientNameIdArrayInt = new int[clientNameIdArray.Length];
                    try
                    {
                        // validate client name id
                        clientNameIdArrayInt = Array.ConvertAll<string, int>(clientNameIdArray, new Converter<string, int>(Convert.ToInt32));
                    }
                    catch (Exception)
                    {
                        BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
                        fResponse.Message = "Invalid Client Name Id(s).";
                        fResponse.Status = "Failure";
                        return XML.Serializer.SerializeAsXml(fResponse);
                    }
                    #region delete old name assocs
                    foreach (Biz.Submission bizSubmission in filtered)
                    {
                        int nameCount = bizSubmission.SubmissionClientCoreClientNames.Count;

                        for (int nameIndex = 0; nameIndex < nameCount; nameIndex++)
                        {
                            bizSubmission.SubmissionClientCoreClientNames[(nameCount - nameIndex) - 1].Delete();
                        }
                    }
                    dm.CommitAll();
                    #endregion
                    #region add new name assocs
                    foreach (Biz.Submission bizSubmission in filtered)
                    {
                        foreach (int nameIdInt in clientNameIdArrayInt)
                        {
                            Biz.SubmissionClientCoreClientName newName = bizSubmission.NewSubmissionClientCoreClientName();
                            newName.ClientCoreNameId = nameIdInt;
                        }
                    }
                    #endregion
                    #endregion
                    #region address handling
                    string[] clientaddressIdArray = clientInfoArray[2].Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    int[] clientaddressIdArrayInt = new int[clientaddressIdArray.Length];
                    try
                    {
                        // validate client address id
                        clientaddressIdArrayInt = Array.ConvertAll<string, int>(clientaddressIdArray, new Converter<string, int>(Convert.ToInt32));
                    }
                    catch (Exception)
                    {
                        BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
                        fResponse.Message = "Invalid Client address Id(s).";
                        fResponse.Status = "Failure";
                        return XML.Serializer.SerializeAsXml(fResponse);
                    }
                    #region delete old address assocs
                    foreach (Biz.Submission bizSubmission in filtered)
                    {
                        int addressCount = bizSubmission.SubmissionClientCoreClientAddresss.Count;

                        for (int addressIndex = 0; addressIndex < addressCount; addressIndex++)
                        {
                            bizSubmission.SubmissionClientCoreClientAddresss[(addressCount - addressIndex) - 1].Delete();
                        }
                    }
                    dm.CommitAll();
                    #endregion
                    #region add new address assocs
                    foreach (Biz.Submission bizSubmission in filtered)
                    {
                        foreach (int addressIdInt in clientaddressIdArrayInt)
                        {
                            Biz.SubmissionClientCoreClientAddress newaddress = bizSubmission.NewSubmissionClientCoreClientAddress();
                            newaddress.ClientCoreClientAddressId = addressIdInt;
                        }
                    }
                    #endregion
                    #endregion

                    if (!string.IsNullOrEmpty(clientInfoArray[3]))
                    {
                        foreach (Biz.Submission bizSubmission in filtered)
                        {
                            bizSubmission.InsuredName = clientInfoArray[3];
                        }
                    }
                    if (!string.IsNullOrEmpty(clientInfoArray[4]))
                    {
                        foreach (Biz.Submission bizSubmission in filtered)
                        {
                            bizSubmission.Dba = clientInfoArray[4];
                        }
                    }
                }
            }
            try
            {
                dm.CommitAll();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
                fResponse.Message = "An unknown error occurred. Please try again.";
                fResponse.Status = "Failure";
                LogCentral.Current.LogFatal("Switch Clients", ex);
                transaction.Rollback();
                return XML.Serializer.SerializeAsXml(fResponse);
            }
            if (notFoundList.Count == 0)
            {
                BCS.Core.Clearance.BizObjects.Response sResponse = new BCS.Core.Clearance.BizObjects.Response();
                sResponse.Message = "Submissions were successfully switched.";
                sResponse.Status = "Success";
                return XML.Serializer.SerializeAsXml(sResponse);
            }
            else
            {
                BCS.Core.Clearance.BizObjects.Response sResponse = new BCS.Core.Clearance.BizObjects.Response();
                sResponse.Message = string.Format("Submissions were successfully switched. The following {0} submissions were not found.",
                    string.Join(", ", notFoundList.ToArray()));
                sResponse.Status = "Success";
                return XML.Serializer.SerializeAsXml(sResponse);
            }
        }
        #endregion


        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="Id"></param>
        //public static void SetCompany(int Id)
        //{
        //    Common.SetCompany(Id);
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="companyNumber"></param>
        //public static void SetCompanyByCompanyNumber(int companyNumber)
        //{
        //    Biz.DataManager dm = Common.GetDataManager();
        //    dm.QueryCriteria.And(Biz.JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
        //    Biz.Company c = dm.GetCompany();
        //    if (c != null)
        //        SetCompany(dm.GetCompany().Id);
        //}

        #region Search submissions

        internal enum MatchCriteria
        {
            And = 1,
            Or
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public static string SearchSubmissions(string companyNumber, int rows)
        {
            //Biz.SubmissionCollection submissions = GetSubmissions(companyNumber);
            //BizObjects.SubmissionList list =
            //    BuildSubmissionList(submissions, new string[] { }, new object[] { }, new OrmLib.CompareType[] { }, rows, 1);

            //string xml = SerializeSubmissionList(list);
            //return xml;

            return SearchSubmissionsByCompanyNumber(companyNumber, rows, 1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissions(string companyNumber, int pageSize, int pageNo)
        {
            //Biz.SubmissionCollection submissions = GetSubmissions(companyNumber); ;

            //BizObjects.SubmissionList list =
            //    BuildSubmissionList(submissions, new string[] { }, new object[] { }, new OrmLib.CompareType[] { }, pageSize, pageNo);

            //string xml = SerializeSubmissionList(list);            
            //return xml;

            return SearchSubmissionsByCompanyNumber(companyNumber, pageSize, pageNo);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="agencyNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByAgencyNumber(string companyNumber, string agencyNumber, int pageSize, int pageNo)
        {
            return SearchSubmissionsByAgencyNumber(companyNumber, agencyNumber, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByAgencyNumber(string companyNumber, string agencyNumber, int pageSize, int pageNo, bool includeDeleted)
        {

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Agency.Columns.AgencyNumber,
               agencyNumber, OrmLib.MatchType.Partial);
            System.Web.UI.Triplet tr1 = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Agency.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber, OrmLib.MatchType.Exact);

            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri, tr1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="agencyName"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByAgencyName(string companyNumber, string agencyName, int pageSize, int pageNo)
        {
            return SearchSubmissionsByAgencyName(companyNumber, agencyName, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByAgencyName(string companyNumber, string agencyName, int pageSize, int pageNo, bool includeDeleted)
        {
            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Agency.Columns.AgencyName,
                agencyName, OrmLib.MatchType.Partial);
            System.Web.UI.Triplet tr1 = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Agency.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber, OrmLib.MatchType.Exact);

            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri, tr1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByCompanyNumber(string companyNumber, int pageSize, int pageNo)
        {
            return SearchSubmissionsByCompanyNumber(companyNumber, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByCompanyNumber(string companyNumber, int pageSize, int pageNo, bool includeDeleted)
        {

            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="portfolioid"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByClientCoreClientPortfolioId(string companyNumber, long portfolioid, int pageSize, int pageNo)
        {
            return SearchSubmissionsByClientCoreClientPortfolioId(companyNumber, portfolioid, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByClientCoreClientPortfolioId(string companyNumber, long portfolioid, int pageSize, int pageNo, bool includeDeleted)
        {

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.ClientCorePortfolioId, portfolioid.ToString(), OrmLib.MatchType.Partial);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="clientid"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByClientCoreClientId(string companyNumber, long clientid, int pageSize, int pageNo)
        {
            return SearchSubmissionsByClientCoreClientId(companyNumber, clientid, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByClientCoreClientId(string companyNumber, long clientid, int pageSize, int pageNo, bool includeDeleted)
        {
            //return GetSubmissions(companyNumber, "ClientCoreClientId", clientid, pageSize, pageNo);

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.ClientCoreClientId, clientid.ToString(), OrmLib.MatchType.Partial);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="clientid"></param>
        /// <param name="agencyid">agency Id for submissions are searched under its master umbrella</param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        /// 
        public static string SearchByClientCoreClientIdAndAgencyId(string companyNumber, long clientid, long agencyid, int pageSize, int pageNo)
        {
            return SearchByClientCoreClientIdAndAgencyId(companyNumber, clientid, agencyid, pageSize, pageNo, true);
        }
        public static string SearchByClientCoreClientIdAndAgencyId(string companyNumber, long clientid, long agencyid, int pageSize, int pageNo, bool includeDeleted)
        {
            if (true)
            {
                Biz.DataManager dm = Common.GetDataManager();
                dm.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                //if (dm.GetCompany().UsesAPS)
                //{
                //    return new APS.Submissions().SearchByClientCoreClientIdAndAgencyId(companyNumber, clientid, agencyid, pageSize, pageNo);
                //}
            }
            if (agencyid >= 0) // get only submissions tied to agency and its children
            {
                Biz.DataManager dm = Common.GetDataManager();
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agencyid);
                Biz.Agency masterAgency = dm.GetAgency(Biz.FetchPath.Agency.ChildRelationAgency);
                if (null == masterAgency)
                    return BuildEmptySubmissionList();
                long[] agencyIds = new long[masterAgency.ChildRelationAgencys.Count + 1];
                for (int i = 0; i < masterAgency.ChildRelationAgencys.Count; i++)
                {
                    agencyIds[i] = masterAgency.ChildRelationAgencys[i].Id;
                }
                agencyIds[masterAgency.ChildRelationAgencys.Count] = agencyid;

                System.Web.UI.Triplet[] triplets = new System.Web.UI.Triplet[]{
                new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.ClientCoreClientId, clientid.ToString(), OrmLib.MatchType.Exact),
                new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.AgencyId, agencyIds, OrmLib.MatchType.In)};
                Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, triplets);

                BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

                return SerializeSubmissionList(sl);
            }
            else
            {
                System.Web.UI.Triplet[] triplets = new System.Web.UI.Triplet[]{
                new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.ClientCoreClientId, clientid.ToString(), OrmLib.MatchType.Exact)};
                Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, triplets);

                BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

                return SerializeSubmissionList(sl);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="person"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByAgencyContactPerson(string companyNumber, string person, int pageSize, int pageNo)
        {
            return SearchSubmissionsByAgencyContactPerson(companyNumber, person, pageSize, pageNo, true);
        }

        public static string SearchSubmissionsByAgencyContactPerson(string companyNumber, string person, int pageSize, int pageNo, bool includeDeleted)
        {
            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Agency.Columns.ContactPerson,
                person, OrmLib.MatchType.Partial);
            System.Web.UI.Triplet tr1 = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Agency.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber, OrmLib.MatchType.Exact);

            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri, tr1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="statusCode"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsBySubmissionStatus(string companyNumber, string statusCode, int pageSize, int pageNo)
        {
            return SearchSubmissionsBySubmissionStatus(companyNumber, statusCode, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsBySubmissionStatus(string companyNumber, string statusCode, int pageSize, int pageNo, bool includeDeleted)
        {
            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.SubmissionStatus.Columns.StatusCode,
                statusCode, OrmLib.MatchType.Partial);
            System.Web.UI.Triplet tr1 = new System.Web.UI.Triplet(
                Biz.JoinPath.Submission.SubmissionStatus.CompanyNumberSubmissionStatus.CompanyNumber.Columns.PropertyCompanyNumber,
                companyNumber, OrmLib.MatchType.Exact);

            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri, tr1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="number"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsBySubmissionNumber(string companyNumber, long number, int pageSize, int pageNo, bool includeDeleted)
        {
            //return GetSubmissions(companyNumber,"SubmissionNumber", number, pageSize, pageNo);

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.SubmissionNumber, number.ToString(), OrmLib.MatchType.Partial);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="number"></param>
        /// <param name="sequence"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsBySubmissionNumberAndSequence(string companyNumber, long number, int sequence, int pageSize, int pageNo, bool includeDeleted)
        {

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.SubmissionNumber, number.ToString(), OrmLib.MatchType.Partial);
            System.Web.UI.Triplet tri1 = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.Sequence, sequence.ToString(), OrmLib.MatchType.Partial);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri, tri1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="number"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByPolicyNumber(string companyNumber, string number, int pageSize, int pageNo, bool includeDeleted)
        {

            OrmLib.MatchType matchType = OrmLib.MatchType.Exact;
            if (companyNumber == "060")
            //if (companyNumber == "058" || companyNumber == "060" || companyNumber == "076" || companyNumber == "050" || companyNumber == "018")
            {
                matchType = OrmLib.MatchType.EndsWith;
            }

            //System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.PolicyNumber, number, matchType);
            System.Web.UI.Triplet newTri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.PolicyNumberDigitsOnly, Convert.ToInt64(number), OrmLib.MatchType.Exact);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, newTri);


            //List<BCS.DAL.Submission> submissions = BCS.Core.Clearance.SubmissionsEF.GetSubmissionsByPolicyNumber(companyNumber, number, includeDeleted, matchType);
            //BizObjects.SubmissionList sl = BCS.Core.Clearance.SubmissionsEF.BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);
            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByPremiumRange(string companyNumber, decimal from, decimal to, int pageSize, int pageNo, bool includeDeleted)
        {
            //return GetSubmissions(companyNumber, "EstimatedWrittenPremium", from, to, pageSize, pageNo);

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.EstimatedWrittenPremium, from, OrmLib.MatchType.GreaterOrEqual);
            System.Web.UI.Triplet tri1 = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.EstimatedWrittenPremium, to, OrmLib.MatchType.LesserOrEqual);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri, tri1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByEffectiveDateRange(string companyNumber, DateTime from, DateTime to, int pageSize, int pageNo, bool includeDeleted)
        {
            //return GetSubmissions(companyNumber, "EffectiveDate", from, to, pageSize, pageNo);

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.EffectiveDate, from, OrmLib.MatchType.GreaterOrEqual);
            System.Web.UI.Triplet tri1 = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.EffectiveDate, to, OrmLib.MatchType.LesserOrEqual);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri, tri1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByExpirationDateRange(string companyNumber, DateTime from, DateTime to, int pageSize, int pageNo, bool includeDeleted)
        {
            //return GetSubmissions(companyNumber, "ExpirationDate", from, to, pageSize, pageNo);

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.ExpirationDate, from, OrmLib.MatchType.GreaterOrEqual);
            System.Web.UI.Triplet tri1 = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.ExpirationDate, to, OrmLib.MatchType.LesserOrEqual);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri, tri1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsBySubmissionDateRange(string companyNumber, DateTime from, DateTime to, int pageSize, int pageNo, bool includeDeleted)
        {
            //return GetSubmissions(companyNumber, "SubmissionDt", from, to, pageSize, pageNo);

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.SubmissionDt, from, OrmLib.MatchType.GreaterOrEqual);
            System.Web.UI.Triplet tri1 = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.SubmissionDt, to, OrmLib.MatchType.LesserOrEqual);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri, tri1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);


        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="name"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByInsuredName(string companyNumber, string name, int pageSize, int pageNo)
        {
            return SearchSubmissionsByInsuredName(companyNumber, name, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByInsuredName(string companyNumber, string name, int pageSize, int pageNo, bool includeDeleted)
        {
            //return GetSubmissions(companyNumber,"InsuredName", name, pageSize, pageNo);

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.InsuredName, name, OrmLib.MatchType.Partial);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="dba"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByDBA(string companyNumber, string dba, int pageSize, int pageNo)
        {
            return SearchSubmissionsByDBA(companyNumber, dba, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByDBA(string companyNumber, string dba, int pageSize, int pageNo, bool includeDeleted)
        {
            //return GetSubmissions(companyNumber,"Dba", dba, pageSize, pageNo);

            System.Web.UI.Triplet tri = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.Dba, dba, OrmLib.MatchType.Partial);
            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, tri);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }

        static bool itemRemoved = false;
        static System.Web.Caching.CacheItemRemovedReason reason;
        static System.Web.Caching.CacheItemRemovedCallback onRemove = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="k"></param>
        /// <param name="v"></param>
        /// <param name="r"></param>
        public static void RemovedCallback(String k, Object v, System.Web.Caching.CacheItemRemovedReason r)
        {
            itemRemoved = true;
            reason = r;
        }
        private static string SerializeSubmissionList(BizObjects.SubmissionList list)
        {
            return XML.Serializer.SerializeAsXml(list);
        }

        internal static BCS.Core.Clearance.BizObjects.Submission BuildSubmission(BCS.Biz.Submission s)
        {
            if (s == null)
                throw new ArgumentNullException();

            Biz.SubmissionCollection coll = new BCS.Biz.SubmissionCollection();
            coll.Add(s);

            BizObjects.SubmissionList list = BuildSubmissionList(coll, new string[] { }, new string[] { }, null, 0, 0);

            return list.List[0];
        }

        internal static BCS.Core.Clearance.BizObjects.SubmissionList BuildSubmissionList(
            BCS.Biz.SubmissionCollection collection, string[] properties, object[] values, OrmLib.CompareType[] comparers,
            int pageSize, int pageNo)
        {
            return BuildSubmissionList(collection, properties, values, comparers, pageSize, pageNo, MatchCriteria.And);
        }

        internal static BCS.Core.Clearance.BizObjects.SubmissionList BuildSubmissionList(
            BCS.Biz.SubmissionCollection collection, string[] properties, object[] values, OrmLib.CompareType[] comparers,
            int pageSize, int pageNo, MatchCriteria crit)
        {
            if (pageNo < 0)
                return new BizObjects.SubmissionList();
            if (pageNo == 0)
                pageSize = 0;
            if (pageNo > 0 && pageSize < 0)
                pageSize = 0;


            bool Match = properties.Length == 0;
            BizObjects.SubmissionList list = new BCS.Core.Clearance.BizObjects.SubmissionList();

            int min = Int32.MinValue;
            int max = Int32.MaxValue;
            if (pageSize > 0)
            {
                min = (pageNo - 1) * pageSize;
                max = pageNo * pageSize;
            }
            List<BizObjects.Submission> al = new List<BCS.Core.Clearance.BizObjects.Submission>();
            int i = 0;
            BizObjects.Submission bos;
            foreach (Biz.Submission _Submission in collection)
            {
                if (!Match)
                {
                    switch (crit)
                    {
                        case MatchCriteria.And:
                            {
                                Match = MatchAnd(_Submission, comparers, properties, values);
                                break;
                            }
                        case MatchCriteria.Or:
                            {
                                Match = MatchOr(_Submission, comparers, properties, values);
                                break;
                            }
                        default:
                            break;
                    }
                }
                if (Match)
                {
                    bos = new BCS.Core.Clearance.BizObjects.Submission();

                    bos.SubmissionNumberSequence = _Submission.Sequence.Value;

                    if (_Submission.SubmissionLineOfBusinesss != null && _Submission.SubmissionLineOfBusinesss.Count > 0)
                    {
                        bos.LineOfBusinessId = _Submission.SubmissionLineOfBusinesss[0].LineOfBusinessId;
                        bos.LineOfBusiness = _Submission.SubmissionLineOfBusinesss[0].LineOfBusiness.Description;
                        bos.LineOfBusinessList = new BCS.Core.Clearance.BizObjects.LineOfBusiness[_Submission.SubmissionLineOfBusinesss.Count];
                        for (int j = 0; j < _Submission.SubmissionLineOfBusinesss.Count; j++)
                        {
                            bos.LineOfBusinessList[j] = new BCS.Core.Clearance.BizObjects.LineOfBusiness();
                            bos.LineOfBusinessList[j].Code = _Submission.SubmissionLineOfBusinesss[j].LineOfBusiness.Code;
                            bos.LineOfBusinessList[j].Description = _Submission.SubmissionLineOfBusinesss[j].LineOfBusiness.Description;
                            bos.LineOfBusinessList[j].Id = _Submission.SubmissionLineOfBusinesss[j].LineOfBusiness.Id;
                            bos.LineOfBusinessList[j].Line = _Submission.SubmissionLineOfBusinesss[j].LineOfBusiness.Line;
                        }
                    }

                    if (_Submission.SubmissionCompanyNumberAttributeValues != null && _Submission.SubmissionCompanyNumberAttributeValues.Count > 0)
                    {
                        bos.AttributeList = new BCS.Core.Clearance.BizObjects.SubmissionAttribute[_Submission.SubmissionCompanyNumberAttributeValues.Count];
                        for (int j = 0; j < _Submission.SubmissionCompanyNumberAttributeValues.Count; j++)
                        {
                            bos.AttributeList[j] = new BCS.Core.Clearance.BizObjects.SubmissionAttribute();
                            bos.AttributeList[j].AttributeValue = _Submission.SubmissionCompanyNumberAttributeValues[j].AttributeValue;
                            bos.AttributeList[j].CompanyNumberAttributeId = _Submission.SubmissionCompanyNumberAttributeValues[j].CompanyNumberAttributeId;
                            bos.AttributeList[j].Label = _Submission.SubmissionCompanyNumberAttributeValues[j].CompanyNumberAttribute.Label;
                            bos.AttributeList[j].DataType = _Submission.SubmissionCompanyNumberAttributeValues[j].CompanyNumberAttribute.AttributeDataType.DataTypeName;
                        }
                    }
                    if (_Submission.SubmissionCompanyNumberAgentUnspecifiedReasons != null && _Submission.SubmissionCompanyNumberAgentUnspecifiedReasons.Count > 0)
                    {
                        bos.AgentUnspecifiedReasonList = new BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason[_Submission.SubmissionCompanyNumberAgentUnspecifiedReasons.Count];
                        for (int j = 0; j < _Submission.SubmissionCompanyNumberAgentUnspecifiedReasons.Count; j++)
                        {
                            bos.AgentUnspecifiedReasonList[j] = new BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason();
                            bos.AgentUnspecifiedReasonList[j].CompanyNumberAgentUnspecifiedReasonId =
                                _Submission.SubmissionCompanyNumberAgentUnspecifiedReasons[j].CompanyNumberAgentUnspecifiedReasonId;
                            bos.AgentUnspecifiedReasonList[j].Reason =
                                    _Submission.SubmissionCompanyNumberAgentUnspecifiedReasons[j].CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason.Reason;
                        }
                    }

                    if (_Submission.SubmissionCompanyNumberCodes != null && _Submission.SubmissionCompanyNumberCodes.Count > 0)
                    {
                        bos.SubmissionCodeTypess = new BCS.Core.Clearance.BizObjects.SubmissionCodeType[_Submission.SubmissionCompanyNumberCodes.Count];
                        for (int j = 0; j < _Submission.SubmissionCompanyNumberCodes.Count; j++)
                        {
                            bos.SubmissionCodeTypess[j] = new BCS.Core.Clearance.BizObjects.SubmissionCodeType();
                            bos.SubmissionCodeTypess[j].CodeName = _Submission.SubmissionCompanyNumberCodes[j].CompanyNumberCode.CompanyNumberCodeType.CodeName;
                            bos.SubmissionCodeTypess[j].CodeValue = _Submission.SubmissionCompanyNumberCodes[j].CompanyNumberCode.Code;
                            bos.SubmissionCodeTypess[j].CodeId = _Submission.SubmissionCompanyNumberCodes[j].CompanyNumberCode.Id;
                            bos.SubmissionCodeTypess[j].CodeDescription = _Submission.SubmissionCompanyNumberCodes[j].CompanyNumberCode.Description;
                            bos.SubmissionCodeTypess[j].CodeTypeId = _Submission.SubmissionCompanyNumberCodes[j].CompanyNumberCode.CompanyNumberCodeType.Id;
                        }
                    }

                    int[] ids = new int[_Submission.SubmissionCompanyNumberCodes.Count];
                    for (int k = 0; k < _Submission.SubmissionCompanyNumberCodes.Count; k++)
                    {
                        ids[k] = _Submission.SubmissionCompanyNumberCodes[k].CompanyNumberCodeId;
                        if (_Submission.SubmissionCompanyNumberCodes[k].CompanyNumberCode.CompanyNumberCodeType.CodeName == "Policy Symbol")
                        {
                            bos.SubmissionCodeTypes = _Submission.SubmissionCompanyNumberCodes[k].CompanyNumberCode.Code;
                        }
                    }

                    if (!_Submission.ClientCorePortfolioId.IsNull)
                        bos.ClientCorePortfolioId = _Submission.ClientCorePortfolioId.Value;

                    // agent
                    bos.AgentId = _Submission.AgentId.IsNull ? 0 : _Submission.AgentId.Value;
                    if (_Submission.Agent != null)
                    {
                        bos.Agent = new BCS.Core.Clearance.BizObjects.Agent();
                        bos.Agent.Active = _Submission.Agent.Active.Value;
                        bos.Agent.AddressId = _Submission.Agent.AddressId.IsNull ? 0 : _Submission.Agent.AddressId.Value;
                        bos.Agent.AgencyId = _Submission.Agent.AgencyId.Value;
                        bos.Agent.BrokerNo = _Submission.Agent.BrokerNo;
                        bos.Agent.DepartmentName = _Submission.Agent.DepartmentName;
                        bos.Agent.EntryBy = _Submission.Agent.EntryBy;
                        bos.Agent.EntryDate = _Submission.Agent.EntryDt.Value;
                        bos.Agent.FirstName = _Submission.Agent.FirstName;
                        bos.Agent.FunctionalTitle = _Submission.Agent.FunctionalTitle;
                        bos.Agent.Id = _Submission.Agent.Id;
                        bos.Agent.MailStopCode = _Submission.Agent.MailStopCode;
                        bos.Agent.Middle = _Submission.Agent.Middle;
                        bos.Agent.ModifiedBy = _Submission.Agent.ModifiedBy;
                        bos.Agent.ModifiedDt = _Submission.Agent.ModifiedDt.IsNull ? DateTime.MinValue : _Submission.Agent.ModifiedDt.Value;
                        bos.Agent.Prefix = _Submission.Agent.Prefix;
                        bos.Agent.PrimaryPhone = _Submission.Agent.PrimaryPhone;
                        bos.Agent.PrimaryPhoneAreaCode = _Submission.Agent.PrimaryPhoneAreaCode;
                        bos.Agent.ProfessionalTitle = _Submission.Agent.ProfessionalTitle;
                        bos.Agent.SecondaryPhone = _Submission.Agent.SecondaryPhone;
                        bos.Agent.SecondaryPhoneAreaCode = _Submission.Agent.SecondaryPhoneAreaCode;
                        bos.Agent.SuffixTitle = _Submission.Agent.SuffixTitle;
                        bos.Agent.Surname = _Submission.Agent.Surname;
                        bos.Agent.UserId = _Submission.Agent.UserId.IsNull ? 0 : _Submission.Agent.UserId.Value;
                    }

                    // contact
                    bos.ContactId = _Submission.ContactId.IsNull ? 0 : _Submission.ContactId.Value;
                    if (_Submission.Contact != null)
                    {
                        bos.Contact = new BCS.Core.Clearance.BizObjects.Contact();
                        bos.Contact.AgencyId = _Submission.Contact.AgencyId.Value;
                        bos.Contact.DateOfBirth = _Submission.Contact.DateOfBirth.IsNull ? DateTime.MinValue : _Submission.Contact.DateOfBirth.Value;
                        bos.Contact.Email = _Submission.Contact.Email;
                        bos.Contact.Fax = _Submission.Contact.Fax;
                        bos.Contact.Id = _Submission.Contact.Id;
                        bos.Contact.Name = _Submission.Contact.Name;
                        bos.Contact.PrimaryPhone = _Submission.Contact.PrimaryPhone;
                        bos.Contact.PrimaryPhoneExtension = _Submission.Contact.PrimaryPhoneExtension;
                        bos.Contact.SecondaryPhone = _Submission.Contact.SecondaryPhone;
                        bos.Contact.SecondaryPhoneExtension = _Submission.Contact.SecondaryPhoneExtension;
                    }

                    //agency
                    bos.AgencyId = _Submission.AgencyId.Value;
                    if (_Submission.Agency != null)
                    {
                        bos.Agency = new BCS.Core.Clearance.BizObjects.Agency();
                        bos.Agency.Active = _Submission.Agency.Active.Value;
                        bos.Agency.AgencyName = _Submission.Agency.AgencyName;
                        bos.Agency.AgencyNumber = _Submission.Agency.AgencyNumber;
                        bos.Agency.City = _Submission.Agency.City;
                        bos.Agency.CompanyNumberId = _Submission.Agency.CompanyNumberId.Value;
                        bos.Agency.EntryBy = _Submission.Agency.EntryBy;
                        bos.Agency.EntryDate = _Submission.Agency.EntryDt.Value;
                        bos.Agency.FEIN = _Submission.Agency.FEIN;
                        bos.Agency.Id = _Submission.Agency.Id;
                        bos.Agency.MasterAgencyId = _Submission.Agency.MasterAgencyId.IsNull ? 0 : _Submission.Agency.MasterAgencyId.Value;
                        bos.Agency.ModifiedBy = _Submission.Agency.ModifiedBy;
                        bos.Agency.ModifiedDt = _Submission.Agency.ModifiedDt.IsNull ? DateTime.MinValue : _Submission.Agency.ModifiedDt.Value;
                        bos.Agency.State = _Submission.Agency.State;

                        bos.AgencyName = _Submission.Agency.AgencyName == null ? string.Empty : _Submission.Agency.AgencyName;
                    }

                    bos.ClientCoreClientId = _Submission.ClientCoreClientId.Value;
                    bos.Deleted = (bool)_Submission.Deleted;
                    bos.DeletedBy = _Submission.DeletedBy;
                    bos.DeletedDate = (DateTime)_Submission.DeletedDt;


                    //if (_Submission.SubmissionClientCoreClientAddress != null)
                    //    bos.ClientCoreAddressId = _Submission.SubmissionClientCoreClientAddress.ClientCoreClientAddressId;
                    #region client core client address ids
                    List<int> ccids = new List<int>();
                    foreach (Biz.SubmissionClientCoreClientAddress var in _Submission.SubmissionClientCoreClientAddresss)
                    {
                        ccids.Add(var.ClientCoreClientAddressId);
                    }
                    bos.ClientCoreAddressIds = new int[ccids.Count];
                    ccids.CopyTo(bos.ClientCoreAddressIds);
                    // TODO: for the moment, we will have old element populated too to easily identify submissions with a address
                    // on the default.aspx page.
                    string[] strccids = Array.ConvertAll<int, string>(bos.ClientCoreAddressIds, new Converter<int, string>(Convert.ToString));
                    bos.ClientCoreAddressIdsString = string.Join(",", strccids);
                    #region TODO: stop gap for other apps to catch up
                    if (ccids.Count > 0)
                    {
                        bos.ClientCoreAddressId = ccids[0].ToString();
                    }
                    else
                    {
                        bos.ClientCoreAddressId = "0";
                    }
                    #endregion

                    #endregion
                    #region client core client name ids
                    ccids = new List<int>();
                    foreach (Biz.SubmissionClientCoreClientName var in _Submission.SubmissionClientCoreClientNames)
                    {
                        ccids.Add(var.ClientCoreNameId);
                    }
                    bos.ClientCoreNameIds = new int[ccids.Count];
                    ccids.CopyTo(bos.ClientCoreNameIds);

                    #endregion


                    #region Submission Comments

                    if (_Submission.SubmissionComments.Count > 0)
                    {

                        // bubble up and latest comment as comment for easy display in grids
                        bos.Comments = _Submission.SubmissionComments.SortByEntryDt(OrmLib.SortDirection.Descending)[0].Comment;

                        bos.SubmissionComments = new BCS.Core.Clearance.BizObjects.SubmissionComment[_Submission.SubmissionComments.Count];
                        for (int j = 0; j < _Submission.SubmissionComments.Count; j++)
                        {
                            bos.SubmissionComments[j] = new BCS.Core.Clearance.BizObjects.SubmissionComment();

                            bos.SubmissionComments[j].Comment = _Submission.SubmissionComments[j].Comment;
                            bos.SubmissionComments[j].EntryBy = _Submission.SubmissionComments[j].EntryBy;
                            bos.SubmissionComments[j].EntryDt = _Submission.SubmissionComments[j].EntryDt.Value;
                            bos.SubmissionComments[j].Id = _Submission.SubmissionComments[j].Id;
                            bos.SubmissionComments[j].ModifiedBy = _Submission.SubmissionComments[j].ModifiedBy;
                            bos.SubmissionComments[j].ModifiedDt =
                                _Submission.SubmissionComments[j].ModifiedDt.IsNull ?
                                DateTime.MinValue : _Submission.SubmissionComments[j].ModifiedDt.Value;
                            bos.SubmissionComments[j].SubmissionId = _Submission.SubmissionComments[j].SubmissionId.Value;
                        }
                    }
                    else
                    {
                        bos.SubmissionComments = new BCS.Core.Clearance.BizObjects.SubmissionComment[0];
                    }
                    #endregion
                    bos.CompanyNumberId = _Submission.CompanyNumberId.Value;
                    if (_Submission.CompanyNumber != null)
                        bos.CompanyId = _Submission.CompanyNumber.CompanyId.Value;
                    bos.EffectiveDate = _Submission.EffectiveDate.IsNull ? DateTime.MinValue : _Submission.EffectiveDate.Value;
                    bos.EstimatedWrittenPremium = _Submission.EstimatedWrittenPremium.IsNull ? decimal.Zero : _Submission.EstimatedWrittenPremium.Value;
                    bos.OtherCarrierPremium = _Submission.OtherCarrierPremium.IsNull ? decimal.Zero :
                        _Submission.OtherCarrierPremium.Value;
                    bos.ExpirationDate = _Submission.ExpirationDate.IsNull ? DateTime.MinValue : _Submission.ExpirationDate.Value;
                    bos.Id = _Submission.Id;
                    bos.PolicyNumber = string.IsNullOrEmpty(_Submission.PolicyNumber) ? string.Empty : _Submission.PolicyNumber;
                    if (_Submission.PolicySystem != null)
                    {
                        bos.PolicySystem = new BCS.Core.Clearance.BizObjects.PolicySystem();
                        bos.PolicySystem.Abbreviation = _Submission.PolicySystem.Abbreviation;
                        bos.PolicySystem.AssociatedCompanyNumberId = _Submission.PolicySystem.AssociatedCompanyNumberId.Value;
                        bos.PolicySystem.Id = _Submission.PolicySystem.Id;
                        bos.PolicySystem.PropertyPolicySystem = _Submission.PolicySystem.PropertyPolicySystem;
                    }
                    bos.SubmissionBy = _Submission.SubmissionBy;
                    bos.SubmissionDt = _Submission.SubmissionDt.IsNull ? DateTime.MinValue : _Submission.SubmissionDt.Value;
                    bos.SubmissionNumber = _Submission.SubmissionNumber.Value;
                    bos.InsuredName = _Submission.InsuredName;
                    bos.Dba = _Submission.Dba;

                    // other carrier
                    if (!_Submission.OtherCarrierId.IsNull)
                        bos.OtherCarrierId = _Submission.OtherCarrierId.Value;
                    if (_Submission.OtherCarrier != null)
                    {
                        bos.OtherCarrier = new BCS.Core.Clearance.BizObjects.OtherCarrier();
                        bos.OtherCarrier.Id = _Submission.OtherCarrier.Id;
                        bos.OtherCarrier.Code = _Submission.OtherCarrier.Code;
                        bos.OtherCarrier.Description = _Submission.OtherCarrier.Description;
                    }

                    // status
                    if (!_Submission.SubmissionStatusId.IsNull)
                        bos.SubmissionStatusId = _Submission.SubmissionStatusId.Value;
                    if (_Submission.SubmissionStatus != null)
                    {
                        bos.SubmissionStatus = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
                        bos.SubmissionStatus.Id = _Submission.SubmissionStatus.Id;
                        bos.SubmissionStatus.StatusCode = _Submission.SubmissionStatus.StatusCode;
                    }
                    // status reason
                    if (!_Submission.SubmissionStatusReasonId.IsNull)
                    {
                        bos.SubmissionStatusReasonId = _Submission.SubmissionStatusReasonId.Value;
                        bos.SubmissionStatusReason = _Submission.SubmissionStatusReason.ReasonCode;
                    }

                    bos.SubmissionStatusDate = _Submission.SubmissionStatusDate.IsNull ? DateTime.MinValue : _Submission.SubmissionStatusDate.Value;
                    bos.SubmissionStatusReasonOther = _Submission.SubmissionStatusReasonOther;

                    if (!_Submission.SubmissionTokenId.IsNull)
                        bos.SubmissionTokenId = _Submission.SubmissionTokenId.Value;

                    // type
                    bos.SubmissionTypeId = _Submission.SubmissionTypeId.Value;
                    if (_Submission.SubmissionType != null)
                    {
                        bos.SubmissionType = new BCS.Core.Clearance.BizObjects.SubmissionType();
                        bos.SubmissionType.Abbreviation = _Submission.SubmissionType.Abbreviation;
                        bos.SubmissionType.Id = _Submission.SubmissionType.Id;
                        bos.SubmissionType.TypeName = _Submission.SubmissionType.TypeName;
                    }
                    bos.UnderwriterInitials = _Submission.Person_by_UnderwriterPersonId == null ? string.Empty : _Submission.Person_by_UnderwriterPersonId.Initials;
                    bos.UnderwriterId = _Submission.UnderwriterPersonId.IsNull ? 0 : _Submission.UnderwriterPersonId.Value;
                    bos.UnderwriterName = _Submission.Person_by_UnderwriterPersonId == null ? string.Empty : _Submission.Person_by_UnderwriterPersonId.UserName;
                    bos.UnderwriterFullName = _Submission.Person_by_UnderwriterPersonId == null ? string.Empty : _Submission.Person_by_UnderwriterPersonId.FullName;
                    bos.AnalystId = _Submission.AnalystPersonId.IsNull ? 0 : _Submission.AnalystPersonId.Value;
                    bos.AnalystName = _Submission.Person_by_AnalystPersonId == null ? string.Empty : _Submission.Person_by_AnalystPersonId.UserName;
                    bos.AnalystFullName = _Submission.Person_by_AnalystPersonId == null ? string.Empty : _Submission.Person_by_AnalystPersonId.FullName; bos.AnalystId = _Submission.AnalystPersonId.IsNull ? 0 : _Submission.AnalystPersonId.Value;
                    bos.AnalystName = _Submission.Person_by_AnalystPersonId == null ? string.Empty : _Submission.Person_by_AnalystPersonId.UserName;
                    bos.AnalystFullName = _Submission.Person_by_AnalystPersonId == null ? string.Empty : _Submission.Person_by_AnalystPersonId.FullName;
                    bos.TechnicianId = _Submission.TechnicianPersonId.IsNull ? 0 : _Submission.TechnicianPersonId.Value;
                    bos.TechnicianName = _Submission.Person_by_TechnicianPersonId == null ? string.Empty : _Submission.Person_by_TechnicianPersonId.UserName;
                    bos.TechnicianFullName = _Submission.Person_by_TechnicianPersonId == null ? string.Empty : _Submission.Person_by_TechnicianPersonId.FullName;
                    bos.PolicySystemId = _Submission.PolicySystemId.IsNull ? 0 : _Submission.PolicySystemId.Value;
                    bos.UpdatedBy = _Submission.UpdatedBy;
                    bos.UpdatedDt = _Submission.UpdatedDt.IsNull ? DateTime.MinValue : _Submission.UpdatedDt.Value;
                    bos.CancellationDate = _Submission.CancellationDate.IsNull ? DateTime.MinValue : _Submission.CancellationDate.Value;

                    #region previous underwriter
                    SubmissionHistoryCollection shc = _Submission.SubmissionHistorys;
                    shc = shc.FilterByUnderwriterPersonId(0, OrmLib.CompareType.Not);
                    shc = shc.FilterByUnderwriterPersonId(SqlInt32.Null, OrmLib.CompareType.Not);
                    shc = shc.FilterByUnderwriterPersonId(bos.UnderwriterId, OrmLib.CompareType.Not);
                    shc = shc.SortByHistoryDt(OrmLib.SortDirection.Descending);
                    if (shc.Count > 0)
                    {
                        bos.PreviousUnderwriterId = shc[0].UnderwriterPersonId.Value;
                        DataManager dm = Common.GetDataManager();
                        dm.QueryCriteria.And(JoinPath.Person.Columns.Id, bos.PreviousUnderwriterId);
                        Person previousPerson = dm.GetPerson();
                        // person can be deleted, so if exists
                        if (null != previousPerson)
                        {
                            bos.PreviousUnderwriterName = previousPerson.UserName;
                            bos.PreviousUnderwriterFullName = previousPerson.FullName;
                        }

                    }
                    #endregion

                    #region SubmissionLineOfBusinessChildren

                    if (_Submission.SubmissionLineOfBusinessChildrens.Count > 0)
                    {
                        bos.LineOfBusinessChildList = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild[_Submission.SubmissionLineOfBusinessChildrens.Count];
                        for (int j = 0; j < _Submission.SubmissionLineOfBusinessChildrens.Count; j++)
                        {
                            bos.LineOfBusinessChildList[j] = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild();

                            bos.LineOfBusinessChildList[j].Id = _Submission.SubmissionLineOfBusinessChildrens[j].Id;
                            bos.LineOfBusinessChildList[j].CompanyNumberLOBGridId = _Submission.SubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGridId.Value;
                            bos.LineOfBusinessChildList[j].SubmissionStatusId =
                                _Submission.SubmissionLineOfBusinessChildrens[j].SubmissionStatusId.IsNull ?
                                0 : _Submission.SubmissionLineOfBusinessChildrens[j].SubmissionStatusId.Value;
                            bos.LineOfBusinessChildList[j].SubmissionStatusReasonId =
                                _Submission.SubmissionLineOfBusinessChildrens[j].SubmissionStatusReasonId.IsNull ?
                                0 : _Submission.SubmissionLineOfBusinessChildrens[j].SubmissionStatusReasonId.Value;

                            if (_Submission.SubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGrid != null)
                            {
                                bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid = new BCS.Core.Clearance.BizObjects.CompanyNumberLOBGrid();
                                bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Code = _Submission.SubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGrid.MulitLOBCode;
                                bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.CompanyNumberId = _Submission.SubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGrid.CompanyNumberId.Value;
                                bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Description = _Submission.SubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGrid.MultiLOBDesc;
                                bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Id = _Submission.SubmissionLineOfBusinessChildrens[j].CompanyNumberLOBGrid.Id;
                            }

                            if (_Submission.SubmissionLineOfBusinessChildrens[j].SubmissionStatus != null)
                            {
                                bos.LineOfBusinessChildList[j].SubmissionStatus = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
                                bos.LineOfBusinessChildList[j].SubmissionStatus.Id = _Submission.SubmissionLineOfBusinessChildrens[j].SubmissionStatus.Id;
                                bos.LineOfBusinessChildList[j].SubmissionStatus.StatusCode = _Submission.SubmissionLineOfBusinessChildrens[j].SubmissionStatus.StatusCode;
                            }

                            if (_Submission.SubmissionLineOfBusinessChildrens[j].SubmissionStatusReason != null)
                            {
                                bos.LineOfBusinessChildList[j].SubmissionStatusReason = new BCS.Core.Clearance.BizObjects.SubmissionStatusReason();
                                bos.LineOfBusinessChildList[j].SubmissionStatusReason.Id = _Submission.SubmissionLineOfBusinessChildrens[j].SubmissionStatusReason.Id;
                                bos.LineOfBusinessChildList[j].SubmissionStatusReason.ReasonCode = _Submission.SubmissionLineOfBusinessChildrens[j].SubmissionStatusReason.ReasonCode;
                            }
                        }
                        #region first lob child
                        int index0 = 0;
                        {
                            bos.FirstSubmissionLineOfBusinessChild = new BCS.Core.Clearance.BizObjects.FirstSubmissionLineOfBusinessChild();
                            bos.FirstSubmissionLineOfBusinessChild.Id = _Submission.SubmissionLineOfBusinessChildrens[index0].Id;
                            bos.FirstSubmissionLineOfBusinessChild.LobId = _Submission.SubmissionLineOfBusinessChildrens[index0].CompanyNumberLOBGridId.Value;

                            if (_Submission.SubmissionLineOfBusinessChildrens[index0].CompanyNumberLOBGrid != null)
                            {
                                bos.FirstSubmissionLineOfBusinessChild.LobCode = _Submission.SubmissionLineOfBusinessChildrens[index0].CompanyNumberLOBGrid.MulitLOBCode;
                                bos.FirstSubmissionLineOfBusinessChild.LobDescription = _Submission.SubmissionLineOfBusinessChildrens[index0].CompanyNumberLOBGrid.MultiLOBDesc;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        bos.LineOfBusinessChildList = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild[0];
                    }
                    #endregion

                    al.Add(bos);
                    i++;
                    if (i > max)
                        break;
                    Match = properties.Length == 0;
                }
            }

            if (pageSize > 0 && al.Count > 0)
            {
                int theSize = 0;
                if (pageNo > 1)
                    theSize = al.Count - min - 1;
                else
                    theSize = al.Count <= pageSize ? al.Count : pageSize;
                if (i <= max && theSize != al.Count)
                    theSize++;
                if (theSize < 0)
                {
                    list.PageNo = pageNo;
                    return list;
                }

                list.List = new BCS.Core.Clearance.BizObjects.Submission[theSize];
                al.CopyTo(min, list.List, 0, list.List.Length);
                list.HasNextPage = i > max;
                list.PageNo = pageNo;
                list.PageSize = list.List.Length;
            }
            else
            {
                //list.List = new BCS.Core.Clearance.BizObjects.Submission[al.Count];
                //al.CopyTo(list.List);
                list.List = al.ToArray();
                list.HasNextPage = i > max;
                list.PageNo = 1;
                list.PageSize = al.Count;
            }
            return list;
        }


        private static string BuildEmptySubmissionList()
        {
            BizObjects.SubmissionList list = new BCS.Core.Clearance.BizObjects.SubmissionList();
            string xml = SerializeSubmissionList(list);
            return xml;
        }

        private static bool MatchAnd(Biz.Submission submission, OrmLib.CompareType[] compareTypes, string[] properties, object[] values)
        {
            if (properties.Length != values.Length)
                throw new ArgumentException("Properties, Filters and Filter types must be of equal length");

            if (compareTypes == null)
            {
                compareTypes = new OrmLib.CompareType[properties.Length];
            }


            for (int i = 0; i < compareTypes.Length; i++)
            {
                if (!Match(submission, compareTypes[i], properties[i], values[i]))
                    return false;
            }

            return true;
        }
        private static bool MatchOr(Biz.Submission submission, OrmLib.CompareType[] compareTypes, string[] properties, object[] values)
        {
            if (properties.Length != values.Length)
                throw new ArgumentException("Properties, Filters and Filter types must be of equal length");

            if (compareTypes == null)
            {
                compareTypes = new OrmLib.CompareType[properties.Length];
            }
            for (int i = 0; i < compareTypes.Length; i++)
            {
                if (Match(submission, compareTypes[i], properties[i], values[i]))
                    return true;
            }
            return false;
        }
        private static bool Match(BCS.Biz.Submission submission, OrmLib.CompareType compareType, string property, object value)
        {
            try
            {
                switch (compareType)
                {
                    case OrmLib.CompareType.Contains:
                        {
                            if (submission[property] is Nullable)
                            {
                                if ((submission[property] as INullable).IsNull)
                                {
                                    if (value == null)
                                        return true;
                                    else
                                        return false;
                                }
                            }
                            if (submission[property] == null)
                            {
                                if (value == null)
                                    return true;
                                else
                                    return false;
                            }
                            else
                            {
                                if (value == null)
                                    return false;
                            }
                            string a = submission[property].ToString();
                            string b = value.ToString();
                            return a.IndexOf(b, StringComparison.CurrentCultureIgnoreCase) >= 0;
                        }
                    case OrmLib.CompareType.EndsWith:
                        {
                            if (submission[property] is Nullable)
                            {
                                if ((submission[property] as INullable).IsNull)
                                {
                                    if (value == null)
                                        return true;
                                    else
                                        return false;
                                }
                            }
                            if (submission[property] == null)
                            {
                                if (value == null)
                                    return true;
                                else
                                    return false;
                            }
                            else
                            {
                                if (value == null)
                                    return false;
                            }
                            string a = submission[property].ToString();
                            string b = value.ToString();
                            return a.EndsWith(b, StringComparison.CurrentCultureIgnoreCase);
                        }
                    case OrmLib.CompareType.Exact:
                        {
                            if (submission[property] is Nullable)
                            {
                                if ((submission[property] as INullable).IsNull)
                                {
                                    if (value == null)
                                        return true;
                                    else
                                        return false;
                                }
                            }
                            if (submission[property] == null)
                            {
                                if (value == null)
                                    return true;
                                else
                                    return false;
                            }
                            else
                            {
                                if (value == null)
                                    return false;
                            }
                            int i = CompareNotNulls(submission, property, value);
                            return i == 0;
                        }
                    case OrmLib.CompareType.Greater:
                        {
                            if (submission[property] is Nullable)
                            {
                                if ((submission[property] as INullable).IsNull)
                                {
                                    return false;
                                }
                            }
                            if (submission[property] == null || value == null)
                                return false;
                            int i = CompareNotNulls(submission, property, value);
                            return i > 0;
                        }
                    case OrmLib.CompareType.GreaterOrEqual:
                        {
                            if (submission[property] is Nullable)
                            {
                                if ((submission[property] as INullable).IsNull)
                                {
                                    if (value == null)
                                        return true;
                                    else
                                        return false;
                                }
                            }
                            if (submission[property] == null)
                            {
                                if (value == null)
                                    return true;
                                else
                                    return false;
                            }
                            else
                            {
                                if (value == null)
                                    return false;
                            }
                            int i = CompareNotNulls(submission, property, value);
                            return i >= 0;
                        }
                    case OrmLib.CompareType.Lesser:
                        {
                            if (submission[property] is Nullable)
                            {
                                if ((submission[property] as INullable).IsNull)
                                {
                                    return false;
                                }
                            }
                            if (submission[property] == null || value == null)
                                return false;
                            int i = CompareNotNulls(submission, property, value);
                            return i < 0;
                        }
                    case OrmLib.CompareType.LesserOrEqual:
                        {
                            if (submission[property] is Nullable)
                            {
                                if ((submission[property] as INullable).IsNull)
                                {
                                    if (value == null)
                                        return true;
                                    else
                                        return false;
                                }
                            }
                            if (submission[property] == null)
                            {
                                if (value == null)
                                    return true;
                                else
                                    return false;
                            }
                            else
                            {
                                if (value == null)
                                    return false;
                            }
                            int i = CompareNotNulls(submission, property, value);
                            return i <= 0;
                        }
                    case OrmLib.CompareType.Not:
                        {
                            if (submission[property] is Nullable)
                            {
                                if ((submission[property] as INullable).IsNull)
                                {
                                    if (value == null)
                                        return true;
                                    else
                                        return false;
                                }
                            }
                            if (submission[property] == null)
                            {
                                if (value == null)
                                    return false;
                                else
                                    return true;
                            }
                            else
                            {
                                if (value == null)
                                    return true;
                            }
                            int i = CompareNotNulls(submission, property, value);
                            return i != 0;
                        }
                    case OrmLib.CompareType.NotContain:
                        {
                            if (submission[property] is Nullable)
                            {
                                if ((submission[property] as INullable).IsNull)
                                {
                                    return false;
                                }
                            }
                            if (submission[property] == null || value == null)
                                return false;
                            string a = submission[property].ToString();
                            string b = value.ToString();
                            return a.IndexOf(b, StringComparison.CurrentCultureIgnoreCase) < 0;
                        }
                    case OrmLib.CompareType.StartsWith:
                        {
                            if (submission[property] is Nullable)
                            {
                                if ((submission[property] as INullable).IsNull)
                                {
                                    if (value == null)
                                        return true;
                                    else
                                        return false;
                                }
                            }
                            if (submission[property] == null)
                            {
                                if (value == null)
                                    return true;
                                else
                                    return false;
                            }
                            else
                            {
                                if (value == null)
                                    return false;
                            }
                            string a = submission[property].ToString();
                            string b = value.ToString();
                            return a.StartsWith(b, StringComparison.CurrentCultureIgnoreCase);
                        }
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }

        private static int CompareNotNulls(BCS.Biz.Submission submission, string property, object value)
        {
            System.Reflection.PropertyInfo pai = typeof(BCS.Biz.Submission).GetProperty(property);
            Type t = pai.PropertyType;
            IComparable o = (IComparable)Convert.ChangeType(submission[property], t);

            Type vt = value.GetType();
            if (vt == t)
            { }
            else
            {
                ConstructorInfo ci = t.GetConstructor(new Type[] { vt });
                if (ci == null)
                {
                    throw new ArgumentException(string.Format("Invalid cast from {0} to {1}", vt.FullName, t.FullName));
                }

                value = ci.Invoke(new object[] { value });
            }
            IComparable v = (IComparable)Convert.ChangeType(value, t);
            int i = o.CompareTo(v);
            return i;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns></returns>
        public static Biz.SubmissionStatusHistoryCollection GetSubmissionStatusHistory(int submissionId)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatusHistory.Columns.SubmissionId, submissionId);
            Biz.SubmissionStatusHistoryCollection coll = dm.GetSubmissionStatusHistoryCollection(Biz.FetchPath.SubmissionStatusHistory.SubmissionStatus);
            if (coll != null)
                coll = coll.SortByStatusChangeDt(OrmLib.SortDirection.Descending);
            return coll;
        }

        public static string ToggleSubmissionStatusHistoryActive(int statusId, string user)
        {
            int d = 0;
            return ToggleSubmissionStatusHistoryActive(statusId, user, ref d);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statusId">Id of the status to be toggled.</param>
        /// <param name="user">updating user.</param>
        /// <param name="newSubmissionStatusId">The Id of the status the submission was updated to.</param>
        /// <returns></returns>
        public static string ToggleSubmissionStatusHistoryActive(int statusId, string user, ref int newSubmissionStatusId)
        {
            DateTime changeDate = DateTime.Now;
            DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(JoinPath.Submission.SubmissionStatusHistory.Columns.Id, statusId);
            Submission submission = dm.GetSubmission(FetchPath.Submission.SubmissionStatusHistory);
            if (submission == null)
                return "Invalid history Id provided.";
            SubmissionStatusHistory history = submission.SubmissionStatusHistorys.FindById(statusId);
            if (history == null)
                return "Invalid history Id provided.";

            bool deactivating = history.Active.IsTrue;
            newSubmissionStatusId = submission.SubmissionStatusId.IsNull ? 0 : submission.SubmissionStatusId.Value;

            // update the history
            history.Active = !history.Active;
            history.StatusChangeDt = changeDate;
            history.StatusChangeBy = user;

            if (!history.PreviousSubmissionStatusId.IsNull)
            {
                // we only update the submission status, when it is the same status that is getting de-activated on the history
                if (deactivating && submission.SubmissionStatusId == history.PreviousSubmissionStatusId)
                {
                    SubmissionStatusHistoryCollection histories = submission.SubmissionStatusHistorys.FilterById(statusId, OrmLib.CompareType.Not);
                    histories = histories.FilterByActive(true);
                    histories = histories.SortByStatusChangeDt(OrmLib.SortDirection.Descending);
                    if (histories != null && histories.Count > 0)
                    {
                        submission.UpdatedBy = user;
                        submission.UpdatedDt = changeDate;
                        submission.SubmissionStatusId = histories[0].PreviousSubmissionStatusId;
                        newSubmissionStatusId = histories[0].PreviousSubmissionStatusId.Value;
                        submission.SubmissionStatusDate = histories[0].SubmissionStatusDate;
                    }
                }
            }
            try
            {
                dm.CommitAll();
                // remove the cache dependency used to cache submissions for page
                System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
            }

            catch (Exception)
            {
                return "There was a problem updating the status history.";
            }
            return string.Format("Status flag successfully {0}.", history.Active ? "activated" : "de-activated");
        }

        /// <summary>
        /// updates submissions with the underwriter or analyst specified by the rolecolumn paramter
        /// </summary>
        /// <param name="submissionIds"></param>
        /// <param name="personid"></param>
        /// <param name="rolecolumn"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string UpdatePersonAssignments(string[] submissionIds, int personid, string rolecolumn, string user)
        {
            Type t = typeof(Biz.Submission);
            System.Reflection.PropertyInfo pi = t.GetProperty(rolecolumn);

            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, submissionIds, OrmLib.MatchType.In);
            Biz.SubmissionCollection ss = dm.GetSubmissionCollection();
            foreach (Biz.Submission var in ss)
            {
                pi.SetValue(var, Common.ConvertToSqlInt32(personid), null);

                var.UpdatedBy = user;
                var.UpdatedDt = Common.ConvertToSqlDateTime(DateTime.Now);
            }

            try
            {
                dm.CommitAll();
                System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
            }
            catch (Exception ex)
            {
                BTS.LogFramework.LogCentral.Current.LogFatal(ex.Message, ex);
                return "There was a problem updating the submissions.";
            }
            return "Submissions successfully updated.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="criteria">first - OrmLib.DataManagerBase.JoinPathRelation, second - data, third - OrmLib.MatchType </param>
        /// <returns></returns>
        private static Biz.SubmissionCollection SearchSubmissions(string companyNumber, bool includeDeleted, params System.Web.UI.Triplet[] criteria)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.CommandTimeout = Config.Timeout;
            dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);

            //Filter deleted submissions.
            if (!includeDeleted)
            {
                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Deleted, false);
            }

            foreach (System.Web.UI.Triplet var in criteria)
            {
                dm.QueryCriteria.And((OrmLib.DataManagerBase.JoinPathRelation)var.First, var.Second,
                    var.Third == null ? OrmLib.MatchType.Exact : (OrmLib.MatchType)var.Third);
            }



            Biz.SubmissionCollection submissions = dm.GetSubmissionCollection(SubmissionFetchPath());

            return submissions;

        }

        public static string GetSubmissionsByPolicyAndAttribute(string companyNumber, string policyNumber, string attributeLabel, string attributeValue)
        {
            return GetSubmissionsByPolicyAndAttribute(companyNumber, policyNumber, attributeLabel, attributeValue, true);
        }
        public static string GetSubmissionsByPolicyAndAttribute(string companyNumber, string policyNumber, string attributeLabel, string attributeValue, bool includeDeleted)
        {
            if (string.IsNullOrEmpty(companyNumber) || string.IsNullOrEmpty(policyNumber) || string.IsNullOrEmpty(attributeLabel) || string.IsNullOrEmpty(attributeValue))
                return BuildEmptySubmissionList();

            System.Web.UI.Triplet polTriplet = new System.Web.UI.Triplet(Biz.JoinPath.Submission.Columns.PolicyNumber,
                policyNumber, OrmLib.MatchType.Exact);
            System.Web.UI.Triplet alTriplet = new System.Web.UI.Triplet(Biz.JoinPath.Submission.SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute.Columns.Label,
                attributeLabel, OrmLib.MatchType.Exact);
            System.Web.UI.Triplet avTriplet = new System.Web.UI.Triplet(Biz.JoinPath.Submission.SubmissionCompanyNumberAttributeValue.Columns.AttributeValue,
                attributeValue, OrmLib.MatchType.Partial);

            Biz.SubmissionCollection submissions = SearchSubmissions(companyNumber, includeDeleted, polTriplet, alTriplet, avTriplet);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, 0, 0);

            return SerializeSubmissionList(sl);
        }
        public string UpdateClientId(string companyNumber, string oldClientId, string newClientId, string insuredName, string dba, string user, bool isSearchScreen)
        {
            DataManager dm1 = Common.GetDataManager();
            dm1.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Company company = dm1.GetCompany();
            if (null == companyNumber)
                return "Invalid company number.";
            //if (company.UsesAPS)
            //{
            //    return new APS.Submissions().UpdateClientId(companyNumber, oldClientId, newClientId, insuredName, dba, user);
            //}

            SqlTransaction transaction = null;

            DataManager dm = Common.GetDataManager(out transaction);
            dm.QueryCriteria.And(JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(JoinPath.Submission.Columns.ClientCoreClientId, oldClientId);

            // get submissions
            SubmissionCollection submissions = dm.GetSubmissionCollection(
                FetchPath.Submission.CompanyNumber,
                FetchPath.Submission.SubmissionClientCoreClientAddress,
                FetchPath.Submission.SubmissionClientCoreClientName);
            if (null == submissions || 0 == submissions.Count)
                return string.Format("No submissions were found for the client {0}.", oldClientId);

            // get the new client
            BizObjects.ClearanceClient client = Clients.GetClientByIdAsObject(companyNumber, newClientId.ToString(), true, isSearchScreen);

            foreach (Submission submission in submissions)
            {
                #region delete old address assocs
                int addressCount = submission.SubmissionClientCoreClientAddresss.Count;

                for (int i = 0; i < addressCount; i++)
                {
                    submission.SubmissionClientCoreClientAddresss[(addressCount - i) - 1].Delete();
                }
                #endregion
                #region delete old name assocs
                int nameCount = submission.SubmissionClientCoreClientNames.Count;

                for (int i = 0; i < nameCount; i++)
                {
                    submission.SubmissionClientCoreClientNames[(nameCount - i) - 1].Delete();
                }
                #endregion
            }
            dm.CommitAll();

            foreach (Submission submission in submissions)
            {
                #region Insured Name
                // update only if something is passed
                if (Common.IsNotNullOrEmpty(insuredName))
                {
                    submission.InsuredName = insuredName;
                }
                #endregion
                #region DBA
                // update only if something is passed
                if (Common.IsNotNullOrEmpty(dba))
                {
                    submission.Dba = dba;
                }
                #endregion
                if (null != client.ClientCoreClient.Addresses)
                {
                    foreach (BTS.ClientCore.Wrapper.Structures.Info.Address var in client.ClientCoreClient.Addresses)
                    {
                        SubmissionClientCoreClientAddress newAddr = submission.NewSubmissionClientCoreClientAddress();
                        newAddr.ClientCoreClientAddressId = Convert.ToInt32(var.AddressId);
                    }
                }
                if (null != client.ClientCoreClient.Client.Names)
                {
                    foreach (BTS.ClientCore.Wrapper.Structures.Info.Name var in client.ClientCoreClient.Client.Names)
                    {
                        SubmissionClientCoreClientName newAddr = submission.NewSubmissionClientCoreClientName();
                        newAddr.ClientCoreNameId = Convert.ToInt32(var.SequenceNumber);
                    }
                }
                submission.ClientCoreClientId = Convert.ToInt64(client.ClientCoreClient.Client.ClientId);
                submission.ClientCorePortfolioId = Convert.ToInt64(client.ClientCoreClient.Client.PortfolioId);
                submission.UpdatedBy = user;
                submission.UpdatedDt = DateTime.Now;
            }

            try
            {
                dm.CommitAll();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                LogCentral.Current.LogFatal(
                    string.Format(
                    "There was a problem reassigning the client id for " +
                    "company number : '{0}', fromClientId : '{1}', toClientId : '{2}', insuredName : '{3}',  dba : '{4}'",
                    companyNumber, oldClientId, newClientId, insuredName, dba), ex);
                return "There was a problem reassigning the client.";
            }

            // assumed success if reached here
            return "Submissions were successfully reassigned.";
        }

        public static void MarkSubmissionAsDeleted(int submissionId)
        {
            try
            {
                Biz.DataManager dm = Common.GetDataManager();
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, submissionId);

                Biz.Submission sub = dm.GetSubmission();

                if (sub == null)
                    return;

                sub.DeletedDt = DateTime.Now;
                sub.DeletedBy = System.Web.HttpContext.Current.User.Identity.Name;
                sub.Deleted = true;

                dm.CommitAll();
            }
            catch (Exception ex)
            {

            }
        }


    }



    internal class AgencyPortSubmissions
    {
        internal static OrmLib.DataManagerBase.FetchPathRelation[] SubmissionFetchPath = new OrmLib.DataManagerBase.FetchPathRelation[] { 
            Biz.FetchPath.Submission.CompanyNumber,
            Biz.FetchPath.Submission.Agency,
        };

        internal static Biz.SubmissionCollection SearchSubmissions(bool useCache, string companyNumber, params System.Web.UI.Triplet[] criteria)
        {
            // TODO: where should the value come from to cache or not.
            if (!useCache)
            {
                Biz.DataManager dm = Common.GetDataManager();
                dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                foreach (System.Web.UI.Triplet var in criteria)
                {
                    if (var.Third == null)
                        var.Third = OrmLib.MatchType.Exact;
                    dm.QueryCriteria.And((OrmLib.DataManagerBase.JoinPathRelation)var.First, var.Second, (OrmLib.MatchType)var.Third);
                }
                return dm.GetSubmissionCollection(SubmissionFetchPath);
            }

            string dependency = companyNumber;
            foreach (System.Web.UI.Triplet triplet in criteria)
            {
                dependency += string.Format("{0}{1}{2}", triplet.First.ToString(), triplet.Second.ToString(), triplet.Third.ToString());
            }

            if (System.Web.HttpContext.Current.Cache["SearchedCacheDependency"] != null)
            {
                if (System.Web.HttpContext.Current.Cache["SearchedCacheDependency"].ToString() != dependency)
                { System.Web.HttpContext.Current.Cache["SearchedCacheDependency"] = dependency; }
            }
            else
            { System.Web.HttpContext.Current.Cache["SearchedCacheDependency"] = dependency; }

            Biz.SubmissionCollection submissions;
            if (System.Web.HttpContext.Current.Cache["SearchedSubmissions"] == null)
            {
                Biz.DataManager dm = Common.GetDataManager();
                dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                foreach (System.Web.UI.Triplet var in criteria)
                {
                    dm.QueryCriteria.And((OrmLib.DataManagerBase.JoinPathRelation)var.First, var.Second, (OrmLib.MatchType)var.Third);
                }
                submissions = dm.GetSubmissionCollection(SubmissionFetchPath);

                onRemove = new System.Web.Caching.CacheItemRemovedCallback(RemovedCallback);
                System.Web.HttpContext.Current.Cache.Insert("SearchedSubmissions", submissions,
                    new System.Web.Caching.CacheDependency(null, new string[] { "SearchedCacheDependency" }),
                    System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 20, 0),
                    System.Web.Caching.CacheItemPriority.NotRemovable, onRemove);

                return submissions;
            }
            else
            {
                submissions = System.Web.HttpContext.Current.Cache["SearchedSubmissions"] as Biz.SubmissionCollection;
            }

            return submissions;
        }


        static bool itemRemoved = false;
        static System.Web.Caching.CacheItemRemovedReason reason;
        static System.Web.Caching.CacheItemRemovedCallback onRemove = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="k"></param>
        /// <param name="v"></param>
        /// <param name="r"></param>
        public static void RemovedCallback(String k, Object v, System.Web.Caching.CacheItemRemovedReason r)
        {
            itemRemoved = true;
            reason = r;
        }
    }
}
