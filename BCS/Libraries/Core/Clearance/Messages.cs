using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.Core.Clearance
{
    /// <summary>
    /// class used as a message center
    /// </summary>
    public static class Messages
    {
        /// <summary>
        /// 
        /// </summary>
        public static string SuccessfulEditUser = "User was successfully updated.";

        /// <summary>
        /// 
        /// </summary>
        public static string SuccessfulAddUser = "User was successfully added.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulAddSubmission = "Unable to add a submission.";

        /// <summary>
        /// Message for successful reassign.
        /// </summary>
        public static string SuccessfulReSequenceSubmission = "Submission was successfully re-sequenced.";

        /// <summary>
        /// 
        /// </summary>
        public static string SuccessfulAddSubmission = "Submission successfully Added.";        
        /// <summary>
        /// 
        /// </summary>
        public static string SuccessfulEditSubmission = "Submission successfully Edited.";
        
        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulSubmissionToken = "Unable to add a submission token.";

        /// <summary>
        /// message for non existent Agent
        /// </summary>
        public static string NoSuchAgent = "Agent specified is not found.";

        /// <summary>
        /// message for non existent Agent
        /// </summary>
        public static string NoSuchContact = "Contact specified is not found.";

        /// <summary>
        /// message for non existent Agency
        /// </summary>
        public static string NoSuchAgency = "Agency specified is not found.";

        /// <summary>
        /// message for non existent Agency
        /// </summary>
        public static string NoSuchCodeName = "Company number code name specified is not found.";

        /// <summary>
        /// message for non existent Company
        /// </summary>
        public static string NoSuchCompany = "Company specified is not found.";

        /// <summary>
        /// message for non existent Company Number
        /// </summary>
        public static string NoSuchCompanyNumber = "Company Number specified is not found.";

        /// <summary>
        ///  message for non existent Submission Status
        /// </summary>
        public static string NoSuchSubmissionStatus = "Submission Status specified is not found.";

        /// <summary>
        ///  message for non existent Submission Status
        /// </summary>
        public static string NoSuchSubmissionStatusReason = "Submission Status Reason specified is not found.";

        /// <summary>
        ///  message for non existent Submission Token
        /// </summary>
        public static string NoSuchSubmissionToken = "Submission Token specified is not found.";

        /// <summary>
        ///  message for non existent Submission Type
        /// </summary>
        public static string NoSuchSubmissionType = "Submission Type specified is not found.";

        /// <summary>
        ///  message for non existent Underwriter
        /// </summary>
        public static string NoSuchUnderwriter = "Underwriter person was not found for the agency, line of business and company number specified.";

        /// <summary>
        ///  message for non existent Analyst
        /// </summary>
        public static string NoSuchAnalyst = "Analyst person was not found for the agency, line of business and company number specified.";

        /// <summary>
        ///  message for non existent Analyst
        /// </summary>
        public static string NoSuchTechnician = "Technician person was not found for the agency, line of business and company number specified.";
        
        /// <summary>
        ///  message for non existent PolicySystem
        /// </summary>
        public static string NoSuchPolicySystem = "Policy System was not found for the selected company, line of business and company number specified.";
                
        /// <summary>
        ///  message for non existent Company Number Code
        /// </summary>
        public static string NoSuchCompanyNumberCode = "Company Number Code was not found.";

        /// <summary>
        /// message when multiple clients found
        /// </summary>
        public static string MultipleClientsFound = "Multiple clients were found.";

        /// <summary>
        /// message when multiple address were found
        /// </summary>
        public static string MultipleAddressesFound = "Multiple addresses were found for the client.";

        /// <summary>
        /// message when multiple companies were found
        /// </summary>
        public static string MultipleCompaniesFound = "Multiple companies were found for the {0}{1}.";

        /// <summary>
        /// message when multiple submission types were found
        /// </summary>
        public static string MultipleSubmissionTypesFound = "Multiple submission types were found for the {0}{1}.";

        /// <summary>
        /// message when multiple agencies were found
        /// </summary>
        public static string MultipleAgenciesFound = "Multiple agencies were found for the {0}{1}.";

        /// <summary>
        /// message when multiple submission statuses were found
        /// </summary>
        public static string MultipleSubmissionStatusesFound = "Multiple submission statuses were found for the {0}{1}.";

        /// <summary>
        /// message when multiple line of businesses were found
        /// </summary>
        public static string MultipleLineOfBusinessFound = "Multiple line of businesses were found for the {0}{1}.";

        /// <summary>
        /// message when multiple company numbers are found for a code name
        /// </summary>
        public static string MultipleCompanyNumberCodes = "Multiple company numbers were found for the {0}{1}.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulAgencyDeleteFK =
            "This agency can not be deleted because it is currently associated with one or more submissions, agents, agency line of businesses or agency line of business person underwriting roles.";
        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulAgencyUpdateFK =
            "This agency can not be updated because it is currently associated with one or more submissions, agents, agency line of businesses or agency line of business person underwriting roles.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulAgentDeleteFK =
            "This agent can not be deleted because it is currently associated with one or more submissions.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulContactDeleteFK =
            "This contact can not be deleted because it is currently associated with one or more submissions.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulClassCodeDeleteFK =
            "This Class Code can not be deleted because it is currently associated with one or more clients.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulAgentUpdateFK =
            "This agent can not be updated because it is currently associated with one or more submissions.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulContactUpdateFK =
            "This contact can not be updated because it is currently associated with one or more submissions.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulClassCodeUpdateFK =
            "This class code can not be updated because it is currently associated with one or more clients and/or sic codes.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulCompanyNumberCodeDeleteFK =
           "This Company Number Code can not be deleted because it is currently associated with one or more submissions.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulCompanyNumberCodeUpdateFK =
            "This Company Number Code can not be updated because it is currently associated with one or more submissions.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulCompanyNumberCodeTypeDeleteFK =
           "This Company Number Code Type can not be deleted because it is currently associated with one or more company number code.";
        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulCompanyNumberCodeTypeUpdateFK =
            "This Company Number Type Code can not be updated because it is currently associated with one or more company number code.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulPersonDeleteFK =
           "This Person can not be deleted because it is currently associated with one or more submissions or agency line of business person.";
        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulPersonUpdateFK =
            "This Person can not be updated because it is currently associated with one or more submissions or agency line of business person.";

        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulCompanyNumberAttributeDeleteFK =
           "This Company Number Attribute can not be deleted because it is currently associated with one or more submissions or clients.";
        /// <summary>
        /// 
        /// </summary>
        public static string UnsuccessfulCompanyNumberAttributeUpdateFK =
            "This Company Number Attribute can not be updated because it is currently associated with one or more submissions or clients.";
    }
}
