using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("number_control_list")]
    public class NumberControlList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("number_controls"), XmlArrayItem("number_control", typeof(NumberControl))]
        public NumberControl[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class NumberControl
    {
        private int _id, _companyNumberId, _numberControlTypeId;
        private long _controlNumber;
        private string _numberControlTypeName;

        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        
        /// <summary>
        /// id of the company number this belongs to
        /// </summary>
        [XmlElement("company_number_id")]
        public int CompanyNumberId
        {
            get { return _companyNumberId; }
            set { _companyNumberId = value; }
        }

        /// <summary>
        /// id of the number control type
        /// </summary>
        [XmlElement("numbercontrol_type_id")]
        public int NumberControlTypeId
        {
            get { return _numberControlTypeId; }
            set { _numberControlTypeId = value; }
        }

        /// <summary>
        /// number control type name
        /// </summary>
        [XmlElement("numbercontrol_type_name")]
        public string NumberControlTypeName
        {
            get { return _numberControlTypeName; }
            set { _numberControlTypeName = value; }
        }

        /// <summary>
        /// the control number
        /// </summary>
        [XmlElement("control_number")]
        public long ControlNumber
        {
            get { return _controlNumber; }
            set { _controlNumber = value; }
        }
    }
}
