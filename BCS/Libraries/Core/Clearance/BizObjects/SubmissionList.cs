using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// list of submissions
    /// </summary>
    [XmlRoot("submission_list")]
    public class SubmissionList : PagedResponse
    {
        /// <summary>
        /// submissions
        /// </summary>
        [XmlArray("submissions"),XmlArrayItem("submission",typeof(Submission))]
        public Submission[] List;
    }
}
