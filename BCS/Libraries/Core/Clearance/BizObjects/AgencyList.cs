using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("agency_list")]
    public class AgencyList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_number")]
        public string CompanyNumber;

        /// <summary>
        /// list of agencies for the compnay number
        /// </summary>
        [XmlArray("agencies"), XmlArrayItem("agency", typeof(Agency))]
        public Agency[] List;

    }

    /// <summary>
    /// serializable class agency
    /// </summary>
    [XmlRoot("agency")]
    public class Agency
    {
        private int _id, _companyNumberId, _masterAgencyId;
        private int _referralAgencyId;
        private decimal _fax, _phone;
        private string _FEIN, _contactPerson, _email, _agencyNumber, _referralAgencyNumber, _agencyName, _address, _address2, _city, _state,
            _stateTaxId, _imageFax, _branchId, _entryBy, _modifiedBy, _contactPersonPhone, _contactPersonExtension, _contactPersonFax,
            _contactPersonEmail, _stateOfIncorporation, _comments, _zip, _captiveBroker;
        private bool _active, _isMaster;

        public bool IsMaster
        {
            get { return _isMaster; }
            set { _isMaster = value; }
        }
        private DateTime _modifiedDate, _entryDate, _effectiveDate, _canceledDate, _referralDate;
        private StateList _licensedStatesList;
                
        /// <summary>
        /// agency id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get {return _id;}
            set {_id = value;}
        }

        /// <summary>
        /// company number id
        /// </summary>
        [XmlElement("master_agency_id")]
        public int MasterAgencyId
        {
            get { return _masterAgencyId; }
            set { _masterAgencyId = value; }
        }

        /// <summary>
        /// company number id
        /// </summary>
        [XmlElement("company_number_id")]
        public int CompanyNumberId
        {
            get { return _companyNumberId; }
            set { _companyNumberId = value; }
        }

        /// <summary>
        /// fein
        /// </summary>
        [XmlElement("FEIN")]
        public string FEIN
        {
            get { return _FEIN; }
            set { _FEIN = value; }
        }

        /// <summary>
        /// state tax id
        /// </summary>
        [XmlElement("state_tax_id")]
        public string StateTaxId
        {
            get { return _stateTaxId; }
            set { _stateTaxId = value; }
        }
        
        /// <summary>
        /// contact person
        /// </summary>
        [XmlElement("contact_person")]
        public string ContactPerson
        {
            get { return _contactPerson; }
            set { _contactPerson = value; }
        }

        /// <summary>
        /// contact person phone
        /// </summary>
        [XmlElement("contact_person_phone")]
        public string ContactPersonPhone
        {
            get { return _contactPersonPhone; }
            set { _contactPersonPhone = value; }
        }
        /// <summary>
        /// contact person phone extension
        /// </summary>
        [XmlElement("contact_person_extension")]
        public string ContactPersonExtension
        {
            get { return _contactPersonExtension; }
            set { _contactPersonExtension = value; }
        }

        /// <summary>
        /// contact person phone fax
        /// </summary>
        [XmlElement("contact_person_fax")]
        public string ContactPersonFax
        {
            get { return _contactPersonFax; }
            set { _contactPersonFax = value; }
        }

        /// <summary>
        /// contact person phone email
        /// </summary>
        [XmlElement("contact_person_email")]
        public string ContactPersonEmail
        {
            get { return _contactPersonEmail; }
            set { _contactPersonEmail = value; }
        }

        /// <summary>
        /// email
        /// </summary>
        [XmlElement("email")]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }


        /// <summary>
        /// agency number
        /// </summary>
        [XmlElement("agency_number")]
        public string AgencyNumber
        {
            get { return _agencyNumber; }
            set { _agencyNumber = value; }
        }

        /// <summary>
        /// agency number
        /// </summary>
        [XmlElement("referral_agency_number")]
        public string ReferralAgencyNumber
        {
            get { return _referralAgencyNumber; }
            set { _referralAgencyNumber = value; }
        }

        /// <summary>
        /// agency number
        /// </summary>
        [XmlElement("referral_agency_id")]
        public int ReferralAgencyId
        {
            get { return _referralAgencyId; }
            set { _referralAgencyId = value; }
        }

        /// <summary>
        /// referral date
        /// </summary>
        [XmlElement("referral_dt")]
        public DateTime ReferralDate
        {
            get { return _referralDate; }
            set { _referralDate = value; }
        }

        /// <summary>
        /// agency name
        /// </summary>
        [XmlElement("agency_name")]
        public string AgencyName
        {
            get { return _agencyName; }
            set { _agencyName = value; }
        }

        /// <summary>
        /// address
        /// </summary>
        [XmlElement("address")]
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        /// <summary>
        /// address
        /// </summary>
        [XmlElement("address2")]
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }
        /// <summary>
        /// city
        /// </summary>
        [XmlElement("city")]
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        /// <summary>
        /// state
        /// </summary>
        [XmlElement("state")]
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
                
        /// <summary>
        /// state
        /// </summary>
        [XmlElement("state_of_incorporation")]
        public string StateOfIncorporation
        {
            get { return _stateOfIncorporation; }
            set { _stateOfIncorporation = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public StateList LicensedStatesList
        {
            get { return _licensedStatesList; }
            set { _licensedStatesList = value; }
        }        

        /// <summary>
        /// zip
        /// </summary>
        [XmlElement("zip")]
        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        /// <summary>
        /// phone
        /// </summary>
        [XmlElement("phone")]
        public decimal Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

         /// <summary>
        /// fax
        /// </summary>
        [XmlElement("fax")]
        public decimal Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        /// <summary>
        /// image fax
        /// </summary>
        [XmlElement("image_fax")]
        public string ImageFax
        {
            get { return _imageFax; }
            set { _imageFax = value; }
        }

        /// <summary>
        /// comments
        /// </summary>
        [XmlElement("comments")]
        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        /// <summary>
        /// branch id
        /// </summary>
        [XmlElement("branch_id")]
        public string BranchId
        {
            get { return _branchId; }
            set { _branchId = value; }
        }

        /// <summary>
        /// is active agency
        /// </summary>
        [XmlElement("active")]
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        /// <summary>
        /// entered by
        /// </summary>
        [XmlElement("entry_by")]
        public string EntryBy
        {
             get { return _entryBy; }
             set { _entryBy = value; }
        }

        /// <summary>
        /// date of entry
        /// </summary>
        [XmlElement("entry_date")]
        public DateTime EntryDate
        {
            get { return _entryDate; }
            set { _entryDate = value; }
        }

        /// <summary>
        /// modified by
        /// </summary>
        [XmlElement("modified_by")]
        public string ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        /// <summary>
        /// modified date
        /// </summary>
        [XmlElement("modified_dt")]
        public DateTime ModifiedDt
        {
            get { return _modifiedDate; }
            set { _modifiedDate = value; }
        }

        /// <summary>
        /// canceled date
        /// </summary>
        [XmlElement("cancel_dt")]
        public DateTime CancelDate
        {
            get { return _canceledDate; }
            set { _canceledDate = value; }
        }

        /// <summary>
        /// effective date
        /// </summary>
        [XmlElement("effective_dt")]
        public DateTime EffectiveDate
        {
            get { return _effectiveDate; }
            set { _effectiveDate = value; }
        }

        /// <summary>
        /// Captive Broker
        /// </summary>
        [XmlElement("captive_broker")]
        public string CaptiveBroker
        {
            get { return _captiveBroker; }
            set { _captiveBroker = value; }
        }
    }
}
