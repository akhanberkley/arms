using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("address_type_list")]
    public class AddressTypeList
    {
        /// <summary>
        /// list of code types
        /// </summary>
        [XmlArray("address_types"), XmlArrayItem("address_type", typeof(AddressType))]
        public AddressType[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class AddressType
    {
        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// type
        /// </summary>
        [XmlElement("type")]
        public string Type;
    }
}
