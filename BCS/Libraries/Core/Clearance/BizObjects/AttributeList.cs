using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("attribute_list")]
    public class CompanyNumberAttributeList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_number")]
        public string CompanyNumber;

        /// <summary>
        /// list of agencies for the compnay number
        /// </summary>
        [XmlArray("attributes"), XmlArrayItem("attribute", typeof(CompanyNumberAttribute))]
        public CompanyNumberAttribute[] List;
    }
    /// <summary>
    /// 
    /// </summary>
    public class CompanyNumberAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("attribute_id")]
        public int AttributeId;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("data_type_name")]
        public string DataTypeName;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("user_control")]
        public string UserControl;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("label")]
        public string Label;

        /// <summary>
        /// indicates whether this attribute is specific to clients or submissions.
        /// </summary>
        [XmlElement("client_specific")]
        public bool ClientSpecific;

        /// <summary>
        /// read only
        /// </summary>
        [XmlElement("read_only")]
        public bool ReadOnly;

        /// <summary>
        /// display order
        /// </summary>
        [XmlElement("display_order")]
        public short DisplayOrder;
    }
}
