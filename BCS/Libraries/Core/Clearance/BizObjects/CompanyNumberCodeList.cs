using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// class CompanyNumberCodeList
    /// </summary>
    [XmlRoot("company_number_code_list")]
    public class CompanyNumberCodeList
    {
        /// <summary>
        /// CodeName
        /// </summary>
        [XmlElement("code_name")]
        public string CodeName;

        /// <summary>
        /// Company number
        /// </summary>
        [XmlElement("company_number")]
        public string CompanyNumber;

        

        /// <summary>
        /// list of codes
        /// </summary>
        [XmlArray("code_number_codes"), XmlArrayItem("code_type", typeof(CompanyNumberCode))]
        public CompanyNumberCode[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class CompanyNumberCode    
    {
        private int _id, _companyNumberId, _companyCodeTypeId;
        private string _code, _description;
        private DateTime expirationDate;

        public DateTime ExpirationDate
        {
            get { return expirationDate; }
            set { expirationDate = value; }
        }
        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get{ return _id; }
            set { _id = value; }
        }


        /// <summary>
        /// company number id
        /// </summary>
        [XmlElement("company_number_id")]
        public int CompanyNumberId
        {
            get { return _companyNumberId; }
            set { _companyNumberId = value; }
        }

        /// <summary>
        /// CompanyCodeTypeId
        /// </summary>
        [XmlElement("company_code_type_id")]
        public int CompanyCodeTypeId
        {
            get { return _companyCodeTypeId; }
            set { _companyCodeTypeId = value; }
        }

        /// <summary>
        /// code
        /// </summary>
        [XmlElement("code")]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        /// <summary>
        /// description
        /// </summary>
        [XmlElement("description")]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
}
