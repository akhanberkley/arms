using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("client_name_type_list")]
    public class ClientNameTypeList
    {
        /// <summary>
        /// list of code types
        /// </summary>
        [XmlArray("client_name_types"), XmlArrayItem("client_name_type", typeof(ClientNameType))]
        public ClientNameType[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class ClientNameType
    {
        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// code name
        /// </summary>
        [XmlElement("code_type")]
        public string NameType;
    }
}
