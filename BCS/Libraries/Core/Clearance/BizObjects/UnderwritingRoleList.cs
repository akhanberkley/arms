using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("underwriting_roles_list")]
    public class UnderwritingRoleList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("underwriting_roles"), XmlArrayItem("underwriting_role", typeof(UnderwritingRole))]
        public UnderwritingRole[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class UnderwritingRole
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("role_code")]
        public string RoleCode;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("role_name")]
        public string RoleName;
    }
}
