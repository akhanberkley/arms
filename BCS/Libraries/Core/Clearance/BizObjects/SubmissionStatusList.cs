using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>    
    [XmlRoot("submission_status_list")]
    public class SubmissionStatusList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("submission_statuses"), XmlArrayItem("submission_status", typeof(SubmissionStatus))]
        public SubmissionStatus[] List;
    }

    /// <summary>
    /// serializable class SubmissionStatus
    /// </summary>
    [Serializable]
    [XmlRoot("submission_status")]
    public class SubmissionStatus
    {
        /// <summary>
        /// submission status id
        /// </summary>
        [XmlElement("id")]
        public int Id;

        private string statusCode;
        /// <summary>
        /// submission status code
        /// </summary>
        [XmlElement("status_code")]
        public string StatusCode
        {
            get { return statusCode; }
            set { statusCode = value; }
        }
    }
}
