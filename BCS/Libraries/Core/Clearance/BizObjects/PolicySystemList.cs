using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("policy_system_list")]
    public class PolicySystemList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("policy_systems"), XmlArrayItem("policy_system", typeof(PolicySystem))]
        public PolicySystem[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class PolicySystem
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("associated_company_number_id")]
        public int AssociatedCompanyNumberId;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("property_policy_system")]
        public string PropertyPolicySystem;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("abbreviation")]
        public string Abbreviation;
    }
}
