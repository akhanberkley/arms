using System;
using System.Xml.Serialization;
using System.Collections;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("state_list")]
    [Serializable]
    public class StateList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("states"), XmlArrayItem("state", typeof(State))]
        public State[] List;

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            //return base.Equals(obj);
             if (obj == null)
                return false;
            
            if (this.GetType() != obj.GetType())
                return false;

            StateList oStateList = obj as StateList;

            if (List == null && oStateList.List == null)
                return true;
            if (List == null || oStateList.List == null)
                return false;

            if (List.Length != oStateList.List.Length)
                return false;

            for (int i = 0; i < List.Length; i++)
            {
                if (!List[i].Equals(oStateList.List[i]))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oStateList"></param>
        /// <returns></returns>
        public bool Equals(StateList oStateList)
        {
            if (oStateList == null)
                return false;

            if (List == null && oStateList.List == null)
                return true;
            if (List == null || oStateList.List == null)
                return false;

            if (List.Length != oStateList.List.Length)
                return false;

            for (int i = 0; i < List.Length; i++)
            {
                if (!List[i].Equals(oStateList.List[i]))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Returns the hash code for the current object
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    /// <summary>
    /// serializable class State
    /// </summary>
    [Serializable]
    public class State
    {
        private int _id;
        private string _stateName, _abbreviation;
        /// <summary>
        /// Gets or sets id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// Gets or sets state name
        /// </summary>
        [XmlElement("state_name")]
        public string StateName
        {
            get { return _stateName; }
            set { _stateName = value; }
        }

        /// <summary>
        /// Gets or sets abbreviation
        /// </summary>
        [XmlElement("abbreviation")]
        public string Abbreviation
        {
            get { return _abbreviation; }
            set { _abbreviation = value; }
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            //return base.Equals(obj);

            if (obj == null)
                return false;

            if (this.GetType() != obj.GetType())
                return false;

            State oState = (State)obj;
            if (!object.Equals(Abbreviation, oState.Abbreviation))
                return false;
            if (!object.Equals(StateName, oState.StateName))
                return false;
            if (!Id.Equals(oState.Id))
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oState"></param>
        /// <returns></returns>
        public bool Equals(State oState)
        {
            if (oState == null)
                return false;
            if (!object.Equals(Abbreviation, oState.Abbreviation))
                return false;
            if (!object.Equals(StateName, oState.StateName))
                return false;
            if (!Id.Equals(oState.Id))
                return false;

            return true;
        }

        /// <summary>
        /// Returns the hash code for the current object
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}







  
