using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// submission
    /// </summary>
    [XmlRoot("submission")]
    public class Submission : Response
    {
        /// <summary>
        /// submission identifier
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// company number
        /// </summary>
        [XmlElement("company_number_id")]
        public int CompanyNumberId;

        /// <summary>
        /// company number
        /// </summary>
        [XmlElement("company_id")]
        public int CompanyId;


        /// <summary>
        /// submission type id
        /// </summary>
        [XmlElement("submission_type_id")]
        public int SubmissionTypeId;

        /// <summary>
        /// submission type id
        /// </summary>
        [XmlElement("submission_type_abbr")]
        public string SubmissionTypeAbbr;

        /// <summary>
        /// submission type id
        /// </summary>
        [XmlElement("submission_type_name")]
        public string SubmissionTypeName;

        /// <summary>
        /// submission type
        /// </summary>
        [XmlElement("submission_type")]
        public SubmissionType SubmissionType;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("submission_token_id")]
        public int SubmissionTokenId;

        /// <summary>
        /// id of the associated client
        /// </summary>
        [XmlElement("clientcore_client_id")]
        public long ClientCoreClientId;

        /// <summary>
        /// address ids of the associated client
        /// </summary>
        [XmlArray("clientcore_address_ids"), XmlArrayItem("clientcore_address_id", typeof(Int32))]
        public int[] ClientCoreAddressIds;

        /// <summary>
        /// temporary holder until UI dependent on this is worked out
        /// </summary>
        [XmlElement("clientcore_address_id")]
        public string ClientCoreAddressId;

        /// <summary>
        /// comma delimited address ids used by UI to display associated submissions
        /// </summary>
        [XmlElement("clientcore_address_ids_string")]
        public string ClientCoreAddressIdsString;

        /// <summary>
        /// name ids of the associated client
        /// </summary>
        [XmlArray("clientcore_name_ids"), XmlArrayItem("clientcore_name_id", typeof(Int32))]
        public int[] ClientCoreNameIds;

        /// <summary>
        /// portfolio id of the associated client
        /// </summary>
        [XmlElement("clientcore_portfolio_id")]
        public long ClientCorePortfolioId;

        /// <summary>
        /// id of the associated agency
        /// </summary>
        [XmlElement("agency_id")]
        public int AgencyId;

        /// <summary>
        /// name of the associated agency
        /// </summary>
        [XmlElement("agency_name")]
        public string AgencyName;

        /// <summary>
        /// number of the associated agency
        /// </summary>
        [XmlElement("agency_number")]
        public string AgencyNumber;

        /// <summary>
        /// insured name
        /// </summary>
        [XmlElement("insured_name")]
        public string InsuredName;

        /// <summary>
        /// dba
        /// </summary>
        [XmlElement("dba")]
        public string Dba;

        /// <summary>
        /// associated agency info
        /// </summary>
        [XmlElement("agency")]
        public Agency Agency;

        /// <summary>
        /// id of associated agent
        /// </summary>
        [XmlElement("agent_id")]
        public int AgentId;

        /// <summary>
        /// associated agent info
        /// </summary>
        [XmlElement("agent")]
        public Agent Agent;

        /// <summary>
        /// id of associated Contact
        /// </summary>
        [XmlElement("contact_id")]
        public int ContactId;

        /// <summary>
        /// associated Contact info
        /// </summary>
        [XmlElement("contact")]
        public Contact Contact;

        /// <summary>
        /// id of associated status
        /// </summary>
        [XmlElement("submission_status_id")]
        public int SubmissionStatusId;

        /// <summary>
        /// submission status date
        /// </summary>
        [XmlElement("submission_status_date")]
        public DateTime SubmissionStatusDate;

        /// <summary>
        /// submission cancellation date
        /// </summary>
        [XmlElement("submission_cancellation_date")]
        public DateTime CancellationDate;

        /// <summary>
        /// code of the associated status
        /// </summary>
        [XmlElement("submission_status_code")]
        public string SubmissionStatusCode;

        /// <summary>
        /// id of the associated status reason
        /// </summary>
        [XmlElement("submission_status_reason_id")]
        public int SubmissionStatusReasonId;

        /// <summary>
        /// Reason code of the associated status reason
        /// </summary>
        [XmlElement("submission_status_reason")]
        public string SubmissionStatusReason;

        /// <summary>
        /// user comment on the submission status
        /// </summary>        
        [XmlElement("submission_status_reason_other")]
        public string SubmissionStatusReasonOther;

        /// <summary>
        /// submission status
        /// </summary>
        [XmlElement("submission_status")]
        public SubmissionStatus SubmissionStatus;

        /// <summary>
        /// id of associated other carrier
        /// </summary>
        [XmlElement("other_carrier_id")]
        public int OtherCarrierId;

        /// <summary>
        /// other carrier
        /// </summary>
        [XmlElement("other_carrier")]
        public OtherCarrier OtherCarrier;

        /// <summary>
        /// id of associated prior other carrier
        /// </summary>
        [XmlElement("prior_other_carrier_id")]
        public int PriorOtherCarrierId;

        /// <summary>
        /// prior other carrier
        /// </summary>
        [XmlElement("prior_other_carrier")]
        public OtherCarrier PriroOtherCarrier;

        /// <summary>
        /// submission number
        /// </summary>
        [XmlElement("submission_number")]
        public long SubmissionNumber;

        /// <summary>
        /// submission number sequence
        /// </summary>
        [XmlElement("submission_number_sequence")]
        public int SubmissionNumberSequence;

        /// <summary>
        /// policy number
        /// </summary>
        [XmlElement("policy_number")]
        public string PolicyNumber;

        /// <summary>
        /// estimated written premium
        /// </summary>
        [XmlElement("estimated_written_premium")]
        public decimal EstimatedWrittenPremium;

        /// <summary>
        /// other carrier premium
        /// </summary>
        [XmlElement("other_carrier_premium")]
        public decimal OtherCarrierPremium;

        /// <summary>
        /// effective date
        /// </summary>
        [XmlElement(ElementName = "effective_date")]
        public DateTime EffectiveDate;

        /// <summary>
        /// expiration date
        /// </summary>
        [XmlElement("expiration_date")]
        public DateTime ExpirationDate;

        /// <summary>
        /// id of the associated underwriter
        /// </summary>
        [XmlElement("underwriter_id")]
        public int UnderwriterId;

        /// <summary>
        /// name of the associated underwriter
        /// </summary>
        [XmlElement("underwriter_name")]
        public string UnderwriterName;

        /// <summary>
        /// initials of the associated underwriter
        /// </summary>
        [XmlElement("underwriter_initials")]
        public string UnderwriterInitials;
        
        /// <summary>
        /// name of the associated underwriter
        /// </summary>
        [XmlElement("underwriter_full_name")]
        public string UnderwriterFullName;

        /// <summary>
        /// id of the previous underwriter
        /// </summary>
        [XmlElement("previous_underwriter_id")]
        public int PreviousUnderwriterId;

        /// <summary>
        /// name of the associated underwriter
        /// </summary>
        [XmlElement("previous_underwriter_name")]
        public string PreviousUnderwriterName;

        /// <summary>
        /// name of the associated underwriter
        /// </summary>
        [XmlElement("previous_underwriter_full_name")]
        public string PreviousUnderwriterFullName;

        /// <summary>
        /// id of the associated analyst
        /// </summary>
        [XmlElement("analyst_id")]
        public int AnalystId;

        /// <summary>
        ///  name of the associated analyst
        /// </summary>
        [XmlElement("analyst_name")]
        public string AnalystName;

        /// <summary>
        ///  name of the associated analyst
        /// </summary>
        [XmlElement("analyst_full_name")]
        public string AnalystFullName;

        /// <summary>
        /// id of the associated technician
        /// </summary>
        [XmlElement("technician_id")]
        public int TechnicianId;

        /// <summary>
        ///  name of the associated technician
        /// </summary>
        [XmlElement("technician_name")]
        public string TechnicianName;

        /// <summary>
        ///  name of the associated technician
        /// </summary>
        [XmlElement("technician_full_name")]
        public string TechnicianFullName;

        /// <summary>
        /// id of the associated policy system
        /// </summary>
        [XmlElement("policy_system_id")]
        public int PolicySystemId;

        /// <summary>
        /// policy system the policy was written on
        /// </summary>
        [XmlElement("policy_system")]
        public PolicySystem PolicySystem;

        /// <summary>
        /// general comments
        /// </summary>
        [XmlElement("comments")]
        public string Comments;

        /// <summary>
        /// submitted by
        /// </summary>
        [XmlElement("submission_by")]
        public string SubmissionBy;

        /// <summary>
        /// submitted date
        /// </summary> 
        [XmlElement("submission_dt")]
        public DateTime SubmissionDt;

        /// <summary>
        /// last updated by
        /// </summary>
        [XmlElement("updated_by")]
        public string UpdatedBy;

        /// <summary>
        /// last updated date
        /// </summary>
        [XmlElement("updated_dt")]
        public DateTime UpdatedDt;

        /// <summary>
        /// id of the associated underwriting unit
        /// </summary>
        [XmlElement("line_of_business_id")]
        public int LineOfBusinessId;

        /// <summary>
        /// line of associated underwriting unit
        /// </summary>
        [XmlElement("line_of_business")]
        public string LineOfBusiness;

        ///// <summary>
        ///// 
        ///// </summary>
        //[XmlElement("code_type_ids")]
        //public string SubmissionCodeTypeIds;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("code_type_ids")]
        public int[] SubmissionCodeTypeIds;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("code_types")]
        public string SubmissionCodeTypes;

        /// <summary>
        /// submission code types
        /// </summary>
        [XmlArray("submission_code_types"), XmlArrayItem("submission_code_type", typeof(SubmissionCodeType))]
        public SubmissionCodeType[] SubmissionCodeTypess;

        /// <summary>
        /// submissions attributes
        /// </summary>
        [XmlArray("submission_attributes"), XmlArrayItem("submission_attribute", typeof(SubmissionAttribute))]
        public SubmissionAttribute[] AttributeList;

        /// <summary>
        /// submissions line of businesses
        /// </summary>
        [XmlArray("submission_line_of_businesses"), XmlArrayItem("submission_line_of_business", typeof(LineOfBusiness))]
        public LineOfBusiness[] LineOfBusinessList;

        [XmlElement("first_submission_line_of_business_child")]
        public FirstSubmissionLineOfBusinessChild FirstSubmissionLineOfBusinessChild;

        /// <summary>
        /// submissions line of business children
        /// </summary>
        [XmlArray("submission_line_of_business_children"), XmlArrayItem("submission_line_of_business_child", typeof(SubmissionLineOfBusinessChild))]
        public SubmissionLineOfBusinessChild[] LineOfBusinessChildList;

        /// <summary>
        /// agent unspecified reasons
        /// </summary>
        [XmlArray("submission_agent_unspecified_reasons"), XmlArrayItem("submission_agent_unspecified_reason", typeof(SubmissionAgentUnspecifiedReason))]
        public SubmissionAgentUnspecifiedReason[] AgentUnspecifiedReasonList;

        /// <summary>
        /// comments
        /// </summary>
        [XmlArray("submission_comments"), XmlArrayItem("submission_comment", typeof(SubmissionComment))]
        public SubmissionComment[] SubmissionComments;

        [XmlElement("deleted")]
        public bool Deleted;

        [XmlElement("deleted_by")]
        public string DeletedBy;

        [XmlElement("Deleted_date")]
        public DateTime DeletedDate;

        [XmlElement("submission_company_number_code_ids")]
        public string CompanyNumberCodeIds;
     
    }

    /// <summary>
    /// 
    /// </summary>
    public class SubmissionAgentUnspecifiedReason
    {
        /// <summary>
        /// company number agent unspecified reason
        /// </summary>
        [XmlElement("company_number_agent_unspecified_reason_id")]
        public int CompanyNumberAgentUnspecifiedReasonId;

        /// <summary>
        /// agent unspecified reason
        /// </summary>
        [XmlElement("agent_unspecified_reason")]
        public string Reason;
    }

    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("submission_code_type")]
    public class SubmissionCodeType
    {
        /// <summary>
        /// code id
        /// </summary>
        [XmlElement("code_id")]
        public int CodeId;

        /// <summary>
        /// code type id
        /// </summary>
        [XmlElement("code_type_id")]
        public int CodeTypeId;

        /// <summary>
        /// code name
        /// </summary>
        [XmlElement("code_name")]
        public string CodeName;

        /// <summary>
        /// code value
        /// </summary>
        [XmlElement("code_value")]
        public string CodeValue;

        /// <summary>
        /// code value
        /// </summary>
        [XmlElement("code_description")]
        public string CodeDescription;
    }

    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("submission_attribute")]
    public class SubmissionAttribute
    {
        /// <summary>
        /// company number
        /// </summary>
        [XmlElement("company_number_attribute_id")]
        public int CompanyNumberAttributeId;

        /// <summary>
        /// attribute value
        /// </summary>
        [XmlElement("attribute_label")]
        public string Label;

        /// <summary>
        /// attribute type
        /// </summary>
        [XmlElement("attribute_type")]
        public string DataType;

        /// <summary>
        /// attribute value
        /// </summary>
        [XmlElement("attribute_value")]
        public string AttributeValue;
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("submission_line_of_business_child")]
    public class SubmissionLineOfBusinessChild
    {
        private int id;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private int companyNumberLOBGridId;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_number_lob_grid_id")]
        public int CompanyNumberLOBGridId
        {
            get { return companyNumberLOBGridId; }
            set { companyNumberLOBGridId = value; }
        }

        private CompanyNumberLOBGrid companyNumberLOBGrid;
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_number_lob_grid")]
        public CompanyNumberLOBGrid CompanyNumberLOBGrid
        {
            get { return companyNumberLOBGrid; }
            set { companyNumberLOBGrid = value; }
        }

        private int submissionStatusId;
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("submission_status_child_id")]
        public int SubmissionStatusId
        {
            get { return submissionStatusId; }
            set { submissionStatusId = value; }
        }

        private SubmissionStatus submissionStatus;
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("submission_status_child")]
        public SubmissionStatus SubmissionStatus
        {
            get { return submissionStatus; }
            set { submissionStatus = value; }
        }


        private int submissionStatusReasonId;
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("submission_status_reason_child_id")]
        public int SubmissionStatusReasonId
        {
            get { return submissionStatusReasonId; }
            set { submissionStatusReasonId = value; }
        }

        private SubmissionStatusReason submissionStatusReason;
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("submission_status_reason_child")]
        public SubmissionStatusReason SubmissionStatusReason
        {
            get { return submissionStatusReason; }
            set { submissionStatusReason = value; }
        }

        /// <summary>
        /// to indicate if this is to be deleted on update submission
        /// </summary>
        [XmlElement("deleted")]
        public bool Deleted;
    }

    [Serializable]
    public class FirstSubmissionLineOfBusinessChild
    {
        int id, lobId;
        string lobCode, lobDescription;

        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// lob id of firt submission lob
        /// </summary>
        [XmlElement("first_lob_id")]
        public int LobId
        {
            get { return lobId; }
            set { lobId = value; }
        }

        /// <summary>
        /// lob code of the first submission lob
        /// </summary>
        [XmlElement("first_lob_code")]
        public string LobCode
        {
            get { return lobCode; }
            set { lobCode = value; }
        }

        /// <summary>
        /// lob description of the first submission lob
        /// </summary>
        [XmlElement("first_lob_description")]
        public string LobDescription
        {
            get { return lobDescription; }
            set { lobDescription = value; }
        }
    }

    [Serializable]
    [XmlRoot("submission_comment")]
    public class SubmissionComment
    {
        int id, submissionId;
        string comment, entryBy, modifiedBy;
        DateTime entryDt, modifiedDt;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("submission_id")]
        public int SubmissionId
        {
            get { return submissionId; }
            set { submissionId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("comment")]
        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("entry_by")]
        public string EntryBy
        {
            get { return entryBy; }
            set { entryBy = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("modified_by")]
        public string ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("entrydt")]
        public DateTime EntryDt
        {
            get { return entryDt; }
            set { entryDt = value; }
        }

        [XmlElement("modified_dt")]
        public DateTime ModifiedDt
        {
            get { return modifiedDt; }
            set { modifiedDt = value; }
        }

        /// <summary>
        /// to indicate if this is to be deleted on update submission
        /// </summary>
        [XmlElement("deleted")]
        public bool Deleted;
    }
}
