using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("agent_list")]
    public class AgentList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_number")]
        public string CompanyNumber;

        /// <summary>
        /// list of agencies for the compnay number
        /// </summary>
        [XmlArray("agents"), XmlArrayItem("agent", typeof(Agent))]
        public Agent[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class Agent
    {
        private int _id, _agencyId, _userId, _addressid;

        private string _brokerNo, _prefix, _firstName, _middle, _suffixTitle, _surname, _professionalTitle,
            _functionalTitle, _departmentName, _mailStopCode, _primaryPhoneAreaCode, _modifiedBy, _entryBy,
            _primaryPhone, _secondaryPhoneAreaCode, _secondaryPhone, _faxAreaCode, _fax, _emailAddress, _taxid;

        private bool _active;

        private DateTime _entryDate, _modifiedDt, _effectiveDate, _canceledDate, _dob;

        /// <summary>
        /// agent id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// agency the agent belongs to
        /// </summary>
        [XmlElement("agency_id")]
        public int AgencyId
        {
            get { return _agencyId; }
            set { _agencyId = value; }
        }

        /// <summary>
        /// user id
        /// </summary>
        [XmlElement("user_id")]
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        /// <summary>
        /// broker no
        /// </summary>
        [XmlElement("broker_no")]
        public string BrokerNo
        {
            get { return _brokerNo; }
            set { _brokerNo = value; }
        }

        /// <summary>
        /// address id
        /// </summary>
        [XmlElement("address_id")]
        public int AddressId
        {
            get { return _addressid; }
            set { _addressid = value; }
        }

        /// <summary>
        /// name prefix
        /// </summary>
        [XmlElement("prefix")]
        public string Prefix
        {
            get { return _prefix; }
            set { _prefix = value; }
        }

        /// <summary>
        /// tax id
        /// </summary>
        [XmlElement("taxid")]
        public string TaxId
        {
            get { return _taxid; }
            set { _taxid = value; }
        }

        /// <summary>
        /// first name
        /// </summary>
        [XmlElement("first_name")]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        /// <summary>
        /// middle name
        /// </summary>
        [XmlElement("middle")]
        public string Middle
        {
            get { return _middle; }
            set { _middle = value; }
        }

        /// <summary>
        /// surname
        /// </summary>
        [XmlElement("surname")]
        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }

        /// <summary>
        /// name suffix
        /// </summary>
        [XmlElement("suffix_title")]
        public string SuffixTitle
        {
            get { return _suffixTitle; }
            set { _suffixTitle = value; }
        }

        /// <summary>
        /// prefessional title
        /// </summary>
        [XmlElement("professional_title")]
        public string ProfessionalTitle
        {
            get { return _professionalTitle; }
            set { _professionalTitle = value; }
        }

        /// <summary>
        /// functional title
        /// </summary>
        [XmlElement("functional_title")]
        public string FunctionalTitle
        {
            get { return _functionalTitle; }
            set { _functionalTitle = value; }
        }

        /// <summary>
        /// department name
        /// </summary>
        [XmlElement("department_name")]
        public string DepartmentName
        {
            get { return _departmentName; }
            set { _departmentName = value; }
        }

        /// <summary>
        /// mail stop code
        /// </summary>
        [XmlElement("mail_stop_code")]
        public string MailStopCode
        {
            get { return _mailStopCode; }
            set { _mailStopCode = value; }
        }

        /// <summary>
        /// primary phone area code
        /// </summary>
        [XmlElement("primary_phone_area_code")]
        public string PrimaryPhoneAreaCode
        {
            get { return _primaryPhoneAreaCode; }
            set { _primaryPhoneAreaCode = value; }
        }

        /// <summary>
        /// primary phone
        /// </summary>
        [XmlElement("primary_phone")]
        public string PrimaryPhone
        {
            get { return _primaryPhone; }
            set { _primaryPhone = value; }
        }

        /// <summary>
        /// secondary phone area code
        /// </summary>
        [XmlElement("secondary_phone_area_code")]
        public string SecondaryPhoneAreaCode
        {
            get { return _secondaryPhoneAreaCode; }
            set { _secondaryPhoneAreaCode = value; }
        }

        /// <summary>
        /// secondary phone
        /// </summary>
        [XmlElement("secondary_phone")]
        public string SecondaryPhone
        {
            get { return _secondaryPhone; }
            set { _secondaryPhone = value; }
        }

        /// <summary>
        /// secondary phone area code
        /// </summary>
        [XmlElement("fax_area_code")]
        public string FaxAreaCode
        {
            get { return _faxAreaCode; }
            set { _faxAreaCode = value; }
        }

        /// <summary>
        /// secondary phone
        /// </summary>
        [XmlElement("fax")]
        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        /// <summary>
        /// email
        /// </summary>
        [XmlElement("email_address")]
        public string EmailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }

        /// <summary>
        /// is active agency
        /// </summary>
        [XmlElement("active")]
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        /// <summary>
        /// entered by
        /// </summary>
        [XmlElement("entry_by")]
        public string EntryBy
        {
            get { return _entryBy; }
            set { _entryBy = value; }
        }

        /// <summary>
        /// date of entry
        /// </summary>
        [XmlElement("entry_date")]
        public DateTime EntryDate
        {
            get { return _entryDate; }
            set { _entryDate = value; }
        }

        /// <summary>
        /// modified by
        /// </summary>
        [XmlElement("modified_by")]
        public string ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }

        /// <summary>
        /// modified date
        /// </summary>
        [XmlElement("modified_dt")]
        public DateTime ModifiedDt
        {
            get { return _modifiedDt; }
            set { _modifiedDt = value; }
        }

        /// <summary>
        /// date of birth
        /// </summary>
        [XmlElement("dob")]
        public DateTime DOB
        {
            get { return _dob; }
            set { _dob = value; }
        }

        /// <summary>
        /// canceled date
        /// </summary>
        [XmlElement("cancel_dt")]
        public DateTime CancelDate
        {
            get { return _canceledDate; }
            set { _canceledDate = value; }
        }

        /// <summary>
        /// effective date
        /// </summary>
        [XmlElement("effective_dt")]
        public DateTime EffectiveDate
        {
            get { return _effectiveDate; }
            set { _effectiveDate = value; }
        }
    }
}
