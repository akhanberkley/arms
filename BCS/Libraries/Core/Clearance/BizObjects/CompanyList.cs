using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("company_list")]
    public class CompanyList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("companies"), XmlArrayItem("company", typeof(Company))]
        public Company[] List;
    }

    /// <summary>
    /// company
    /// </summary>
    [XmlRoot("company")]
    public class Company
    {
        /// <summary>
        /// company id
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("clientcore_corp_id")]
        public string ClientCoreCorpId;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("clientcore_corp_code")]
        public string ClientCoreCorpCode;

        /// <summary>
        /// company name
        /// </summary>
        [XmlElement("name")]
        public string CompanyName;

        /// <summary>
        /// company initials
        /// </summary>
        [XmlElement("initials")]
        public string CompanyInitials;

        /// <summary>
        /// allow submissions on error
        /// </summary>
        [XmlElement("allow_on_error")]
        public bool AllowSubmissionsOnError;

        /// <summary>
        /// DSIK
        /// </summary>
        [XmlElement("dsik")]
        public string DSIK;

        /// <summary>
        /// end point url on the client core system
        /// </summary>
        [XmlElement("clientcore_url")]
        public string ClientCoreEndPointUrl;

        /// <summary>
        /// system code on the client core system
        /// </summary>
        [XmlElement("clientcore_system_cd")]
        public string ClientCoreSystemCd;

        /// <summary>
        /// user name on the client core system
        /// </summary>
        [XmlElement("clientcore_username")]
        public string ClientCoreUsername;

        /// <summary>
        /// password on the client core system
        /// </summary>
        [XmlElement("clientcore_password")]
        public string ClientCorePassword;

        /// <summary>
        /// owner on the client core system
        /// </summary>
        [XmlElement("clientcore_owner")]
        public string ClientCoreOwner;

        /// <summary>
        /// owner on the client core system
        /// </summary>
        [XmlElement("clientcore_client_type")]
        public string ClientCoreClientType;

        /// <summary>
        /// local id on the client core system
        /// </summary>
        [XmlElement("clientcore_local_id")]
        public string ClientCoreLocalId;

        /// <summary>
        /// The role Id of this company for an underwriter
        /// </summary>
        [XmlElement("underwriter_role_id")]
        public int UnderwriterRoleId;

        /// <summary>
        /// The role Id of this company for an underwriter analyst
        /// </summary>
        [XmlElement("analyst_role_id")]
        public int AnalystRoleId;

        /// <summary>
        /// company logo
        /// </summary>
        [XmlElement("logo_url")]
        public string LogoUrl;

        /// <summary>
        /// themes and styles for the company
        /// </summary>
        [XmlElement("theme_stylesheet")]
        public string ThemeStyleSheet;

        /// <summary>
        /// The service type to use for the company, 0 = old, 1 = new.
        /// </summary>
        [XmlElement("service_type")]
        public int ServiceType;

        /// <summary>
        /// filter parameters for searches
        /// </summary>
        [XmlElement("search_filters")]
        public BTS.ClientCore.WS.SF.processingOption[] SearchFilters;

        /// <summary>
        /// filter parameters for searches
        /// </summary>
        [XmlElement("dietSearch_options")]
        public string dietSearchOptions;

        /// <summary>
        /// filter parameters for searches
        /// </summary>
        [XmlElement("filter_perform_clientsearch")]
        public bool filterPerformClientSearch;

        /// <summary>
        /// filter parameters for searches
        /// </summary>
        [XmlElement("filter_get_client_info")]
        public bool filterGetClientInfo;

        [XmlElement("client_type")]
        public string ClientType;
    }
}
