using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("agency_line_of_business_list")]
    public class AgencyLineOfBusinessList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_number_id")]
        public string CompanyNumberId;

        /// <summary>
        /// list of agencies for the compnay number
        /// </summary>
        [XmlArray("agency_line_of_businesses"), XmlArrayItem("agency_line_of_business", typeof(AgencyLineOfBusiness))]
        public AgencyLineOfBusiness[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class AgencyLineOfBusiness
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("agency")]
        public Agency Agency;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("line_of_business")]
        public LineOfBusiness LineOfBusiness;
    }
}
