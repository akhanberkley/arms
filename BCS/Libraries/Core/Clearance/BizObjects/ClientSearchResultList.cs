﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{

    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("clientsearchresult_list")]
    public class ClientSearchResultList
    {
        ///<summary>
        ///
        ///</summary>
        [XmlArray("clientsearchresults"), XmlArrayItem("clientsearchresult", typeof(ClientSearchResult))]
        public ClientSearchResult[] List;

    }
    public class ClientSearchResult
    {
        ///<summary>
        ///
        ///</summary>
        [XmlElement("clientid")]
        public int ClientId;


        ///<summary>
        ///
        ///</summary>
        [XmlElement("client-name")]
        public String Name;

        ///<summary>
        ///
        ///</summary>
        [XmlElement("client-address")]
        public String Address;
    }
}
