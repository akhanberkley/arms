using System;
using System.Xml.Serialization;
using System.Collections;

namespace BCS.Core.Clearance.BizObjects
{
    [XmlRoot("role_list")]
    [Serializable]
    public class RoleList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("roles"), XmlArrayItem("role", typeof(Role))]
        public Role[] List;

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (this.GetType() != obj.GetType())
                return false;

            RoleList oRoleList = obj as RoleList;

            if (List == null && oRoleList.List == null)
                return true;
            if (List == null || oRoleList.List == null)
                return false;

            if (List.Length != oRoleList.List.Length)
                return false;

            for (int i = 0; i < List.Length; i++)
            {
                if (!List[i].Equals(oRoleList.List[i]))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oRoleList"></param>
        /// <returns></returns>
        public bool Equals(RoleList oRoleList)
        {
            if (oRoleList == null)
                return false;

            if (List == null && oRoleList.List == null)
                return true;
            if (List == null || oRoleList.List == null)
                return false;

            if (List.Length != oRoleList.List.Length)
                return false;

            for (int i = 0; i < List.Length; i++)
            {
                if (!List[i].Equals(oRoleList.List[i]))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Returns the hash code for the current object
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }


    [Serializable]
    public class Role
    {
        private int idField;
        private string roleNameField;
        private bool readOnlyField, universalAccessField;

        /// <summary>
        /// Gets or sets id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return idField; }
            set { idField = value; }
        }

        /// <summary>
        /// Gets or sets role name
        /// </summary>
        [XmlElement("role_name")]
        public string RoleName
        {
            get { return roleNameField; }
            set { roleNameField = value; }
        }

        /// <summary>
        /// Gets or sets read only flag
        /// </summary>
        [XmlElement("read_only")]
        public bool ReadOnly
        {
            get { return readOnlyField; }
            set { readOnlyField = value; }
        }

        /// <summary>
        /// Gets or sets universal access flag
        /// </summary>
        [XmlElement("universal_access")]
        public bool UniversalAccess
        {
            get { return universalAccessField; }
            set { universalAccessField = value; }
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (this.GetType() != obj.GetType())
                return false;

            Role oRole = (Role)obj;
            if (!object.Equals(RoleName, oRole.RoleName))
                return false;
            if (!object.Equals(UniversalAccess, oRole.UniversalAccess))
                return false;
            if (!object.Equals(ReadOnly, oRole.ReadOnly))
                return false;
            if (!Id.Equals(oRole.Id))
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oRole"></param>
        /// <returns></returns>
        public bool Equals(Role oRole)
        {
            if (oRole == null)
                return false;
            if (!object.Equals(RoleName, oRole.RoleName))
                return false;
            if (!object.Equals(UniversalAccess, oRole.UniversalAccess))
                return false;
            if (!object.Equals(ReadOnly, oRole.ReadOnly))
                return false;
            if (!Id.Equals(oRole.Id))
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
