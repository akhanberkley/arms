using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("prefix_list")]
    public class PrefixList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("prefixes"), XmlArrayItem("prefix", typeof(Prefix))]
        public Prefix[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class Prefix
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("prefix_title")]
        public string PrefixTitle;
    }
}