using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("geographic_directional_list")]
    public class GeographicDirectionalList
    {
        /// <summary>
        /// list of Geographic Directionals
        /// </summary>
        [XmlArray("geographic_directionals"), XmlArrayItem("geographic_directional", typeof(GeographicDirectional))]
        public GeographicDirectional[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class GeographicDirectional
    {
        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// company number id
        /// </summary>
        [XmlElement("g_directional")]
        public string GDirectional;

        /// <summary>
        /// code name
        /// </summary>
        [XmlElement("abbreviation")]
        public string Abbreviation;
    }
}
