using System;
using System.Xml.Serialization;
using System.Collections;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("user_list")]
    public class UserList : ICollection
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("users"), XmlArrayItem("user", typeof(User))]
        public User[] List;

        #region IList Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int Add(object value)
        {
            if (value is User)
            {
                ArrayList al = new ArrayList(List);
                int i = al.Add(value);
                User[] newList = new User[al.Count];
                al.CopyTo(newList, 0);
                this.List = newList;
                return i;

            }
            else
            {
                throw new ArgumentException("value should be of type BCS.Core.Clearance.BizObjects.State");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            List = null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Contains(object value)
        {
            bool contains = false;
            foreach (User cn in List)
            {
                if (cn == value)
                {
                    contains = true;
                    break;
                }

            }
            return contains;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int IndexOf(object value)
        {
            for (int i = 0; i < List.Length; i++)
            {
                if (List[i] == value)
                    return i;
            }
            return -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        public void Insert(int index, object value)
        {
            if (value is User)
            {
                ArrayList al = new ArrayList(List);
                al.Insert(index, value);
                User[] newList = new User[al.Count];
                al.CopyTo(newList, 0);
                this.List = newList;
            }
            else
            {
                throw new ArgumentException("value should be of type BCS.Core.Clearance.BizObjects.State");
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsFixedSize
        {
            get { return false; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Remove(object value)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        public void RemoveAt(int index)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public object this[int index]
        {
            get
            {
                if (List != null)
                    return List[index];
                return null;
            }
            set
            {
                List[index] = (User)value;
            }
        }

        #endregion

        #region ICollection Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public void CopyTo(Array array, int index)
        {
            List.CopyTo(array, index);
        }
        /// <summary>
        /// 
        /// </summary>
        public int Count
        {
            get { return List.Length; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsSynchronized
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        /// <summary>
        /// 
        /// </summary>
        public object SyncRoot
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion

        #region IEnumerable Members
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return new UserListEnumerator(this);
        }

        #endregion
    }

    /// <summary>
    /// class represents a clearance user
    /// </summary>
    public class User
    {
        private int idField, primaryCompanyIdField;
        private int agencyIdField;
        private string usernameField, notes, segmentation;
        private bool activeField;
        private RoleList roleListField;

        /// <summary>
        /// Gets or sets the id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return idField; }
            set { idField = value; }
        }
        
        /// <summary>
        /// Gets or sets the id of the agency associated with the user, primarily used for remote agency role
        /// </summary>
        [XmlElement("agency_id")]
        public int AgencyId
        {
            get { return agencyIdField; }
            set { agencyIdField = value; }
        }

        /// <summary>
        /// Gets or sets the id of the primary company the user is of
        /// </summary>
        [XmlElement("primary_company")]
        public int PrimaryCompanyId
        {
            get { return primaryCompanyIdField; }
            set { primaryCompanyIdField = value; }
        }

        /// <summary>
        /// Gets or sets the username
        /// </summary>
        [XmlElement("username")]
        public string Username
        {
            get { return usernameField; }
            set { usernameField = value; }
        }

        /// <summary>
        /// Gets or sets the active flag
        /// </summary>
        [XmlElement("active")]
        public bool Active
        {
            get { return activeField; }
            set { activeField = value; }
        }

        /// <summary>
        /// Gets or sets the notes
        /// </summary>
        [XmlElement("notes")]
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }
        
        /// <summary>
        /// Gets or sets the notes
        /// </summary>
        [XmlElement("segmentation")]
        public string Segmentation
        {
            get { return segmentation; }
            set { segmentation = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public RoleList RoleList
        {
            get { return roleListField; }
            set { roleListField = value; }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class UserListEnumerator : IEnumerator
    {
        private UserList slist;
        private int index = -1;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        public UserListEnumerator(UserList list)
        {
            slist = list;
        }

        #region IEnumerator Members

        /// <summary>
        /// 
        /// </summary>
        public object Current
        {
            get
            {
                if (index > -1)
                    return
                      slist[index];
                else
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            index++;
            if (index < slist.Count)
                return true;
            else
                return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Reset()
        {
            index = -1;
        }

        #endregion
    }
}
