using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("street_suffix_list")]
    public class StreetSuffixList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("street_suffixes"), XmlArrayItem("street_suffix", typeof(StreetSuffix))]
        public StreetSuffix[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class StreetSuffix
    {
        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("suffix")]
        public string Suffix;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("common_abbreviation")]
        public string CommonAbbreviation;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("approved_abbreviation")]
        public string ApprovedAbbreviation;


    }
}
