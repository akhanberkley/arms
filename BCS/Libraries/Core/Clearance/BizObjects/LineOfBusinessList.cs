using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("line_of_business_list")]
    public class LineOfBusinessList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_number")]
        public string CompanyNumber;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("agency_id")]
        public int AgencyId;

        /// <summary>
        /// 
        /// </summary>
        [XmlArray("line_of_businesses"), XmlArrayItem("line_of_business", typeof(LineOfBusiness))]
        public LineOfBusiness[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class LineOfBusiness
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("code")]
        public string Code;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("description")]
        public string Description;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("line")]
        public string Line;
    }


}
