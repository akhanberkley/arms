using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("person_list")]
    public class PersonList
    {

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("agency_id")]
        public int AgencyId;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("line_of_business_id")]
        public int LineOfBusinessId;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("underwriting_role_id")]
        public int UnderwriterRoleId;        

        /// <summary>
        /// 
        /// </summary>
        [XmlArray("persons"), XmlArrayItem("person", typeof(Person))]
        public Person[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class Person
    {
        private int idField, companyNumberIdField;
        private string userNameField, fullNameField, emailAddressField, initialsField;
        private bool isPrimaryField, activeField;
        private DateTime retirementDateField;

        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return idField; }
            set { idField = value; }
        }

        /// <summary>
        /// Gets or sets the company number id
        /// </summary>
        [XmlElement("company_number_id")]
        public int CompanyNumberId
        {
            get { return companyNumberIdField; }
            set { companyNumberIdField = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("user_name")]
        public string UserName
        {
            get { return userNameField; }
            set { userNameField = value; }
        }

        /// <summary>
        /// Gets or sets the Full name
        /// </summary>
        [XmlElement("full_name")]
        public string FullName
        {
            get { return fullNameField; }
            set { fullNameField = value; }
        }

        /// <summary>
        /// Gets or sets the Email Address
        /// </summary>
        [XmlElement("email_address")]
        public string EmailAddress
        {
            get { return emailAddressField; }
            set { emailAddressField = value; }
        }

        /// <summary>
        /// Gets or sets the primary flag
        /// </summary>
        [XmlElement("is_primary")]
        public bool IsPrimary
        {
            get { return isPrimaryField; }
            set { isPrimaryField = value; }
        }

        /// <summary>
        /// Gets or sets the Initials
        /// </summary>
        [XmlElement("initials")]
        public string Initials
        {
            get { return initialsField; }
            set { initialsField = value; }
        }
        /// <summary>
        /// Gets or sets the active flag
        /// </summary>
        [XmlElement("active")]
        public bool Active
        {
            get { return activeField; }
            set { activeField = value; }
        }
        
        /// <summary>
        /// Gets or sets the retirement date
        /// </summary>
        [XmlElement("retirement_date")]
        public DateTime RetirementDate
        {
            get { return retirementDateField; }
            set { retirementDateField = value; }
        }
    }
}
