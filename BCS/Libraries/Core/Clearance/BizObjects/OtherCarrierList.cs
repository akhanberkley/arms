using System;
using System.Xml.Serialization;
namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("other_carrier_list")]
    public class OtherCarrierList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("other_carriers"), XmlArrayItem("other_carrier", typeof(OtherCarrier))]
        public OtherCarrier[] List;
    }
    /// <summary>
    /// serializable class OtherCarrier
    /// </summary>
    [XmlRoot("other_carrier")]
    public class OtherCarrier
    {
        /// <summary>
        /// other carrier id
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// other carrier code
        /// </summary>
        [XmlElement("code")]
        public string Code;

        /// <summary>
        /// other carrier description
        /// </summary>
        [XmlElement("description")]
        public string Description;
    }
}
