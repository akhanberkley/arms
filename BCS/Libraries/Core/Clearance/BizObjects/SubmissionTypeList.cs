using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// list of submission types
    /// </summary>
    [XmlRoot("submission_type_list")]
    public class SubmissionTypeList
    {
        /// <summary>
        /// list of submission types
        /// </summary>
        [XmlArray("submission_types"),XmlArrayItem("submission_type",typeof(SubmissionType))]
        public SubmissionType[] List;
    }

    /// <summary>
    /// serializable class Submission Type
    /// </summary>    
    public class SubmissionType
    {
        /// <summary>
        /// submission type id
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// submission type name
        /// </summary>
        [XmlElement("type_name")]
        public string TypeName;

        /// <summary>
        /// abbreviation
        /// </summary>
        [XmlElement("abbr")]
        public string Abbreviation;
    }
}
