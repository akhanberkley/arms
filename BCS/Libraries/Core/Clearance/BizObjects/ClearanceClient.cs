using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    public struct ClearanceClient
    {
        /// <summary>
        /// Id
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// CompanyNumber
        /// </summary>
        [XmlElement("company_number_id")]
        public int CompanyNumberId;


        /// <summary>
        /// ClientCore Client Id
        /// </summary>
        [XmlElement("clientcore_client_id")]
        public long ClientCoreClientId;

        /// <summary>
        /// client info from client core
        /// </summary>
        [XmlElement("clientcore_client")]
        public BTS.ClientCore.Wrapper.Structures.Info.ClientInfo ClientCoreClient;

        /// <summary>
        /// SIC Code Id
        /// </summary>
        [XmlElement("sic_code_id")]
        public int SICCodeId;

        /// <summary>
        /// SIC Code
        /// </summary>
        [XmlElement("sic_code")]
        public string SICCode;

        /// <summary>
        /// SIC Code Id description
        /// </summary>
        [XmlElement("sic_code_description")]
        public string SICCodeDescription;

        /// <summary>
        /// NAICS Code Id
        /// </summary>
        [XmlElement("naics_code_id")]
        public int NAICSCodeId;

        /// <summary>
        /// NAICS Code
        /// </summary>
        [XmlElement("naics_code")]
        public string NAICSCode;

        ///<summary>
        ///NAICS Code Description
        ///</summary>
        [XmlElement("naics_code_description")]
        public string NAICSCodeDescription;

        /// <summary>
        /// Business Description
        /// </summary>
        [XmlElement("business_description")]
        public string BusinessDescription;

        /// <summary>
        /// Business Description
        /// </summary>
        [XmlElement("phone_number")]
        public string PhoneNumber;

        /// <summary>
        /// 
        /// </summary>
        [XmlArray("company_number_attributes"), XmlArrayItem("company_number_attribute", typeof(ClientCompanyNumberAttributeValue))]
        public ClientCompanyNumberAttributeValue[] CompanyNumberAttributes;

        /// <summary>
        /// 
        /// </summary>
        [XmlArray("company_number_codes"), XmlArrayItem("company_number_code", typeof(ClientCompanyNumberCode))]
        public ClientCompanyNumberCode[] CompanyNumberCodes;

        /// <summary>
        ///     
        /// </summary>
        [XmlArray("class_codes"), XmlArrayItem("class_code", typeof(ClientClassCode))]
        public ClientClassCode[] ClientClassCodes;

    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public struct ClientClassCode
    {
        private int id, clientId, classCodeId;

        private ClassCode classCode;

        public ClassCode ClassCode
        {
            get { return classCode; }
            set { classCode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("class_code_id")]
        public int ClassCodeId
        {
            get { return classCodeId; }
            set { classCodeId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("client_id")]

        public int ClientId
        {
            get { return clientId; }
            set { clientId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string description;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("class_code_description")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public struct ClientCompanyNumberAttributeValue
    {
        /// <summary>
        /// Attribute Id
        /// </summary>
        [XmlElement("attribute_id")]
        public int AttributeId;

        /// <summary>
        /// Attribute Value
        /// </summary>
        [XmlElement("attribute_value")]
        public string AttributeValue;
    }

    /// <summary>
    /// 
    /// </summary>
    public struct ClientCompanyNumberCode
    {
        /// <summary>
        /// Company Number Code Id
        /// </summary>
        [XmlElement("companynumber_code_id")]
        public int CompanyNumberCodeId;
    }
}
