using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    [XmlRoot("link_list")]
    public class LinkList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("links"), XmlArrayItem("link", typeof(Link))]
        public Link[] List;
    }

    public class Link
    {
        private int idField, companyIdField, displayOrderField;

        public int DisplayOrder
        {
            get { return displayOrderField; }
            set { displayOrderField = value; }
        }
        private string urlField, descriptionField;

        [XmlElement("id")]
        public int Id
        {
            get { return idField; }
            set { idField = value; }
        }

        [XmlElement("company_id")]
        public int CompanyId
        {
            get { return companyIdField; }
            set { companyIdField = value; }
        }

        [XmlElement("url")]
        public string Url
        {
            get { return urlField; }
            set { urlField = value; }
        }

        [XmlElement("description")]
        public string Description
        {
            get { return descriptionField; }
            set { descriptionField = value; }
        }
    }
}
