using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("suffix_list")]
    public class SuffixList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("suffixes"), XmlArrayItem("suffix", typeof(Suffix))]
        public Suffix[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class Suffix
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id;
        
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("suffix_title")]
        public string SuffixTitle;
    }
}
