using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("siccode_list")]
    public class SicCodeList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("siccodes"), XmlArrayItem("siccode", typeof(SicCode))]
        public SicCode[] List;
    }
    /// <summary>
    /// 
    /// </summary>
    public class SicCode
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("code")]
        public string Code;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("code_description")]
        public string CodeDescription;
    }
}
