using System;
using System.Xml.Serialization;


namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// class CodeTypes for a company
    /// </summary>
    [XmlRoot("code_type_list")]
    public class CodeTypeList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_number")]
        public string CompanyNumber;

        /// <summary>
        /// list of code types
        /// </summary>
        [XmlArray("code_types"),XmlArrayItem("code_type",typeof(CodeType))]
        public CodeType[] List;
    }

    /// <summary>
    /// represents a code type
    /// </summary>
    public class CodeType
    {
        private int _id, _companyNumberId, _defaultCompanyNumberCodeId;
        private short _displayOrder;
        private string _codeName;
        private bool _clientSpecific, _required, _visibleInLOBGridSection,_classCodeSpecific;

        /// <summary>
        /// UI specific indication whether it should appear in LOBGrid section of the submission page.
        /// </summary>
        public bool VisibleInLOBGridSection
        {
            get { return _visibleInLOBGridSection; }
            set { _visibleInLOBGridSection = value; }
        }

        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        /// <summary>
        /// company number id
        /// </summary>
        [XmlElement("company_number_id")]
        public int CompanyNumberId
        {
            get { return _companyNumberId; }
            set { _companyNumberId = value; }
        }

        /// <summary>
        /// default company number code id
        /// </summary>
        [XmlElement("default_company_number_code_id")]
        public int DefaultCompanyNumberCodeId
        {
            get { return _defaultCompanyNumberCodeId; }
            set { _defaultCompanyNumberCodeId = value; }
        }

        /// <summary>
        /// display order
        /// </summary>
        [XmlElement("display_order")]
        public short DisplayOrder
        {
            get { return _displayOrder; }
            set { _displayOrder = value; }
        }

        /// <summary>
        /// code name
        /// </summary>
        [XmlElement("code_name")]
        public string CodeName
        {
            get { return _codeName; }
            set { _codeName = value; }
        }

        /// <summary>
        /// code name
        /// </summary>
        [XmlElement("client_specific")]
        public bool ClientSpecific
        {
            get { return _clientSpecific; }
            set { _clientSpecific = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Required
        {
            get { return _required; }
            set { _required = value; }
        }
        /// <summary>
        /// code name
        /// </summary>
        [XmlElement("class_code_specific")]
        public bool ClassCodeSpecific
        {
            get { return _classCodeSpecific; }
            set { _classCodeSpecific = value; }
        }
    }
}
