using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{   
    /// <summary>
    /// Response
    /// </summary>
    [XmlRoot("response")]    
    public class Response
    {
        /// <summary>
        /// message
        /// </summary>
        [XmlElement("message")]
        public string Message;

        /// <summary>
        /// status
        /// </summary>
        [XmlElement("status")]
        public string Status;

        /// <summary>
        /// stack trace
        /// </summary>
        [XmlElement("stack_trace")]
        public string StackTrace;

        /// <summary>
        /// any returnable value
        /// </summary>
        [XmlElement("value", Namespace="")]
        public object Value;
    }

    /// <summary>
    /// 
    /// </summary>
    public class PagedResponse : Response
    { /// <summary>
        /// 
        /// </summary>
        [XmlElement("page_no")]
        public int PageNo;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("page_size")]
        public int PageSize;

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("has_next_page")]
        public bool HasNextPage;
    }
}
