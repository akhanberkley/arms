﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{

    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("submission_status_history_list")]
    public class SubmissionStatusHistoryList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_number")]
        public string CompanyNumber;

        /// <summary>
        /// list of agencies for the compnay number
        /// </summary>
        [XmlArray("submission_status_history"), XmlArrayItem("submission_status_history", typeof(SubmissionStatusHistory))]
        public SubmissionStatusHistory[] List;
    }
    public class SubmissionStatusHistory
    {
        private int _id, _submissionid;

        private string _previousSubmissionStatus , _statusChangedBy;

        private DateTime _submissionStatusDate, _statusChangeDate ;

        private bool _active;

        /// <summary>
        /// id of the history record
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// submission ID
        /// </summary>
        [XmlElement("submission_id")]
        public int SubmissionID
        {
            get { return _submissionid; }
            set { _submissionid = value; }
        }

        /// <summary>
        /// previous submission status
        /// </summary>
        [XmlElement("previous_submission_status")]
        public string PreviousSubmissionStatus
        {
            get { return _previousSubmissionStatus; }
            set { _previousSubmissionStatus = value; }
        }

        /// <summary>
        /// submission status date
        /// </summary>
        [XmlElement("submission_status_date")]
        public DateTime SubmissionStatusDate
        {
            get { return _submissionStatusDate; }
            set { _submissionStatusDate = value; }
        }

        /// <summary>
        /// status change date
        /// </summary>
        [XmlElement("status_change_date")]
        public DateTime StatusChangeDate
        {
            get { return _statusChangeDate; }
            set { _statusChangeDate = value; }
        }


        /// <summary>
        /// status changed by
        /// </summary>
        [XmlElement("status_changed_by")]
        public string StatusChangeBy
        {
            get { return _statusChangedBy; }
            set { _statusChangedBy = value; }
        }



        /// <summary>
        /// is active 
        /// </summary>
        [XmlElement("active")]
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }


    }
}
