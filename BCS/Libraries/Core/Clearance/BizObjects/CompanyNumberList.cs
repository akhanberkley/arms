using System;
using System.Xml.Serialization;
using System.Collections;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("company_number_list")]
    public class CompanyNumberList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_name")]
        public string CompanyName;

        /// <summary>
        /// 
        /// </summary>
        [XmlArray("company_numbers"), XmlArrayItem("", typeof(CompanyNumber))]
        public CompanyNumber[] List;
    }

    /// <summary>
    /// seriazable class company number
    /// </summary>
    public class CompanyNumber
    {
        private int _id, _companyId;
        private string _companyNumber;

        /// <summary>
        /// company number id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// company id
        /// </summary>
        [XmlElement("company_id")]
        public int CompanyId
        {
            get { return _companyId; }
            set { _companyId = value; }
        }
        /// <summary>
        /// company number
        /// </summary>
        [XmlElement("company_number")]
        public string PropertyCompanyNumber
        {
            get { return _companyNumber; }
            set { _companyNumber = value; }
        }
    }
}
