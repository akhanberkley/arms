using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("company_number_lob_grid_list")]
    public class CompanyNumberLOBGridList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("company_number_lob_grids"), XmlArrayItem("company_number_lob_grid", typeof(CompanyNumberLOBGrid))]
        public CompanyNumberLOBGrid[] List;
    }
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class CompanyNumberLOBGrid
    {
        private int id;
        private string code, description;
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("code")]
        public string Code
        {
            get { return code; }
            set { code = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("description")]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("company_number_id")]
        public int CompanyNumberId;
    }
}
