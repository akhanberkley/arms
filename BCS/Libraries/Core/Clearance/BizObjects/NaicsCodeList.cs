﻿using System;
using System.Xml.Serialization;


namespace BCS.Core.Clearance.BizObjects
{

    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("naicscode_list")]
    public class  NaicsCodeList
    {
        ///<summary>
        ///
        ///</summary>
        [XmlArray("naicscodes"), XmlArrayItem("naicscode", typeof(NaicsCode))]
        public NaicsCode[] List;

    }

    /// <summary>
    /// 
    /// </summary>

    public class NaicsCode
    {
        ///<summary>
        ///
        ///</summary>
        [XmlElement("id")]
        public int Id;


        ///<summary>
        ///
        ///</summary>
        [XmlElement("code")]
        public string Code;


        ///<summary>
        ///
        ///</summary>
        [XmlElement("code_description")]
        public string CodeDescription;
    }
}
