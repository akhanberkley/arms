using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("submission_status_reason_list")]
    public class SubmissionStatusReasonList
    {
        /// <summary>
        /// reasons for status
        /// </summary>
        [XmlElement("id")]        
        public string SubmissionStatusCode;

        /// <summary>
        /// 
        /// </summary>
        [XmlArray("submission_status_reasons"), XmlArrayItem("submission_status_reason", typeof(SubmissionStatusReason))]
        public SubmissionStatusReason[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("submission_status_reason")]
    public class SubmissionStatusReason
    {
        /// <summary>
        /// submission status id
        /// </summary>
        [XmlElement("id")]
        public int Id;
        
        private string reasonCode;
        /// <summary>
        /// submission status code
        /// </summary>
        [XmlElement("reason_code")]
        public string ReasonCode
        {
            get { return reasonCode; }
            set { reasonCode = value; }
        }
    }
}
