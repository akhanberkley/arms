using System;
using System.Xml.Serialization;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("contact_list")]
    public class ContactList
    {
        /// <summary>
        /// list of agencies for the compnay number
        /// </summary>
        [XmlArray("contacts"), XmlArrayItem("contact", typeof(Contact))]
        public Contact[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    public class Contact
    {
        private int idField, agencyIdField;
        private string nameField, primaryPhoneField, primaryPhoneExtensionField, secondaryPhoneField, secondaryPhoneExtensionField, faxField, emailField;
        private DateTime dobField, retDateField;

        public DateTime RetirementDate
        {
            get { return retDateField; }
            set { retDateField = value; }
        }

        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return idField; }
            set { idField = value; }
        }
        
        /// <summary>
        /// agency the contact belongs to
        /// </summary>
        [XmlElement("agency_id")]
        public int AgencyId
        {
            get { return agencyIdField; }
            set { agencyIdField = value; }
        }



        /// <summary>
        /// name
        /// </summary>
        [XmlElement("name")]
        public string Name
        {
            get { return nameField; }
            set { nameField = value; }
        }

        /// <summary>
        /// primary phone
        /// </summary>
        [XmlElement("primary_phone")]
        public string PrimaryPhone
        {
            get { return primaryPhoneField; }
            set { primaryPhoneField = value; }
        }

        /// <summary>
        /// primary phone extension
        /// </summary>
        [XmlElement("primary_phone_extension")]
        public string PrimaryPhoneExtension
        {
            get { return primaryPhoneExtensionField; }
            set { primaryPhoneExtensionField = value; }
        }

        /// <summary>
        /// secondary phone
        /// </summary>
        [XmlElement("secondary_phone")]
        public string SecondaryPhone
        {
            get { return secondaryPhoneField; }
            set { secondaryPhoneField = value; }
        }

        /// <summary>
        /// secondary phone extension
        /// </summary>
        [XmlElement("secondary_phone_extension")]
        public string SecondaryPhoneExtension
        {
            get { return secondaryPhoneExtensionField; }
            set { secondaryPhoneExtensionField = value; }
        }

        /// <summary>
        /// fax
        /// </summary>
        [XmlElement("fax")]
        public string Fax
        {
            get { return faxField; }
            set { faxField = value; }
        }

        /// <summary>
        /// email
        /// </summary>
        [XmlElement("email")]
        public string Email
        {
            get { return emailField; }
            set { emailField = value; }
        }

        /// <summary>
        /// date of birth
        /// </summary>
        [XmlElement("dob")]
        public DateTime DateOfBirth
        {
            get { return dobField; }
            set { dobField = value; }
        }
    }
}