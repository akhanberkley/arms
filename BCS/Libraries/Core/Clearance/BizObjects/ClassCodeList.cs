using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace BCS.Core.Clearance.BizObjects
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("class_code_list")]
    public class ClassCodeList
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("classcodes"), XmlArrayItem("classcode", typeof(ClassCode))]
        public ClassCode[] List;
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ClassCode
    {
       
        public ClassCode()
        {
            HazardGrades = new List<HazardGrade>();
            ClassCodeCompanyNumberCodes = new List<ClassCodeCompanyNumberCode>();
        }

        private int _id, _siccodeId, _companyNumberId, _naicscodeId;
        private string _codeValue, _description;       

        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /// <summary>
        /// company number id
        /// </summary>
        [XmlElement("company_number_id")]
        public int CompanyNumberId
        {
            get { return _companyNumberId; }
            set { _companyNumberId = value; }
        }

        /// <summary>
        /// agency the contact belongs to
        /// </summary>
        [XmlElement("sic_code_id")]
        public int SicCodeId
        {
            get { return _siccodeId; }
            set { _siccodeId = value; }
        }

        /// <summary>
        /// CodeValue
        /// </summary>
        [XmlElement("code_value")]
        public string CodeValue
        {
            get { return _codeValue; }
            set { _codeValue = value; }
        }

        /// <summary>
        /// description
        /// </summary>
        [XmlElement("description")]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [XmlArray("hazard_grades"), XmlArrayItem("hazard_grade", typeof(HazardGrade))]
        public List<HazardGrade> HazardGrades;

        [XmlArray("class_code_attributes"), XmlArrayItem("class_code_attribute", typeof(ClassCodeCompanyNumberCode))]
        public List<ClassCodeCompanyNumberCode> ClassCodeCompanyNumberCodes;
        /// <summary>
        /// Naics Code Id
        /// </summary>
        [XmlElement("naics_code_id")]
        public int NaicsCodeId
        {
           get { return _naicscodeId;}
           set { _naicscodeId = value;}
        }

    }

    [Serializable]
    public class HazardGrade
    {
        
        private string _hazardGradeTypeCode, _hazardGradeValue;

        [XmlElement("hazard_grade_type_code")]
        public string HazardGradeTypeCode
        {
            get { return _hazardGradeTypeCode; }
            set { _hazardGradeTypeCode = value; }
        }

        [XmlElement("hazard_grade_value")]
        public string HazardGradeValue
        {
            get { return _hazardGradeValue; }
            set { _hazardGradeValue = value; }
        }
    }

    [Serializable]
    public class ClassCodeCompanyNumberCode
    {
        private string _code, _codeDescription, _codeTypeName;
        private int _codeTypeId, _codeId;

        [XmlElement("attribute_value")]
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        [XmlElement("attribute_description")]
        public string CodeDescription
        {
            get { return _codeDescription; }
            set { _codeDescription = value; }
        }

        [XmlElement("attribute_type_Name")]
        public string CodeTypeName
        {
            get { return _codeTypeName; }
            set { _codeTypeName = value; }
        }

        [XmlIgnore()]
        public int CodeId
        {
            get { return _codeId; }
            set { _codeId = value; }
        }

        [XmlIgnore()]
        public int CodeTypeId
        {
            get { return _codeTypeId; }
            set { _codeTypeId = value; }
        }
    }

}
