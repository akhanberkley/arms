//using System;
//using System.Text.RegularExpressions;
//using System.Xml;
//using System.Data.SqlTypes;
//using System.Configuration;
//using BTS.ClientCore.WS;
//using BTS.ClientCore.WS.SF;

//using structures = BTS.ClientCore.Wrapper.Structures;
//using System.Reflection;
//using System.Data.SqlClient;

//using BCS.DAL;
//using System.Linq;

//namespace BCS.Core.Clearance
//{
//    /// <summary>
//    /// common methods
//    /// </summary>
//    public class CommonEF
//    {

//        internal static Lookups GetLookupManager()
//        {
//            return new Lookups(DSN);
//        }

//        //private static BTS.ClientCore.WS.ClientCoreWebServiceService _ccws = null;
//        internal static BTS.ClientCore.WS.ClientCoreWebServiceService GetClientCoreWebServiceService(string endPointURL)
//        {
//            return new ClientCoreWebServiceHelper(endPointURL);
//            //return _ccws;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        internal static string DSN
//        {
//            get
//            {
//                object o = ConfigurationManager.ConnectionStrings["DSN"];
//                if (o != null)
//                {
//                   return o.ToString();
//                }
//                else
//                    throw new ApplicationException("Invalid DSN");
//            }
//        }

        

//        internal static BizObjects.Company BuildCompany(string companyNumber)
//        {
//            Biz.DataManager dm = GetDataManager();
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
//            Biz.Company company = dm.GetCompany();

//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.CompanyParameter.Columns.CompanyID, company.Id);
//            Biz.CompanyParameterCollection parameters = dm.GetCompanyParameterCollection();

//            if (company == null)
//                throw new ArgumentException("Company does not exist.");

//            BizObjects.Company basic = new BCS.Core.Clearance.BizObjects.Company();

//            if (parameters != null && parameters.Count > 0)
//            {
//                //basic.ServiceType = (int)company.CompanyParameters[0].SearchFilterService;
//                basic.SearchFilters = BuildSearchFilter(parameters[0]);
//                basic.ServiceType = (int)parameters[0].SearchFilterService;
//                if (basic.ServiceType == 0 && parameters[0].DietSearch)
//                {
//                    basic.ServiceType = 2;
//                    basic.dietSearchOptions = parameters[0].DietSearchOptions;
//                }

//                basic.filterGetClientInfo = (bool)parameters[0].FilterGetClientInfo;
//                basic.filterPerformClientSearch = (bool)parameters[0].FilterPerformClientSearch;
//            }

//            basic.AllowSubmissionsOnError = company.AllowSubmissionsOnError.IsNull ? false : company.AllowSubmissionsOnError.Value;
//            basic.AnalystRoleId = company.AnalystRoleId.IsNull ? 0 : company.AnalystRoleId.Value;
//            basic.ClientCoreClientType = company.ClientCoreClientType;
//            basic.ClientCoreEndPointUrl = company.ClientCoreEndPointUrl;
//            basic.ClientCoreLocalId = company.ClientCoreLocalId;
//            basic.ClientCoreUsername = company.ClientCoreUsername;
//            basic.ClientCorePassword = company.ClientCorePassword;
//            basic.ClientCoreSystemCd = company.ClientCoreSystemCd;
//            basic.ClientCoreCorpId = company.ClientCoreCorpId;
//            basic.ClientCoreCorpCode = company.ClientCoreCorpCode;
//            basic.ClientCoreOwner = company.ClientCoreOwner;
//            basic.ClientCoreLocalId = company.ClientCoreLocalId;
//            basic.CompanyInitials = company.CompanyInitials;
//            basic.CompanyName = company.CompanyName;
//            basic.DSIK = company.DSIK;
//            basic.Id = company.Id;
//            basic.LogoUrl = company.LogoUrl;
//            basic.ThemeStyleSheet = company.ThemeStyleSheet;
//            basic.UnderwriterRoleId = company.UnderwriterRoleId.IsNull ? 0 : company.UnderwriterRoleId.Value;            

//            return basic;

//        }

//        internal static BTS.ClientCore.WS.SF.processingOption[] BuildSearchFilter(Biz.CompanyParameter companyParameter)
//        {
//            if (string.IsNullOrEmpty(companyParameter.SearchFilter)) return null;

//            System.Collections.Generic.List<BTS.ClientCore.WS.SF.processingOption> searchFilters = new System.Collections.Generic.List<BTS.ClientCore.WS.SF.processingOption>();   

//            foreach(string item in companyParameter.SearchFilter.Split('|'))
//            {
//                searchFilters.Add((BTS.ClientCore.WS.SF.processingOption)Enum.Parse(typeof(BTS.ClientCore.WS.SF.processingOption), item));
//            }

//            if (searchFilters.Count < 1) return null;

//            return searchFilters.ToArray();
//        }

//        internal static XmlNode RemoveTypeAttributes(XmlNode doc)
//        {
//            foreach (XmlNode node in doc.ChildNodes)
//            {
//                if (node.Attributes != null)
//                {
//                    node.Attributes.RemoveAll();
//                    //foreach (XmlAttribute attr in node.Attributes)
//                    //{
//                    //    if (attr.Name == "xsi:type" || attr.Name == "xmlns:xsi" || attr.Name == "xmlns:xsd")
//                    //    {
//                    //        node.Attributes.Remove(attr);
//                    //        //break;
//                    //    }

//                    //}
//                    RemoveTypeAttributes(node);
//                }
//            }
//            return doc;
//        }

//        internal static XmlNode RemoveTypeAttributes(XmlNode doc, params string[] exceptions)
//        {
//             System.Collections.Specialized.StringCollection exs = new System.Collections.Specialized.StringCollection();
//             System.Collections.Specialized.StringCollection toBeRemoved = new System.Collections.Specialized.StringCollection();

//            foreach (string str in exceptions)
//            {
//                exs.Add(str);
//            }
//            foreach (XmlNode node in doc.ChildNodes)
//            {
//                if (node.Attributes != null)
//                {
//                    foreach(XmlAttribute attr in node.Attributes)
//                    {
//                        if (exs.Contains(attr.Name))
//                            continue;
//                        toBeRemoved.Add(attr.Name);

//                    }
//                    foreach (string tober in toBeRemoved)
//                    {
//                        node.Attributes.RemoveNamedItem(tober);
//                    }
//                    RemoveTypeAttributes(node,exceptions);
//                }
//            }
//            return doc;
//        }

//        internal static string RemoveTypeAttributes(string xml)
//        {
//            try
//            {
//                XmlDocument doc = new XmlDocument();
//                doc.LoadXml(xml);
//                foreach (XmlNode node in doc.ChildNodes)
//                {
//                    if (node.Attributes != null)
//                    {
//                        node.Attributes.RemoveAll();
//                        RemoveTypeAttributes(node);
//                    }
//                }
//                return doc.InnerXml;
//            }
//            catch (Exception ex)
//            {
//                BTS.LogFramework.LogCentral.Current.LogWarn(xml, ex);
//                throw;
//            }
//        }

//        internal static string RemoveTypeAttributes(string xml, params string[] exceptions)
//        {
//            try
//            {
//                System.Collections.Specialized.StringCollection exs = new System.Collections.Specialized.StringCollection();
//                System.Collections.Specialized.StringCollection toBeRemoved = new System.Collections.Specialized.StringCollection();

//                foreach (string str in exceptions)
//                {
//                    exs.Add(str);
//                }

//                XmlDocument doc = new XmlDocument();
//                doc.LoadXml(xml);
//                foreach (XmlNode node in doc.ChildNodes)
//                {
//                    if (node.Attributes != null)
//                    {
//                        foreach (XmlAttribute attr in node.Attributes)
//                        {
//                            if (exs.Contains(attr.Name))
//                                continue;
//                            toBeRemoved.Add(attr.Name);

//                        }
//                        foreach (string tober in toBeRemoved)
//                        {
//                            node.Attributes.RemoveNamedItem(tober);
//                        }
//                        RemoveTypeAttributes(node, exceptions);
//                    }
//                }
//                return doc.InnerXml;
//            }
//            catch (Exception ex)
//            {
//                BTS.LogFramework.LogCentral.Current.LogWarn(xml, ex);
//                throw;
//            }
//        }

//        /// <summary>
//        /// builds a xml input string for user info to the client core
//        /// </summary>
//        /// <param name="company"></param>
//        /// <returns></returns>
//        public static string GetCommonBasicXml(BizObjects.Company company)
//        {
//            if (company == null)
//                throw new NullReferenceException("Company parameter cannot be null");
//            BTS.ClientCore.Wrapper.Structures.TFGGlobalBasicXMLDO basic =
//                new BTS.ClientCore.Wrapper.Structures.TFGGlobalBasicXMLDO();
//            basic.LocalId = company.ClientCoreLocalId;
//            basic.Username = company.ClientCoreUsername;
//            basic.Password = company.ClientCorePassword;
//            basic.SystemCd = company.ClientCoreSystemCd;

//            string xml = BCS.Core.XML.Serializer.SerializeAsXml(basic);

//            // TODO :: analyze if needed the code below
//            // -----
//            XmlDocument doc = new XmlDocument();
//            doc.LoadXml(xml);
//            XmlNode node = RemoveTypeAttributes(doc);
//            xml = node.InnerXml;
//            // -----
//            return xml;
//        }

//        /// <summary>
//        /// builds xml for client core
//        /// </summary>
//        public static string BuildClientSearchXml(object client)
//        {
//            string xml = XML.Serializer.SerializeAsXml(client);
//            return xml;
//        }        

//        private static int[] GetSubmissionCodeTypes(int submissionid, out string names)
//        {
//            Biz.DataManager dm = GetDataManager();
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.SubmissionCompanyNumberCode.Columns.SubmissionId, submissionid);
//            Biz.SubmissionCompanyNumberCodeCollection codes = dm.GetSubmissionCompanyNumberCodeCollection();
//            int[] ids = new int[codes.Count];
//            for (int i = 0; i < codes.Count; i++)
//            {
//                ids[i] = codes[i].CompanyNumberCodeId;
//            }

//            // gather codes for "Policy Symbol'

//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(
//                Biz.JoinPath.CompanyNumberCode.Columns.Id, ids, OrmLib.MatchType.In).And(
//                Biz.JoinPath.CompanyNumberCode.CompanyNumberCodeType.Columns.CodeName, "Policy Symbol");
//            Biz.CompanyNumberCodeCollection aa = dm.GetCompanyNumberCodeCollection();

//            //if (aa.Count > 1)
//            //    throw new Exception("Inconsistent data.");
//            if (aa.Count == 0)
//                names = string.Empty;
//            else
//                names = aa[0].Code;


//            return ids;
//        }

//        private static Biz.LineOfBusiness GetLineOfBusiness(int submissionid)
//        {
//            Biz.DataManager dm = GetDataManager();
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.SubmissionLineOfBusiness.Columns.SubmissionId, submissionid);
//            Biz.SubmissionLineOfBusiness slob = dm.GetSubmissionLineOfBusiness();

//            if (slob == null)
//                return null;

//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, slob.LineOfBusinessId);
//            return dm.GetLineOfBusiness();
//        }

      

//        #region Sql Data Type Helpers

//        /// <summary>
//        /// helper method to convert an object to sql int type
//        /// </summary>
//        /// <param name="o"></param>
//        /// <returns></returns>
//        public static SqlInt32 ConvertToSqlInt32(object o)
//        {
//            try
//            {
//                SqlInt32 r = (SqlInt32)Convert.ToInt32(o);
//                return r;
//            }
//            catch (Exception ex)
//            {

//                throw ex;
//            }
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="o"></param>
//        /// <returns></returns>
//        public static SqlInt64 ConvertToSqlInt64(object o)
//        {
//            try
//            {
//                SqlInt64 r = (SqlInt64)Convert.ToInt64(o);
//                return r;
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        /// <summary>
//        /// helper method to convert an object to sql date time
//        /// </summary>
//        /// <param name="o"></param>
//        /// <returns></returns>
//        public static SqlDateTime ConvertToSqlDateTime(object o)
//        {
//            try
//            {
//                if (o == null || o.ToString().Length == 0)
//                    return SqlDateTime.Null;
//                if (Convert.ToDateTime(o) == DateTime.MinValue)
//                    return SqlDateTime.Null;
//                SqlDateTime r = (SqlDateTime)Convert.ToDateTime(o);
//                return r;
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }
//        #endregion



//        internal static Biz.Agency VerifyAgency( int agencyid)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agencyid);
//            return dm.GetAgency();
//        }

//        internal static Biz.Agency VerifyAgency(string agencyid)
//        {
//            Biz.DataManager dm = GetDataManager();
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agencyid);
//            return dm.GetAgency();
//        }

//        internal static Biz.Agent VerifyAgent( int agentid)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agentid);
//            return dm.GetAgent();
//        }


//        internal static Contact VerifyContact( int contactid)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.Contact.Columns.Id, contactid);
//            return dm.GetContact();
//        }

//        internal static Biz.Agent VerifyAgent(string agentid)
//        {
//            Biz.DataManager dm = GetDataManager();
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agentid);
//            return dm.GetAgent();
//        }

//        internal static Biz.SubmissionStatus VerifySubmissionStatus( int statusid)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatus.Columns.Id, statusid);
//            return dm.GetSubmissionStatus();
//        }

//        internal static Biz.SubmissionStatus VerifySubmissionStatus(string statusid)
//        {
//            Biz.DataManager dm = GetDataManager();
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatus.Columns.Id, statusid);
//            return dm.GetSubmissionStatus();
//        }
//        internal static SubmissionStatusReason VerifySubmissionStatusReason( int statusreasonid)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatusReason.Columns.Id, statusreasonid);
//            return dm.GetSubmissionStatusReason();

//            using (
//        }

//        internal static Biz.CompanyNumber VerifyCompanyNumber( string companynumber)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companynumber);
//            return dm.GetCompanyNumber();
//        }

//        internal static Biz.Company VerifyCompany(string companyid)
//        {
//            Biz.DataManager dm = GetDataManager();
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, companyid);
//            return dm.GetCompany();
//        }

//        internal static Company VerifyCompany( int companyid)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.Company.Columns.Id, companyid);
//            return dm.GetCompany();
//        }

//        /// <summary>
//        /// helper method to verify submission token
//        /// </summary>       
//        internal static SubmissionToken VerifySubmissionToken(int tokenid)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.SubmissionToken.Columns.Id, tokenid);
//            return dm.GetSubmissionToken();
//        }

//        /// <summary>
//        /// method to verify submission type
//        /// </summary>      
//        internal static SubmissionType VerifySubmissionType(int typeid)
//        {
//            using (BCSContext bc = DataAccess.NewBCSContext())
//            {
//                SubmissionType type = bc.SubmissionType.Where(t => t.Id == typeid).SingleOrDefault();
//                return type;
//            }
//        }

//        internal static LineOfBusiness VerifyLineOfBusiness( int id)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.LineOfBusiness.Columns.Id, id);
//            return dm.GetLineOfBusiness();
//        }

//        internal static Biz.Person VerifyUnderWriter(Biz. int uid)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.Id, uid);
//            return dm.GetPerson();
//        }

//        internal static PolicySystem VerifyPolicySystem( int id)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.PolicySystem.Columns.Id, id);
//            return dm.GetPolicySystem();
//        }

//        internal static Biz.Person VerifyUnderWriter(int uid)
//        {
//            Biz.DataManager dm = GetDataManager();
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Person.Columns.Id, uid);
//            return dm.GetPerson();
//        }

//        internal static CompanyNumberCodeCollection VerifyCompanyNumberCode( int[] companynumbercodeids)
//        {
//            dm.QueryCriteria.Clear();
//            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.CompanyCodeTypeId, companynumbercodeids,OrmLib.MatchType.In);
//            return dm.GetCompanyNumberCodeCollection();            
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="date"></param>
//        /// <returns></returns>
//        public static string ConvertToClientCoreDateTime(object date)
//        {
//            System.Globalization.GregorianCalendar gcal = new System.Globalization.GregorianCalendar();
//            DateTime gdate = Convert.ToDateTime(date);
//            int com = gcal.GetYear(gdate) * 1000 + gcal.GetDayOfYear(gdate);
//            return com.ToString();
//        }

//        internal static string BuildAddClientXml(structures.Add.TFGClientXMLDO client)
//        {
//            string xml = BCS.Core.XML.Serializer.SerializeAsXml(client);
//            xml = CreateFullEndTags(xml);
//            XmlDocument doc = new XmlDocument();
//            doc.LoadXml(xml);
//            XmlNode node = RemoveTypeAttributes(doc);
//            xml = node.InnerXml;
//            xml = xml.Replace("corp_Id", "corp_Id ");
//            return xml;
//        }

//        private static string CreateFullEndTags(string xml)
//        {
//            Regex regex = new Regex("<[a-zA-Z-_\\d\\s]*/>");
//            MatchCollection mc = regex.Matches(xml);
//            foreach (Match match in mc)
//            {
//                string r = match.Value;
//                r = r.Substring(1, r.Length - 4);
//                r = string.Concat("<", r, ">", "</", r, ">");
//                xml = new Regex(match.Value).Replace(xml, r);
//            }
//            // TODO :: analyze if not needed. added while trying to add a client to client core (xml issues).
//            //xml = new Regex("\\r\\n").Replace(xml, string.Empty);
//            return xml;

//        }

//        internal static string BuildClientSearchByIdXml(string id, BCS.Core.Clearance.BizObjects.Company company)
//        {
//            structures.Info.TFGClientExportIDInfoDO client = new BTS.ClientCore.Wrapper.Structures.Info.TFGClientExportIDInfoDO();
//            client.corpID = company.ClientCoreCorpId;
//            client.EffectiveDate = ConvertToClientCoreDateTime(DateTime.Now);
//            client.ClientID = id;
//            client.SeqNbr = "1000";
//            string xml = XML.Serializer.SerializeAsXml(client);
//            return xml;
//        }
//        internal static bool IsNotNullOrEmpty(string s)
//        {
//            return !string.IsNullOrEmpty(s);
//        }

//        internal static string InvokeperformClientSearch(string companyNumber, BTS.ClientCore.Wrapper.Structures.Search.SearchClient client)
//        {
//            BizObjects.Company ucompany = BuildCompany(companyNumber);

//            if (!ucompany.filterPerformClientSearch)
//                ucompany.SearchFilters = null;

//            if (!string.IsNullOrEmpty(ucompany.dietSearchOptions) && ucompany.ServiceType==2)
//                client.FillData = ucompany.dietSearchOptions;

//            client.CorpID = ucompany.ClientCoreCorpId;
//            string basicXML = GetCommonBasicXml(ucompany);
//            string searchXML = Common.BuildClientSearchXml(client);
//            object searchService = null;
//            object objXML = null;

//            if (ucompany.ServiceType ==1)
//            {
//                searchService = new ClientCoreServiceHelperSF(ucompany.ClientCoreEndPointUrl);
//            }
//            //else if (IsNotNullOrEmpty(ucompany.DSIK))
//            //{
//            //    searchService = new ClientCoreServiceHelper(ucompany.DSIK, ucompany.ClientCoreEndPointUrl);
//            //}
//            else
//            {
//                searchService = new ClientCoreWebServiceHelper(ucompany.ClientCoreEndPointUrl);
//            }

//            Type serviceType = searchService.GetType();
//            MethodInfo searchMethod = serviceType.GetMethod("performClientSearch");

//            if (ucompany.ServiceType == 2)
//            {
//                objXML = serviceType.InvokeMember("performClientSearchLite", BindingFlags.InvokeMethod, null, searchService, new string[] { basicXML, searchXML });
//            }
//            else if (ucompany.ServiceType == 1)
//            {
//                objXML = serviceType.InvokeMember("performClientSearch", BindingFlags.InvokeMethod, null, searchService, new object[] { basicXML, searchXML, ucompany.SearchFilters });
//            }
//            else 
//            {
//                objXML = serviceType.InvokeMember("performClientSearch", BindingFlags.InvokeMethod, null, searchService, new string[] { basicXML, searchXML });
//            }
          
            

//            // TODO: probable logging here
//            if (objXML != null)
//                return objXML.ToString();
//            else return "<string>No Results Found with specified criteria: Please try again.</string>";
//        }

//        internal static string InvokeperformPortfolioSearch(string companyNumber, BTS.ClientCore.Wrapper.Structures.Search.SearchPortfolio client)
//        {
//            BizObjects.Company ucompany = BuildCompany(companyNumber);
//            string basicXML = GetCommonBasicXml(ucompany);

//            client.CorpId = ucompany.ClientCoreCorpId;
//            string searchXML = Common.BuildClientSearchXml(client);

//            object searchService;

//            //if (ucompany.ServiceType == 1)
//                searchService = new ClientCoreServiceHelperSF(ucompany.ClientCoreEndPointUrl);
//            //else
//            //    searchService = new ClientCoreServiceHelper(ucompany.DSIK, ucompany.ClientCoreEndPointUrl);
            
//            Type serviceType = searchService.GetType();
//            object objXML = serviceType.InvokeMember("performPortfolioSearch", BindingFlags.InvokeMethod, null, searchService, new string[] { basicXML, searchXML });

//            // TODO: probable logging here
//            if (objXML != null)
//                return objXML.ToString();
//            else return "<string>No Results Found with specified criteria: Please try again.</string>";
//        }

//        internal static string InvokegetClientInfo(string companyNumber, string id, bool fromSearchScreen)
//        {
//            BizObjects.Company ucompany = BuildCompany(companyNumber);

//            if (!ucompany.filterPerformClientSearch && fromSearchScreen)
//                ucompany.SearchFilters = null;
//            else if (!ucompany.filterGetClientInfo && !fromSearchScreen)
//                ucompany.SearchFilters = null;

//            string basicXML = GetCommonBasicXml(ucompany);
//            string infoXML = Common.BuildClientSearchByIdXml(id, ucompany);
//            object searchService = null;
//            object objXML = null;

//            if (ucompany.ServiceType == 1)
//            {
//                searchService = new ClientCoreServiceHelperSF(ucompany.ClientCoreEndPointUrl);
//            }
//            //else if (IsNotNullOrEmpty(ucompany.DSIK))
//            //{
//            //    searchService = new ClientCoreServiceHelper(ucompany.DSIK, ucompany.ClientCoreEndPointUrl);
//            //}
//            else
//            {
//                searchService = new ClientCoreWebServiceHelper(ucompany.ClientCoreEndPointUrl);
//            }

//            Type serviceType = searchService.GetType();
//            MethodInfo searchMethod = serviceType.GetMethod("getClientInfo");

//            if (ucompany.ServiceType != 1)
//            {
//                objXML = serviceType.InvokeMember("getClientInfo", BindingFlags.InvokeMethod, null, searchService, new string[] { basicXML, infoXML });
//            }
//            else
//            {
//                objXML = serviceType.InvokeMember("getClientInfo", BindingFlags.InvokeMethod, null, searchService, new object[] { basicXML, infoXML,ucompany.SearchFilters });
//            }

//            // TODO: probable logging here
//            if (objXML != null)
//                return objXML.ToString();
//            else return string.Empty;
//        }

//        internal static string InvokeimportClient(string companyNumber, string importXML)
//        {
//            BizObjects.Company ucompany = BuildCompany(companyNumber);
//            string basicXML = GetCommonBasicXml(ucompany);
//            object searchService;
//            if (ucompany.ServiceType == 1)
//            {
//                searchService = new ClientCoreServiceHelperSF(ucompany.ClientCoreEndPointUrl);
//            }
//            //else if (IsNotNullOrEmpty(ucompany.DSIK))
//            //{
//            //    searchService = new ClientCoreServiceHelper(ucompany.DSIK, ucompany.ClientCoreEndPointUrl);
//            //}
//            else
//            {
//                searchService = new ClientCoreWebServiceHelper(ucompany.ClientCoreEndPointUrl);
//            }
//            Type serviceType = searchService.GetType();
//            MethodInfo searchMethod = serviceType.GetMethod("importClient");
//            object objXML = serviceType.InvokeMember("importClient", BindingFlags.InvokeMethod, null, searchService, new string[] { basicXML, importXML });

//            // TODO: probable logging here
//            if (objXML != null)
//                return objXML.ToString();
//            else return string.Empty;
//        }



//        internal static string InvokeaddProspect(string companyNumber, string clientId, string policyNumber)
//        {
//            BizObjects.Company ucompany = BuildCompany(companyNumber);
//            string basicXML = string.Format("<root><clientId>{0}</clientId><policyNumber>{1}</policyNumber></root>", clientId, policyNumber);
//            object searchService;
//            searchService = new ClientCoreWebServiceHelper(ucompany.ClientCoreEndPointUrl);
//            Type serviceType = searchService.GetType();
//            MethodInfo searchMethod = serviceType.GetMethod("importClient");
//            object objXML = serviceType.InvokeMember("importClient",
//                BindingFlags.InvokeMethod, null, searchService, new object[] { basicXML, string.Empty });

//            // TODO: probable logging here
//            if (objXML != null)
//                return objXML.ToString();
//            else return string.Empty;
//        }

//        internal static string InvokeupdatePortfolio(string companyNumber, string id, string newName)
//        {
//            BizObjects.Company ucompany = BuildCompany(companyNumber);
//            string basicXML = GetCommonBasicXml(ucompany);
//            structures.Search.SearchedPortfolios lios = new structures.Search.SearchedPortfolios();
//            lios.Portfolios = new BTS.ClientCore.Wrapper.Structures.Search.SearchedPortfolio[1];
//            lios.Portfolios[0] = new BTS.ClientCore.Wrapper.Structures.Search.SearchedPortfolio();
//            lios.Portfolios[0].Id = id;
//            lios.Portfolios[0].Name = newName;
//            string importXML = XML.Serializer.SerializeAsXml(lios);

//            object searchService;

//            if (ucompany.ServiceType == 1)
//            {
//                searchService = new ClientCoreServiceHelperSF(ucompany.ClientCoreEndPointUrl);
//            }
//            //else if (IsNotNullOrEmpty(ucompany.DSIK))
//            //{
//            //    searchService = new ClientCoreServiceHelper(ucompany.DSIK, ucompany.ClientCoreEndPointUrl);
//            //}
//            else
//            {
//                searchService = new ClientCoreWebServiceHelper(ucompany.ClientCoreEndPointUrl);
//            }
//            Type serviceType = searchService.GetType();
//            MethodInfo searchMethod = serviceType.GetMethod("updatePortfolio");
            
//            object objXML = serviceType.InvokeMember("updatePortfolio", BindingFlags.InvokeMethod, null, searchService, new string[] { basicXML, importXML });

//            // TODO: probable logging here
//            if (objXML != null)
//                return objXML.ToString();
//            else return string.Empty;
//        }

//        public static serviceContext BuildContext(BizObjects.Company ucompany)
//        {
//            serviceContext context = new serviceContext();
            
//            context.localeId = ucompany.ClientCoreLocalId;
//            context.password = ucompany.ClientCorePassword;
//            context.systemCode = ucompany.ClientCoreSystemCd;
//            context.userid = Int32.Parse(ucompany.ClientCoreCorpId);
//            context.username = ucompany.ClientCoreUsername;

//            return context;
//        }

//        /************* Relationship Methods *****************/
//        internal static clientProductRelation[] InvokeGetProductRelation(string companyNumber, string id)
//        {
//            BizObjects.Company ucompany = BuildCompany(companyNumber);

//            serviceContext context = BuildContext(ucompany);
            
//            productRelationParams parameters = new productRelationParams();
//            parameters.clientId = id;
//            parameters.productId = null;
//            parameters.productRelationId = null;

//            object searchService = null;
//            clientProductRelation[] result = null;

//            if (ucompany.ServiceType == 1)
//            {
//                searchService = new ClientCoreServiceHelperSF(ucompany.ClientCoreEndPointUrl);
//            }
//            else
//            {
//                searchService = new ClientCoreWebServiceHelper(ucompany.ClientCoreEndPointUrl);
//            }

//            Type serviceType = searchService.GetType();
//            MethodInfo searchMethod = serviceType.GetMethod("getProductRelation");

//            result = (clientProductRelation[])serviceType.InvokeMember("getProductRelation", BindingFlags.InvokeMethod, null, searchService, new object[] { context, parameters });

//            // TODO: probable logging here
//            return result;
//        }


//        internal static void InvokeModifyProductRelation(string companyNumber, string id,string productName,int submissionId)
//        {
//            BizObjects.Company ucompany = BuildCompany(companyNumber);

//            serviceContext context = BuildContext(ucompany);

//            clientProductRelation relation = new clientProductRelation();

//            relation.clientId = id;
//            relation.productNumber = submissionId.ToString();
//            relation.productEffectiveDate = DateTime.Now.ToString();

//            string basicXML = GetCommonBasicXml(ucompany);
//            string infoXML = Common.BuildClientSearchByIdXml(id, ucompany);
//            object searchService = null;
//            clientProductRelation[] result = null;

//            if (ucompany.ServiceType == 1)
//            {
//                searchService = new ClientCoreServiceHelperSF(ucompany.ClientCoreEndPointUrl);
//            }
//            else
//            {
//                searchService = new ClientCoreWebServiceHelper(ucompany.ClientCoreEndPointUrl);
//            }

//            Type serviceType = searchService.GetType();
//            MethodInfo searchMethod = serviceType.GetMethod("getClientInfo");

//            //if (ucompany.ServiceType != 1)
//            //{
//            //    result = (clientProductRelation[])serviceType.InvokeMember("getClientInfo", BindingFlags.InvokeMethod, null, searchService, new object[] {context,parameters});
//            //}
//            // TODO: probable logging here
//            //return result;
//        }
//    }

//    /// <summary>
//    /// Old services
//    /// </summary>
//    public class ClientCoreWebServiceHelper : BTS.ClientCore.WS.ClientCoreWebServiceService
//    {
//        /// <summary>
//        /// 
//        /// </summary>
//        public ClientCoreWebServiceHelper(string endPointUrl)
//        {
//            base.Url = endPointUrl;
//        }
//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="uri"></param>
//        /// <returns></returns>
//        protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
//        {
//            //return base.GetWebRequest(uri);
//            System.Net.WebRequest request = base.GetWebRequest(uri);
//            (request as System.Net.HttpWebRequest).KeepAlive = false;
//            return request;
//        }
//    }
    
//    /// <summary>
//    /// new services
//    /// </summary>
//    //public class ClientCoreServiceHelper : BTS.ClientCore.WS.ClientCoreService
//    //{
//    //    /// <summary>
//    //    /// 
//    //    /// </summary>
//    //    public ClientCoreServiceHelper(string dsik, string endPointUrl)
//    //    {
//    //        //base.SecurityValue = new BTS.ClientCore.WS.Security();
//    //        //base.SecurityValue.dsik = dsik;
//    //        base.Url = endPointUrl;
//    //    }
//    //    /// <summary>
//    //    /// 
//    //    /// </summary>
//    //    /// <param name="uri"></param>
//    //    /// <returns></returns>
//    //    protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
//    //    {
//    //        //return base.GetWebRequest(uri);
//    //        System.Net.WebRequest request = base.GetWebRequest(uri);
//    //        (request as System.Net.HttpWebRequest).KeepAlive = false;
//    //        return request;
//    //    }
//    //}

//    /// <summary>
//    /// new services
//    /// </summary>
//    public class ClientCoreServiceHelperSF : BTS.ClientCore.WS.SF.ClientCoreService
//    {
//        /// <summary>
//        /// 
//        /// </summary>
//        public ClientCoreServiceHelperSF(string endPointUrl)
//        {
       
//            //base.SecurityValue = new BTS.ClientCore.WS.USG.Security();
//            //base.SecurityValue.dsik = dsik;
//            base.Url = endPointUrl;
//        }
//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="uri"></param>
//        /// <returns></returns>
//        protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
//        {
//            //return base.GetWebRequest(uri);
//            System.Net.WebRequest request = base.GetWebRequest(uri);
//            (request as System.Net.HttpWebRequest).KeepAlive = false;
//            return request;
//        }
//    }

//    //public class ClientCoreWebServiceSearchHelper : BTS.ClientCore.WS.ClientCoreService
//    //{
//    //    /// <summary>
//    //    /// 
//    //    /// </summary>
//    //    public ClientCoreWebServiceSearchHelper(string dsik, string url)
//    //    {
//    //        base.SecurityValue = new BTS.ClientCore.WS.Security();
//    //        base.SecurityValue.dsik = dsik;
//    //        base.Url = url + "performClientSearch";
//    //    }
//    //    /// <summary>
//    //    /// 
//    //    /// </summary>
//    //    /// <param name="uri"></param>
//    //    /// <returns></returns>
//    //    protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
//    //    {
//    //        //return base.GetWebRequest(uri);
//    //        System.Net.WebRequest request = base.GetWebRequest(uri);
//    //        (request as System.Net.HttpWebRequest).KeepAlive = false;
//    //        return request;
//    //    }

//    //    #region debugging code for new cc service
//    //    protected override XmlWriter GetWriterForMessage(System.Web.Services.Protocols.SoapClientMessage message, int bufferSize)
//    //    {
//    //        return base.GetWriterForMessage(message, bufferSize);
//    //    }

//    //    protected override XmlReader GetReaderForMessage(System.Web.Services.Protocols.SoapClientMessage message, int bufferSize)
//    //    {
//    //        if (message.Stage == System.Web.Services.Protocols.SoapMessageStage.BeforeDeserialize)
//    //        {
//    //            System.IO.Stream oldStream = message.Stream;
//    //            System.IO.MemoryStream newStream = new System.IO.MemoryStream();

//    //            Copy(oldStream, newStream);
//    //            System.IO.FileStream fs = new System.IO.FileStream("c:\\ccfiles\\Searchreturned" + string.Format("{0}.xml", DateTime.Now.ToString("MMddyyyyHHmmss")), System.IO.FileMode.Append,
//    //                System.IO.FileAccess.Write);
//    //            System.IO.StreamWriter w = new System.IO.StreamWriter(fs);

//    //            //string soapString = "SoapRequest";
//    //            //w.WriteLine("-----" + soapString +
//    //            //    " at " + DateTime.Now);
//    //            w.Flush();
//    //            newStream.Position = 0;
//    //            Copy(newStream, fs);
//    //            w.Close();
//    //            newStream.Position = 0;



//    //            XmlReaderSettings rSettings = new XmlReaderSettings();
//    //            System.IO.TextReader reader = new System.IO.StreamReader(newStream);
//    //            XmlReader xmlReader = XmlReader.Create(reader, rSettings);
//    //            //xmlReader.("performClientSearchReturn");
//    //            xmlReader.MoveToContent();
//    //            string contentRead = xmlReader.ReadOuterXml();
//    //            contentRead = contentRead.Replace("performClientSearchReturn", "p435:performClientSearchReturn");
//    //            XmlDocument doc = new XmlDocument();
//    //            doc.LoadXml(contentRead);

//    //            System.IO.MemoryStream mm = new System.IO.MemoryStream();
//    //            // TODO: check if can be optimized
//    //            mm.Write(System.Text.Encoding.UTF8.GetBytes(contentRead), 0, contentRead.Length);
//    //            mm.Position = 0;
//    //            newStream = new System.IO.MemoryStream();
//    //            Copy(mm, newStream);


//    //            XmlWriterSettings settings = new XmlWriterSettings();
//    //            settings.OmitXmlDeclaration = true;

//    //            fs = new System.IO.FileStream("c:\\ccfiles\\Searchcorrected" + string.Format("{0}.xml", DateTime.Now.ToString("MMddyyyyHHmmss")), System.IO.FileMode.Append,
//    //                System.IO.FileAccess.Write);
//    //            XmlWriter xmlWriter = XmlWriter.Create(fs, settings);
//    //            xmlWriter.Flush();
//    //            newStream.Position = 0;
//    //            Copy(newStream, fs);
//    //            xmlWriter.Close();
//    //            newStream.Position = 0;

//    //            System.IO.TextReader treader = new System.IO.StringReader(doc.InnerXml);

//    //            //XmlSchemaValidator validator = new XmlSchemaValidator(xmlReader.NameTable, new XmlSchemaSet(xmlReader.NameTable),
//    //            //    new XmlNamespaceManager(xmlReader.NameTable), XmlSchemaValidationFlags.ReportValidationWarnings);
//    //            //foreach (XmlNode var in doc.ChildNodes)
//    //            //{
//    //            //    validator.ValidateElement(var.Name, 
//    //            //}

//    //            XmlReader retReader = XmlReader.Create(treader, rSettings);
//    //            return retReader;

//    //        }

//    //        XmlReader ret = base.GetReaderForMessage(message, bufferSize);
//    //        return ret;
//    //    }

//    //    void Copy(System.IO.Stream from, System.IO.Stream to)
//    //    {
//    //        System.IO.TextReader reader = new System.IO.StreamReader(from);
//    //        System.IO.TextWriter writer = new System.IO.StreamWriter(to);
//    //        writer.WriteLine(reader.ReadToEnd());
//    //        writer.Flush();
//    //    } 
//    //    #endregion

//    //}

//    //public class ClientCoreWebServiceInfoHelper : BTS.ClientCore.WS.ClientCoreService
//    //{
//    //    /// <summary>
//    //    /// 
//    //    /// </summary>
//    //    public ClientCoreWebServiceInfoHelper(string dsik, string url)
//    //    {
//    //        base.SecurityValue = new BTS.ClientCore.WS.Security();
//    //        base.SecurityValue.dsik = dsik;
//    //        base.Url = url + "getClientInfo";
//    //    }
//    //    /// <summary>
//    //    /// 
//    //    /// </summary>
//    //    /// <param name="uri"></param>
//    //    /// <returns></returns>
//    //    protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
//    //    {
//    //        //return base.GetWebRequest(uri);
//    //        System.Net.WebRequest request = base.GetWebRequest(uri);
//    //        (request as System.Net.HttpWebRequest).KeepAlive = false;
//    //        return request;
//    //    }

//    //    #region debugging code for new cc service
//    //    //protected override XmlWriter GetWriterForMessage(System.Web.Services.Protocols.SoapClientMessage message, int bufferSize)
//    //    //{
//    //    //    return base.GetWriterForMessage(message, bufferSize);
//    //    //}

//    //    //protected override XmlReader GetReaderForMessage(System.Web.Services.Protocols.SoapClientMessage message, int bufferSize)
//    //    //{
//    //    //    if (message.Stage == System.Web.Services.Protocols.SoapMessageStage.BeforeDeserialize)
//    //    //    {
//    //    //        System.IO.Stream oldStream = message.Stream;
//    //    //        System.IO.MemoryStream newStream = new System.IO.MemoryStream();

//    //    //        Copy(oldStream, newStream);
//    //    //        System.IO.FileStream fs = new System.IO.FileStream("c:\\ccfiles\\log" + string.Format("{0}.xml", DateTime.Now.ToString("MMddyyyyHHmmss")), System.IO.FileMode.Append,
//    //    //            System.IO.FileAccess.Write);
//    //    //        System.IO.StreamWriter w = new System.IO.StreamWriter(fs);

//    //    //        //string soapString = "SoapRequest";
//    //    //        //w.WriteLine("-----" + soapString +
//    //    //        //    " at " + DateTime.Now);
//    //    //        w.Flush();
//    //    //        newStream.Position = 0;
//    //    //        Copy(newStream, fs);
//    //    //        w.Close();
//    //    //        newStream.Position = 0;



//    //    //        XmlReaderSettings rSettings = new XmlReaderSettings();
//    //    //        System.IO.TextReader reader = new System.IO.StreamReader(newStream);
//    //    //        XmlReader xmlReader = XmlReader.Create(reader, rSettings);
//    //    //        //xmlReader.("performClientSearchReturn");
//    //    //        xmlReader.MoveToContent();
//    //    //        string contentRead = xmlReader.ReadOuterXml();
//    //    //        contentRead = contentRead.Replace("xmlns=\"http://ccimport.service.clientcore.freedomgroup.com\"", "xmlns=\"http://webservice.clientcore.bts.com\"");
//    //    //        XmlDocument doc = new XmlDocument();
//    //    //        doc.LoadXml(contentRead);


//    //    //        XmlWriterSettings settings = new XmlWriterSettings();
//    //    //        settings.OmitXmlDeclaration = true;

//    //    //        fs = new System.IO.FileStream("c:\\ccfiles\\InfoLog" + string.Format("{0}.xml", DateTime.Now.ToString("MMddyyyyHHmmss")), System.IO.FileMode.Append,
//    //    //            System.IO.FileAccess.Write);
//    //    //        XmlWriter xmlWriter = XmlWriter.Create(fs, settings);
//    //    //        xmlWriter.Flush();
//    //    //        newStream.Position = 0;
//    //    //        Copy(newStream, fs);
//    //    //        xmlWriter.Close();
//    //    //        newStream.Position = 0;

//    //    //        System.IO.TextReader treader = new System.IO.StringReader(doc.InnerXml);

//    //    //        //XmlSchemaValidator validator = new XmlSchemaValidator(xmlReader.NameTable, new XmlSchemaSet(xmlReader.NameTable),
//    //    //        //    new XmlNamespaceManager(xmlReader.NameTable), XmlSchemaValidationFlags.ReportValidationWarnings);
//    //    //        //foreach (XmlNode var in doc.ChildNodes)
//    //    //        //{
//    //    //        //    validator.ValidateElement(var.Name, 
//    //    //        //}

//    //    //        XmlReader retReader = XmlReader.Create(treader, rSettings);
//    //    //        return retReader;

//    //    //    }

//    //    //    XmlReader ret = base.GetReaderForMessage(message, bufferSize);
//    //    //    return ret;
//    //    //}

//    //    //void Copy(System.IO.Stream from, System.IO.Stream to)
//    //    //{
//    //    //    System.IO.TextReader reader = new System.IO.StreamReader(from);
//    //    //    System.IO.TextWriter writer = new System.IO.StreamWriter(to);
//    //    //    writer.WriteLine(reader.ReadToEnd());
//    //    //    writer.Flush();
//    //    //}
//    //    #endregion
//    //}

//    //public class ClientCoreWebServiceImportHelper : BTS.ClientCore.WS.ClientCoreService
//    //{
//    //    /// <summary>
//    //    /// 
//    //    /// </summary>
//    //    public ClientCoreWebServiceImportHelper(string dsik, string url)
//    //    {
//    //        base.SecurityValue = new BTS.ClientCore.WS.Security();
//    //        base.SecurityValue.dsik = dsik;
//    //        base.Url = url + "importClient";
//    //    }
//    //    /// <summary>
//    //    /// 
//    //    /// </summary>
//    //    /// <param name="uri"></param>
//    //    /// <returns></returns>
//    //    protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
//    //    {
//    //        //return base.GetWebRequest(uri);
//    //        System.Net.WebRequest request = base.GetWebRequest(uri);
//    //        (request as System.Net.HttpWebRequest).KeepAlive = false;
//    //        return request;
//    //    }

//    //    #region debugging code for new cc service
//    //    //protected override XmlWriter GetWriterForMessage(System.Web.Services.Protocols.SoapClientMessage message, int bufferSize)
//    //    //{
//    //    //    return base.GetWriterForMessage(message, bufferSize);
//    //    //}

//    //    //protected override System.Net.WebResponse GetWebResponse(System.Net.WebRequest request)
//    //    //{
//    //    //    return base.GetWebResponse(request);
//    //    //}

//    //    //protected override XmlReader GetReaderForMessage(System.Web.Services.Protocols.SoapClientMessage message, int bufferSize)
//    //    //{
//    //    //    if (message.Stage == System.Web.Services.Protocols.SoapMessageStage.BeforeDeserialize)
//    //    //    {
//    //    //        System.IO.Stream oldStream = message.Stream;
//    //    //        System.IO.MemoryStream newStream = new System.IO.MemoryStream();

//    //    //        Copy(oldStream, newStream);
//    //    //        System.IO.FileStream fs = new System.IO.FileStream("c:\\ccfiles\\ImportReturned" + string.Format("{0}.xml", DateTime.Now.ToString("MMddyyyyHHmmss")),
//    //    //            System.IO.FileMode.Create,
//    //    //            System.IO.FileAccess.Write);
//    //    //        System.IO.StreamWriter w = new System.IO.StreamWriter(fs);

//    //    //        //string soapString = "SoapRequest";
//    //    //        //w.WriteLine("-----" + soapString +
//    //    //        //    " at " + DateTime.Now);
//    //    //        w.Flush();
//    //    //        newStream.Position = 0;
//    //    //        Copy(newStream, fs);
//    //    //        w.Close();
//    //    //        fs.Close();
//    //    //        newStream.Position = 0;



//    //    //        XmlReaderSettings rSettings = new XmlReaderSettings();
//    //    //        System.IO.TextReader reader = new System.IO.StreamReader(newStream);
//    //    //        XmlReader xmlReader = XmlReader.Create(reader, rSettings);
//    //    //        xmlReader.MoveToContent();
//    //    //        string contentRead = xmlReader.ReadOuterXml();
//    //    //        contentRead = contentRead.Replace("importClientReturn", "p435:importClientReturn");
//    //    //        XmlDocument doc = new XmlDocument();
//    //    //        doc.LoadXml(contentRead);

//    //    //        System.IO.MemoryStream mm = new System.IO.MemoryStream();
//    //    //        // TODO: check if can be optimized
//    //    //        mm.Write(System.Text.Encoding.UTF8.GetBytes(contentRead), 0, contentRead.Length);
//    //    //        mm.Position = 0;
//    //    //        newStream = new System.IO.MemoryStream();
//    //    //        Copy(mm, newStream);


//    //    //        XmlWriterSettings settings = new XmlWriterSettings();
//    //    //        settings.OmitXmlDeclaration = true;

//    //    //        fs = new System.IO.FileStream("c:\\ccfiles\\ImportCorrected" + string.Format("{0}.xml", DateTime.Now.ToString("MMddyyyyHHmmss")),
//    //    //            System.IO.FileMode.Create,
//    //    //            System.IO.FileAccess.Write);
//    //    //        XmlWriter xmlWriter = XmlWriter.Create(fs, settings);
//    //    //        xmlWriter.Flush();
//    //    //        newStream.Position = 0;
//    //    //        Copy(newStream, fs);
//    //    //        xmlWriter.Close();
//    //    //        fs.Close();
//    //    //        newStream.Position = 0;

//    //    //        System.IO.TextReader treader = new System.IO.StringReader(doc.InnerXml);

//    //    //        //XmlSchemaValidator validator = new XmlSchemaValidator(xmlReader.NameTable, new XmlSchemaSet(xmlReader.NameTable),
//    //    //        //    new XmlNamespaceManager(xmlReader.NameTable), XmlSchemaValidationFlags.ReportValidationWarnings);
//    //    //        //foreach (XmlNode var in doc.ChildNodes)
//    //    //        //{
//    //    //        //    validator.ValidateElement(var.Name, 
//    //    //        //}

//    //    //        XmlReader retReader = XmlReader.Create(treader, rSettings);
//    //    //        return retReader;

//    //    //    }

//    //    //    XmlReader ret = base.GetReaderForMessage(message, bufferSize);
//    //    //    return ret;
//    //    //}

//    //    //void Copy(System.IO.Stream from, System.IO.Stream to)
//    //    //{
//    //    //    System.IO.TextReader reader = new System.IO.StreamReader(from);
//    //    //    System.IO.TextWriter writer = new System.IO.StreamWriter(to);
//    //    //    writer.WriteLine(reader.ReadToEnd());
//    //    //    writer.Flush();
//    //    //}
//    //    #endregion
//    //}

//    //public class ClientCoreWebServicePortfolioHelper : BTS.ClientCore.WS.ClientCoreService
//    //{
//    //    /// <summary>
//    //    /// 
//    //    /// </summary>
//    //    public ClientCoreWebServicePortfolioHelper(string dsik, string url)
//    //    {
//    //        // TODO: 
//    //        //base.SecurityValue = new BTS.ClientCore.WS.Security();
//    //        //base.SecurityValue.dsik = dsik;
//    //        base.Url = url + "performPortfolioSearch";
//    //    }
//    //    /// <summary>
//    //    /// 
//    //    /// </summary>
//    //    /// <param name="uri"></param>
//    //    /// <returns></returns>
//    //    protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
//    //    {
//    //        //return base.GetWebRequest(uri);
//    //        System.Net.WebRequest request = base.GetWebRequest(uri);
//    //        (request as System.Net.HttpWebRequest).KeepAlive = false;
//    //        return request;
//    //    }

//    //    #region debugging code for new cc service
//    //    //protected override XmlWriter GetWriterForMessage(System.Web.Services.Protocols.SoapClientMessage message, int bufferSize)
//    //    //{
//    //    //    return base.GetWriterForMessage(message, bufferSize);
//    //    //}

//    //    //protected override System.Net.WebResponse GetWebResponse(System.Net.WebRequest request)
//    //    //{
//    //    //    return base.GetWebResponse(request);
//    //    //}

         

//    //    //protected override XmlReader GetReaderForMessage(System.Web.Services.Protocols.SoapClientMessage message, int bufferSize)
//    //    //{
//    //    //    if (message.Stage == System.Web.Services.Protocols.SoapMessageStage.BeforeDeserialize)
//    //    //    {
//    //    //        System.IO.Stream oldStream = message.Stream;
//    //    //        System.IO.MemoryStream newStream = new System.IO.MemoryStream();

//    //    //        Copy(oldStream, newStream);
//    //    //        System.IO.FileStream fs = new System.IO.FileStream("c:\\ccfiles\\PortfolioSearchreturned" + string.Format("{0}.xml", DateTime.Now.ToString("MMddyyyyHHmmss")),
//    //    //            System.IO.FileMode.Create,
//    //    //            System.IO.FileAccess.Write);
//    //    //        System.IO.StreamWriter w = new System.IO.StreamWriter(fs);

//    //    //        //string soapString = "SoapRequest";
//    //    //        //w.WriteLine("-----" + soapString +
//    //    //        //    " at " + DateTime.Now);
//    //    //        w.Flush();
//    //    //        newStream.Position = 0;
//    //    //        Copy(newStream, fs);
//    //    //        w.Close();
//    //    //        fs.Close();
//    //    //        newStream.Position = 0;



//    //    //        XmlReaderSettings rSettings = new XmlReaderSettings();
//    //    //        System.IO.TextReader reader = new System.IO.StreamReader(newStream);
//    //    //        XmlReader xmlReader = XmlReader.Create(reader, rSettings);
//    //    //        xmlReader.MoveToContent();
//    //    //        string contentRead = xmlReader.ReadOuterXml();
//    //    //        contentRead = contentRead.Replace("performPortfolioSearchReturn", "p435:performPortfolioSearchReturn");
//    //    //        contentRead = contentRead.Replace("client_list", "vector");
//    //    //        contentRead = contentRead.Replace("/client_list", "/vector");
//    //    //        XmlDocument doc = new XmlDocument();
//    //    //        doc.LoadXml(contentRead);

//    //    //        System.IO.MemoryStream mm = new System.IO.MemoryStream();
//    //    //        // TODO: check if can be optimized
//    //    //        mm.Write(System.Text.Encoding.UTF8.GetBytes(contentRead), 0, contentRead.Length);
//    //    //        mm.Position = 0;
//    //    //        newStream = new System.IO.MemoryStream();
//    //    //        Copy(mm, newStream);


//    //    //        XmlWriterSettings settings = new XmlWriterSettings();
//    //    //        settings.OmitXmlDeclaration = true;

//    //    //        fs = new System.IO.FileStream("c:\\ccfiles\\PortfolioSearchcorrected" + string.Format("{0}.xml", DateTime.Now.ToString("MMddyyyyHHmmss")),
//    //    //            System.IO.FileMode.Create,
//    //    //            System.IO.FileAccess.Write);
//    //    //        XmlWriter xmlWriter = XmlWriter.Create(fs, settings);
//    //    //        xmlWriter.Flush();
//    //    //        newStream.Position = 0;
//    //    //        Copy(newStream, fs);
//    //    //        xmlWriter.Close();
//    //    //        fs.Close();
//    //    //        newStream.Position = 0;

//    //    //        System.IO.TextReader treader = new System.IO.StringReader(doc.InnerXml);

//    //    //        //XmlSchemaValidator validator = new XmlSchemaValidator(xmlReader.NameTable, new XmlSchemaSet(xmlReader.NameTable),
//    //    //        //    new XmlNamespaceManager(xmlReader.NameTable), XmlSchemaValidationFlags.ReportValidationWarnings);
//    //    //        //foreach (XmlNode var in doc.ChildNodes)
//    //    //        //{
//    //    //        //    validator.ValidateElement(var.Name, 
//    //    //        //}

//    //    //        XmlReader retReader = XmlReader.Create(treader, rSettings);
//    //    //        return retReader;

//    //    //    }

//    //    //    XmlReader ret = base.GetReaderForMessage(message, bufferSize);
//    //    //    return ret;
//    //    //}

//    //    //void Copy(System.IO.Stream from, System.IO.Stream to)
//    //    //{
//    //    //    System.IO.TextReader reader = new System.IO.StreamReader(from);
//    //    //    System.IO.TextWriter writer = new System.IO.StreamWriter(to);
//    //    //    writer.WriteLine(reader.ReadToEnd());
//    //    //    writer.Flush();
//    //    //} 
//    //    #endregion
//    //}
//}
