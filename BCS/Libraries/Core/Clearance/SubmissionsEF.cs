using System;
using System.Data.SqlTypes;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Reflection;
using structures = BTS.ClientCore.Wrapper.Structures;
//using BCS.Biz;
using System.Data.SqlClient;
using BTS.LogFramework;
using System.Web.UI;
using System.Linq;
using BCS.DAL;
using BOA.Util.Extensions;
using System.Data.Objects;
using System.Data;
//using Amib.Threading;


namespace BCS.Core.Clearance
{
    /// <summary>
    /// class handles Submissions Data Access 
    /// </summary>
    public class SubmissionsEF
    {
        #region Get Submissions


        public static string[] submissionRelations = new string[] { 
                                           // "CompanyNumberCode.CompanyNumberCodeType"
                                           //,"SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute.AttributeDataType"
                                           //,"LineOfBusiness"
                                            "CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason"
                                           //,"SubmissionLineOfBusinessChildren.CompanyNumberLOBGrid"
                                           //,"SubmissionHistory"
                                           ,"Person"
                                           ,"Person1"
                                           ,"Person2"
                                           ,"Agency"
                                           //,"SubmissionClientCoreClientAddress"
                                           //,"SubmissionClientCoreClientName"
                                           //,"SubmissionComment"
                                           ,"CompanyNumber"
                                           ,"SubmissionType"
                                           ,"PolicySystem"
                                           ,"OtherCarrier"
                                           ,"SubmissionStatus"
                                           ,"SubmissionStatusReason"
                                           ,"Agent"
                                           ,"Contact"
                                          };

        public static string GetSubmissionById(long id)
        {
            {
                List<Submission> subs = DataAccess.GetEntityList<Submission>("Id", id, submissionRelations);

                BizObjects.SubmissionList list = BuildSubmissionList(subs, new string[] { }, new string[] { }, null, 0, 0);

                string xml = XML.Serializer.SerializeAsXml(list);
                return xml;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// Commented 8/22/2011 - TG - Possibly not needed
        //public string GetSubmissions(string companyNumber,ListDictionary dict)
        //{
        //    OrmLib.DataManagerBase.CriteriaGroup criteria = new OrmLib.DataManagerBase.CriteriaGroup();
        //    BizObjects.SubmissionList list;
        //    if (dict.Contains("Id"))
        //    { criteria.And(Biz.JoinPath.Submission.Columns.Id, dict["Id"]); }
        //    else if (dict.Contains("ClientId"))
        //    { criteria.And(Biz.JoinPath.Submission.Columns.ClientCoreClientId, dict["ClientId"]); }
        //    else if (dict.Contains("ClientIds"))
        //    {
        //        string[] clientids = dict["ClientIds"].ToString().Split(",".ToCharArray());
        //        foreach(string id in clientids)
        //            criteria.Or(Biz.JoinPath.Submission.Columns.ClientCoreClientId, id);
        //    }
        //    else if (dict.Contains("FEIN"))
        //    { criteria.And(Biz.JoinPath.Submission.Agency.Columns.FEIN, dict["FEIN"]); }
        //    else if (dict.Contains("AgencyId"))
        //    { criteria.And(Biz.JoinPath.Submission.Columns.AgencyId, dict["AgencyId"]); }
        //    else if (dict.Contains("AgentId"))
        //    { criteria.Or(Biz.JoinPath.Submission.Columns.AgentId, dict["AgentId"]); }
        //    else
        //    {
        //        // get clients from client core
        //        structures.Search.Vector result;

        //        if (dict.Contains("ClientTaxId"))
        //        {
        //            result = (structures.Search.Vector)XML.Deserializer.Deserialize(
        //                Clients.GetClientByTaxId(companyNumber, dict["ClientTaxId"].ToString()),typeof(structures.Search.Vector));
        //        }
        //        else if (dict.Contains("NameFirst"))
        //        {
        //            if (dict.Contains("NameBusiness").ToString().Length == 0)
        //            {
        //                result = (structures.Search.Vector)XML.Deserializer.Deserialize(
        //                    Clients.GetClientsByName(companyNumber, 
        //                    dict["NameFirst"].ToString(), dict["NameLast"].ToString()), typeof(structures.Search.Vector));

        //            }
        //            else
        //            {
        //                result = (structures.Search.Vector)XML.Deserializer.Deserialize(
        //                Clients.GetClientByBusinessName(companyNumber, dict["NameBusiness"].ToString()), typeof(structures.Search.Vector));
        //            }
        //        }
        //        else if (dict.Contains("HouseNo"))
        //        {
        //            string temp = Clients.GetClientsByAddress(companyNumber, dict["HouseNo"].ToString(), dict["Address1"].ToString(),
        //                dict["Address2"].ToString(),
        //                dict["Address3"].ToString(), dict["Address4"].ToString(), dict["City"].ToString(),
        //                dict["State"].ToString(), dict["County"].ToString(), dict["Country"].ToString(),
        //                dict["Zip"].ToString());

        //            result = (structures.Search.Vector)XML.Deserializer.Deserialize(temp, typeof(structures.Search.Vector));
        //        }
        //        else
        //        {
        //            result = new BTS.ClientCore.Wrapper.Structures.Search.Vector();
        //            result.Error = "No values were supplied.";
        //        }
        //        if (result.Error == null && result.Clients != null)
        //        {
        //            foreach (structures.Search.Client client in result.Clients)
        //            {
        //                criteria.Or(Biz.JoinPath.Submission.Columns.ClientCoreClientId, client.Info.ClientId);
        //            }
        //        }
        //        if (result.Clients == null)
        //        {
        //            list = new BCS.Core.Clearance.BizObjects.SubmissionList();
        //            list.Message = "No clients were found with the search criteria specified.";
        //            string x = XML.Serializer.SerializeAsXml(list);
        //            return x;
        //        }
        //    }


        //    Biz.DataManager dm = Common.GetDataManager();

        //    if (!dict.Contains("Id"))
        //    {
        //        criteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
        //    }

        //    dm.QueryCriteria = criteria;
        //    Biz.SubmissionCollection scoll = dm.GetSubmissionCollection(SubmissionFetchPath());
        //    list = BuildSubmissionList(scoll, new string[] { }, new string[] { }, null, 0, 0);

        //    // return the string as serialized Response object
        //    string xml = XML.Serializer.SerializeAsXml(list);
        //    return xml;
        //}

        //private static OrmLib.DataManagerBase.FetchPathRelation[] SubmissionFetchPath()
        //{
        //    return new OrmLib.DataManagerBase.FetchPathRelation[]
        //    {
        //        Biz.FetchPath.Submission.All,
        //        Biz.FetchPath.Submission.SubmissionCompanyNumberCode.CompanyNumberCode,
        //        Biz.FetchPath.Submission.SubmissionCompanyNumberCode.CompanyNumberCode.CompanyNumberCodeType,
        //        Biz.FetchPath.Submission.SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute,
        //        Biz.FetchPath.Submission.SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute.AttributeDataType,
        //        Biz.FetchPath.Submission.SubmissionLineOfBusiness.LineOfBusiness,
        //        Biz.FetchPath.Submission.SubmissionCompanyNumberAgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReason,
        //        Biz.FetchPath.Submission.SubmissionCompanyNumberAgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason,
        //        Biz.FetchPath.Submission.SubmissionLineOfBusinessChildren.CompanyNumberLOBGrid,
        //        Biz.FetchPath.Submission.SubmissionHistory
        //    };
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="criteria">first - OrmLib.DataManagerBase.JoinPathRelation, second - data, third - MatchType </param>
        /// <returns></returns>
        public static string GetSubmissions(string companyNumber, bool includeDeleted, params EFilter[] criteria)
        {
            List<EFilter> filters = new List<EFilter>();

            filters.Add(new EFilter("CompanyNumber.CompanyNumber1", companyNumber));
            if (!includeDeleted)
            {
                filters.Add(new EFilter("Deleted", false));
            }

            if (criteria != null)
            {
                foreach (EFilter filter in criteria)
                    filters.Add(filter);
            }

            BCS.Core.Clearance.BuildSubmissionList listBuilder = new Clearance.BuildSubmissionList();

            BizObjects.SubmissionList sl = listBuilder.Build(filters, submissionRelations);

            // return the string as serialized Response object
            string xml = XML.Serializer.SerializeAsXml(sl);
            return xml;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="policyNumber"></param>
        ///// <param name="companyNumber"></param>
        ///// <returns></returns>
        //public static string GetSubmissionsByPolicyNumberAndCompanyNumber(string policyNumber, string companyNumber)
        //{
        //    Biz.DataManager dm = Common.GetDataManager();
        //    dm.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
        //    Company company = dm.GetCompany();
        //    //if (company.UsesAPS)
        //    //    return new APS.Submissions().GetSubmissionsByPolicyNumberAndCompanyNumber(policyNumber, companyNumber);

        //    dm.QueryCriteria.Clear();
        //    dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber).And(
        //        Biz.JoinPath.Submission.Columns.PolicyNumber, policyNumber);
        //    Biz.SubmissionCollection scoll = dm.GetSubmissionCollection(SubmissionFetchPath());
        //    if (scoll != null)
        //        scoll = scoll.SortBySequence(OrmLib.SortDirection.Descending).SortByEffectiveDate(OrmLib.SortDirection.Descending); 

        //    BizObjects.SubmissionList list = BuildSubmissionList(scoll, new string[] { }, new string[] { }, null, 0, 0);

        //    if (list != null && list.List != null)
        //        list.List = list.List.OrderByDescending(e => e.EffectiveDate).ThenByDescending(s => s.SubmissionNumberSequence).ToArray();

        //    string xml = XML.Serializer.SerializeAsXml(list);
        //    return xml;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="policyNumber"></param>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static string GetSubmissionsByPolicyNumberAndCompanyNumber(string policyNumber, string companyNumber)
        {

            List<Submission> subs = SearchSubmissions(companyNumber, true, new EFilter("PolicyNumber", policyNumber));

            BizObjects.SubmissionList list = BuildSubmissionList(subs, new string[] { }, new string[] { }, null, 0, 0);
            if (list != null && list.List != null)
                list.List = list.List.OrderByDescending(e => e.EffectiveDate).ThenByDescending(s => s.SubmissionNumberSequence).ToArray();

            string xml = XML.Serializer.SerializeAsXml(list);
            return xml;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="submissionNumber"></param>
        /// <param name="companyNumber"></param>
        /// <returns></returns>
        public static string GetSubmissionBySubmissionNumberAndCompanyNumber(string submissionNumber, string companyNumber)
        {
            //Biz.DataManager dm = Common.GetDataManager();
            //dm.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //Company company = dm.GetCompany();
            ////if (company.UsesAPS)
            ////    return new APS.Submissions().GetSubmissionBySubmissionNumberAndCompanyNumber(submissionNumber, companyNumber);

            //dm.QueryCriteria.Clear();
            //dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber).And(
            //    Biz.JoinPath.Submission.Columns.SubmissionNumber, submissionNumber);
            //Biz.SubmissionCollection scoll = dm.GetSubmissionCollection(SubmissionFetchPath());
            //if (scoll != null && scoll.Count > 0)
            //{
            //    scoll = scoll.SortBySequence(OrmLib.SortDirection.Descending);
            //    BizObjects.Submission list = BuildSubmission(scoll[0]);
            //    string xml = XML.Serializer.SerializeAsXml(list);
            //    return xml;
            //}
            //return null;

            long subNum = 0;
            long.TryParse(submissionNumber, out subNum);

            List<Submission> subs = SearchSubmissions(companyNumber, true, new EFilter("SubmissionNumber", subNum));

            if (subs == null || subs.Count == 0)
                return null;

            BizObjects.Submission list = BuildSubmission(subs.First());
            string xml = XML.Serializer.SerializeAsXml(list);
            return xml;

        }

        public static string GetSubmissionbyEffectiveCompanyNumberClientAgencyUnit(DateTime effectiveDate, string companyNumber, int clientID
                                                           , string agencyNumber, string underwriterUnitCode)
        {
            //Biz.DataManager dm = Common.GetDataManager();
            //dm.QueryCriteria.Clear();

            //if (effectiveDate != DateTime.MinValue)
            //    dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.EffectiveDate, effectiveDate);
            //if (!string.IsNullOrEmpty(companyNumber))
            //    dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //if (clientID > 0)
            //    dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.ClientCoreClientId, clientID);
            //if (!string.IsNullOrEmpty(agencyNumber))
            //    dm.QueryCriteria.And(Biz.JoinPath.Submission.Agency.Columns.AgencyNumber, agencyNumber);
            //if (!string.IsNullOrEmpty(underwriterUnitCode))
            //    dm.QueryCriteria.And(Biz.JoinPath.Submission.SubmissionLineOfBusiness.LineOfBusiness.Columns.Code, underwriterUnitCode);
            //Biz.SubmissionCollection scoll = dm.GetSubmissionCollection(SubmissionFetchPath());
            //if (scoll != null)
            //    scoll = scoll.SortBySequence(OrmLib.SortDirection.Descending);

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                var submissions = bc.Submission.Include("CompanyNumberCode.CompanyNumberCodeType")
                    .Include("SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute.AttributeDataType")
                    .Include("LineOfBusiness")
                    .Include("CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason")
                    .Include("SubmissionLineOfBusinessChildren.CompanyNumberLOBGrid")
                    .Include("Person")
                    .Include("SubmissionHistory").AsQueryable();

                if (effectiveDate != DateTime.MinValue)
                    submissions = submissions.Where(s => s.EffectiveDate == effectiveDate);
                if (!string.IsNullOrEmpty(companyNumber))
                    submissions = submissions.Where(s => s.CompanyNumber.CompanyNumber1 == companyNumber);
                if (clientID > 0)
                    submissions = submissions.Where(s => s.ClientCoreClientId == clientID);
                if (!string.IsNullOrEmpty(agencyNumber))
                    submissions = submissions.Where(s => s.Agency.AgencyNumber == agencyNumber);
                if (!string.IsNullOrEmpty(underwriterUnitCode))
                    submissions = submissions.Where(s => s.LineOfBusiness.Where(l => l.Code == underwriterUnitCode).FirstOrDefault().Code == underwriterUnitCode);
                if (submissions.Count() == 0)
                    return null;

                BizObjects.SubmissionList list = BuildSubmissionList(submissions.ToList(), new string[] { }, new string[] { }, null, 0, 0);
                string xml = XML.Serializer.SerializeAsXml(list);
                return xml;
            }


        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="submissionNumber"></param>
        /// <param name="companyNumber"></param>
        /// <param name="sequence"></param>
        /// <returns></returns>
        public static string GetSubmissionBySubmissionNumberAndCompanyNumber(
            string submissionNumber, string companyNumber, string sequence)
        {
            //Biz.DataManager dm = Common.GetDataManager();
            //dm.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //Company company = dm.GetCompany();
            ////if (company.UsesAPS)
            //    //return new APS.Submissions().GetSubmissionBySubmissionNumberAndCompanyNumber(submissionNumber, companyNumber, sequence);

            //dm.QueryCriteria.Clear();
            //dm.QueryCriteria.And(
            //    Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber).And(
            //    Biz.JoinPath.Submission.Columns.SubmissionNumber, submissionNumber).And(
            //    Biz.JoinPath.Submission.Columns.Sequence,sequence);
            //Biz.Submission scoll = dm.GetSubmission(SubmissionFetchPath());
            //if (scoll != null)
            //{
            //    BizObjects.Submission list = BuildSubmission(scoll);
            //    string xml = XML.Serializer.SerializeAsXml(list);
            //    return xml;
            //}

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                long subNum = 0;
                long.TryParse(submissionNumber, out subNum);

                int seq = 0;
                Int32.TryParse(sequence, out seq);

                var submissions = bc.Submission.Include("CompanyNumberCode.CompanyNumberCodeType")
                    .Include("SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute.AttributeDataType")
                    .Include("LineOfBusiness")
                    .Include("CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason")
                    .Include("SubmissionLineOfBusinessChildren.CompanyNumberLOBGrid")
                    .Include("SubmissionHistory")
                    .Include("Person")
                    .Where(s => s.SubmissionNumber == subNum && s.CompanyNumber.CompanyNumber1 == companyNumber && s.Sequence == seq);

                if (submissions == null || submissions.Count() == 0)
                    return null;

                BizObjects.Submission list = BuildSubmission(submissions.First());
                string xml = XML.Serializer.SerializeAsXml(list);
                return xml;
            }

        }


        #endregion

        #region Add Submissions

        public static string AddSubmission(string clientid, string clientaddressids, string clientnameids, string clientportfolioid,
            string submissionBy, string submissionDate, int typeid, int agencyid,
            int agentid, string agentreasonids, int contactid, string companynumber, int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber,
            decimal premium, string cocodeids, string attributes, string effective, string expiration,
            int underwriterid, int analystid, int technicianid, int policysystemid, string comments, string lobids, string lobchildren, string insuredname,
            string dba, string subno, int othercarrierid, decimal othercarrierpremium)
        {
            return AddSubmission(clientid, clientaddressids, clientnameids, clientportfolioid,
             submissionBy, submissionDate, typeid, agencyid,
             agentid, agentreasonids, contactid, companynumber, statusid, statusdate, statusreasonid, statusreasonother, policynumber,
             premium, cocodeids, attributes, effective, expiration,
             underwriterid, analystid, technicianid, policysystemid, comments, lobids, lobchildren, insuredname,
             dba, subno, othercarrierid, othercarrierpremium, null, null);
        }


        /// <summary>
        /// method to add a submission by mostly ids
        /// </summary>
        public static string AddSubmission(string clientid, string clientaddressids, string clientnameids, string clientportfolioid,
            string submissionBy, string submissionDate, int typeid, int agencyid,
            int agentid, string agentreasonids, int contactid, string companynumber, int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber,
            decimal premium, string cocodeids, string attributes, string effective, string expiration,
            int underwriterid, int analystid, int technicianid, int policysystemid, string comments, string lobids, string lobchildren, string insuredname,
            string dba, string subno, int othercarrierid, decimal othercarrierpremium, string clientAddressIDsAndTypes, string clientNameIdsAndTypes)
        {
            //Biz.DataManager dm = Common.GetDataManager();
            //dm.QueryCriteria.And(Biz.JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companynumber);
            //Company company = dm.GetCompany(Biz.FetchPath.Company.CompanyParameter);
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                Company company = Company.GetCompanyByCompanyNumber(companynumber);

                BizObjects.Submission s = new BCS.Core.Clearance.BizObjects.Submission();

                string requiredMessagePrefix = "Required :-";

                if (cocodeids == null)
                    cocodeids = string.Empty;
                if (attributes == null)
                    attributes = string.Empty;
                string[] scompanycodeids = cocodeids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                int[] companycodeids = new int[scompanycodeids.Length];
                for (int i = 0; i < scompanycodeids.Length; i++)
                {
                    companycodeids[i] = Convert.ToInt32(scompanycodeids[i]);
                }


                if (!Common.IsNotNullOrEmpty(submissionBy))
                {
                    s.Message += requiredMessagePrefix + "Submission by.";
                    s.Status = "Failure";
                    return XML.Serializer.SerializeAsXml(s);
                }
                if (!Common.IsNotNullOrEmpty(clientid))
                {
                    s.Message += requiredMessagePrefix + "Client Id.";
                    s.Status = "Failure";
                    return XML.Serializer.SerializeAsXml(s);
                }
                if (!Common.IsNotNullOrEmpty(clientaddressids))
                {
                    s.Message += requiredMessagePrefix + "Client Address Id.";
                    s.Status = "Failure";
                    return XML.Serializer.SerializeAsXml(s);
                }

                // verify if agency exists
                Agency agency = DataAccess.GetEntityById<Agency>(agencyid);
                if (agency == null)
                {
                    s.Message += requiredMessagePrefix + Messages.NoSuchAgency;
                    s.Status = "Failure";
                    return XML.Serializer.SerializeAsXml(s);
                }
                // verify if agent exists
                Agent agent = DataAccess.GetEntityById<Agent>(agentid);
                if (agent == null)
                {
                    s.Message += Messages.NoSuchAgent;
                    //return XML.Serializer.SerializeAsXml(s);
                }

                // verify if agent exists
                Contact contact = DataAccess.GetEntityById<Contact>(contactid);
                if (contact == null)
                {
                    s.Message += Messages.NoSuchContact;
                    //return XML.Serializer.SerializeAsXml(s);
                }
                // verify company exists
                CompanyNumber cnum = DataAccess.GetEntityByColumnValue<CompanyNumber>("CompanyNumber1", companynumber);
                if (cnum == null)
                {
                    s.Message += requiredMessagePrefix + Messages.NoSuchCompanyNumber;
                    s.Status = "Failure";
                    return XML.Serializer.SerializeAsXml(s);
                }
                // verify status exists
                SubmissionStatus status = DataAccess.GetEntityById<SubmissionStatus>(statusid);
                if (status == null)
                {
                    s.Message += Messages.NoSuchSubmissionStatus;
                    s.Status = "Failure";
                    //return XML.Serializer.SerializeAsXml(s);
                }

                //Ensure Default Submission Status
                if (company.CompanyParameter != null && company.CompanyParameter.Count > 0 && company.CompanyParameter.ElementAt(0).EnsureDefaultStatusOnAdd)
                {
                    int? defaultStatus = (from ss in bc.CompanyNumberSubmissionStatus
                                          where ss.DefaultSelection == true
                                          && ss.CompanyNumber.CompanyNumber1 == companynumber
                                          select ss.SubmissionStatusId).FirstOrDefault();

                    if (defaultStatus != null && defaultStatus != statusid)
                        statusid = Convert.ToInt32(defaultStatus);
                }


                // verify status reason exists
                SubmissionStatusReason statusreason = DataAccess.GetEntityById<SubmissionStatusReason>(statusreasonid);

                if (statusreason == null)
                {
                    s.Message += Messages.NoSuchSubmissionStatusReason;
                }

                // verify type exists
                SubmissionType type = DataAccess.GetEntityById<SubmissionType>(typeid);
                if (type == null)
                {
                    s.Message += requiredMessagePrefix + Messages.NoSuchSubmissionType;
                    s.Status = "Failure";
                    return XML.Serializer.SerializeAsXml(s);
                }

                //verify underwriter            
                Person uperson = DataAccess.GetEntityById<Person>(underwriterid);
                if (uperson == null)
                {
                    s.Message += Messages.NoSuchUnderwriter;
                }

                //verify analyst, since underwriter and analyst are basically persons, same method is used
                Person aperson = DataAccess.GetEntityById<Person>(analystid);
                if (aperson == null)
                {
                    s.Message += Messages.NoSuchAnalyst;
                    //return XML.Serializer.SerializeAsXml(s);
                }

                //verify technician, since underwriter and technician are basically persons, same method is used
                Person tperson = DataAccess.GetEntityById<Person>(technicianid);
                if (tperson == null)
                {
                    s.Message += Messages.NoSuchTechnician;
                    //return XML.Serializer.SerializeAsXml(s);
                }

                //verify Policy System
                PolicySystem apolicysystem = DataAccess.GetEntityById<PolicySystem>(policysystemid);

                if (apolicysystem == null)
                {
                    s.Message += Messages.NoSuchPolicySystem;
                }

                // verify company number code 
                List<CompanyNumberCode> cnc = DataAccess.GetEntityCollectionById<CompanyNumberCode>(companycodeids);
                if (cnc == null)
                {
                    s.Message += Messages.NoSuchCompanyNumberCode;
                    //return XML.Serializer.SerializeAsXml(s);
                }

                DateTime dtNow = DateTime.Now;

                int sequence = 0;
                long submissioNo = 0;
                Int64.TryParse(subno, out submissioNo);
                long controlNo =
                SubmissionNumberGenerator.GetSubmissionControlNumber(submissioNo, companynumber, out sequence);

                //Biz.Submission submission = dm.NewSubmission(Common.ConvertToSqlInt64(clientid),
                //    Common.ConvertToSqlInt64(controlNo),
                //    submissionBy,
                //    Common.IsNotNullOrEmpty(submissionDate) ? Common.ConvertToSqlDateTime(submissionDate) : Common.ConvertToSqlDateTime(dtNow),
                //     agency, cnum, type);

                Submission submission = new Submission();
                submission.ClientCoreClientId = clientid.ToT<long>();
                //control no?
                submission.SubmissionBy = submissionBy;
                submission.SubmissionDt = string.IsNullOrWhiteSpace(submissionDate) ? DateTime.Now : submissionDate.ToT<DateTime>();

                if (agent != null)
                    submission.AgentId = agentid;

                if (contact != null)
                    submission.ContactId = contactid;

                submission.Sequence = sequence;

                submission.EffectiveDate = effective.ToT<DateTime>();
                submission.EstimatedWrittenPremium = premium;
                submission.OtherCarrierPremium = othercarrierpremium;
                submission.ExpirationDate = expiration.ToT<DateTime>();
                submission.PolicyNumber = policynumber != null ? policynumber.Trim() : policynumber;

                if (uperson != null)
                    submission.UnderwriterPersonId = underwriterid;

                if (aperson != null)
                    submission.AnalystPersonId = analystid;

                if (tperson != null)
                    submission.TechnicianPersonId = technicianid;

                submission.PolicySystemId = apolicysystem.IfNotNull(policysystemid);
                submission.OtherCarrierId = othercarrierid.IfNotNull(othercarrierid);

                submission.UpdatedBy = submissionBy;
                submission.UpdatedDt = dtNow;
                submission.InsuredName = insuredname;
                submission.Dba = dba;
                if (clientportfolioid != "0")
                    submission.ClientCorePortfolioId = clientportfolioid.ToT<Int64>();
                else
                    submission.ClientCorePortfolioId = clientid.ToT<Int64>();
                submission.SubmissionStatusId = status.IfNotNull(statusid);
                submission.SubmissionStatusDate = statusdate.ToT<DateTime>();
                submission.SubmissionStatusReason = statusreason;
                submission.SubmissionStatusReasonOther = statusreasonother;

                SubmissionStatusHistory submissionStatusHistory = new SubmissionStatusHistory();
                submission.SubmissionStatusHistory.Add(submissionStatusHistory);
                submissionStatusHistory.PreviousSubmissionStatusId = status.IfNotNull(status.Id);
                submissionStatusHistory.SubmissionStatusDate = BOA.Util.Common.GetSafeT<DateTime>(statusdate);
                submissionStatusHistory.StatusChangeBy = submissionBy;

                string[] arrClientAddressIds = clientaddressids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string clientaddressid in arrClientAddressIds)
                {
                    submission.SubmissionClientCoreClientAddress.Add(new SubmissionClientCoreClientAddress() { ClientCoreClientAddressId = clientaddressid.ToInt() });
                }

                if (clientnameids != null)
                {
                    string[] arrClientNameIds = clientnameids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string clientnameid in arrClientNameIds)
                    {
                        submission.SubmissionClientCoreClientName.Add(new SubmissionClientCoreClientName() { ClientCoreNameId = clientnameid.ToInt() });
                    }
                }

                //Call to update the name and adderss types for a client. 
                CompanyNumber cn = DataAccess.GetEntityByColumnValue<CompanyNumber>("CompanyNumber1", companynumber);

                int companyNumberId = cn.Id;
                long longClient = clientid.ToT<long>();
                BCS.Core.Clearance.Clients.UpdateClientNameAddressTypes(companyNumberId, longClient, clientAddressIDsAndTypes, clientNameIdsAndTypes);

                //Biz.SubmissionLineOfBusiness slob = submission.NewSubmissionLineOfBusiness();
                //slob.LineOfBusinessId = lineofbusiness;            

                string[] lineofbusinesses = lobids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                List<LineOfBusiness> nlobs = DataAccess.GetEntityCollectionById<LineOfBusiness>(lineofbusinesses);
                foreach (LineOfBusiness nlob in nlobs)
                {
                    var lob = new LineOfBusiness() { Id = nlob.Id };
                    bc.LineOfBusiness.Attach(lob);
                    submission.LineOfBusiness.Add(lob);
                }

                List<CompanyNumberCode> ncnc = DataAccess.GetEntityCollectionById<CompanyNumberCode>(companycodeids);

                foreach (CompanyNumberCode iid in ncnc)
                {
                    CompanyNumberCode code = new CompanyNumberCode() { Id = iid.Id };
                    bc.CompanyNumberCode.Attach(code);
                    submission.CompanyNumberCode.Add(code);
                }

                string[] tattributes = attributes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string ta in tattributes)
                {
                    string[] ts = ta.Split("=".ToCharArray());
                    submission.SubmissionCompanyNumberAttributeValue.Add(new SubmissionCompanyNumberAttributeValue() { CompanyNumberAttributeId = ts[0].ToInt(), AttributeValue = ts[1] });
                }

                #region Submission Line Of Business Children

                if (lobchildren == null)
                    lobchildren = string.Empty;
                string[] lobchildrenarray = lobchildren.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string lobchild in lobchildrenarray)
                {
                    string[] lobchildparts = lobchild.Split("=".ToCharArray(), StringSplitOptions.None);

                    // index 0 contains Id, index length-1 contains deleted
                    if (lobchildparts[0] == "0" && lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
                        continue;

                    SubmissionLineOfBusinessChildren nslobc = new SubmissionLineOfBusinessChildren();
                    submission.SubmissionLineOfBusinessChildren.Add(nslobc);

                    nslobc.CompanyNumberLOBGridId = Convert.ToInt32(lobchildparts[1]);
                    nslobc.SubmissionStatusId = lobchildparts[2] == "0" ? null : lobchildparts[2].ToT<int?>();
                    nslobc.SubmissionStatusReasonId = lobchildparts[3] == "0" ? null : lobchildparts[3].ToT<int?>();
                }


                #endregion

                #region Submission Comments

                if (comments == null)
                    comments = string.Empty;
                string[] lobcommentarray = comments.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string lobchild in lobcommentarray)
                {
                    string[] lobchildparts = lobchild.Split(new string[] { "=====" }, StringSplitOptions.None);

                    // index 0 contains Id, index length-1 contains deleted
                    if (lobchildparts[0] == "0" && lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
                        continue;

                    SubmissionComment nslobc = new SubmissionComment();
                    submission.SubmissionComment.Add(nslobc);
                    nslobc.Comment = lobchildparts[1];
                    nslobc.EntryBy = lobchildparts[2];
                    nslobc.EntryDt = Convert.ToDateTime(lobchildparts[3]);
                }


                #endregion

                if (Common.IsNotNullOrEmpty(agentreasonids))
                {
                    string[] tagentreasonids = agentreasonids.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string tarids in tagentreasonids)
                    {
                        CompanyNumberAgentUnspecifiedReason agentReason = new CompanyNumberAgentUnspecifiedReason() { Id = tarids.ToInt() };
                        bc.CompanyNumberAgentUnspecifiedReason.Attach(agentReason);
                        submission.CompanyNumberAgentUnspecifiedReason.Add(agentReason);
                    }
                }

                submission.SystemDt = dtNow;

                bc.Submission.AddObject(submission);
                bc.SaveChanges();

                if (submission != null)
                {
                    s = BuildSubmission(submission);
                    s.Message += Messages.SuccessfulAddSubmission;
                    s.Status = "Success";

                    // remove the cache dependency used to cache submissions for page
                    System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
                    return XML.Serializer.SerializeAsXml(s);
                }
                else
                {
                    s.Message += Messages.UnsuccessfulAddSubmission;
                    s.Status = "Failure";
                    return XML.Serializer.SerializeAsXml(s);
                }
            }
        }


        #endregion

        #region Modify Submissions

        public static string ModifySubmission(string id, long clientid, string clientaddressids, string clientnameids,
           long clientportfolioid, int companyid, int typeid, int agencyid, int agentid, string agentreasonids, int contactid, int companynumberid,
           int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string cocodeids, string attributes, decimal premium,
           string effective, string expiration, int underwriterid, int analystid, int technicianid, int policysystemid, string comments, string updatedby,
           string lineofbusinessesids, string lobchildren, string insuredname, string dba, int othercarrierid, decimal othercarrierpremium)
        {
            return ModifySubmission(id, clientid, clientaddressids, clientnameids,
             clientportfolioid, companyid, typeid, agencyid, agentid, agentreasonids, contactid, companynumberid,
             statusid, statusdate, statusreasonid, statusreasonother, policynumber, cocodeids, attributes, premium,
             effective, expiration, underwriterid, analystid, technicianid, policysystemid, comments, updatedby,
             lineofbusinessesids, lobchildren, insuredname, dba, othercarrierid, othercarrierpremium
            , null, null);
        }

        /// <summary>
        /// most parameters by Id
        /// </summary>       
        public static string ModifySubmission(string id, long clientid, string clientaddressids, string clientnameids,
            long clientportfolioid, int companyid, int typeid, int agencyid, int agentid, string agentreasonids, int contactid, int companynumberid,
            int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string cocodeids, string attributes, decimal premium,
            string effective, string expiration, int underwriterid, int analystid, int technicianid, int policysystemid, string comments, string updatedby,
            string lineofbusinessesids, string lobchildren, string insuredname, string dba, int othercarrierid, decimal othercarrierpremium
            , string clientAddressIDsAndTypes, string clientNameIdsAndTypes)
        {
            string requiredMessagePrefix = "Required :-";

            if (!Common.IsNotNullOrEmpty(id))
            {
                BizObjects.Submission _submission = new BCS.Core.Clearance.BizObjects.Submission();
                _submission.Message = requiredMessagePrefix + "Id.";
                _submission.Status = "Failure";
                return XML.Serializer.SerializeAsXml(_submission);
            }

            if (!Common.IsNotNullOrEmpty(updatedby))
            {
                BizObjects.Submission _submission = new BCS.Core.Clearance.BizObjects.Submission();
                _submission.Message = requiredMessagePrefix + "updated by.";
                _submission.Status = "Failure";
                return XML.Serializer.SerializeAsXml(_submission);
            }
            if (agencyid == 0)
            {
                BizObjects.Submission _submission = new BCS.Core.Clearance.BizObjects.Submission();
                _submission.Message = requiredMessagePrefix + "Agency.";
                _submission.Status = "Failure";
                return XML.Serializer.SerializeAsXml(_submission);
            }

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                int subId = 0;
                Int32.TryParse(id, out subId);
                var submission = bc.Submission.Where(s => s.Id == subId).SingleOrDefault();

                if (submission == null)
                {
                    BizObjects.Submission _submission = new BCS.Core.Clearance.BizObjects.Submission();
                    _submission.Message = string.Format("Submission with Id {0} does not exist.", id);
                    _submission.Status = "Failure";
                    return XML.Serializer.SerializeAsXml(_submission);
                }

                // required by db schema
                if (agencyid != 0)
                    submission.AgencyId = agencyid;
                if (agentid != 0)
                    submission.AgentId = agentid;
                else
                    submission.AgentId = null;

                if (contactid != 0)
                    submission.ContactId = contactid;
                else
                    submission.ContactId = null;

                if (clientid != 0)
                    submission.ClientCoreClientId = BOA.Util.Common.GetSafeT<long>(clientid);
                else
                    submission.ClientCoreClientId = 0;

                // required by db schema
                if (companynumberid != 0)
                    submission.CompanyNumberId = companynumberid;

                submission.EffectiveDate = BOA.Util.Common.GetSafeT<DateTime>(effective);

                submission.EstimatedWrittenPremium = premium;
                submission.OtherCarrierPremium = othercarrierpremium;

                if (Common.IsNotNullOrEmpty(expiration))
                    submission.ExpirationDate = BOA.Util.Common.GetSafeT<DateTime>(expiration);
                else
                    submission.ExpirationDate = null;

                submission.PolicyNumber = policynumber.Trim();

                submission.InsuredName = insuredname;

                submission.Dba = dba;

                if (clientportfolioid != 0)
                    submission.ClientCorePortfolioId = BOA.Util.Common.GetSafeT<long>(clientportfolioid);
                else
                    submission.ClientCorePortfolioId = submission.ClientCoreClientId;

                #region Status and StatusHistory update
                // TODO: analyze #790 #952
                int? prevStatus = submission.SubmissionStatusId;
                if ((prevStatus == null && statusid == 0) && (submission.SubmissionStatusDate == BOA.Util.Common.GetSafeT<DateTime>(statusdate)))
                { }
                else
                {
                    int prevstatus = prevStatus == null ? 0 : prevStatus.Value;
                    if (prevstatus != statusid || (submission.SubmissionStatusDate == null) || submission.SubmissionStatusDate != BOA.Util.Common.GetSafeT<DateTime>(statusdate))
                    {
                        var submissionStatus = bc.SubmissionStatus.Where(s => s.Id == statusid);

                        if (submissionStatus != null)
                        {
                            SubmissionStatusHistory statusHis = new SubmissionStatusHistory();
                            statusHis.PreviousSubmissionStatusId = statusid;
                            statusHis.StatusChangeBy = updatedby;
                            statusHis.StatusChangeDt = BOA.Util.Common.GetSafeT<DateTime>(statusdate);

                            submission.SubmissionStatusHistory.Add(statusHis);
                        }
                    }
                }
                if (statusid != 0)
                    submission.SubmissionStatusId = statusid;
                else
                    submission.SubmissionStatusId = null;
                if (Common.IsNotNullOrEmpty(statusdate))
                    submission.SubmissionStatusDate = BOA.Util.Common.GetSafeT<DateTime>(statusdate);
                else
                    submission.SubmissionStatusDate = null;

                if (statusreasonid != 0)
                    submission.SubmissionStatusReasonId = statusreasonid;
                else
                    submission.SubmissionStatusReasonId = null;

                submission.SubmissionStatusReasonOther = statusreasonother;
                #endregion

                if (Common.IsNotNullOrEmpty(attributes))
                {
                    string[] sa = attributes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string ts in sa)
                    {
                        string[] ta = ts.Split("=".ToCharArray());

                        int attrId = 0;
                        int.TryParse(ta[0], out attrId);

                        SubmissionCompanyNumberAttributeValue attrVal = submission.SubmissionCompanyNumberAttributeValue.Where(a => a.CompanyNumberAttributeId == attrId).FirstOrDefault();
                        if (attrVal != null)
                        {
                            if (attrVal.AttributeValue == ta[1])
                                continue;
                            else
                            {
                                attrVal.AttributeValue = ta[1];
                            }
                        }
                        else
                        {
                            submission.SubmissionCompanyNumberAttributeValue.Add(new SubmissionCompanyNumberAttributeValue() { CompanyNumberAttributeId = attrId, AttributeValue = ta[1] });
                        }
                    }
                }
                // required by schema
                if (typeid != 0)
                    submission.SubmissionTypeId = typeid;

                if (underwriterid != 0)
                    submission.UnderwriterPersonId = underwriterid;
                else
                    submission.UnderwriterPersonId = null;
                if (analystid != 0)
                    submission.AnalystPersonId = analystid;
                else
                    submission.AnalystPersonId = null;

                if (technicianid != 0)
                    submission.TechnicianPersonId = technicianid;
                else
                    submission.TechnicianPersonId = null;

                if (othercarrierid != 0)
                    submission.OtherCarrierId = othercarrierid;
                else
                    submission.OtherCarrierId = null;

                if (policysystemid != 0)
                    submission.PolicySystemId = policysystemid;
                else
                    submission.PolicySystemId = null;

                submission.UpdatedBy = updatedby;

                submission.UpdatedDt = DateTime.Now;

                if (Common.IsNotNullOrEmpty(lineofbusinessesids))
                {
                    int lei = submission.LineOfBusiness.Count();

                    for (int i = 0; i < lei; i++)
                    {
                        LineOfBusiness lob = bc.LineOfBusiness.Where(c => c.Id == submission.LineOfBusiness.ElementAt((lei - i) - 1).Id).SingleOrDefault();
                        bc.LineOfBusiness.Attach(lob);
                        submission.LineOfBusiness.Remove(lob);
                    }

                    bc.SaveChanges();

                    string[] slineofbusinessesids = lineofbusinessesids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    int[] ilineofbusinessesids = new int[slineofbusinessesids.Length];
                    for (int i = 0; i < slineofbusinessesids.Length; i++)
                    {
                        ilineofbusinessesids[i] = Convert.ToInt32(slineofbusinessesids[i]);
                    }

                    var lobs = bc.LineOfBusiness.Where(l => ilineofbusinessesids.Contains(l.Id));


                    foreach (LineOfBusiness lob in lobs)
                    {
                        submission.LineOfBusiness.Add(lob);
                    }
                }

                #region New Address handling
                int le = submission.SubmissionClientCoreClientAddress.Count();

                for (int i = le - 1; i >= 0; i--)
                {
                    submission.SubmissionClientCoreClientAddress.Remove(submission.SubmissionClientCoreClientAddress.ElementAt(i));
                }

                if (Common.IsNotNullOrEmpty(clientaddressids))
                {
                    string[] arrAddressids = clientaddressids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string anAddressId in arrAddressids)
                    {
                        submission.SubmissionClientCoreClientAddress.Add(new SubmissionClientCoreClientAddress() { ClientCoreClientAddressId = anAddressId.ToInt() });
                    }
                }

                #endregion
                #region New Name handling
                le = submission.SubmissionClientCoreClientName.Count();

                for (int i = le - 1; i >= 0; i--)
                {
                    submission.SubmissionClientCoreClientName.Remove(submission.SubmissionClientCoreClientName.ElementAt(i));
                }

                bc.SaveChanges();

                if (Common.IsNotNullOrEmpty(clientnameids))
                {
                    string[] arrNameids = clientnameids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string aNameId in arrNameids)
                    {
                        submission.SubmissionClientCoreClientName.Add(new SubmissionClientCoreClientName() { ClientCoreNameId = aNameId.ToInt() });
                    }
                }

                #endregion


                //ModifySubmissionCompanyNumberCodes(submission, cocodeids);
                if (!string.IsNullOrWhiteSpace(cocodeids))
                {
                    string[] cncodes = cocodeids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    var currentCodes = submission.CompanyNumberCode;
                    var newCodes = bc.CompanyNumberCode.Where(c => cncodes.Select(o => o.ToInt()).Contains(c.Id));

                    var codesToDelete = currentCodes.Where(c => !newCodes.Contains(c));
                    var codesToAdd = newCodes.Where(c => !currentCodes.Contains(c));

                    while (codesToDelete.Count() > 0)
                        submission.CompanyNumberCode.Remove(codesToDelete.First());

                    while (codesToAdd.Count() > 0)
                        submission.CompanyNumberCode.Add(codesToAdd.First());
                }


                //Call to update the name and adderss types for a client. 
                BCS.Core.Clearance.Clients.UpdateClientNameAddressTypes(companynumberid, clientid, clientAddressIDsAndTypes, clientNameIdsAndTypes);

                #region Submission Line Of Business Children

                if (Common.IsNotNullOrEmpty(lobchildren))
                {
                    string[] lobchildrenarray = lobchildren.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string lobchild in lobchildrenarray)
                    {
                        string[] lobchildparts = lobchild.Split("=".ToCharArray(), StringSplitOptions.None);

                        // index 0 contains Id, index length-1 contains deleted
                        if (lobchildparts[0] == "0")
                        {
                            if (lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
                                continue;
                            else
                            {
                                submission.SubmissionLineOfBusinessChildren.Add(new SubmissionLineOfBusinessChildren() { CompanyNumberLOBGridId = lobchildparts[1].ToInt(), SubmissionStatusId = lobchildparts[2].ToInt(), SubmissionStatusReasonId = lobchildparts[3].ToInt() });
                            }
                        }
                        else
                        {
                            if (lobchildparts[lobchildparts.Length - 1].ToLower() == "true")
                            {
                                submission.SubmissionLineOfBusinessChildren.Remove(submission.SubmissionLineOfBusinessChildren.Where(l => l.Id == lobchildparts[0].ToInt()).SingleOrDefault());
                            }
                            else
                            {
                                var slobc = submission.SubmissionLineOfBusinessChildren.Where(l => l.Id == lobchildparts[0].ToInt()).SingleOrDefault();
                                if (slobc != null)
                                {
                                    slobc.CompanyNumberLOBGridId = lobchildparts[1].ToInt();
                                    slobc.SubmissionStatusId = lobchildparts[2].ToInt();
                                    slobc.SubmissionStatusReasonId = lobchildparts[3].ToInt();
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Submission Comment

                if (Common.IsNotNullOrEmpty(comments))
                {
                    string[] lobcommentrenarray = comments.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string lobcomment in lobcommentrenarray)
                    {
                        string[] lobcommentparts = lobcomment.Split(new string[] { "=====" }, StringSplitOptions.None);

                        // index 0 contains Id, index length-1 contains deleted
                        if (lobcommentparts[0] == "0")
                        {
                            if (lobcommentparts[lobcommentparts.Length - 1].ToLower() == "true")
                                continue;
                            else
                            {
                                submission.SubmissionComment.Add(new SubmissionComment() { Comment = lobcommentparts[1], EntryBy = lobcommentparts[2], EntryDt = lobcommentparts[3].ToT<DateTime>() });
                            }
                        }
                        else
                        {
                            if (lobcommentparts[lobcommentparts.Length - 1].ToLower() == "true")
                            {
                                submission.SubmissionComment.Remove(submission.SubmissionComment.Where(s => s.Id == lobcommentparts[0].ToInt()).SingleOrDefault());
                            }
                            else
                            {
                                SubmissionComment comment = submission.SubmissionComment.Where(c => c.Id == lobcommentparts[0].ToInt()).SingleOrDefault();

                                if (comment.Comment != lobcommentparts[1])
                                {
                                    comment.Comment = lobcommentparts[1];
                                    comment.ModifiedBy = lobcommentparts[2];
                                    comment.ModifiedDt = Convert.ToDateTime(lobcommentparts[3]);
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Agent Unspecified reasons

                string[] tagentreasonids = agentreasonids.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                var oldAgentVals = submission.CompanyNumberAgentUnspecifiedReason;
                var newAgentVals = bc.CompanyNumberAgentUnspecifiedReason.Where(c => tagentreasonids.Select(r => r.ToInt()).Contains(c.Id));

                var deleteAgentVals = oldAgentVals.Where(o => !newAgentVals.Contains(o));
                var addAgentVals = newAgentVals.Where(n => !oldAgentVals.Contains(n));

                while (deleteAgentVals.Count() > 0)
                    submission.CompanyNumberAgentUnspecifiedReason.Remove(deleteAgentVals.First());

                while (addAgentVals.Count() > 0)
                    submission.CompanyNumberAgentUnspecifiedReason.Add(addAgentVals.First());

                #endregion

                bc.SaveChanges();

                BizObjects.Submission sub = BuildSubmission(submission);
                sub.Message += Messages.SuccessfulEditSubmission;
                sub.Status = "Success";
                // remove the cache dependency used to cache submissions for page
                System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
                return XML.Serializer.SerializeAsXml(sub);
            }

        }

        public static void CopySubmissionStatus(int srcSubmissionId, int tgtSubmissionId)
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                var srcStatuses = bc.SubmissionStatusHistory.Where(s => s.SubmissionId == srcSubmissionId).OrderByDescending(s => s.StatusChangeDt);
                var tgtStatuses = bc.SubmissionStatusHistory.Where(s => s.SubmissionId == tgtSubmissionId);

                while (tgtStatuses.Count() > 0)
                    bc.SubmissionStatusHistory.DeleteObject(tgtStatuses.First());

                double i = 1.0;
                foreach (var item in srcStatuses)
                {
                    if (!item.Active)
                        continue;

                    SubmissionStatusHistory his = new SubmissionStatusHistory();
                    his.Active = true;
                    his.StatusChangeDt = DateTime.Now.AddMinutes(i * -1);
                    his.PreviousSubmissionStatusId = item.PreviousSubmissionStatusId;
                    his.StatusChangeBy = item.StatusChangeBy;
                    his.SubmissionStatusDate = DateTime.Now.Date;
                    his.SubmissionId = tgtSubmissionId;

                    bc.SubmissionStatusHistory.AddObject(his);
                    i++;
                }
                var tgtSubmission = bc.Submission.Where(s => s.Id == tgtSubmissionId).SingleOrDefault();
                var srcSubmission = bc.Submission.Where(s => s.Id == srcSubmissionId).SingleOrDefault();

                tgtSubmission.SubmissionStatusId = srcSubmission.SubmissionStatusId;
                tgtSubmission.SubmissionStatusReasonId = srcSubmission.SubmissionStatusReasonId;

                bc.SaveChanges();
            }
        }

        ////commented 8/18/2011 - tg - not sure it's needed
        //private static void ModifySubmissionCompanyNumberCodes(Submission submission, string codeids)
        //{
        //    Dictionary<int, int> oldValues = new Dictionary<int,int>();
        //    foreach (CompanyNumberCode cnc in submission.CompanyNumberCode)
        //    {
        //        oldValues.Add(cnc.CompanyCodeTypeId, cnc.Id);
        //    }

        //    Dictionary<int, int> newValues = new Dictionary<int, int>();
        //    List<CompanyNumberCode> coll = DataAccess.GetEntityCollectionById<CompanyNumberCode>(codeids.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));

        //    foreach (CompanyNumberCode cnc in coll)
        //    {
        //        newValues.Add(cnc.CompanyCodeTypeId, cnc.Id);
        //    }

        //    List<int> toBeInserted = new List<int>();
        //    List<int> toBeDeleted = new List<int>();
        //    Dictionary<int, int[]> toBeUpdated = new Dictionary<int,int[]>();


        //    foreach (int intVar in oldValues.Keys)
        //    {
        //        if (newValues.ContainsKey(intVar))
        //        {
        //            if (newValues[intVar] == oldValues[intVar])
        //            {
        //                /* leave alone -  */
        //            }
        //            else
        //            {
        //                toBeUpdated.Add(intVar, new int[] { oldValues[intVar], newValues[intVar] });
        //            }
        //        }
        //        else
        //            toBeDeleted.Add(oldValues[intVar]);
        //    }
        //    foreach (int intVar in newValues.Keys)
        //    {
        //        if (oldValues.ContainsKey(intVar))
        //        { /* we have already taken care of this condition in previous loop */ }
        //        else
        //        {
        //            toBeInserted.Add(newValues[intVar]);
        //        }
        //    }

        //    foreach (int[] intVar in toBeUpdated.Values)
        //    {
        //        submission.SubmissionCompanyNumberCodes.FindByCompanyNumberCodeId(intVar[0]).CompanyNumberCodeId = intVar[1];

        //    }
        //    foreach (int intVar in toBeInserted)
        //    {
        //        Biz.SubmissionCompanyNumberCode sCode = submission.NewSubmissionCompanyNumberCode();
        //        sCode.CompanyNumberCodeId = intVar;
        //    }
        //    foreach (int intVar in toBeDeleted)
        //    {
        //        submission.SubmissionCompanyNumberCodes.FindByCompanyNumberCodeId(intVar).Delete();
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientid"></param>
        /// <param name="portfoilioid"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string UpdatePortfolioId(string companyNumber, long clientid, long portfoilioid, string user)
        {
            if (portfoilioid == 0)
                return string.Empty;

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                var submissions = bc.Submission.Where(s => s.CompanyNumber.CompanyNumber1 == companyNumber && s.ClientCoreClientId == clientid);

                foreach (var submission in submissions)
                {
                    if (portfoilioid == 0 || (submission.ClientCorePortfolioId == null && submission.ClientCorePortfolioId.Value == portfoilioid))
                        continue;

                    submission.ClientCorePortfolioId = portfoilioid;
                    submission.UpdatedDt = DateTime.Now;
                    submission.UpdatedBy = user;
                }

                bc.SaveChanges();
            }

            return string.Empty;
        }

        /// <summary>
        /// deletes existing addresses and adds new ones provided
        /// </summary>
        /// <param name="submissionId"></param>
        /// <param name="addressids"></param>
        /// <returns></returns>
        public static string UpdateSubmissionClientAddresses(int submissionId, string clientaddressids)
        {

            using (BCSContext bc = DataAccess.NewBCSContext())
            {

                var submission = bc.Submission.Where(s => s.Id == submissionId).SingleOrDefault();

                while (submission.SubmissionClientCoreClientAddress.Count > 0)
                    submission.SubmissionClientCoreClientAddress.Remove(submission.SubmissionClientCoreClientAddress.First());

                try
                {
                    bc.SaveChanges();
                }
                catch (Exception)
                {
                    return "There was a problem updating the client core addresses.";
                }

                if (Common.IsNotNullOrEmpty(clientaddressids))
                {
                    string[] arrAddressids = clientaddressids.Split("|".ToCharArray());
                    foreach (string anAddressId in arrAddressids)
                    {
                        submission.SubmissionClientCoreClientAddress.Add(new SubmissionClientCoreClientAddress() { ClientCoreClientAddressId = anAddressId.ToInt() });
                    }
                }
                try
                {
                    bc.SaveChanges();
                }
                catch (Exception)
                {
                    return "There was a problem updating the client core addresses.";
                }
                System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
                return "Addresses were successfully updated.";
            }
        }

        /// <summary>
        /// switches all sequences of multiple submission's to a new client
        /// </summary>
        /// <param name="companyNumber">the company number which the submissions belong</param>
        /// <param name="numbers">submission or policy numbers</param>
        /// <param name="clientInfo">: delimited client information. clientid:nameids:addressids:insuredname:dba. insuredname and dba are not updated if empty</param>
        /// <param name="numberType">SubmissionNumber or PolicyNumber</param>
        /// <returns>serialized response</returns>
        /// TG - 8/22/2011 - To Do
        //public string SwitchClient(string companyNumber, string numbers, string clientInfo, string numberType)
        //{
        //    if (string.IsNullOrEmpty(companyNumber))
        //    {
        //        BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
        //        fResponse.Message = "Company Number is required.";
        //        fResponse.Status = "Failure";
        //        return XML.Serializer.SerializeAsXml(fResponse);
        //    }
        //    if (string.IsNullOrEmpty(numbers) || string.IsNullOrEmpty(clientInfo))
        //    {
        //        BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
        //        fResponse.Message = string.IsNullOrEmpty(numbers) ? "No submission numbers were passed. Unable to process." : "No client ids were passed. Unable to process.";
        //        fResponse.Status = "Failure";
        //        return XML.Serializer.SerializeAsXml(fResponse);
        //    }
        //    string[] numbersStringArray = numbers.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        //    long[] numbersArray = new long[numbersStringArray.Length];
        //    switch (numberType)
        //    {
        //        case "SubmissionNumber":
        //            {
        //                try
        //                {
        //                    numbersArray = Array.ConvertAll<string, long>(numbersStringArray, new Converter<string, long>(Convert.ToInt64));
        //                }
        //                catch (Exception)
        //                {
        //                    BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
        //                    fResponse.Message = "Invalid Submission number(s).";
        //                    fResponse.Status = "Failure";
        //                    return XML.Serializer.SerializeAsXml(fResponse);
        //                }
        //                break;
        //            }
        //        default:
        //            break;
        //    }

        //    SqlTransaction transaction = null;
        //    Biz.SubmissionCollection bizSubmissions = new SubmissionCollection();
        //    Biz.DataManager dm = Common.GetDataManager(out transaction);
        //    dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
        //    switch (numberType)
        //    {
        //        case "SubmissionNumber":
        //            {
        //                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.SubmissionNumber, numbersStringArray, MatchType.In);
        //                break;
        //            }
        //        case "PolicyNumber":
        //            {
        //                dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.PolicyNumber, numbersStringArray, MatchType.In);
        //                break;
        //            }
        //        default:
        //            break;
        //    }
        //    try
        //    {
        //        bizSubmissions = dm.GetSubmissionCollection(Biz.FetchPath.Submission.SubmissionClientCoreClientAddress,
        //                Biz.FetchPath.Submission.SubmissionClientCoreClientName);
        //    }
        //    catch (Exception)
        //    {
        //        BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
        //        fResponse.Message = "There was a problem retrieving the submissions. Please try again.";
        //        fResponse.Status = "Failure";
        //        return XML.Serializer.SerializeAsXml(fResponse);
        //    }

        //    List<string> notFoundList = new List<string>();

        //    for (int i = 0; i < numbersStringArray.Length; i++)
        //    {
        //        MethodInfo mi = typeof(BCS.Biz.SubmissionCollection).GetMethod("FilterBy" + numberType, new Type[] { typeof(string) });
        //        Biz.SubmissionCollection filtered = (Biz.SubmissionCollection)mi.Invoke(bizSubmissions, new object[] { numbersStringArray[i] });

        //        if (null == filtered || 0 == filtered.Count)
        //        {
        //            notFoundList.Add(numbersStringArray[i]);
        //            continue;
        //        }
        //        string clientInfoString = clientInfo.Trim(',');
        //        {
        //            string[] clientInfoArray = clientInfoString.Split(":".ToCharArray(), StringSplitOptions.None);
        //            if (clientInfoArray.Length != 5) // client id, name ids, address ids, insured name, dba
        //            {
        //                BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
        //                fResponse.Message = "Invalid client information passed.";
        //                fResponse.Status = "Failure";
        //                return XML.Serializer.SerializeAsXml(fResponse);
        //            }
        //            long clientIdLong = 0;
        //            try
        //            {
        //                // validate client id
        //                clientIdLong = Convert.ToInt64(clientInfoArray[0]);
        //            }
        //            catch (FormatException)
        //            {
        //                BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
        //                fResponse.Message = "Invalid Client Id(s).";
        //                fResponse.Status = "Failure";
        //                return XML.Serializer.SerializeAsXml(fResponse);
        //            }
        //            foreach (Biz.Submission bizSubmission in filtered)
        //            {
        //                bizSubmission.ClientCoreClientId = clientIdLong;
        //            }
        //            #region name handling
        //            string[] clientNameIdArray = clientInfoArray[1].Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        //            int[] clientNameIdArrayInt = new int[clientNameIdArray.Length];
        //            try
        //            {
        //                // validate client name id
        //                clientNameIdArrayInt = Array.ConvertAll<string, int>(clientNameIdArray, new Converter<string, int>(Convert.ToInt32));
        //            }
        //            catch (Exception)
        //            {
        //                BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
        //                fResponse.Message = "Invalid Client Name Id(s).";
        //                fResponse.Status = "Failure";
        //                return XML.Serializer.SerializeAsXml(fResponse);
        //            }
        //            #region delete old name assocs
        //            foreach (Biz.Submission bizSubmission in filtered)
        //            {
        //                int nameCount = bizSubmission.SubmissionClientCoreClientNames.Count;

        //                for (int nameIndex = 0; nameIndex < nameCount; nameIndex++)
        //                {
        //                    bizSubmission.SubmissionClientCoreClientNames[(nameCount - nameIndex) - 1].Delete();
        //                }
        //            }
        //            dm.CommitAll();
        //            #endregion
        //            #region add new name assocs
        //            foreach (Biz.Submission bizSubmission in filtered)
        //            {
        //                foreach (int nameIdInt in clientNameIdArrayInt)
        //                {
        //                    Biz.SubmissionClientCoreClientName newName = bizSubmission.NewSubmissionClientCoreClientName();
        //                    newName.ClientCoreNameId = nameIdInt;
        //                }
        //            }
        //            #endregion
        //            #endregion
        //            #region address handling
        //            string[] clientaddressIdArray = clientInfoArray[2].Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        //            int[] clientaddressIdArrayInt = new int[clientaddressIdArray.Length];
        //            try
        //            {
        //                // validate client address id
        //                clientaddressIdArrayInt = Array.ConvertAll<string, int>(clientaddressIdArray, new Converter<string, int>(Convert.ToInt32));
        //            }
        //            catch (Exception)
        //            {
        //                BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
        //                fResponse.Message = "Invalid Client address Id(s).";
        //                fResponse.Status = "Failure";
        //                return XML.Serializer.SerializeAsXml(fResponse);
        //            }
        //            #region delete old address assocs
        //            foreach (Biz.Submission bizSubmission in filtered)
        //            {
        //                int addressCount = bizSubmission.SubmissionClientCoreClientAddresss.Count;

        //                for (int addressIndex = 0; addressIndex < addressCount; addressIndex++)
        //                {
        //                    bizSubmission.SubmissionClientCoreClientAddresss[(addressCount - addressIndex) - 1].Delete();
        //                }
        //            }
        //            dm.CommitAll();
        //            #endregion
        //            #region add new address assocs
        //            foreach (Biz.Submission bizSubmission in filtered)
        //            {
        //                foreach (int addressIdInt in clientaddressIdArrayInt)
        //                {
        //                    Biz.SubmissionClientCoreClientAddress newaddress = bizSubmission.NewSubmissionClientCoreClientAddress();
        //                    newaddress.ClientCoreClientAddressId = addressIdInt;
        //                }
        //            }
        //            #endregion
        //            #endregion

        //            if (!string.IsNullOrEmpty(clientInfoArray[3]))
        //            {
        //                foreach (Biz.Submission bizSubmission in filtered)
        //                {
        //                    bizSubmission.InsuredName = clientInfoArray[3];
        //                }
        //            }
        //            if (!string.IsNullOrEmpty(clientInfoArray[4]))
        //            {
        //                foreach (Biz.Submission bizSubmission in filtered)
        //                {
        //                    bizSubmission.Dba = clientInfoArray[4];
        //                }
        //            }
        //        }
        //    }
        //    try
        //    {
        //        dm.CommitAll();
        //        transaction.Commit();
        //    }
        //    catch (Exception ex)
        //    {
        //        BCS.Core.Clearance.BizObjects.Response fResponse = new BCS.Core.Clearance.BizObjects.Response();
        //        fResponse.Message = "An unknown error occurred. Please try again.";
        //        fResponse.Status = "Failure";
        //        LogCentral.Current.LogFatal("Switch Clients", ex);
        //        transaction.Rollback();
        //        return XML.Serializer.SerializeAsXml(fResponse);
        //    }
        //    if (notFoundList.Count == 0)
        //    {
        //        BCS.Core.Clearance.BizObjects.Response sResponse = new BCS.Core.Clearance.BizObjects.Response();
        //        sResponse.Message = "Submissions were successfully switched.";
        //        sResponse.Status = "Success";
        //        return XML.Serializer.SerializeAsXml(sResponse);
        //    }
        //    else
        //    {
        //        BCS.Core.Clearance.BizObjects.Response sResponse = new BCS.Core.Clearance.BizObjects.Response();
        //        sResponse.Message = string.Format("Submissions were successfully switched. The following {0} submissions were not found.",
        //            string.Join(", ", notFoundList.ToArray()));
        //        sResponse.Status = "Success";
        //        return XML.Serializer.SerializeAsXml(sResponse);
        //    }            
        //}
        #endregion


        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="Id"></param>
        //public static void SetCompany(int Id)
        //{
        //    Common.SetCompany(Id);
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="companyNumber"></param>
        //public static void SetCompanyByCompanyNumber(int companyNumber)
        //{
        //    Biz.DataManager dm = Common.GetDataManager();
        //    dm.QueryCriteria.And(Biz.JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
        //    Biz.Company c = dm.GetCompany();
        //    if (c != null)
        //        SetCompany(dm.GetCompany().Id);
        //}

        #region Search submissions

        internal enum MatchCriteria
        {
            And = 1,
            Or
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public static string SearchSubmissions(string companyNumber, int rows)
        {
            return SearchSubmissionsByCompanyNumber(companyNumber, rows, 1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissions(string companyNumber, int pageSize, int pageNo)
        {
            return SearchSubmissionsByCompanyNumber(companyNumber, pageSize, pageNo);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="agencyNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByAgencyNumber(string companyNumber, string agencyNumber, int pageSize, int pageNo)
        {
            return SearchSubmissionsByAgencyNumber(companyNumber, agencyNumber, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByAgencyNumber(string companyNumber, string agencyNumber, int pageSize, int pageNo, bool includeDeleted)
        {
            EFilter f1 = new EFilter() { ColumnName = "Agency.AgencyNumber", Value = agencyNumber, MatchType = DAL.MatchType.Partial };
            EFilter f2 = new EFilter() { ColumnName = "Agency.CompanyNumber.CompanyNumber1", Value = companyNumber, MatchType = DAL.MatchType.Exact };

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1, f2);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="agencyName"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByAgencyName(string companyNumber, string agencyName, int pageSize, int pageNo)
        {
            return SearchSubmissionsByAgencyName(companyNumber, agencyName, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByAgencyName(string companyNumber, string agencyName, int pageSize, int pageNo, bool includeDeleted)
        {

            EFilter f1 = new EFilter() { ColumnName = "Agency.AgencyName", Value = agencyName, MatchType = DAL.MatchType.Partial };
            EFilter f2 = new EFilter() { ColumnName = "Agency.CompanyNumber.CompanyNumber1", Value = companyNumber, MatchType = DAL.MatchType.Exact };

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1, f2);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByCompanyNumber(string companyNumber, int pageSize, int pageNo)
        {
            return SearchSubmissionsByCompanyNumber(companyNumber, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByCompanyNumber(string companyNumber, int pageSize, int pageNo, bool includeDeleted)
        {

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="portfolioid"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByClientCoreClientPortfolioId(string companyNumber, long portfolioid, int pageSize, int pageNo)
        {
            return SearchSubmissionsByClientCoreClientPortfolioId(companyNumber, portfolioid, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByClientCoreClientPortfolioId(string companyNumber, long portfolioid, int pageSize, int pageNo, bool includeDeleted)
        {
            EFilter filter = new EFilter() { ColumnName = "ClientCorePortfolioId", MatchType = DAL.MatchType.Partial, Value = portfolioid.ToString() };

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, filter);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="clientid"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByClientCoreClientId(string companyNumber, long clientid, int pageSize, int pageNo)
        {
            return SearchSubmissionsByClientCoreClientId(companyNumber, clientid, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByClientCoreClientId(string companyNumber, long clientid, int pageSize, int pageNo, bool includeDeleted)
        {
            EFilter filter = new EFilter() { ColumnName = "ClientCoreClientId", MatchType = DAL.MatchType.Partial, Value = clientid.ToString() };

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, filter);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="clientid"></param>
        /// <param name="agencyid">agency Id for submissions are searched under its master umbrella</param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        /// 
        public static string SearchByClientCoreClientIdAndAgencyId(string companyNumber, long clientid, long agencyid, int pageSize, int pageNo)
        {
            return SearchByClientCoreClientIdAndAgencyId(companyNumber, clientid, agencyid, pageSize, pageNo, true);
        }
        public static string SearchByClientCoreClientIdAndAgencyId(string companyNumber, long clientid, long agencyid, int pageSize, int pageNo, bool includeDeleted)
        {
            List<EFilter> filters = new List<EFilter>();
            filters.Add(new EFilter("CompanyNumber.CompanyNumber1", companyNumber));

            if (agencyid >= 0) // get only submissions tied to agency and its children
            {
                Biz.DataManager dm = Common.GetDataManager();
                dm.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agencyid);
                Biz.Agency masterAgency = dm.GetAgency(Biz.FetchPath.Agency.ChildRelationAgency);
                if (null == masterAgency)
                    return BuildEmptySubmissionList();
                long[] agencyIds = new long[masterAgency.ChildRelationAgencys.Count + 1];
                for (int i = 0; i < masterAgency.ChildRelationAgencys.Count; i++)
                {
                    agencyIds[i] = masterAgency.ChildRelationAgencys[i].Id;
                }
                agencyIds[masterAgency.ChildRelationAgencys.Count] = agencyid;

                List<int> subIds = null;

                using (BCSContext bc = DataAccess.NewBCSContext())
                {
                    var subs = bc.Submission.Where(s => (s.AgencyId == agencyid || s.Agency.MasterAgencyId == agencyid)
                                                                    && s.ClientCoreClientId == clientid
                                                                    );
                    if (!includeDeleted)
                        subs = subs.Where(s => !s.Deleted);

                    subIds = subs.Select(s => s.Id).ToList();
                }

                if (subIds == null || subIds.Count == 0)
                    return BuildEmptySubmissionList();

                filters.Add(new EFilter("Id", subIds, DAL.MatchType.In));

                List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, filters.ToArray());

                BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

                return SerializeSubmissionList(sl);
            }
            else
            {
                filters.Add(new EFilter("ClientCoreClientId", clientid));
                filters.Add(new EFilter("CompanyNumber.CompanyNumber1", companyNumber));
                if (!includeDeleted)
                {
                    filters.Add(new EFilter("Deleted", false));
                }

                //List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, filters.ToArray());

                //BCS.Core.Clearance.BuildSubmissionList listBuilder = new Clearance.BuildSubmissionList(submissions, new string[] { }, new string[] { }, null, 0, 0, MatchCriteria.And);

                BCS.Core.Clearance.BuildSubmissionList listBuilder = new Clearance.BuildSubmissionList();

                BizObjects.SubmissionList sl = listBuilder.Build(filters, submissionRelations);

                //BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);
                return SerializeSubmissionList(sl);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="person"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByAgencyContactPerson(string companyNumber, string person, int pageSize, int pageNo)
        {
            return SearchSubmissionsByAgencyContactPerson(companyNumber, person, pageSize, pageNo, true);
        }

        public static string SearchSubmissionsByAgencyContactPerson(string companyNumber, string person, int pageSize, int pageNo, bool includeDeleted)
        {
            EFilter f1 = new EFilter() { ColumnName = "Agency.ContactPerson", Value = person, MatchType = DAL.MatchType.Partial };
            EFilter f2 = new EFilter() { ColumnName = "Agency.CompanyNumber.CompanyNumber1", Value = companyNumber, MatchType = DAL.MatchType.Exact };

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1, f2);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="statusCode"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsBySubmissionStatus(string companyNumber, string statusCode, int pageSize, int pageNo)
        {
            return SearchSubmissionsBySubmissionStatus(companyNumber, statusCode, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsBySubmissionStatus(string companyNumber, string statusCode, int pageSize, int pageNo, bool includeDeleted)
        {
            EFilter f1 = new EFilter() { ColumnName = "SubmissionStatus.StatusCode", Value = statusCode, MatchType = DAL.MatchType.Partial };
            EFilter f2 = new EFilter() { ColumnName = "CompanyNumber.CompanyNumber1", Value = companyNumber, MatchType = DAL.MatchType.Exact };

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1, f2);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="number"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsBySubmissionNumber(string companyNumber, long number, int pageSize, int pageNo, bool includeDeleted)
        {

            EFilter f1 = new EFilter() { ColumnName = "SubmissionNumber", Value = number };

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="number"></param>
        /// <param name="sequence"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsBySubmissionNumberAndSequence(string companyNumber, long number, int sequence, int pageSize, int pageNo, bool includeDeleted)
        {

            EFilter f1 = new EFilter("SubmissionNumber", number);
            EFilter f2 = new EFilter("Sequence", sequence);

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1, f2);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="number"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByPolicyNumber(string companyNumber, string number, int pageSize, int pageNo, bool includeDeleted)
        {

            DAL.MatchType matchType = DAL.MatchType.Exact;
            if (companyNumber == "060")
            //if (companyNumber == "058" || companyNumber == "060" || companyNumber == "076" || companyNumber == "050" || companyNumber == "018")
            {
                matchType = DAL.MatchType.EndsWith;
            }

            EFilter f1 = new EFilter("PolicyNumber", number, matchType);
            // EFilter f1 = new EFilter("PolicyNumber", number,DAL.MatchType.Exact);

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByPremiumRange(string companyNumber, decimal from, decimal to, int pageSize, int pageNo, bool includeDeleted)
        {
            //return GetSubmissions(companyNumber, "EstimatedWrittenPremium", from, to, pageSize, pageNo);

            EFilter f1 = new EFilter("EstimatedWrittenPremium", from, DAL.MatchType.GreaterThanOrEqual);
            EFilter f2 = new EFilter("EstimatedWrittenPremium", to, DAL.MatchType.LessThanOrEqual);

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1, f2);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByEffectiveDateRange(string companyNumber, DateTime from, DateTime to, int pageSize, int pageNo, bool includeDeleted)
        {
            EFilter f1 = new EFilter("EffectiveDate", from, DAL.MatchType.GreaterThanOrEqual);
            EFilter f2 = new EFilter("EffectiveDate", to, DAL.MatchType.LessThanOrEqual);

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1, f2);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByExpirationDateRange(string companyNumber, DateTime from, DateTime to, int pageSize, int pageNo, bool includeDeleted)
        {
            //return GetSubmissions(companyNumber, "ExpirationDate", from, to, pageSize, pageNo);

            EFilter f1 = new EFilter("ExpirationDate", from, DAL.MatchType.GreaterThanOrEqual);
            EFilter f2 = new EFilter("ExpirationDate", to, DAL.MatchType.LessThanOrEqual);

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1, f2);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsBySubmissionDateRange(string companyNumber, DateTime from, DateTime to, int pageSize, int pageNo, bool includeDeleted)
        {
            EFilter f1 = new EFilter("SubmissionDt", from, DAL.MatchType.GreaterThanOrEqual);
            EFilter f2 = new EFilter("SubmissionDt", to, DAL.MatchType.LessThanOrEqual);

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1, f2);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="name"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByInsuredName(string companyNumber, string name, int pageSize, int pageNo)
        {
            return SearchSubmissionsByInsuredName(companyNumber, name, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByInsuredName(string companyNumber, string name, int pageSize, int pageNo, bool includeDeleted)
        {
            EFilter f1 = new EFilter("InsuredName", name, DAL.MatchType.Partial);

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="dba"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNo"></param>
        /// <returns></returns>
        public static string SearchSubmissionsByDBA(string companyNumber, string dba, int pageSize, int pageNo)
        {
            return SearchSubmissionsByDBA(companyNumber, dba, pageSize, pageNo, true);
        }
        public static string SearchSubmissionsByDBA(string companyNumber, string dba, int pageSize, int pageNo, bool includeDeleted)
        {
            EFilter f1 = new EFilter("Dba", dba, DAL.MatchType.Partial);

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, pageSize, pageNo);

            return SerializeSubmissionList(sl);
        }

        static bool itemRemoved = false;
        static System.Web.Caching.CacheItemRemovedReason reason;
        static System.Web.Caching.CacheItemRemovedCallback onRemove = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="k"></param>
        /// <param name="v"></param>
        /// <param name="r"></param>
        public static void RemovedCallback(String k, Object v, System.Web.Caching.CacheItemRemovedReason r)
        {
            itemRemoved = true;
            reason = r;
        }
        private static string SerializeSubmissionList(BizObjects.SubmissionList list)
        {
            return XML.Serializer.SerializeAsXml(list);
        }

        internal static BCS.Core.Clearance.BizObjects.Submission BuildSubmission(Submission s)
        {
            if (s == null)
                throw new ArgumentNullException();

            List<Submission> coll = new List<Submission>();
            coll.Add(s);

            BizObjects.SubmissionList list = BuildSubmissionList(coll, new string[] { }, new string[] { }, null, 0, 0);

            return list.List[0];
        }

        internal static BCS.Core.Clearance.BizObjects.SubmissionList BuildSubmissionList(
            List<Submission> collection, string[] properties, object[] values, OrmLib.CompareType[] comparers,
            int pageSize, int pageNo)
        {
            return BuildSubmissionList(collection, properties, values, comparers, pageSize, pageNo, MatchCriteria.And);
        }

        internal static BCS.Core.Clearance.BizObjects.SubmissionList BuildSubmissionList(
                List<int> collection, string[] properties, object[] values, OrmLib.CompareType[] comparers,
                int pageSize, int pageNo)
        {
            return BuildSubmissionList(collection, properties, values, comparers, pageSize, pageNo, MatchCriteria.And);
        }

        internal static BCS.Core.Clearance.BizObjects.SubmissionList BuildSubmissionList(
    List<int> collection, string[] properties, object[] values, OrmLib.CompareType[] comparers,
    int pageSize, int pageNo, MatchCriteria crit)
        {
            if (pageNo < 0)
                return new BizObjects.SubmissionList();
            if (pageNo == 0)
                pageSize = 0;
            if (pageNo > 0 && pageSize < 0)
                pageSize = 0;


            bool Match = properties.Length == 0;
            BizObjects.SubmissionList list = new BCS.Core.Clearance.BizObjects.SubmissionList();

            int min = Int32.MinValue;
            int max = Int32.MaxValue;
            if (pageSize > 0)
            {
                min = (pageNo - 1) * pageSize;
                max = pageNo * pageSize;
            }
            List<BizObjects.Submission> al = new List<BCS.Core.Clearance.BizObjects.Submission>();
            int i = 0;
            BizObjects.Submission bos;

            using (BCSContext bc = DataAccess.NewBCSContext())
            {

                foreach (int subId in collection)
                {
                    Submission _Submission = bc.Submission.Where(s => s.Id == subId).Single();
                    if (!Match)
                    {
                        switch (crit)
                        {
                            case MatchCriteria.And:
                                {
                                    Match = MatchAnd(_Submission, comparers, properties, values);
                                    break;
                                }
                            case MatchCriteria.Or:
                                {
                                    Match = MatchOr(_Submission, comparers, properties, values);
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    if (Match)
                    {
                        bos = new BCS.Core.Clearance.BizObjects.Submission();

                        bos.SubmissionNumberSequence = _Submission.Sequence;

                        if (_Submission.LineOfBusiness != null && _Submission.LineOfBusiness.Count > 0)
                        {
                            bos.LineOfBusinessId = _Submission.LineOfBusiness.ElementAt(0).Id;
                            bos.LineOfBusiness = _Submission.LineOfBusiness.ElementAt(0).Description;
                            bos.LineOfBusinessList = new BCS.Core.Clearance.BizObjects.LineOfBusiness[_Submission.LineOfBusiness.Count];
                            for (int j = 0; j < _Submission.LineOfBusiness.Count; j++)
                            {
                                bos.LineOfBusinessList[j] = new BCS.Core.Clearance.BizObjects.LineOfBusiness();
                                bos.LineOfBusinessList[j].Code = _Submission.LineOfBusiness.ElementAt(j).Code;
                                bos.LineOfBusinessList[j].Description = _Submission.LineOfBusiness.ElementAt(j).Description;
                                bos.LineOfBusinessList[j].Id = _Submission.LineOfBusiness.ElementAt(j).Id;
                                bos.LineOfBusinessList[j].Line = _Submission.LineOfBusiness.ElementAt(j).Line;
                            }
                        }

                        if (_Submission.SubmissionCompanyNumberAttributeValue != null && _Submission.SubmissionCompanyNumberAttributeValue.Count > 0)
                        {
                            bos.AttributeList = new BCS.Core.Clearance.BizObjects.SubmissionAttribute[_Submission.SubmissionCompanyNumberAttributeValue.Count];
                            for (int j = 0; j < _Submission.SubmissionCompanyNumberAttributeValue.Count; j++)
                            {
                                bos.AttributeList[j] = new BCS.Core.Clearance.BizObjects.SubmissionAttribute();
                                bos.AttributeList[j].AttributeValue = _Submission.SubmissionCompanyNumberAttributeValue.ElementAt(j).AttributeValue;
                                bos.AttributeList[j].CompanyNumberAttributeId = _Submission.SubmissionCompanyNumberAttributeValue.ElementAt(j).CompanyNumberAttributeId;
                                bos.AttributeList[j].Label = _Submission.SubmissionCompanyNumberAttributeValue.ElementAt(j).CompanyNumberAttribute.Label;
                                bos.AttributeList[j].DataType = _Submission.SubmissionCompanyNumberAttributeValue.ElementAt(j).CompanyNumberAttribute.AttributeDataType.DataTypeName;
                            }
                        }
                        if (_Submission.CompanyNumberAgentUnspecifiedReason != null && _Submission.CompanyNumberAgentUnspecifiedReason.Count > 0)
                        {
                            bos.AgentUnspecifiedReasonList = new BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason[_Submission.CompanyNumberAgentUnspecifiedReason.Count];
                            for (int j = 0; j < _Submission.CompanyNumberAgentUnspecifiedReason.Count; j++)
                            {
                                bos.AgentUnspecifiedReasonList[j] = new BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason();
                                bos.AgentUnspecifiedReasonList[j].CompanyNumberAgentUnspecifiedReasonId =
                                    _Submission.CompanyNumberAgentUnspecifiedReason.ElementAt(j).Id;
                                bos.AgentUnspecifiedReasonList[j].Reason =
                                        _Submission.CompanyNumberAgentUnspecifiedReason.ElementAt(j).AgentUnspecifiedReason.Reason;
                            }
                        }

                        if (_Submission.CompanyNumberCode != null && _Submission.CompanyNumberCode.Count > 0)
                        {
                            bos.SubmissionCodeTypess = new BCS.Core.Clearance.BizObjects.SubmissionCodeType[_Submission.CompanyNumberCode.Count];
                            for (int j = 0; j < _Submission.CompanyNumberCode.Count; j++)
                            {
                                bos.SubmissionCodeTypess[j] = new BCS.Core.Clearance.BizObjects.SubmissionCodeType();
                                bos.SubmissionCodeTypess[j].CodeName = _Submission.CompanyNumberCode.ElementAt(j).CompanyNumberCodeType.CodeName;
                                bos.SubmissionCodeTypess[j].CodeValue = _Submission.CompanyNumberCode.ElementAt(j).Code;
                                bos.SubmissionCodeTypess[j].CodeId = _Submission.CompanyNumberCode.ElementAt(j).Id;
                                bos.SubmissionCodeTypess[j].CodeDescription = _Submission.CompanyNumberCode.ElementAt(j).Description;
                                bos.SubmissionCodeTypess[j].CodeTypeId = _Submission.CompanyNumberCode.ElementAt(j).CompanyNumberCodeType.Id;
                            }
                        }

                        int[] ids = new int[_Submission.CompanyNumberCode.Count];
                        for (int k = 0; k < _Submission.CompanyNumberCode.Count; k++)
                        {
                            ids[k] = _Submission.CompanyNumberCode.ElementAt(k).Id;
                            if (_Submission.CompanyNumberCode.ElementAt(k).CompanyNumberCodeType.CodeName == "Policy Symbol")
                            {
                                bos.SubmissionCodeTypes = _Submission.CompanyNumberCode.ElementAt(k).Code;
                            }
                        }

                        if (!(_Submission.ClientCorePortfolioId == null))
                            bos.ClientCorePortfolioId = _Submission.ClientCorePortfolioId.Value;

                        // agent
                        bos.AgentId = _Submission.AgentId == null ? 0 : _Submission.AgentId.Value;
                        if (_Submission.Agent != null)
                        {
                            bos.Agent = new BCS.Core.Clearance.BizObjects.Agent();
                            bos.Agent.Active = _Submission.Agent.Active;
                            bos.Agent.AddressId = _Submission.Agent.AddressId == null ? 0 : _Submission.Agent.AddressId.Value;
                            bos.Agent.AgencyId = _Submission.Agent.AgencyId;
                            bos.Agent.BrokerNo = _Submission.Agent.BrokerNo;
                            bos.Agent.DepartmentName = _Submission.Agent.DepartmentName;
                            bos.Agent.EntryBy = _Submission.Agent.EntryBy;
                            bos.Agent.EntryDate = _Submission.Agent.EntryDt;
                            bos.Agent.FirstName = _Submission.Agent.FirstName;
                            bos.Agent.FunctionalTitle = _Submission.Agent.FunctionalTitle;
                            bos.Agent.Id = _Submission.Agent.Id;
                            bos.Agent.MailStopCode = _Submission.Agent.MailStopCode;
                            bos.Agent.Middle = _Submission.Agent.Middle;
                            bos.Agent.ModifiedBy = _Submission.Agent.ModifiedBy;
                            if (_Submission.Agent.ModifiedDt != null)
                                bos.Agent.ModifiedDt = Convert.ToDateTime(_Submission.Agent.ModifiedDt);
                            bos.Agent.Prefix = _Submission.Agent.Prefix;
                            bos.Agent.PrimaryPhone = _Submission.Agent.PrimaryPhone;
                            bos.Agent.PrimaryPhoneAreaCode = _Submission.Agent.PrimaryPhoneAreaCode;
                            bos.Agent.ProfessionalTitle = _Submission.Agent.ProfessionalTitle;
                            bos.Agent.SecondaryPhone = _Submission.Agent.SecondaryPhone;
                            bos.Agent.SecondaryPhoneAreaCode = _Submission.Agent.SecondaryPhoneAreaCode;
                            bos.Agent.SuffixTitle = _Submission.Agent.SuffixTitle;
                            bos.Agent.Surname = _Submission.Agent.Surname;
                            bos.Agent.UserId = _Submission.Agent.UserId.IfNotNull(_Submission.Agent.UserId.ToInt());
                        }

                        // contact
                        bos.ContactId = _Submission.ContactId.IfNotNull(_Submission.ContactId.ToInt());
                        if (_Submission.Contact != null)
                        {
                            bos.Contact = new BCS.Core.Clearance.BizObjects.Contact();
                            bos.Contact.AgencyId = _Submission.Contact.AgencyId;
                            bos.Contact.DateOfBirth = BOA.Util.Common.GetSafeT<DateTime>(_Submission.Contact.DateOfBirth);
                            bos.Contact.Email = _Submission.Contact.Email;
                            bos.Contact.Fax = _Submission.Contact.Fax;
                            bos.Contact.Id = _Submission.Contact.Id;
                            bos.Contact.Name = _Submission.Contact.Name;
                            bos.Contact.PrimaryPhone = _Submission.Contact.PrimaryPhone;
                            bos.Contact.PrimaryPhoneExtension = _Submission.Contact.PrimaryPhoneExtension;
                            bos.Contact.SecondaryPhone = _Submission.Contact.SecondaryPhone;
                            bos.Contact.SecondaryPhoneExtension = _Submission.Contact.SecondaryPhoneExtension;
                        }

                        //agency
                        bos.AgencyId = _Submission.AgencyId;
                        if (_Submission.Agency != null)
                        {
                            bos.Agency = new BCS.Core.Clearance.BizObjects.Agency();
                            bos.Agency.Active = _Submission.Agency.Active;
                            bos.Agency.AgencyName = _Submission.Agency.AgencyName;
                            bos.Agency.AgencyNumber = _Submission.Agency.AgencyNumber;
                            bos.Agency.City = _Submission.Agency.City;
                            bos.Agency.CompanyNumberId = _Submission.Agency.CompanyNumberId;
                            bos.Agency.EntryBy = _Submission.Agency.EntryBy;
                            bos.Agency.EntryDate = _Submission.Agency.EntryDt;
                            bos.Agency.FEIN = _Submission.Agency.FEIN;
                            bos.Agency.Id = _Submission.Agency.Id;
                            bos.Agency.MasterAgencyId = BOA.Util.Common.GetSafeT<int>(_Submission.Agency.MasterAgencyId);
                            bos.Agency.ModifiedBy = _Submission.Agency.ModifiedBy;
                            bos.Agency.ModifiedDt = BOA.Util.Common.GetSafeT<DateTime>(_Submission.Agency.ModifiedDt);
                            bos.Agency.State = _Submission.Agency.State;

                            bos.AgencyName = _Submission.Agency.AgencyName == null ? string.Empty : _Submission.Agency.AgencyName;
                        }

                        bos.ClientCoreClientId = _Submission.ClientCoreClientId;
                        bos.Deleted = (bool)_Submission.Deleted;
                        bos.DeletedBy = _Submission.DeletedBy;
                        bos.DeletedDate = _Submission.DeletedDt.GetSafeDateTime();


                        //if (_Submission.SubmissionClientCoreClientAddress != null)
                        //    bos.ClientCoreAddressId = _Submission.SubmissionClientCoreClientAddress.ClientCoreClientAddressId;
                        #region client core client address ids
                        List<int> ccids = new List<int>();
                        foreach (SubmissionClientCoreClientAddress var in _Submission.SubmissionClientCoreClientAddress)
                        {
                            ccids.Add(var.ClientCoreClientAddressId);
                        }
                        bos.ClientCoreAddressIds = new int[ccids.Count];
                        ccids.CopyTo(bos.ClientCoreAddressIds);
                        // TODO: for the moment, we will have old element populated too to easily identify submissions with a address
                        // on the default.aspx page.
                        string[] strccids = Array.ConvertAll<int, string>(bos.ClientCoreAddressIds, new Converter<int, string>(Convert.ToString));
                        bos.ClientCoreAddressIdsString = string.Join(",", strccids);
                        #region TODO: stop gap for other apps to catch up
                        if (ccids.Count > 0)
                        {
                            bos.ClientCoreAddressId = ccids[0].ToString();
                        }
                        else
                        {
                            bos.ClientCoreAddressId = "0";
                        }
                        #endregion

                        #endregion
                        #region client core client name ids
                        ccids = new List<int>();
                        foreach (SubmissionClientCoreClientName var in _Submission.SubmissionClientCoreClientName)
                        {
                            ccids.Add(var.ClientCoreNameId);
                        }
                        bos.ClientCoreNameIds = new int[ccids.Count];
                        ccids.CopyTo(bos.ClientCoreNameIds);

                        #endregion


                        #region Submission Comments

                        if (_Submission.SubmissionComment.Count > 0)
                        {

                            // bubble up and latest comment as comment for easy display in grids
                            bos.Comments = _Submission.SubmissionComment.ToList().SortDESC("EntryDt").First().Comment;

                            bos.SubmissionComments = new BCS.Core.Clearance.BizObjects.SubmissionComment[_Submission.SubmissionComment.Count];
                            for (int j = 0; j < _Submission.SubmissionComment.Count; j++)
                            {
                                bos.SubmissionComments[j] = new BCS.Core.Clearance.BizObjects.SubmissionComment();

                                bos.SubmissionComments[j].Comment = _Submission.SubmissionComment.ElementAt(j).Comment;
                                bos.SubmissionComments[j].EntryBy = _Submission.SubmissionComment.ElementAt(j).EntryBy;
                                bos.SubmissionComments[j].EntryDt = _Submission.SubmissionComment.ElementAt(j).EntryDt;
                                bos.SubmissionComments[j].Id = _Submission.SubmissionComment.ElementAt(j).Id;
                                bos.SubmissionComments[j].ModifiedBy = _Submission.SubmissionComment.ElementAt(j).ModifiedBy;
                                bos.SubmissionComments[j].ModifiedDt = BOA.Util.Common.GetSafeT<DateTime>(_Submission.SubmissionComment.ElementAt(j).ModifiedDt);
                                bos.SubmissionComments[j].SubmissionId = _Submission.SubmissionComment.ElementAt(j).SubmissionId;
                            }
                        }
                        else
                        {
                            bos.SubmissionComments = new BCS.Core.Clearance.BizObjects.SubmissionComment[0];
                        }
                        #endregion
                        bos.CompanyNumberId = _Submission.CompanyNumberId;
                        if (_Submission.CompanyNumber != null)
                            bos.CompanyId = _Submission.CompanyNumber.CompanyId;
                        bos.EffectiveDate = _Submission.EffectiveDate.GetSafe<DateTime>();
                        bos.EstimatedWrittenPremium = _Submission.EstimatedWrittenPremium.GetSafe<decimal>();
                        bos.OtherCarrierPremium = _Submission.OtherCarrierPremium.GetSafe<decimal>();
                        bos.ExpirationDate = _Submission.ExpirationDate.GetSafeDateTime();
                        bos.Id = _Submission.Id;
                        bos.PolicyNumber = string.IsNullOrEmpty(_Submission.PolicyNumber) ? string.Empty : _Submission.PolicyNumber;
                        if (_Submission.PolicySystem != null)
                        {
                            bos.PolicySystem = new BCS.Core.Clearance.BizObjects.PolicySystem();
                            bos.PolicySystem.Abbreviation = _Submission.PolicySystem.Abbreviation;
                            bos.PolicySystem.AssociatedCompanyNumberId = _Submission.PolicySystem.AssociatedCompanyNumberId;
                            bos.PolicySystem.Id = _Submission.PolicySystem.Id;
                            bos.PolicySystem.PropertyPolicySystem = _Submission.PolicySystem.PolicySystem1;
                        }
                        bos.SubmissionBy = _Submission.SubmissionBy;
                        bos.SubmissionDt = _Submission.SubmissionDt.GetSafeDateTime();
                        bos.SubmissionNumber = _Submission.SubmissionNumber;
                        bos.InsuredName = _Submission.InsuredName;
                        bos.Dba = _Submission.Dba;

                        // other carrier
                        if (_Submission.OtherCarrierId.HasValue)
                            bos.OtherCarrierId = _Submission.OtherCarrierId.ToInt();
                        if (_Submission.OtherCarrier != null)
                        {
                            bos.OtherCarrier = new BCS.Core.Clearance.BizObjects.OtherCarrier();
                            bos.OtherCarrier.Id = _Submission.OtherCarrier.Id;
                            bos.OtherCarrier.Code = _Submission.OtherCarrier.Code;
                            bos.OtherCarrier.Description = _Submission.OtherCarrier.Description;
                        }

                        // status
                        if (_Submission.SubmissionStatusId.HasValue)
                            bos.SubmissionStatusId = _Submission.SubmissionStatusId.Value;
                        if (_Submission.SubmissionStatus != null)
                        {
                            bos.SubmissionStatus = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
                            bos.SubmissionStatus.Id = _Submission.SubmissionStatus.Id;
                            bos.SubmissionStatus.StatusCode = _Submission.SubmissionStatus.StatusCode;
                        }
                        // status reason
                        if (_Submission.SubmissionStatusReasonId.HasValue)
                        {
                            bos.SubmissionStatusReasonId = _Submission.SubmissionStatusReasonId.Value;
                            bos.SubmissionStatusReason = _Submission.SubmissionStatusReason.ReasonCode;
                        }

                        bos.SubmissionStatusDate = _Submission.SubmissionStatusDate.GetSafeDateTime();
                        bos.SubmissionStatusReasonOther = _Submission.SubmissionStatusReasonOther;

                        if (_Submission.SubmissionTokenId.HasValue)
                            bos.SubmissionTokenId = _Submission.SubmissionTokenId.Value;

                        // type
                        bos.SubmissionTypeId = _Submission.SubmissionTypeId;
                        if (_Submission.SubmissionType != null)
                        {
                            bos.SubmissionType = new BCS.Core.Clearance.BizObjects.SubmissionType();
                            bos.SubmissionType.Abbreviation = _Submission.SubmissionType.Abbreviation;
                            bos.SubmissionType.Id = _Submission.SubmissionType.Id;
                            bos.SubmissionType.TypeName = _Submission.SubmissionType.TypeName;
                        }
                        bos.UnderwriterId = _Submission.UnderwriterPersonId.GetSafeInt32();
                        bos.UnderwriterName = _Submission.UnderwriterPersonId == null ? string.Empty : _Submission.Person.UserName;
                        bos.UnderwriterFullName = _Submission.UnderwriterPersonId == null ? string.Empty : _Submission.Person.FullName;
                        bos.AnalystId = _Submission.AnalystPersonId.GetSafeInt32();
                        bos.AnalystName = _Submission.AnalystPersonId == null ? string.Empty : _Submission.Person1.UserName;
                        bos.AnalystFullName = _Submission.AnalystPersonId == null ? string.Empty : _Submission.Person1.FullName;
                        bos.TechnicianId = _Submission.TechnicianPersonId.GetSafeInt32();
                        bos.TechnicianName = _Submission.TechnicianPersonId == null ? string.Empty : _Submission.Person2.UserName;
                        bos.TechnicianFullName = _Submission.TechnicianPersonId == null ? string.Empty : _Submission.Person2.FullName;
                        bos.PolicySystemId = _Submission.PolicySystemId.GetSafeInt32();
                        bos.UpdatedBy = _Submission.UpdatedBy;
                        bos.UpdatedDt = _Submission.UpdatedDt.GetSafeDateTime();
                        bos.CancellationDate = _Submission.CancellationDate.GetSafeDateTime();

                        #region previous underwriter

                        List<SubmissionHistory> shc = _Submission.SubmissionHistory.ToList();

                        shc = shc.Where(s => s.UnderwriterPersonId != 0 && s.UnderwriterPersonId != null && s.UnderwriterPersonId != bos.UnderwriterId).ToList().SortDESC("HistoryDt");

                        if (shc.Count > 0)
                        {
                            bos.PreviousUnderwriterId = shc[0].UnderwriterPersonId.Value;

                            Person previousPerson = DataAccess.GetEntityById<Person>(shc.First().UnderwriterPersonId.ToInt());
                            // person can be deleted, so if exists

                            if (null != previousPerson)
                            {
                                bos.PreviousUnderwriterName = previousPerson.UserName;
                                bos.PreviousUnderwriterFullName = previousPerson.FullName;
                            }

                        }
                        #endregion

                        #region SubmissionLineOfBusinessChildren

                        if (_Submission.SubmissionLineOfBusinessChildren.Count > 0)
                        {
                            bos.LineOfBusinessChildList = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild[_Submission.SubmissionLineOfBusinessChildren.Count];
                            for (int j = 0; j < _Submission.SubmissionLineOfBusinessChildren.Count; j++)
                            {
                                bos.LineOfBusinessChildList[j] = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild();

                                bos.LineOfBusinessChildList[j].Id = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).Id;
                                bos.LineOfBusinessChildList[j].CompanyNumberLOBGridId = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGridId;
                                bos.LineOfBusinessChildList[j].SubmissionStatusId =
                                    _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatusId.GetSafeInt32();
                                bos.LineOfBusinessChildList[j].SubmissionStatusReasonId =
                                    _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatusReasonId.GetSafeInt32();

                                if (_Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGrid != null)
                                {
                                    bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid = new BCS.Core.Clearance.BizObjects.CompanyNumberLOBGrid();
                                    bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Code = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGrid.MulitLOBCode;
                                    bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.CompanyNumberId = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGrid.CompanyNumberId;
                                    bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Description = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGrid.MultiLOBDesc;
                                    bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Id = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGrid.Id;
                                }

                                if (_Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatus != null)
                                {
                                    bos.LineOfBusinessChildList[j].SubmissionStatus = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
                                    bos.LineOfBusinessChildList[j].SubmissionStatus.Id = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatus.Id;
                                    bos.LineOfBusinessChildList[j].SubmissionStatus.StatusCode = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatus.StatusCode;
                                }

                                if (_Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatusReason != null)
                                {
                                    bos.LineOfBusinessChildList[j].SubmissionStatusReason = new BCS.Core.Clearance.BizObjects.SubmissionStatusReason();
                                    bos.LineOfBusinessChildList[j].SubmissionStatusReason.Id = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatusReason.Id;
                                    bos.LineOfBusinessChildList[j].SubmissionStatusReason.ReasonCode = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatusReason.ReasonCode;
                                }
                            }
                            #region first lob child
                            int index0 = 0;
                            {
                                bos.FirstSubmissionLineOfBusinessChild = new BCS.Core.Clearance.BizObjects.FirstSubmissionLineOfBusinessChild();
                                bos.FirstSubmissionLineOfBusinessChild.Id = _Submission.SubmissionLineOfBusinessChildren.ElementAt(index0).Id;
                                bos.FirstSubmissionLineOfBusinessChild.LobId = _Submission.SubmissionLineOfBusinessChildren.ElementAt(index0).CompanyNumberLOBGridId;

                                if (_Submission.SubmissionLineOfBusinessChildren.ElementAt(index0).CompanyNumberLOBGrid != null)
                                {
                                    bos.FirstSubmissionLineOfBusinessChild.LobCode = _Submission.SubmissionLineOfBusinessChildren.ElementAt(index0).CompanyNumberLOBGrid.MulitLOBCode;
                                    bos.FirstSubmissionLineOfBusinessChild.LobDescription = _Submission.SubmissionLineOfBusinessChildren.ElementAt(index0).CompanyNumberLOBGrid.MultiLOBDesc;
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            bos.LineOfBusinessChildList = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild[0];
                        }
                        #endregion

                        al.Add(bos);
                        i++;
                        if (i > max)
                            break;
                        Match = properties.Length == 0;
                    }

                }

            }

            if (pageSize > 0 && al.Count > 0)
            {
                int theSize = 0;
                if (pageNo > 1)
                    theSize = al.Count - min - 1;
                else
                    theSize = al.Count <= pageSize ? al.Count : pageSize;
                if (i <= max && theSize != al.Count)
                    theSize++;
                if (theSize < 0)
                {
                    list.PageNo = pageNo;
                    return list;
                }

                list.List = new BCS.Core.Clearance.BizObjects.Submission[theSize];
                al.CopyTo(min, list.List, 0, list.List.Length);
                list.HasNextPage = i > max;
                list.PageNo = pageNo;
                list.PageSize = list.List.Length;
            }
            else
            {
                //list.List = new BCS.Core.Clearance.BizObjects.Submission[al.Count];
                //al.CopyTo(list.List);
                list.List = al.ToArray();
                list.HasNextPage = i > max;
                list.PageNo = 1;
                list.PageSize = al.Count;
            }
            return list;
        }



        internal static BCS.Core.Clearance.BizObjects.SubmissionList BuildSubmissionList(
            List<Submission> collection, string[] properties, object[] values, OrmLib.CompareType[] comparers,
            int pageSize, int pageNo, MatchCriteria crit)
        {
            if (pageNo < 0)
                return new BizObjects.SubmissionList();
            if (pageNo == 0)
                pageSize = 0;
            if (pageNo > 0 && pageSize < 0)
                pageSize = 0;


            bool Match = properties.Length == 0;
            BizObjects.SubmissionList list = new BCS.Core.Clearance.BizObjects.SubmissionList();

            int min = Int32.MinValue;
            int max = Int32.MaxValue;
            if (pageSize > 0)
            {
                min = (pageNo - 1) * pageSize;
                max = pageNo * pageSize;
            }
            List<BizObjects.Submission> al = new List<BCS.Core.Clearance.BizObjects.Submission>();
            int i = 0;
            BizObjects.Submission bos;

            using (System.Data.EntityClient.EntityConnection conn = new System.Data.EntityClient.EntityConnection(DataAccess.ConnectionString()))
            {
                conn.Open();

                foreach (Submission _Submission in collection)
                {


                    using (BCSContext bc = new BCSContext(conn))
                    {


                        bc.Submission.Attach(_Submission);

                        if (!Match)
                        {
                            switch (crit)
                            {
                                case MatchCriteria.And:
                                    {
                                        Match = MatchAnd(_Submission, comparers, properties, values);
                                        break;
                                    }
                                case MatchCriteria.Or:
                                    {
                                        Match = MatchOr(_Submission, comparers, properties, values);
                                        break;
                                    }
                                default:
                                    break;
                            }
                        }
                        if (Match)
                        {

                            bos = new BCS.Core.Clearance.BizObjects.Submission();

                            _Submission.LineOfBusiness.Load();// (bc.Submission.Where(s => s.Id == _Submission.Id).Select(s => s.LineOfBusiness).SingleOrDefault());

                            bos.SubmissionNumberSequence = _Submission.Sequence;
                            if (_Submission.LineOfBusiness != null && _Submission.LineOfBusiness.Count > 0)
                            {
                                bos.LineOfBusinessId = _Submission.LineOfBusiness.ElementAt(0).Id;
                                bos.LineOfBusiness = _Submission.LineOfBusiness.ElementAt(0).Description;
                                bos.LineOfBusinessList = new BCS.Core.Clearance.BizObjects.LineOfBusiness[_Submission.LineOfBusiness.Count];
                                for (int j = 0; j < _Submission.LineOfBusiness.Count; j++)
                                {
                                    bos.LineOfBusinessList[j] = new BCS.Core.Clearance.BizObjects.LineOfBusiness();
                                    bos.LineOfBusinessList[j].Code = _Submission.LineOfBusiness.ElementAt(j).Code;
                                    bos.LineOfBusinessList[j].Description = _Submission.LineOfBusiness.ElementAt(j).Description;
                                    bos.LineOfBusinessList[j].Id = _Submission.LineOfBusiness.ElementAt(j).Id;
                                    bos.LineOfBusinessList[j].Line = _Submission.LineOfBusiness.ElementAt(j).Line;
                                }
                            }

                            _Submission.SubmissionCompanyNumberAttributeValue.Load();

                            if (_Submission.SubmissionCompanyNumberAttributeValue != null && _Submission.SubmissionCompanyNumberAttributeValue.Count > 0)
                            {
                                bos.AttributeList = new BCS.Core.Clearance.BizObjects.SubmissionAttribute[_Submission.SubmissionCompanyNumberAttributeValue.Count];
                                for (int j = 0; j < _Submission.SubmissionCompanyNumberAttributeValue.Count; j++)
                                {
                                    bos.AttributeList[j] = new BCS.Core.Clearance.BizObjects.SubmissionAttribute();
                                    bos.AttributeList[j].AttributeValue = _Submission.SubmissionCompanyNumberAttributeValue.ElementAt(j).AttributeValue;
                                    bos.AttributeList[j].CompanyNumberAttributeId = _Submission.SubmissionCompanyNumberAttributeValue.ElementAt(j).CompanyNumberAttributeId;
                                    bos.AttributeList[j].Label = _Submission.SubmissionCompanyNumberAttributeValue.ElementAt(j).CompanyNumberAttribute.Label;
                                    bos.AttributeList[j].DataType = _Submission.SubmissionCompanyNumberAttributeValue.ElementAt(j).CompanyNumberAttribute.AttributeDataType.DataTypeName;
                                }
                            }

                            _Submission.CompanyNumberAgentUnspecifiedReason.Load();

                            if (_Submission.CompanyNumberAgentUnspecifiedReason != null && _Submission.CompanyNumberAgentUnspecifiedReason.Count > 0)
                            {
                                bos.AgentUnspecifiedReasonList = new BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason[_Submission.CompanyNumberAgentUnspecifiedReason.Count];
                                for (int j = 0; j < _Submission.CompanyNumberAgentUnspecifiedReason.Count; j++)
                                {
                                    bos.AgentUnspecifiedReasonList[j] = new BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason();
                                    bos.AgentUnspecifiedReasonList[j].CompanyNumberAgentUnspecifiedReasonId =
                                        _Submission.CompanyNumberAgentUnspecifiedReason.ElementAt(j).Id;
                                    bos.AgentUnspecifiedReasonList[j].Reason =
                                            _Submission.CompanyNumberAgentUnspecifiedReason.ElementAt(j).AgentUnspecifiedReason.Reason;
                                }
                            }

                            _Submission.CompanyNumberCode.Load();// = bc.Submission.Include("CompanyNumberCode.CompanyNumberCodeType").Where(s=>s.Id == _Submission.Id).Select(s=>s.CompanyNumberCode).SingleOrDefault();

                            if (_Submission.CompanyNumberCode != null && _Submission.CompanyNumberCode.Count > 0)
                            {
                                bos.SubmissionCodeTypess = new BCS.Core.Clearance.BizObjects.SubmissionCodeType[_Submission.CompanyNumberCode.Count];
                                for (int j = 0; j < _Submission.CompanyNumberCode.Count; j++)
                                {
                                    bos.SubmissionCodeTypess[j] = new BCS.Core.Clearance.BizObjects.SubmissionCodeType();
                                    bos.SubmissionCodeTypess[j].CodeName = _Submission.CompanyNumberCode.ElementAt(j).CompanyNumberCodeType.CodeName;
                                    bos.SubmissionCodeTypess[j].CodeValue = _Submission.CompanyNumberCode.ElementAt(j).Code;
                                    bos.SubmissionCodeTypess[j].CodeId = _Submission.CompanyNumberCode.ElementAt(j).Id;
                                    bos.SubmissionCodeTypess[j].CodeDescription = _Submission.CompanyNumberCode.ElementAt(j).Description;
                                    bos.SubmissionCodeTypess[j].CodeTypeId = _Submission.CompanyNumberCode.ElementAt(j).CompanyNumberCodeType.Id;
                                }
                            }

                            int[] ids = new int[_Submission.CompanyNumberCode.Count];
                            for (int k = 0; k < _Submission.CompanyNumberCode.Count; k++)
                            {
                                ids[k] = _Submission.CompanyNumberCode.ElementAt(k).Id;
                                if (_Submission.CompanyNumberCode.ElementAt(k).CompanyNumberCodeType.CodeName == "Policy Symbol")
                                {
                                    bos.SubmissionCodeTypes = _Submission.CompanyNumberCode.ElementAt(k).Code;
                                }
                            }

                            if (!(_Submission.ClientCorePortfolioId == null))
                                bos.ClientCorePortfolioId = _Submission.ClientCorePortfolioId.Value;

                            // agent
                            bos.AgentId = _Submission.AgentId == null ? 0 : _Submission.AgentId.Value;
                            if (_Submission.Agent != null)
                            {
                                bos.Agent = new BCS.Core.Clearance.BizObjects.Agent();
                                bos.Agent.Active = _Submission.Agent.Active;
                                bos.Agent.AddressId = _Submission.Agent.AddressId == null ? 0 : _Submission.Agent.AddressId.Value;
                                bos.Agent.AgencyId = _Submission.Agent.AgencyId;
                                bos.Agent.BrokerNo = _Submission.Agent.BrokerNo;
                                bos.Agent.DepartmentName = _Submission.Agent.DepartmentName;
                                bos.Agent.EntryBy = _Submission.Agent.EntryBy;
                                bos.Agent.EntryDate = _Submission.Agent.EntryDt;
                                bos.Agent.FirstName = _Submission.Agent.FirstName;
                                bos.Agent.FunctionalTitle = _Submission.Agent.FunctionalTitle;
                                bos.Agent.Id = _Submission.Agent.Id;
                                bos.Agent.MailStopCode = _Submission.Agent.MailStopCode;
                                bos.Agent.Middle = _Submission.Agent.Middle;
                                bos.Agent.ModifiedBy = _Submission.Agent.ModifiedBy;
                                if (_Submission.Agent.ModifiedDt != null)
                                    bos.Agent.ModifiedDt = Convert.ToDateTime(_Submission.Agent.ModifiedDt);
                                bos.Agent.Prefix = _Submission.Agent.Prefix;
                                bos.Agent.PrimaryPhone = _Submission.Agent.PrimaryPhone;
                                bos.Agent.PrimaryPhoneAreaCode = _Submission.Agent.PrimaryPhoneAreaCode;
                                bos.Agent.ProfessionalTitle = _Submission.Agent.ProfessionalTitle;
                                bos.Agent.SecondaryPhone = _Submission.Agent.SecondaryPhone;
                                bos.Agent.SecondaryPhoneAreaCode = _Submission.Agent.SecondaryPhoneAreaCode;
                                bos.Agent.SuffixTitle = _Submission.Agent.SuffixTitle;
                                bos.Agent.Surname = _Submission.Agent.Surname;
                                bos.Agent.UserId = _Submission.Agent.UserId.IfNotNull(_Submission.Agent.UserId.ToInt());
                            }

                            // contact
                            bos.ContactId = _Submission.ContactId.IfNotNull(_Submission.ContactId.ToInt());
                            if (_Submission.Contact != null)
                            {
                                bos.Contact = new BCS.Core.Clearance.BizObjects.Contact();
                                bos.Contact.AgencyId = _Submission.Contact.AgencyId;
                                bos.Contact.DateOfBirth = BOA.Util.Common.GetSafeT<DateTime>(_Submission.Contact.DateOfBirth);
                                bos.Contact.Email = _Submission.Contact.Email;
                                bos.Contact.Fax = _Submission.Contact.Fax;
                                bos.Contact.Id = _Submission.Contact.Id;
                                bos.Contact.Name = _Submission.Contact.Name;
                                bos.Contact.PrimaryPhone = _Submission.Contact.PrimaryPhone;
                                bos.Contact.PrimaryPhoneExtension = _Submission.Contact.PrimaryPhoneExtension;
                                bos.Contact.SecondaryPhone = _Submission.Contact.SecondaryPhone;
                                bos.Contact.SecondaryPhoneExtension = _Submission.Contact.SecondaryPhoneExtension;
                            }

                            //agency
                            bos.AgencyId = _Submission.AgencyId;
                            if (_Submission.Agency != null)
                            {
                                bos.Agency = new BCS.Core.Clearance.BizObjects.Agency();
                                bos.Agency.Active = _Submission.Agency.Active;
                                bos.Agency.AgencyName = _Submission.Agency.AgencyName;
                                bos.Agency.AgencyNumber = _Submission.Agency.AgencyNumber;
                                bos.Agency.City = _Submission.Agency.City;
                                bos.Agency.CompanyNumberId = _Submission.Agency.CompanyNumberId;
                                bos.Agency.EntryBy = _Submission.Agency.EntryBy;
                                bos.Agency.EntryDate = _Submission.Agency.EntryDt;
                                bos.Agency.FEIN = _Submission.Agency.FEIN;
                                bos.Agency.Id = _Submission.Agency.Id;
                                bos.Agency.MasterAgencyId = BOA.Util.Common.GetSafeT<int>(_Submission.Agency.MasterAgencyId);
                                bos.Agency.ModifiedBy = _Submission.Agency.ModifiedBy;
                                bos.Agency.ModifiedDt = BOA.Util.Common.GetSafeT<DateTime>(_Submission.Agency.ModifiedDt);
                                bos.Agency.State = _Submission.Agency.State;

                                bos.AgencyName = _Submission.Agency.AgencyName == null ? string.Empty : _Submission.Agency.AgencyName;
                            }

                            bos.ClientCoreClientId = _Submission.ClientCoreClientId;
                            bos.Deleted = (bool)_Submission.Deleted;
                            bos.DeletedBy = _Submission.DeletedBy;
                            bos.DeletedDate = _Submission.DeletedDt.GetSafeDateTime();


                            //if (_Submission.SubmissionClientCoreClientAddress != null)
                            //    bos.ClientCoreAddressId = _Submission.SubmissionClientCoreClientAddress.ClientCoreClientAddressId;
                            #region client core client address ids
                            List<int> ccids = new List<int>();

                            _Submission.SubmissionClientCoreClientAddress.Load();// = bc.Submission.Where(s=>s.Id == _Submission.Id).Select(s=>s.SubmissionClientCoreClientAddress).SingleOrDefault();

                            foreach (SubmissionClientCoreClientAddress var in _Submission.SubmissionClientCoreClientAddress)
                            {
                                ccids.Add(var.ClientCoreClientAddressId);
                            }
                            bos.ClientCoreAddressIds = new int[ccids.Count];
                            ccids.CopyTo(bos.ClientCoreAddressIds);
                            // TODO: for the moment, we will have old element populated too to easily identify submissions with a address
                            // on the default.aspx page.
                            string[] strccids = Array.ConvertAll<int, string>(bos.ClientCoreAddressIds, new Converter<int, string>(Convert.ToString));
                            bos.ClientCoreAddressIdsString = string.Join(",", strccids);
                            #region TODO: stop gap for other apps to catch up
                            if (ccids.Count > 0)
                            {
                                bos.ClientCoreAddressId = ccids[0].ToString();
                            }
                            else
                            {
                                bos.ClientCoreAddressId = "0";
                            }
                            #endregion

                            #endregion
                            #region client core client name ids
                            ccids = new List<int>();
                            _Submission.SubmissionClientCoreClientName.Load();// = bc.Submission.Where(s=>s.Id == _Submission.Id).Select(s=>s.SubmissionClientCoreClientName).SingleOrDefault();
                            foreach (SubmissionClientCoreClientName var in _Submission.SubmissionClientCoreClientName)
                            {
                                ccids.Add(var.ClientCoreNameId);
                            }
                            bos.ClientCoreNameIds = new int[ccids.Count];
                            ccids.CopyTo(bos.ClientCoreNameIds);

                            #endregion


                            #region Submission Comments
                            _Submission.SubmissionComment.Load();// = bc.Submission.Where(s=>s.Id == _Submission.Id).Select(s=>s.SubmissionComment).SingleOrDefault();
                            if (_Submission.SubmissionComment.Count > 0)
                            {

                                // bubble up and latest comment as comment for easy display in grids
                                bos.Comments = _Submission.SubmissionComment.ToList().SortDESC("EntryDt").First().Comment;

                                bos.SubmissionComments = new BCS.Core.Clearance.BizObjects.SubmissionComment[_Submission.SubmissionComment.Count];
                                for (int j = 0; j < _Submission.SubmissionComment.Count; j++)
                                {
                                    bos.SubmissionComments[j] = new BCS.Core.Clearance.BizObjects.SubmissionComment();

                                    bos.SubmissionComments[j].Comment = _Submission.SubmissionComment.ElementAt(j).Comment;
                                    bos.SubmissionComments[j].EntryBy = _Submission.SubmissionComment.ElementAt(j).EntryBy;
                                    bos.SubmissionComments[j].EntryDt = _Submission.SubmissionComment.ElementAt(j).EntryDt;
                                    bos.SubmissionComments[j].Id = _Submission.SubmissionComment.ElementAt(j).Id;
                                    bos.SubmissionComments[j].ModifiedBy = _Submission.SubmissionComment.ElementAt(j).ModifiedBy;
                                    bos.SubmissionComments[j].ModifiedDt = BOA.Util.Common.GetSafeT<DateTime>(_Submission.SubmissionComment.ElementAt(j).ModifiedDt);
                                    bos.SubmissionComments[j].SubmissionId = _Submission.SubmissionComment.ElementAt(j).SubmissionId;
                                }
                            }
                            else
                            {
                                bos.SubmissionComments = new BCS.Core.Clearance.BizObjects.SubmissionComment[0];
                            }
                            #endregion
                            bos.CompanyNumberId = _Submission.CompanyNumberId;
                            if (_Submission.CompanyNumber != null)
                                bos.CompanyId = _Submission.CompanyNumber.CompanyId;
                            bos.EffectiveDate = _Submission.EffectiveDate.GetSafe<DateTime>();
                            bos.EstimatedWrittenPremium = _Submission.EstimatedWrittenPremium.GetSafe<decimal>();
                            bos.OtherCarrierPremium = _Submission.OtherCarrierPremium.GetSafe<decimal>();
                            bos.ExpirationDate = _Submission.ExpirationDate.GetSafeDateTime();
                            bos.Id = _Submission.Id;
                            bos.PolicyNumber = string.IsNullOrEmpty(_Submission.PolicyNumber) ? string.Empty : _Submission.PolicyNumber;
                            if (_Submission.PolicySystem != null)
                            {
                                bos.PolicySystem = new BCS.Core.Clearance.BizObjects.PolicySystem();
                                bos.PolicySystem.Abbreviation = _Submission.PolicySystem.Abbreviation;
                                bos.PolicySystem.AssociatedCompanyNumberId = _Submission.PolicySystem.AssociatedCompanyNumberId;
                                bos.PolicySystem.Id = _Submission.PolicySystem.Id;
                                bos.PolicySystem.PropertyPolicySystem = _Submission.PolicySystem.PolicySystem1;
                            }
                            bos.SubmissionBy = _Submission.SubmissionBy;
                            bos.SubmissionDt = _Submission.SubmissionDt.GetSafeDateTime();
                            bos.SubmissionNumber = _Submission.SubmissionNumber;
                            bos.InsuredName = _Submission.InsuredName;
                            bos.Dba = _Submission.Dba;

                            // other carrier
                            if (_Submission.OtherCarrierId.HasValue)
                                bos.OtherCarrierId = _Submission.OtherCarrierId.ToInt();
                            if (_Submission.OtherCarrier != null)
                            {
                                bos.OtherCarrier = new BCS.Core.Clearance.BizObjects.OtherCarrier();
                                bos.OtherCarrier.Id = _Submission.OtherCarrier.Id;
                                bos.OtherCarrier.Code = _Submission.OtherCarrier.Code;
                                bos.OtherCarrier.Description = _Submission.OtherCarrier.Description;
                            }

                            // status
                            if (_Submission.SubmissionStatusId.HasValue)
                                bos.SubmissionStatusId = _Submission.SubmissionStatusId.Value;
                            if (_Submission.SubmissionStatus != null)
                            {
                                bos.SubmissionStatus = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
                                bos.SubmissionStatus.Id = _Submission.SubmissionStatus.Id;
                                bos.SubmissionStatus.StatusCode = _Submission.SubmissionStatus.StatusCode;
                            }
                            // status reason
                            if (_Submission.SubmissionStatusReasonId.HasValue)
                            {
                                bos.SubmissionStatusReasonId = _Submission.SubmissionStatusReasonId.Value;
                                bos.SubmissionStatusReason = _Submission.SubmissionStatusReason.ReasonCode;
                            }

                            bos.SubmissionStatusDate = _Submission.SubmissionStatusDate.GetSafeDateTime();
                            bos.SubmissionStatusReasonOther = _Submission.SubmissionStatusReasonOther;

                            if (_Submission.SubmissionTokenId.HasValue)
                                bos.SubmissionTokenId = _Submission.SubmissionTokenId.Value;

                            // type
                            bos.SubmissionTypeId = _Submission.SubmissionTypeId;
                            if (_Submission.SubmissionType != null)
                            {
                                bos.SubmissionType = new BCS.Core.Clearance.BizObjects.SubmissionType();
                                bos.SubmissionType.Abbreviation = _Submission.SubmissionType.Abbreviation;
                                bos.SubmissionType.Id = _Submission.SubmissionType.Id;
                                bos.SubmissionType.TypeName = _Submission.SubmissionType.TypeName;
                            }
                            bos.UnderwriterId = _Submission.UnderwriterPersonId.GetSafeInt32();
                            bos.UnderwriterName = _Submission.UnderwriterPersonId == null ? string.Empty : _Submission.Person.UserName;
                            bos.UnderwriterFullName = _Submission.UnderwriterPersonId == null ? string.Empty : _Submission.Person.FullName;
                            bos.AnalystId = _Submission.AnalystPersonId.GetSafeInt32();
                            bos.AnalystName = _Submission.AnalystPersonId == null ? string.Empty : _Submission.Person1.UserName;
                            bos.AnalystFullName = _Submission.AnalystPersonId == null ? string.Empty : _Submission.Person1.FullName;
                            bos.TechnicianId = _Submission.TechnicianPersonId.GetSafeInt32();
                            bos.TechnicianName = _Submission.TechnicianPersonId == null ? string.Empty : _Submission.Person2.UserName;
                            bos.TechnicianFullName = _Submission.TechnicianPersonId == null ? string.Empty : _Submission.Person2.FullName;
                            bos.PolicySystemId = _Submission.PolicySystemId.GetSafeInt32();
                            bos.UpdatedBy = _Submission.UpdatedBy;
                            bos.UpdatedDt = _Submission.UpdatedDt.GetSafeDateTime();
                            bos.CancellationDate = _Submission.CancellationDate.GetSafeDateTime();

                            #region previous underwriter

                            List<SubmissionHistory> shc = _Submission.SubmissionHistory.ToList();

                            shc = shc.Where(s => s.UnderwriterPersonId != 0 && s.UnderwriterPersonId != null && s.UnderwriterPersonId != bos.UnderwriterId).ToList().SortDESC("HistoryDt");

                            if (shc.Count > 0)
                            {
                                bos.PreviousUnderwriterId = shc[0].UnderwriterPersonId.Value;

                                Person previousPerson = DataAccess.GetEntityById<Person>(shc.First().UnderwriterPersonId.ToInt());
                                // person can be deleted, so if exists

                                if (null != previousPerson)
                                {
                                    bos.PreviousUnderwriterName = previousPerson.UserName;
                                    bos.PreviousUnderwriterFullName = previousPerson.FullName;
                                }

                            }
                            #endregion

                            #region SubmissionLineOfBusinessChildren
                            //_Submission.SubmissionLineOfBusinessChildren = bc.Submission
                            //        .Include("SubmissionLineOfBusinessChildren.CompanyNumberLOBGrid")
                            //        .Include("SubmissionLineOfBusinessChildren.SubmissionStatus")
                            //        .Include("SubmissionLineOfBusinessChildren.SubmissionStatusReason").Where(s=>s.Id == _Submission.Id).Select(s=>s.SubmissionLineOfBusinessChildren).SingleOrDefault();


                            _Submission.SubmissionLineOfBusinessChildren.Load();

                            if (_Submission.SubmissionLineOfBusinessChildren.Count > 0)
                            {
                                bos.LineOfBusinessChildList = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild[_Submission.SubmissionLineOfBusinessChildren.Count];
                                for (int j = 0; j < _Submission.SubmissionLineOfBusinessChildren.Count; j++)
                                {
                                    bos.LineOfBusinessChildList[j] = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild();

                                    bos.LineOfBusinessChildList[j].Id = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).Id;
                                    bos.LineOfBusinessChildList[j].CompanyNumberLOBGridId = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGridId;
                                    bos.LineOfBusinessChildList[j].SubmissionStatusId =
                                        _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatusId.GetSafeInt32();
                                    bos.LineOfBusinessChildList[j].SubmissionStatusReasonId =
                                        _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatusReasonId.GetSafeInt32();

                                    if (_Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGrid != null)
                                    {
                                        bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid = new BCS.Core.Clearance.BizObjects.CompanyNumberLOBGrid();
                                        bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Code = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGrid.MulitLOBCode;
                                        bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.CompanyNumberId = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGrid.CompanyNumberId;
                                        bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Description = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGrid.MultiLOBDesc;
                                        bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Id = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).CompanyNumberLOBGrid.Id;
                                    }

                                    if (_Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatus != null)
                                    {
                                        bos.LineOfBusinessChildList[j].SubmissionStatus = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
                                        bos.LineOfBusinessChildList[j].SubmissionStatus.Id = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatus.Id;
                                        bos.LineOfBusinessChildList[j].SubmissionStatus.StatusCode = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatus.StatusCode;
                                    }

                                    if (_Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatusReason != null)
                                    {
                                        bos.LineOfBusinessChildList[j].SubmissionStatusReason = new BCS.Core.Clearance.BizObjects.SubmissionStatusReason();
                                        bos.LineOfBusinessChildList[j].SubmissionStatusReason.Id = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatusReason.Id;
                                        bos.LineOfBusinessChildList[j].SubmissionStatusReason.ReasonCode = _Submission.SubmissionLineOfBusinessChildren.ElementAt(j).SubmissionStatusReason.ReasonCode;
                                    }
                                }
                                #region first lob child
                                int index0 = 0;
                                {
                                    bos.FirstSubmissionLineOfBusinessChild = new BCS.Core.Clearance.BizObjects.FirstSubmissionLineOfBusinessChild();
                                    bos.FirstSubmissionLineOfBusinessChild.Id = _Submission.SubmissionLineOfBusinessChildren.ElementAt(index0).Id;
                                    bos.FirstSubmissionLineOfBusinessChild.LobId = _Submission.SubmissionLineOfBusinessChildren.ElementAt(index0).CompanyNumberLOBGridId;

                                    if (_Submission.SubmissionLineOfBusinessChildren.ElementAt(index0).CompanyNumberLOBGrid != null)
                                    {
                                        bos.FirstSubmissionLineOfBusinessChild.LobCode = _Submission.SubmissionLineOfBusinessChildren.ElementAt(index0).CompanyNumberLOBGrid.MulitLOBCode;
                                        bos.FirstSubmissionLineOfBusinessChild.LobDescription = _Submission.SubmissionLineOfBusinessChildren.ElementAt(index0).CompanyNumberLOBGrid.MultiLOBDesc;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                bos.LineOfBusinessChildList = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild[0];
                            }
                            #endregion

                            al.Add(bos);
                            i++;
                            if (i > max)
                                break;
                            Match = properties.Length == 0;
                        }
                        bc.Submission.Detach(_Submission);

                    }
                }
            }

            if (pageSize > 0 && al.Count > 0)
            {
                int theSize = 0;
                if (pageNo > 1)
                    theSize = al.Count - min - 1;
                else
                    theSize = al.Count <= pageSize ? al.Count : pageSize;
                if (i <= max && theSize != al.Count)
                    theSize++;
                if (theSize < 0)
                {
                    list.PageNo = pageNo;
                    return list;
                }

                list.List = new BCS.Core.Clearance.BizObjects.Submission[theSize];
                al.CopyTo(min, list.List, 0, list.List.Length);
                list.HasNextPage = i > max;
                list.PageNo = pageNo;
                list.PageSize = list.List.Length;
            }
            else
            {
                //list.List = new BCS.Core.Clearance.BizObjects.Submission[al.Count];
                //al.CopyTo(list.List);
                list.List = al.ToArray();
                list.HasNextPage = i > max;
                list.PageNo = 1;
                list.PageSize = al.Count;
            }
            return list;
        }


        private static string BuildEmptySubmissionList()
        {
            BizObjects.SubmissionList list = new BCS.Core.Clearance.BizObjects.SubmissionList();
            string xml = SerializeSubmissionList(list);
            return xml;
        }

        private static bool MatchAnd(Submission submission, OrmLib.CompareType[] compareTypes, string[] properties, object[] values)
        {
            if (properties.Length != values.Length)
                throw new ArgumentException("Properties, Filters and Filter types must be of equal length");

            if (compareTypes == null)
            {
                compareTypes = new OrmLib.CompareType[properties.Length];
            }


            for (int i = 0; i < compareTypes.Length; i++)
            {
                if (!Match(submission, compareTypes[i], properties[i], values[i]))
                    return false;
            }

            return true;
        }
        private static bool MatchOr(Submission submission, OrmLib.CompareType[] compareTypes, string[] properties, object[] values)
        {
            if (properties.Length != values.Length)
                throw new ArgumentException("Properties, Filters and Filter types must be of equal length");

            if (compareTypes == null)
            {
                compareTypes = new OrmLib.CompareType[properties.Length];
            }
            for (int i = 0; i < compareTypes.Length; i++)
            {
                if (Match(submission, compareTypes[i], properties[i], values[i]))
                    return true;
            }
            return false;
        }
        private static bool Match(Submission submission, OrmLib.CompareType compareType, string property, object value)
        {
            return false;
            //try
            //{
            //    switch (compareType)
            //    {
            //        case OrmLib.CompareType.Contains:
            //            {
            //                if (submission[property] is Nullable)
            //                {
            //                    if ((submission[property] as INullable).IsNull)
            //                    {
            //                        if (value == null)
            //                            return true;
            //                        else
            //                            return false;
            //                    }
            //                }
            //                if (submission[property] == null)
            //                {
            //                    if (value == null)
            //                        return true;
            //                    else
            //                        return false;
            //                }
            //                else
            //                {
            //                    if (value == null)
            //                        return false;
            //                }
            //                string a = submission[property].ToString();
            //                string b = value.ToString();
            //                return a.IndexOf(b,StringComparison.CurrentCultureIgnoreCase) >= 0;
            //            }
            //        case OrmLib.CompareType.EndsWith:
            //            {
            //                if (submission[property] is Nullable)
            //                {
            //                    if ((submission[property] as INullable).IsNull)
            //                    {
            //                        if (value == null)
            //                            return true;
            //                        else
            //                            return false;
            //                    }
            //                }
            //                if (submission[property] == null)
            //                {
            //                    if (value == null)
            //                        return true;
            //                    else
            //                        return false;
            //                }
            //                else
            //                {
            //                    if (value == null)
            //                        return false;
            //                }
            //                string a = submission[property].ToString();
            //                string b = value.ToString();
            //                return a.EndsWith(b,StringComparison.CurrentCultureIgnoreCase);
            //            }
            //        case OrmLib.CompareType.Exact:
            //            {
            //                if (submission[property] is Nullable)
            //                {
            //                    if ((submission[property] as INullable).IsNull)
            //                    {
            //                        if (value == null)
            //                            return true;
            //                        else
            //                            return false;
            //                    }
            //                }
            //                if (submission[property] == null)
            //                {
            //                    if (value == null)
            //                        return true;
            //                    else
            //                        return false;
            //                }
            //                else
            //                {
            //                    if (value == null)
            //                        return false;
            //                }
            //                int i = CompareNotNulls(submission, property, value);
            //                return i == 0;
            //            }
            //        case OrmLib.CompareType.Greater:
            //            {
            //                if (submission[property] is Nullable)
            //                {
            //                    if ((submission[property] as INullable).IsNull)
            //                    {
            //                        return false;
            //                    }
            //                }
            //                if (submission[property] == null || value == null)
            //                    return false;
            //                int i = CompareNotNulls(submission, property, value);
            //                return i > 0;
            //            }
            //        case OrmLib.CompareType.GreaterOrEqual:
            //            {
            //                if (submission[property] is Nullable)
            //                {
            //                    if ((submission[property] as INullable).IsNull)
            //                    {
            //                        if (value == null)
            //                            return true;
            //                        else
            //                            return false;
            //                    }
            //                }
            //                if (submission[property] == null)
            //                {
            //                    if (value == null)
            //                        return true;
            //                    else
            //                        return false;
            //                }
            //                else
            //                {
            //                    if (value == null)
            //                        return false;
            //                }
            //                int i = CompareNotNulls(submission, property, value);
            //                return i >= 0;
            //            }
            //        case OrmLib.CompareType.Lesser:
            //            {
            //                if (submission[property] is Nullable)
            //                {
            //                    if ((submission[property] as INullable).IsNull)
            //                    {
            //                        return false;
            //                    }
            //                }
            //                if (submission[property] == null || value == null)
            //                    return false;
            //                int i = CompareNotNulls(submission, property, value);
            //                return i < 0;
            //            }
            //        case OrmLib.CompareType.LesserOrEqual:
            //            {
            //                if (submission[property] is Nullable)
            //                {
            //                    if ((submission[property] as INullable).IsNull)
            //                    {
            //                        if (value == null)
            //                            return true;
            //                        else
            //                            return false;
            //                    }
            //                }
            //                if (submission[property] == null)
            //                {
            //                    if (value == null)
            //                        return true;
            //                    else
            //                        return false;
            //                }
            //                else
            //                {
            //                    if (value == null)
            //                        return false;
            //                }
            //                int i = CompareNotNulls(submission, property, value);
            //                return i <= 0;
            //            }
            //        case OrmLib.CompareType.Not:
            //            {
            //                if (submission[property] is Nullable)
            //                {
            //                    if ((submission[property] as INullable).IsNull)
            //                    {
            //                        if (value == null)
            //                            return true;
            //                        else
            //                            return false;
            //                    }
            //                }
            //                if (submission[property] == null)
            //                {
            //                    if (value == null)
            //                        return false;
            //                    else
            //                        return true;
            //                }
            //                else
            //                {
            //                    if (value == null)
            //                        return true;
            //                }
            //                int i = CompareNotNulls(submission, property, value);
            //                return i != 0;
            //            }
            //        case OrmLib.CompareType.NotContain:
            //            {
            //                if (submission[property] is Nullable)
            //                {
            //                    if ((submission[property] as INullable).IsNull)
            //                    {
            //                        return false;
            //                    }
            //                }
            //                if (submission[property] == null || value == null)
            //                    return false;
            //                string a = submission[property].ToString();
            //                string b = value.ToString();
            //                return a.IndexOf(b,StringComparison.CurrentCultureIgnoreCase) < 0;
            //            }
            //        case OrmLib.CompareType.StartsWith:
            //            {
            //                if (submission[property] is Nullable)
            //                {
            //                    if ((submission[property] as INullable).IsNull)
            //                    {
            //                        if (value == null)
            //                            return true;
            //                        else
            //                            return false;
            //                    }
            //                }
            //                if (submission[property] == null)
            //                {
            //                    if (value == null)
            //                        return true;
            //                    else
            //                        return false;
            //                }
            //                else
            //                {
            //                    if (value == null)
            //                        return false;
            //                }
            //                string a = submission[property].ToString();
            //                string b = value.ToString();
            //                return a.StartsWith(b,StringComparison.CurrentCultureIgnoreCase);
            //            }
            //        default:
            //            break;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //return false;
        }

        private static int CompareNotNulls(BCS.Biz.Submission submission, string property, object value)
        {
            System.Reflection.PropertyInfo pai = typeof(BCS.Biz.Submission).GetProperty(property);
            Type t = pai.PropertyType;
            IComparable o = (IComparable)Convert.ChangeType(submission[property], t);

            Type vt = value.GetType();
            if (vt == t)
            { }
            else
            {
                ConstructorInfo ci = t.GetConstructor(new Type[] { vt });
                if (ci == null)
                {
                    throw new ArgumentException(string.Format("Invalid cast from {0} to {1}", vt.FullName, t.FullName));
                }

                value = ci.Invoke(new object[] { value });
            }
            IComparable v = (IComparable)Convert.ChangeType(value, t);
            int i = o.CompareTo(v);
            return i;
        }

        #endregion


        public static string GetSubmissionStatusHistoryBySubmissionNumberAndCompanyNumber(Int64 submissionNumber, string companyNumber, int seqNbr = 0)
        {
            List<DAL.SubmissionStatusHistory> ssh = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                var sst = (from sh in bc.SubmissionStatusHistory
                           join st in bc.SubmissionStatus on sh.PreviousSubmissionStatusId equals st.Id
                           join sb in bc.Submission on sh.SubmissionId equals sb.Id
                           join cc in bc.CompanyNumber on sb.CompanyNumberId equals cc.Id
                           where sb.SubmissionNumber == submissionNumber && cc.CompanyNumber1 == companyNumber
                           select new { sh.Id, sh.SubmissionId, st.StatusCode, sh.SubmissionStatusDate, sh.StatusChangeDt, sh.StatusChangeBy, sh.Active, sb.Sequence }).ToList();
                if (seqNbr > 0)
                    sst = sst.Where(p => p.Sequence == seqNbr).ToList();
                if (sst.Count > 0)
                {
                    BizObjects.SubmissionStatusHistory[] list = new BCS.Core.Clearance.BizObjects.SubmissionStatusHistory[sst.Count];
                    List<BizObjects.SubmissionStatusHistory> sslist = new List<BizObjects.SubmissionStatusHistory>();
                    foreach (var t in sst)
                    {
                        BizObjects.SubmissionStatusHistory item = new BizObjects.SubmissionStatusHistory();
                        item.Id = t.Id;
                        item.SubmissionID = t.SubmissionId;
                        item.PreviousSubmissionStatus = t.StatusCode;
                        item.SubmissionStatusDate = t.SubmissionStatusDate.ToDateTime();
                        item.StatusChangeDate = t.StatusChangeDt;
                        item.StatusChangeBy = t.StatusChangeBy;
                        item.Active = t.Active;
                        sslist.Add(item);
                    }
                    return XML.Serializer.SerializeAsXml(sslist);
                }
                else
                    return "No Submission Status History Found";
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="submissionId"></param>
        /// <returns></returns>
        public static List<SubmissionStatusHistory> GetSubmissionStatusHistory(int submissionId)
        {
            List<SubmissionStatusHistory> coll = DataAccess.GetEntityList<SubmissionStatusHistory>("SubmissionId", submissionId, "SubmissionStatus").SortDESC("StatusChangeDt");

            return coll;
        }

        public static string ToggleSubmissionStatusHistoryActive(int statusId, string user)
        {
            int d = 0;
            return ToggleSubmissionStatusHistoryActive(statusId, user, ref d);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statusId">Id of the status to be toggled.</param>
        /// <param name="user">updating user.</param>
        /// <param name="newSubmissionStatusId">The Id of the status the submission was updated to.</param>
        /// <returns></returns>
        public static string ToggleSubmissionStatusHistoryActive(int statusId, string user, ref int newSubmissionStatusId)
        {
            DateTime changeDate = DateTime.Now;

            Submission submission = null;
            SubmissionStatusHistory history = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                history = bc.SubmissionStatusHistory.Include("Submission").Where(s => s.Id == statusId).SingleOrDefault();

                if (history != null && history.Submission != null)
                    submission = history.Submission;
                else
                    return "Invalid history Id provided.";


                bool deactivating = history.Active;
                newSubmissionStatusId = submission.SubmissionStatusId.GetSafeInt32();

                // update the history
                history.Active = !history.Active;
                history.StatusChangeDt = changeDate;
                history.StatusChangeBy = user;

                if (history.PreviousSubmissionStatusId.HasValue)
                {
                    // we only update the submission status, when it is the same status that is getting de-activated on the history
                    if (deactivating && submission.SubmissionStatusId == history.PreviousSubmissionStatusId)
                    {
                        List<SubmissionStatusHistory> histories = submission.SubmissionStatusHistory.Where(s => s.Id != statusId && s.Active).ToList().SortDESC("StatusChangeDt");
                        if (histories != null && histories.Count > 0)
                        {
                            submission.UpdatedBy = user;
                            submission.UpdatedDt = changeDate;
                            submission.SubmissionStatusId = histories[0].PreviousSubmissionStatusId;
                            newSubmissionStatusId = histories[0].PreviousSubmissionStatusId.Value;
                            submission.SubmissionStatusDate = histories[0].SubmissionStatusDate;
                        }
                    }
                }
                try
                {
                    bc.SaveChanges();
                    // remove the cache dependency used to cache submissions for page
                    System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
                }

                catch (Exception)
                {
                    return "There was a problem updating the status history.";
                }
            }
            return string.Format("Status flag successfully {0}.", history.Active ? "activated" : "de-activated");
        }

        /// <summary>
        /// updates submissions with the underwriter or analyst specified by the rolecolumn paramter
        /// </summary>
        /// <param name="submissionIds"></param>
        /// <param name="personid"></param>
        /// <param name="rolecolumn"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string UpdatePersonAssignments(string[] submissionIds, int personid, string rolecolumn, string user)
        {
            Type t = typeof(Biz.Submission);
            System.Reflection.PropertyInfo pi = t.GetProperty(rolecolumn);

            //Biz.DataManager dm = Common.GetDataManager();
            //dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, submissionIds, MatchType.In);
            //Biz.SubmissionCollection ss = dm.GetSubmissionCollection();

            List<int> subIds = new List<int>();

            foreach (string item in submissionIds)
                subIds.Add(item.ToInt());

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                var ss = bc.Submission.Where(s => subIds.Contains(s.Id)).ToList();

                foreach (var sub in ss)
                {
                    pi.SetValue(sub, Common.ConvertToSqlInt32(personid), null);

                    sub.UpdatedBy = user;
                    sub.UpdatedDt = DateTime.Now;
                }

                try
                {
                    bc.SaveChanges();
                    System.Web.HttpContext.Current.Cache.Remove("SearchedCacheDependency");
                }
                catch (Exception ex)
                {
                    BTS.LogFramework.LogCentral.Current.LogFatal(ex.Message, ex);
                    return "There was a problem updating the submissions.";
                }
            }

            return "Submissions successfully updated.";
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="companyNumber"></param>
        ///// <param name="criteria">first - OrmLib.DataManagerBase.JoinPathRelation, second - data, third - MatchType </param>
        ///// <returns></returns>
        //private static Biz.SubmissionCollection SearchSubmissions(string companyNumber, bool includeDeleted, params System.Web.UI.Triplet[] criteria)
        //{
        //    Biz.DataManager dm = Common.GetDataManager();
        //    dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);

        //    //Filter deleted submissions.
        //    if (!includeDeleted)
        //    {
        //        dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Deleted, false);
        //    }

        //    foreach (System.Web.UI.Triplet var in criteria)
        //    {
        //        dm.QueryCriteria.And((OrmLib.DataManagerBase.JoinPathRelation)var.First, var.Second,
        //            var.Third == null ? MatchType.Exact : (MatchType)var.Third);
        //    }            

        //    Biz.SubmissionCollection submissions = dm.GetSubmissionCollection(SubmissionFetchPath());

        //    return submissions;

        //}

        private static List<BCS.DAL.Submission> SearchSubmissions(string companyNumber, bool includeDeleted, params DAL.EFilter[] criteria)
        {
            //using (BCSContext bc = DataAccess.NewBCSContext())
            //{
            //    var submissions = bc.Submission.Include("CompanyNumberCode.CompanyNumberCodeType")
            //        .Include("SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute.AttributeDataType")
            //        .Include("LineOfBusiness")
            //        .Include("CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason")
            //        .Include("SubmissionLineOfBusinessChildren.CompanyNumberLOBGrid")
            //        .Include("SubmissionHistory")
            //        .Where(s => s.CompanyNumber.CompanyNumber1 == companyNumber);
            //    if (includeDeleted)
            //        submissions = submissions.Where(s => s.Deleted);
            //    else
            //        submissions = submissions.Where(s => !s.Deleted);

            List<EFilter> filters = new List<EFilter>();

            filters.Add(new EFilter("CompanyNumber.CompanyNumber1", companyNumber));

            if (criteria != null && criteria.Length > 0)
            {
                filters.AddRange(criteria);
            }

            if (!includeDeleted)
            {
                filters.Add(new EFilter("Deleted", false));
            }

            return DataAccess.GetEntityList<Submission>(filters, submissionRelations);

            //}

        }

        private static List<int> SearchSubmissionsByIds(string companyNumber, bool includeDeleted, params DAL.EFilter[] criteria)
        {
            //using (BCSContext bc = DataAccess.NewBCSContext())
            //{
            //    var submissions = bc.Submission.Include("CompanyNumberCode.CompanyNumberCodeType")
            //        .Include("SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute.AttributeDataType")
            //        .Include("LineOfBusiness")
            //        .Include("CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason")
            //        .Include("SubmissionLineOfBusinessChildren.CompanyNumberLOBGrid")
            //        .Include("SubmissionHistory")
            //        .Where(s => s.CompanyNumber.CompanyNumber1 == companyNumber);
            //    if (includeDeleted)
            //        submissions = submissions.Where(s => s.Deleted);
            //    else
            //        submissions = submissions.Where(s => !s.Deleted);

            List<EFilter> filters = new List<EFilter>();

            filters.Add(new EFilter("CompanyNumber.CompanyNumber1", companyNumber));

            if (criteria != null && criteria.Length > 0)
            {
                filters.AddRange(criteria);
            }

            if (!includeDeleted)
            {
                filters.Add(new EFilter("Deleted", false));
            }

            return DataAccess.GetEntityListIds<Submission>(filters, submissionRelations);

            //}

        }

        public static string GetSubmissionsByPolicyAndAttribute(string companyNumber, string policyNumber, string attributeLabel, string attributeValue)
        {
            return GetSubmissionsByPolicyAndAttribute(companyNumber, policyNumber, attributeLabel, attributeValue, true);
        }
        public static string GetSubmissionsByPolicyAndAttribute(string companyNumber, string policyNumber, string attributeLabel, string attributeValue, bool includeDeleted)
        {
            if (string.IsNullOrEmpty(companyNumber) || string.IsNullOrEmpty(policyNumber) || string.IsNullOrEmpty(attributeLabel) || string.IsNullOrEmpty(attributeValue))
                return BuildEmptySubmissionList();

            EFilter f1 = new EFilter("PolicyNumber", policyNumber);
            EFilter f2 = new EFilter("SubmissionCompanyNumberAttributeValue.CompanyNumberAttribute.Label", attributeLabel);
            EFilter f3 = new EFilter("SubmissionCompanyNumberAttributeValue.AttributeValue", attributeValue, DAL.MatchType.Partial);

            List<Submission> submissions = SearchSubmissions(companyNumber, includeDeleted, f1, f2, f3);

            BizObjects.SubmissionList sl = BuildSubmissionList(submissions, new string[] { }, new object[] { }, null, 0, 0);

            return SerializeSubmissionList(sl);
        }
        public string UpdateClientId(string companyNumber, string oldClientId, string newClientId, string insuredName, string dba, string user, bool isSearchScreen)
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                Company company = Company.GetCompanyByCompanyNumber(companyNumber);
                if (null == companyNumber)
                    return "Invalid company number.";

                //bc.Connection.BeginTransaction();

                List<Submission> submissions = bc.Submission.Include("CompanyNumber").Include("SubmissionClientCoreClientAddress").Include("SubmissionClientCoreClientName")
                        .Where(s => s.CompanyNumber.CompanyNumber1 == companyNumber && s.ClientCoreClientId == oldClientId.ToT<long>()).ToList();


                // get submissions
                if (null == submissions || 0 == submissions.Count)
                    return string.Format("No submissions were found for the client {0}.", oldClientId);

                // get the new client
                BizObjects.ClearanceClient client = Clients.GetClientByIdAsObject(companyNumber, newClientId.ToString(), true, isSearchScreen);

                foreach (Submission submission in submissions)
                {
                    while (submission.SubmissionClientCoreClientAddress.Count > 0)
                    {
                        submission.SubmissionClientCoreClientAddress.Remove(submission.SubmissionClientCoreClientAddress.First());
                    }
                    while (submission.SubmissionClientCoreClientName.Count > 0)
                    {
                        submission.SubmissionClientCoreClientName.Remove(submission.SubmissionClientCoreClientName.First());
                    }
                }

                foreach (Submission submission in submissions)
                {
                    #region Insured Name
                    // update only if something is passed
                    if (Common.IsNotNullOrEmpty(insuredName))
                    {
                        submission.InsuredName = insuredName;
                    }
                    #endregion
                    #region DBA
                    // update only if something is passed
                    if (Common.IsNotNullOrEmpty(dba))
                    {
                        submission.Dba = dba;
                    }
                    #endregion
                    if (null != client.ClientCoreClient.Addresses)
                    {
                        foreach (BTS.ClientCore.Wrapper.Structures.Info.Address var in client.ClientCoreClient.Addresses)
                        {
                            SubmissionClientCoreClientAddress newAddr = new SubmissionClientCoreClientAddress();
                            newAddr.ClientCoreClientAddressId = var.AddressId.ToInt();

                            submission.SubmissionClientCoreClientAddress.Add(newAddr);
                        }
                    }
                    if (null != client.ClientCoreClient.Client.Names)
                    {
                        foreach (BTS.ClientCore.Wrapper.Structures.Info.Name var in client.ClientCoreClient.Client.Names)
                        {
                            SubmissionClientCoreClientName newAddr = new SubmissionClientCoreClientName();
                            newAddr.ClientCoreNameId = var.SequenceNumber.ToInt();

                            submission.SubmissionClientCoreClientName.Add(newAddr);
                        }
                    }
                    submission.ClientCoreClientId = Convert.ToInt64(client.ClientCoreClient.Client.ClientId);
                    submission.ClientCorePortfolioId = Convert.ToInt64(client.ClientCoreClient.Client.PortfolioId);
                    submission.UpdatedBy = user;
                    submission.UpdatedDt = DateTime.Now;
                }

                try
                {
                    bc.SaveChanges();
                }
                catch (Exception ex)
                {

                    LogCentral.Current.LogFatal(
                        string.Format(
                        "There was a problem reassigning the client id for " +
                        "company number : '{0}', fromClientId : '{1}', toClientId : '{2}', insuredName : '{3}',  dba : '{4}'",
                        companyNumber, oldClientId, newClientId, insuredName, dba), ex);
                    return "There was a problem reassigning the client.";
                }
            }

            // assumed success if reached here
            return "Submissions were successfully reassigned.";
        }

        public static void MarkSubmissionAsDeleted(int submissionId)
        {
            try
            {
                using (BCSContext bc = DataAccess.NewBCSContext())
                {
                    var sub = bc.Submission.Where(s => s.Id == submissionId).SingleOrDefault();

                    if (sub == null)
                        return;

                    sub.Deleted = true;
                    sub.DeletedBy = System.Web.HttpContext.Current.User.Identity.Name;
                    sub.DeletedDt = DateTime.Now;

                    bc.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static List<BCS.DAL.Submission> GetSubmissionsByPolicyNumber(string companyNumber, string policyNumber, bool includeDeleted, OrmLib.MatchType matchType)
        {
            #region Ignore
            //string str = "";
            //if (matchType == OrmLib.MatchType.Exact)
            //{
            //    str = "SELECT * FROM Submission INNER JOIN CompanyNumber ON CompanyNumber.Id = Submission.CompanyNumberId WHERE CompanyNumber = @cmpNumber AND PolicyNumber = @policyNumber";
            //}
            //else
            //{
            //    str = "SELECT * FROM Submission INNER JOIN CompanyNumber ON CompanyNumber.Id = Submission.CompanyNumberId WHERE CompanyNumber = @cmpNumber AND PolicyNumber LIKE '%@policyNumber%' ";
            //}

            //if (!includeDeleted)
            //    str = str + "AND Deleted = 0";
            //SqlConnection conn = new SqlConnection(Common.DSN);
            //List<BCS.DAL.Submission> submissionList = null;
            //Biz.SubmissionCollection sublist = null;

            //try
            //{
            //    conn.Open();
            //    SqlCommand myCmd = new SqlCommand(str, conn);
            //    SqlDataAdapter submissionAdapter = new SqlDataAdapter();
            //    submissionAdapter.SelectCommand = myCmd;

            //    SqlParameter param1 = new SqlParameter();
            //    param1.ParameterName = "@cmpNumber";
            //    param1.SqlDbType = SqlDbType.VarChar;
            //    param1.Direction = ParameterDirection.Input;
            //    param1.Value = companyNumber;
            //    param1.Size = 3;

            //    SqlParameter param2 = new SqlParameter();
            //    param2.ParameterName = "@policyNumber";
            //    param2.SqlDbType = SqlDbType.VarChar;
            //    param2.Direction = ParameterDirection.Input;
            //    param2.Value = policyNumber;
            //    param2.Size = 50;

            //    myCmd.Parameters.Add(param1);
            //    myCmd.Parameters.Add(param2);


            //    //myCmd.Parameters.Add("@cmpNumber", SqlDbType.VarChar, 3).Value = companyNumber;
            //    //myCmd.Parameters.Add("@policyNumber", SqlDbType.VarChar, 50).Value = policyNumber;

            //    DataSet submissions = new DataSet();
            //    //submissionAdapter.Fill(submissions, "Submission");
            //    submissionAdapter.Fill(submissions);

            //    foreach (DataRow dRow in submissions.Tables[0].Rows)
            //    {
            //        BCS.DAL.Submission mySubmission = new BCS.DAL.Submission();
            //        //Biz.Submission mySubmission = new Biz.Submission();

            //        mySubmission.Id = (int)dRow["Id"];
            //        mySubmission.CompanyNumberId = (int)dRow["CompanyNumberId"];
            //        mySubmission.SubmissionTypeId = (int)dRow["SubmissionTypeId"];
            //        mySubmission.SubmissionTokenId = (int)dRow["SubmissionTokenId"];
            //        mySubmission.ClientCoreClientId = (int)dRow["ClientCoreClientId"];
            //        mySubmission.ClientCorePortfolioId = (int)dRow["ClientCorePortfolioId"];
            //        mySubmission.AgencyId = (int)dRow["AgencyId"];
            //        mySubmission.AgentId = (int)dRow["AgentId"];
            //        mySubmission.ContactId = (int)dRow["ContactId"];
            //        mySubmission.SubmissionStatusId = (int)dRow["SubmissionStatusId"];
            //        mySubmission.SubmissionStatusDate = (DateTime)dRow["SubmissionStatusDate"];
            //        mySubmission.CancellationDate = (DateTime)dRow["CancellationDate"];
            //        mySubmission.SubmissionStatusReasonId = (int)dRow["SubmissionStatusReasonId"];
            //        mySubmission.SubmissionStatusReasonOther = (string)dRow["SubmissionStatusReasonOther"];
            //        mySubmission.UnderwriterPersonId = (int)dRow["UnderwriterPersonId"];
            //        mySubmission.AnalystPersonId = (int)dRow["AnalystPersonId"];
            //        mySubmission.TechnicianPersonId = (int)dRow["TechnicianPersonId"];
            //        mySubmission.PolicySystemId = (int)dRow["PolicySystemId"];
            //        mySubmission.SubmissionNumber = (long)dRow["SubmissionNumber"];
            //        mySubmission.Sequence = (int)dRow["Sequence"];
            //        mySubmission.PolicyNumber = (string)dRow["PolicyNumber"];
            //        mySubmission.EstimatedWrittenPremium = (decimal)dRow["EstimatedWrittenPremium"];
            //        mySubmission.EffectiveDate = (DateTime)dRow["EffectiveDate"];
            //        mySubmission.ExpirationDate = (DateTime)dRow["ExpirationDate"];
            //        mySubmission.InsuredName = (string)dRow["InsuredName"];
            //        mySubmission.Dba = (string)dRow["Dba"];
            //        mySubmission.Comments = (string)dRow["Comments"];
            //        mySubmission.OtherCarrierId = (int)dRow["OtherCarrierId"];
            //        mySubmission.OtherCarrierPremium = (decimal)dRow["OtherCarrierPremium"];
            //        mySubmission.SubmissionBy = (string)dRow["SubmissionBy"];
            //        mySubmission.UpdatedBy = (string)dRow["UpdatedBy"];
            //        mySubmission.UpdatedDt = (DateTime)dRow["UpdatedDt"];
            //        mySubmission.SystemDt = (DateTime)dRow["SystemDt"];
            //        mySubmission.SystemId = (string)dRow["SystemId"];
            //        mySubmission.Deleted = (bool)dRow["Deleted"];
            //        mySubmission.DeletedDt = (DateTime)dRow["DeletedBy"];
            //        mySubmission.DeletedBy = (string)dRow["DeletedBy"];

            //        submissionList.Add(mySubmission);
            //    }
            //}
            //catch (Exception ex)
            //{
            //}
            //finally
            //{
            //    conn.Close();
            //}
            #endregion

            List<BCS.DAL.Submission> subList = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                subList = bc.usp_GetSubmissionsByPolicyNumber(companyNumber, policyNumber).ToList();
            }
            return subList;


        }

        public static BCS.DAL.CompanyParameter GetCompanyParameterByCompanyNumber(string companyNumber)
        {
            BCS.DAL.CompanyParameter cmpParam = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                cmpParam = (from cp in bc.CompanyParameter
                            join cmp in bc.Company on cp.CompanyID equals cmp.Id
                            join cnum in bc.CompanyNumber on cmp.Id equals cnum.CompanyId
                            where cnum.CompanyNumber1 == companyNumber
                            select cp).FirstOrDefault();

                return cmpParam;
            }
        }

        public static BCS.DAL.CompanyClientAdvancedSearch GetAdvSearchAttributesByCompany(int companyId)
        {
            BCS.DAL.CompanyClientAdvancedSearch advSearchAttributes = null;
            if (System.Web.HttpContext.Current.Cache["advSearchAttributes" + companyId] != null)
            {
                advSearchAttributes = (BCS.DAL.CompanyClientAdvancedSearch)System.Web.HttpContext.Current.Cache["advSearchAttributes" + companyId];
                return advSearchAttributes;
            }
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                advSearchAttributes = bc.CompanyClientAdvancedSearch.Where(p => p.CompanyId == companyId).FirstOrDefault();
                if (advSearchAttributes != null)
                    System.Web.HttpContext.Current.Cache.Add("advSearchAttributes" + companyId, advSearchAttributes, null, System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
                return advSearchAttributes;
            }
        }

        #region submission search screen
        public static List<CompanyNumberAttribute> GetCompanyNumberAttributes(int companyNumberId)
        {
            List<CompanyNumberAttribute> attrs = null;

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                attrs = bc.CompanyNumberAttribute.Include("AttributeDataType").Where(a => a.CompanyNumberId == companyNumberId).ToList();
            }

            return attrs;
        }

        public static System.Web.UI.Control GenerateControlForAttribute(int attributeId)
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                var attributes = bc.CompanyNumberAttribute.Include("AttributeDataType").Where(a => a.Id == attributeId).ToList();

                foreach (var attr in attributes)
                {
                    switch (attr.AttributeDataType.DataTypeName)
                    {
                        case "BIT":
                            System.Web.UI.WebControls.CheckBox chk = new System.Web.UI.WebControls.CheckBox();
                            chk.ID = string.Format("txt-{0}", attr.Id);
                            return chk;
                        default:
                            System.Web.UI.WebControls.TextBox txt = new System.Web.UI.WebControls.TextBox();
                            txt.ID = string.Format("txt-{0}", attr.Id);
                            return txt;
                    }
                }
            }

            return null;
        }

        public static List<CompanyNumberCodeType> GetCompanyNumberCodeTypes(int companyNumberId)
        {
            List<CompanyNumberCodeType> types = null;

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                types = bc.CompanyNumberCodeType
                    .Include("CompanyNumberCode")
                    .Where(c => c.CompanyNumberId == companyNumberId && !c.ClassCodeSpecific && !c.ClientSpecific)
                    .OrderBy(c => c.CodeName).ToList();
            }

            return types;
        }

        public static System.Web.UI.WebControls.ListItemCollection GetItemsForCodeTypeDropDown(int typeId)
        {
            System.Web.UI.WebControls.ListItemCollection collection = new System.Web.UI.WebControls.ListItemCollection();

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                var codes = bc.CompanyNumberCode.Where(c => c.CompanyCodeTypeId == typeId).OrderBy(c => c.Code).ToList();

                if (codes != null)
                {
                    foreach (var code in codes)
                    {
                        collection.Add(new System.Web.UI.WebControls.ListItem(code.Code, code.Id.ToString()));
                    }
                }
            }

            return collection;
        }

        public static System.Web.UI.WebControls.ListItemCollection GetCompanyUsersForDropDown(int companyId)
        {
            System.Web.UI.WebControls.ListItemCollection collection = new System.Web.UI.WebControls.ListItemCollection();

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                var users = bc.User.Where(u => u.PrimaryCompanyId == companyId && !u.Role.Select(r => r.Id).Contains(1)).OrderBy(u => u.Username);
                foreach (var user in users)
                {
                    collection.Add(new System.Web.UI.WebControls.ListItem(user.Username.Replace("WRBTS\\", "").Replace("\\", ""), user.Username));
                }
            }

            return collection;
        }

        public static SubmissionSearchPage GetSubmissionSearchPageFiltersByCompanyNumberID(int companyNumberId)
        {
            SubmissionSearchPage filters = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                filters = bc.SubmissionSearchPage.Where(p => p.CompanyNumberId == companyNumberId).FirstOrDefault();
            }

            return filters;
        }

        #endregion

    }


    //TG - 8/22/2011 - to do
    //internal class AgencyPortSubmissions
    //{
    //    internal static OrmLib.DataManagerBase.FetchPathRelation[] SubmissionFetchPath = new OrmLib.DataManagerBase.FetchPathRelation[] { 
    //        Biz.FetchPath.Submission.CompanyNumber,
    //        Biz.FetchPath.Submission.Agency,
    //    };

    //    internal static Biz.SubmissionCollection SearchSubmissions(bool useCache, string companyNumber, params System.Web.UI.Triplet[] criteria)
    //    {
    //        // TODO: where should the value come from to cache or not.
    //        if (!useCache)
    //        {
    //            Biz.DataManager dm = Common.GetDataManager();
    //            dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
    //            foreach (System.Web.UI.Triplet var in criteria)
    //            {
    //                if (var.Third == null)
    //                    var.Third = MatchType.Exact;
    //                dm.QueryCriteria.And((OrmLib.DataManagerBase.JoinPathRelation)var.First, var.Second, (MatchType)var.Third);
    //            }
    //            return dm.GetSubmissionCollection(SubmissionFetchPath);
    //        }

    //        string dependency = companyNumber;
    //        foreach (System.Web.UI.Triplet triplet in criteria)
    //        {
    //            dependency += string.Format("{0}{1}{2}", triplet.First.ToString(), triplet.Second.ToString(), triplet.Third.ToString());
    //        }

    //        if (System.Web.HttpContext.Current.Cache["SearchedCacheDependency"] != null)
    //        {
    //            if (System.Web.HttpContext.Current.Cache["SearchedCacheDependency"].ToString() != dependency)
    //            { System.Web.HttpContext.Current.Cache["SearchedCacheDependency"] = dependency; }
    //        }
    //        else
    //        { System.Web.HttpContext.Current.Cache["SearchedCacheDependency"] = dependency; }

    //        Biz.SubmissionCollection submissions;
    //        if (System.Web.HttpContext.Current.Cache["SearchedSubmissions"] == null)
    //        {
    //            Biz.DataManager dm = Common.GetDataManager();
    //            dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
    //            foreach (System.Web.UI.Triplet var in criteria)
    //            {
    //                dm.QueryCriteria.And((OrmLib.DataManagerBase.JoinPathRelation)var.First, var.Second, (MatchType)var.Third);
    //            }
    //            submissions = dm.GetSubmissionCollection(SubmissionFetchPath);

    //            onRemove = new System.Web.Caching.CacheItemRemovedCallback(RemovedCallback);
    //            System.Web.HttpContext.Current.Cache.Insert("SearchedSubmissions", submissions,
    //                new System.Web.Caching.CacheDependency(null, new string[] { "SearchedCacheDependency" }),
    //                System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 20, 0),
    //                System.Web.Caching.CacheItemPriority.NotRemovable, onRemove);

    //            return submissions;
    //        }
    //        else
    //        {
    //            submissions = System.Web.HttpContext.Current.Cache["SearchedSubmissions"] as Biz.SubmissionCollection;
    //        }

    //        return submissions;
    //    }


    //    static bool itemRemoved = false;
    //    static System.Web.Caching.CacheItemRemovedReason reason;
    //    static System.Web.Caching.CacheItemRemovedCallback onRemove = null;

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="k"></param>
    //    /// <param name="v"></param>
    //    /// <param name="r"></param>
    //    public static void RemovedCallback(String k, Object v, System.Web.Caching.CacheItemRemovedReason r)
    //    {
    //        itemRemoved = true;
    //        reason = r;
    //    }
    //}


    internal class BuildSubmissionList
    {

        #region constructors
        public BuildSubmissionList()
        {
        }

        public BuildSubmissionList(List<Submission> collection, string[] properties, object[] values, OrmLib.CompareType[] comparers,
            int pageSize, int pageNo, BCS.Core.Clearance.SubmissionsEF.MatchCriteria crit)
        {
            _submissions = collection;
            _properties = properties;
            _values = values;
            _comparers = comparers;
            //_pageSize=_pageSize;
            //_pageNo = pageNo;
            //_crit = crit;
        }
        #endregion

        #region variables
        private BCSContext _bc;
        private List<Submission> _submissions;
        private List<BizObjects.Submission> _bizSubs;
        private string[] _properties;
        private object[] _values;
        private OrmLib.CompareType[] _comparers;
        private const int _maxThreads = 10;
        private int _listSize = 1;

        #region relations

        private List<int> _subIds;
        private List<SubmissionLineOfBusinessView> _lobids;
        private List<SubmissionAttributesView> _attributes;
        private List<SubmissionCompanyNumberCodeView> _codes;
        private List<SubmissionCompanyNumberAgentUnspecifiedReasonView> _agentReasons;
        private List<SubmissionClientCoreClientAddress> _clientAddresses;
        private List<SubmissionClientCoreClientName> _clientNames;
        private List<SubmissionComment> _comments;
        private List<SubmissionLineOfBusinessChildrenView> _lobsC;

        #endregion

        #endregion

        #region methods

        #region Build Relationships

        private void BuildLOB()
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                _lobids = bc.SubmissionLineOfBusinessView.Where(l => _subIds.Contains(l.SubmissionId)).ToList();
            }
        }

        private void BuildAttributes()
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                _attributes = bc.SubmissionAttributesView.Where(s => _subIds.Contains(s.SubmissionId)).ToList();
            }
        }

        private void BuildCodes()
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {

                _codes = bc.SubmissionCompanyNumberCodeView.Where(c => _subIds.Contains(c.SubmissionId)).ToList();
            }
        }

        private void BuildClientAddress()
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                _clientAddresses = bc.SubmissionClientCoreClientAddress.Where(c => _subIds.Contains(c.SubmissionId)).ToList();
            }
        }

        private void BuildClientNames()
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                _clientNames = bc.SubmissionClientCoreClientName.Where(c => _subIds.Contains(c.SubmissionId)).ToList();
            }
        }

        private void BuildComments()
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                _comments = bc.SubmissionComment.Where(c => _subIds.Contains(c.SubmissionId)).ToList();
            }
        }

        private void BuildAgentReasons()
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                _agentReasons = bc.SubmissionCompanyNumberAgentUnspecifiedReasonView.Where(s => _subIds.Contains(s.SubmissionId)).ToList();
            }
        }

        private void BuildLineOfBusinessChildren()
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                _lobsC = bc.SubmissionLineOfBusinessChildrenView.Where(s => _subIds.Contains(s.SubmissionId)).ToList();
            }
        }

        #endregion



        /// <summary>
        /// Gets list of submissions and related entities and builds them into a BizObjects.SubmissionList
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="includeEntities"></param>
        /// <returns></returns>
        public BizObjects.SubmissionList Build(List<EFilter> filters, params string[] includeEntities)
        {
            DateTime start = DateTime.Now;

            string maxResultsString = System.Configuration.ConfigurationManager.AppSettings["MaxResultSize"];

            int maxResults = 0;
            Int32.TryParse(maxResultsString, out maxResults);

            using (_bc = DataAccess.NewBCSContext())
            {
                _submissions = DataAccess.GetEntityListWithContext<Submission>(_bc, filters, includeEntities);

                if (maxResults > 0 && _submissions.Count > maxResults)
                    throw new Exception(string.Format("Return list will contain more than {0} submissions.  Please add additional search filters.", maxResults));

                _subIds = _submissions.Select(s => s.Id).ToList();
            }

            if (_submissions.Count == 0)
                return new BCS.Core.Clearance.BizObjects.SubmissionList();

            System.Threading.Thread tBuildLob = new System.Threading.Thread(new System.Threading.ThreadStart(BuildLOB));
            System.Threading.Thread tBuildLobC = new System.Threading.Thread(new System.Threading.ThreadStart(BuildLineOfBusinessChildren));
            System.Threading.Thread tBuildAgentReasons = new System.Threading.Thread(new System.Threading.ThreadStart(BuildAgentReasons));
            System.Threading.Thread tBuildAttributes = new System.Threading.Thread(new System.Threading.ThreadStart(BuildAttributes));
            System.Threading.Thread tBuildClientAddress = new System.Threading.Thread(new System.Threading.ThreadStart(BuildClientAddress));
            System.Threading.Thread tBuildClientNames = new System.Threading.Thread(new System.Threading.ThreadStart(BuildClientNames));
            System.Threading.Thread tBuildCodes = new System.Threading.Thread(new System.Threading.ThreadStart(BuildCodes));
            System.Threading.Thread tBuildComments = new System.Threading.Thread(new System.Threading.ThreadStart(BuildComments));

            tBuildLob.Start();
            tBuildLobC.Start();
            tBuildAgentReasons.Start();
            tBuildAttributes.Start();
            tBuildClientAddress.Start();
            tBuildClientNames.Start();
            tBuildCodes.Start();
            tBuildComments.Start();

            tBuildLob.Join();
            tBuildLobC.Join();
            tBuildAgentReasons.Join();
            tBuildAttributes.Join();
            tBuildClientAddress.Join();
            tBuildClientNames.Join();
            tBuildCodes.Join();
            tBuildComments.Join();


            string test = (DateTime.Now - start).TotalMilliseconds.ToString();

            start = DateTime.Now;
            BizObjects.SubmissionList subList = BuildList(_submissions);

            _agentReasons.Clear();
            _lobids.Clear();
            _lobsC.Clear();
            _attributes.Clear();
            _codes.Clear();
            _clientAddresses.Clear();
            _clientNames.Clear();
            _comments.Clear();

            test = (DateTime.Now - start).TotalMilliseconds.ToString();

            return subList;
        }


        internal BCS.Core.Clearance.BizObjects.SubmissionList BuildList(
            List<Submission> collection)
        {
            BizObjects.SubmissionList list = new BCS.Core.Clearance.BizObjects.SubmissionList();

            //int min = Int32.MinValue;
            int max = Int32.MaxValue;

            List<BizObjects.Submission> al = new List<BCS.Core.Clearance.BizObjects.Submission>();
            int i = 0;
            BizObjects.Submission bos;

            foreach (Submission _Submission in collection)
            {
                //DateTime start = DateTime.Now;
                bos = new BCS.Core.Clearance.BizObjects.Submission();

                List<SubmissionLineOfBusinessView> lobs = null;

                lock (_lobids)
                {
                    lobs = _lobids.Where(s => s.SubmissionId == _Submission.Id).ToList();
                }

                //_Submission.LineOfBusiness
                bos.SubmissionNumberSequence = _Submission.Sequence;
                if (lobs != null && lobs.Count > 0)
                {
                    bos.LineOfBusinessId = lobs.ElementAt(0).Id;
                    bos.LineOfBusiness = lobs.ElementAt(0).Description;
                    bos.LineOfBusinessList = new BCS.Core.Clearance.BizObjects.LineOfBusiness[lobs.Count];
                    for (int j = 0; j < lobs.Count; j++)
                    {
                        bos.LineOfBusinessList[j] = new BCS.Core.Clearance.BizObjects.LineOfBusiness();
                        bos.LineOfBusinessList[j].Code = lobs.ElementAt(j).Code;
                        bos.LineOfBusinessList[j].Description = lobs.ElementAt(j).Description;
                        bos.LineOfBusinessList[j].Id = lobs.ElementAt(j).Id;
                        bos.LineOfBusinessList[j].Line = lobs.ElementAt(j).Line;
                    }
                }

                List<SubmissionAttributesView> atts = _attributes.Where(s => s.SubmissionId == _Submission.Id).ToList();

                if (atts != null && atts.Count > 0)
                {
                    bos.AttributeList = new BCS.Core.Clearance.BizObjects.SubmissionAttribute[atts.Count];
                    for (int j = 0; j < atts.Count; j++)
                    {
                        bos.AttributeList[j] = new BCS.Core.Clearance.BizObjects.SubmissionAttribute();
                        bos.AttributeList[j].AttributeValue = atts[j].AttributeValue;
                        bos.AttributeList[j].CompanyNumberAttributeId = atts[j].CompanyNumberAttributeId;
                        bos.AttributeList[j].Label = atts[j].Label;
                        bos.AttributeList[j].DataType = atts[j].DataTypeName;
                    }
                }

                List<SubmissionCompanyNumberAgentUnspecifiedReasonView> unspr = null;
                lock (_agentReasons)
                {
                    unspr = _agentReasons.Where(s => s.SubmissionId == _Submission.Id).ToList();
                }

                if (unspr != null && unspr.Count > 0)
                {
                    bos.AgentUnspecifiedReasonList = new BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason[unspr.Count];
                    for (int j = 0; j < unspr.Count; j++)
                    {
                        bos.AgentUnspecifiedReasonList[j] = new BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason();
                        bos.AgentUnspecifiedReasonList[j].CompanyNumberAgentUnspecifiedReasonId =
                            unspr.ElementAt(j).Id;
                        bos.AgentUnspecifiedReasonList[j].Reason =
                                unspr.ElementAt(j).Reason;
                    }
                }

                List<SubmissionCompanyNumberCodeView> codes = null;
                lock (_codes)
                {
                    codes = _codes.Where(s => s.SubmissionId == _Submission.Id).ToList();
                }

                if (codes != null && codes.Count > 0)
                {
                    bos.SubmissionCodeTypess = new BCS.Core.Clearance.BizObjects.SubmissionCodeType[codes.Count];
                    for (int j = 0; j < codes.Count; j++)
                    {
                        bos.SubmissionCodeTypess[j] = new BCS.Core.Clearance.BizObjects.SubmissionCodeType();
                        bos.SubmissionCodeTypess[j].CodeName = codes.ElementAt(j).CodeName;
                        bos.SubmissionCodeTypess[j].CodeValue = codes.ElementAt(j).Code;
                        bos.SubmissionCodeTypess[j].CodeId = codes.ElementAt(j).CodeId;
                        bos.SubmissionCodeTypess[j].CodeDescription = codes.ElementAt(j).CodeDescription;
                        bos.SubmissionCodeTypess[j].CodeTypeId = codes.ElementAt(j).CodeTypeId;
                    }
                }

                int[] ids = new int[codes.Count];
                for (int k = 0; k < codes.Count; k++)
                {
                    ids[k] = codes.ElementAt(k).CodeId;
                    if (codes.ElementAt(k).CodeName == "Policy Symbol")
                    {
                        bos.SubmissionCodeTypes = codes.ElementAt(k).Code;
                    }
                }

                if (!(_Submission.ClientCorePortfolioId == null))
                    bos.ClientCorePortfolioId = _Submission.ClientCorePortfolioId.Value;


                // agent
                bos.AgentId = _Submission.AgentId == null ? 0 : _Submission.AgentId.Value;
                if (_Submission.Agent != null)
                {
                    bos.Agent = new BCS.Core.Clearance.BizObjects.Agent();
                    bos.Agent.Active = _Submission.Agent.Active;
                    bos.Agent.AddressId = _Submission.Agent.AddressId.HasValue ? _Submission.Agent.AddressId.Value : 0;
                    bos.Agent.AgencyId = _Submission.Agent.AgencyId;
                    bos.Agent.BrokerNo = _Submission.Agent.BrokerNo;
                    bos.Agent.DepartmentName = _Submission.Agent.DepartmentName;
                    bos.Agent.EntryBy = _Submission.Agent.EntryBy;
                    bos.Agent.EntryDate = _Submission.Agent.EntryDt;
                    bos.Agent.FirstName = _Submission.Agent.FirstName;
                    bos.Agent.FunctionalTitle = _Submission.Agent.FunctionalTitle;
                    bos.Agent.Id = _Submission.Agent.Id;
                    bos.Agent.MailStopCode = _Submission.Agent.MailStopCode;
                    bos.Agent.Middle = _Submission.Agent.Middle;
                    bos.Agent.ModifiedBy = _Submission.Agent.ModifiedBy;
                    bos.Agent.ModifiedDt = _Submission.Agent.ModifiedDt.HasValue ? _Submission.Agent.ModifiedDt.Value : DateTime.MinValue;
                    bos.Agent.Prefix = _Submission.Agent.Prefix;
                    bos.Agent.PrimaryPhone = _Submission.Agent.PrimaryPhone;
                    bos.Agent.PrimaryPhoneAreaCode = _Submission.Agent.PrimaryPhoneAreaCode;
                    bos.Agent.ProfessionalTitle = _Submission.Agent.ProfessionalTitle;
                    bos.Agent.SecondaryPhone = _Submission.Agent.SecondaryPhone;
                    bos.Agent.SecondaryPhoneAreaCode = _Submission.Agent.SecondaryPhoneAreaCode;
                    bos.Agent.SuffixTitle = _Submission.Agent.SuffixTitle;
                    bos.Agent.Surname = _Submission.Agent.Surname;
                    bos.Agent.UserId = _Submission.Agent.UserId.HasValue ? _Submission.Agent.UserId.Value : 0;
                }


                // contact
                bos.ContactId = _Submission.ContactId.HasValue ? _Submission.ContactId.Value : 0;
                if (_Submission.Contact != null)
                {
                    bos.Contact = new BCS.Core.Clearance.BizObjects.Contact();
                    bos.Contact.AgencyId = _Submission.Contact.AgencyId;
                    bos.Contact.DateOfBirth = _Submission.Contact.DateOfBirth.HasValue ? _Submission.Contact.DateOfBirth.Value : DateTime.MinValue;
                    bos.Contact.Email = _Submission.Contact.Email;
                    bos.Contact.Fax = _Submission.Contact.Fax;
                    bos.Contact.Id = _Submission.Contact.Id;
                    bos.Contact.Name = _Submission.Contact.Name;
                    bos.Contact.PrimaryPhone = _Submission.Contact.PrimaryPhone;
                    bos.Contact.PrimaryPhoneExtension = _Submission.Contact.PrimaryPhoneExtension;
                    bos.Contact.SecondaryPhone = _Submission.Contact.SecondaryPhone;
                    bos.Contact.SecondaryPhoneExtension = _Submission.Contact.SecondaryPhoneExtension;
                }

                //agency
                bos.AgencyId = _Submission.AgencyId;
                if (_Submission.Agency != null)
                {
                    bos.Agency = new BCS.Core.Clearance.BizObjects.Agency();
                    bos.Agency.Active = _Submission.Agency.Active;
                    bos.Agency.AgencyName = _Submission.Agency.AgencyName;
                    bos.Agency.AgencyNumber = _Submission.Agency.AgencyNumber;
                    bos.Agency.City = _Submission.Agency.City;
                    bos.Agency.CompanyNumberId = _Submission.Agency.CompanyNumberId;
                    bos.Agency.EntryBy = _Submission.Agency.EntryBy;
                    bos.Agency.EntryDate = _Submission.Agency.EntryDt;
                    bos.Agency.FEIN = _Submission.Agency.FEIN;
                    bos.Agency.Id = _Submission.Agency.Id;
                    bos.Agency.MasterAgencyId = _Submission.Agency.MasterAgencyId.HasValue ? _Submission.Agency.MasterAgencyId.Value : 0;
                    bos.Agency.ModifiedBy = _Submission.Agency.ModifiedBy;
                    bos.Agency.ModifiedDt = _Submission.Agency.ModifiedDt.HasValue ? _Submission.Agency.ModifiedDt.Value : DateTime.MinValue;
                    bos.Agency.State = _Submission.Agency.State;

                    bos.AgencyName = _Submission.Agency.AgencyName;
                }

                bos.ClientCoreClientId = _Submission.ClientCoreClientId;
                bos.Deleted = _Submission.Deleted;
                bos.DeletedBy = _Submission.DeletedBy;
                bos.DeletedDate = _Submission.DeletedDt.HasValue ? _Submission.DeletedDt.Value : DateTime.MinValue;

                #region client core client address ids
                List<int> ccids = new List<int>();

                List<SubmissionClientCoreClientAddress> addresses = _clientAddresses.Where(s => s.SubmissionId == _Submission.Id).ToList();

                foreach (SubmissionClientCoreClientAddress var in addresses)
                {
                    ccids.Add(var.ClientCoreClientAddressId);
                }
                bos.ClientCoreAddressIds = new int[ccids.Count];
                ccids.CopyTo(bos.ClientCoreAddressIds);
                // TODO: for the moment, we will have old element populated too to easily identify submissions with a address
                // on the default.aspx page.
                string[] strccids = Array.ConvertAll<int, string>(bos.ClientCoreAddressIds, new Converter<int, string>(Convert.ToString));
                bos.ClientCoreAddressIdsString = string.Join(",", strccids);
                #region TODO: stop gap for other apps to catch up
                if (ccids.Count > 0)
                {
                    bos.ClientCoreAddressId = ccids[0].ToString();
                }
                else
                {
                    bos.ClientCoreAddressId = "0";
                }
                #endregion

                #endregion
                #region client core client name ids
                ccids = new List<int>();

                List<SubmissionClientCoreClientName> clientNames = _clientNames.Where(s => s.SubmissionId == _Submission.Id).ToList();

                foreach (SubmissionClientCoreClientName var in clientNames)
                {
                    ccids.Add(var.ClientCoreNameId);
                }
                bos.ClientCoreNameIds = new int[ccids.Count];
                ccids.CopyTo(bos.ClientCoreNameIds);

                #endregion




                #region Submission Comments

                List<SubmissionComment> comments = _comments.Where(s => s.SubmissionId == _Submission.Id).ToList();
                if (comments.Count > 0)
                {

                    // bubble up and latest comment as comment for easy display in grids
                    bos.Comments = comments.ToList().SortDESC("EntryDt").First().Comment;

                    bos.SubmissionComments = new BCS.Core.Clearance.BizObjects.SubmissionComment[comments.Count];
                    for (int j = 0; j < comments.Count; j++)
                    {
                        bos.SubmissionComments[j] = new BCS.Core.Clearance.BizObjects.SubmissionComment();

                        bos.SubmissionComments[j].Comment = comments.ElementAt(j).Comment;
                        bos.SubmissionComments[j].EntryBy = comments.ElementAt(j).EntryBy;
                        bos.SubmissionComments[j].EntryDt = comments.ElementAt(j).EntryDt;
                        bos.SubmissionComments[j].Id = comments.ElementAt(j).Id;
                        bos.SubmissionComments[j].ModifiedBy = comments.ElementAt(j).ModifiedBy;
                        bos.SubmissionComments[j].ModifiedDt = comments.ElementAt(j).ModifiedDt.HasValue ? comments.ElementAt(j).ModifiedDt.Value : DateTime.MinValue;
                        bos.SubmissionComments[j].SubmissionId = comments.ElementAt(j).SubmissionId;
                    }
                }
                else
                {
                    bos.SubmissionComments = new BCS.Core.Clearance.BizObjects.SubmissionComment[0];
                }
                #endregion




                bos.CompanyNumberId = _Submission.CompanyNumberId;
                if (_Submission.CompanyNumber != null)
                    bos.CompanyId = _Submission.CompanyNumber.CompanyId;
                bos.EffectiveDate = _Submission.EffectiveDate.HasValue ? _Submission.EffectiveDate.Value : DateTime.MinValue;
                bos.EstimatedWrittenPremium = _Submission.EstimatedWrittenPremium.HasValue ? _Submission.EstimatedWrittenPremium.Value : 0;
                bos.OtherCarrierPremium = _Submission.OtherCarrierPremium.HasValue ? _Submission.OtherCarrierPremium.Value : 0;
                bos.ExpirationDate = _Submission.ExpirationDate.HasValue ? _Submission.ExpirationDate.Value : DateTime.MinValue;
                bos.Id = _Submission.Id;
                bos.PolicyNumber = string.IsNullOrEmpty(_Submission.PolicyNumber) ? string.Empty : _Submission.PolicyNumber;
                if (_Submission.PolicySystem != null)
                {
                    bos.PolicySystem = new BCS.Core.Clearance.BizObjects.PolicySystem();
                    bos.PolicySystem.Abbreviation = _Submission.PolicySystem.Abbreviation;
                    bos.PolicySystem.AssociatedCompanyNumberId = _Submission.PolicySystem.AssociatedCompanyNumberId;
                    bos.PolicySystem.Id = _Submission.PolicySystem.Id;
                    bos.PolicySystem.PropertyPolicySystem = _Submission.PolicySystem.PolicySystem1;
                }
                bos.SubmissionBy = _Submission.SubmissionBy;
                bos.SubmissionDt = _Submission.SubmissionDt;
                bos.SubmissionNumber = _Submission.SubmissionNumber;
                bos.InsuredName = _Submission.InsuredName;
                bos.Dba = _Submission.Dba;

                // other carrier
                if (_Submission.OtherCarrierId.HasValue)
                    bos.OtherCarrierId = _Submission.OtherCarrierId.Value;
                if (_Submission.OtherCarrier != null)
                {
                    bos.OtherCarrier = new BCS.Core.Clearance.BizObjects.OtherCarrier();
                    bos.OtherCarrier.Id = _Submission.OtherCarrier.Id;
                    bos.OtherCarrier.Code = _Submission.OtherCarrier.Code;
                    bos.OtherCarrier.Description = _Submission.OtherCarrier.Description;
                }

                // status
                if (_Submission.SubmissionStatusId.HasValue)
                    bos.SubmissionStatusId = _Submission.SubmissionStatusId.Value;
                if (_Submission.SubmissionStatus != null)
                {
                    bos.SubmissionStatus = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
                    bos.SubmissionStatus.Id = _Submission.SubmissionStatus.Id;
                    bos.SubmissionStatus.StatusCode = _Submission.SubmissionStatus.StatusCode;
                }
                // status reason
                if (_Submission.SubmissionStatusReasonId.HasValue)
                {
                    bos.SubmissionStatusReasonId = _Submission.SubmissionStatusReasonId.Value;
                    bos.SubmissionStatusReason = _Submission.SubmissionStatusReason.ReasonCode;
                }

                bos.SubmissionStatusDate = _Submission.SubmissionStatusDate.HasValue ? _Submission.SubmissionStatusDate.Value : DateTime.MinValue;
                bos.SubmissionStatusReasonOther = _Submission.SubmissionStatusReasonOther;

                if (_Submission.SubmissionTokenId.HasValue)
                    bos.SubmissionTokenId = _Submission.SubmissionTokenId.Value;

                // type
                bos.SubmissionTypeId = _Submission.SubmissionTypeId;
                if (_Submission.SubmissionType != null)
                {
                    bos.SubmissionType = new BCS.Core.Clearance.BizObjects.SubmissionType();
                    bos.SubmissionType.Abbreviation = _Submission.SubmissionType.Abbreviation;
                    bos.SubmissionType.Id = _Submission.SubmissionType.Id;
                    bos.SubmissionType.TypeName = _Submission.SubmissionType.TypeName;
                }
                bos.UnderwriterId = _Submission.UnderwriterPersonId.HasValue ? _Submission.UnderwriterPersonId.Value : 0;
                bos.UnderwriterName = _Submission.UnderwriterPersonId == null ? string.Empty : _Submission.Person.UserName;
                bos.UnderwriterFullName = _Submission.UnderwriterPersonId == null ? string.Empty : _Submission.Person.FullName;
                bos.AnalystId = _Submission.AnalystPersonId.HasValue ? _Submission.AnalystPersonId.Value : 0;
                bos.AnalystName = _Submission.AnalystPersonId == null ? string.Empty : _Submission.Person1.UserName;
                bos.AnalystFullName = _Submission.AnalystPersonId == null ? string.Empty : _Submission.Person1.FullName;
                bos.TechnicianId = _Submission.TechnicianPersonId.HasValue ? _Submission.TechnicianPersonId.Value : 0;
                bos.TechnicianName = _Submission.TechnicianPersonId == null ? string.Empty : _Submission.Person2.UserName;
                bos.TechnicianFullName = _Submission.TechnicianPersonId == null ? string.Empty : _Submission.Person2.FullName;
                bos.PolicySystemId = _Submission.PolicySystemId.HasValue ? _Submission.PolicySystemId.Value : 0;
                bos.UpdatedBy = _Submission.UpdatedBy;
                bos.UpdatedDt = _Submission.UpdatedDt.HasValue ? _Submission.UpdatedDt.Value : DateTime.MinValue;
                bos.CancellationDate = _Submission.CancellationDate.HasValue ? _Submission.CancellationDate.Value : DateTime.MinValue;




                #region previous underwriter

                //List<SubmissionHistory> shc = _Submission.SubmissionHistory.ToList();

                //shc = shc.Where(s => s.UnderwriterPersonId != 0 && s.UnderwriterPersonId != null && s.UnderwriterPersonId != bos.UnderwriterId).ToList().SortDESC("HistoryDt");

                //if (shc.Count > 0)
                //{
                //    bos.PreviousUnderwriterId = shc[0].UnderwriterPersonId.Value;

                //    Person previousPerson = DataAccess.GetEntityById<Person>(shc.First().UnderwriterPersonId.ToInt());
                //    // person can be deleted, so if exists

                //    if (null != previousPerson)
                //    {
                //        bos.PreviousUnderwriterName = previousPerson.UserName;
                //        bos.PreviousUnderwriterFullName = previousPerson.FullName;
                //    }

                //}
                #endregion

                #region SubmissionLineOfBusinessChildren

                List<SubmissionLineOfBusinessChildrenView> sLobs = _lobsC.Where(s => s.SubmissionId == _Submission.Id).ToList();

                if (sLobs.Count > 0)
                {
                    bos.LineOfBusinessChildList = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild[sLobs.Count];
                    for (int j = 0; j < sLobs.Count; j++)
                    {
                        bos.LineOfBusinessChildList[j] = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild();

                        bos.LineOfBusinessChildList[j].Id = sLobs.ElementAt(j).Id;
                        bos.LineOfBusinessChildList[j].CompanyNumberLOBGridId = sLobs.ElementAt(j).CompanyNumberLOBGridId;
                        bos.LineOfBusinessChildList[j].SubmissionStatusId =
                            sLobs.ElementAt(j).SubmissionStatusId.HasValue ? sLobs.ElementAt(j).SubmissionStatusId.Value : 0;
                        bos.LineOfBusinessChildList[j].SubmissionStatusReasonId =
                            sLobs.ElementAt(j).SubmissionStatusReasonId.HasValue ? sLobs.ElementAt(j).SubmissionStatusReasonId.Value : 0;

                        if (sLobs.ElementAt(j).CompanyNumberLOBGridId > 0)
                        {
                            bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid = new BCS.Core.Clearance.BizObjects.CompanyNumberLOBGrid();
                            bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Code = sLobs.ElementAt(j).MulitLOBCode;
                            bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.CompanyNumberId = sLobs.ElementAt(j).CompanyNumberId;
                            bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Description = sLobs.ElementAt(j).MultiLOBDesc;
                            bos.LineOfBusinessChildList[j].CompanyNumberLOBGrid.Id = sLobs.ElementAt(j).CompanyNumberLOBGridId;
                        }
                        if (sLobs.ElementAt(j).SubmissionStatusId.HasValue)
                        {
                            bos.LineOfBusinessChildList[j].SubmissionStatus = new BCS.Core.Clearance.BizObjects.SubmissionStatus();
                            bos.LineOfBusinessChildList[j].SubmissionStatus.Id = sLobs.ElementAt(j).SubmissionStatusId.Value;
                            bos.LineOfBusinessChildList[j].SubmissionStatus.StatusCode = sLobs.ElementAt(j).StatusCode;
                        }
                        if (sLobs.ElementAt(j).SubmissionStatusReasonId.HasValue)
                        {
                            bos.LineOfBusinessChildList[j].SubmissionStatusReason = new BCS.Core.Clearance.BizObjects.SubmissionStatusReason();
                            bos.LineOfBusinessChildList[j].SubmissionStatusReason.Id = sLobs.ElementAt(j).SubmissionStatusReasonId.Value;
                            bos.LineOfBusinessChildList[j].SubmissionStatusReason.ReasonCode = sLobs.ElementAt(j).ReasonCode;
                        }
                    }
                    #region first lob child
                    int index0 = 0;
                    {
                        bos.FirstSubmissionLineOfBusinessChild = new BCS.Core.Clearance.BizObjects.FirstSubmissionLineOfBusinessChild();
                        bos.FirstSubmissionLineOfBusinessChild.Id = sLobs.ElementAt(index0).Id;
                        bos.FirstSubmissionLineOfBusinessChild.LobId = sLobs.ElementAt(index0).CompanyNumberLOBGridId;

                        if (sLobs.ElementAt(index0).CompanyNumberLOBGridId > 0)
                        {
                            bos.FirstSubmissionLineOfBusinessChild.LobCode = sLobs.ElementAt(index0).MulitLOBCode;
                            bos.FirstSubmissionLineOfBusinessChild.LobDescription = sLobs.ElementAt(index0).MultiLOBDesc;
                        }
                    }
                    #endregion
                }
                else
                {
                    bos.LineOfBusinessChildList = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild[0];
                }
                #endregion

                al.Add(bos);

                i++;
                if (i > max)
                    break;


            }

            //list.List = new BCS.Core.Clearance.BizObjects.Submission[al.Count];
            //al.CopyTo(list.List);
            list.List = al.ToArray();
            list.HasNextPage = i > max;
            list.PageNo = 1;
            list.PageSize = al.Count;

            return list;
        }



        #endregion
    }
}
