#region Using
using System;
using System.Collections.Generic;
using System.Text;
using BCS.Core.Clearance.Admin;
using BCS.Biz;
using System.Web.UI;
using BCS.Core.Clearance.AgencyPortStructures;
using structures = BTS.ClientCore.Wrapper.Structures;
using logs = BTS.LogFramework;
using System.Configuration;
using System.Data.SqlTypes;
using BO = BCS.Core.Clearance.BizObjects;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Serialization;
#endregion

namespace BCS.Core.Clearance
{
    /// <summary>
    /// 
    /// </summary>
    public class AgencyPort
    {

        public BO.Response Submit(string AgencyLocationNumber, string clientId, string CustomerName, string CustomerDBA, string CustomerLegalEntity,
           BCS.Core.Clearance.AgencyPortStructures.AddressType CustomerAddress, string CustomerPhoneNumber, string TaxId,
           string CompanyNumber, CallType CallType, MatchType matchType, int WorkItemId, string submissionXML)
        {
            return Submit(AgencyLocationNumber, clientId, CustomerName, CustomerDBA, CustomerLegalEntity,
            CustomerAddress, CustomerPhoneNumber, TaxId,
            CompanyNumber, CallType, matchType, WorkItemId, submissionXML,false);
        }
        /// <param name="AgencyLocationNumber">AgencyNumber in clearance</param>
        /// <param name="clientId">Client Id, other search criteria are ignored when its passed</param>
        /// <param name="CustomerName">Name</param>
        /// <param name="CustomerDBA">Clearance side name type?</param>
        /// <param name="CustomerLegalEntity">?</param>
        /// <param name="CustomerAddress"></param>
        /// <param name="CustomerPhoneNumber">Phone Number</param>
        /// <param name="TaxId">optional</param>
        /// <param name="WorkItemId">AgencyPort work item Id. Not yet used</param>
        /// <param name="CallType"></param>
        /// <param name="CompanyInitials"></param>
        /// <param name="submissionXML"></param>
        public BO.Response Submit(string AgencyLocationNumber, string clientId, string CustomerName, string CustomerDBA, string CustomerLegalEntity,
            BCS.Core.Clearance.AgencyPortStructures.AddressType CustomerAddress, string CustomerPhoneNumber, string TaxId,
            string CompanyNumber, CallType CallType, MatchType matchType, int WorkItemId, string submissionXML, bool isSearchScreen)
        {
            string failureMessage = string.Empty;

            // check if anything was passed for client at all
            if (string.IsNullOrEmpty(CustomerDBA) && string.IsNullOrEmpty(CustomerLegalEntity) && string.IsNullOrEmpty(CustomerName)
                && string.IsNullOrEmpty(TaxId) && string.IsNullOrEmpty(clientId))
            {
                failureMessage = "No client information provided.";
                return BuildResponse(ClearanceStatus.NotCleared, failureMessage, null);
            }

            //string CompanyInitials = GetCompanyInitials(CompanyNumber);
            Biz.Agency agency = GetAgency(CompanyNumber, AgencyLocationNumber);
            if (agency == null)
            {
                failureMessage = string.Format("Agency Port : Submitting agency is not found in clearance for agency number : '{0}'", AgencyLocationNumber);
                logs.LogCentral.Current.LogError(failureMessage);
                return BuildResponse(ClearanceStatus.NotCleared, failureMessage, null);
            }
            //int daysBlockHeld = GetDaysBlockHeld(CompanyInitials);

            DateTime baseDate = DateTime.Today;
            //int matchLength = GetMatchLength(CompanyInitials);
            int matchLength = agency.CompanyNumber.Company.AgencyPortMatchLength.Value;
            if (matchLength > 0)
            {
                if (Common.IsNotNullOrEmpty(CustomerDBA))
                {
                    if (CustomerDBA.Length > matchLength)
                    {
                        CustomerDBA = CustomerDBA.Remove(matchLength);
                    }
                }
                if (Common.IsNotNullOrEmpty(CustomerLegalEntity))
                {
                    if (CustomerLegalEntity.Length > matchLength)
                    {
                        CustomerLegalEntity = CustomerLegalEntity.Remove(matchLength);
                    }
                }
                if (Common.IsNotNullOrEmpty(CustomerName))
                {
                    if (CustomerName.Length > matchLength)
                    {
                        CustomerName = CustomerName.Remove(matchLength);
                    }
                }
            }

            if (CustomerAddress == null)
                CustomerAddress = new BCS.Core.Clearance.AgencyPortStructures.AddressType();
            // get clients
            structures.Search.Vector vector = new BTS.ClientCore.Wrapper.Structures.Search.Vector();
            // TODO: required input for client search
            string clientsXML = string.Empty;
            if (Common.IsNotNullOrEmpty(clientId))
            {
                vector.Clients = new BTS.ClientCore.Wrapper.Structures.Search.Client[1];
                vector.Clients[0] = new BTS.ClientCore.Wrapper.Structures.Search.Client();
                vector.Clients[0].Info = new BTS.ClientCore.Wrapper.Structures.Search.ClientInfo();
                vector.Clients[0].Info.ClientId = clientId;
            }
            else
            {
                if (Common.IsNotNullOrEmpty(TaxId))
                {
                    clientsXML = Clients.GetClientByTaxId(CompanyNumber, TaxId);
                    vector = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));

                    // enhancement request, to search by remaining criteria, if not found by FEIN. Earlier it was if FEIN is passed, ignore others
                    // just make taxid empty and call this method again
                    if ((Common.IsNotNullOrEmpty(vector.Error) || Common.IsNotNullOrEmpty(vector.Message)) && (vector.Clients == null || vector.Clients.Length == 0))
                    {
                        string ignoredTaxId = string.Empty;
                        return Submit(AgencyLocationNumber, clientId, CustomerName, CustomerDBA, CustomerLegalEntity, CustomerAddress,
                            CustomerPhoneNumber, ignoredTaxId, CompanyNumber, CallType, matchType, WorkItemId, submissionXML);
                    }
                }
                else
                {
                    Dictionary<string, structures.Search.Client> dictAllClients = new Dictionary<string, BTS.ClientCore.Wrapper.Structures.Search.Client>();
                    structures.Search.Vector v;

                    //MatchType matchType = GetMatchType(CompanyInitials);
                    switch (matchType)
                    {
                        case MatchType.AddressOrName:
                            {
                                #region separately search on each name type and address
                                // search on customer name, no type
                                if (Common.IsNotNullOrEmpty(CustomerName))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, string.Empty, string.Empty,
                                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, CustomerName);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }

                                // search on customer name, dba type
                                if (Common.IsNotNullOrEmpty(CustomerDBA))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, string.Empty, string.Empty,
                                            string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, CustomerDBA);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }

                                // search on customer name, legal type                    
                                if (Common.IsNotNullOrEmpty(CustomerLegalEntity))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, string.Empty, string.Empty,
                                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, CustomerLegalEntity);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }

                                // search on address
                                if (IsNotNullOrEmpty(CustomerAddress))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, CustomerAddress.HouseNumber, CustomerAddress.ADDR1,
                                            CustomerAddress.Item, CustomerAddress.State, CustomerAddress.Zip, string.Empty, string.Empty, string.Empty);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }
                                #endregion
                                break;
                            }
                        case MatchType.AddressAndName:
                            {
                                #region search combining (each name type + address)
                                // search on customer name, no type and address
                                if (Common.IsNotNullOrEmpty(CustomerName))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, CustomerAddress.HouseNumber, CustomerAddress.ADDR1,
                                            CustomerAddress.Item, CustomerAddress.State, CustomerAddress.Zip, string.Empty, string.Empty, CustomerName);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }

                                // search on customer name, dba type and address
                                if (Common.IsNotNullOrEmpty(CustomerDBA))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, CustomerAddress.HouseNumber, CustomerAddress.ADDR1,
                                            CustomerAddress.Item, CustomerAddress.State, CustomerAddress.Zip, string.Empty, string.Empty, CustomerDBA);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }

                                // search on customer name, legal type and address
                                if (Common.IsNotNullOrEmpty(CustomerLegalEntity))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, CustomerAddress.HouseNumber, CustomerAddress.ADDR1,
                                            CustomerAddress.Item, CustomerAddress.State, CustomerAddress.Zip, string.Empty, string.Empty, CustomerLegalEntity);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }
                                #endregion
                                break;
                            }
                        default:
                            throw new ApplicationException("Invalid Match type");
                    }



                    vector.Clients = new BTS.ClientCore.Wrapper.Structures.Search.Client[dictAllClients.Count];
                    dictAllClients.Values.CopyTo(vector.Clients, 0);
                } 
            }            
            if ((Common.IsNotNullOrEmpty(vector.Error) || Common.IsNotNullOrEmpty(vector.Message)) && (vector.Clients == null || vector.Clients.Length == 0))
            {
                failureMessage = string.Format("Client does not exist.");
                // TODO: Add a client, what if only partial client info is passed, like taxid and no names and addresses
                return BuildResponse(ClearanceStatus.NotCleared, failureMessage, null);
            }
            // check for multiple matches, TODO: analyze what constitutes a match
            if (vector.Clients.Length > 1)
            {
                failureMessage = "Multiple clients were found.";
                return BuildResponse(ClearanceStatus.NotCleared, failureMessage, null);
            }

            // proceed for submissions, since we are dealing with only one client match
            Triplet criteria = new Triplet(JoinPath.Submission.Columns.ClientCoreClientId, vector.Clients[0].Info.ClientId);
            Biz.SubmissionCollection submissions = AgencyPortSubmissions.SearchSubmissions(false, CompanyNumber, criteria);
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberSubmissionStatus.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
            Biz.CompanyNumberSubmissionStatusCollection companyStatuses = dm.GetCompanyNumberSubmissionStatusCollection();
            // check to see if any submission matches
            foreach (Biz.Submission var in submissions)
            {
                bool blockFound = false;

                if (var.AgencyId.Value == agency.Id)
                    continue;
                if ( (!var.Agency.MasterAgencyId.IsNull) && (!agency.MasterAgencyId.IsNull))
                {
                    if (!var.Agency.MasterAgencyId.Equals(agency.MasterAgencyId))
                    {
                        blockFound = true;
                    }
                }

                if (!var.SubmissionStatusId.IsNull)
                {
                    Biz.CompanyNumberSubmissionStatus varSubmissionStatus = companyStatuses.FindBySubmissionStatusId(var.SubmissionStatusId.Value);
                    if (varSubmissionStatus.IsClearable)
                    {
                        int daysBlockHeld = varSubmissionStatus.DaysStatusIsActive.IsNull ? 0 : varSubmissionStatus.DaysStatusIsActive.Value;
                        if (var.SubmissionDt.Value.AddDays(daysBlockHeld) > baseDate)
                        {
                            blockFound = true;
                        }
                    }
                    else // if not clearable
                    {
                        blockFound = true; 
                    }
                }

                if (blockFound)
                {
                    if (CallType != CallType.Override)
                    {
                        failureMessage = string.Format("An existing submission is found by agency, name {0}, number {1}.", var.Agency.AgencyName, var.Agency.AgencyNumber);
                        return BuildResponse(ClearanceStatus.NotCleared, failureMessage, null);
                    }
                    else
                    {
                        // a block is found, no need to continue.
                        break;
                    }
                }
            }
            switch (CallType)
            {
                case CallType.Query:
                    {
                        // by this time, nothing should be found that would indicate a not cleared status, so send a cleared status
                        return BuildResponse(ClearanceStatus.Cleared, string.Empty, null);
                    }
                case CallType.Override:
                case CallType.Submission:
                    {
                        string separator = "|";
                        // get client info
                        string aclientstr = Clients.GetClientById(CompanyNumber, vector.Clients[0].Info.ClientId, true, isSearchScreen);
                        BO.ClearanceClient foundClient = (BO.ClearanceClient)XML.Deserializer.Deserialize(aclientstr, typeof(BO.ClearanceClient), "seq_nbr");

                        // build submission add parameters
                        BO.Submission aSubmission = (BO.Submission)XML.Deserializer.Deserialize(submissionXML, typeof(BO.Submission));                        
                        
                        #region address ids
                        List<string> lstAddresses = new List<string>();
                        foreach (structures.Info.Address var in foundClient.ClientCoreClient.Addresses)
                        {
                            lstAddresses.Add(var.AddressId);
                        }
                        string[] arrAddresses = new string[lstAddresses.Count];
                        lstAddresses.CopyTo(arrAddresses);
                        #endregion

                        #region name ids
                        List<string> lstNames = new List<string>();
                        foreach (structures.Info.Name var in foundClient.ClientCoreClient.Client.Names)
                        {
                            lstNames.Add(var.SequenceNumber);
                        }
                        string[] arrNames = new string[lstNames.Count];
                        lstNames.CopyTo(arrNames);
                        #endregion

                        #region Agent Unspecified Reasons
                        string agentunspecifiedreasons = string.Empty;
                        dm = Common.GetDataManager();
                        if (aSubmission.AgentUnspecifiedReasonList != null && aSubmission.AgentUnspecifiedReasonList.Length > 0)
                        {
                            dm.QueryCriteria.And(JoinPath.AgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReason.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            AgentUnspecifiedReasonCollection allAgentReasons = dm.GetAgentUnspecifiedReasonCollection();

                            List<string> ids = new List<string>();
                            for (int i = 0; i < aSubmission.AgentUnspecifiedReasonList.Length; i++)
                            {
                                AgentUnspecifiedReason reason = allAgentReasons.FindByReason(aSubmission.AgentUnspecifiedReasonList[i].Reason);
                                if (reason != null)
                                {
                                    ids.Add(reason.Id.ToString());
                                }
                            }
                            string[] arrAgentReasons = new string[ids.Count];
                            ids.CopyTo(arrAgentReasons);
                            agentunspecifiedreasons = string.Join(separator, arrAgentReasons);
                        }
                        #endregion

                        #region Code types
                        string codeids = string.Empty;


                        if (aSubmission.SubmissionCodeTypess != null && aSubmission.SubmissionCodeTypess.Length > 0)
                        {
                            #region Get all code types for this company number
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.CompanyNumberCodeType.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            CompanyNumberCodeTypeCollection allTypes = dm.GetCompanyNumberCodeTypeCollection();
                            #endregion

                            #region Get all codes for this company number
                            dm.QueryCriteria.Clear();
                            dm.QueryCriteria.And(JoinPath.CompanyNumberCode.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            CompanyNumberCodeCollection allCodes = dm.GetCompanyNumberCodeCollection();
                            #endregion

                            List<string> listcodeids = new List<string>();
                            for (int i = 0; i < aSubmission.SubmissionCodeTypess.Length; i++)
                            {
                               CompanyNumberCodeType cncType = allTypes.FindByCodeName(aSubmission.SubmissionCodeTypess[i].CodeName);
                               if (cncType != null)
                               {
                                   CompanyNumberCode cnCode = allCodes.FindByCode(aSubmission.SubmissionCodeTypess[i].CodeValue);
                                   if (cnCode != null)
                                   {
                                       listcodeids.Add(cnCode.Id.ToString());
                                   }
                               }
                            }

                            string[] scodeids = new string[listcodeids.Count];
                            listcodeids.CopyTo(scodeids);
                            codeids = string.Join(separator, scodeids);
                        }
                        #endregion

                        #region Line Of Businesses
                        string lobss = string.Empty;
                        string lobid = string.Empty;
                        if (aSubmission.LineOfBusinessList != null && aSubmission.LineOfBusinessList.Length > 0)
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            LineOfBusinessCollection allLobs = dm.GetLineOfBusinessCollection();

                            List<string> ids = new List<string>();                            
                            for (int i = 0; i < aSubmission.LineOfBusinessList.Length; i++)
                            {
                                LineOfBusiness lob = allLobs.FindByCode(aSubmission.LineOfBusinessList[i].Code);
                                if (lob != null)
                                {
                                    ids.Add(lob.Id.ToString());
                                }
                            }
                            string[] arr = new string[ids.Count];
                            ids.CopyTo(arr);
                            lobss = string.Join(separator, arr);
                            if (arr.Length > 0)
                                lobid = arr[0];
                        }
                        #endregion
            
                        #region TODO: Line Of Business Children
                        string lobchildren = string.Empty;
                        
                        #endregion

                        #region Attributes
                        string attributes = string.Empty;
                        if (aSubmission.AttributeList != null && aSubmission.AttributeList.Length > 0)
                        {
                            string[] sattributes = new string[aSubmission.AttributeList.Length];
                            for (int i = 0; i < aSubmission.AttributeList.Length; i++)
                            {
                                //sattributes[i] = aSubmission.AttributeList[i].CompanyNumberAttributeId.ToString();
                                sattributes[i] = string.Format("{0}={1}", aSubmission.AttributeList[i].CompanyNumberAttributeId, aSubmission.AttributeList[i].AttributeValue);
                            }
                            attributes = string.Join("|", sattributes);
                        }
                        #endregion

                        #region Underwriter
                        if (Common.IsNotNullOrEmpty(aSubmission.UnderwriterName))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            dm.QueryCriteria.And(JoinPath.Person.Columns.FullName, aSubmission.UnderwriterName);
                            Person underwriter = dm.GetPerson();
                            if (underwriter != null)
                            {
                                dm = Common.GetDataManager();
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, underwriter.Id);
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agency.Id);
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.LineOfBusinessId, lobid);
                                //dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, );

                                AgencyLineOfBusinessPersonUnderwritingRolePersonCollection realUnderwriters = dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection();
                                realUnderwriters = realUnderwriters.FilterByPersonId(underwriter.Id);

                                if (realUnderwriters.Count == 1)
                                    aSubmission.UnderwriterId = realUnderwriters[0].PersonId;
                            }
                        } 
                        #endregion

                        #region Analyst
                        if (Common.IsNotNullOrEmpty(aSubmission.AnalystName))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            dm.QueryCriteria.And(JoinPath.Person.Columns.FullName, aSubmission.AnalystName);
                            Person analyst = dm.GetPerson();
                            if (analyst != null)
                            {
                                dm = Common.GetDataManager();
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, analyst.Id);
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agency.Id);
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.LineOfBusinessId, lobid);
                                //dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, );

                                AgencyLineOfBusinessPersonUnderwritingRolePersonCollection realAnalysts = dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection();
                                realAnalysts = realAnalysts.FilterByPersonId(analyst.Id);
                                if (realAnalysts.Count ==1)
                                    aSubmission.AnalystId = realAnalysts[0].PersonId;
                            }
                        }
                        #endregion

                        #region Submission Status
                        if (Common.IsNotNullOrEmpty(aSubmission.SubmissionStatusCode))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.SubmissionStatus.CompanyNumberSubmissionStatus.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            dm.QueryCriteria.And(JoinPath.SubmissionStatus.Columns.StatusCode, aSubmission.SubmissionStatusCode);
                            SubmissionStatus status = dm.GetSubmissionStatus();
                            if (status != null)
                            {
                                aSubmission.SubmissionStatusId = status.Id;

                                #region Submission Status Reason
                                if (Common.IsNotNullOrEmpty(aSubmission.SubmissionStatusReason))
                                {
                                    dm = Common.GetDataManager();
                                    dm.QueryCriteria.And(JoinPath.SubmissionStatusReason.SubmissionStatusSubmissionStatusReason.Columns.SubmissionStatusId, status.Id);
                                    dm.QueryCriteria.And(JoinPath.SubmissionStatusReason.Columns.ReasonCode, aSubmission.SubmissionStatusReason);
                                    SubmissionStatusReason statusReason = dm.GetSubmissionStatusReason();
                                    if (statusReason != null)
                                        aSubmission.SubmissionStatusReasonId = statusReason.Id;
                                }
                                #endregion
                            }
                        }
                        #endregion

                        #region Submission Type
                        if (Common.IsNotNullOrEmpty(aSubmission.SubmissionTypeName))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.SubmissionType.CompanyNumberSubmissionType.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            dm.QueryCriteria.And(JoinPath.SubmissionType.Columns.TypeName, aSubmission.SubmissionTypeName);
                            SubmissionType type = dm.GetSubmissionType();
                            if (type != null)
                                aSubmission.SubmissionTypeId = type.Id; 
                        }
                        #endregion

                        #region Agent
                        if (aSubmission.Agent != null && Common.IsNotNullOrEmpty(aSubmission.Agent.Surname))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.Agent.Columns.AgencyId, agency.Id);
                            dm.QueryCriteria.And(JoinPath.Agent.Columns.Surname, aSubmission.Agent.Surname);
                            dm.QueryCriteria.And(JoinPath.Agent.Columns.FirstName, aSubmission.Agent.FirstName);
                            Agent agent = dm.GetAgent();
                            if (agent != null)
                                aSubmission.AgentId = agent.Id;
                        }
                        #endregion

                        #region Contact
                        if (aSubmission.Contact != null && Common.IsNotNullOrEmpty(aSubmission.Contact.Name))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.Contact.Columns.AgencyId, agency.Id);
                            dm.QueryCriteria.And(JoinPath.Contact.Columns.Name, aSubmission.Contact.Name);
                            Contact contact = dm.GetContact();
                            if (contact != null)
                                aSubmission.ContactId = contact.Id;
                        }
                        #endregion

                        #region Other Carrier
                        if (aSubmission.OtherCarrier != null && Common.IsNotNullOrEmpty(aSubmission.OtherCarrier.Code))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.OtherCarrier.CompanyNumberOtherCarrier.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            dm.QueryCriteria.And(JoinPath.OtherCarrier.Columns.Code, aSubmission.OtherCarrier.Code);
                            OtherCarrier otherCarrier = dm.GetOtherCarrier();
                            if (otherCarrier != null)
                                aSubmission.OtherCarrierId = otherCarrier.Id;
                        }
                        #endregion

                        // TODO: other submission parameters may need to be added when agency port decides.
                        string sXML = Submissions.AddSubmission(foundClient.ClientCoreClient.Client.ClientId, string.Join(separator, arrAddresses),
                            string.Join(separator, arrNames), foundClient.ClientCoreClient.Client.PortfolioId, aSubmission.SubmissionBy,
                            aSubmission.SubmissionDt.ToString(),
                            aSubmission.SubmissionTypeId, agency.Id, aSubmission.AgentId, agentunspecifiedreasons,
                            aSubmission.ContactId, CompanyNumber, aSubmission.SubmissionStatusId, aSubmission.SubmissionStatusDate.ToString(),
                            aSubmission.SubmissionStatusReasonId, aSubmission.SubmissionStatusReasonOther, aSubmission.PolicyNumber,
                            aSubmission.EstimatedWrittenPremium, codeids, attributes,
                            aSubmission.EffectiveDate.ToString(), aSubmission.ExpirationDate.ToString(),
                            aSubmission.UnderwriterId, aSubmission.AnalystId,
                            0, // TODO: sending none for technician
                            aSubmission.PolicySystemId, aSubmission.Comments,
                            lobss, lobchildren, aSubmission.InsuredName, aSubmission.Dba, aSubmission.SubmissionNumber.ToString(),
                            aSubmission.OtherCarrierId, aSubmission.OtherCarrierPremium,null,null);

                        BO.Submission coresub = (BCS.Core.Clearance.BizObjects.Submission)BCS.Core.XML.Deserializer.Deserialize(
                            sXML, typeof(BCS.Core.Clearance.BizObjects.Submission));
                        if (coresub != null)
                        {
                            if (coresub.Status == "Success")
                            {
                                return BuildResponse(ClearanceStatus.Cleared, coresub.Message, coresub.SubmissionNumber);
                            }
                            else
                            {

                                return BuildResponse(ClearanceStatus.NotCleared, coresub.Message, null);
                            }
                        }
                        else
                        {
                            return BuildResponse(ClearanceStatus.NotCleared, "Unknown error.", null);
                        }
                    }
                default:
                    {
                        return BuildResponse(ClearanceStatus.NotCleared, "Unknown Call Type.", null);
                    }
            }
        }

        private string GetCompanyInitials(string companyNumber)
        {
            DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            Company company = dm.GetCompany();
            if (company != null)
                return company.CompanyInitials;
            throw new ApplicationException("Invalid company number");
        }

        private bool IsNotNullOrEmpty(BCS.Core.Clearance.AgencyPortStructures.AddressType CustomerAddress)
        {
            if (CustomerAddress == null)
                return true;
            return Common.IsNotNullOrEmpty(CustomerAddress.ADDR1) && Common.IsNotNullOrEmpty(CustomerAddress.HouseNumber) && 
                Common.IsNotNullOrEmpty(CustomerAddress.Item) && Common.IsNotNullOrEmpty(CustomerAddress.State) &&
                Common.IsNotNullOrEmpty(CustomerAddress.Zip);
        }

        public Agency GetAgency(string CompanyNumber, string AgencyLocationNumber)
        {
            DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(JoinPath.Agency.Columns.AgencyNumber, AgencyLocationNumber);
            dm.QueryCriteria.And(JoinPath.Agency.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
            return dm.GetAgency(FetchPath.Agency.CompanyNumber, FetchPath.Agency.CompanyNumber.Company);
        }

        //private string GetCompanyNumber(string CompanyInitials)
        //{
        //    // TODO: sending 40 for the moment
        //    return "40";
        //}

        private BO.Response BuildResponse(ClearanceStatus status, string message, object value)
        {
            BO.Response response = new BO.Response();
            response.Message = message;
            response.Status = StringEnum.GetStringValue(status);
            response.Value = value;

            return response;
        }

        /// <param name="companyNumber"></param>
        /// <param name="agencyNumber"></param>
        /// <param name="dba">Is this clearance side dba flag</param>
        /// <param name="state">state abbreviation</param>
        /// <param name="clientName"></param>
        public string SearchClients(string companyNumber, string agencyNumber, string dba, string state, string clientName, bool isSearchScreen)
        {
            // get the company number to search on
            Agency agency = GetAgency(companyNumber, agencyNumber);
            if (agency == null)
                return XML.Serializer.SerializeAsXml(new ClientResults());
                        
            // get client ids for this agencies submissions
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(JoinPath.Submission.Columns.AgencyId, agency.Id);
            SubmissionCollection submissions = dm.GetSubmissionCollection();
            List<string> clientIds = new List<string>();
            foreach (Submission item in submissions)
            {
                if (!clientIds.Contains(item.ClientCoreClientId.Value.ToString()))
                {
                    clientIds.Add(item.ClientCoreClientId.Value.ToString());
                }
            }
            string[] ids = new string[clientIds.Count];
            clientIds.CopyTo(ids);
            string clientsXML = Clients.GetClientsByIds(companyNumber, ids, isSearchScreen);
            structures.Search.Vector o = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));

            if (o.Clients == null || o.Clients.Length == 0)
                return XML.Serializer.SerializeAsXml(new ClientResults());

            List<ClientResultsClientInfo> apClients = new List<ClientResultsClientInfo>();
            foreach (structures.Search.Client client in o.Clients)
            {                
                ClientResultsClientInfo apClient = new ClientResultsClientInfo();
                // TODO: ignoring the statement below. Not sure if Id(int) will be passed or agencyNumber(string) will be passed
                //apClient.AgencyId = new int[] { agencyNumber };

                apClient.Id = client.Info.ClientId;

                #region Addresses
                apClient.Address = new AgencyPortStructures.AddressType[client.Addresses.Length];
                List<AgencyPortStructures.AddressType> apAddresses = new List<AgencyPortStructures.AddressType>();
                foreach (structures.Search.AddressVersion av in client.Addresses)
                {
                    AgencyPortStructures.AddressType addressType = new AgencyPortStructures.AddressType();
                    addressType.ADDR1 = av.Address1;
                    addressType.ADDR2 = av.Address2;
                    addressType.ADDR3 = av.Address3;
                    addressType.ADDR4 = av.Address4;                    
                    addressType.Country = av.CountryId;
                    addressType.HouseNumber = av.HouseNumber;
                    addressType.Item = av.City;
                    addressType.Item1 = av.County;
                    addressType.State = av.StateProvCode;
                    addressType.Zip = av.PostalCode;
                    
                    apAddresses.Add(addressType);
                }
                apAddresses.CopyTo(apClient.Address); 
                #endregion

                // dont filter if dba is not passed
                bool DBAFound = string.IsNullOrEmpty(dba);
                foreach (structures.Search.ClientName cn in client.Names)
                {
                    if (DBAFound)
                        break;
                    // TODO: confirm "DBA" is the name type for dba's.
                    if (cn.NameType == "DBA")
                    {
                        if (cn.NameBusiness.Contains(dba))
                            DBAFound = true;
                    }
                }
                bool nameFound = string.IsNullOrEmpty(clientName);

                foreach (structures.Search.ClientName cn in client.Names)
                {
                    if (nameFound)
                        break;

                    if (cn.NameBusiness.Contains(clientName))
                        nameFound = true;
                }

                if (DBAFound && nameFound)
                {
                    foreach (structures.Search.ClientName cn in client.Names)
                    {
                        if (cn.PrimaryNameFlag == "Y")
                        {
                            apClient.ClientName = cn.NameBusiness;
                        }
                    }

                    apClients.Add(apClient);
                }
            }
            
            List<ClientResultsClientInfo> reClients = new List<ClientResultsClientInfo>();
            if (Common.IsNotNullOrEmpty(state))
            {
                // filter for state
                foreach (ClientResultsClientInfo apClient in apClients)
                {
                    foreach (AgencyPortStructures.AddressType at in apClient.Address)
                    {
                        if (at.State == state)
                        {
                            if (!reClients.Contains(apClient))
                                reClients.Add(apClient);
                        }
                    }
                }
            }
            else
                reClients.AddRange(apClients);

            ClientResults results = new ClientResults();
            results.ClientInfo = new ClientResultsClientInfo[reClients.Count];
            reClients.CopyTo(results.ClientInfo);

            return XML.Serializer.SerializeAsXml(results);
        }

        private int GetMatchLength(string companyInitials)
        {
            string key = string.Format(MatchLength4Company, companyInitials);
            object o = ConfigurationManager.AppSettings[key];
            if (o != null)
            {
                return Convert.ToInt32(o);
            }
            else
                throw new ApplicationException("Invalid Match Length.");
        }
        private int GetDaysBlockHeld(string companyInitials)
        {
            string key = string.Format(DaysBlockHeld4Company, companyInitials);
            object o = ConfigurationManager.AppSettings[key];
            if (o != null)
            {
                return Convert.ToInt32(o);
            }
            else
                throw new ApplicationException("Invalid Days Block Held.");
        }
        //private MatchType GetMatchType(string companyInitials)
        //{
        //    string key = string.Format(MatchType4Company, companyInitials);
        //    object o = ConfigurationManager.AppSettings[key];
        //    if (o != null)
        //    {
        //        return (MatchType)Enum.Parse(typeof(MatchType), o.ToString());
        //    }
        //    else
        //        throw new ApplicationException("Invalid Match Type.");
        //}
        
        private string MatchLength4Company = "{0}MatchLength";
        private string DaysBlockHeld4Company = "{0}DaysBlockHeld";
        private string MatchType4Company = "{0}MatchType";

        public static List<long> GetAllClientIdsForAgency(string companyNumber, string agencyNumber)
        {
            List<long> clientIds = null;
           
            using (BCS.DAL.BCSContext bc = BCS.DAL.DataAccess.NewBCSContext())
            {
                clientIds = bc.Submission.Where(s => s.Agency.AgencyNumber == agencyNumber && s.CompanyNumber.CompanyNumber1 == companyNumber).Select(s => s.ClientCoreClientId).Distinct().ToList();
            }

            return clientIds;
        }

        public BO.Response SubmitNew(string AgencyLocationNumber, string clientId, string CustomerName, string CustomerDBA, string CustomerLegalEntity,
          BCS.Core.Clearance.AgencyPortStructures.AddressType CustomerAddress, string CustomerPhoneNumber, string TaxId,
          string CompanyNumber, CallType CallType, MatchType matchType, int WorkItemId, string submissionXML)
        {
            return SubmitNew(AgencyLocationNumber, clientId, CustomerName, CustomerDBA, CustomerLegalEntity,
            CustomerAddress, CustomerPhoneNumber, TaxId,
            CompanyNumber, CallType, matchType, WorkItemId, submissionXML, false, false, string.Empty);
        }
        public BO.Response SubmitNew(string AgencyLocationNumber, string clientId, string CustomerName, string CustomerDBA, string CustomerLegalEntity,
           BCS.Core.Clearance.AgencyPortStructures.AddressType CustomerAddress, string CustomerPhoneNumber, string TaxId,
           string CompanyNumber, CallType CallType, MatchType matchType, int WorkItemId, string submissionXML,bool CheckMultipleSubmmissions,string policyType)
        {
            return SubmitNew(AgencyLocationNumber, clientId, CustomerName, CustomerDBA, CustomerLegalEntity,
            CustomerAddress, CustomerPhoneNumber, TaxId,
            CompanyNumber, CallType, matchType, WorkItemId, submissionXML, false, CheckMultipleSubmmissions, policyType);
        }
        /// <param name="AgencyLocationNumber">AgencyNumber in clearance</param>
        /// <param name="clientId">Client Id, other search criteria are ignored when its passed</param>
        /// <param name="CustomerName">Name</param>
        /// <param name="CustomerDBA">Clearance side name type?</param>
        /// <param name="CustomerLegalEntity">?</param>
        /// <param name="CustomerAddress"></param>
        /// <param name="CustomerPhoneNumber">Phone Number</param>
        /// <param name="TaxId">optional</param>
        /// <param name="WorkItemId">AgencyPort work item Id. Not yet used</param>
        /// <param name="CallType"></param>
        /// <param name="CompanyInitials"></param>
        /// <param name="submissionXML"></param>
        public BO.Response SubmitNew(string AgencyLocationNumber, string clientId, string CustomerName, string CustomerDBA, string CustomerLegalEntity,
            BCS.Core.Clearance.AgencyPortStructures.AddressType CustomerAddress, string CustomerPhoneNumber, string TaxId,
            string CompanyNumber, CallType CallType, MatchType matchType, int WorkItemId, string submissionXML, bool isSearchScreen, bool checkMultipleSubmissions, string policyType)
        {
            string failureMessage = string.Empty;

            // check if anything was passed for client at all
            if (string.IsNullOrEmpty(CustomerDBA) && string.IsNullOrEmpty(CustomerLegalEntity) && string.IsNullOrEmpty(CustomerName)
                && string.IsNullOrEmpty(TaxId) && string.IsNullOrEmpty(clientId))
            {
                failureMessage = "No client information provided.";
                return BuildResponse(ClearanceStatus.NotCleared, failureMessage, null);
            }

            //string CompanyInitials = GetCompanyInitials(CompanyNumber);
            Biz.Agency agency = GetAgency(CompanyNumber, AgencyLocationNumber);
            if (agency == null)
            {
                failureMessage = string.Format("Agency Port : Submitting agency is not found in clearance for agency number : '{0}'", AgencyLocationNumber);
                logs.LogCentral.Current.LogError(failureMessage);
                return BuildResponse(ClearanceStatus.NotCleared, failureMessage, null);
            }
            //int daysBlockHeld = GetDaysBlockHeld(CompanyInitials);

            DateTime baseDate = DateTime.Today;
            //int matchLength = GetMatchLength(CompanyInitials);
            int matchLength = agency.CompanyNumber.Company.AgencyPortMatchLength.Value;
            if (matchLength > 0)
            {
                if (Common.IsNotNullOrEmpty(CustomerDBA))
                {
                    if (CustomerDBA.Length > matchLength)
                    {
                        CustomerDBA = CustomerDBA.Remove(matchLength);
                    }
                }
                if (Common.IsNotNullOrEmpty(CustomerLegalEntity))
                {
                    if (CustomerLegalEntity.Length > matchLength)
                    {
                        CustomerLegalEntity = CustomerLegalEntity.Remove(matchLength);
                    }
                }
                if (Common.IsNotNullOrEmpty(CustomerName))
                {
                    if (CustomerName.Length > matchLength)
                    {
                        CustomerName = CustomerName.Remove(matchLength);
                    }
                }
            }

            if (CustomerAddress == null)
                CustomerAddress = new BCS.Core.Clearance.AgencyPortStructures.AddressType();
            // get clients
            structures.Search.Vector vector = new BTS.ClientCore.Wrapper.Structures.Search.Vector();
            // TODO: required input for client search
            string clientsXML = string.Empty;
            if (Common.IsNotNullOrEmpty(clientId))
            {
                vector.Clients = new BTS.ClientCore.Wrapper.Structures.Search.Client[1];
                vector.Clients[0] = new BTS.ClientCore.Wrapper.Structures.Search.Client();
                vector.Clients[0].Info = new BTS.ClientCore.Wrapper.Structures.Search.ClientInfo();
                vector.Clients[0].Info.ClientId = clientId;
            }
            else
            {
                if (Common.IsNotNullOrEmpty(TaxId))
                {
                    clientsXML = Clients.GetClientByTaxId(CompanyNumber, TaxId);
                    vector = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));

                    // enhancement request, to search by remaining criteria, if not found by FEIN. Earlier it was if FEIN is passed, ignore others
                    // just make taxid empty and call this method again
                    if ((Common.IsNotNullOrEmpty(vector.Error) || Common.IsNotNullOrEmpty(vector.Message)) && (vector.Clients == null || vector.Clients.Length == 0))
                    {
                        string ignoredTaxId = string.Empty;
                        return SubmitNew(AgencyLocationNumber, clientId, CustomerName, CustomerDBA, CustomerLegalEntity, CustomerAddress,
                            CustomerPhoneNumber, ignoredTaxId, CompanyNumber, CallType, matchType, WorkItemId, submissionXML, checkMultipleSubmissions, policyType);
                    }
                }
                else
                {
                    Dictionary<string, structures.Search.Client> dictAllClients = new Dictionary<string, BTS.ClientCore.Wrapper.Structures.Search.Client>();
                    structures.Search.Vector v;

                    //MatchType matchType = GetMatchType(CompanyInitials);
                    switch (matchType)
                    {
                        case MatchType.AddressOrName:
                            {
                                #region separately search on each name type and address
                                // search on customer name, no type
                                if (Common.IsNotNullOrEmpty(CustomerName))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, string.Empty, string.Empty,
                                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, CustomerName);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }

                                // search on customer name, dba type
                                if (Common.IsNotNullOrEmpty(CustomerDBA))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, string.Empty, string.Empty,
                                            string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, CustomerDBA);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }

                                // search on customer name, legal type                    
                                if (Common.IsNotNullOrEmpty(CustomerLegalEntity))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, string.Empty, string.Empty,
                                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, CustomerLegalEntity);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }

                                // search on address
                                if (IsNotNullOrEmpty(CustomerAddress))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, CustomerAddress.HouseNumber, CustomerAddress.ADDR1,
                                            CustomerAddress.Item, CustomerAddress.State, CustomerAddress.Zip, string.Empty, string.Empty, string.Empty);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }
                                #endregion
                                break;
                            }
                        case MatchType.AddressAndName:
                            {
                                #region search combining (each name type + address)
                                // search on customer name, no type and address
                                if (Common.IsNotNullOrEmpty(CustomerName))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, CustomerAddress.HouseNumber, CustomerAddress.ADDR1,
                                            CustomerAddress.Item, CustomerAddress.State, CustomerAddress.Zip, string.Empty, string.Empty, CustomerName);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }

                                // search on customer name, dba type and address
                                if (Common.IsNotNullOrEmpty(CustomerDBA))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, CustomerAddress.HouseNumber, CustomerAddress.ADDR1,
                                            CustomerAddress.Item, CustomerAddress.State, CustomerAddress.Zip, string.Empty, string.Empty, CustomerDBA);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }

                                // search on customer name, legal type and address
                                if (Common.IsNotNullOrEmpty(CustomerLegalEntity))
                                {
                                    clientsXML = Clients.GetClient(CompanyNumber, string.Empty, CustomerAddress.HouseNumber, CustomerAddress.ADDR1,
                                            CustomerAddress.Item, CustomerAddress.State, CustomerAddress.Zip, string.Empty, string.Empty, CustomerLegalEntity);

                                    v = (structures.Search.Vector)XML.Deserializer.Deserialize(clientsXML, typeof(structures.Search.Vector));
                                    vector.Error += v.Error;
                                    vector.Message += v.Message;
                                    if (v.Clients != null)
                                    {
                                        foreach (structures.Search.Client aClient in v.Clients)
                                        {
                                            try
                                            {
                                                dictAllClients.Add(aClient.Info.ClientId, aClient);
                                            }
                                            catch (ArgumentException)
                                            { }
                                        }
                                    }
                                }
                                #endregion
                                break;
                            }
                        default:
                            throw new ApplicationException("Invalid Match type");
                    }



                    vector.Clients = new BTS.ClientCore.Wrapper.Structures.Search.Client[dictAllClients.Count];
                    dictAllClients.Values.CopyTo(vector.Clients, 0);
                }
            }
            if ((Common.IsNotNullOrEmpty(vector.Error) || Common.IsNotNullOrEmpty(vector.Message)) && (vector.Clients == null || vector.Clients.Length == 0))
            {
                failureMessage = string.Format("Client does not exist.");
                // TODO: Add a client, what if only partial client info is passed, like taxid and no names and addresses
                return BuildResponse(ClearanceStatus.NotCleared, failureMessage, null);
            }
            // check for multiple matches, TODO: analyze what constitutes a match
            if (vector.Clients.Length > 1 && !checkMultipleSubmissions)
            {
                failureMessage = "Multiple clients were found.";
                return BuildResponse(ClearanceStatus.NotCleared, failureMessage, null);
            }

            // proceed for submissions
            List<Triplet> searchFilters = new List<Triplet>();
            Triplet criteria = null;
            if (!checkMultipleSubmissions)
                criteria = new Triplet(JoinPath.Submission.Columns.ClientCoreClientId, vector.Clients[0].Info.ClientId);
            else
            {
                string[] clientIds = vector.Clients.Select(c => c.Info.ClientId).ToArray();
                criteria = new Triplet(JoinPath.Submission.Columns.ClientCoreClientId, clientIds, OrmLib.MatchType.In);
            }

            searchFilters.Add(criteria);

            if (!string.IsNullOrWhiteSpace(policyType))
            {
                searchFilters.Add(new Triplet(JoinPath.Submission.SubmissionCompanyNumberCode.CompanyNumberCode.Columns.Code, policyType, OrmLib.MatchType.Exact));
                searchFilters.Add(new Triplet(JoinPath.Submission.SubmissionCompanyNumberCode.CompanyNumberCode.CompanyNumberCodeType.Columns.LineOfBusinessSpecific, true));
            }

            Biz.SubmissionCollection submissions = AgencyPortSubmissions.SearchSubmissions(false, CompanyNumber, searchFilters.ToArray());
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberSubmissionStatus.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
            Biz.CompanyNumberSubmissionStatusCollection companyStatuses = dm.GetCompanyNumberSubmissionStatusCollection();
            
            // check to see if any submission matches
            foreach (Biz.Submission var in submissions)
            {
                bool blockFound = false;

                if (var.AgencyId.Value == agency.Id)
                    continue;
                if ((!var.Agency.MasterAgencyId.IsNull) && (!agency.MasterAgencyId.IsNull))
                {
                    if (!var.Agency.MasterAgencyId.Equals(agency.MasterAgencyId))
                    {
                        blockFound = true;
                    }
                }

                if (!var.SubmissionStatusId.IsNull)
                {
                    Biz.CompanyNumberSubmissionStatus varSubmissionStatus = companyStatuses.FindBySubmissionStatusId(var.SubmissionStatusId.Value);
                    if (varSubmissionStatus.IsClearable)
                    {
                        int daysBlockHeld = varSubmissionStatus.DaysStatusIsActive.IsNull ? 0 : varSubmissionStatus.DaysStatusIsActive.Value;
                        if (var.SubmissionDt.Value.AddDays(daysBlockHeld) > baseDate)
                        {
                            blockFound = true;
                        }
                    }
                    else // if not clearable
                    {
                        blockFound = true;
                    }
                }

                if (blockFound)
                {
                    if (CallType != CallType.Override)
                    {
                        failureMessage = string.Format("An existing submission is found by agency, name {0}, number {1}.", var.Agency.AgencyName, var.Agency.AgencyNumber);
                        return BuildResponse(ClearanceStatus.NotCleared, failureMessage, null);
                    }
                    else
                    {
                        // a block is found, no need to continue.
                        break;
                    }
                }
            }
            switch (CallType)
            {
                case CallType.Query:
                    {
                        // by this time, nothing should be found that would indicate a not cleared status, so send a cleared status
                        return BuildResponse(ClearanceStatus.Cleared, string.Empty, null);
                    }
                case CallType.Override:
                case CallType.Submission:
                    {
                        string separator = "|";
                        // get client info
                        string aclientstr = Clients.GetClientById(CompanyNumber, vector.Clients[0].Info.ClientId, true, isSearchScreen);
                        BO.ClearanceClient foundClient = (BO.ClearanceClient)XML.Deserializer.Deserialize(aclientstr, typeof(BO.ClearanceClient), "seq_nbr");

                        // build submission add parameters
                        BO.Submission aSubmission = (BO.Submission)XML.Deserializer.Deserialize(submissionXML, typeof(BO.Submission));

                        #region address ids
                        List<string> lstAddresses = new List<string>();
                        foreach (structures.Info.Address var in foundClient.ClientCoreClient.Addresses)
                        {
                            lstAddresses.Add(var.AddressId);
                        }
                        string[] arrAddresses = new string[lstAddresses.Count];
                        lstAddresses.CopyTo(arrAddresses);
                        #endregion

                        #region name ids
                        List<string> lstNames = new List<string>();
                        foreach (structures.Info.Name var in foundClient.ClientCoreClient.Client.Names)
                        {
                            lstNames.Add(var.SequenceNumber);
                        }
                        string[] arrNames = new string[lstNames.Count];
                        lstNames.CopyTo(arrNames);
                        #endregion

                        #region Agent Unspecified Reasons
                        string agentunspecifiedreasons = string.Empty;
                        dm = Common.GetDataManager();
                        if (aSubmission.AgentUnspecifiedReasonList != null && aSubmission.AgentUnspecifiedReasonList.Length > 0)
                        {
                            dm.QueryCriteria.And(JoinPath.AgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReason.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            AgentUnspecifiedReasonCollection allAgentReasons = dm.GetAgentUnspecifiedReasonCollection();

                            List<string> ids = new List<string>();
                            for (int i = 0; i < aSubmission.AgentUnspecifiedReasonList.Length; i++)
                            {
                                AgentUnspecifiedReason reason = allAgentReasons.FindByReason(aSubmission.AgentUnspecifiedReasonList[i].Reason);
                                if (reason != null)
                                {
                                    ids.Add(reason.Id.ToString());
                                }
                            }
                            string[] arrAgentReasons = new string[ids.Count];
                            ids.CopyTo(arrAgentReasons);
                            agentunspecifiedreasons = string.Join(separator, arrAgentReasons);
                        }
                        #endregion

                        #region Code types
                        string codeids = string.Empty;

                        if (aSubmission.CompanyNumberCodeIds != null && aSubmission.CompanyNumberCodeIds.Length > 0)
                            codeids = aSubmission.CompanyNumberCodeIds;
                        else
                        {
                            if (aSubmission.SubmissionCodeTypess != null && aSubmission.SubmissionCodeTypess.Length > 0)
                            {
                                #region Get all code types for this company number
                                dm = Common.GetDataManager();
                                dm.QueryCriteria.And(JoinPath.CompanyNumberCodeType.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                                CompanyNumberCodeTypeCollection allTypes = dm.GetCompanyNumberCodeTypeCollection();
                                #endregion

                                #region Get all codes for this company number
                                dm.QueryCriteria.Clear();
                                dm.QueryCriteria.And(JoinPath.CompanyNumberCode.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                                CompanyNumberCodeCollection allCodes = dm.GetCompanyNumberCodeCollection();
                                #endregion

                                List<string> listcodeids = new List<string>();
                                for (int i = 0; i < aSubmission.SubmissionCodeTypess.Length; i++)
                                {
                                    CompanyNumberCodeType cncType = allTypes.FindByCodeName(aSubmission.SubmissionCodeTypess[i].CodeName);
                                    if (cncType != null)
                                    {
                                        CompanyNumberCode cnCode = allCodes.FindByCode(aSubmission.SubmissionCodeTypess[i].CodeValue);
                                        if (cnCode != null)
                                        {
                                            listcodeids.Add(cnCode.Id.ToString());
                                        }
                                    }
                                }

                                string[] scodeids = new string[listcodeids.Count];
                                listcodeids.CopyTo(scodeids);
                                codeids = string.Join(separator, scodeids);
                            }
                        }
                        #endregion

                        #region Line Of Businesses
                        string lobss = string.Empty;
                        string lobid = string.Empty;
                        if (aSubmission.LineOfBusinessList != null && aSubmission.LineOfBusinessList.Length > 0)
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            LineOfBusinessCollection allLobs = dm.GetLineOfBusinessCollection();

                            List<string> ids = new List<string>();
                            for (int i = 0; i < aSubmission.LineOfBusinessList.Length; i++)
                            {
                                LineOfBusiness lob = allLobs.FindByCode(aSubmission.LineOfBusinessList[i].Code);
                                if (lob != null)
                                {
                                    ids.Add(lob.Id.ToString());
                                }
                            }
                            string[] arr = new string[ids.Count];
                            ids.CopyTo(arr);
                            lobss = string.Join(separator, arr);
                            if (arr.Length > 0)
                                lobid = arr[0];
                        }
                        #endregion

                        #region TODO: Line Of Business Children
                        string lobchildren = string.Empty;

                        #endregion

                        #region Attributes
                        string attributes = string.Empty;
                        if (aSubmission.AttributeList != null && aSubmission.AttributeList.Length > 0)
                        {
                            string[] sattributes = new string[aSubmission.AttributeList.Length];
                            for (int i = 0; i < aSubmission.AttributeList.Length; i++)
                            {
                                //sattributes[i] = aSubmission.AttributeList[i].CompanyNumberAttributeId.ToString();
                                sattributes[i] = string.Format("{0}={1}", aSubmission.AttributeList[i].CompanyNumberAttributeId, aSubmission.AttributeList[i].AttributeValue);
                            }
                            attributes = string.Join("|", sattributes);
                        }
                        #endregion

                        #region Underwriter
                        if (Common.IsNotNullOrEmpty(aSubmission.UnderwriterName))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            dm.QueryCriteria.And(JoinPath.Person.Columns.FullName, aSubmission.UnderwriterName);
                            Person underwriter = dm.GetPerson();
                            if (underwriter != null)
                            {
                                dm = Common.GetDataManager();
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, underwriter.Id);
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agency.Id);
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.LineOfBusinessId, lobid);
                                //dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, );

                                AgencyLineOfBusinessPersonUnderwritingRolePersonCollection realUnderwriters = dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection();
                                realUnderwriters = realUnderwriters.FilterByPersonId(underwriter.Id);

                                if (realUnderwriters.Count == 1)
                                    aSubmission.UnderwriterId = realUnderwriters[0].PersonId;
                            }
                        }
                        #endregion

                        #region Analyst
                        if (Common.IsNotNullOrEmpty(aSubmission.AnalystName))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            dm.QueryCriteria.And(JoinPath.Person.Columns.FullName, aSubmission.AnalystName);
                            Person analyst = dm.GetPerson();
                            if (analyst != null)
                            {
                                dm = Common.GetDataManager();
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, analyst.Id);
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agency.Id);
                                dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.LineOfBusinessId, lobid);
                                //dm.QueryCriteria.And(JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, );

                                AgencyLineOfBusinessPersonUnderwritingRolePersonCollection realAnalysts = dm.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection();
                                realAnalysts = realAnalysts.FilterByPersonId(analyst.Id);
                                if (realAnalysts.Count == 1)
                                    aSubmission.AnalystId = realAnalysts[0].PersonId;
                            }
                        }
                        #endregion

                        #region Submission Status
                        if (Common.IsNotNullOrEmpty(aSubmission.SubmissionStatusCode))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.SubmissionStatus.CompanyNumberSubmissionStatus.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            dm.QueryCriteria.And(JoinPath.SubmissionStatus.Columns.StatusCode, aSubmission.SubmissionStatusCode);
                            SubmissionStatus status = dm.GetSubmissionStatus();
                            if (status != null)
                            {
                                aSubmission.SubmissionStatusId = status.Id;

                                #region Submission Status Reason
                                if (Common.IsNotNullOrEmpty(aSubmission.SubmissionStatusReason))
                                {
                                    dm = Common.GetDataManager();
                                    dm.QueryCriteria.And(JoinPath.SubmissionStatusReason.SubmissionStatusSubmissionStatusReason.Columns.SubmissionStatusId, status.Id);
                                    dm.QueryCriteria.And(JoinPath.SubmissionStatusReason.Columns.ReasonCode, aSubmission.SubmissionStatusReason);
                                    SubmissionStatusReason statusReason = dm.GetSubmissionStatusReason();
                                    if (statusReason != null)
                                        aSubmission.SubmissionStatusReasonId = statusReason.Id;
                                }
                                #endregion
                            }
                        }
                        #endregion

                        #region Submission Type
                        if (Common.IsNotNullOrEmpty(aSubmission.SubmissionTypeName))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.SubmissionType.CompanyNumberSubmissionType.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            dm.QueryCriteria.And(JoinPath.SubmissionType.Columns.TypeName, aSubmission.SubmissionTypeName);
                            SubmissionType type = dm.GetSubmissionType();
                            if (type != null)
                                aSubmission.SubmissionTypeId = type.Id;
                        }
                        #endregion

                        #region Agent
                        if (aSubmission.Agent != null && Common.IsNotNullOrEmpty(aSubmission.Agent.Surname))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.Agent.Columns.AgencyId, agency.Id);
                            dm.QueryCriteria.And(JoinPath.Agent.Columns.Surname, aSubmission.Agent.Surname);
                            dm.QueryCriteria.And(JoinPath.Agent.Columns.FirstName, aSubmission.Agent.FirstName);
                            Agent agent = dm.GetAgent();
                            if (agent != null)
                                aSubmission.AgentId = agent.Id;
                        }
                        #endregion

                        #region Contact
                        if (aSubmission.Contact != null && Common.IsNotNullOrEmpty(aSubmission.Contact.Name))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.Contact.Columns.AgencyId, agency.Id);
                            dm.QueryCriteria.And(JoinPath.Contact.Columns.Name, aSubmission.Contact.Name);
                            Contact contact = dm.GetContact();
                            if (contact != null)
                                aSubmission.ContactId = contact.Id;
                        }
                        #endregion

                        #region Other Carrier
                        if (aSubmission.OtherCarrier != null && Common.IsNotNullOrEmpty(aSubmission.OtherCarrier.Code))
                        {
                            dm = Common.GetDataManager();
                            dm.QueryCriteria.And(JoinPath.OtherCarrier.CompanyNumberOtherCarrier.CompanyNumber.Columns.PropertyCompanyNumber, CompanyNumber);
                            dm.QueryCriteria.And(JoinPath.OtherCarrier.Columns.Code, aSubmission.OtherCarrier.Code);
                            OtherCarrier otherCarrier = dm.GetOtherCarrier();
                            if (otherCarrier != null)
                                aSubmission.OtherCarrierId = otherCarrier.Id;
                        }
                        #endregion

                        // TODO: other submission parameters may need to be added when agency port decides.
                        string sXML = Submissions.AddSubmission(foundClient.ClientCoreClient.Client.ClientId, string.Join(separator, arrAddresses),
                            string.Join(separator, arrNames), foundClient.ClientCoreClient.Client.PortfolioId, aSubmission.SubmissionBy,
                            aSubmission.SubmissionDt.ToString(),
                            aSubmission.SubmissionTypeId, agency.Id, aSubmission.AgentId, agentunspecifiedreasons,
                            aSubmission.ContactId, CompanyNumber, aSubmission.SubmissionStatusId, aSubmission.SubmissionStatusDate.ToString(),
                            aSubmission.SubmissionStatusReasonId, aSubmission.SubmissionStatusReasonOther, aSubmission.PolicyNumber,
                            aSubmission.EstimatedWrittenPremium, codeids, attributes,
                            aSubmission.EffectiveDate.ToString(), aSubmission.ExpirationDate.ToString(),
                            aSubmission.UnderwriterId, aSubmission.AnalystId,
                            0, // TODO: sending none for technician
                            aSubmission.PolicySystemId, aSubmission.Comments,
                            lobss, lobchildren, aSubmission.InsuredName, aSubmission.Dba, aSubmission.SubmissionNumber.ToString(),
                            aSubmission.OtherCarrierId, aSubmission.OtherCarrierPremium, null, null);

                        BO.Submission coresub = (BCS.Core.Clearance.BizObjects.Submission)BCS.Core.XML.Deserializer.Deserialize(
                            sXML, typeof(BCS.Core.Clearance.BizObjects.Submission));
                        if (coresub != null)
                        {
                            if (coresub.Status == "Success")
                            {
                                return BuildResponse(ClearanceStatus.Cleared, coresub.Message, coresub.SubmissionNumber);
                            }
                            else
                            {

                                return BuildResponse(ClearanceStatus.NotCleared, coresub.Message, null);
                            }
                        }
                        else
                        {
                            return BuildResponse(ClearanceStatus.NotCleared, "Unknown error.", null);
                        }
                    }
                default:
                    {
                        return BuildResponse(ClearanceStatus.NotCleared, "Unknown Call Type.", null);
                    }
            }
        }

    }



}
