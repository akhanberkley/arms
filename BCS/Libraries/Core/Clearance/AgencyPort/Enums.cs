using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.Core.Clearance
{
    /// <summary>
    /// 
    /// </summary>
    public enum CallType
    {
        /// <summary>
        /// Indicates just a query whether a submission is cleared
        /// </summary>
        Query = 1,
        /// <summary>
        /// Indicates add a submission if cleared
        /// </summary>
        Submission,
        /// <summary>
        /// Adds a submission regardless of cleared or not
        /// </summary>
        Override
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ClearanceStatus
    {
        /// <summary>
        /// 
        /// </summary>
        [StringValue("Cleared")]
        Cleared,

        /// <summary>
        /// 
        /// </summary>
        [StringValue("Not Cleared")]
        NotCleared,
    }
    /// <summary>
    /// 
    /// </summary>
    public enum MatchType
    {
        /// <summary>
        /// 
        /// </summary>
        AddressOrName = 0,

        /// <summary>
        /// 
        /// </summary>
        AddressAndName
    }

}
