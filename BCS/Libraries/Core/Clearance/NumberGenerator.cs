using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.Core.Clearance
{
    /// <summary>
    /// 
    /// </summary>
    public class NumberGenerator
    {
        private static object aLock = new object();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyNumber"></param>
        /// <param name="typeName"></param>
        /// <param name="incrementBy"></param>
        /// <returns></returns>
        public static string GetNextNumberControlValueByCompanyNumberAndTypeName(string companyNumber, string typeName, int incrementBy)
        {
            lock (aLock)
            {
                Biz.DataManager dm = Common.GetDataManager();
                dm.QueryCriteria.And(Biz.JoinPath.NumberControl.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                dm.QueryCriteria.And(Biz.JoinPath.NumberControl.NumberControlType.Columns.TypeName, typeName);
                Biz.NumberControlCollection scoll = dm.GetNumberControlCollection(Biz.FetchPath.NumberControl.NumberControlType);

                if (scoll == null || scoll.Count == 0)
                {
                    //throw new ArgumentException(string.Format("Control Number is not setup for the company number \"{0}\" and type name \"{1}\".",
                    //    companyNumber, typeName));
                    return "0";
                }
                if (scoll.Count > 1)
                {
                    throw new ArgumentException(string.Format("More than one Control Number exists for the company number \"{0}\" and type name \"{1}\".",
                                       companyNumber, typeName));
                }

                int defaultIncrement = 1;

                if (incrementBy > 0)
                {
                    defaultIncrement = incrementBy;
                }

                long newValue = scoll[0].ControlNumber.Value + defaultIncrement;

                scoll[0].ControlNumber = newValue;

                dm.CommitAll();

                string retValue = newValue.ToString();

                if (!string.IsNullOrEmpty(scoll[0].PaddingChar))
                {
                    string PaddingChar = scoll[0].PaddingChar;
                    int TotalLength = scoll[0].TotalLength.Value;
                    bool PadHead = scoll[0].PadHead.Value;

                    if (newValue.ToString().Length != TotalLength)
                    {
                        // false pads at end
                        if (PadHead)
                        {
                            for (int i = newValue.ToString().Length; i < TotalLength; i++)
                            {
                                retValue = PaddingChar + retValue;
                            }
                        }
                        else
                        {
                            for (int i = newValue.ToString().Length; i < TotalLength; i++)
                            {
                                retValue += PaddingChar;
                            }
                        }
                    }
                }

                return retValue.ToString();
            }
        }

        public static void IncrementControlValueIfHigher(string companyNumber, string typeName,long newControl)
        {
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.NumberControl.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(Biz.JoinPath.NumberControl.NumberControlType.Columns.TypeName, typeName);
            Biz.NumberControlCollection scoll = dm.GetNumberControlCollection(Biz.FetchPath.NumberControl.NumberControlType);

            if (scoll.Count == 1 && scoll[0].ControlNumber.Value < newControl)
            {
                scoll[0].ControlNumber = newControl;
                dm.CommitAll();
            }
        }
    }
}
