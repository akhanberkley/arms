using System;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Text;

namespace BCS.Core.XML
{
    /// <summary>
    /// Summary description for Deserializer.
    /// </summary>
    public class Deserializer
    {
        /// <summary>
        /// empty constructor
        /// </summary>
        public Deserializer()
        { }

        /// <summary>
        /// Helper method to convert an xml string to a object of type specified. Removes all the attributes
        /// before serialization. Use Deserialize(string xml, Type t, params string[] exceptions) if attributes
        /// need to be deserialized. those attributes specified in the exceptions will not be removed.
        /// </summary>
        public static object Deserialize(string xml, Type t)
        {
            //TODO :: this call is required because clientcore xml contains some namespaces (*java*)
            // investigate for XmlSerializer support to ignore namespaces or attributes
            xml = BCS.Core.Clearance.Common.RemoveTypeAttributes(xml);

            XmlSerializer x = new XmlSerializer(t);

            MemoryStream stream = new MemoryStream(StringToUTF8ByteArray(xml));

            xml = UTF8ByteArrayToString(StringToUTF8ByteArray(xml));

            try
            {
                return x.Deserialize(stream);
            }
            catch (Exception ex)
            {
                throw new Exception("Caught exception trying to deserialize " + xml, ex);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="t"></param>
        /// <param name="exceptions"></param>
        /// <returns></returns>
        public static object Deserialize(string xml, Type t, params string[] exceptions)
        {
            //TODO :: this call is required because clientcore xml contains some namespaces (*java*)
            // investigate for XmlSerializer support to ignore namespaces or attributes
            xml = BCS.Core.Clearance.Common.RemoveTypeAttributes(xml,exceptions);

            XmlSerializer x = new XmlSerializer(t);

            MemoryStream stream = new MemoryStream(StringToUTF8ByteArray(xml));

            //xml = UTF8ByteArrayToString(StringToUTF8ByteArray(xml));

            try
            {
                return x.Deserialize(stream);
            }
            catch (Exception ex)
            {
                throw new Exception("Caught exception trying to deserialize " + xml, ex);
            }
        }

        private static Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }


        private static String UTF8ByteArrayToString(Byte[] characters)
        {

            UTF8Encoding encoding = new UTF8Encoding();
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }
 


    }
}
