using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace BCS.Core.XML
{
	/// <summary>
	/// Summary description for ObjectXml.
	/// </summary>
	public class ObjectXml
	{
        /// <summary>
        /// BuildDynamicObjectXml
        /// </summary>
        /// <param name="objectToSerialize"></param>
        /// <param name="overrides"></param>
        /// <param name="attributes"></param>
        /// <returns></returns>
        /// <remarks>ms-help://MS.VSCC.2003/MS.MSDNQTR.2003FEB.1033/cpref/html/frlrfsystemreflectionemitpropertybuilderclasstopic.htm</remarks>
		public static System.Type BuildDynamicObjectXml(object objectToSerialize, XmlAttributeOverrides overrides, params string[] attributes)
		{
			string[] objectToSerializeNameArray = objectToSerialize.GetType().Name.Split('.');
			string objectToSerializeName = objectToSerializeNameArray[objectToSerializeNameArray.Length-1];

			AppDomain appDomain = Thread.GetDomain();
			AssemblyName asmName = new AssemblyName();
			asmName.Name = "DynamicObjectXml";

			AssemblyBuilder asmBuilder = appDomain.DefineDynamicAssembly(asmName, AssemblyBuilderAccess.Run);
			ModuleBuilder modBuilder = asmBuilder.DefineDynamicModule("ModuleObjectXml");
			TypeBuilder typeBuilder = modBuilder.DefineType(objectToSerializeName+"Xml", TypeAttributes.Public);

			foreach (PropertyInfo pi in objectToSerialize.GetType().GetProperties())
			{
				// We will create generic public fields:
				FieldBuilder fieldBuilder = typeBuilder.DefineField(pi.Name,
					pi.PropertyType,
					FieldAttributes.Public);
			}

			return typeBuilder.CreateType();
		}
	}
}
