using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace BCS.Core.XML
{
	/// <summary>
	/// Summary description for CustomXmlSerializer.
	/// </summary>
	public class CustomXmlSerializer : XmlSerializer
	{
        /// <summary>
        /// CustomXmlSerializer
        /// </summary>
        /// <param name="objectToSerialize"></param>
        /// <param name="overrides"></param>
		public CustomXmlSerializer(object objectToSerialize, XmlAttributeOverrides overrides) : 
			base(objectToSerialize.GetType(), overrides)
		{

		}
	}
}
