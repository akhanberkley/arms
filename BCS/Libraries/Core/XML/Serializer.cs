using System;
using System.Text;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace BCS.Core.XML
{
	/// <summary>
	/// Summary description for Serializer.
	/// </summary>
	public class Serializer
	{
        /// <summary>
        /// Private ctor
        /// </summary>
        private Serializer() { }

        /// <summary>
        /// Serialize object as XML
        /// </summary>
        /// <param name="toSerialize"></param>
        /// <returns></returns>
        public static string SerializeAsXml(object toSerialize)
        {
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            XmlSerializer x = new XmlSerializer(toSerialize.GetType());
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;            
            //settings.NewLineChars = string.Empty;
            settings.Encoding = Encoding.UTF8; 
           
            StringBuilder sb = new StringBuilder();
            XmlWriter w = XmlWriter.Create(sb,settings);
                                 
            try
            {
                x.Serialize(w, toSerialize, namespaces);
            }
            catch (Exception ex)
            {
                throw new Exception("Caught exception trying to serialize", ex);
            }

            return sb.ToString();
            
            
        }

        /// <summary>
        /// Convert Stream to String
        /// </summary>
        /// <param name="theStream"></param>
        /// <returns></returns>
        private static string ConvertStreamToString(System.IO.Stream theStream)
        {
            string theString = "";
            theStream.Position = 0;

            using (System.IO.StreamReader sr = new System.IO.StreamReader(theStream))
            {
                try
                {                    
                    theString = sr.ReadToEnd();
                }
                catch (Exception ex)
                {
                    throw new Exception("Caught exception reading StreamReader", ex);
                }

                // Close and clean up the StreamReader
                sr.Close();
            }

            return theString;
        }

		#region Not Used (but kept because it is good code)
//		/// <summary>
//		/// Serialization Format
//		/// </summary>
//		public enum SerializationFormat
//		{
//			/// <summary>
//			/// Binary
//			/// </summary>
//			Binary,
//			/// <summary>
//			/// SOAP
//			/// </summary>
//			SOAP,
//			/// <summary>
//			/// Xml
//			/// </summary>
//			Xml
//		}
//
//		#region Serialize/Deserialize to disk
//		/// <summary>
//		/// Serialize to Disk
//		/// </summary>
//		/// <param name="path"></param>
//		/// <param name="request"></param>
//		/// <param name="format"></param>
//		/// <returns></returns>
//		public static System.Boolean SerializeToDisk(string path, object request, SerializationFormat format) 
//		{
//			try 
//			{
//				System.IO.MemoryStream oStream = Serialize(request, format);
//				
//				if(System.IO.File.Exists(path)) System.IO.File.Delete(path);
//				
//				System.IO.FileStream output = System.IO.File.Open(path, System.IO.FileMode.CreateNew);
//				output.Write(oStream.ToArray(),0,Convert.ToInt32(oStream.Length));
//				output.Close();
//				oStream.Close();
//
//				return true;
//			} 
//			catch 
//			{
//				return false;
//			}
//		}
//
//		/// <summary>
//		/// DeSerialize from Disk
//		/// </summary>
//		/// <param name="path"></param>
//		/// <param name="format"></param>
//		/// <returns></returns>
//		public static object DeSerializeFromDisk(string path, SerializationFormat format) 
//		{
//			try 
//			{
//				if(System.IO.File.Exists(path)) 
//				{
//					System.IO.FileStream input = System.IO.File.Open(path, System.IO.FileMode.Open);
//					byte[] filecontents=new byte[Convert.ToInt32(input.Length)];
//					input.Read(filecontents, 0, Convert.ToInt32(input.Length));
//					input.Close();
//					//System.IO.File.Delete(path);
//					System.IO.MemoryStream inStream = new System.IO.MemoryStream(filecontents);
//					object newObj = DeSerialize(inStream, format);
//					inStream.Close();
//					
//					return newObj;
//				} 
//				else 
//				{
//					return null;
//				}
//			} 
//			catch
//			{
//				return null;
//			}
//		}
//		#endregion
//
//		#region wrappers for binary and xml serialization
//		/// <summary>
//		/// Serialize
//		/// </summary>
//		/// <param name="request"></param>
//		/// <param name="format"></param>
//		/// <returns></returns>
//		public static System.IO.MemoryStream Serialize(object request, SerializationFormat format) 
//		{
//			return Serialize(request, format, null);
//		}
//
//		/// <summary>
//		/// Serialize
//		/// </summary>
//		/// <param name="request"></param>
//		/// <param name="format"></param>
//		/// <param name="attributeList"></param>
//		/// <returns></returns>
//		public static System.IO.MemoryStream Serialize(object request, SerializationFormat format, params string[] attributeList)
//		{
//			switch (format)
//			{
//				case SerializationFormat.Binary:
//					return SerializeBinary(request);
//				case SerializationFormat.SOAP:
//					return SerializeSOAP(request);
//				case SerializationFormat.Xml:
//					return SerializeXml(request, attributeList);
//				default:
//					return null;
//			}
//		}
//
//		/// <summary>
//		/// Deserialize
//		/// </summary>
//		/// <param name="memStream"></param>
//		/// <param name="format"></param>
//		/// <returns></returns>
//		public static object DeSerialize(System.IO.MemoryStream memStream, SerializationFormat format) 
//		{
//			switch (format)
//			{
//				case SerializationFormat.Binary:
//					return DeSerializeBinary(memStream);
//				case SerializationFormat.SOAP:
//					return DeSerializeSOAP(memStream);
//				case SerializationFormat.Xml:
//					return null;
//				default:
//					return null;
//			}
//		}
//		#endregion
//
//		#region Binary Serializers
//		/// <summary>
//		/// Serialize Binary
//		/// </summary>
//		/// <param name="request"></param>
//		/// <returns></returns>
//		public static System.IO.MemoryStream SerializeBinary(object request) 
//		{
//			System.Runtime.Serialization.Formatters.Binary.BinaryFormatter serializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
//			System.IO.MemoryStream memStream = new System.IO.MemoryStream();
//			
//			serializer.Serialize(memStream, request);
//			
//			return memStream;
//		}
//
//		/// <summary>
//		/// DeSerialize Binary
//		/// </summary>
//		/// <param name="memStream"></param>
//		/// <returns></returns>
//		public static object DeSerializeBinary(System.IO.MemoryStream memStream) 
//		{
//			memStream.Position=0;
//			System.Runtime.Serialization.Formatters.Binary.BinaryFormatter deserializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
//			
//			object newobj = deserializer.Deserialize(memStream);
//			memStream.Close();
//			
//			return newobj;
//		}
//		#endregion
//
//		#region XML Serializers
//		/// <summary>
//		/// Serialize object (properties) as XML
//		/// </summary>
//		/// <param name="entity"></param>
//		/// <param name="attributeList">Array of properties to be attributes instead of elements</param>
//		/// <returns></returns>
//		/// <remarks>Useful if you object cannot Serialize on it's own. Uses reflection to build
//		/// the Xml elements. System.IO.StreamReader streamReader = new System.IO.StreamReader(stream, System.Text.Encoding.Default);
//		/// return streamReader; // Here's your XML text</remarks>
//		public static System.IO.MemoryStream SerializeXml(object entity, params string[] attributeList)
//		{
//			System.IO.MemoryStream stream = new System.IO.MemoryStream();
//			XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.Default);
//
//			string[] fullObjectTypeName = entity.GetType().ToString().Split('.');
//			writer.WriteStartElement(fullObjectTypeName[fullObjectTypeName.Length-1]);
//
//			foreach (PropertyInfo pi in entity.GetType().GetProperties())
//			{
//				bool isAttribute = false;
//
//				// Does this property become an attribute?
//				if (attributeList != null)
//				{
//					for (int i = 0; i < attributeList.Length; i++)
//					{
//						if (attributeList[i] == pi.Name)
//						{
//							writer.WriteAttributeString(pi.Name, null, pi.GetValue(entity, null).ToString());
//							isAttribute = true;
//							break;
//						}
//					}
//				}
//				
//				if (!isAttribute)
//				{
//					if (pi.Name != "Item" && pi.PropertyType.ToString().IndexOf("Collection") < 0)
//						writer.WriteElementString(pi.Name, pi.GetValue(entity, null).ToString());
//				}
//			}
//
//			writer.WriteEndElement();
//
//			// Write the XML to file and close the writer
//			writer.Flush();
//			
//			// Reset the stream position
//			stream.Position = 0;
//
//			// Return the stream
//			return stream;
//		}
//		#endregion
//
//		#region SOAP Serializers
//		/// <summary>
//		/// Serialize SOAP
//		/// </summary>
//		/// <param name="request"></param>
//		/// <returns></returns>
//		public static System.IO.MemoryStream SerializeSOAP(object request) 
//		{
//			System.Runtime.Serialization.Formatters.Soap.SoapFormatter serializer = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
//
//			System.IO.MemoryStream memStream = new System.IO.MemoryStream();
//			serializer.Serialize(memStream, request);
//			
//			return memStream;
//		}
//
//		/// <summary>
//		/// DeSerialize SOAP
//		/// </summary>
//		/// <param name="memStream"></param>
//		/// <returns></returns>
//		public static object DeSerializeSOAP(System.IO.MemoryStream memStream) 
//		{
//			object sr;
//			System.Runtime.Serialization.Formatters.Soap.SoapFormatter deserializer = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
//			memStream.Position=0;
//			sr = deserializer.Deserialize(memStream);
//			memStream.Close();
//			
//			return sr;
//		}
//		#endregion
		#endregion
	}
}
