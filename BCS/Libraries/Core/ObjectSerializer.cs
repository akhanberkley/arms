using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace BCS.Core
{
    /// <summary>
    /// Summary description for Serializer.
    /// </summary>
    public sealed class ObjectSerializer
    {
        /// <summary>
        /// Private ctor
        /// </summary>
        private ObjectSerializer() { }

        /// <summary>
        /// Serialize object as XML
        /// </summary>
        /// <param name="toSerialize"></param>
        /// <returns></returns>
        public static string SerializeAsXml(object toSerialize)
        {
            XmlSerializer x = new XmlSerializer(toSerialize.GetType());
            MemoryStream stream = new MemoryStream();

            // Calling application should handle the exception here:
            x.Serialize(stream, toSerialize);

            return ConvertStreamToString(stream);
        }

        /// <summary>
        /// Convert Stream to String
        /// </summary>
        /// <param name="theStream"></param>
        /// <returns></returns>
        private static string ConvertStreamToString(System.IO.Stream theStream)
        {
            string theString = "";
            theStream.Position = 0;

            using (System.IO.StreamReader sr = new System.IO.StreamReader(theStream))
            {
                // Calling application should handle the exception here:
                theString = sr.ReadToEnd();

                // Close and clean up the StreamReader
                sr.Close();
            }

            return theString;
        }
    }
}
