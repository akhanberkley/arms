using System;
using System.Data;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Configuration;

namespace BCS.Core.Utility
{
	/// <summary>
	/// Summary description for XmlHelper.
	/// </summary>
	public sealed class XmlHelper
	{
		#region private utility methods & constructors

		//Since this class provides only static methods, make the default constructor private to prevent 
		//instances from being created with "new XmlHelper()".
		private XmlHelper() {}

		#endregion

		#region public methods

		#region transform XSL
		/// <summary>
		/// Makes an xslt transformation of a DataSet content.
		/// </summary>
		/// <param name="ds">DataSet content to transform.</param>
		/// <param name="transformFile">Xslt file.</param>
		/// <returns>A string that represents the transformation result.</returns>
		static public string XslTransformToString(DataSet ds, string transformFile) 
		{
			return XslTransformToString(ds,transformFile,Encoding.UTF8);
		}

		/// <summary>
		/// Makes an xslt transformation of a DataSet content with an encoding type.
		/// </summary>
		/// <param name="ds">DataSet content to transform.</param>
		/// <param name="transformFile">Xslt file.</param>
		/// <param name="encodingType">Encoding type (ASCII, UTF8, Unicode).</param>
		/// <returns>A string that represents the transformation result.</returns>
		static public string XslTransformToString(DataSet ds, string transformFile, System.Text.Encoding encodingType) 
		{
			XmlDataDocument ddoc = new XmlDataDocument(ds);			
			return XslTransformToString(ddoc,transformFile,encodingType);
		}

		/// <summary>
		/// Makes an xslt transformation of a XmlDataDocument.
		/// </summary>
		/// <param name="ddoc">XmlDataDocument to transform.</param>
		/// <param name="transformFile">Xslt file.</param>
		/// <returns>A string that represents the transformation result.</returns>
		static public string XslTransformToString(XmlDataDocument ddoc, string transformFile) 
		{	
			return XslTransformToString(ddoc,transformFile,Encoding.UTF8);
		}

		/// <summary>
		/// Makes an xslt transformation of Xml string.
		/// </summary>
		/// <param name="xmlString">xml string.</param>
		/// <param name="transformFile">Xslt file.</param>
		/// <returns>A string that represents the transformation result.</returns>
		static public string XslTransformToString(string xmlString, string transformFile) 
		{
			XmlDataDocument ddoc = new XmlDataDocument();
			ddoc.LoadXml(xmlString);
			return XslTransformToString(ddoc,transformFile,Encoding.UTF8);
		}

		/// <summary>
		/// Makes an xslt transformation of a XmlDataDocument with a specific encoding.
		/// </summary>
		/// <param name="ddoc">XmlDataDocument to transform.</param>
		/// <param name="transformFile">Xslt file.</param>
		/// <param name="encodingType">Encoding type (ASCII, UTF8, Unicode).</param>
		/// <returns>A string that represents the transformation result.</returns>
		static public string XslTransformToString(XmlDataDocument ddoc, string transformFile, System.Text.Encoding encodingType) 
		{
			StringWriter sw = new StringWriter();		
			XmlTextWriter xtw = new XmlTextWriter(sw);
			//XslTransform tx = new XslTransform();
            XslCompiledTransform ctx = new XslCompiledTransform();

			//tx.Load(transformFile);
            ctx.Load(transformFile);

			try 
			{
				//see article:
				//http://www.c-sharpcorner.com/Code/2002/May/XslTPerformace.asp
				//tx.Transform(ddoc.CreateNavigator(), null, xtw, null);
                ctx.Transform(ddoc.CreateNavigator(), xtw);
		
				return sw.ToString();
			} 
			finally 
			{
				xtw.Close();
			}			
			
			//Another example...
			//http://www.fawcette.com/xmlmag/2002_03/online/online_eprods/xml_dwahlin03_08/default_pf.asp#
			/*
			StringWriter sw = new StringWriter();
			tx.Transform(ddoc.CreateNavigator(),null,sw);
			string ret = sw.ToString(System.Globalization.CultureInfo.InvariantCulture);
			sw.Close();
			return ret;
			*/
		}

		#endregion

		#region Validate XML

		/// <summary>
		/// Validates an XML document against an XSD schema.
		/// </summary>
		/// <param name="xmlReader">XmlReader object to validate.</param>
		/// <param name="schemaFile">XSD schema file.</param>
		/// <returns>An XmlDocument that represents the xml loaded and validated.</returns>
		static public XmlDocument ValidateXmlDocument(XmlReader xmlReader, string schemaFile)
		{
			XmlTextReader xr = null;
			if(schemaFile.Length > 0){xr = new XmlTextReader(schemaFile);}
			return ValidateXmlDocument(xmlReader,xr);
		}

		/// <summary>
		/// Validates an XML document against an XSD schema.
		/// </summary>
		/// <param name="xmlReader">XmlReader object to validate.</param>
		/// <param name="schemaStream">XSD schema stream.</param>
		/// <returns>An XmlDocument that represents the xml loaded and validated.</returns>
		static public XmlDocument ValidateXmlDocument(XmlReader xmlReader, Stream schemaStream)
		{
			return ValidateXmlDocument(xmlReader,new XmlTextReader(schemaStream));
		}

		/// <summary>
		/// Validates an XML document against an XSD schema.
		/// </summary>
		/// <param name="xmlReader">XmlReader object to validate.</param>
		/// <param name="schema">XSD schema XmlReader.</param>
		/// <returns>An XmlDocument that represents the xml loaded and validated.</returns>
		static public XmlDocument ValidateXmlDocument(XmlReader xmlReader, XmlReader schema)
		{
			// reference the XML document through a Schema validating reader
			XmlDocument configXmlDoc = new XmlDocument();

            XmlReader configReader = null;
			try 
			{
                XmlReaderSettings settings = new XmlReaderSettings();                			
				if (schema != null && !schema.EOF) 
				{
                    settings.Schemas.Add(null, schema);
                    settings.ValidationType = ValidationType.Schema;
				}
                configReader = XmlReader.Create(xmlReader, settings);

				// load and validate the document
				configXmlDoc.PreserveWhitespace = false;				
				configXmlDoc.Load(configReader);
			}
			catch(Exception e) 
			{
				throw e;
			} 
			finally
			{
				if(configReader != null)
				{
					configReader.Close();
					configReader = null;
				}
			}
			return configXmlDoc; 
		}

		/// <summary>
		/// Validates an XML document against an XSD schema.
		/// </summary>
		/// <param name="xmlReader">XmlReader object to validate.</param>
		/// <returns>An XmlDocument that represents the xml loaded and validated.</returns>
		static public XmlDocument ValidateXmlDocument(XmlReader xmlReader)
		{
			return ValidateXmlDocument(xmlReader,"");
		}

		/// <summary>
		/// Validates an XML document against an XSD schema.
		/// </summary>
		/// <param name="xmlDocumentFile">Xml file name to validate.</param>
		/// <param name="schemaFile">XSD schema file name.</param>
		/// <returns>An XmlDocument that represents the xml loaded and validated.</returns>
		static public XmlDocument ValidateXmlDocument(string xmlDocumentFile, string schemaFile)
		{
			return ValidateXmlDocument(new XmlTextReader(xmlDocumentFile),schemaFile);
		}

		/// <summary>
		/// Validates an XML document against an XSD schema.
		/// </summary>
		/// <param name="xmlDocumentFile">Xml file name to validate.</param>
		/// <returns>An XmlDocument that represents the xml loaded and validated.</returns>
		static public XmlDocument ValidateXmlDocument(string xmlDocumentFile) 
		{
			return ValidateXmlDocument(new XmlTextReader(xmlDocumentFile),"");
		}

		#endregion

		#region GetDataSetFromXmlFragment
		/// <summary>
		/// Loads a DataSet from a given Xml and creates its schema.
		/// </summary>
		/// <param name="oXml">Xml Element.</param>
		/// <returns>The DataSet created from the Xml passed.</returns>
		public static DataSet GetDataSetFromXmlFragment(XmlElement oXml)
		{
			DataSet ds = new DataSet();
			XmlTextReader oReader = new XmlTextReader(oXml.OuterXml,
				XmlNodeType.Element,
				new XmlParserContext(null, null, null, XmlSpace.None));
			// now lets create a schema off of the instance data
			ds.ReadXml(oReader, XmlReadMode.Auto);
			return ds;
		}
		#endregion

		#region AttributeValues

		/// <summary>
		/// Gets the attribute value from an Xml node.
		/// </summary>
		/// <param name="xNode">Node to extract the specific attribute value.</param>
		/// <param name="attr">Attribute name.</param>
		/// <param name="def">default attribute value.</param>
		/// <returns>Attribute value.</returns>
		public static string AttrValue(XmlNode xNode, string attr, string def) 
		{
			string res = def;
			try
			{
				// AGV
				// FIX: try not to generate an exception as it's very expensive.
				// First fixed in version: 2.6.0.1 - 26, Oct 2004.
				if( xNode.Attributes != null && xNode.Attributes[attr] != null )
					res = xNode.Attributes[attr].InnerText;
			} 
			catch{}
			if (res == null || res.Length == 0) res = def;

			return res;
		}

		/// <summary>
		/// Gets the attribute value from an Xml node.
		/// </summary>
		/// <param name="node">Node to extract the specific attribute value.</param>
		/// <param name="xpath">Xpath of the attribute inside the node.</param>
		/// <param name="attr">Attribute name.</param>
		/// <param name="def">default attribute value.</param>
		/// <returns>Attribute value.</returns>
		public static string AttrValue(XmlNode node, string xpath, string attr, string def) 
		{
			XmlNode xNode;
			XmlAttribute xAttr;

			if( xpath.IndexOf( "//" )!=-1 )
			{
				xpath = String.Concat("//" , node.LocalName , xpath.Remove( 0,1));
			}
			if (node == null) return def;

			xNode = node.SelectSingleNode(xpath);
			if (xNode == null) return def;

			xAttr = (XmlAttribute)xNode.Attributes.GetNamedItem(attr);
			if (xAttr == null || xAttr.InnerText == null || xAttr.InnerText.Length == 0) return def;

			return xAttr.InnerText;
		}

		/// <summary>
		/// Returns a collection of name/value from the an XmlNode object.
		/// </summary>
		/// <param name="xnode">Node to get the collection of attributes.</param>
		/// <returns>Attributes collection.</returns>
		public static Hashtable AttrsValues(XmlNode xnode)
		{
			return AttrsValues(xnode,"","");
		}

		/// <summary>
		/// Returns a collection of name/value from the an XmlNode object.
		/// </summary>
		/// <param name="xnode">Node to get the collection of attributes.</param>
		/// <param name="xpath">Optional xpath.</param>
		/// <returns>Attributes collection.</returns>
		public static Hashtable AttrsValues(XmlNode xnode, string xpath)
		{
			return AttrsValues(xnode,xpath,"");
		}

		/// <summary>
		/// Returns a collection of name/value from the an XmlNode object.
		/// </summary>
		/// <param name="xnode">Node to get the collection of attributes.</param>
		/// <param name="xpath">Optional xpath.</param>
		/// <param name="exclude">Attribute to exclude from the collection.</param>
		/// <returns>Attributes collection.</returns>
		public static Hashtable AttrsValues(XmlNode xnode, string xpath, string exclude)
		{
			Hashtable ret;
			XmlNode node = null;

			if( xpath.IndexOf( "//" )!=-1 )
			{
				xpath = String.Concat("//" , xnode.LocalName , xpath.Remove( 0,1));
			}
			if(xpath.Length > 0)
			{
				node = xnode.SelectSingleNode(xpath);
			} 
			else 
			{
				node = xnode.Clone();
			}

			if(node == null)
			{
				ret = new Hashtable();
			}
			else 
			{
				SingleTagSectionHandler st = new SingleTagSectionHandler();			
				//ret = new Hashtable((Hashtable)st.Create(null,null,node), CaseInsensitiveHashCodeProvider.DefaultInvariant, CaseInsensitiveComparer.DefaultInvariant);

                ret = new Hashtable((Hashtable)st.Create(null, null, node) ,  StringComparer.InvariantCulture);
				if(exclude.Length > 0) ret.Remove(exclude);
			}
			return ret;
		}

		/// <summary>
		/// Provides name-value pair configuration information from a configuration section.
		/// </summary>
		/// <param name="xnode">The XmlNode that contains the configuration information to be handled. Provides direct access to the XML contents of the configuration section.</param>
		/// <param name="xpath"></param>
		/// <returns>A NameValueCollection.</returns>
		public static NameValueCollection NodeValues(XmlNode xnode, string xpath)
		{
			NameValueCollection ret = new NameValueCollection();

			if( xpath.IndexOf( "//" )!=-1 )
			{
				xpath = String.Concat("//" , xnode.LocalName , xpath.Remove( 0,1));
			}
			XmlNode node = xnode.SelectSingleNode(xpath);
			if(node != null)
			{
				for(int i = 0; i < node.ChildNodes.Count; i++)
				{
					ret.Add(i.ToString(System.Globalization.CultureInfo.InvariantCulture),node.ChildNodes.Item(i).InnerText);
				}
			}
			return ret;
		}

		/// <summary>
		/// Provides name-value pair configuration information from a configuration section.
		/// </summary>
		/// <param name="xnode">The XmlNode that contains the configuration information to be handled. Provides direct access to the XML contents of the configuration section.</param>
		/// <param name="xpath"></param>
		/// <returns>A NameValueCollection.</returns>
		public static IDictionary NodeAttrs(XmlNode xnode, string xpath)
		{
			return NodeAttrs(xnode, xpath, false);
		}

		/// <summary>
		/// Provides name-value pair configuration information from a configuration section.
		/// </summary>
		/// <param name="xnode">The XmlNode that contains the configuration information to be handled. Provides direct access to the XML contents of the configuration section.</param>
		/// <param name="xpath"></param>
		/// <param name="useFirstAttrAsKey">Use the first attribute of each node as the item key value.</param>
		/// <returns>A NameValueCollection.</returns>
		public static IDictionary NodeAttrs(XmlNode xnode, string xpath, bool useFirstAttrAsKey)
		{
			HybridDictionary ret = new HybridDictionary(true);

			if( xpath.IndexOf( "//" )!=-1 )
			{
				xpath = String.Concat("//" , xnode.LocalName , xpath.Remove( 0,1));
			}
			
			XmlNode node = xnode.SelectSingleNode(xpath);
			
			if(node != null)
			{
				int index = 0;
				for(int i = 0; i < node.ChildNodes.Count; i++)
				{
					XmlNode item = node.ChildNodes.Item(i);
					if(item.NodeType == XmlNodeType.Element && item.Attributes.Count > 0)
					{
						NameValueCollection attrs = new NameValueCollection(item.Attributes.Count);
						foreach(XmlAttribute attr in item.Attributes)
						{
							attrs.Add(attr.Name,attr.Value);
						}
						
						string key;
						if(useFirstAttrAsKey)
							key = item.Attributes[0].Value;
						else
							key = String.Concat(item.Name, "_", index.ToString(System.Globalization.CultureInfo.InvariantCulture));

						ret.Add(key, attrs);
						index++;
					}
				}
			}
			
			return ret;
		}

		#endregion
		
		/// <summary>
		/// This function return a new stand alone XmlReader 
		/// from an XmlReader object bound to an ADO.NET connection.
		/// </summary>
		/// <author>RFI 09/02/2004 12:50</author>
		/// <param name="xr">Original conected XmlReader</param>
		/// <param name="closeReader">True to close original XmlReader after read it</param>
		/// <returns>New disconected XmlReader</returns>
		public static XmlReader CreateDisconectedXmlReader(XmlReader xr, bool closeReader)
		{
			//initialize variables
			MemoryStream stream = new MemoryStream();
			XmlWriter xw = new XmlTextWriter( stream, new UTF8Encoding(false) );

			//Read the original XmlReader
			xw.WriteNode(xr, false);

			//Write xml data from the original XmlReader to string
			xw.Flush();

			if (closeReader)
				xr.Close();

			//return new XmlReader from string that contain the original xml data
			stream.Position = 0;
			XmlReader result = new XmlTextReader( stream, XmlNodeType.Element, null );
			
			stream = null;
			xr = null;
			xw = null;

			return result;
		}

		#endregion
	}
}
