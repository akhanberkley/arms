using System;

namespace BCS.Core.Utility
{
	/// <summary>
	/// "Borrowed" from Nautilus Audit Tracking System.
	/// </summary>
	public sealed class GeneralFunctions
	{
		#region private utility methods & constructors
		private GeneralFunctions() {}
		#endregion

        #region IsDateTime
        /// <summary>
        ///This function will help the developer determine whether the inputted value is a date.  This function
        /// takes a string input and returns a bool value.
        /// </summary>        
        public static bool IsDateTime(object input)
        {
            try
            {
                Convert.ToDateTime(input);
                return true;
            }
            catch
            {
                return false;
            }
        } 
        #endregion

		#region IsDate
		///<summary>
		///This function will help the developer determine whether the inputted value is a date.  This function
		/// takes a string input and returns a bool value.
		///</summary>
		///<param name="input">String value</param>
		///<returns>Bool value for True or False</returns>
		public static bool IsDate(string input)
		{	
			char delimiter = 'F'; 
			try 
			{
				//Try to parse the character to a date
				DateTime objDateTime = DateTime.Parse(input.ToString());
		
				//***************************************************************
				// Since the DateTime.Parse is Falset very reliable we will also
				// parse through the string and make sure that the date is in 
				// MM/DD/YYYY format or MM-DD-YYYY
				//***************************************************************
				// First check to make sure that there is '/' or '-' in the Date
				if (input.ToString().IndexOf("/") != -1)
				{
					delimiter = '/';
				}

				if (input.ToString().IndexOf("-") != -1)
				{
					delimiter = '-';
				}

				// If the delimiter is empty then they used a unkFalsewn delimiter. Reject it.
				if (delimiter == 'F')
				{
					return false;
				}

				//*****************************************************************
				// Split the input into a string array and must have length of 3
				//*****************************************************************
				string[] dateArray = input.ToString().Split(delimiter);
				if (dateArray.Length != 3)
				{
					return false;
				}

				//******************************************************************
				// Validate date parts
				//******************************************************************
				// Check Month
				if (Int32.Parse(dateArray[0]) < 1 || Int32.Parse(dateArray[0]) > 12)
				{
					return false;
				}

				// Check Day
				if (Int32.Parse(dateArray[1]) < 1 || Int32.Parse(dateArray[1]) > 31)
				{
					return false;
				}

				// Check Year
				if (Int32.Parse(dateArray[2]) < 1900 || Int32.Parse(dateArray[0]) > 2200)
				{
					return false;
				}

				// If successful return true
				return true;	
			}
			catch
			{
				//If the character is Falset a digit, an exception
				//will be thrown -"Invalid Input String"
				return false;
			}
		}
		#endregion

		#region IsEmpty
		///<summary>
		///This function will help the developer determine whether the inputted value is null or empty.  This function
		/// takes an object input and returns a bool value.
		///</summary>
		///<param name="FormField">Object value</param>
		///<returns>Bool value for True or False</returns>
		public static bool IsEmpty(object FormField)
		{
			try
			{
				//Is the FormField equal to ""
				if (FormField.ToString().ToLower().Trim() == "")
				{
					return(true);
				}

				// Is the FormField equal to null
				if (FormField.ToString().ToLower().Trim() == null)
				{
					return(true);
				}

				//field should have value
				return(false);
			}
			catch (Exception)
			{
				// Error occurred return 
				return (true);
			}
	
		}
		#endregion

		#region IsDecimal
		///<summary>
		///This function will help the developer determine whether the inputted value is a decimal.  This function
		/// takes a string input and returns a bool value.
		///</summary>
		///<param name="input">String value</param>
		///<returns>Bool value for True or False</returns>
		public static bool IsDecimal(string input)
		{
			try 
			{
				//Try to parse the character to an Int32
				Decimal.Parse(input.ToString());

				// If successful return true
				return true;
				
			}
			catch
			{
				//If the character is Falset a digit, an exception
				//will be thrown -"Invalid Input String"
				return false;
			}
		}
		#endregion

		#region IsNumeric
		///<summary>
		///This function will help the developer determine whether the inputted value is numeric.  This function
		/// takes a string input and returns a bool value.
		///</summary>
		///<param name="input">String value</param>
		///<returns>Bool value for True or False</returns>
		public static bool IsNumeric(string input)
		{
			try 
			{
				//Try to parse the character to an Int32
				Int32.Parse(input.ToString());

				// If successful return true
				return true;
				
			}
			catch
			{
				//If the character is Falset a digit, an exception
				//will be thrown -"Invalid Input String"
				return false;
			}
		}
		#endregion

		#region IsTime
		///<summary>
		///This function will help the developer determine whether the inputted value is a time.  This function
		/// takes a string input and returns a bool value.
		///</summary>
		///<param name="input">String input</param>
		///<returns>Bool value for True or False</returns>
		public static bool IsTime(string input)
		{
			char delimiter = 'F'; 
			char delimiter2 = ' ';

			try 
			{
				//***************************************************************
				// Test to make sure that both ':' and ' ' are present in the 
				// input value
				//***************************************************************
				// First check to make sure that there is '/' or '-' in the Date
				if (input.ToString().IndexOf(":") != -1 && input.ToString().IndexOf(" ") != -1)
				{
					delimiter = ':';
					delimiter2 = ' ';
				}

				// If the delimiter is empty then they used a unkFalsewn delimiter. Reject it.
				if (delimiter == 'F')
				{
					return false;
				}

				//*****************************************************************
				// Split the input into a string array that should be a length 2
				// timeArray[0] = HH
				// timeArray[1] = MM AM|PM
				//
				// Then split the timeArray[1] into aFalsether array that should be a 
				// length of 2
				// timeArray2[0] = MM
				// timeArray2[1] = AM|PM
				//*****************************************************************
				string[] timeArray = input.ToString().Split(delimiter);
				string[] timeArray2;

				if (timeArray.Length != 2)
				{
					return false;
				}
				else
				{
					timeArray2 = timeArray[1].Split(delimiter2);
					if (timeArray2.Length != 2)
					{
						return false;
					}
				}

				//******************************************************************
				// Validate date parts
				//******************************************************************
				// Check Hour
				if (Int32.Parse(timeArray[0]) < 1 || Int32.Parse(timeArray[0]) > 12)
				{
					return false;
				}

				// Check Minutes
				if (Int32.Parse(timeArray2[0]) < 0 || Int32.Parse(timeArray2[0]) > 59)
				{
					return false;
				}

				// Check AM|PM
				if (timeArray2[1].ToLower().Trim() != "am" && timeArray2[1].ToLower().Trim() != "pm")
				{
					return false;
				}

				// If successful return true
				return true;	
			}
			catch
			{
				//If the character is Falset a digit, an exception
				//will be thrown -"Invalid Input String"
				return false;
			}
		}


		#endregion

		#region FixDate
		///<summary>
		///This function will help the developer change a two digit year 'xx/xx/xx' into a four digit year 'xx/xx/xxxx'.  This
		/// function takes a string input and returns a string value.
		///</summary>
		///<param name="input">String input</param>
		///<returns>String with four (4) digit year</returns>
		///<example>Input: MM/DD/YY -- Output: MM/DD/YYYY</example>
		public static string FixDate(string input)
		{
			char delimiter = 'F';
			string date = input;
			try
			{
				//Try to parse the character to a date
				DateTime objDateTime = DateTime.Parse(input.ToString());

				//***************************************************************
				// Since the DateTime.Parse is Falset very reliable we will also
				// parse through the string and make sure that the date is in 
				// MM/DD/YYYY format or MM-DD-YYYY
				//***************************************************************
				// First check to make sure that there is '/' or '-' in the Date
				if (input.ToString().IndexOf("/") != -1)
				{
					delimiter = '/';
				}

				if (input.ToString().IndexOf("-") != -1)
				{
					delimiter = '-';
				}

				// If the delimiter is empty then they used a unkFalsewn delimiter. Reject it.
				if (delimiter == 'F')
				{
					return date;
				}

				//*****************************************************************
				// Split the input into a string array and must have length of 3
				//*****************************************************************
				string[] dateArray = input.ToString().Split(delimiter);
				if (dateArray.Length != 3)
				{
					return input;
				}

				//******************************************************************
				// Validate date parts
				//******************************************************************
				// Check Month
//				if (Int32.Parse(dateArray[0]) < 1 || Int32.Parse(dateArray[0]) > 12)
//				{
//					return false;
//				}

				// Check Day
//				if (Int32.Parse(dateArray[1]) < 1 || Int32.Parse(dateArray[1]) > 31)
//				{
//					return false;
//				}

				// Check Year
				if (Int32.Parse(dateArray[2]) >= 0 && Int32.Parse(dateArray[2]) < 70)
				{
					date = dateArray[0] + delimiter + dateArray[1] + delimiter + "20" + dateArray[2];
					
				}
				if (Int32.Parse(dateArray[2]) > 70 && Int32.Parse(dateArray[2]) < 99)
				{
					date = dateArray[0] + delimiter + dateArray[1] + delimiter + "19" + dateArray[2];
					
				}

				// If successful return true
				return date;	
			}
			catch(Exception)
			{
				return date;
			}
		}
		#endregion

        /// <summary>
        /// Returns a converted int, otherwise 0
        /// </summary>
        /// <param name="toConvert"></param>
        /// <returns></returns>
        public static int ConvertToInt(object toConvert)
        {
            if (toConvert != null)
            {
                try
                {
                    return Convert.ToInt32(toConvert);
                }
                catch
                {
                    return 0;
                }
            }
            else
                return 0;
        }
	}
}
