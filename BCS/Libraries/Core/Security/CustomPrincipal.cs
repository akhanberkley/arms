using System;
using System.Security.Principal;

namespace BCS.Core.Security
{
	/// <summary>
	/// Summary description for CustomPrincipal.
	/// http://msdn.microsoft.com/library/en-us/dnnetsec/html/SecNetHT06.asp?frame=true
	/// </summary>
	public class CustomPrincipal : IPrincipal
	{
		private IIdentity m_identity;
		private string [] m_roles;
        private long m_agency;

		/// <summary>
		/// Default ctor
		/// </summary>
		/// <param name="identity"></param>
		/// <param name="roles"></param>
        public CustomPrincipal(IIdentity identity, string[] roles)
            : this(identity, roles, 0)
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="roles"></param>
        /// <param name="agency"></param>
        public CustomPrincipal(IIdentity identity, string[] roles, long agency)
        {
            m_identity = identity;
            if (roles != null)
            {
                m_roles = new string[roles.Length];
                roles.CopyTo(m_roles, 0);
                Array.Sort(m_roles);
            }
            m_agency = agency;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool HasAgency()
        {
            return m_agency >= 0 && IsInRole("Remote Agency") &&
                !IsInAnyRoles("System Administrator", "Agency Read-Only", "Agency Administrator", "Assignment Administrator");
        }
        /// <summary>
        /// 
        /// </summary>
        public long Agency
        {
            get
            { return m_agency; }
        }

		/// <summary>
		/// Checks whether a principal is in all of the specified set of roles
		/// </summary>
		/// <param name="roles"></param>
		/// <returns></returns>
		public bool IsInAllRoles( params string [] roles )
        {
            if (m_roles == null)
                return false;
			foreach (string searchrole in roles )
			{
				if (Array.BinarySearch(m_roles, searchrole) < 0 )
					return false;
			}
			return true;
		}

		/// <summary>
		/// Checks whether a principal is in any of the specified set of roles
		/// </summary>
		/// <param name="roles"></param>
		/// <returns></returns>
		public bool IsInAnyRoles( params string [] roles )
		{
            if (m_roles == null)
                return false;
			foreach (string searchrole in roles )
			{
                if (Array.BinarySearch(m_roles, searchrole) >= 0)
					return true;
			}
			return false;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool IsInOnlyRole(string role)
        {
            if (m_roles == null)
                return false;
            return (m_roles.Length == 1 && m_roles[0] == role);
        }


		#region IPrincipal Members
		/// <summary>
		/// Current identity instance
		/// </summary>
		public IIdentity Identity
		{
			get
			{
				return m_identity;
			}
		}

		/// <summary>
		/// Current roles
		/// </summary>
		public string[] Roles
		{
			get
			{
				return m_roles;
			}
		}

		/// <summary>
		/// Is in role?
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
		public bool IsInRole(string role)
		{
            if (m_roles == null)
                return false;
			return Array.BinarySearch( m_roles, role ) >=0 ? true : false;
		}

		#endregion
	}
}
