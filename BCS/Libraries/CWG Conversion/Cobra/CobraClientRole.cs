namespace BCS.CWGConversion.Cobra.Structures
{
    using System;
    using System.Data;
    using System.Configuration;

    public class CobraClientRole
    {
        #region fields
        private int _clientRoleId;
        private DateTime _effective;
        private DateTime _expiration;
        private ActiveFlag _active;
        private int _clientLabelId;
        private string _account;
        private string _systemId;
        private int _clientId;
        private int _typeMasterId;


        private CobraClientLabel _cobraClientLabel;
        #endregion

        /// <summary>
        /// constructor
        /// </summary>
        public CobraClientRole() { }

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public int ClientRoleId
        {
            get { return this._clientRoleId; }
            set { this._clientRoleId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Effective
        {
            get { return this._effective; }
            set { this._effective = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Expiration
        {
            get { return this._expiration; }
            set { this._expiration = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ActiveFlag Active
        {
            get { return this._active; }
            set { this._active = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ClientLabelId
        {
            get { return this._clientLabelId; }
            set { this._clientLabelId = value; }
        }

        /// <summary>
        /// currently used for maintaining name sequence numbers
        /// </summary>
        public string Account
        {
            get
            {
                if (this._account != null)
                    return this._account;
                else
                    return "";
            }
            set { this._account = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string SystemId
        {
            get
            {
                if (this._systemId != null)
                    return this._systemId;
                else
                    return "";
            }
            set { this._systemId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ClientId
        {
            get { return this._clientId; }
            set { this._clientId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TypeMasterId
        {
            get { return this._typeMasterId; }
            set { this._typeMasterId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public CobraClientLabel CobraClientLabel
        {
            get { return this._cobraClientLabel; }
            set { this._cobraClientLabel = value; }
        }
        #endregion
    }
}