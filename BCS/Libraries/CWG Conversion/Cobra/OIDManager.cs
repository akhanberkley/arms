using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Data.OleDb;
using System.Data;
using DB = BCS.CWGConversion.Database;

namespace BCS.CWGConversion.Cobra
{
    public enum OIDType
    {
        CLBL, // for label
        CLNT, // for master
        CROLE // for role
    }
    public class OIDManager
    {
        private static OleDbCommand GetOleDbCommand(string commandText, string connectionString, CommandType commandType)
        {
            OleDbCommand command = new OleDbCommand(commandText);

            command.Connection = DB.Database.GetOpenOleDbConnection(connectionString);
            command.CommandTimeout = 60;
            command.CommandType = commandType;

            return command;
        }

        internal static void FillOidSet(OIDType oIDType, int count, ref StringCollection MasterOidCollection)
        {
            int nextOid = 0;
            int maxReservedOid = 0;
            string connectionString = Properties.Settings.Default.CobraDSN4OleDb;
            OleDbCommand command = GetOleDbCommand("get_next_set_oids", connectionString, CommandType.StoredProcedure);

            #region Parameters
            OleDbParameter[] arParams = new OleDbParameter[2];

            arParams[0] = new OleDbParameter("Xc_oid_cl_abr", OleDbType.Char);
            arParams[0].Value = oIDType.ToString();
            command.Parameters.Add(arParams[0]);

            arParams[1] = new OleDbParameter("n_max_oid", OleDbType.Decimal);
            arParams[1].Value = count;
            command.Parameters.Add(arParams[1]);
            #endregion

            DataSet nextOidDs = new DataSet();

            OleDbDataAdapter da = new OleDbDataAdapter(command);

            try
            {
                // column indexes
                int nextOidColIndex = 0;
                int maxOidColIndex = 1;
                int errorColIndex = 2;

                da.Fill(nextOidDs);

                if (nextOidDs.Tables[0] != null)
                {
                    if (nextOidDs.Tables[0].Rows.Count > 0)
                    {
                        // Check for sqlcode errror
                        // 0 indicates success
                        if (Convert.ToInt32(nextOidDs.Tables[0].Rows[0][errorColIndex]) == 0)
                        {
                            nextOid = Convert.ToInt32(nextOidDs.Tables[0].Rows[0][nextOidColIndex]);
                            maxReservedOid = Convert.ToInt32(nextOidDs.Tables[0].Rows[0][maxOidColIndex]);
                        }
                        else
                        {
                            nextOid = -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DB.Database.CloseOpenOleDbConnection(command.Connection);
            }
        }
    }
    class LabelOIDManager
    {
        protected StringCollection LabelOidCollection = new StringCollection();

        #region Singleton Gobble-Di-Goop
        // Lock synchronization object
        private static object syncLock = new object();

        private static LabelOIDManager instance;
        protected LabelOIDManager(int count)
        {
            OIDManager.FillOidSet(OIDType.CLBL, count, ref LabelOidCollection);
        }
        public static LabelOIDManager Instance(int count)
        {
            // Support multithreaded applications through
            // 'Double checked locking' pattern which (once
            // the instance exists) avoids locking each
            // time the method is invoked 
            if (instance == null)
            {
                lock (syncLock)
                {
                    if (instance == null)
                    {
                        instance = new LabelOIDManager(count);
                    }
                }
            }

            return instance;
        }

        public string GetNextOID(OIDType oIDType)
        {
            throw new NotImplementedException("Not Implemented.");
        }
        #endregion
    }
    class RoleOIDManager
    {
        protected StringCollection RoleOidCollection = new StringCollection();

        #region Singleton Gobble-Di-Goop
        // Lock synchronization object
        private static object syncLock = new object();

        private static RoleOIDManager instance;
        protected RoleOIDManager(int count)
        {
            OIDManager.FillOidSet(OIDType.CROLE, count, ref RoleOidCollection);
        }
        public static RoleOIDManager Instance(int count)
        {
            // Support multithreaded applications through
            // 'Double checked locking' pattern which (once
            // the instance exists) avoids locking each
            // time the method is invoked 
            if (instance == null)
            {
                lock (syncLock)
                {
                    if (instance == null)
                    {
                        instance = new RoleOIDManager(count);
                    }
                }
            }

            return instance;
        }

        public string GetNextOID(OIDType oIDType)
        {
            throw new NotImplementedException("Not Implemented.");
        }
        #endregion
    }
    class MasterOIDManager
    {
        protected StringCollection MasterOidCollection = new StringCollection();

        #region Singleton Gobble-Di-Goop
        // Lock synchronization object
        private static object syncLock = new object();

        private static MasterOIDManager instance;
        protected MasterOIDManager(int count)
        {
            OIDManager.FillOidSet(OIDType.CLNT, count, ref MasterOidCollection);
        }
        public static MasterOIDManager Instance(int count)
        {
            // Support multithreaded applications through
            // 'Double checked locking' pattern which (once
            // the instance exists) avoids locking each
            // time the method is invoked 
            if (instance == null)
            {
                lock (syncLock)
                {
                    if (instance == null)
                    {
                        instance = new MasterOIDManager(count);
                    }
                }
            }

            return instance;
        }

        public string GetNextOID(OIDType oIDType)
        {
            throw new NotImplementedException("Not Implemented.");
        }
        #endregion
    }

}
