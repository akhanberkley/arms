using System;
using System.Collections.Generic;
using System.Text;
using BCS.CWGConversion.Cobra.Structures;
using BTS.LogFramework;
using CCStructures = BTS.ClientCore.Wrapper.Structures;
using System.Threading;

namespace BCS.CWGConversion.Cobra
{

    class BCSClientSystemManager
    {

        BCS.ClientSys.Biz.DataManager dataManager;
        BCS.ClientSys.Biz.Lookups lookupManager;

        public BCSClientSystemManager(BCS.ClientSys.Biz.DataManager iDataManager, BCS.ClientSys.Biz.Lookups iLookupManager)
        {
            dataManager = Common.GetClientSysDataManager();
            lookupManager = iLookupManager;
        }
        public BCSClientSystemManager()
        {
        }

        /// <summary>
        /// to avoid bcs having to call getclientbyid again we ref the things needed (addressids, nameids, portfolioid,)
        /// and dba for import edits
        /// </summary>
        /// <param name="clientImport"></param>
        /// <param name="portfolioId"></param>
        /// <param name="addressIdsList"></param>
        /// <param name="nameIdsList"></param>
        /// <returns>Response's data property will be of type int</returns>
        public Response importClientRefs(CCStructures.Import.ClientImportData clientImport, ref string portfolioId, ref List<string> addressIdsList,
            ref List<string> nameIdsList)
        {
            // edit op
            if (string.IsNullOrEmpty(clientImport.Client.CCClientId))
            {
                return importClientAddRefs(clientImport, ref portfolioId, ref addressIdsList, ref nameIdsList);
            }
            else // add op
            {
                return importClientEditRefs(clientImport, ref portfolioId, ref addressIdsList, ref nameIdsList);
            }
        }

        private Response importClientAddRefs(BTS.ClientCore.Wrapper.Structures.Import.ClientImportData clientImport, ref string portfolioId, ref List<string> addressIdsList,
            ref List<string> nameIdsList)
        {
            Response response = new Response();

            // we need at least one name, so that we can add the first one as PRIMRY/PRSPT to cobra.
            // TODO: Do we do the same for addresses too?
            if (clientImport.Client.NameInfos == null || clientImport.Client.NameInfos.Length == 0)
            {
                string helperMessage = "There must be at least one name. Please try again.";
                LogCentral.Current.LogFatal(string.Format("{0}.", helperMessage));
                response.ResponseCode = ResponseCode.Failure;
                response.Data = 0;
                response.Reason = helperMessage;
                return response;
            }

            #region Cobra Handling
            #region Build and add the client master object
            CobraClientMaster clientMaster = new CobraClientMaster();

            // We use the first name to set the client master
            if (clientImport.Client.NameInfos[0].BusinessName.Length > 0)
            {
                clientMaster.ClientType = ClientType.Organization;

                clientMaster.LastName = clientImport.Client.NameInfos[0].BusinessName;
            }
            else
            {
                clientMaster.ClientType = ClientType.Individual;

                clientMaster.FirstName = clientImport.Client.NameInfos[0].FirstName;
                clientMaster.MiddleName = clientImport.Client.NameInfos[0].MiddleName;
                clientMaster.LastName = clientImport.Client.NameInfos[0].LastName;
            }

            // bug in COBRA UI does not recognise F as Taxnumber, so hard coding it to T
            clientMaster.TaxType = "T";

            try
            {
                clientMaster.Tax = Convert.ToInt32(clientImport.Client.PersonalInfo.TaxId.Replace("-", ""));
            }
            catch
            {
                // commeinting sinice it generates too many unwanted warnings
                //LogCentral.Current.LogWarn("Unable to convert tax Id (" + clientImport.Client.PersonalInfo.TaxId + ") to numeric. Setting to 0");
                clientMaster.Tax = 0;
            }


            //int masterClientId = CobraOleDbController.AddClientMaster(clientMaster);
            clientMaster.ClientId = new CobraOleDbController().GetNextOidForMaster();
            if (clientMaster.ClientId < 1)
            {
                string helperMessage = string.Format("Unable to add client to the cobra system. The Client Id from cobra is '{0}'", clientMaster.ClientId);
                // we will not log here, instead pass to the calling method
                // LogCentral.Current.LogFatal(string.Format("{0}", helperMessage), exc);
                response.ResponseCode = ResponseCode.Failure;
                response.Data = 0;
                response.Reason = helperMessage;
                response.Exception = null;
                return response;
            }


            #endregion
            if (Properties.Settings.Default.ThreadCobra)
            {
                Thread cobraThread = new Thread(new ParameterizedThreadStart(new BCSClientSystemManager().HandleCobraAdd));
                cobraThread.Start(clientMaster);
            }
            else
            {
                HandleCobraAdd(clientMaster);
            }

            #endregion

            #region BCS Client System Handling
            DateTime startTime = DateTime.Now;

            DateTime dateNow = DateTime.Now;

            BCS.ClientSys.Biz.Client newClient = dataManager.NewClient(clientMaster.ClientId, dateNow,
                lookupManager.Companys.FindByCompanyNumber(Properties.Settings.Default.CompanyNumber));

            newClient.TaxNumber = clientImport.Client.PersonalInfo.TaxId;
            newClient.TaxTypeCode =
                lookupManager.TaxTypes.FindByTaxTypeDescription(clientImport.Client.PersonalInfo.TaxIdTypeCode) != null ?
                lookupManager.TaxTypes.FindByTaxTypeDescription(clientImport.Client.PersonalInfo.TaxIdTypeCode).Code : null;
            newClient.PortfolioId = clientMaster.ClientId;

            for (int i = 0; i < clientImport.Client.NameInfos.Length; i++)
            {
                CCStructures.Import.NameInfo var = clientImport.Client.NameInfos[i];
                BCS.ClientSys.Biz.ClientName newClientName = newClient.NewClientName();
                newClientName.BusinessName = var.BusinessName;
                newClientName.EntryDt = dateNow;
                // we are adding first name to cobra
                newClientName.InBillingSystem = i == 0;
                newClientName.OrderIndex = i + 1;
                
                // this process only loads from legacy system
                newClientName.FromLegacySystem = true;
            }
            for (int i = 0; i < clientImport.Client.AddressInfos.Length; i++)
            {
                CCStructures.Import.AddressInfo var = clientImport.Client.AddressInfos[i];
                BCS.ClientSys.Biz.ClientAddress newClientAddress = newClient.NewClientAddress();
                newClientAddress.City = var.CityName;
                newClientAddress.CountryCode = var.Country;
                newClientAddress.County = var.County;
                newClientAddress.EntryDt = dateNow;
                newClientAddress.InBillingSystem = i == 0;
                newClientAddress.OrderIndex = i + 1;
                newClientAddress.StateCode = var.StateCode;
                newClientAddress.StreetLine2 = var.ADDR2;
                newClientAddress.StreetName = var.ADDR1;
                newClientAddress.StreetNumber = var.HouseNumber;
                newClientAddress.ZipCode = var.Zip;

                // this process only loads from legacy system
                newClientAddress.FromLegacySystem = true;

                // pad the zip extension
                while (newClientAddress.ZipCode.Length < 9)
                {
                    newClientAddress.ZipCode += "0";
                }
            }

            try
            {
                // commit
                dataManager.CommitAll();
                foreach (BCS.ClientSys.Biz.ClientAddress committedClientAddress in newClient.ClientAddresss)
                {
                    addressIdsList.Add(committedClientAddress.Id.ToString());
                }
                foreach (BCS.ClientSys.Biz.ClientName committedClientName in newClient.ClientNames)
                {
                    // order index is used by bcs as client core as name identifier 
                    nameIdsList.Add(committedClientName.OrderIndex.ToString());
                }
                portfolioId = newClient.PortfolioId.ToString();
            }
            catch (Exception exc)
            {
                string helperMessage = string.Format("Unable to add client to the bcs client system. The Client Id from cobra is '{0}'", clientMaster.ClientId);
                // we will not log here, instead pass to the calling method
                // LogCentral.Current.LogFatal(string.Format("{0}", helperMessage), exc);
                response.ResponseCode = ResponseCode.Failure;
                response.Data = 0;
                response.Reason = helperMessage;
                response.Exception = exc;
                return response;
            }

            DateTime endTime = DateTime.Now;
            Common.LogTimeTaken("client sytem add", startTime, endTime);

            #endregion

            response.Data = clientMaster.ClientId;
            response.ResponseCode = ResponseCode.Success;
            return response;
        }

        // TODO: implement refs
        private Response importClientEditRefs(BTS.ClientCore.Wrapper.Structures.Import.ClientImportData clientImport, ref string portfolioId, ref List<string> addressIdsList,
            ref List<string> nameIdsList)
        {
            Response response = new Response();
            bool somethingUpdateable = false;
            DateTime dateNow = DateTime.Now;

            dataManager.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientImport.Client.CCClientId);
            BCS.ClientSys.Biz.Client bizclient = dataManager.GetClient(BCS.ClientSys.Biz.FetchPath.Client.AllChildren);
            if (bizclient == null)
            {
                int ClientIdNumeric = Convert.ToInt32(clientImport.Client.CCClientId);
                if (!CobraOleDbController.ClientExists(ClientIdNumeric))
                {
                    string helperMessage = string.Format("Unable to find the client with Id {0}. Please try again.", ClientIdNumeric);
                    LogCentral.Current.LogFatal(string.Format("{0}.", helperMessage));
                    response.ResponseCode = ResponseCode.Failure;
                    response.Data = 0;
                    response.Reason = helperMessage;
                    return response;
                }
                else // the client might exist just in cobra
                {
                    // just add the client row, the rest is taken over by the code below
                    bizclient = dataManager.NewClient(ClientIdNumeric,
                        DateTime.Now, lookupManager.Companys.FindByCompanyNumber(Properties.Settings.Default.CompanyNumber));
                    somethingUpdateable = true;
                }
            }

            #region Cobra Tax Id update
            int clientTaxId = 0;
            try
            {
                clientTaxId = Convert.ToInt32(clientImport.Client.PersonalInfo.TaxId.Replace("-", ""));
            }
            catch
            {
                // commenting, generates too many log entries
                //LogCentral.Current.LogWarn("Unable to convert tax Id (" + clientImport.Client.PersonalInfo.TaxId + ") to numeric. Setting to 0");
                clientTaxId = 0;
            }
            int result = CobraOleDbController.UpdateClientMaster(Convert.ToInt32(clientImport.Client.CCClientId), clientTaxId);
            if (result != 0)
            {
                string helperMessage = "Unable to update the taxid in cobra. Please try again.";
                LogCentral.Current.LogWarn(string.Format("{0}.", helperMessage));
            }
            #endregion

            if (bizclient.TaxNumber != clientImport.Client.PersonalInfo.TaxId || bizclient.TaxTypeCode != clientImport.Client.PersonalInfo.TaxIdTypeCode)
            {
                bizclient.TaxNumber = clientImport.Client.PersonalInfo.TaxId;
                bizclient.TaxTypeCode =
                    lookupManager.TaxTypes.FindByTaxTypeDescription(clientImport.Client.PersonalInfo.TaxIdTypeCode) != null ?
                    lookupManager.TaxTypes.FindByTaxTypeDescription(clientImport.Client.PersonalInfo.TaxIdTypeCode).Code : null;
                somethingUpdateable = true;
            }
            if (!(bizclient.PortfolioId.IsNull && string.IsNullOrEmpty(clientImport.Client.PortfolioId)) &&
                bizclient.PortfolioId.ToString() != clientImport.Client.PortfolioId)
            {
                dataManager.QueryCriteria.Clear();
                dataManager.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientImport.Client.PortfolioId);
                BCS.ClientSys.Biz.Client portfolioClient = dataManager.GetClient();
                if (portfolioClient != null)
                {
                    bizclient.PortfolioId = portfolioClient.ClientCoreClientId;
                }
                else
                {
                    bizclient.PortfolioId = bizclient.ClientCoreClientId;
                }

                somethingUpdateable = true;
            }

            #region Names
            List<CCStructures.Import.NameInfo> addnlNameList = new List<BTS.ClientCore.Wrapper.Structures.Import.NameInfo>();
            foreach (CCStructures.Import.NameInfo nameVar in clientImport.Client.NameInfos)
            {
                if (string.IsNullOrEmpty(nameVar.NameSeqNum))
                {
                    addnlNameList.Add(nameVar);
                    somethingUpdateable = true;
                    continue;
                }
                BCS.ClientSys.Biz.ClientName thisName = bizclient.ClientNames.FindByOrderIndex(nameVar.NameSeqNum);
                if (thisName.BusinessName != nameVar.BusinessName)
                {
                    thisName.BusinessName = nameVar.BusinessName;
                    somethingUpdateable = true;
                }
            }
            int lastNameOrderIndex = 0;
            if (bizclient.ClientNames != null && bizclient.ClientNames.Count > 0)
                lastNameOrderIndex = bizclient.ClientNames.SortByOrderIndex(OrmLib.SortDirection.Descending)[0].OrderIndex.Value;
            int nextNameOrderIndex = lastNameOrderIndex + 1;
            foreach (CCStructures.Import.NameInfo nameVar in addnlNameList)
            {
                BCS.ClientSys.Biz.ClientName newClientName = bizclient.NewClientName();
                newClientName.BusinessName = nameVar.BusinessName;
                newClientName.EntryDt = dateNow;
                newClientName.InBillingSystem = false;
                newClientName.OrderIndex = nextNameOrderIndex; nextNameOrderIndex++;


                // this process only loads from legacy system
                newClientName.FromLegacySystem = true;
            }
            #endregion

            #region Addresses
            List<CCStructures.Import.AddressInfo> addnlAddressList = new List<BTS.ClientCore.Wrapper.Structures.Import.AddressInfo>();
            foreach (CCStructures.Import.AddressInfo addrVar in clientImport.Client.AddressInfos)
            {
                if (string.IsNullOrEmpty(addrVar.CCAddressId))
                {
                    addnlAddressList.Add(addrVar);
                    somethingUpdateable = true;
                    continue;
                }
                BCS.ClientSys.Biz.ClientAddress thisAddr = bizclient.ClientAddresss.FindById(addrVar.CCAddressId);
                if (thisAddr.City != addrVar.CityName)
                {
                    thisAddr.City = addrVar.CityName;
                    somethingUpdateable = true;
                }
                if (thisAddr.CountryCode != addrVar.Country)
                {
                    thisAddr.CountryCode = addrVar.Country;
                    somethingUpdateable = true;
                }
                if (thisAddr.County != addrVar.County)
                {
                    thisAddr.County = addrVar.County;
                    somethingUpdateable = true;
                }
                if (thisAddr.StateCode != addrVar.StateCode)
                {
                    thisAddr.StateCode = addrVar.StateCode;
                    somethingUpdateable = true;
                }
                if (thisAddr.StreetLine2 != addrVar.ADDR2)
                {
                    thisAddr.StreetLine2 = addrVar.ADDR2;
                    somethingUpdateable = true;
                }
                if (thisAddr.StreetName != addrVar.ADDR1)
                {
                    thisAddr.StreetName = addrVar.ADDR1;
                    somethingUpdateable = true;
                }
                if (thisAddr.StreetNumber != addrVar.HouseNumber)
                {
                    thisAddr.StreetNumber = addrVar.HouseNumber;
                    somethingUpdateable = true;
                }
                if (thisAddr.ZipCode != addrVar.Zip)
                {
                    thisAddr.ZipCode = addrVar.Zip;
                    somethingUpdateable = true;
                }
            }

            int lastAddressOrderIndex = 0;
            if (bizclient.ClientAddresss != null && bizclient.ClientAddresss.Count > 0)
                lastAddressOrderIndex = bizclient.ClientAddresss.SortByOrderIndex(OrmLib.SortDirection.Descending)[0].OrderIndex.Value;
            int nextAddressOrderIndex = lastNameOrderIndex + 1;

            foreach (CCStructures.Import.AddressInfo addrVar in addnlAddressList)
            {
                BCS.ClientSys.Biz.ClientAddress newClientAddr = bizclient.NewClientAddress();
                newClientAddr.City = addrVar.CityName;
                newClientAddr.CountryCode = addrVar.Country;
                newClientAddr.County = addrVar.County;
                newClientAddr.EntryDt = dateNow;
                newClientAddr.InBillingSystem = false;
                newClientAddr.OrderIndex = nextAddressOrderIndex; nextAddressOrderIndex++;
                newClientAddr.StateCode = addrVar.StateCode;
                newClientAddr.StreetLine2 = addrVar.ADDR2;
                newClientAddr.StreetName = addrVar.ADDR1;
                newClientAddr.StreetNumber = addrVar.HouseNumber;
                newClientAddr.ZipCode = addrVar.Zip;

                // this process only loads from legacy system
                newClientAddr.FromLegacySystem = true;
            }
            #endregion

            if (somethingUpdateable)
            {
                dataManager.CommitAll();
                foreach (BCS.ClientSys.Biz.ClientAddress committedClientAddress in bizclient.ClientAddresss)
                {
                    addressIdsList.Add(committedClientAddress.Id.ToString());
                }
                foreach (BCS.ClientSys.Biz.ClientName committedClientName in bizclient.ClientNames)
                {
                    // order index is used by bcs as client core as name identifier 
                    nameIdsList.Add(committedClientName.OrderIndex.ToString());
                }
                portfolioId = bizclient.PortfolioId.IsNull ? bizclient.Id.ToString() : bizclient.PortfolioId.ToString();
            }

            response.Data = clientImport.Client.CCClientId;
            response.ResponseCode = ResponseCode.Success;
            return response;
        }
       
        private void HandleCobraAdd(object obj)
        {
            DateTime startTime = DateTime.Now;
            CobraClientMaster clientMaster = obj as CobraClientMaster;
            // client master should already be built by the caller
            int masterResult = new CobraOleDbController().AddClientMaster(clientMaster);
            
            // 0 is returned in case of failure
            if (masterResult == 0)
            {
                // since this is an async call, we will log all details so that we can do something later
                string helperMessage = string.Format(
                    "Unable to add client master entry. ClientId - {0}, ClientType - {1}, Effective - {2}, Expiration - {3}, LastName - {4}, Tax - {5}."
                    , clientMaster.ClientId, clientMaster.ClientType,
                    clientMaster.Effective, clientMaster.Expiration, clientMaster.LastName, clientMaster.Tax);
                LogCentral.Current.LogFatal(helperMessage);
                return;
            }

            #region Build and Add client label object
            CobraClientLabel clientLabel = new CobraClientLabel();
            string[] processedNames = Common.ProcessBusinessName(clientMaster.LastName, 4);
            clientLabel.Label1 = processedNames[0];

            int labelId = new CobraOleDbController().AddClientLabel(clientLabel);
            if (labelId == 0)
            {
                // only thing we are adding to client_label here is Label1
                string helperMessage = string.Format("Unable to add client label entry. Label1 - {0}.", clientLabel.Label1);
                LogCentral.Current.LogError(helperMessage);
            }
            #endregion

            #region Build and Add client role object
            CobraClientRole clientRole = new CobraClientRole();

            clientRole.Account = "0000";
            clientRole.ClientId = masterResult;
            clientRole.ClientLabelId = labelId;
            clientRole.Effective = DateTime.Now;

            int roleId = new CobraOleDbController().AddClientRole(clientRole, "PRIMRY");
            if (roleId == 0)
            {
                string helperMessage = 
                    string.Format(
                    "Unable to add client role entry. Account - {0}, ClientId - {1}, ClientLabelId - {2}, Effective - {3}, Type - PRIMRY",
                    clientRole.Account, masterResult, labelId, clientMaster.Effective );
                LogCentral.Current.LogError(helperMessage);
            }
            #endregion

            #region Build and Add client search label object
            int clientSearchLabelResult = new CobraOleDbController().AddClientSearchLabel(clientLabel.Label1, clientLabel.ClientLabelId, "PRIMRY");
            if (clientSearchLabelResult == 0)
            {
                string helperMessage = string.Format(
                    "Unable to add client label search entry. Label1 - {0}, ClientLabelId - {1}, type - PRIMRY",
                    clientLabel.Label1, clientLabel.ClientLabelId);
                LogCentral.Current.LogWarn(string.Format("{0}", helperMessage));
            }
            #endregion
            
            DateTime endTime = DateTime.Now;
            Common.LogTimeTaken(string.Format("cobra sytem add ({0})", clientMaster.ClientId), startTime, endTime);
        }

        public void addProspect(int clientId, string policyNumber)
        {
            #region TODO Test region fail some prospects
            //if (clientId % 2 == 0)
            //{
            //    Program.AddToFailedProspects(clientId.ToString(), policyNumber);
            //}
            //else
            //{
            //    Thread ap = new Thread(new ParameterizedThreadStart(new BCSClientSystemManager().HandleaddProspect));
            //    ap.Start(new string[] { clientId.ToString(), policyNumber });
            //}
            #endregion
            if (Properties.Settings.Default.ThreadCobra)
            {
                Thread ap = new Thread(new ParameterizedThreadStart(new BCSClientSystemManager().HandleaddProspect));
                ap.Start(new string[] { clientId.ToString(), policyNumber });
            }
            else
            {
                HandleaddProspect(new string[] { clientId.ToString(), policyNumber });
            }
        }
        public void HandleaddProspect(object obj)
        {
            string[] strArrObj = obj as string[];

            int clientId = Convert.ToInt32(strArrObj[0]);
            string policyNumber = strArrObj[1];

            // check if a policy already exists
            bool policyExists = CobraOleDbController.PolicyExists(clientId, policyNumber + Properties.Settings.Default.PolicyModAppender);
            // already there return the same
            if (policyExists)
            {
                LogCentral.Current.LogError(string.Format("policy already exists. clientId - {0}, policyNumber - {1}", clientId, policyNumber));
                //return "policy already exists.";
                return;
            }

            BCS.ClientSys.Biz.DataManager dataManager = Common.GetClientSysDataManager();

            #region get client from client system
            // getting a client with BCS.ClientSys.Biz.FetchPath.Client.AllChildren sometimes timing out. lets split it
            //dataManager.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientId);
            //BCS.ClientSys.Biz.Client client = dataManager.GetClient(BCS.ClientSys.Biz.FetchPath.Client.AllChildren);

            //BCS.ClientSys.Biz.ClientName firstName = client.ClientNames.SortByOrderIndex(OrmLib.SortDirection.Ascending)[0];
            //BCS.ClientSys.Biz.ClientAddress firstAddress = client.ClientAddresss.SortByOrderIndex(OrmLib.SortDirection.Ascending)[0];

            // get names separately
            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Client.Columns.ClientCoreClientId, clientId);
            BCS.ClientSys.Biz.ClientNameCollection names = dataManager.GetClientNameCollection();
            
            // get addresses separately
            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientAddress.Client.Columns.ClientCoreClientId, clientId);
            BCS.ClientSys.Biz.ClientAddressCollection addresses = dataManager.GetClientAddressCollection();

            // fiter and check if there are any 'in billing' names set up
            BCS.ClientSys.Biz.ClientNameCollection inBSNames = names.FilterByInBillingSystem(true);
            if (inBSNames != null && inBSNames.Count > 0)
            {
                // if there are, continue with those
                names = inBSNames;
            }
            // fiter and check if there are any 'in billing' addresses set up
            BCS.ClientSys.Biz.ClientAddressCollection inBSAddresses = addresses.FilterByInBillingSystem(true);
            if (inBSAddresses != null && inBSAddresses.Count > 0)
            {
                // if there are, continue with those
                addresses = inBSAddresses;
            }

            BCS.ClientSys.Biz.ClientName firstName = names.SortByOrderIndex(OrmLib.SortDirection.Ascending)[0];
            BCS.ClientSys.Biz.ClientAddress firstAddress = addresses.SortByOrderIndex(OrmLib.SortDirection.Ascending)[0];
            #endregion
          

            CobraClientLabel clientLabel = new CobraClientLabel();
            clientLabel.City = firstAddress.City;


            #region Process business name
            clientLabel.Label1 = firstName.BusinessName;

            string[] processedNames = Common.ProcessBusinessName(firstName.BusinessName, 4);

            if (processedNames.Length == 0 || string.IsNullOrEmpty(processedNames[0]))
            {
                string helperMessage = string.Format("Unable to process business name while adding a prospect clientId - {0}, policyNumber - {1}.",
                    clientId, policyNumber);
                LogCentral.Current.LogError(helperMessage);
            }
            clientLabel.Label1 = processedNames[0];

            // if any arr element is empty, it means subsequent are empty
            if (string.IsNullOrEmpty(processedNames[1]))
            {
                clientLabel.Label2 = firstAddress.StreetNumber + " " + firstAddress.StreetName;
                clientLabel.Label3 = firstAddress.StreetLine2;
            }
            else
            {
                clientLabel.Label2 = processedNames[1];

                // if any arr element is empty, it means subsequent are empty
                if (string.IsNullOrEmpty(processedNames[2]))
                {
                    clientLabel.Label3 = firstAddress.StreetNumber + " " + firstAddress.StreetName;
                    clientLabel.Label4 = firstAddress.StreetLine2;
                }
                else
                {
                    clientLabel.Label3 = processedNames[2];

                    // if any arr element is empty, it means subsequent are empty
                    if (string.IsNullOrEmpty(processedNames[3]))
                    {
                        clientLabel.Label4 = firstAddress.StreetNumber + " " + firstAddress.StreetName;
                    }
                    else
                    {
                        clientLabel.Label4 = processedNames[3];
                    }
                }
            }
            #endregion

            clientLabel.State = firstAddress.StateCode;
            clientLabel.Zip = firstAddress.ZipCode;

            int labelId = new CobraOleDbController().AddClientLabel(clientLabel);
            if (labelId == 0)
            {
                string helperMessage = string.Format("Unable to add client label entry while adding prospect for clientid {0}, policynumber {1}.",
                    clientId, policyNumber);
                helperMessage += string.Format(" Label1 = {0}, Label2 = {1}, Label3 = {2}, Label4 = {3}.",
                    clientLabel.Label1, clientLabel.Label2, clientLabel.Label3, clientLabel.Label4);
                LogCentral.Current.LogError(helperMessage);
                Program.AddToFailedProspects(clientId.ToString(), policyNumber);
                return;
            }

            CobraClientRole clientRole = new CobraClientRole();

            clientRole.Account = policyNumber + Properties.Settings.Default.PolicyModAppender;
            clientRole.ClientId = clientId;
            clientRole.ClientLabelId = labelId;
            clientRole.Effective = DateTime.Now;

            int roleId = new CobraOleDbController().AddClientRole(clientRole, "PRSPT");
            // TODO: 
            if (roleId == 0)
            {
                string helperMessage = string.Format("Unable to add client role entry while adding prospect for clientid {0}, policynumber {1}.",
                    clientId, policyNumber);
                helperMessage += string.Format(" LableId = {0}, Label1 = {1}, Label2 = {2}, Label3 = {3}, Label4 = {4}.",
                    clientLabel.ClientLabelId, clientLabel.Label1, clientLabel.Label2, clientLabel.Label3, clientLabel.Label4);
                LogCentral.Current.LogError(string.Format("{0}.", helperMessage));
                Program.AddToFailedProspects(clientId.ToString(), policyNumber);
                return;
            }

            //return string.Empty;
        }
        /// <summary>
        /// after introducing refs, this method is not needed
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns>Response's data property will be of type BTS.ClientCore.Wrapper.Structures.Info.ClientInfo</returns>
        [Obsolete("after introducing refs, this method is not needed", true)]
        public Response GetClientById(string clientId)
        {
            Response response = new Response();

            if (string.IsNullOrEmpty(clientId))
            {
                string helperMessage = "No client id provided. Please try again";
                LogCentral.Current.LogFatal(string.Format("{0}.", helperMessage));
                response.ResponseCode = ResponseCode.Failure;
                response.Data = null;
                response.Reason = helperMessage;
                return response;
            }

            #region Get Client
            BCS.ClientSys.Biz.DataManager dataManager = Common.GetClientSysDataManager();
            dataManager.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientId);
            BCS.ClientSys.Biz.Client client = dataManager.GetClient(BCS.ClientSys.Biz.FetchPath.Client.AllChildren);
            #endregion

            #region Build Client
            CCStructures.Info.ClientInfo returnClient = new CCStructures.Info.ClientInfo();
            if (client == null)
            {
                string helperMessage = string.Format("Client with id {0} not found. Please try again", clientId);
                LogCentral.Current.LogFatal(string.Format("{0}.", helperMessage));
                response.ResponseCode = ResponseCode.Failure;
                response.Data = returnClient;
                response.Reason = helperMessage;
                return response;
            }
            #region Build Personal info
            returnClient.Client = new CCStructures.Info.Client();
            returnClient.Client.ClientId = client.ClientCoreClientId.ToString();
            returnClient.Client.PortfolioId = client.PortfolioId.IsNull ? string.Empty : client.PortfolioId.ToString();
            if (Common.IsNotNullOrEmpty(client.TaxNumber))
            {
                returnClient.Client.TaxId = client.TaxNumber;
                returnClient.Client.TaxIdType = client.TaxType != null ? client.TaxType.TaxTypeDescription : null;
            }
            #endregion

            #region Build Names
            List<CCStructures.Info.Name> nameList = new List<CCStructures.Info.Name>();
            BCS.ClientSys.Biz.ClientNameCollection sortedClientNames = client.ClientNames.SortByOrderIndex(OrmLib.SortDirection.Ascending);
            foreach (BCS.ClientSys.Biz.ClientName var in sortedClientNames)
            {
                CCStructures.Info.Name nameVar = new CCStructures.Info.Name();
                nameVar.NameBusiness = var.BusinessName;
                nameVar.SequenceNumber = var.OrderIndex.ToString();
                nameList.Add(nameVar);
            }
            CCStructures.Info.Name[] nameArray = new CCStructures.Info.Name[nameList.Count];
            nameList.CopyTo(nameArray);
            returnClient.Client.Names = nameArray;
            #endregion

            #region Build Addresses
            List<CCStructures.Info.Address> addressList = new List<CCStructures.Info.Address>();
            BCS.ClientSys.Biz.ClientAddressCollection sortedClientAddresses = client.ClientAddresss.SortByOrderIndex(OrmLib.SortDirection.Ascending);
            foreach (BCS.ClientSys.Biz.ClientAddress var in sortedClientAddresses)
            {
                CCStructures.Info.Address addressVar = new CCStructures.Info.Address();
                addressVar.HouseNumber = var.StreetNumber;
                addressVar.Address1 = var.StreetName;
                addressVar.Address2 = var.StreetLine2;
                addressVar.AddressId = var.Id.ToString();
                addressVar.City = var.City;
                addressVar.Country = var.CountryCode;
                addressVar.County = var.County;
                addressVar.PostalCode = var.ZipCode;
                addressVar.StateProvinceId = var.StateCode;

                addressList.Add(addressVar);
            }
            CCStructures.Info.Address[] addressArray = new CCStructures.Info.Address[addressList.Count];
            addressList.CopyTo(addressArray);
            returnClient.Addresses = addressArray;
            #endregion



            #endregion

            response.Data = returnClient;
            response.ResponseCode = ResponseCode.Success;
            return response;
        }
    }

    //class BCSClientSystemManagerOld
    //{
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="clientImport"></param>
    //    /// <returns>Response's data property will be of type int</returns>
    //    public CobraResponse importClient(CCStructures.Import.ClientImportData clientImport)
    //    {
    //        // edit op
    //        if (string.IsNullOrEmpty(clientImport.Client.CCClientId))
    //        {
    //            return importClientAdd(clientImport);
    //        }
    //        else // add op
    //        {
    //            // TODO remove the second param
    //            return importClientEdit(clientImport);
    //        }
    //    }

    //    private CobraResponse importClientAdd(BTS.ClientCore.Wrapper.Structures.Import.ClientImportData clientImport)
    //    {
    //        CobraResponse response = new CobraResponse();

    //        // we need at least one name, so that we can add the first one as PRIMRY/PRSPT to cobra.
    //        // TODO: Do we do the same for addresses too?
    //        if (clientImport.Client.NameInfos == null || clientImport.Client.NameInfos.Length == 0)
    //        {
    //            string helperMessage = "There must be at least one name. Please try again.";
    //            LogCentral.Current.LogFatal(string.Format("{0}.", helperMessage));
    //            response.ResponseCode = ResponseCode.Failure;
    //            response.Data = 0;
    //            response.Reason = helperMessage;
    //            return response;
    //        }

    //        #region Cobra Handling
    //        DateTime startTime = DateTime.Now;
    //        #region Build and add the client master object
    //        CobraClientMaster clientMaster = new CobraClientMaster();

    //        // We use the first name to set the client master
    //        if (clientImport.Client.NameInfos[0].BusinessName.Length > 0)
    //        {
    //            clientMaster.ClientType = ClientType.Organization;

    //            clientMaster.LastName = clientImport.Client.NameInfos[0].BusinessName;
    //        }
    //        else
    //        {
    //            clientMaster.ClientType = ClientType.Individual;

    //            clientMaster.FirstName = clientImport.Client.NameInfos[0].FirstName;
    //            clientMaster.MiddleName = clientImport.Client.NameInfos[0].MiddleName;
    //            clientMaster.LastName = clientImport.Client.NameInfos[0].LastName;
    //        }

    //        // bug in COBRA UI does not recognise F as Taxnumber, so hard coding it to T
    //        clientMaster.TaxType = "T";

    //        try
    //        {
    //            clientMaster.Tax = Convert.ToInt32(clientImport.Client.PersonalInfo.TaxId.Replace("-", ""));
    //        }
    //        catch
    //        {
    //            LogCentral.Current.LogWarn("Unable to convert tax Id (" + clientImport.Client.PersonalInfo.TaxId + ") to numeric. Setting to 0");
    //            clientMaster.Tax = 0;
    //        }

    //        int masterClientId = CobraOleDbController.AddClientMaster(clientMaster);

    //        if (masterClientId == 0)
    //        {
    //            string helperMessage = "Unable to add client master entry.";
    //            LogCentral.Current.LogFatal(string.Format("{0}", helperMessage));
    //            response.ResponseCode = ResponseCode.Failure;
    //            response.Data = 0;
    //            response.Reason = helperMessage;
    //            return response;
    //        }
    //        #endregion

    //        #region Build and Add client label object
    //        CobraClientLabel clientLabel = new CobraClientLabel();
    //        CCStructures.Import.NameInfo varNameInfo = clientImport.Client.NameInfos[0];
    //        if (varNameInfo.BusinessName.Length > 0)
    //        {
    //            string[] processedNames = Common.ProcessBusinessName(varNameInfo.BusinessName, 4);
    //            clientLabel.Label1 = processedNames[0];
    //        }
    //        else
    //        {
    //            clientLabel.Label1 = varNameInfo.FirstName + " " + varNameInfo.LastName;
    //        }

    //        int labelId = CobraOleDbController.AddClientLabel(clientLabel);
    //        if (labelId == 0)
    //        {
    //            string helperMessage = "Unable to add client label entry.";
    //            LogCentral.Current.LogFatal(string.Format("{0}", helperMessage));
    //            response.ResponseCode = ResponseCode.Failure;
    //            response.Data = 0;
    //            response.Reason = helperMessage;
    //            return response;
    //        }
    //        #endregion

    //        #region Build and Add client role object
    //        CobraClientRole clientRole = new CobraClientRole();

    //        clientRole.Account = "0000";
    //        clientRole.ClientId = masterClientId;
    //        clientRole.ClientLabelId = labelId;
    //        clientRole.Effective = DateTime.Now;

    //        int roleId = CobraOleDbController.AddClientRole(clientRole, "PRIMRY");
    //        if (roleId == 0)
    //        {
    //            string helperMessage = "Unable to add client role entry.";
    //            LogCentral.Current.LogFatal(string.Format("{0}", helperMessage));
    //            response.ResponseCode = ResponseCode.Failure;
    //            response.Data = 0;
    //            response.Reason = helperMessage;
    //            return response;
    //        }
    //        #endregion

    //        int clientSearchLabelResult = CobraOleDbController.AddClientSearchLabel(clientLabel.Label1, clientLabel.ClientLabelId, "PRIMRY");
    //        if (clientSearchLabelResult == 0)
    //        {
    //            string helperMessage = "Unable to add client label search entry.";
    //            LogCentral.Current.LogWarn(string.Format("{0}", helperMessage));
    //        }

    //        DateTime endTime = DateTime.Now;
    //        Common.LoadTimeTaken("cobra client add", startTime, endTime);

    //        #endregion

    //        #region BCS Client System Handling
    //        startTime = DateTime.Now;

    //        DateTime dateNow = DateTime.Now;

    //        BCS.ClientSys.Biz.DataManager dataManager = Common.GetClientSysDataManager();
    //        BCS.ClientSys.Biz.Lookups lookupManager = Common.GetClientSysLookupManager();
    //        BCS.ClientSys.Biz.Client newClient = dataManager.NewClient(masterClientId, dateNow,
    //            lookupManager.Companys.FindByCompanyNumber(Properties.Settings.Default.CompanyNumber));

    //        newClient.TaxNumber = clientImport.Client.PersonalInfo.TaxId;
    //        newClient.TaxTypeCode =
    //            lookupManager.TaxTypes.FindByTaxTypeDescription(clientImport.Client.PersonalInfo.TaxIdTypeCode) != null ?
    //            lookupManager.TaxTypes.FindByTaxTypeDescription(clientImport.Client.PersonalInfo.TaxIdTypeCode).Code : null;
    //        newClient.PortfolioId = masterClientId;

    //        for (int i = 0; i < clientImport.Client.NameInfos.Length; i++)
    //        {
    //            CCStructures.Import.NameInfo var = clientImport.Client.NameInfos[i];
    //            BCS.ClientSys.Biz.ClientName newClientName = newClient.NewClientName();
    //            newClientName.BusinessName = var.BusinessName;
    //            newClientName.EntryDt = dateNow;
    //            // we are adding first name to cobra
    //            newClientName.InBillingSystem = i == 0;
    //            newClientName.OrderIndex = i + 1;
    //        }
    //        for (int i = 0; i < clientImport.Client.AddressInfos.Length; i++)
    //        {
    //            CCStructures.Import.AddressInfo var = clientImport.Client.AddressInfos[i];
    //            BCS.ClientSys.Biz.ClientAddress newClientAddress = newClient.NewClientAddress();
    //            newClientAddress.City = var.CityName;
    //            newClientAddress.CountryCode = var.Country;
    //            newClientAddress.County = var.County;
    //            newClientAddress.EntryDt = dateNow;
    //            newClientAddress.InBillingSystem = false;
    //            newClientAddress.OrderIndex = i + 1;
    //            newClientAddress.StateCode = var.StateCode;
    //            newClientAddress.StreetLine2 = var.ADDR2;
    //            newClientAddress.StreetName = var.ADDR1;
    //            newClientAddress.StreetNumber = var.HouseNumber;
    //            newClientAddress.ZipCode = var.Zip;

    //            // pad the zip extension
    //            while (newClientAddress.ZipCode.Length < 9)
    //            {
    //                newClientAddress.ZipCode += "0";
    //            }
    //        }

    //        try
    //        {
    //            dataManager.CommitAll();
    //        }
    //        catch (Exception)
    //        {
    //            string helperMessage = string.Format("Unable to add client to the bcs client system. The Client Id from cobra is '{0}'", masterClientId);
    //            LogCentral.Current.LogFatal(string.Format("{0}", helperMessage));
    //            response.ResponseCode = ResponseCode.Failure;
    //            response.Data = 0;
    //            response.Reason = helperMessage;
    //            return response;
    //        }

    //        endTime = DateTime.Now;
    //        Common.LoadTimeTaken("client sytem add", startTime, endTime);

    //        #endregion

    //        response.Data = masterClientId;
    //        response.ResponseCode = ResponseCode.Success;
    //        return response;
    //    }

    //    private CobraResponse importClientEdit(BTS.ClientCore.Wrapper.Structures.Import.ClientImportData clientImport)
    //    {
    //        CobraResponse response = new CobraResponse();
    //        bool somethingUpdateable = false;
    //        DateTime dateNow = DateTime.Now;

    //        BCS.ClientSys.Biz.Lookups lookupManager = Common.GetClientSysLookupManager();
    //        BCS.ClientSys.Biz.DataManager dataManager = Common.GetClientSysDataManager();
    //        dataManager.QueryCriteria.And(Biz.JoinPath.Client.Columns.ClientCoreClientId, clientImport.Client.CCClientId);
    //        BCS.ClientSys.Biz.Client bizclient = dataManager.GetClient(BCS.ClientSys.Biz.FetchPath.Client.AllChildren);
    //        if (bizclient == null)
    //        {
    //            int ClientIdNumeric = Convert.ToInt32(clientImport.Client.CCClientId);
    //            if (!CobraOleDbController.ClientExists(ClientIdNumeric))
    //            {
    //                string helperMessage = string.Format("Unable to find the client with Id {0}. Please try again.", ClientIdNumeric);
    //                LogCentral.Current.LogFatal(string.Format("{0}.", helperMessage));
    //                response.ResponseCode = ResponseCode.Failure;
    //                response.Data = 0;
    //                response.Reason = helperMessage;
    //                return response;
    //            }
    //            else // the client might exist just in cobra
    //            {
    //                // just add the client row, the rest is taken over by the code below
    //                bizclient = dataManager.NewClient(ClientIdNumeric,
    //                    DateTime.Now, lookupManager.Companys.FindByCompanyNumber(Properties.Settings.Default.CompanyNumber));
    //                somethingUpdateable = true;
    //            }
    //        }

    //        #region Cobra Tax Id update
    //        int clientTaxId = 0;
    //        try
    //        {
    //            clientTaxId = Convert.ToInt32(clientImport.Client.PersonalInfo.TaxId.Replace("-", ""));
    //        }
    //        catch
    //        {
    //            LogCentral.Current.LogWarn("Unable to convert tax Id (" + clientImport.Client.PersonalInfo.TaxId + ") to numeric. Setting to 0");
    //            clientTaxId = 0;
    //        }
    //        int result = CobraOleDbController.UpdateClientMaster(Convert.ToInt32(clientImport.Client.CCClientId), clientTaxId);
    //        if (result != 0)
    //        {
    //            string helperMessage = "Unable to update the taxid in cobra. Please try again.";
    //            LogCentral.Current.LogWarn(string.Format("{0}.", helperMessage));
    //        }
    //        #endregion

    //        if (bizclient.TaxNumber != clientImport.Client.PersonalInfo.TaxId || bizclient.TaxTypeCode != clientImport.Client.PersonalInfo.TaxIdTypeCode)
    //        {
    //            bizclient.TaxNumber = clientImport.Client.PersonalInfo.TaxId;
    //            bizclient.TaxTypeCode =
    //                lookupManager.TaxTypes.FindByTaxTypeDescription(clientImport.Client.PersonalInfo.TaxIdTypeCode) != null ?
    //                lookupManager.TaxTypes.FindByTaxTypeDescription(clientImport.Client.PersonalInfo.TaxIdTypeCode).Code : null;
    //            somethingUpdateable = true;
    //        }
    //        if (!(bizclient.PortfolioId.IsNull && string.IsNullOrEmpty(clientImport.Client.PortfolioId)) &&
    //            bizclient.PortfolioId.ToString() != clientImport.Client.PortfolioId)
    //        {
    //            dataManager.QueryCriteria.Clear();
    //            dataManager.QueryCriteria.And(Biz.JoinPath.Client.Columns.ClientCoreClientId, clientImport.Client.PortfolioId);
    //            BCS.ClientSys.Biz.Client portfolioClient = dataManager.GetClient();
    //            if (portfolioClient != null)
    //            {
    //                bizclient.PortfolioId = portfolioClient.ClientCoreClientId;
    //            }
    //            else
    //            {
    //                bizclient.PortfolioId = bizclient.ClientCoreClientId;
    //            }

    //            somethingUpdateable = true;
    //        }

    //        #region Names
    //        List<CCStructures.Import.NameInfo> addnlNameList = new List<BTS.ClientCore.Wrapper.Structures.Import.NameInfo>();
    //        foreach (CCStructures.Import.NameInfo nameVar in clientImport.Client.NameInfos)
    //        {
    //            if (string.IsNullOrEmpty(nameVar.NameSeqNum))
    //            {
    //                addnlNameList.Add(nameVar);
    //                somethingUpdateable = true;
    //                continue;
    //            }
    //            BCS.ClientSys.Biz.ClientName thisName = bizclient.ClientNames.FindByOrderIndex(nameVar.NameSeqNum);
    //            if (thisName.BusinessName != nameVar.BusinessName)
    //            {
    //                thisName.BusinessName = nameVar.BusinessName;
    //                somethingUpdateable = true;
    //            }
    //        }
    //        int lastNameOrderIndex = 0;
    //        if (bizclient.ClientNames != null && bizclient.ClientNames.Count > 0)
    //            lastNameOrderIndex = bizclient.ClientNames.SortByOrderIndex(OrmLib.SortDirection.Descending)[0].OrderIndex.Value;
    //        int nextNameOrderIndex = lastNameOrderIndex + 1;
    //        foreach (CCStructures.Import.NameInfo nameVar in addnlNameList)
    //        {
    //            BCS.ClientSys.Biz.ClientName newClientName = bizclient.NewClientName();
    //            newClientName.BusinessName = nameVar.BusinessName;
    //            newClientName.EntryDt = dateNow;
    //            newClientName.InBillingSystem = false;
    //            newClientName.OrderIndex = nextNameOrderIndex; nextNameOrderIndex++;
    //        }
    //        #endregion

    //        #region Addresses
    //        List<CCStructures.Import.AddressInfo> addnlAddressList = new List<BTS.ClientCore.Wrapper.Structures.Import.AddressInfo>();
    //        foreach (CCStructures.Import.AddressInfo addrVar in clientImport.Client.AddressInfos)
    //        {
    //            if (string.IsNullOrEmpty(addrVar.CCAddressId))
    //            {
    //                addnlAddressList.Add(addrVar);
    //                somethingUpdateable = true;
    //                continue;
    //            }
    //            BCS.ClientSys.Biz.ClientAddress thisAddr = bizclient.ClientAddresss.FindById(addrVar.CCAddressId);
    //            if (thisAddr.City != addrVar.CityName)
    //            {
    //                thisAddr.City = addrVar.CityName;
    //                somethingUpdateable = true;
    //            }
    //            if (thisAddr.CountryCode != addrVar.Country)
    //            {
    //                thisAddr.CountryCode = addrVar.Country;
    //                somethingUpdateable = true;
    //            }
    //            if (thisAddr.County != addrVar.County)
    //            {
    //                thisAddr.County = addrVar.County;
    //                somethingUpdateable = true;
    //            }
    //            if (thisAddr.StateCode != addrVar.StateCode)
    //            {
    //                thisAddr.StateCode = addrVar.StateCode;
    //                somethingUpdateable = true;
    //            }
    //            if (thisAddr.StreetLine2 != addrVar.ADDR2)
    //            {
    //                thisAddr.StreetLine2 = addrVar.ADDR2;
    //                somethingUpdateable = true;
    //            }
    //            if (thisAddr.StreetName != addrVar.ADDR1)
    //            {
    //                thisAddr.StreetName = addrVar.ADDR1;
    //                somethingUpdateable = true;
    //            }
    //            if (thisAddr.StreetNumber != addrVar.HouseNumber)
    //            {
    //                thisAddr.StreetNumber = addrVar.HouseNumber;
    //                somethingUpdateable = true;
    //            }
    //            if (thisAddr.ZipCode != addrVar.Zip)
    //            {
    //                thisAddr.ZipCode = addrVar.Zip;
    //                somethingUpdateable = true;
    //            }
    //        }

    //        int lastAddressOrderIndex = 0;
    //        if (bizclient.ClientAddresss != null && bizclient.ClientAddresss.Count > 0)
    //            lastAddressOrderIndex = bizclient.ClientAddresss.SortByOrderIndex(OrmLib.SortDirection.Descending)[0].OrderIndex.Value;
    //        int nextAddressOrderIndex = lastNameOrderIndex + 1;

    //        foreach (CCStructures.Import.AddressInfo addrVar in addnlAddressList)
    //        {
    //            BCS.ClientSys.Biz.ClientAddress newClientAddr = bizclient.NewClientAddress();
    //            newClientAddr.City = addrVar.CityName;
    //            newClientAddr.CountryCode = addrVar.Country;
    //            newClientAddr.County = addrVar.County;
    //            newClientAddr.EntryDt = dateNow;
    //            newClientAddr.InBillingSystem = false;
    //            newClientAddr.OrderIndex = nextAddressOrderIndex; nextAddressOrderIndex++;
    //            newClientAddr.StateCode = addrVar.StateCode;
    //            newClientAddr.StreetLine2 = addrVar.ADDR2;
    //            newClientAddr.StreetName = addrVar.ADDR1;
    //            newClientAddr.StreetNumber = addrVar.HouseNumber;
    //            newClientAddr.ZipCode = addrVar.Zip;
    //        }
    //        #endregion

    //        if (somethingUpdateable)
    //            dataManager.CommitAll();

    //        response.Data = clientImport.Client.CCClientId;
    //        response.ResponseCode = ResponseCode.Success;
    //        return response;
    //    }

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="clientId"></param>
    //    /// <returns>Response's data property will be of type BTS.ClientCore.Wrapper.Structures.Info.ClientInfo</returns>
    //    public CobraResponse GetClientById(string clientId)
    //    {
    //        CobraResponse response = new CobraResponse();

    //        if (string.IsNullOrEmpty(clientId))
    //        {
    //            string helperMessage = "No client id provided. Please try again";
    //            LogCentral.Current.LogFatal(string.Format("{0}.", helperMessage));
    //            response.ResponseCode = ResponseCode.Failure;
    //            response.Data = null;
    //            response.Reason = helperMessage;
    //            return response;
    //        }

    //        #region Get Client
    //        BCS.ClientSys.Biz.DataManager dataManager = Common.GetClientSysDataManager();
    //        dataManager.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientId);
    //        BCS.ClientSys.Biz.Client client = dataManager.GetClient(BCS.ClientSys.Biz.FetchPath.Client.AllChildren);
    //        #endregion

    //        #region Build Client
    //        CCStructures.Info.ClientInfo returnClient = new CCStructures.Info.ClientInfo();
    //        if (client == null)
    //        {
    //            string helperMessage = string.Format("Client with id {0} not found. Please try again", clientId);
    //            LogCentral.Current.LogFatal(string.Format("{0}.", helperMessage));
    //            response.ResponseCode = ResponseCode.Failure;
    //            response.Data = returnClient;
    //            response.Reason = helperMessage;
    //            return response;
    //        }
    //        #region Build Personal info
    //        returnClient.Client = new CCStructures.Info.Client();
    //        returnClient.Client.ClientId = client.ClientCoreClientId.ToString();
    //        returnClient.Client.PortfolioId = client.PortfolioId.IsNull ? string.Empty : client.PortfolioId.ToString();
    //        if (Common.IsNotNullOrEmpty(client.TaxNumber))
    //        {
    //            returnClient.Client.TaxId = client.TaxNumber;
    //            returnClient.Client.TaxIdType = client.TaxType != null ? client.TaxType.TaxTypeDescription : null;
    //        }
    //        #endregion

    //        #region Build Names
    //        List<CCStructures.Info.Name> nameList = new List<CCStructures.Info.Name>();
    //        BCS.ClientSys.Biz.ClientNameCollection sortedClientNames = client.ClientNames.SortByOrderIndex(OrmLib.SortDirection.Ascending);
    //        foreach (BCS.ClientSys.Biz.ClientName var in sortedClientNames)
    //        {
    //            CCStructures.Info.Name nameVar = new CCStructures.Info.Name();
    //            nameVar.NameBusiness = var.BusinessName;
    //            nameVar.SequenceNumber = var.OrderIndex.ToString();
    //            nameList.Add(nameVar);
    //        }
    //        CCStructures.Info.Name[] nameArray = new CCStructures.Info.Name[nameList.Count];
    //        nameList.CopyTo(nameArray);
    //        returnClient.Client.Names = nameArray;
    //        #endregion

    //        #region Build Addresses
    //        List<CCStructures.Info.Address> addressList = new List<CCStructures.Info.Address>();
    //        BCS.ClientSys.Biz.ClientAddressCollection sortedClientAddresses = client.ClientAddresss.SortByOrderIndex(OrmLib.SortDirection.Ascending);
    //        foreach (BCS.ClientSys.Biz.ClientAddress var in sortedClientAddresses)
    //        {
    //            CCStructures.Info.Address addressVar = new CCStructures.Info.Address();
    //            addressVar.HouseNumber = var.StreetNumber;
    //            addressVar.Address1 = var.StreetName;
    //            addressVar.Address2 = var.StreetLine2;
    //            addressVar.AddressId = var.Id.ToString();
    //            addressVar.City = var.City;
    //            addressVar.Country = var.CountryCode;
    //            addressVar.County = var.County;
    //            addressVar.PostalCode = var.ZipCode;
    //            addressVar.StateProvinceId = var.StateCode;

    //            addressList.Add(addressVar);
    //        }
    //        CCStructures.Info.Address[] addressArray = new CCStructures.Info.Address[addressList.Count];
    //        addressList.CopyTo(addressArray);
    //        returnClient.Addresses = addressArray;
    //        #endregion



    //        #endregion

    //        response.Data = returnClient;
    //        response.ResponseCode = ResponseCode.Success;
    //        return response;
    //    }
    //}
}
