using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.CWGConversion.Cobra
{
    /// <summary>
    /// 
    /// </summary>
    public enum ClientType
    {
        /// <summary>
        /// 
        /// </summary>
        [StringValue("Organization")]
        Organization,

        /// <summary>
        /// 
        /// </summary>
        [StringValue("Individual")]
        Individual
    }
}