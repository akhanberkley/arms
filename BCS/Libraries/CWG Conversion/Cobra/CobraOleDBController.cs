using System;
using System.Data;
using System.Configuration;
using System.Data.OleDb;
using DB = BCS.CWGConversion.Database;
using Props = BCS.CWGConversion.Properties.Settings;


namespace BCS.CWGConversion.Cobra.Structures
{
    public class CobraOleDbController
    {
        public int AddClientLabel(CobraClientLabel clientLabel)
        {
            #region Internal Client Label stuff
            string connectionString = Props.Default.CobraDSN4OleDb;
            clientLabel.ClientLabelId = GetNextOid("CLBL", connectionString);
            #endregion

            // assume success. sp returns 0 as success
            int results = 0;

            OleDbCommand command = GetOleDbCommand("client_label_sp", connectionString, CommandType.StoredProcedure);

            #region Parameters
            OleDbParameter[] arParams = new OleDbParameter[8];

            arParams[0] = new OleDbParameter("Wn_clt_lbl_id", OleDbType.Decimal);
            arParams[0].Value = clientLabel.ClientLabelId;
            command.Parameters.Add(arParams[0]);

            arParams[1] = new OleDbParameter("Wt_aplus_lbl_1", OleDbType.Char);
            arParams[1].Value = clientLabel.Label1;
            command.Parameters.Add(arParams[1]);

            arParams[2] = new OleDbParameter("Wt_aplus_lbl_2", OleDbType.Char);
            arParams[2].Value = clientLabel.Label2;
            command.Parameters.Add(arParams[2]);

            arParams[3] = new OleDbParameter("Wt_aplus_lbl_3", OleDbType.Char);
            arParams[3].Value = clientLabel.Label3;
            command.Parameters.Add(arParams[3]);

            arParams[4] = new OleDbParameter("Wt_aplus_lbl_4", OleDbType.Char);
            arParams[4].Value = clientLabel.Label4;
            command.Parameters.Add(arParams[4]);

            arParams[5] = new OleDbParameter("Wt_aplus_city", OleDbType.Char);
            arParams[5].Value = clientLabel.City;
            command.Parameters.Add(arParams[5]);

            arParams[6] = new OleDbParameter("Wc_aplus_st", OleDbType.Char);
            arParams[6].Value = clientLabel.State;
            command.Parameters.Add(arParams[6]);

            arParams[7] = new OleDbParameter("Wc_aplus_zip", OleDbType.Char);
            arParams[7].Value = clientLabel.Zip;
            command.Parameters.Add(arParams[7]);
            #endregion

            try
            {
                results = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DB.Database.CloseOpenOleDbConnection(command.Connection);
            }
            if (results != 0)
            {
                //BTS.LogFramework.LogCentral.Current.LogError(string.Format("returned value from client_label_sp is {0}", results));
                return 0;
            }
            else
            {
                // success if reached here
                return clientLabel.ClientLabelId;
            }
        }


        internal int AddClientSearchLabel(string label, int labelId, string masterType)
        {
            int results = 0;

            string connectionString = Props.Default.CobraDSN4OleDb;

            OleDbCommand command = GetOleDbCommand("INSERT INTO client_label_srch VALUES (?, ?, ?)", connectionString, CommandType.Text);

            OleDbParameter ifxParam = new OleDbParameter("t_aplus_lbl_nam", OleDbType.Char);
            ifxParam.Value = label != null ? label.ToUpper() : string.Empty;
            command.Parameters.Add(ifxParam);

            ifxParam = new OleDbParameter("n_clt_lbl_id", OleDbType.Decimal);
            ifxParam.Value = labelId;
            command.Parameters.Add(ifxParam);

            ifxParam = new OleDbParameter("n_tmst_id", OleDbType.Decimal);
            ifxParam.Value = GetTypeMasterId("Role", masterType, connectionString);
            command.Parameters.Add(ifxParam);
            try
            {
                results = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception)
            {
                results = -1;
            }
            finally
            {
                DB.Database.CloseOpenOleDbConnection(command.Connection);
            }
            if (results != 0)
                return 0;
            else
            {
                // success if reached here
                return labelId;
            }
        }

        public int AddClientRole(CobraClientRole clientRole, string type)
        {

            string connectionString = Props.Default.CobraDSN4OleDb;
            clientRole.ClientRoleId = GetNextOid("CROLE", connectionString);
            clientRole.TypeMasterId = GetTypeMasterId("Role", type, connectionString);
            
            // assume success. sp returns 0 as success
            int results = 0;

            OleDbCommand command = GetOleDbCommand("client_role_sp", connectionString, CommandType.StoredProcedure);

            #region Parameters
            OleDbParameter[] arParams = new OleDbParameter[11];

            arParams[0] = new OleDbParameter("Wd_crol_bk", OleDbType.DBTimeStamp);
            arParams[0].Value = DateTime.Now;
            command.Parameters.Add(arParams[0]);

            arParams[1] = new OleDbParameter("Wd_crol_eff", OleDbType.Date);
            arParams[1].Value = clientRole.Effective.ToShortDateString();
            command.Parameters.Add(arParams[1]);

            arParams[2] = new OleDbParameter("Wd_crol_exp", OleDbType.Date);
            arParams[2].Value = clientRole.Expiration.ToShortDateString();
            command.Parameters.Add(arParams[2]);

            arParams[3] = new OleDbParameter("Wf_crol_act", OleDbType.Char);
            arParams[3].Value = StringEnum.GetStringValue(clientRole.Active);
            command.Parameters.Add(arParams[3]);

            arParams[4] = new OleDbParameter("Wn_crol_id", OleDbType.Decimal);
            arParams[4].Value = clientRole.ClientRoleId;
            command.Parameters.Add(arParams[4]);

            arParams[5] = new OleDbParameter("Wn_clt_lbl_id", OleDbType.Decimal);
            //if (type == "PRIMRY")
            //    arParams[5].Value = DBNull.Value;
            //else
            //    arParams[5].Value = clientRole.ClientLabelId;
            arParams[5].Value = clientRole.ClientLabelId;
            command.Parameters.Add(arParams[5]);

            arParams[6] = new OleDbParameter("Wn_crol_acct", OleDbType.Char);
            arParams[6].Value = clientRole.Account;
            command.Parameters.Add(arParams[6]);

            arParams[7] = new OleDbParameter("Wt_crol_sys_id", OleDbType.Char);
            //if (type == "PRSPT")
            //    arParams[7].Value = "CLRN";
            //else
            //    arParams[7].Value = clientRole.SystemId;
            arParams[7].Value = "CLRN";
            command.Parameters.Add(arParams[7]);

            arParams[8] = new OleDbParameter("Wn_clt_id", OleDbType.Decimal);
            arParams[8].Value = clientRole.ClientId;
            command.Parameters.Add(arParams[8]);

            arParams[9] = new OleDbParameter("Wn_tmst_id", OleDbType.Decimal);
            arParams[9].Value = clientRole.TypeMasterId;
            command.Parameters.Add(arParams[9]);

            // I = insert. U = update
            arParams[10] = new OleDbParameter("Wc_crol_hst_typ", OleDbType.Char);
            arParams[10].Value = "I";
            command.Parameters.Add(arParams[10]);
            #endregion

            try
            {
                results = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DB.Database.CloseOpenOleDbConnection(command.Connection);
            }

            if (results != 0)
                return 0;
            else
            {
                // success if reached here
                return clientRole.ClientRoleId;
            }
        }

        /// <summary>
        /// Adds a client master entry
        /// </summary>
        /// <param name="client"></param>
        /// <returns>returns the client id</returns>
        public int AddClientMaster(CobraClientMaster clientMaster)
        {
            string connectionString = Props.Default.CobraDSN4OleDb;
            #region Internal Client Master stuff
            // commenting the line belowfor the conversion process, the calling method needs to ensure GetNextOid is called.
            // the reason is other systems are just dependent on this id. they need not wait until the process is finished
            // clientMaster.ClientId = GetNextOid("CLNT", connectionString);
            #region Compute the type master id
            switch (StringEnum.GetStringValue(clientMaster.ClientType))
            {
                case "Organization":
                    clientMaster.TypeMasterId = GetTypeMasterId("Client", "ORG", connectionString);
                    break;
                case "Individual":
                    clientMaster.TypeMasterId = GetTypeMasterId("Client", "IND", connectionString);
                    break;
                default:    // default to organization
                    clientMaster.TypeMasterId = GetTypeMasterId("Client", "ORG", connectionString);
                    break;
            }
            #endregion
            clientMaster.Effective = DateTime.Now;
            clientMaster.Expiration = Common.GetDefaultExpirationDate();

            #endregion

            // assume success. sp returns 0 as success
            int results = 0;

            OleDbCommand command = GetOleDbCommand("client_master_sp", connectionString, CommandType.StoredProcedure);

            #region Parameters
            OleDbParameter[] arParams = new OleDbParameter[15];

            arParams[0] = new OleDbParameter("Wc_clt_sex", OleDbType.Char);
            arParams[0].Value = clientMaster.Sex;
            command.Parameters.Add(arParams[0]);

            arParams[1] = new OleDbParameter("Wd_clt_bk", OleDbType.DBTimeStamp);
            arParams[1].Value = DateTime.Now;
            command.Parameters.Add(arParams[1]);

            arParams[2] = new OleDbParameter("Wd_clt_eff", OleDbType.Date);
            arParams[2].Value = clientMaster.Effective.ToShortDateString();
            command.Parameters.Add(arParams[2]);

            arParams[3] = new OleDbParameter("Wd_clt_exp", OleDbType.Date);
            if (clientMaster.Expiration == DateTime.MinValue)
                arParams[3].Value = DBNull.Value;
            else
                clientMaster.Expiration.ToShortDateString();
            command.Parameters.Add(arParams[3]);

            arParams[4] = new OleDbParameter("Wf_clt_act", OleDbType.Char);
            arParams[4].Value = StringEnum.GetStringValue(clientMaster.Active);
            command.Parameters.Add(arParams[4]);

            arParams[5] = new OleDbParameter("Wn_clt_id", OleDbType.Decimal);
            arParams[5].Value = clientMaster.ClientId;
            command.Parameters.Add(arParams[5]);

            arParams[6] = new OleDbParameter("Wt_clt_nam_pre", OleDbType.Char);
            arParams[6].Value = clientMaster.Prefix;
            command.Parameters.Add(arParams[6]);

            arParams[7] = new OleDbParameter("Wt_clt_nam_fst", OleDbType.Char);
            arParams[7].Value = clientMaster.FirstName;
            command.Parameters.Add(arParams[7]);

            arParams[8] = new OleDbParameter("Wt_clt_nam_lst", OleDbType.Char);
            arParams[8].Value = clientMaster.LastName;
            command.Parameters.Add(arParams[8]);

            arParams[9] = new OleDbParameter("Wt_clt_nam_mid", OleDbType.Char);
            arParams[9].Value = clientMaster.MiddleName;
            command.Parameters.Add(arParams[9]);

            arParams[10] = new OleDbParameter("Wt_clt_nam_suf", OleDbType.Char);
            arParams[10].Value = clientMaster.Suffix;
            command.Parameters.Add(arParams[10]);

            arParams[11] = new OleDbParameter("Wn_tmst_id", OleDbType.Decimal);
            arParams[11].Value = clientMaster.TypeMasterId;
            command.Parameters.Add(arParams[11]);

            arParams[12] = new OleDbParameter("Wn_clt_tax", OleDbType.Decimal);
            arParams[12].Value = clientMaster.Tax;
            command.Parameters.Add(arParams[12]);

            arParams[13] = new OleDbParameter("Wc_clt_tax_typ", OleDbType.Char);
            arParams[13].Value = clientMaster.TaxType;
            command.Parameters.Add(arParams[13]);

            // I = insert. U = update
            arParams[14] = new OleDbParameter("Wc_clt_hst_typ", OleDbType.Char);
            arParams[14].Value = "I";
            command.Parameters.Add(arParams[14]);
            #endregion

            try
            {
                results = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DB.Database.CloseOpenOleDbConnection(command.Connection);
            }
            // if sp returns failure, we return failure too (0).
            if (results != 0)
                return 0;
            else
            {
                //CobraClientMaster addedMaster = GetClientMasterByClientId(clientMaster.ClientId, connectionString);
                //return addedMaster.ClientId;
                return clientMaster.ClientId;
            }
        }

        public static int UpdateClientMaster(int id, int taxId)
        {
            string connectionString = Props.Default.CobraDSN4OleDb;

            CobraClientMaster client = GetClientMasterByClientId(id, connectionString);
            client.Tax = taxId;
            return UpdateClientMaster(client, connectionString);
        }

        private static int UpdateClientMaster(CobraClientMaster client, string connectionString)
        {
            int results = 0;

            OleDbCommand command = GetOleDbCommand("client_master_sp", connectionString, CommandType.StoredProcedure);

            #region Parameters
            OleDbParameter[] arParams = new OleDbParameter[15];

            arParams[0] = new OleDbParameter("Wc_clt_sex", OleDbType.Char);
            arParams[0].Value = client.Sex;
            command.Parameters.Add(arParams[0]);

            arParams[1] = new OleDbParameter("Wd_clt_bk", OleDbType.DBTimeStamp);
            arParams[1].Value = DateTime.Now;
            command.Parameters.Add(arParams[1]);

            arParams[2] = new OleDbParameter("Wd_clt_eff", OleDbType.Date);
            arParams[2].Value = client.Effective.ToShortDateString();
            command.Parameters.Add(arParams[2]);

            arParams[3] = new OleDbParameter("Wd_clt_exp", OleDbType.Date);
            if (client.Expiration == DateTime.MinValue)
                arParams[3].Value = DBNull.Value;
            else
                client.Expiration.ToShortDateString();
            command.Parameters.Add(arParams[3]);

            arParams[4] = new OleDbParameter("Wf_clt_act", OleDbType.Char);
            arParams[4].Value = StringEnum.GetStringValue(client.Active);
            command.Parameters.Add(arParams[4]);

            arParams[5] = new OleDbParameter("Wn_clt_id", OleDbType.Decimal);
            arParams[5].Value = client.ClientId;
            command.Parameters.Add(arParams[5]);

            arParams[6] = new OleDbParameter("Wt_clt_nam_pre", OleDbType.Char);
            arParams[6].Value = client.Prefix;
            command.Parameters.Add(arParams[6]);

            arParams[7] = new OleDbParameter("Wt_clt_nam_fst", OleDbType.Char);
            arParams[7].Value = client.FirstName;
            command.Parameters.Add(arParams[7]);

            arParams[8] = new OleDbParameter("Wt_clt_nam_lst", OleDbType.Char);
            arParams[8].Value = client.LastName;
            command.Parameters.Add(arParams[8]);

            arParams[9] = new OleDbParameter("Wt_clt_nam_mid", OleDbType.Char);
            arParams[9].Value = client.MiddleName;
            command.Parameters.Add(arParams[9]);

            arParams[10] = new OleDbParameter("Wt_clt_nam_suf", OleDbType.Char);
            arParams[10].Value = client.Suffix;
            command.Parameters.Add(arParams[10]);

            arParams[11] = new OleDbParameter("Wn_tmst_id", OleDbType.Decimal);
            arParams[11].Value = client.TypeMasterId;
            command.Parameters.Add(arParams[11]);

            arParams[12] = new OleDbParameter("Wn_clt_tax", OleDbType.Decimal);
            arParams[12].Value = client.Tax;
            command.Parameters.Add(arParams[12]);

            arParams[13] = new OleDbParameter("Wc_clt_tax_typ", OleDbType.Char);
            arParams[13].Value = client.TaxType;
            command.Parameters.Add(arParams[13]);

            // I = insert. U = update
            arParams[14] = new OleDbParameter("Wc_clt_hst_typ", OleDbType.Char);
            arParams[14].Value = "U";
            command.Parameters.Add(arParams[14]);
            #endregion

            try
            {
                results = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DB.Database.CloseOpenOleDbConnection(command.Connection);
            }

            return results;
        }

        private static CobraClientMaster GetClientMasterByClientId(int clientId, string connectionString)
        {
            CobraClientMaster cobraClient = new CobraClientMaster();

            OleDbCommand command = GetOleDbCommand("select * from client_master where n_clt_id = ?",
                connectionString, CommandType.Text);

            #region Parameters
            OleDbParameter[] arParams = new OleDbParameter[2];

            arParams[0] = new OleDbParameter("Wn_clt_id", OleDbType.Decimal);
            arParams[0].Value = clientId;
            command.Parameters.Add(arParams[0]);
            #endregion

            DataSet clientDS = new DataSet();

            OleDbDataAdapter da = new OleDbDataAdapter(command);

            try
            {
                da.Fill(clientDS);

                if (clientDS.Tables[0] != null)
                {
                    if (clientDS.Tables[0].Rows.Count > 0)
                    {
                        #region Populate Our CobraClient object
                        cobraClient.Sex = clientDS.Tables[0].Rows[0]["c_clt_sex"].ToString().Trim();

                        if (clientDS.Tables[0].Rows[0]["d_clt_eff"] != DBNull.Value)
                        {
                            cobraClient.Effective = Convert.ToDateTime(clientDS.Tables[0].Rows[0]["d_clt_eff"]);
                        }

                        if (clientDS.Tables[0].Rows[0]["d_clt_exp"] != DBNull.Value)
                        {
                            cobraClient.Expiration = Convert.ToDateTime(clientDS.Tables[0].Rows[0]["d_clt_exp"]);
                        }

                        switch (clientDS.Tables[0].Rows[0]["f_clt_act"].ToString())
                        {
                            case "A":
                                cobraClient.Active = ActiveFlag.Active;
                                break;
                            case "I":
                                cobraClient.Active = ActiveFlag.Inactive;
                                break;
                        }

                        cobraClient.ClientId = Convert.ToInt32(clientDS.Tables[0].Rows[0]["n_clt_id"]);

                        cobraClient.Prefix = clientDS.Tables[0].Rows[0]["t_clt_nam_pre"].ToString().Trim();
                        cobraClient.FirstName = clientDS.Tables[0].Rows[0]["t_clt_nam_fst"].ToString().Trim();
                        cobraClient.LastName = clientDS.Tables[0].Rows[0]["t_clt_nam_lst"].ToString().Trim();
                        cobraClient.MiddleName = clientDS.Tables[0].Rows[0]["t_clt_nam_mid"].ToString().Trim();
                        cobraClient.Suffix = clientDS.Tables[0].Rows[0]["t_clt_nam_suf"].ToString().Trim();

                        cobraClient.TypeMasterId = Convert.ToInt32(clientDS.Tables[0].Rows[0]["n_tmst_id"]);

                        cobraClient.Tax = Convert.ToInt32(clientDS.Tables[0].Rows[0]["n_clt_tax"]);

                        cobraClient.TaxType = clientDS.Tables[0].Rows[0]["c_clt_tax_typ"].ToString().Trim();
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DB.Database.CloseOpenOleDbConnection(command.Connection);
            }

            return cobraClient;
        }

        /// <summary>
        /// Calls the get_next_oid stored procedure
        /// This is a table that contains the next OID to use for a given table.
        /// To get the next OID, you need to pass in a identifier for the tables you'll be adding records to.
        /// Here are the ones you'll be concerned with:
        /// CLNT = code for client_master
        /// CROLE = code for client_role
        /// CLBL = code for client_label
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="connectionString"></param>
        /// <returns>Next Object Id</returns>
        private int GetNextOid(string identifier, string connectionString)
        {
            int nextOid = 0;

            OleDbCommand command = GetOleDbCommand("get_next_oid", connectionString, CommandType.StoredProcedure);

            #region Parameters
            OleDbParameter[] arParams = new OleDbParameter[1];

            arParams[0] = new OleDbParameter("Xc_oid_cl_abr", OleDbType.Char);
            arParams[0].Value = identifier;
            command.Parameters.Add(arParams[0]);
            #endregion

            DataSet nextOidDs = new DataSet();

            OleDbDataAdapter da = new OleDbDataAdapter(command);

            try
            {
                da.Fill(nextOidDs);

                if (nextOidDs.Tables[0] != null)
                {
                    if (nextOidDs.Tables[0].Rows.Count > 0)
                    {
                        // Check for sqlcode errror
                        // 0 indicates success
                        if (Convert.ToInt32(nextOidDs.Tables[0].Rows[0][1]) == 0)
                        {
                            nextOid = Convert.ToInt32(nextOidDs.Tables[0].Rows[0][0]);
                            if (Properties.Settings.Default.LogOIDs)
                            {
                                BTS.LogFramework.LogCentral.Current.LogInfo(string.Format("returned code from get_next_oid for {0} is {1}",
                                     identifier, nextOidDs.Tables[0].Rows[0][0]));
                            }
                        }
                        else
                        {
                            BTS.LogFramework.LogCentral.Current.LogError(string.Format("unsuccessful returned code from get_next_oid for {0} is {1}",
                                identifier, nextOidDs.Tables[0].Rows[0][1].ToString()));
                            nextOid = -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DB.Database.CloseOpenOleDbConnection(command.Connection);
            }

            return nextOid;
        }

        internal int GetNextOidForMaster()
        {
            string connectionString = Props.Default.CobraDSN4OleDb;

            return GetNextOid("CLNT", connectionString);
        }


        /// <summary>
        /// Retrieve the type master Id from for any given object and type.
        /// </summary>
        /// <param name="typeMasterObject"></param>
        /// <param name="typeMasterType"></param>
        /// <param name="connectionString"></param>
        /// <returns>TypeMasterId</returns>
        private static int GetTypeMasterId(string typeMasterObject, string typeMasterType, string connectionString)
        {
            int typeMasterId = 0;

            OleDbCommand command = GetOleDbCommand("select n_tmst_id from type_master where t_tmst_obj = ? and t_tmst_typ = ?",
                connectionString, CommandType.Text);

            #region Parameters
            OleDbParameter[] arParams = new OleDbParameter[2];

            arParams[0] = new OleDbParameter("Wt_tmst_obj", OleDbType.Char);
            arParams[0].Value = typeMasterObject;
            command.Parameters.Add(arParams[0]);

            arParams[1] = new OleDbParameter("Wt_tmst_typ", OleDbType.Char);
            arParams[1].Value = typeMasterType;
            command.Parameters.Add(arParams[1]);
            #endregion

            DataSet typeMasterDS = new DataSet();

            OleDbDataAdapter da = new OleDbDataAdapter(command);

            try
            {
                da.Fill(typeMasterDS);

                if (typeMasterDS.Tables[0] != null)
                {
                    if (typeMasterDS.Tables[0].Rows.Count > 0)
                    {
                        typeMasterId = Convert.ToInt32(typeMasterDS.Tables[0].Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DB.Database.CloseOpenOleDbConnection(command.Connection);
            }

            return typeMasterId;
        }

        private static OleDbCommand GetOleDbCommand(string commandText, string connectionString, CommandType commandType)
        {
            OleDbCommand command = new OleDbCommand(commandText);

            command.Connection = DB.Database.GetOpenOleDbConnection(connectionString);
            command.CommandTimeout = 60;
            command.CommandType = commandType;

            return command;
        }

        public static bool ClientExists(int clientId)
        {
            // non-positive client id return false
            if (clientId <= 0)
                return false;
            string connectionString = Props.Default.CobraDSN4OleDb;

            OleDbCommand command = GetOleDbCommand(string.Format("select * from client_master where n_clt_id = {0}",
                clientId),
                connectionString, CommandType.Text);
            object result = command.ExecuteScalar();
            return result != null;
        }

        public static bool PolicyExists(int clientId, string policyNumber)
        {
            if (clientId <= 0 || string.IsNullOrEmpty(policyNumber))
                return false;
            string connectionString = Props.Default.CobraDSN4OleDb;

            OleDbCommand command = GetOleDbCommand(string.Format("select * from client_role where n_clt_id = {0} and n_crol_acct = '{1}'",
                clientId, policyNumber),
                connectionString, CommandType.Text);
            object result = command.ExecuteScalar();
            return result != null;
        }
    } 

    //public class CobraOleDbControllerBackup
    //{
    //    private CobraOleDbController()
    //    { }

    //    public static int AddClientLabel(CobraClientLabel clientLabel)
    //    {
    //        #region Internal Client Label stuff
    //        string connectionString = Props.Default.CobraDSN4OleDb;
    //        clientLabel.ClientLabelId = GetNextOid("CLBL", connectionString);
    //        #endregion

    //        // assume success. sp returns 0 as success
    //        int results = 0;

    //        OleDbCommand command = GetOleDbCommand("client_label_sp", connectionString, CommandType.StoredProcedure);

    //        #region Parameters
    //        OleDbParameter[] arParams = new OleDbParameter[8];

    //        arParams[0] = new OleDbParameter("Wn_clt_lbl_id", OleDbType.Decimal);
    //        arParams[0].Value = clientLabel.ClientLabelId;
    //        command.Parameters.Add(arParams[0]);

    //        arParams[1] = new OleDbParameter("Wt_aplus_lbl_1", OleDbType.Char);
    //        arParams[1].Value = clientLabel.Label1;
    //        command.Parameters.Add(arParams[1]);

    //        arParams[2] = new OleDbParameter("Wt_aplus_lbl_2", OleDbType.Char);
    //        arParams[2].Value = clientLabel.Label2;
    //        command.Parameters.Add(arParams[2]);

    //        arParams[3] = new OleDbParameter("Wt_aplus_lbl_3", OleDbType.Char);
    //        arParams[3].Value = clientLabel.Label3;
    //        command.Parameters.Add(arParams[3]);

    //        arParams[4] = new OleDbParameter("Wt_aplus_lbl_4", OleDbType.Char);
    //        arParams[4].Value = clientLabel.Label4;
    //        command.Parameters.Add(arParams[4]);

    //        arParams[5] = new OleDbParameter("Wt_aplus_city", OleDbType.Char);
    //        arParams[5].Value = clientLabel.City;
    //        command.Parameters.Add(arParams[5]);

    //        arParams[6] = new OleDbParameter("Wc_aplus_st", OleDbType.Char);
    //        arParams[6].Value = clientLabel.State;
    //        command.Parameters.Add(arParams[6]);

    //        arParams[7] = new OleDbParameter("Wc_aplus_zip", OleDbType.Char);
    //        arParams[7].Value = clientLabel.Zip;
    //        command.Parameters.Add(arParams[7]);
    //        #endregion

    //        try
    //        {
    //            results = Convert.ToInt32(command.ExecuteScalar());
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //        finally
    //        {
    //            DB.Database.CloseOpenOleDbConnection(command.Connection);
    //        }
    //        if (results != 0)
    //            return 0;
    //        else
    //        {
    //            // success if reached here
    //            return clientLabel.ClientLabelId;
    //        }
    //    }


    //    internal static int AddClientSearchLabel(string label, int labelId, string masterType)
    //    {
    //        int results = 0;

    //        string connectionString = Props.Default.CobraDSN4OleDb;

    //        OleDbCommand command = GetOleDbCommand("INSERT INTO client_label_srch VALUES (?, ?, ?)", connectionString, CommandType.Text);

    //        OleDbParameter ifxParam = new OleDbParameter("t_aplus_lbl_nam", OleDbType.Char);
    //        ifxParam.Value = label != null ? label.ToUpper() : string.Empty;
    //        command.Parameters.Add(ifxParam);

    //        ifxParam = new OleDbParameter("n_clt_lbl_id", OleDbType.Decimal);
    //        ifxParam.Value = labelId;
    //        command.Parameters.Add(ifxParam);

    //        ifxParam = new OleDbParameter("n_tmst_id", OleDbType.Decimal);
    //        ifxParam.Value = GetTypeMasterId("Role", masterType, connectionString);
    //        command.Parameters.Add(ifxParam);
    //        try
    //        {
    //            results = Convert.ToInt32(command.ExecuteScalar());
    //        }
    //        catch (Exception)
    //        {
    //            results = -1;
    //        }
    //        finally
    //        {
    //            DB.Database.CloseOpenOleDbConnection(command.Connection);
    //        }
    //        if (results != 0)
    //            return 0;
    //        else
    //        {
    //            // success if reached here
    //            return labelId;
    //        }
    //    }

    //    public static int AddClientRole(CobraClientRole clientRole, string type)
    //    {

    //        string connectionString = Props.Default.CobraDSN4OleDb;
    //        clientRole.ClientRoleId = GetNextOid("CROLE", connectionString);
    //        clientRole.TypeMasterId = GetTypeMasterId("Role", type, connectionString);

    //        // assume success. sp returns 0 as success
    //        int results = 0;

    //        OleDbCommand command = GetOleDbCommand("client_role_sp", connectionString, CommandType.StoredProcedure);

    //        #region Parameters
    //        OleDbParameter[] arParams = new OleDbParameter[11];

    //        arParams[0] = new OleDbParameter("Wd_crol_bk", OleDbType.DBTimeStamp);
    //        arParams[0].Value = DateTime.Now;
    //        command.Parameters.Add(arParams[0]);

    //        arParams[1] = new OleDbParameter("Wd_crol_eff", OleDbType.Date);
    //        arParams[1].Value = clientRole.Effective.ToShortDateString();
    //        command.Parameters.Add(arParams[1]);

    //        arParams[2] = new OleDbParameter("Wd_crol_exp", OleDbType.Date);
    //        arParams[2].Value = clientRole.Expiration.ToShortDateString();
    //        command.Parameters.Add(arParams[2]);

    //        arParams[3] = new OleDbParameter("Wf_crol_act", OleDbType.Char);
    //        arParams[3].Value = StringEnum.GetStringValue(clientRole.Active);
    //        command.Parameters.Add(arParams[3]);

    //        arParams[4] = new OleDbParameter("Wn_crol_id", OleDbType.Decimal);
    //        arParams[4].Value = clientRole.ClientRoleId;
    //        command.Parameters.Add(arParams[4]);

    //        arParams[5] = new OleDbParameter("Wn_clt_lbl_id", OleDbType.Decimal);
    //        if (type == "PRIMRY")
    //            arParams[5].Value = DBNull.Value;
    //        else
    //            arParams[5].Value = clientRole.ClientLabelId;
    //        command.Parameters.Add(arParams[5]);

    //        arParams[6] = new OleDbParameter("Wn_crol_acct", OleDbType.Char);
    //        arParams[6].Value = clientRole.Account;
    //        command.Parameters.Add(arParams[6]);

    //        arParams[7] = new OleDbParameter("Wt_crol_sys_id", OleDbType.Char);
    //        if (type == "PRSPT")
    //            arParams[7].Value = "CLRN";
    //        else
    //            arParams[7].Value = clientRole.SystemId;
    //        command.Parameters.Add(arParams[7]);

    //        arParams[8] = new OleDbParameter("Wn_clt_id", OleDbType.Decimal);
    //        arParams[8].Value = clientRole.ClientId;
    //        command.Parameters.Add(arParams[8]);

    //        arParams[9] = new OleDbParameter("Wn_tmst_id", OleDbType.Decimal);
    //        arParams[9].Value = clientRole.TypeMasterId;
    //        command.Parameters.Add(arParams[9]);

    //        // I = insert. U = update
    //        arParams[10] = new OleDbParameter("Wc_crol_hst_typ", OleDbType.Char);
    //        arParams[10].Value = "I";
    //        command.Parameters.Add(arParams[10]);
    //        #endregion

    //        try
    //        {
    //            results = Convert.ToInt32(command.ExecuteScalar());
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //        finally
    //        {
    //            DB.Database.CloseOpenOleDbConnection(command.Connection);
    //        }

    //        if (results != 0)
    //            return 0;
    //        else
    //        {
    //            // success if reached here
    //            return clientRole.ClientRoleId;
    //        }
    //    }

    //    /// <summary>
    //    /// Adds a client master entry
    //    /// </summary>
    //    /// <param name="client"></param>
    //    /// <returns>returns the client id</returns>
    //    public static int AddClientMaster(CobraClientMaster clientMaster)
    //    {
    //        string connectionString = Props.Default.CobraDSN4OleDb;
    //        #region Internal Client Master stuff
    //        // commenting the line belowfor the conversion process, the calling method needs to ensure GetNextOid is called.
    //        // the reason is other systems are just dependent on this id. they need not wait until the process is finished
    //        // clientMaster.ClientId = GetNextOid("CLNT", connectionString);
    //        #region Compute the type master id
    //        switch (StringEnum.GetStringValue(clientMaster.ClientType))
    //        {
    //            case "Organization":
    //                clientMaster.TypeMasterId = GetTypeMasterId("Client", "ORG", connectionString);
    //                break;
    //            case "Individual":
    //                clientMaster.TypeMasterId = GetTypeMasterId("Client", "IND", connectionString);
    //                break;
    //            default:    // default to organization
    //                clientMaster.TypeMasterId = GetTypeMasterId("Client", "ORG", connectionString);
    //                break;
    //        }
    //        #endregion
    //        clientMaster.Effective = DateTime.Now;
    //        clientMaster.Expiration = Common.GetDefaultExpirationDate();

    //        #endregion

    //        // assume success. sp returns 0 as success
    //        int results = 0;

    //        OleDbCommand command = GetOleDbCommand("client_master_sp", connectionString, CommandType.StoredProcedure);

    //        #region Parameters
    //        OleDbParameter[] arParams = new OleDbParameter[15];

    //        arParams[0] = new OleDbParameter("Wc_clt_sex", OleDbType.Char);
    //        arParams[0].Value = clientMaster.Sex;
    //        command.Parameters.Add(arParams[0]);

    //        arParams[1] = new OleDbParameter("Wd_clt_bk", OleDbType.DBTimeStamp);
    //        arParams[1].Value = DateTime.Now;
    //        command.Parameters.Add(arParams[1]);

    //        arParams[2] = new OleDbParameter("Wd_clt_eff", OleDbType.Date);
    //        arParams[2].Value = clientMaster.Effective.ToShortDateString();
    //        command.Parameters.Add(arParams[2]);

    //        arParams[3] = new OleDbParameter("Wd_clt_exp", OleDbType.Date);
    //        if (clientMaster.Expiration == DateTime.MinValue)
    //            arParams[3].Value = DBNull.Value;
    //        else
    //            clientMaster.Expiration.ToShortDateString();
    //        command.Parameters.Add(arParams[3]);

    //        arParams[4] = new OleDbParameter("Wf_clt_act", OleDbType.Char);
    //        arParams[4].Value = StringEnum.GetStringValue(clientMaster.Active);
    //        command.Parameters.Add(arParams[4]);

    //        arParams[5] = new OleDbParameter("Wn_clt_id", OleDbType.Decimal);
    //        arParams[5].Value = clientMaster.ClientId;
    //        command.Parameters.Add(arParams[5]);

    //        arParams[6] = new OleDbParameter("Wt_clt_nam_pre", OleDbType.Char);
    //        arParams[6].Value = clientMaster.Prefix;
    //        command.Parameters.Add(arParams[6]);

    //        arParams[7] = new OleDbParameter("Wt_clt_nam_fst", OleDbType.Char);
    //        arParams[7].Value = clientMaster.FirstName;
    //        command.Parameters.Add(arParams[7]);

    //        arParams[8] = new OleDbParameter("Wt_clt_nam_lst", OleDbType.Char);
    //        arParams[8].Value = clientMaster.LastName;
    //        command.Parameters.Add(arParams[8]);

    //        arParams[9] = new OleDbParameter("Wt_clt_nam_mid", OleDbType.Char);
    //        arParams[9].Value = clientMaster.MiddleName;
    //        command.Parameters.Add(arParams[9]);

    //        arParams[10] = new OleDbParameter("Wt_clt_nam_suf", OleDbType.Char);
    //        arParams[10].Value = clientMaster.Suffix;
    //        command.Parameters.Add(arParams[10]);

    //        arParams[11] = new OleDbParameter("Wn_tmst_id", OleDbType.Decimal);
    //        arParams[11].Value = clientMaster.TypeMasterId;
    //        command.Parameters.Add(arParams[11]);

    //        arParams[12] = new OleDbParameter("Wn_clt_tax", OleDbType.Decimal);
    //        arParams[12].Value = clientMaster.Tax;
    //        command.Parameters.Add(arParams[12]);

    //        arParams[13] = new OleDbParameter("Wc_clt_tax_typ", OleDbType.Char);
    //        arParams[13].Value = clientMaster.TaxType;
    //        command.Parameters.Add(arParams[13]);

    //        // I = insert. U = update
    //        arParams[14] = new OleDbParameter("Wc_clt_hst_typ", OleDbType.Char);
    //        arParams[14].Value = "I";
    //        command.Parameters.Add(arParams[14]);
    //        #endregion

    //        try
    //        {
    //            results = Convert.ToInt32(command.ExecuteScalar());
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //        finally
    //        {
    //            DB.Database.CloseOpenOleDbConnection(command.Connection);
    //        }
    //        // if sp returns failure, we return failure too (0).
    //        if (results != 0)
    //            return 0;
    //        else
    //        {
    //            CobraClientMaster addedMaster = GetClientMasterByClientId(clientMaster.ClientId, connectionString);
    //            return addedMaster.ClientId;
    //        }
    //    }

    //    public static int UpdateClientMaster(int id, int taxId)
    //    {
    //        string connectionString = Props.Default.CobraDSN4OleDb;

    //        CobraClientMaster client = GetClientMasterByClientId(id, connectionString);
    //        client.Tax = taxId;
    //        return UpdateClientMaster(client, connectionString);
    //    }

    //    private static int UpdateClientMaster(CobraClientMaster client, string connectionString)
    //    {
    //        int results = 0;

    //        OleDbCommand command = GetOleDbCommand("client_master_sp", connectionString, CommandType.StoredProcedure);

    //        #region Parameters
    //        OleDbParameter[] arParams = new OleDbParameter[15];

    //        arParams[0] = new OleDbParameter("Wc_clt_sex", OleDbType.Char);
    //        arParams[0].Value = client.Sex;
    //        command.Parameters.Add(arParams[0]);

    //        arParams[1] = new OleDbParameter("Wd_clt_bk", OleDbType.Date);
    //        arParams[1].Value = DateTime.Now;
    //        command.Parameters.Add(arParams[1]);

    //        arParams[2] = new OleDbParameter("Wd_clt_eff", OleDbType.Date);
    //        arParams[2].Value = client.Effective.ToShortDateString();
    //        command.Parameters.Add(arParams[2]);

    //        arParams[3] = new OleDbParameter("Wd_clt_exp", OleDbType.Date);
    //        if (client.Expiration == DateTime.MinValue)
    //            arParams[3].Value = DBNull.Value;
    //        else
    //            client.Expiration.ToShortDateString();
    //        command.Parameters.Add(arParams[3]);

    //        arParams[4] = new OleDbParameter("Wf_clt_act", OleDbType.Char);
    //        arParams[4].Value = StringEnum.GetStringValue(client.Active);
    //        command.Parameters.Add(arParams[4]);

    //        arParams[5] = new OleDbParameter("Wn_clt_id", OleDbType.Decimal);
    //        arParams[5].Value = client.ClientId;
    //        command.Parameters.Add(arParams[5]);

    //        arParams[6] = new OleDbParameter("Wt_clt_nam_pre", OleDbType.Char);
    //        arParams[6].Value = client.Prefix;
    //        command.Parameters.Add(arParams[6]);

    //        arParams[7] = new OleDbParameter("Wt_clt_nam_fst", OleDbType.Char);
    //        arParams[7].Value = client.FirstName;
    //        command.Parameters.Add(arParams[7]);

    //        arParams[8] = new OleDbParameter("Wt_clt_nam_lst", OleDbType.Char);
    //        arParams[8].Value = client.LastName;
    //        command.Parameters.Add(arParams[8]);

    //        arParams[9] = new OleDbParameter("Wt_clt_nam_mid", OleDbType.Char);
    //        arParams[9].Value = client.MiddleName;
    //        command.Parameters.Add(arParams[9]);

    //        arParams[10] = new OleDbParameter("Wt_clt_nam_suf", OleDbType.Char);
    //        arParams[10].Value = client.Suffix;
    //        command.Parameters.Add(arParams[10]);

    //        arParams[11] = new OleDbParameter("Wn_tmst_id", OleDbType.Decimal);
    //        arParams[11].Value = client.TypeMasterId;
    //        command.Parameters.Add(arParams[11]);

    //        arParams[12] = new OleDbParameter("Wn_clt_tax", OleDbType.Decimal);
    //        arParams[12].Value = client.Tax;
    //        command.Parameters.Add(arParams[12]);

    //        arParams[13] = new OleDbParameter("Wc_clt_tax_typ", OleDbType.Char);
    //        arParams[13].Value = client.TaxType;
    //        command.Parameters.Add(arParams[13]);

    //        // I = insert. U = update
    //        arParams[14] = new OleDbParameter("Wc_clt_hst_typ", OleDbType.Char);
    //        arParams[14].Value = "U";
    //        command.Parameters.Add(arParams[14]);
    //        #endregion

    //        try
    //        {
    //            results = Convert.ToInt32(command.ExecuteScalar());
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //        finally
    //        {
    //            DB.Database.CloseOpenOleDbConnection(command.Connection);
    //        }

    //        return results;
    //    }

    //    private static CobraClientMaster GetClientMasterByClientId(int clientId, string connectionString)
    //    {
    //        CobraClientMaster cobraClient = new CobraClientMaster();

    //        OleDbCommand command = GetOleDbCommand("select * from client_master where n_clt_id = ?",
    //            connectionString, CommandType.Text);

    //        #region Parameters
    //        OleDbParameter[] arParams = new OleDbParameter[2];

    //        arParams[0] = new OleDbParameter("Wn_clt_id", OleDbType.Decimal);
    //        arParams[0].Value = clientId;
    //        command.Parameters.Add(arParams[0]);
    //        #endregion

    //        DataSet clientDS = new DataSet();

    //        OleDbDataAdapter da = new OleDbDataAdapter(command);

    //        try
    //        {
    //            da.Fill(clientDS);

    //            if (clientDS.Tables[0] != null)
    //            {
    //                if (clientDS.Tables[0].Rows.Count > 0)
    //                {
    //                    #region Populate Our CobraClient object
    //                    cobraClient.Sex = clientDS.Tables[0].Rows[0]["c_clt_sex"].ToString().Trim();

    //                    if (clientDS.Tables[0].Rows[0]["d_clt_eff"] != DBNull.Value)
    //                    {
    //                        cobraClient.Effective = Convert.ToDateTime(clientDS.Tables[0].Rows[0]["d_clt_eff"]);
    //                    }

    //                    if (clientDS.Tables[0].Rows[0]["d_clt_exp"] != DBNull.Value)
    //                    {
    //                        cobraClient.Expiration = Convert.ToDateTime(clientDS.Tables[0].Rows[0]["d_clt_exp"]);
    //                    }

    //                    switch (clientDS.Tables[0].Rows[0]["f_clt_act"].ToString())
    //                    {
    //                        case "A":
    //                            cobraClient.Active = ActiveFlag.Active;
    //                            break;
    //                        case "I":
    //                            cobraClient.Active = ActiveFlag.Inactive;
    //                            break;
    //                    }

    //                    cobraClient.ClientId = Convert.ToInt32(clientDS.Tables[0].Rows[0]["n_clt_id"]);

    //                    cobraClient.Prefix = clientDS.Tables[0].Rows[0]["t_clt_nam_pre"].ToString().Trim();
    //                    cobraClient.FirstName = clientDS.Tables[0].Rows[0]["t_clt_nam_fst"].ToString().Trim();
    //                    cobraClient.LastName = clientDS.Tables[0].Rows[0]["t_clt_nam_lst"].ToString().Trim();
    //                    cobraClient.MiddleName = clientDS.Tables[0].Rows[0]["t_clt_nam_mid"].ToString().Trim();
    //                    cobraClient.Suffix = clientDS.Tables[0].Rows[0]["t_clt_nam_suf"].ToString().Trim();

    //                    cobraClient.TypeMasterId = Convert.ToInt32(clientDS.Tables[0].Rows[0]["n_tmst_id"]);

    //                    cobraClient.Tax = Convert.ToInt32(clientDS.Tables[0].Rows[0]["n_clt_tax"]);

    //                    cobraClient.TaxType = clientDS.Tables[0].Rows[0]["c_clt_tax_typ"].ToString().Trim();
    //                    #endregion
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //        finally
    //        {
    //            DB.Database.CloseOpenOleDbConnection(command.Connection);
    //        }

    //        return cobraClient;
    //    }

    //    /// <summary>
    //    /// Calls the get_next_oid stored procedure
    //    /// This is a table that contains the next OID to use for a given table.
    //    /// To get the next OID, you need to pass in a identifier for the tables you'll be adding records to.
    //    /// Here are the ones you'll be concerned with:
    //    /// CLNT = code for client_master
    //    /// CROLE = code for client_role
    //    /// CLBL = code for client_label
    //    /// </summary>
    //    /// <param name="identifier"></param>
    //    /// <param name="connectionString"></param>
    //    /// <returns>Next Object Id</returns>
    //    private static int GetNextOid(string identifier, string connectionString)
    //    {
    //        int nextOid = 0;

    //        OleDbCommand command = GetOleDbCommand("get_next_oid", connectionString, CommandType.StoredProcedure);

    //        #region Parameters
    //        OleDbParameter[] arParams = new OleDbParameter[1];

    //        arParams[0] = new OleDbParameter("Xc_oid_cl_abr", OleDbType.Char);
    //        arParams[0].Value = identifier;
    //        command.Parameters.Add(arParams[0]);
    //        #endregion

    //        DataSet nextOidDs = new DataSet();

    //        OleDbDataAdapter da = new OleDbDataAdapter(command);

    //        try
    //        {
    //            da.Fill(nextOidDs);

    //            if (nextOidDs.Tables[0] != null)
    //            {
    //                if (nextOidDs.Tables[0].Rows.Count > 0)
    //                {
    //                    // Check for sqlcode errror
    //                    // 0 indicates success
    //                    if (Convert.ToInt32(nextOidDs.Tables[0].Rows[0][1]) == 0)
    //                    {
    //                        nextOid = Convert.ToInt32(nextOidDs.Tables[0].Rows[0][0]);
    //                    }
    //                    else
    //                    {
    //                        nextOid = -1;
    //                    }
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //        finally
    //        {
    //            DB.Database.CloseOpenOleDbConnection(command.Connection);
    //        }

    //        return nextOid;
    //    }

    //    internal static int GetNextOidForMaster()
    //    {
    //        string connectionString = Props.Default.CobraDSN4OleDb;

    //        return GetNextOid("CLNT", connectionString);
    //    }


    //    /// <summary>
    //    /// Retrieve the type master Id from for any given object and type.
    //    /// </summary>
    //    /// <param name="typeMasterObject"></param>
    //    /// <param name="typeMasterType"></param>
    //    /// <param name="connectionString"></param>
    //    /// <returns>TypeMasterId</returns>
    //    private static int GetTypeMasterId(string typeMasterObject, string typeMasterType, string connectionString)
    //    {
    //        int typeMasterId = 0;

    //        OleDbCommand command = GetOleDbCommand("select n_tmst_id from type_master where t_tmst_obj = ? and t_tmst_typ = ?",
    //            connectionString, CommandType.Text);

    //        #region Parameters
    //        OleDbParameter[] arParams = new OleDbParameter[2];

    //        arParams[0] = new OleDbParameter("Wt_tmst_obj", OleDbType.Char);
    //        arParams[0].Value = typeMasterObject;
    //        command.Parameters.Add(arParams[0]);

    //        arParams[1] = new OleDbParameter("Wt_tmst_typ", OleDbType.Char);
    //        arParams[1].Value = typeMasterType;
    //        command.Parameters.Add(arParams[1]);
    //        #endregion

    //        DataSet typeMasterDS = new DataSet();

    //        OleDbDataAdapter da = new OleDbDataAdapter(command);

    //        try
    //        {
    //            da.Fill(typeMasterDS);

    //            if (typeMasterDS.Tables[0] != null)
    //            {
    //                if (typeMasterDS.Tables[0].Rows.Count > 0)
    //                {
    //                    typeMasterId = Convert.ToInt32(typeMasterDS.Tables[0].Rows[0][0]);
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            throw ex;
    //        }
    //        finally
    //        {
    //            DB.Database.CloseOpenOleDbConnection(command.Connection);
    //        }

    //        return typeMasterId;
    //    }

    //    private static OleDbCommand GetOleDbCommand(string commandText, string connectionString, CommandType commandType)
    //    {
    //        OleDbCommand command = new OleDbCommand(commandText);

    //        command.Connection = DB.Database.GetOpenOleDbConnection(connectionString);
    //        command.CommandTimeout = 60;
    //        command.CommandType = commandType;

    //        return command;
    //    }

    //    public static bool ClientExists(int clientId)
    //    {
    //        // non-positive client id return false
    //        if (clientId <= 0)
    //            return false;
    //        string connectionString = Props.Default.CobraDSN4OleDb;

    //        OleDbCommand command = GetOleDbCommand(string.Format("select * from client_master where n_clt_id = {0}",
    //            clientId),
    //            connectionString, CommandType.Text);
    //        object result = command.ExecuteScalar();
    //        return result != null;
    //    }

    //    public static bool PolicyExists(int clientId, string policyNumber)
    //    {
    //        if (clientId <= 0 || string.IsNullOrEmpty(policyNumber))
    //            return false;
    //        string connectionString = Props.Default.CobraDSN4OleDb;

    //        OleDbCommand command = GetOleDbCommand(string.Format("select * from client_role where n_clt_id = {0} and n_crol_acct = '{1}'",
    //            clientId, policyNumber),
    //            connectionString, CommandType.Text);
    //        object result = command.ExecuteScalar();
    //        return result != null;
    //    }
    //} 
}
