using System;
using System.Data;
using System.Configuration;


namespace BCS.CWGConversion.Cobra.Structures
{
    public class CobraClientLabel
    {
        #region private fields
        private int _clientLabelId;
        private string _clientId;


        private string _label1;
        private string _label2;
        private string _label3;
        private string _label4;
        private string _city;
        private string _state;
        private string _zip;
        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string ClientId
        {
            get { return _clientId; }
            set { _clientId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ClientLabelId
        {
            get { return this._clientLabelId; }
            set { this._clientLabelId = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Label1
        {
            get
            {
                if (this._label1 != null)
                    return this._label1;
                else
                    return "";
            }
            set
            {
                if (value != null && value.Length > 38)
                    this._label1 = value.Substring(0, 38);
                else
                    this._label1 = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Label2
        {
            get
            {
                if (this._label2 != null)
                    return this._label2;
                else
                    return "";
            }
            set
            {
                if (value != null && value.Length > 38)
                    this._label2 = value.Substring(0, 38);
                else
                    this._label2 = value;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string Label3
        {
            get
            {
                if (this._label3 != null)
                    return this._label3;
                else
                    return "";
            }
            set
            {
                if (value != null && value.Length > 38)
                    this._label3 = value.Substring(0, 38);
                else
                    this._label3 = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Label4
        {
            get
            {
                if (this._label4 != null)
                    return this._label4;
                else
                    return "";
            }
            set
            {
                if (value != null && value.Length > 38)
                    this._label4 = value.Substring(0, 38);
                else
                    this._label4 = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string City
        {
            get
            {
                if (this._city != null)
                    return this._city;
                else
                    return "";
            }
            set
            {
                if (value != null && value.Length > 25)
                    this._city = value.Substring(0, 25);
                else
                    this._city = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string State
        {
            get
            {
                if (this._state != null)
                    return this._state;
                else
                    return "";
            }
            set
            {
                if (value != null && value.Length > 2)
                    this._state = value.Substring(0, 2);
                else
                    this._state = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Zip
        {
            get
            {
                if (this._zip != null)
                    return this._zip;
                else
                    return "";
            }
            set
            {
                if (value != null && value.Length > 9)
                    this._zip = value.Substring(0, 9);
                else
                    this._zip = value;
            }
        }
        #endregion
    } 
}
