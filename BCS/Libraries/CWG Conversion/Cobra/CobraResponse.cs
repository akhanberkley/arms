using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.CWGConversion.Cobra
{
    class Response
    {
        public Response()
        {
            this.ResponseCode = ResponseCode.Failure;
            this.Reason = "Not yet known.";
            this.Data = 0;
        }
        Exception exceptionField;

        public Exception Exception
        {
            get { return exceptionField; }
            set { exceptionField = value; }
        }
        ResponseCode responseCodeField;

        public ResponseCode ResponseCode
        {
            get { return responseCodeField; }
            set { responseCodeField = value; }
        }
        string reasonField;

        public string Reason
        {
            get { return reasonField; }
            set { reasonField = value; }
        }
        object dataField;

        public object Data
        {
            get { return dataField; }
            set { dataField = value; }
        }
    }
}
