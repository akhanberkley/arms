namespace BCS.CWGConversion.Cobra.Structures
{
    using System;
    using System.Data;
    using System.Configuration;

    public class CobraClientMaster
    {
        #region fields
        private int _clientId;
        private string _prefix;
        private string _firstName;
        private string _lastName;
        private string _middleName;
        private string _suffix;
        private int _tax;
        private string _taxType;
        private string _sex;
        private ActiveFlag _active;
        private DateTime _effective;
        private DateTime _expiration;
        private int _typeMasterId;
        private ClientType _clientType;

        private CobraClientRole[] _cobraClientRoles;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public int ClientId
        {
            get { return this._clientId; }
            set { this._clientId = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Prefix
        {
            get
            {
                if (this._prefix != null)
                    return this._prefix;
                else
                    return "";
            }
            set { this._prefix = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FirstName
        {
            get
            {
                if (this._firstName != null)
                    return this._firstName;
                else
                    return "";
            }
            set { this._firstName = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastName
        {
            get
            {
                if (this._lastName != null)
                    return this._lastName;
                else
                    return "";
            }
            set { this._lastName = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MiddleName
        {
            get
            {
                if (this._middleName != null)
                    return this._middleName;
                else
                    return "";
            }
            set { this._middleName = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Suffix
        {
            get
            {
                if (this._suffix != null)
                    return this._suffix;
                else
                    return "";
            }
            set { this._suffix = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Tax
        {
            get { return this._tax; }
            set { this._tax = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TaxType
        {
            get
            {
                if (this._taxType != null)
                    return this._taxType;
                else
                    return "";
            }
            set { this._taxType = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Sex
        {
            get
            {
                if (this._sex != null)
                    return this._sex;
                else
                    return "";
            }
            set { this._sex = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public ActiveFlag Active
        {
            get { return this._active; }
            set { this._active = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Effective
        {
            get { return this._effective; }
            set { this._effective = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Expiration
        {
            get { return this._expiration; }
            set { this._expiration = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int TypeMasterId
        {
            get { return this._typeMasterId; }
            set { this._typeMasterId = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public ClientType ClientType
        {
            get { return this._clientType; }
            set { this._clientType = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public CobraClientRole[] CobraClientRoles
        {
            get { return this._cobraClientRoles; }
            set { this._cobraClientRoles = value; }
        }
        #endregion
    }
}