using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.CWGConversion.Cobra
{
    public enum ActiveFlag
    {
        /// <summary>
        /// 
        /// </summary>
        [StringValue("A")]
        Active,

        /// <summary>
        /// 
        /// </summary>
        [StringValue("I")]
        Inactive
    }
}
