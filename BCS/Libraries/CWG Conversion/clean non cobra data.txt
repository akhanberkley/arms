-- clears out submissions and clients for a date range 
DECLARE @STARTDT DATETIME;
DECLARE @ENDDT DATETIME;
DECLARE @SYSTEMID VARCHAR(50);
DECLARE @CMPYNUMID VARCHAR(50);

SET @STARTDT = ''; -- COPY THIS FROM THE LOG
SET @ENDDT = ''; -- COPY THIS FROM THE LOG
SET @SYSTEMID = 'CWG Conversion';
SET @CMPYNUMID = 3;

DECLARE @TEMPTABLE TABLE(
    Id int NOT NULL);

/******************* SECTION CLIENTS *************************************/
INSERT INTO @TEMPTABLE (Id) SELECT Id FROM Client WHERE (CompanyNumberID = @CMPYNUMID AND EntryDt > @STARTDT AND EntryDt < @ENDDT AND EntryBy = @SYSTEMID ) ;

--SELECT Id FROM @TEMPTABLE;

DELETE FROM ClientCoreNameClientNameType WHERE ClientId IN (SELECT Id FROM @TEMPTABLE);
DELETE FROM ClientCoreAddressAddressType WHERE ClientId IN (SELECT Id FROM @TEMPTABLE);
DELETE FROM Client WHERE Id IN (SELECT Id FROM @TEMPTABLE);

/******************* END SECTION CLIENTS *************************************/


DELETE FROM @TEMPTABLE;

/******************* SECTION SUBMISSIONS *************************************/
--SELECT Id FROM Submission WHERE (CompanyNumberID = @CMPYNUMID AND SystemDt > @STARTDT AND SystemDt < @ENDDT AND SystemId = @SYSTEMID ) ;
INSERT INTO @TEMPTABLE (Id) SELECT Id FROM Submission WHERE (CompanyNumberID = @CMPYNUMID AND SystemDt > @STARTDT AND SystemDt < @ENDDT AND SystemId = @SYSTEMID ) ;


--Kill the client core client names tied to the submissions
DELETE FROM SubmissionClientCoreClientName WHERE SubmissionID IN (SELECT Id FROM @TEMPTABLE);

--Kill the client core client addresses tied to the submissions
DELETE FROM SubmissionClientCoreClientAddress WHERE SubmissionID IN (SELECT Id FROM @TEMPTABLE)

--Kill the lob children tied to the submissions
DELETE FROM SubmissionLineOfBusinessChildren WHERE SubmissionID IN (SELECT Id FROM @TEMPTABLE)

--Kill the lob tied to the submissions
DELETE FROM SubmissionLineOfBusiness WHERE SubmissionID IN (SELECT Id FROM @TEMPTABLE)

--** Remove the Trigger to Run the following
--** Note to self: make sure to add the trigger back when complete
DELETE FROM SubmissionCompanyNumberCode WHERE SubmissionID IN (SELECT Id FROM @TEMPTABLE)

--Kill the Code History tied to the submissions
DELETE FROM SubmissionCompanyNumberCodeHistory WHERE SubmissionID IN (SELECT Id FROM @TEMPTABLE)

-- Kill the AgentUnspecifiedReasons 
DELETE FROM SubmissionCompanyNumberAgentUnspecifiedReason  WHERE SubmissionID IN (SELECT Id FROM @TEMPTABLE)

-- Kill the AgentUnspecifiedReasons 
DELETE FROM SubmissionCompanyNumberAttributeValue  WHERE SubmissionID IN (SELECT Id FROM @TEMPTABLE)

--Kill the submissions
DELETE FROM Submission WHERE ID IN (SELECT Id FROM @TEMPTABLE)

/******************* END SECTION SUBMISSIONS *************************************/


/******************* END SECTION BCS_CLIENT CLIENTS *************************************/
use bcs_test_client;
DELETE FROM cLIENTnAME WHERE cLIENTiD IN (SELECT    Id FROM         dbo.Client WHERE EntryDt > @STARTDT AND EntryDt < @ENDDT) ;
DELETE FROM cLIENTaDDRESS WHERE cLIENTiD IN (SELECT    Id FROM         dbo.Client WHERE EntryDt > @STARTDT AND EntryDt < @ENDDT);
DELETE FROM cLIENT WHERE iD IN (SELECT    Id FROM         dbo.Client WHERE EntryDt > @STARTDT AND EntryDt < @ENDDT);

/******************* END SECTION BCS_CLIENT CLIENTS *************************************/
