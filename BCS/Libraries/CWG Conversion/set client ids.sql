
/*

Cursor to set ids on the conv_submission after fresh clients loaded.
helpful in testing

*/


declare @startingClientId int;
-- get this from the first client added with the recent load
Set @startingClientId = 10222066;
DECLARE @CUST_LOC_REP_CODE varchar(50), @CUST_ID varchar(50)

DECLARE sub_cursor CURSOR FOR
SELECT DISTINCT CUST_LOC_REP_CODE, CUST_ID
FROM         dbo.conv_submission

OPEN sub_cursor

-- Perform the first fetch.
FETCH NEXT FROM sub_cursor
INTO @CUST_LOC_REP_CODE, @CUST_ID


-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN
	PRINT 'CUST_LOC_REP_CODE: ' + @CUST_LOC_REP_CODE + '-CUST_ID:' + @CUST_ID + ' Id:' + CAST(@startingClientId AS varchar(50))
	Update conv_submission SET CLIENT_ID = @startingClientId where CUST_LOC_REP_CODE = @CUST_LOC_REP_CODE and CUST_ID = @CUST_ID;
	SET @startingClientId = (@startingClientId+1);
   -- This is executed as long as the previous fetch succeeds.	
   FETCH NEXT FROM sub_cursor
INTO @CUST_LOC_REP_CODE, @CUST_ID
END

CLOSE sub_cursor
DEALLOCATE sub_cursor
GO
