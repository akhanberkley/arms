using System;
using System.Collections.Generic;
using System.Text;
using Props = BCS.CWGConversion.Properties.Settings;
using BTS.LogFramework;
using System.Net.Mail;

namespace BCS.CWGConversion
{
    class Common
    {
        internal static DateTime GetDefaultExpirationDate()
        {
            try
            {
                string d = Props.Default.DefaultExpiration;
                int i = Convert.ToInt32(d);
                return DateTime.Now.AddYears(i);
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }
        internal static string GetNullSafeString(object p)
        {
            if (p == null || p == DBNull.Value)
                return string.Empty;
            else
                return p.ToString();
        }
        internal static string GetNullSafeDateTimeString(object p)
        {
            try
            {
                return Convert.ToDateTime(GetNullSafeString(p)).ToString();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        internal static bool GetNullSafeBool(object p)
        {
            string val = GetNullSafeString(p);
            if (val.Equals("Y", StringComparison.CurrentCultureIgnoreCase) || val.Equals("True", StringComparison.CurrentCultureIgnoreCase))
                return true;
            return false;
        }

        internal static string[] ProcessBusinessName(string ipName, int splitAcrossCount)
        {
            if (ipName == null)
                ipName = string.Empty;

            string pattern = @"\s+";
            System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(pattern);
            ipName = rgx.Replace(ipName, " ");

            int baseLength = 38;
            string[] retArray = new string[splitAcrossCount];

            // initialize all with empty strings
            for (int i = 0; i < retArray.Length; i++)
            {
                retArray[i] = string.Empty;
            }

            // return if nothing to process (length <= 38)
            if (ipName.Length <= baseLength)
            {
                retArray[0] = ipName;
                return retArray;
            }

            int spacesCount = 0;
            foreach (char var in ipName)
            {
                if (char.IsWhiteSpace(var))
                {
                    spacesCount++;
                }
            }

            if (ipName.Length > ((baseLength * splitAcrossCount) + spacesCount))
                ipName = ipName.Substring(0, ((baseLength * splitAcrossCount) + spacesCount));

            string[] splitted = ipName.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            int prevLength = 0;
            for (int i = 0; i < splitAcrossCount; i++)
            {
                retArray[i] = processFirstToken(splitted, baseLength, splitAcrossCount);
                prevLength += retArray[i].Length;
                string nextString = ipName.Substring(prevLength);
                if (nextString.StartsWith(" "))
                    prevLength++;
                splitted = ipName.Substring(prevLength).Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            }

            return retArray;
        }
        private static string processFirstToken(string[] input, int baseLength, int splitAccrossCount)
        {
            string firstToken = string.Empty;
            string previousFirstToken = string.Empty;
            int index = 0;
            while (firstToken.Length < baseLength && index < input.Length)
            {
                previousFirstToken = firstToken;
                firstToken += " " + input[index];
                firstToken = firstToken.TrimStart();
                index++;
            }
            if (firstToken.Length > baseLength)
                firstToken = previousFirstToken;

            if (firstToken == string.Empty && input.Length > 0)
                firstToken = input[0].Substring(0, baseLength);
            return firstToken;
        }

        internal static BCS.ClientSys.Biz.DataManager GetClientSysDataManager()
        {
            return new BCS.ClientSys.Biz.DataManager(Props.Default.ClientSysDSN);
        }
        internal static BCS.ClientSys.Biz.Lookups GetClientSysLookupManager()
        {
            return new BCS.ClientSys.Biz.Lookups(Props.Default.ClientSysDSN);
        }

        internal static bool IsNotNullOrEmpty(string p)
        {
            return !string.IsNullOrEmpty(p);
        }

        internal static BCS.Biz.DataManager GetBCSDataManager()
        {
            return new BCS.Biz.DataManager(Props.Default.BCSDSN);
        }

        internal static BCS.Biz.CompanyNumber GetBCSCompanyNumberObject(BCS.Biz.DataManager dm)
        {
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, Props.Default.CompanyNumber);
            return dm.GetCompanyNumber(BCS.Biz.FetchPath.CompanyNumber.Company, BCS.Biz.FetchPath.CompanyNumber.Company.ClientSystemType);
        }

        public static BCS.Biz.Lookups GetBCSLookupManager()
        {
            return new BCS.Biz.Lookups(Props.Default.BCSDSN);
        }

        public static void LogTimeTaken(string p, DateTime startTime, DateTime endTime)
        {
            TimeSpan ts = endTime.Subtract(startTime);
            string ps = string.Format("Time Taken {0} : {1} minutes OR {2} seconds", p, Math.Round(ts.TotalMinutes, 2), Math.Round(ts.TotalSeconds, 2));
            if (Props.Default.LogTimeTakens)
                LogCentral.Current.LogInfo(ps);
            if (Props.Default.ConsoleTimeTakens)
                Console.WriteLine(ps);
        }

        public static DateTime LogCurrentTime(string p)
        {
            DateTime DateTimeNow = DateTime.Now;
            string ps = string.Format("{0} : {1}", p, DateTimeNow.ToString());
            if (Props.Default.LogTimeTakens)
                LogCentral.Current.LogInfo(ps);
            if (Props.Default.ConsoleTimeTakens)
                Console.WriteLine(ps);
            return DateTimeNow;
        }

        internal static void SendEmail(string text)
        {
            MailAddress messageFrom = new MailAddress(Properties.Settings.Default.MailFrom);
            MailAddress messageTo = new MailAddress(Properties.Settings.Default.MailTo);

            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(messageFrom, messageTo);

            string ccAddresses = Properties.Settings.Default.MailCc;
            string[] ccAddrArray = ccAddresses.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string var in ccAddrArray)
            {
                string trimmedVar = var.Trim();
                if (OrmLib.Email.ValidateEmail(trimmedVar))
                    message.CC.Add(trimmedVar);
            }

            SmtpClient client = new SmtpClient(Properties.Settings.Default.MailServer);

            //message.Subject = string.Format(Properties.Settings.Default.MailSubject, emailType);
            message.Subject = "CWG Conversion process status.";
            message.IsBodyHtml = true;
            //if (emailType == EmailType.Failure)
                message.Priority = MailPriority.High;

                message.Body = text;

            client.Send(message);
        }

        internal static void SendEmail(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("An Exception occurred");
            sb.AppendLine("Message : " + ex.Message);
            sb.AppendLine("Source : " + ex.Source);
            if (ex.InnerException != null)
                sb.AppendLine("Inner Exception : " + ex.InnerException);

            SendEmail(sb.ToString());
        }

        internal static void HighlightConsole()
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
        }
        internal static void UnHighlightConsole()
        {
            //Console.BackgroundColor = ConsoleColor.Gray;
            //Console.ForegroundColor = ConsoleColor.Black;
            Console.ResetColor();
            Console.Beep(80, 1000);
        }
    }    
}
