using System;
using System.Collections.Generic;
using System.Text;
using BCS.CWGConversion.Business.Objects;
using BTS.LogFramework;

namespace BCS.CWGConversion.Conversion
{
    /// <summary>
    /// 
    /// </summary>
    public class NameInBillingSystemComparer : System.Collections.IComparer
    {

        // Calls CaseInsensitiveComparer.Compare with the parameters reversed. bubbles up trues
        int System.Collections.IComparer.Compare(Object x, Object y)
        {
            if (!(x is ClientName) || !(y is ClientName))
                throw new ArgumentException("Invalid comparison");

            ClientName xName = x as ClientName;
            ClientName yName = y as ClientName;

            return (new System.Collections.CaseInsensitiveComparer().Compare(yName.InBillingSystem, xName.InBillingSystem));
        }
        public ClientName[] RefineBillingSysEntries(ClientName[] clientNameArray)
        {
            int inBSCount = 0;
            foreach (ClientName var in clientNameArray)
            {
                if (var.InBillingSystem)
                    inBSCount++;
            }

            // bubble up in billing system entries if more than one found
            if (inBSCount > 1)
            {
                Array.Sort(clientNameArray, new NameInBillingSystemComparer());
            }

            // log if no in billing system entries
            if (inBSCount == 0)
            {
                // log entry to be used as in billing system
                LogCentral.Current.LogWarn(
                    string.Format("No 'In Billing System' Names were found. Treating CUST_LOC_REP_CODE - '{0}', CUST_ID - '{1}', LASTNAME - '{2}', FIRSTNAME - '{3}', MIDDLE - '{4}', Business_name - '{5}', FEIN - '{6}', insured dba indicator - '{7}', in billing sytem indicator - '{8}' as 'In Billing System' entry.",
                    clientNameArray[0].Cust_LOC_REP_CODE, clientNameArray[0].CUST_ID, clientNameArray[0].LASTNAME, clientNameArray[0].FIRSTNAME, clientNameArray[0].MIDDLE, clientNameArray[0].Business_name, clientNameArray[0].FEIN, clientNameArray[0].InsuredDBA_indicator, clientNameArray[0].InBillingSystemIndicator));
            }

            // log if multiple in billing system entries
            if (inBSCount > 1)
            {
                // log entry to be used as in billing system
                LogCentral.Current.LogWarn(
                    string.Format("Multiple({0}) 'In Billing System' Names were found. Treating CUST_LOC_REP_CODE - '{1}', CUST_ID - '{2}', LASTNAME - '{3}', FIRSTNAME - '{4}', MIDDLE - '{5}', Business_name - '{6}', FEIN - '{7}', insured dba indicator - '{8}', in billing sytem indicator - '{9}' as 'In Billing System' entry.",
                    inBSCount, clientNameArray[0].Cust_LOC_REP_CODE, clientNameArray[0].CUST_ID, clientNameArray[0].LASTNAME, clientNameArray[0].FIRSTNAME, clientNameArray[0].MIDDLE, clientNameArray[0].Business_name, clientNameArray[0].FEIN, clientNameArray[0].InsuredDBA_indicator, clientNameArray[0].InBillingSystemIndicator));
            }
            return clientNameArray;
        }
    }
}
