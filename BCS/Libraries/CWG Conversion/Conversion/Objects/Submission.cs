namespace BCS.CWGConversion.Business.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// 
    /// </summary>
    public class Submission
    {
        private int lOC_REP_CODE, agent_Number, cUST_LOC_REP_CODE, cUST_ID;

        private string aPP_NUM, policy_NUM, submission_Type, aPP_EFF_DATE, agency_Number,
             uW_UNIT, policy_symbol, service_Office, uW_CODE,
            no_Agent, no_Signature, sTATUS_CODE, sTATUS_REASON, sTATUS_DATE, client_id, submission_Date, submissionBy;

        public string SubmissionBy
        {
            get { return submissionBy; }
            set { submissionBy = value; }
        }


        /// <summary>
        /// fields for clearance
        /// </summary>
        private string bCS_STATUS_CODE, bCS_STATUS_CODE_REASON, bCS_UW_UNIT, bCS_Policy_symbol, bCS_Service_Office, bCS_STATUS_CODE_REASON_ID,
            bCS_How_Received;

        private bool sendToCobra, specialCase;

        public bool SpecialCase
        {
            get { return specialCase; }
            set { specialCase = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool SendToCobra
        {
            get { return sendToCobra; }
            set { sendToCobra = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Submission_Date
        {
            get { return submission_Date; }
            set { submission_Date = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string BCS_How_Received
        {
            get { return bCS_How_Received; }
            set { bCS_How_Received = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string BCS_STATUS_CODE_REASON_ID
        {
            get { return bCS_STATUS_CODE_REASON_ID; }
            set { bCS_STATUS_CODE_REASON_ID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string BCS_Service_Office
        {
            get { return bCS_Service_Office; }
            set { bCS_Service_Office = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string BCS_Policy_symbol
        {
            get { return bCS_Policy_symbol; }
            set { bCS_Policy_symbol = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string BCS_STATUS_CODE_REASON
        {
            get { return this.bCS_STATUS_CODE_REASON; }
            set { this.bCS_STATUS_CODE_REASON = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string BCS_UW_UNIT
        {
            get { return this.bCS_UW_UNIT; }
            set { this.bCS_UW_UNIT = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        public string BCS_STATUS_CODE
        {
            get { return this.bCS_STATUS_CODE; }
            set { this.bCS_STATUS_CODE = value; }
        }
        private Comment[] comments;
        private AppLOB[] appLOBs;
        private Client client;

        /// <summary>
        /// 
        /// </summary>
        public Client Client
        {
            get { return client; }
            set { client = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Client_id
        {
            get { return client_id; }
            set { client_id = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public AppLOB[] AppLOBs
        {
            get { return appLOBs; }
            set { appLOBs = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Comment[] Comments
        {
            get { return comments; }
            set { comments = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string STATUS_DATE
        {
            get { return sTATUS_DATE; }
            set { sTATUS_DATE = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string STATUS_REASON
        {
            get { return sTATUS_REASON; }
            set { sTATUS_REASON = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string STATUS_CODE
        {
            get { return sTATUS_CODE; }
            set { sTATUS_CODE = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string No_Signature
        {
            get { return no_Signature; }
            set { no_Signature = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string No_Agent
        {
            get { return no_Agent; }
            set { no_Agent = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string UW_CODE
        {
            get { return uW_CODE; }
            set { uW_CODE = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Service_Office
        {
            get { return service_Office; }
            set { service_Office = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Policy_symbol
        {
            get { return policy_symbol; }
            set { policy_symbol = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string UW_UNIT
        {
            get { return uW_UNIT; }
            set { uW_UNIT = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Agency_Number
        {
            get { return agency_Number; }
            set { agency_Number = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string APP_EFF_DATE
        {
            get { return aPP_EFF_DATE; }
            set { aPP_EFF_DATE = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Submission_Type
        {
            get { return submission_Type; }
            set { submission_Type = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Policy_NUM
        {
            get { return policy_NUM; }
            set { policy_NUM = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string APP_NUM
        {
            get { return aPP_NUM; }
            set { aPP_NUM = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CUST_ID
        {
            get { return cUST_ID; }
            set { cUST_ID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int CUST_LOC_REP_CODE
        {
            get { return cUST_LOC_REP_CODE; }
            set { cUST_LOC_REP_CODE = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Agent_Number
        {
            get { return agent_Number; }
            set { agent_Number = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int LOC_REP_CODE
        {
            get { return lOC_REP_CODE; }
            set { lOC_REP_CODE = value; }
        }

    }
}
