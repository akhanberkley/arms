namespace BCS.CWGConversion.Business.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// class representing a client
    /// </summary>
    public class Client
    {
        /// <summary>
        /// client name field
        /// </summary>
        private ClientName[] clientNamesField;

        /// <summary>
        /// client address field
        /// </summary>
        private ClientAddress[] clientAddressesField;

        /// <summary>
        /// submissions field
        /// </summary>
        private Submission[] submissionsField;

        /// <summary>
        /// Gets or sets submissions
        /// </summary>
        public Submission[] Submissions
        {
            get { return submissionsField; }
            set { submissionsField = value; }
        }

        /// <summary>
        /// Gets or sets client names
        /// </summary>
        public ClientName[] ClientNames
        {
            get { return this.clientNamesField; }
            set { this.clientNamesField = value; }
        }

        /// <summary>
        /// Gets or sets client address
        /// </summary>
        public ClientAddress[] ClientAddresses
        {
            get { return this.clientAddressesField; }
            set { this.clientAddressesField = value; }
        }
    }
}
