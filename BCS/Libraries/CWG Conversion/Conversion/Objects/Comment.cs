using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.CWGConversion.Business.Objects
{
    /// <summary>
    /// 
    /// </summary>
    public class Comment
    {
        private int lOC_REP_CODE;

        /// <summary>
        /// 
        /// </summary>
        public int LOC_REP_CODE
        {
            get { return lOC_REP_CODE; }
            set { lOC_REP_CODE = value; }
        }
        private string aPP_NUM, pOLICY_Num, comments, comment_user, comment_Date, comment_Time;

        /// <summary>
        /// 
        /// </summary>
        public string Comment_Time
        {
            get { return comment_Time; }
            set { comment_Time = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Comment_Date
        {
            get { return comment_Date; }
            set { comment_Date = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Comment_user
        {
            get { return comment_user; }
            set { comment_user = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Comments
        {
            get { return comments; }
            set { comments = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string POLICY_Num
        {
            get { return pOLICY_Num; }
            set { pOLICY_Num = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string APP_NUM
        {
            get { return aPP_NUM; }
            set { aPP_NUM = value; }
        }
    }
}
