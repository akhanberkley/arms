namespace BCS.CWGConversion.Business.Objects
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// client address
    /// </summary>
    public class ClientAddress
    {
        /// <summary>
        /// private fields
        /// </summary>
        private int lOC_REP_CODE, cust_id, cust_loc_rep_code;

        /// <summary>
        /// private fields
        /// </summary>
        private string aDDR_NUM, aDDR_STREET, aDDR_STREET2, cITY, sTATE, zIP, zIP_4, cOUNTY_NAME, mailing_physical_ind, inBillingSystemIndicator;
        private bool inBillingSystem;

        /// <summary>
        /// Gets whether in billing system.
        /// </summary>
        public bool InBillingSystem
        {
            get { return inBillingSystem; }
        }

        /// <summary>
        /// Gets or sets whether in billing system as string value. Y implies in billing system.
        /// </summary>
        public string InBillingSystemIndicator
        {
            get { return inBillingSystemIndicator; }
            set
            {
                if (value != "Y")
                    inBillingSystem = false;
                else
                    inBillingSystem = true;

                inBillingSystemIndicator = value;
            }
        }

        /// <summary>
        /// Gets or sets cust log rep code
        /// </summary>
        public int Cust_loc_rep_code
        {
            get { return this.cust_loc_rep_code; }
            set { this.cust_loc_rep_code = value; }
        }

        /// <summary>
        /// Gets or sets cust id
        /// </summary>
        public int Cust_id
        {
            get { return this.cust_id; }
            set { this.cust_id = value; }
        }

        /// <summary>
        /// Gets or sets log rep code
        /// </summary>
        public int LOC_REP_CODE
        {
            get { return this.lOC_REP_CODE; }
            set { this.lOC_REP_CODE = value; }
        }

        /// <summary>
        /// Gets or sets mailing or physical indicator
        /// </summary>
        public string Mailing_physical_ind
        {
            get { return this.mailing_physical_ind; }
            set { this.mailing_physical_ind = value; }
        }

        /// <summary>
        /// Gets or sets county
        /// </summary>
        public string COUNTY_NAME
        {
            get { return this.cOUNTY_NAME; }
            set { this.cOUNTY_NAME = value; }
        }

        /// <summary>
        /// Gets or sets zip extension
        /// </summary>
        public string ZIP_4
        {
            get { return this.zIP_4; }
            set { this.zIP_4 = value; }
        }

        /// <summary>
        /// Gets or sets zip
        /// </summary>
        public string ZIP
        {
            get { return this.zIP; }
            set { this.zIP = value; }
        }

        /// <summary>
        /// Gets or sets state
        /// </summary>
        public string STATE
        {
            get { return this.sTATE; }
            set { this.sTATE = value; }
        }

        /// <summary>
        /// Gets or sets city
        /// </summary>
        public string CITY
        {
            get { return this.cITY; }
            set { this.cITY = value; }
        }

        /// <summary>
        /// Gets or sets address 2
        /// </summary>
        public string ADDR_STREET2
        {
            get { return this.aDDR_STREET2; }
            set { this.aDDR_STREET2 = value; }
        }

        /// <summary>
        /// Gets or sets address
        /// </summary>
        public string ADDR_STREET
        {
            get { return this.aDDR_STREET; }
            set { this.aDDR_STREET = value; }
        }

        /// <summary>
        /// Gets or sets number
        /// </summary>
        public string ADDR_NUM
        {
            get { return this.aDDR_NUM; }
            set { this.aDDR_NUM = value; }
        }
    }
}
