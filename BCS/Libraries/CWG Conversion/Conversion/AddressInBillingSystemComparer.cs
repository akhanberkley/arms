using System;
using System.Collections.Generic;
using System.Text;
using BCS.CWGConversion.Business.Objects;
using BTS.LogFramework;

namespace BCS.CWGConversion.Conversion
{
    /// <summary>
    /// 
    /// </summary>
    public class AddressInBillingSystemComparer : System.Collections.IComparer
    {
        // Calls CaseInsensitiveComparer.Compare with the parameters reversed. bubbles up trues
        int System.Collections.IComparer.Compare(Object x, Object y)
        {
            if (!(x is ClientAddress) || !(y is ClientAddress))
                throw new ArgumentException("Invalid comparison");

            ClientAddress xAddress = x as ClientAddress;
            ClientAddress yAddress = y as ClientAddress;

            return (new System.Collections.CaseInsensitiveComparer().Compare(yAddress.InBillingSystem, xAddress.InBillingSystem));
        }

        public ClientAddress[] RefineBillingSysEntries(ClientAddress[] clientAddressArray)
        {
            int inBSCount = 0;
            foreach (ClientAddress var in clientAddressArray)
            {
                if (var.InBillingSystem)
                    inBSCount++;
            }
            // bubble up in billing system entries if more than one found
            if (inBSCount > 1)
            {
                Array.Sort(clientAddressArray, new AddressInBillingSystemComparer());
            }

            // log if no in billing system entries
            if (inBSCount == 0)
            {
                // log entry to be used as in billing system
                LogCentral.Current.LogWarn(
                    string.Format("No 'In Billing System' Addresses were found. Treating Cust_loc_rep_code - '{0}', Cust_id - '{1}', ADDR_NUM - '{2}', ADDR_STREET - '{3}', ADDR_STREET - '{4}', CITY - '{5}', COUNTY_NAME - '{6}', STATE - '{7}', ZIP - '{8}', ZIP_4 - '{9}', Mailing_physical_ind - '{10}', InBillingSystemIndicator - '{11}' as 'In Billing System' entry.",
                    clientAddressArray[0].Cust_loc_rep_code, clientAddressArray[0].Cust_id, clientAddressArray[0].ADDR_NUM, clientAddressArray[0].ADDR_STREET, clientAddressArray[0].ADDR_STREET, clientAddressArray[0].CITY, clientAddressArray[0].COUNTY_NAME, clientAddressArray[0].STATE, clientAddressArray[0].ZIP, clientAddressArray[0].ZIP_4, clientAddressArray[0].Mailing_physical_ind, clientAddressArray[0].InBillingSystemIndicator));
            }

            // log if multiple in billing system entries
            if (inBSCount > 1)
            {
                // log entry to be used as in billing system
                LogCentral.Current.LogWarn(
                    string.Format("Multiple({0}) 'In Billing System' Addresses were found. Treating Cust_loc_rep_code - '{1}', Cust_id - '{2}', ADDR_NUM - '{3}', ADDR_STREET - '{4}', ADDR_STREET - '{5}', CITY - '{6}', COUNTY_NAME - '{7}', STATE - '{8}', ZIP - '{9}', ZIP_4 - '{10}', Mailing_physical_ind - '{11}', InBillingSystemIndicator - '{12}' as 'In Billing System' entry.",
                    inBSCount, clientAddressArray[0].Cust_loc_rep_code, clientAddressArray[0].Cust_id, clientAddressArray[0].ADDR_NUM, clientAddressArray[0].ADDR_STREET, clientAddressArray[0].ADDR_STREET, clientAddressArray[0].CITY, clientAddressArray[0].COUNTY_NAME, clientAddressArray[0].STATE, clientAddressArray[0].ZIP, clientAddressArray[0].ZIP_4, clientAddressArray[0].Mailing_physical_ind, clientAddressArray[0].InBillingSystemIndicator));
            }

            return clientAddressArray;
        }
    }
}
