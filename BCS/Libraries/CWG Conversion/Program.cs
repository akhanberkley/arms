using System;
using System.Collections.Generic;
using System.Text;
using BTS.LogFramework;

using ConvObjects = BCS.CWGConversion.Business.Objects;
using CONDataManager = BCS.CWGConversion.Business;

namespace BCS.CWGConversion
{
    class Program
    {        
        static bool readOnly = false;
        static List<string[]> failedProspects = new List<string[]>();
        public static void AddToFailedProspects(string clientId, string policyNumber)
        {
            if (!readOnly)
                failedProspects.Add(new string[] { clientId, policyNumber });
        }
        static List<ConvObjects.Client> failedClients = new List<BCS.CWGConversion.Business.Objects.Client>();
        public static void AddToFailedClients(ConvObjects.Client client)
        {
            if (!readOnly)
                failedClients.Add(client);
        }
        static void Main(string[] args)
        {
            #region test region
            
            #endregion

            RegisterUnhandledException();
            LogSeparator();
            DateTime startTime = LogCurrentTime("Process Started at");

            #region Non-Threaded
            if (!Properties.Settings.Default.ThreadedLoad)
            {
                new Loader().Load();

                #region Test region

                //////new Loader().Load(1); 
                //List<ConvObjects.Client> tcs = CONDataManager.DataManager.GetClientsToLoad();
                //List<ConvObjects.Client> testClient = new List<BCS.CWGConversion.Business.Objects.Client>();
                //int counter = 0;
                //foreach (ConvObjects.Client var in tcs)
                //{
                //    if (var.Submissions[0].CUST_ID == 203700 && var.Submissions[0].CUST_LOC_REP_CODE == 2)
                //    {
                //        testClient.Add(var);
                //        break;
                //    }
                //}
                //new Loader().LoadClients(testClient);
                #endregion
            }
            #endregion

            #region Threaded
            if (Properties.Settings.Default.ThreadedLoad)
            {
                #region Hardcoded ranges
                //{
                //    List<System.Threading.Thread> threads = new List<System.Threading.Thread>();
                //    for (int i = 0; i < 7; i++)
                //    {
                //        System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(new Loader().LoadRange));
                //        thread.Name = "Thread" + (i + 1);
                //        thread.Start(i);
                //        threads.Add(thread);
                //    }
                //    // start with all alive
                //    bool allAlive = true;
                //    while (allAlive)
                //    {
                //        // sleep for 2 minutes
                //        System.Threading.Thread.Sleep(120000);
                //        // set allAlive it to false
                //        allAlive = false;
                //        foreach (System.Threading.Thread var in threads)
                //        {
                //            // if atleast one is true
                //            if (var.IsAlive)
                //            {
                //                allAlive = true;
                //                break;
                //            }
                //            else
                //            {
                //                Console.WriteLine("{0} is done", var.Name);
                //            }
                //        }
                //        if (!allAlive)
                //        {
                //            // two minute above is low enough to wait, else we can resume the main thread here
                //        }
                //    }
                //}
                #endregion
                #region Dynamic ranges Get clients to load and divide into sets
                {
                    List<ConvObjects.Client> clientsList = CONDataManager.DataManager.GetClientsToLoad();

                    // subset length
                    int length = 1000;

                    #region TODO: Test Region
                    //clientsList = clientsList.GetRange(0, 99);
                    //length = 23;
                    #endregion

                    int remainder = 0;
                    int quotient = Math.DivRem(clientsList.Count, length, out remainder);

                    int numberOfSubsets = quotient + 1;


                    List<System.Threading.Thread> threads = new List<System.Threading.Thread>();
                    for (int i = 0; i < numberOfSubsets; i++)
                    {
                        int set = i;
                        int startIndex = length * set;


                        if (startIndex + length >= clientsList.Count)
                            length = clientsList.Count - startIndex;

                        Console.WriteLine("from index {0} to {1}", startIndex, length);

                        List<ConvObjects.Client> subset = clientsList.GetRange(startIndex, length);

                        System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(new Loader().LoadRange));
                        thread.Name = "Thread" + (i + 1);
                        thread.Start(subset);
                        threads.Add(thread);
                    }


                    int aliveCount = threads.Count;
                    // 3min?
                    TimeSpan baseSleepingTime = new TimeSpan(0, 3, 0);
                    #region Make the main thread sleep and not consume resources (initial)
                    if (aliveCount > 0)
                    {
                        int actualSleepMs = aliveCount * (int)baseSleepingTime.TotalMilliseconds;
                        LogCentral.Current.LogInfo(string.Format("Sleeping.......Will wake up at {0}.", DateTime.Now.AddMilliseconds(actualSleepMs).ToLongTimeString()));
                        System.Threading.Thread.Sleep(actualSleepMs);
                    }
                    #endregion
                    // start with all alive
                    bool anyOneAlive = true;
                    while (anyOneAlive)
                    {
                        //#region Make the main thread sleep and not consume resources (subsequent)
                        //if (aliveCount != 0)
                        //{
                        //    int actualSleepMs = aliveCount * (int)baseSleepingTime.TotalMilliseconds;
                        //    LogCentral.Current.LogInfo(string.Format("Sleeping.......Will wake up at {0}.", DateTime.Now.AddMilliseconds(actualSleepMs).ToLongTimeString()));
                        //    System.Threading.Thread.Sleep(actualSleepMs);
                        //}
                        //#endregion

                        // set anyOneAlive it to false
                        anyOneAlive = false;
                        foreach (System.Threading.Thread var in threads)
                        {
                            // if atleast one is true
                            if (var.IsAlive)
                            {
                                anyOneAlive = true;
                                break;
                            }
                            else
                            {
                                aliveCount--;
                                //Console.WriteLine("{0} is done", var.Name);
                            }
                        }
                        if (!anyOneAlive)
                        {
                        }
                    }
                }
                #endregion
            }

            #endregion

            LoadFailed();
            DateTime endTime = LogCurrentTime("Process Ended at");
            LogTimeTaken(startTime, endTime);
            Common.SendEmail("Success");
        }

        private static void LoadFailed()
        {
            readOnly = true;
            if (failedProspects.Count > 0 || failedClients.Count > 0)
            {
                if (failedClients.Count > 0)
                {
                    DateTime startTime = LogCurrentTime("Failed Clients Load Started at");
                    new Loader().LoadClients(failedClients);
                    DateTime endTime = LogCurrentTime("Failed Clients Load Ended at");
                    LogTimeTaken(startTime, endTime);
                }

                if (failedProspects.Count > 0)
                {
                    DateTime startTime = LogCurrentTime("Failed Prospects Load Started at");
                    foreach (string[] arrVar in failedProspects)
                    {
                        new BCS.CWGConversion.Cobra.BCSClientSystemManager().HandleaddProspect(arrVar);
                    }
                    DateTime endTime = LogCurrentTime("Failed Prospects Load Ended at");
                    LogTimeTaken(startTime, endTime);
                }
            }
        }

        private static void LogSeparator()
        {
            string separator = "****************************************";
            LogCentral.Current.LogInfo(separator);
            LogCentral.Current.LogWarn(separator);
            LogCentral.Current.LogError(separator);
            LogCentral.Current.LogFatal(separator);
        }

        // same common class logs only if config is true
        internal static void LogTimeTaken(DateTime startTime, DateTime endTime)
        {
            TimeSpan ts = endTime.Subtract(startTime);
            string ps = string.Format("Process Time Taken : {0} minutes OR {1} seconds", Math.Round(ts.TotalMinutes, 2), Math.Round(ts.TotalSeconds, 2));
            LogCentral.Current.LogInfo(ps);
            Console.WriteLine(ps);
        }
        // same common class logs only if config is true
        internal static void LogTimeTaken(string p, DateTime startTime, DateTime endTime)
        {
            TimeSpan ts = endTime.Subtract(startTime);
            string ps = string.Format("{2} Time Taken : {0} minutes OR {1} seconds", Math.Round(ts.TotalMinutes, 2), Math.Round(ts.TotalSeconds, 2), p);
            LogCentral.Current.LogInfo(ps);
            Console.WriteLine(ps);
        }

        internal static DateTime LogCurrentTime(string p)
        {
            DateTime DateTimeNow = DateTime.Now;
            string ps = string.Format("{0} : {1}", p, DateTimeNow.ToString());
            LogCentral.Current.LogInfo(ps);
            Console.WriteLine(ps);
            return DateTimeNow;
        }

        #region Unhandled exception handler
        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand, Flags = System.Security.Permissions.SecurityPermissionFlag.ControlAppDomain)]
        public static void RegisterUnhandledException()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);
        }

        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Console.WriteLine("Exception from {0}", System.Threading.Thread.CurrentThread.Name);
            Exception exceptionInfo = (Exception)args.ExceptionObject;
            LogCentral.Current.LogFatal(exceptionInfo.ToString(), exceptionInfo);
            Common.SendEmail(exceptionInfo);
        } 
        #endregion
    }
}
