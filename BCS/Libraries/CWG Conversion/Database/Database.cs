using System;
using System.Data;
using System.Configuration;
using BTS.LogFramework;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace BCS.CWGConversion.Database
{
    public class Database
    {
        /// <summary>
        /// Private constructor
        /// </summary>
        private Database() { }
                

        internal static System.Data.OleDb.OleDbConnection GetOpenOleDbConnection(string connectionString)
        {
            OleDbConnection conn = null;

            try
            {
                conn = new OleDbConnection(connectionString);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Count not open connection: " + ex.Message);
            }

            return conn;
        }

        internal static bool CloseOpenOleDbConnection(OleDbConnection conn)
        {
            bool isClosed = true;

            try
            {
                // Don't forget:
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Count not close connection: " + ex.Message);
                isClosed = false;
            }

            return isClosed;
        }

        internal static SqlConnection GetOpenConnection(string connectionString)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(connectionString);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Count not open connection: " + ex.Message);
            }

            return conn;
        }
        internal static bool CloseOpenConnection(SqlConnection conn)
        {
            bool isClosed = true;

            try
            {
                // Don't forget:
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Count not close connection: " + ex.Message);
                isClosed = false;
            }

            return isClosed;
        }

        internal static SqlCommand GetSqlCommand(string commandText, string connectionString, CommandType commandType)
        {
            SqlCommand command = new SqlCommand(commandText);

            command.Connection = GetOpenConnection(connectionString);
            command.CommandTimeout = 60;
            command.CommandType = commandType;

            return command;
        }
    } 
}
