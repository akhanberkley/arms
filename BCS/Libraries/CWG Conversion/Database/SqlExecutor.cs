#region Using
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Text; 
#endregion

namespace BCS.CWGConversion.Database
{
    /// <summary>
    /// SQL Executor Class is responsible for SQL Server Database interaction.
    /// </summary>
    internal class SQLExecutor
    {
        #region Query Executing Methods
        /// <summary>
        /// Methods executes a query and returns the results as a DataTable
        /// </summary>
        /// <param name="query">query to be executed</param>
        /// <param name="cString">Connection String</param>
        /// <returns>results as a DataTable</returns>
        public static DataTable ExecuteQuery(string query, string cString)
        {
            try
            {   
                SqlDataAdapter adapter = new SqlDataAdapter(query, cString);
                DataTable dtable = new DataTable();
                adapter.Fill(dtable);
                return dtable;
            }
            catch (Exception ex)
            { throw ex; }
            finally
            {}
        }
        #endregion

        #region Stored Procedure Executing Methods

        /// <summary>
        /// Method executes a Stored Procedure
        /// </summary>
        /// <param name="procName">Name of the Stored Procedure</param>
        /// <param name="cString">The Connection String</param>
        /// <param name="parameters">An ArrayList of SQLParameters</param>
        /// <returns>results in a DataTable</returns>
        public static DataTable ExecuteSP(string procName, string cString, ArrayList parameters)
        {
            // connection pooling using using clause
            using (SqlConnection conn = new SqlConnection(cString))
            {                
                try
                {
                    SqlCommand command = new SqlCommand(procName, conn);
                    command.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                        foreach (SqlParameter p in parameters)
                            command.Parameters.Add(p);

                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds, "DRP");

                    return ds.Tables[0];
                }
                catch (Exception ex)
                { throw ex; }                
            }
        }

        /// <summary>
        /// Method executes a Stored Procedure that returns a scalar value
        /// </summary>
        /// <param name="procName">Name of the Procedure</param>
        /// <param name="cString">An ArrayList of SQLParameters</param>
        /// <param name="parameters">An ArrayList of SQLParameters</param>        
        /// <returns>an Integer</returns>
        public static int ExecuteSPReturnScalar(string procName, string cString, ArrayList parameters)
        {
            using (SqlConnection conn = new SqlConnection(cString))
            {
                try
                {
                    SqlCommand command = new SqlCommand(procName, conn);
                    command.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                        foreach (SqlParameter p in parameters)
                        {
                            if (p.Value == null)
                                p.SqlValue = DBNull.Value;
                            command.Parameters.Add(p);
                        }

                    conn.Open();
                    return command.ExecuteNonQuery();
                }
                catch (Exception ex)
                { throw ex; }
            }
        }

        public static string ExecuteSPOP(string procName, string cString, ArrayList parameters)
        {
            using (SqlConnection conn = new SqlConnection(cString))
            {
                try
                {
                    SqlCommand command = new SqlCommand(procName, conn);
                    command.CommandType = CommandType.StoredProcedure;

                    if (parameters != null)
                        foreach (SqlParameter p in parameters)
                            command.Parameters.Add(p);

                    conn.Open();
                    int i = command.ExecuteNonQuery();
                    return command.Parameters["ERROR_MESSAGE"].Value.ToString();
                }
                catch (Exception ex)
                { throw ex; }
            }
        }
        #endregion
    }
}
