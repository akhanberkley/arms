namespace BCS.CWGConversion
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using ConvObjects = BCS.CWGConversion.Business.Objects;
    using CONDataManager = BCS.CWGConversion.Business;
    using BCSDataManager = BCS.Biz;
    using BCCDataManager = BCS.ClientSys;
    using BCS.CWGConversion.Conversion;
    using BCS.CWGConversion.Cobra;
    using structures = BTS.ClientCore.Wrapper.Structures;
    using BTS.LogFramework;
    using System.Data.SqlTypes;
    using System.Collections.Specialized;
    using BTS.ClientCore.Wrapper.Structures.Import;

    public class Loader
    {
        private BCSDataManager.DataManager bcsDM;
        BCSDataManager.CompanyNumber CompanyNumberObj;

        BCSDataManager.SubmissionTypeCollection allTypes;
        BCSDataManager.SubmissionStatusCollection allStatuses;
        //BCSDataManager.SubmissionStatusReasonCollection allStatusReasons;
        BCSDataManager.LineOfBusinessCollection allUWs;
        BCSDataManager.PersonCollection allPersons;
        BCSDataManager.CompanyNumberCodeCollection allCNCs;
        BCSDataManager.AgencyCollection allAgencies;
        //BCSDataManager.AgentCollection allAgents;
        BCSDataManager.AgentUnspecifiedReasonCollection allAgentReasons;
        BCSDataManager.CompanyNumberLOBGridCollection allLOBGrids;
        BCSDataManager.Lookups bcsLookup;
        BCS.ClientSys.Biz.DataManager clientSysDM;
        BCS.ClientSys.Biz.Lookups clientSysLookup;
        StringCollection agencyNumbersInConv;
        
        public Loader()
        {
            agencyNumbersInConv = CONDataManager.DataManager.GetConversionAgencyNumbers();
            LoadFresh();
        }
        public void LoadFresh()
        {
            bcsDM = Common.GetBCSDataManager();
            CompanyNumberObj = Common.GetBCSCompanyNumberObject(bcsDM);

            allTypes = CONDataManager.DataManager.GetAllTypes(bcsDM, Properties.Settings.Default.CompanyNumber);
            allStatuses = CONDataManager.DataManager.GetAllStatuses(bcsDM, Properties.Settings.Default.CompanyNumber);
            //allStatusReasons = CONDataManager.DataManager.GetAllStatusReasons(bcsDM, Properties.Settings.Default.CompanyNumber);
            allUWs = CONDataManager.DataManager.GetAllUnderwritingUnits(bcsDM, Properties.Settings.Default.CompanyNumber);
            allPersons = CONDataManager.DataManager.GetAllUnderwriters(bcsDM, Properties.Settings.Default.CompanyNumber);
            allCNCs = CONDataManager.DataManager.GetAllCompanyNumberCodes(bcsDM, Properties.Settings.Default.CompanyNumber);
            //allAgencies = CONDataManager.DataManager.GetAllAgencies(bcsDM, Properties.Settings.Default.CompanyNumber);            
            allAgencies = CONDataManager.DataManager.GetAgencies(bcsDM, Properties.Settings.Default.CompanyNumber, agencyNumbersInConv);
            //BCSDataManager.AgentCollection allAgents = CONDataManager.DataManager.GetAllAgents(bcsDM, Properties.Settings.Default.CompanyNumber);
            allAgentReasons = CONDataManager.DataManager.GetAllAgentUnspecifiedReasons(bcsDM, Properties.Settings.Default.CompanyNumber);
            allLOBGrids = CONDataManager.DataManager.GetAllLOBGrids(bcsDM, Properties.Settings.Default.CompanyNumber);

            bcsLookup = Common.GetBCSLookupManager();
            clientSysDM = Common.GetClientSysDataManager();
            clientSysLookup = Common.GetClientSysLookupManager();
        }
        
        public void Load()
        {
            List<ConvObjects.Client> clientsList = CONDataManager.DataManager.GetClientsToLoad();
            LoadClients(clientsList);
        }

        /// <summary>
        /// Test method, loads number of clients
        /// </summary>
        public void Load(int n)
        {
            List<ConvObjects.Client> clientsList = CONDataManager.DataManager.GetClientsToLoad();
            List<ConvObjects.Client> subset = clientsList.GetRange(0, n);
            LoadClients(subset);
        }

        public void LoadRange(object objRange)
        {
            List<ConvObjects.Client> clientsToLoad = objRange as List<ConvObjects.Client>;
            LoadClients(clientsToLoad);
        }        

        public void LoadClients(List<ConvObjects.Client> clientsList)
        {
            Dictionary<string, ConvObjects.Client> bcsFailedClients = new Dictionary<string, ConvObjects.Client>();            
            int currentClientIndex = 1;
            foreach (ConvObjects.Client client in clientsList)
            {
                #region TODO Test region fail some prospects
                //if (currentClientIndex % 2 == 0)
                //{
                //    Program.AddToFailedClients(client);
                //    continue;
                //}
                #endregion
                Console.WriteLine("Client {0} of {1}, from {2}", currentClientIndex, clientsList.Count, System.Threading.Thread.CurrentThread.Name);
                string clientId = string.Empty;

                foreach (ConvObjects.Submission submission in client.Submissions)
                {
                    // get the client id to decide if this is a new client.
                    // assumed that since submissions are for a client, the client id is uniform across submissions, so just get the 
                    // clientid on the first submission.
                    clientId = submission.Client_id;
                    break;
                }
                LoadClient(client, clientId);

                if (currentClientIndex % Properties.Settings.Default.BatchOf == 0)
                {
                    if (Properties.Settings.Default.BatchCommit)
                    {
                        #region TODO:
                        try
                        {
                            if (Properties.Settings.Default.ThreadedLoad)
                            {
                                string threadName = System.Threading.Thread.CurrentThread.Name;
                                int threadId = Convert.ToInt16(threadName.Substring(threadName.IndexOf("Thread")));
                                int sleepTime = threadId * 1000;
                                Console.WriteLine("{0} sleeping for {1} ms", threadName, sleepTime);
                                System.Threading.Thread.Sleep(sleepTime);
                            }
                        }
                        catch (Exception)
                        {
                        } 
                        #endregion
                        try
                        {
                            Common.HighlightConsole();
                            Console.WriteLine("starting commit from {0}", System.Threading.Thread.CurrentThread.Name);
                            Common.UnHighlightConsole();

                            DateTime commitStartTime = DateTime.Now;
                            bcsDM.CommitAll();
                            DateTime commitEndTime = DateTime.Now;
                            Program.LogTimeTaken(
                                string.Format("Committed batch indexed {0} : {1}-{2}", currentClientIndex, commitStartTime.ToString("mm:ss"), commitEndTime.ToString("mm:ss")),
                                commitStartTime, commitEndTime);

                            Common.HighlightConsole();
                            Console.WriteLine("end commit from {0}", System.Threading.Thread.CurrentThread.Name);
                            Common.UnHighlightConsole();
                        }
                        catch (Exception)
                        {
                            // TODO: if commit fails, determine the batch that failed and add it failed clients on BCS to try one more time
                            throw;
                        }
                        LoadFresh();
                    }
                }
                currentClientIndex++;
            }
            try
            {
                // commit final batch
                DateTime finalCommitStartTime = DateTime.Now;
                bcsDM.CommitAll();
                DateTime finalCommitEndTime = DateTime.Now;
                Common.HighlightConsole();
                Program.LogTimeTaken("Committed final", finalCommitStartTime, finalCommitEndTime);
                Common.UnHighlightConsole();
            }
            catch (Exception)
            {
                // TODO: if commit fails, determine the batch that failed and add it failed clients on BCS to try one more time
                throw;
            }
        }

        public void LoadClient(ConvObjects.Client client, string clientId)
        {
            Console.WriteLine("start loading client ");

            #region Build Client

            BTS.ClientCore.Wrapper.Structures.Import.ClientImportData idata = new BTS.ClientCore.Wrapper.Structures.Import.ClientImportData();
            idata.Signature = new BTS.ClientCore.Wrapper.Structures.Import.Signature("CLIENT IMPORT", "1.0", "en_US");
            idata.Client = new BTS.ClientCore.Wrapper.Structures.Import.ImportClient();

            string taxid = string.Empty;
            // defaulting to FEIN since CWG load column name is FEIN
            string taxidType = "FEIN";

            #region Names
            List<BTS.ClientCore.Wrapper.Structures.Import.NameInfo> namesToSend = new List<BTS.ClientCore.Wrapper.Structures.Import.NameInfo>();
            CONDataManager.Objects.ClientName[] refinedNames = new NameInBillingSystemComparer().RefineBillingSysEntries(client.ClientNames);
            foreach (CONDataManager.Objects.ClientName var in refinedNames)
            {
                BTS.ClientCore.Wrapper.Structures.Import.NameInfo aName = new BTS.ClientCore.Wrapper.Structures.Import.NameInfo();
                aName.BusinessName = var.Business_name.Trim();
                if (var.InsuredDBA_indicator == "I")
                    aName.ClearanceNameType = "Insured";
                if (var.InsuredDBA_indicator == "D")
                    aName.ClearanceNameType = "DBA";

                // note : we are not dealing with individual clients
                if (Common.IsNotNullOrEmpty(var.FIRSTNAME.Trim()))
                {
                    aName.BusinessName += string.Format(" {0}", var.FIRSTNAME);
                }
                if (Common.IsNotNullOrEmpty(var.MIDDLE.Trim()))
                {
                    aName.BusinessName += string.Format(" {0}", var.MIDDLE);
                }
                if (Common.IsNotNullOrEmpty(var.LASTNAME.Trim()))
                {
                    aName.BusinessName += string.Format(" {0}", var.LASTNAME);
                }
                aName.BusinessName = aName.BusinessName.Trim();

                taxid = var.FEIN;

                // add it to list
                namesToSend.Add(aName);
            }
            idata.Client.NameInfos = namesToSend.ToArray();
            if (idata.Client.NameInfos != null && idata.Client.NameInfos.Length > 0)
            { idata.Client.NameInfos[0].DisplayFlag = "Y"; }
            #endregion

            #region Addresses
            List<BTS.ClientCore.Wrapper.Structures.Import.AddressInfo> addressesToSend = new List<BTS.ClientCore.Wrapper.Structures.Import.AddressInfo>();
            CONDataManager.Objects.ClientAddress[] refinedAddresses = new AddressInBillingSystemComparer().RefineBillingSysEntries(client.ClientAddresses);
            foreach (CONDataManager.Objects.ClientAddress var in refinedAddresses)
            {
                BTS.ClientCore.Wrapper.Structures.Import.AddressInfo aAddress = new BTS.ClientCore.Wrapper.Structures.Import.AddressInfo();

                if (var.Mailing_physical_ind == "M")
                    aAddress.ClearanceAddressType = "Mailing";
                if (var.Mailing_physical_ind == "P")
                    aAddress.ClearanceAddressType = "Physical";
                if (var.Mailing_physical_ind == "B")
                    aAddress.ClearanceAddressType = "Both";



                aAddress.HouseNumber = var.ADDR_NUM;
                aAddress.ADDR1 = var.ADDR_STREET;
                aAddress.ADDR2 = var.ADDR_STREET2;
                aAddress.CityName = var.CITY;
                aAddress.County = var.COUNTY_NAME;
                aAddress.StateCode = var.STATE;
                aAddress.Zip = var.ZIP + var.ZIP_4;
                aAddress.Country = "USA";

                // add it to list
                addressesToSend.Add(aAddress);
            }
            idata.Client.AddressInfos = addressesToSend.ToArray();
            if (idata.Client.AddressInfos != null && idata.Client.AddressInfos.Length > 0)
            { idata.Client.AddressInfos[0].Primary = "Y"; }
            #endregion

            #region Other Info
            idata.Client.AllowMultipleNames = "Y";
            idata.Client.CCClientId = clientId;
            idata.Client.ClientType = string.Empty;
            idata.Client.EffectiveDate = DateTime.Now.ToString();
            idata.Client.ClientCategory = string.Empty;
            idata.Client.CorporationCode = string.Empty;
            idata.Client.DuplicateClientOption = 1;
            idata.Client.Owner = string.Empty;
            idata.Client.Status = "A";
            idata.Client.VendorFlag = "N";

            idata.Client.PersonalInfo = new BTS.ClientCore.Wrapper.Structures.Import.PersonalInfo();
            idata.Client.PersonalInfo.TaxId = taxid;
            idata.Client.PersonalInfo.TaxIdTypeCode = taxidType; 
            #endregion
            #endregion

            #region documentation from the email chain
            // refer to email chain Nancy M Sinnwell/DM/CWG/WRBerkley 01/28/2009 02:01 PM related to SendToCobra flag
            //  1)  Client ID does NOT exist for the record in the conv_submission table:
            //  Get a new client id from COBRA
            //  Create the primary in COBRA
            //  Create the prospect in COBRA
            //  Create the submission in Clearance
            //  Add the client, client names, and client addresses to BCS_Client

            //  2)  Client ID does exist for the record in the conv_submission table and the SendToCobra flag = 'N':
            //  Create the submission in Clearance
            //  Add the client, client names, and client addresses to BCS_Client

            //  3)  Client ID does exist for the record in the conv_submission table and the SendToCobra flag = 'Y':
            //  Create the prospect in COBRA
            //  Create the submission in Clearance 
            #endregion

            #region Notes
            // creating a prospect or not is handled for each submission. here we need to make a decision on whehter we update bcs and
            // client system. From the email chain, understood that if any of the submission has send to cobra as false, update on bcs 
            // and client system
            #endregion

            // if client id was not passed add, else edit
            if (string.IsNullOrEmpty(clientId))
            {
                bool isSpecialCase = false;
                /* check for special case */
                foreach (ConvObjects.Submission var in client.Submissions)
                {
                    if (var.SpecialCase)
                        isSpecialCase = true;
                }
                string portfolioId = string.Empty;
                string dba = string.Empty;
                string insuredName = string.Empty;
                List<string> addressIds = new List<string>();
                List<string> nameIds = new List<string>();

                #region is special case
                if (isSpecialCase)
                {
                    string consideredClientId = client.Submissions[0].CUST_ID.ToString();
                    // we will have to get the addressids, nameids, dba, insured
                    BCS.ClientSys.Biz.DataManager bcsccsysdm = new BCS.ClientSys.Biz.DataManager(Properties.Settings.Default.ClientSysDSN);
                    bcsccsysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientId);
                    BCS.ClientSys.Biz.Client sysClient = bcsccsysdm.GetClient(BCS.ClientSys.Biz.FetchPath.Client.AllChildren);

                    if (sysClient == null)
                    {
                        // special case should have client existed, so log and skip if otherwise
                        string message = string.Empty;
                        message = string.Format("Unable to find client in Client System. Skipping commiting client CUST_LOC_REP_CODE - {0}, CUST_ID - {1}  and its submissions to BCS.",
                        client.Submissions[0].CUST_LOC_REP_CODE, client.Submissions[0].CUST_ID);
                        return;
                    }
                    else
                    {

                        bcsDM.QueryCriteria.Clear();
                        bcsDM.QueryCriteria.And(BCS.Biz.JoinPath.Client.CompanyNumber.Columns.PropertyCompanyNumber, Properties.Settings.Default.CompanyNumber);
                        bcsDM.QueryCriteria.And(BCS.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientId);
                        BCS.Biz.Client bizclient = bcsDM.GetClient(BCS.Biz.FetchPath.Client.ClientCoreNameClientNameType,
                            BCS.Biz.FetchPath.Client.ClientCoreAddressAddressType);
                        if (bizclient == null)
                        {
                            // special case should have client existed, so log and skip if otherwise
                            string message = string.Empty;
                            message = string.Format("Unable to find client in BCS. Skipping commiting client CUST_LOC_REP_CODE - {0}, CUST_ID - {1}  and its submissions to BCS.",
                            client.Submissions[0].CUST_LOC_REP_CODE, client.Submissions[0].CUST_ID);
                            return;
                        }

                        portfolioId = sysClient.PortfolioId.IsNull ? sysClient.ClientCoreClientId.ToString() : sysClient.PortfolioId.ToString();
                        foreach (BCS.ClientSys.Biz.ClientAddress var in sysClient.ClientAddresss)
                        {
                            addressIds.Add(var.Id.ToString());
                        }
                        foreach (BCS.ClientSys.Biz.ClientName var in sysClient.ClientNames)
                        {
                            nameIds.Add(var.Id.ToString());
                        }
                        BCS.Biz.ClientNameType dbaType = bcsLookup.ClientNameTypes.FindByNameType("DBA");
                        BCS.Biz.ClientNameType insuredType = bcsLookup.ClientNameTypes.FindByNameType("Insured");
                        foreach (BCS.Biz.ClientCoreNameClientNameType var in bizclient.ClientCoreNameClientNameTypes)
                        {
                            if (var.ClientNameTypeId == dbaType.Id)
                            {
                                if (string.IsNullOrEmpty(dba))
                                {
                                    BCS.ClientSys.Biz.DataManager bcscysdm = new BCS.ClientSys.Biz.DataManager(Properties.Settings.Default.ClientSysDSN);
                                    bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Columns.OrderIndex, var.ClientCoreNameId);
                                    bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Client.Columns.ClientCoreClientId, clientId);
                                    dba = bcscysdm.GetClientName().BusinessName;
                                }
                            }
                            if (var.ClientNameTypeId == insuredType.Id)
                            {
                                if (string.IsNullOrEmpty(insuredName))
                                {
                                    BCS.ClientSys.Biz.DataManager bcscysdm = new BCS.ClientSys.Biz.DataManager(Properties.Settings.Default.ClientSysDSN);
                                    bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Columns.OrderIndex, var.ClientCoreNameId);
                                    bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Client.Columns.ClientCoreClientId, clientId);
                                    insuredName = bcscysdm.GetClientName().BusinessName;
                                }
                            }
                            if (!string.IsNullOrEmpty(dba) && !string.IsNullOrEmpty(insuredName))
                            {
                                // save a loop or two, if already got the first dba and insured name
                                break;
                            }
                        }
                    }
                    LoadSubmissions(client.Submissions, consideredClientId, portfolioId, addressIds, nameIds, insuredName, dba);

                    Console.WriteLine("end loading client ({0}, from {1}).", consideredClientId, System.Threading.Thread.CurrentThread.Name);
                }
                #endregion

                #region is not special case
                else
                {
                    // load the client sys client info
                    BCSClientSystemManager cobraManager = new BCSClientSystemManager(clientSysDM, clientSysLookup);

                    Response cobraResponse = cobraManager.importClientRefs(idata, ref portfolioId, ref addressIds, ref nameIds);
                    if (cobraResponse.ResponseCode == ResponseCode.Failure)
                    {
                        string message = cobraResponse.Reason;
                        message += string.Format(" Skipping commiting client CUST_LOC_REP_CODE - {0}, CUST_ID - {1}  and its submissions to BCS.",
                            client.Submissions[0].CUST_LOC_REP_CODE, client.Submissions[0].CUST_ID);

                        LogCentral.Current.LogFatal(message, cobraResponse.Exception);
                        //Program.AddToFailedClients(client);
                        return;
                    }

                    string generatedClientId = cobraResponse.Data.ToString();
                    AddBcsClient(generatedClientId, idata.Client.NameInfos, idata.Client.AddressInfos, ref portfolioId, ref addressIds, ref nameIds, ref insuredName, ref dba);

                    LoadSubmissions(client.Submissions, generatedClientId, portfolioId, addressIds, nameIds, insuredName, dba);

                    Console.WriteLine("end loading client ({0}, from {1}).", generatedClientId, System.Threading.Thread.CurrentThread.Name);
                }
                #endregion
            }
            else
            {
                string portfolioId = string.Empty;
                string dba = string.Empty;
                string insuredName = string.Empty;
                List<string> addressIds = new List<string>();
                List<string> nameIds = new List<string>();

                bool updateBCSAndClientSys = false;
                foreach (ConvObjects.Submission var in client.Submissions)
                {
                    if (!var.SendToCobra)
                    {
                        updateBCSAndClientSys = true;
                    }
                }
                // get the newly calculated addressids, nameids, dba, insured etc
                if (updateBCSAndClientSys)
                {
                    // load the client sys client info
                    BCSClientSystemManager cobraManager = new BCSClientSystemManager(clientSysDM, clientSysLookup);

                    Response cobraResponse = cobraManager.importClientRefs(idata, ref portfolioId, ref addressIds, ref nameIds);
                    if (cobraResponse.ResponseCode == ResponseCode.Failure)
                    {
                        string message = cobraResponse.Reason;
                        message += string.Format(" Skipping commiting client CUST_LOC_REP_CODE - {0}, CUST_ID - {1}  and its submissions to BCS.",
                            client.Submissions[0].CUST_LOC_REP_CODE, client.Submissions[0].CUST_ID);

                        LogCentral.Current.LogFatal(message, cobraResponse.Exception);
                        Program.AddToFailedClients(client);
                        return;
                    }
                    ModifyBcsClient(clientId, idata.Client.NameInfos, idata.Client.AddressInfos, updateBCSAndClientSys, ref addressIds, ref nameIds, ref insuredName, ref dba);
                }
                else // we will have to get the addressids, nameids, dba, insured  
                {

                    BCS.ClientSys.Biz.DataManager bcsccsysdm = new BCS.ClientSys.Biz.DataManager(Properties.Settings.Default.ClientSysDSN);
                    bcsccsysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientId);
                    BCS.ClientSys.Biz.Client sysClient = bcsccsysdm.GetClient(BCS.ClientSys.Biz.FetchPath.Client.AllChildren);

                    if (sysClient == null)
                    {
                        portfolioId = string.Empty;
                        addressIds = new List<string>();
                        nameIds = new List<string>();
                        insuredName = string.Empty;
                        dba = string.Empty;

                        // it might just exist in COBRA
                        AddClientSysClient(idata, ref portfolioId, ref addressIds, ref nameIds);

                        // and it will not exist in bcs too
                        AddBcsClient(idata.Client.CCClientId, idata.Client.NameInfos, idata.Client.AddressInfos,
                            ref portfolioId, ref addressIds, ref nameIds, ref insuredName, ref dba);

                        // try again
                        //BCS.ClientSys.Biz.DataManager bcsccsysdm = new BCS.ClientSys.Biz.DataManager(Properties.Settings.Default.ClientSysDSN);
                        //bcsccsysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientId);
                        sysClient = bcsccsysdm.GetClient(BCS.ClientSys.Biz.FetchPath.Client.AllChildren);

                    }
                    else
                    {

                        bcsDM.QueryCriteria.Clear();
                        bcsDM.QueryCriteria.And(BCS.Biz.JoinPath.Client.CompanyNumber.Columns.PropertyCompanyNumber, Properties.Settings.Default.CompanyNumber);
                        bcsDM.QueryCriteria.And(BCS.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientId);
                        BCS.Biz.Client bizclient = bcsDM.GetClient(BCS.Biz.FetchPath.Client.ClientCoreNameClientNameType,
                            BCS.Biz.FetchPath.Client.ClientCoreAddressAddressType);

                        portfolioId = sysClient.PortfolioId.IsNull ? sysClient.ClientCoreClientId.ToString() : sysClient.PortfolioId.ToString();
                        foreach (BCS.ClientSys.Biz.ClientAddress var in sysClient.ClientAddresss)
                        {
                            addressIds.Add(var.Id.ToString());
                        }
                        foreach (BCS.ClientSys.Biz.ClientName var in sysClient.ClientNames)
                        {
                            nameIds.Add(var.Id.ToString());
                        }
                        BCS.Biz.ClientNameType dbaType = bcsLookup.ClientNameTypes.FindByNameType("DBA");
                        BCS.Biz.ClientNameType insuredType = bcsLookup.ClientNameTypes.FindByNameType("Insured");
                        foreach (BCS.Biz.ClientCoreNameClientNameType var in bizclient.ClientCoreNameClientNameTypes)
                        {
                            if (var.ClientNameTypeId == dbaType.Id)
                            {
                                if (string.IsNullOrEmpty(dba))
                                {
                                    BCS.ClientSys.Biz.DataManager bcscysdm = new BCS.ClientSys.Biz.DataManager(Properties.Settings.Default.ClientSysDSN);
                                    bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Columns.OrderIndex, var.ClientCoreNameId);
                                    bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Client.Columns.ClientCoreClientId, clientId);
                                    dba = bcscysdm.GetClientName().BusinessName;
                                }
                            }
                            if (var.ClientNameTypeId == insuredType.Id)
                            {
                                if (string.IsNullOrEmpty(insuredName))
                                {
                                    BCS.ClientSys.Biz.DataManager bcscysdm = new BCS.ClientSys.Biz.DataManager(Properties.Settings.Default.ClientSysDSN);
                                    bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Columns.OrderIndex, var.ClientCoreNameId);
                                    bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Client.Columns.ClientCoreClientId, clientId);
                                    insuredName = bcscysdm.GetClientName().BusinessName;
                                }
                            }
                            if (!string.IsNullOrEmpty(dba) && !string.IsNullOrEmpty(insuredName))
                            {
                                // save a loop or two, if already got the first dba and insured name
                                break;
                            }
                        }
                    }
                }
                LoadSubmissions(client.Submissions, clientId, portfolioId, addressIds, nameIds, insuredName, dba);
            }
        }

        private void AddClientSysClient(BTS.ClientCore.Wrapper.Structures.Import.ClientImportData clientImport, ref string portfolioId, ref List<string> addressIdsList,
            ref List<string> nameIdsList)
        {
            #region BCS Client System Handling
            DateTime startTime = DateTime.Now;

            DateTime dateNow = DateTime.Now;

            BCS.ClientSys.Biz.Client newClient = clientSysDM.NewClient(Convert.ToInt64(clientImport.Client.CCClientId), dateNow,
                clientSysLookup.Companys.FindByCompanyNumber(Properties.Settings.Default.CompanyNumber));

            newClient.TaxNumber = clientImport.Client.PersonalInfo.TaxId;
            newClient.TaxTypeCode =
                clientSysLookup.TaxTypes.FindByTaxTypeDescription(clientImport.Client.PersonalInfo.TaxIdTypeCode) != null ?
                clientSysLookup.TaxTypes.FindByTaxTypeDescription(clientImport.Client.PersonalInfo.TaxIdTypeCode).Code : null;
            newClient.PortfolioId = Convert.ToInt64(clientImport.Client.CCClientId);

            for (int i = 0; i < clientImport.Client.NameInfos.Length; i++)
            {
                NameInfo var = clientImport.Client.NameInfos[i];
                BCS.ClientSys.Biz.ClientName newClientName = newClient.NewClientName();
                newClientName.BusinessName = var.BusinessName;
                newClientName.EntryDt = dateNow;
                // we are adding first name to cobra
                newClientName.InBillingSystem = i == 0;
                newClientName.OrderIndex = i + 1;

                // this process only loads from legacy system
                newClientName.FromLegacySystem = true;
            }
            for (int i = 0; i < clientImport.Client.AddressInfos.Length; i++)
            {
                AddressInfo var = clientImport.Client.AddressInfos[i];
                BCS.ClientSys.Biz.ClientAddress newClientAddress = newClient.NewClientAddress();
                newClientAddress.City = var.CityName;
                newClientAddress.CountryCode = var.Country;
                newClientAddress.County = var.County;
                newClientAddress.EntryDt = dateNow;
                newClientAddress.InBillingSystem = i == 0;
                newClientAddress.OrderIndex = i + 1;
                newClientAddress.StateCode = var.StateCode;
                newClientAddress.StreetLine2 = var.ADDR2;
                newClientAddress.StreetName = var.ADDR1;
                newClientAddress.StreetNumber = var.HouseNumber;
                newClientAddress.ZipCode = var.Zip;

                // this process only loads from legacy system
                newClientAddress.FromLegacySystem = true;

                // pad the zip extension
                while (newClientAddress.ZipCode.Length < 9)
                {
                    newClientAddress.ZipCode += "0";
                }
            }

            try
            {
                // commit
                clientSysDM.CommitAll();
                foreach (BCS.ClientSys.Biz.ClientAddress committedClientAddress in newClient.ClientAddresss)
                {
                    addressIdsList.Add(committedClientAddress.Id.ToString());
                }
                foreach (BCS.ClientSys.Biz.ClientName committedClientName in newClient.ClientNames)
                {
                    // order index is used by bcs as client core as name identifier 
                    nameIdsList.Add(committedClientName.OrderIndex.ToString());
                }
                portfolioId = newClient.PortfolioId.ToString();
            }
            catch (Exception exc)
            {
                string helperMessage = string.Format("Unable to add client to the bcs client system. The Client Id from cobra is '{0}'", clientImport.Client.CCClientId);
                //// we will not log here, instead pass to the calling method
                //// LogCentral.Current.LogFatal(string.Format("{0}", helperMessage), exc);
                //response.ResponseCode = ResponseCode.Failure;
                //response.Data = 0;
                //response.Reason = helperMessage;
                //response.Exception = exc;
                //return response;
            }

            DateTime endTime = DateTime.Now;
            Common.LogTimeTaken("client sytem add", startTime, endTime);

            #endregion
        }

        private void LoadSubmissions(ConvObjects.Submission[] submissions, string clientId, string portfolioId, List<string> addressIds,
            List<string> nameIds, string insuredName, string dba)
        {
            foreach (ConvObjects.Submission submission in submissions)
            {
                DateTime dtNow = DateTime.Now;
                Console.WriteLine("start loading submission LOC_REP_CODE - {0}, CUST_LOC_REP_CODE - {1}, APP_NUM - {2}, CUST_ID - {3}.",
                        submission.LOC_REP_CODE, submission.CUST_LOC_REP_CODE, submission.APP_NUM, submission.CUST_ID);
                Console.Write(" from thread {0}", System.Threading.Thread.CurrentThread.Name);

                #region required things
                BCSDataManager.Agency agency = allAgencies.FindByAgencyNumber(submission.Agency_Number);
                if (agency == null)
                {
                    LogCentral.Current.LogError("Missing agency - " + submission.Agency_Number);
                    continue;
                }
                BCSDataManager.SubmissionType sType = allTypes.FindByTypeName(submission.Submission_Type);
                if (sType == null)
                {
                    LogCentral.Current.LogError("Missing Submission Type - " + submission.Submission_Type);
                    continue;
                }
                DateTime validEffectiveDate = SqlDateTime.MinValue.Value;
                try
                {
                    validEffectiveDate = Convert.ToDateTime(submission.APP_EFF_DATE);
                }
                catch (Exception)
                {
                    LogCentral.Current.LogError("Unconvertible effective date - " + submission.APP_EFF_DATE);
                    continue;
                }
                BCSDataManager.LineOfBusiness lob = allUWs.FindByCode(submission.BCS_UW_UNIT);
                if (lob == null)
                {
                    LogCentral.Current.LogError("Missing underwriting unit - " + submission.BCS_UW_UNIT);
                    continue;
                } 
                #endregion

                #region Company Number Code Ids
                string[] CodeIdsArray = new string[3];
                CodeIdsArray[0] =
                    allCNCs.FindByCode(submission.Policy_symbol) != null ?
                    allCNCs.FindByCode(submission.Policy_symbol).Id.ToString() : string.Empty;
                CodeIdsArray[1] =
                    allCNCs.FindById(submission.BCS_Service_Office) != null ?
                    allCNCs.FindById(submission.BCS_Service_Office).Id.ToString() : string.Empty;
                string howReceived = submission.BCS_How_Received;
                string[] howReceivedArr = howReceived.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < howReceivedArr.Length; i++)
                {
                    howReceivedArr[i] = howReceivedArr[i].Trim();
                }
                Biz.CompanyNumberCodeCollection filteredCNCs = allCNCs.FilterByCode(howReceivedArr[0]);
                filteredCNCs = filteredCNCs.FilterByDescription(howReceivedArr[1]);
                if (filteredCNCs.Count == 1)
                    CodeIdsArray[2] = filteredCNCs[0].Id.ToString();
                string codeids = string.Join("|", CodeIdsArray);
                #endregion

                #region agent unspecified reasons
                string[] AgentReasonArray = new string[2];
                AgentReasonArray[0] = submission.No_Agent == "1" ? allAgentReasons.FindByReason("Not on File").Id.ToString() : "";
                AgentReasonArray[1] = submission.No_Signature == "1" ? allAgentReasons.FindByReason("No Signature").Id.ToString() : "";
                string AgentReasonsString = string.Join("|", AgentReasonArray);
                #endregion

                #region comments
                string[] commentsArray = new string[submission.Comments.Length];
                for (int i = 0; i < submission.Comments.Length; i++)
                {
                    commentsArray[i] = string.Format("[{0} {1} {2}]\r\n{3}", submission.Comments[i].Comment_Time, submission.Comments[i].Comment_Date, submission.Comments[i].Comment_user, submission.Comments[i].Comments);
                }
                string comments = string.Join("|", commentsArray);
                #endregion
                
                string policyNumber = submission.Policy_NUM;

                int sequence = 0;
                long submissionNumber = new SubmissionNumberGenerator().GetSubmissionControlNumber(0, Properties.Settings.Default.CompanyNumber, out sequence);

                BCSDataManager.Submission bizSubmission = bcsDM.NewSubmission(
                    Convert.ToInt64(clientId),
                    submissionNumber,
                    Common.IsNotNullOrEmpty(submission.SubmissionBy) ? submission.SubmissionBy : Properties.Settings.Default.EntryBy,
                    dtNow,
                    agency,
                    CompanyNumberObj,
                    sType
                    );

                bizSubmission.SystemId = Properties.Settings.Default.EntryBy;
                bizSubmission.UpdatedBy = bizSubmission.SubmissionBy;
                bizSubmission.EffectiveDate = validEffectiveDate;
                bizSubmission.Sequence = sequence;
                foreach (string addressId in addressIds)
                {
                    BCSDataManager.SubmissionClientCoreClientAddress bizSubmissionAddress = bizSubmission.NewSubmissionClientCoreClientAddress();
                    bizSubmissionAddress.ClientCoreClientAddressId = Convert.ToInt32(addressId);
                }
                foreach (string nameId in nameIds)
                {
                    BCSDataManager.SubmissionClientCoreClientName bizSubmissionName = bizSubmission.NewSubmissionClientCoreClientName();
                    bizSubmissionName.ClientCoreNameId = Convert.ToInt32(nameId);
                }
                try
                {
                    bizSubmission.ClientCorePortfolioId = Convert.ToInt64(portfolioId);
                }
                catch (Exception)
                {
                    bizSubmission.ClientCorePortfolioId = Convert.ToInt64(clientId);
                }
                try
                {
                    bizSubmission.SubmissionDt = Convert.ToDateTime(submission.Submission_Date);
                }
                catch (Exception)
                {
                    // submission date is already set to now
                }

                bizSubmission.AgentId = agency.Agents.FindByBrokerNo(submission.Agent_Number.ToString()) != null ? agency.Agents.FindByBrokerNo(submission.Agent_Number.ToString()).Id : SqlInt32.Null;
                foreach (string agentReasonId in AgentReasonArray)
                {
                    if (Common.IsNotNullOrEmpty(agentReasonId))
                    {
                        BCSDataManager.SubmissionCompanyNumberAgentUnspecifiedReason var = bizSubmission.NewSubmissionCompanyNumberAgentUnspecifiedReason();
                        var.CompanyNumberAgentUnspecifiedReasonId = Convert.ToInt32(agentReasonId);
                    }
                }
                bizSubmission.SubmissionStatusId = allStatuses.FindByStatusCode(submission.BCS_STATUS_CODE) != null ? allStatuses.FindByStatusCode(submission.BCS_STATUS_CODE).Id : SqlInt32.Null;
                try
                {
                    bizSubmission.SubmissionStatusDate = Convert.ToDateTime(submission.STATUS_DATE);
                }
                catch (Exception)
                {
                    bizSubmission.SubmissionStatusDate = SqlDateTime.Null;
                }
                // datamanager gives us this way
                if (submission.BCS_STATUS_CODE_REASON_ID != "0")
                    bizSubmission.SubmissionStatusReasonId = Convert.ToInt32(submission.BCS_STATUS_CODE_REASON_ID);

                bizSubmission.PolicyNumber = policyNumber;

                foreach (string codeid in CodeIdsArray)
                {
                    if (Common.IsNotNullOrEmpty(codeid))
                    {
                        BCSDataManager.SubmissionCompanyNumberCode bizCode = bizSubmission.NewSubmissionCompanyNumberCode();
                        bizCode.CompanyNumberCodeId = Convert.ToInt32(codeid); 
                    }
                }
                bizSubmission.UnderwriterPersonId =
                    allPersons.FindByInitials(submission.UW_CODE) != null ? allPersons.FindByInitials(submission.UW_CODE).Id : SqlInt32.Null;

                bizSubmission.Comments = comments;
                bcsDM.NewSubmissionLineOfBusiness(lob, bizSubmission);
                foreach (ConvObjects.AppLOB var in submission.AppLOBs)
                {
                    BCSDataManager.CompanyNumberLOBGrid lobgrid = allLOBGrids.FindByMulitLOBCode(var.BCS_LOB_CODE);
                    if (lobgrid != null)
                    {

                        BCSDataManager.SubmissionLineOfBusinessChildren bizVar =
                            bizSubmission.NewSubmissionLineOfBusinessChildren();

                        bizVar.CompanyNumberLOBGridId = lobgrid.Id;

                        // status
                        if (Common.IsNotNullOrEmpty(var.BCS_STATUS_CODE))
                            bizVar.SubmissionStatusId =
                                allStatuses.FindByStatusCode(var.BCS_STATUS_CODE) != null ?
                                allStatuses.FindByStatusCode(var.BCS_STATUS_CODE).Id : SqlInt32.Null;

                        // status reason
                        if (submission.BCS_STATUS_CODE_REASON_ID != "0")
                            bizVar.SubmissionStatusReasonId = Convert.ToInt32(submission.BCS_STATUS_CODE_REASON_ID);
                    }
                }

                bizSubmission.InsuredName = insuredName;
                bizSubmission.Dba = dba;

                if (!Properties.Settings.Default.BatchCommit)
                    bcsDM.CommitAll();

                if (submission.SendToCobra)
                    new BCSClientSystemManager().addProspect(Convert.ToInt32(clientId), submission.Policy_NUM);
                Console.WriteLine("end loading submission");

                DateTime endTime = DateTime.Now;
                Common.LogTimeTaken("bcs submission add", dtNow, endTime);
            }
        }
       
        private void AddBcsClient(string clientCoreClientId, BTS.ClientCore.Wrapper.Structures.Import.NameInfo[] nameInfos,
            BTS.ClientCore.Wrapper.Structures.Import.AddressInfo[] addressInfos, ref string portfolioId, ref List<string> addressIdsList,
            ref List<string> nameIdsList, ref string insuredName, ref string dba)
        {

            DateTime startTime = DateTime.Now;

            #region clearance client info add

            //BCSDataManager.DataManager bcsDM = Common.GetBCSDataManager();
            BCSDataManager.Client bizclient = bcsDM.NewClient(Convert.ToInt64(clientCoreClientId), CompanyNumberObj);
            bizclient.EntryBy = Properties.Settings.Default.EntryBy;

            // we dont have client attributes, company codes or class codes with this load data
            #endregion

            #region add name and address TYPES on clearance side using refs

            bool isCobraForCounty = false;
            bool isCobraOrRelatedForTypes = false; // for address & name types

            Biz.Company currentCompany = CompanyNumberObj.Company;
            isCobraForCounty = currentCompany.ClientSystemType.Code.Equals("Cobra", StringComparison.CurrentCultureIgnoreCase);
            isCobraOrRelatedForTypes = isCobraForCounty || currentCompany.ClientSystemType.Code.Equals("BCSasClientCore", StringComparison.CurrentCultureIgnoreCase);

            for (int i = 0; i < addressInfos.Length; i++)
            {
                structures.Import.AddressInfo ipAddrInfo = addressInfos[i];
                // cobra pads the zip to 9 digits, so addresses will not be equal
                if (isCobraOrRelatedForTypes)
                {
                    while (ipAddrInfo.Zip.Length < 9)
                    {
                        ipAddrInfo.Zip += "0";
                    }
                }
                if (ipAddrInfo.ClearanceAddressType != null)
                {
                    try
                    {
                        bcsDM.NewClientCoreAddressAddressType(Convert.ToInt32(addressIdsList[i]), bcsLookup.AddressTypes.FindByPropertyAddressType(ipAddrInfo.ClearanceAddressType), bizclient);
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Exception at evaluating addresses {0} {1} {2} {3} {4} ", clientCoreClientId, ipAddrInfo.HouseNumber, ipAddrInfo.ADDR1, ipAddrInfo.CityName, ipAddrInfo.StateCode);

                        Console.WriteLine(addressIdsList.Count);
                        Console.WriteLine(addressInfos.Length);

                        Console.WriteLine(nameInfos[0].BusinessName);

                        LogCentral.Current.LogError(
                            string.Format(
                            "Exception at evaluating addresses {0} {1} {2} {3} {4}. AddressIdsList count id {5}, addressInfos Count is {6}. First client name is {7}.",
                            clientCoreClientId, ipAddrInfo.HouseNumber, ipAddrInfo.ADDR1, ipAddrInfo.CityName, ipAddrInfo.StateCode,
                            addressIdsList.Count,
                            addressInfos.Length,
                            nameInfos[0].BusinessName));

                        // TODO: eat up the exception for now
                        //throw;
                    }
                }

                if (isCobraForCounty)
                {
                    if (Common.IsNotNullOrEmpty(ipAddrInfo.County))
                    {
                        Biz.CobraAddressCounty cobraAddressCounty = bcsDM.NewCobraAddressCounty(Convert.ToInt32(addressIdsList[i]), bizclient);
                        cobraAddressCounty.County = ipAddrInfo.County;
                    }
                }
            }
            for (int i = 0; i < nameInfos.Length; i++)
            {
                structures.Import.NameInfo ipNameInfo = nameInfos[i];
                if (ipNameInfo.ClearanceNameType == "DBA")
                {
                    // the client's first name dba should be the submission's name dba
                    if (string.IsNullOrEmpty(dba))
                        dba = ipNameInfo.BusinessName;
                }
                if (ipNameInfo.ClearanceNameType == "Insured")
                {
                    // the client's first name insured should be the submission's name insured
                    if (string.IsNullOrEmpty(insuredName))
                        insuredName = ipNameInfo.BusinessName;
                }
                if (ipNameInfo.ClearanceNameType != null)
                {
                    try
                    {
                        Biz.ClientNameType temp = bcsLookup.ClientNameTypes.FindByNameType(ipNameInfo.ClearanceNameType);
                        bcsDM.NewClientCoreNameClientNameType(Convert.ToInt32(nameIdsList[i]), bizclient, temp);
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Exception at evaluating names {0} {1}", clientCoreClientId, ipNameInfo.BusinessName);

                        Console.WriteLine(nameIdsList.Count);
                        Console.WriteLine(nameInfos.Length);
                        LogCentral.Current.LogError(
                            string.Format("Exception at evaluating names {0} {1}. nameIdsList.Count is {2}, nameInfos.Length is {3}. First client name is {7}.",
                            clientCoreClientId, ipNameInfo.BusinessName, nameIdsList.Count, nameInfos.Length, nameInfos[0].BusinessName));

                        // TODO: eat up the exception for now
                        //throw;
                    }
                }
            }

            if (!Properties.Settings.Default.BatchCommit)
                bcsDM.CommitAll();
            #endregion

            DateTime endTime = DateTime.Now;
            Common.LogTimeTaken("bcs client add", startTime, endTime);
        }


        private void ModifyBcsClient(string clientCoreClientId, BTS.ClientCore.Wrapper.Structures.Import.NameInfo[] nameInfos,
            BTS.ClientCore.Wrapper.Structures.Import.AddressInfo[] addressInfos, bool updateBCSAndClientSys, ref List<string> addressIdsList,
            ref List<string> nameIdsList, ref string insuredName, ref string dba)
        {

            DateTime startTime = DateTime.Now;

            #region clearance client info update

            bcsDM.QueryCriteria.Clear();
            bcsDM.QueryCriteria.And(BCS.Biz.JoinPath.Client.CompanyNumber.Columns.PropertyCompanyNumber, Properties.Settings.Default.CompanyNumber);
            bcsDM.QueryCriteria.And(BCS.Biz.JoinPath.Client.Columns.ClientCoreClientId, clientCoreClientId);


            BCSDataManager.Client bizclient = bcsDM.GetClient(BCS.Biz.FetchPath.Client.ClientCoreAddressAddressType,
                BCS.Biz.FetchPath.Client.ClientCoreNameClientNameType);
            if (bizclient == null)
            {
                string pid = string.Empty;
                AddBcsClient(clientCoreClientId, nameInfos, addressInfos, ref pid, ref addressIdsList, ref nameIdsList, ref insuredName, ref dba);
                return;
            }
            bizclient.ModifiedBy = Properties.Settings.Default.EntryBy;
            bizclient.ModifiedDt = DateTime.Now;

            // we dont have client attributes, company codes or class codes with this load data
            #endregion

            #region add name and address TYPES on clearance side using refs
            
            // client modification here only deals with additiion of new names and addresses
            List<string> newAddressIdsList = new List<string>();
            List<string> newNameIdsList = new List<string>();

            foreach (string addrId in addressIdsList)
            {
                BCS.Biz.ClientCoreAddressAddressType aClientAddress = bizclient.ClientCoreAddressAddressTypes.FindByClientCoreAddressId(addrId);
                // gather new only
                if (aClientAddress == null)
                {
                    newAddressIdsList.Add(addrId);
                }
            }
            foreach (string nameId in nameIdsList)
            {
                BCS.Biz.ClientCoreNameClientNameType aClientName = bizclient.ClientCoreNameClientNameTypes.FindByClientCoreNameId(nameId);
                // gather new only
                if (aClientName == null)
                {
                    newNameIdsList.Add(nameId);
                }
            }
            BCS.Biz.ClientNameType dbaType = bcsLookup.ClientNameTypes.FindByNameType("DBA");
            BCS.Biz.ClientNameType insuredType = bcsLookup.ClientNameTypes.FindByNameType("Insured");
            foreach (BCS.Biz.ClientCoreNameClientNameType var in bizclient.ClientCoreNameClientNameTypes)
            {
                if (var.ClientNameTypeId == dbaType.Id)
                {
                    if (string.IsNullOrEmpty(dba))
                    {
                        BCS.ClientSys.Biz.DataManager bcscysdm = new BCS.ClientSys.Biz.DataManager(Properties.Settings.Default.ClientSysDSN);
                        bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Columns.OrderIndex, var.ClientCoreNameId);
                        bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Client.Columns.ClientCoreClientId, clientCoreClientId);
                        dba = bcscysdm.GetClientName().BusinessName;
                    }
                }
                if (var.ClientNameTypeId == insuredType.Id)
                {
                    if (string.IsNullOrEmpty(insuredName))
                    {
                        BCS.ClientSys.Biz.DataManager bcscysdm = new BCS.ClientSys.Biz.DataManager(Properties.Settings.Default.ClientSysDSN);
                        bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Columns.OrderIndex, var.ClientCoreNameId);
                        bcscysdm.QueryCriteria.And(BCS.ClientSys.Biz.JoinPath.ClientName.Client.Columns.ClientCoreClientId, clientCoreClientId);
                        insuredName = bcscysdm.GetClientName().BusinessName;
                    }
                }
                if (!string.IsNullOrEmpty(dba) && !string.IsNullOrEmpty(insuredName))
                {
                    // save a loop or two, if already got the first dba and insured name
                    break;
                }
            }

            // from region start to here 

            bool isCobraForCounty = false;
            bool isCobraOrRelatedForTypes = false; // for address & name types

            Biz.Company currentCompany = CompanyNumberObj.Company;
            isCobraForCounty = currentCompany.ClientSystemType.Code.Equals("Cobra", StringComparison.CurrentCultureIgnoreCase);
            isCobraOrRelatedForTypes = isCobraForCounty || currentCompany.ClientSystemType.Code.Equals("BCSasClientCore", StringComparison.CurrentCultureIgnoreCase);

            for (int i = 0; i < addressInfos.Length; i++)
            {
                structures.Import.AddressInfo ipAddrInfo = addressInfos[i];
                // cobra pads the zip to 9 digits, so addresses will not be equal
                if (isCobraOrRelatedForTypes)
                {
                    while (ipAddrInfo.Zip.Length < 9)
                    {
                        ipAddrInfo.Zip += "0";
                    }
                }
                if (ipAddrInfo.ClearanceAddressType != null)
                {
                    try
                    {
                        bcsDM.NewClientCoreAddressAddressType(Convert.ToInt32(newAddressIdsList[i]), bcsLookup.AddressTypes.FindByPropertyAddressType(ipAddrInfo.ClearanceAddressType), bizclient);
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Exception at evaluating addresses {0} {1} {2} {3} {4} ", clientCoreClientId, ipAddrInfo.HouseNumber, ipAddrInfo.ADDR1, ipAddrInfo.CityName, ipAddrInfo.StateCode);

                        Console.WriteLine(newAddressIdsList.Count);
                        Console.WriteLine(addressInfos.Length);

                        Console.WriteLine(nameInfos[0].BusinessName);

                        LogCentral.Current.LogError(
                            string.Format(
                            "Exception at evaluating addresses {0} {1} {2} {3} {4}. AddressIdsList count id {5}, addressInfos Count is {6}. First client name is {7}.",
                            clientCoreClientId, ipAddrInfo.HouseNumber, ipAddrInfo.ADDR1, ipAddrInfo.CityName, ipAddrInfo.StateCode,
                            newAddressIdsList.Count,
                            addressInfos.Length,
                            nameInfos[0].BusinessName));

                        // TODO: eat up the exception for now
                        //throw;
                    }
                }

                if (isCobraForCounty)
                {
                    if (Common.IsNotNullOrEmpty(ipAddrInfo.County))
                    {
                        Biz.CobraAddressCounty cobraAddressCounty = bcsDM.NewCobraAddressCounty(Convert.ToInt32(newAddressIdsList[i]), bizclient);
                        cobraAddressCounty.County = ipAddrInfo.County;
                    }
                }
            }
            for (int i = 0; i < nameInfos.Length; i++)
            {
                structures.Import.NameInfo ipNameInfo = nameInfos[i];
                if (ipNameInfo.ClearanceNameType == "DBA")
                {
                    // the client's first name dba should be the submission's name dba
                    if (string.IsNullOrEmpty(dba))
                        dba = ipNameInfo.BusinessName;
                }
                if (ipNameInfo.ClearanceNameType == "Insured")
                {
                    // the client's first name insured should be the submission's name insured
                    if (string.IsNullOrEmpty(insuredName))
                        insuredName = ipNameInfo.BusinessName;
                }
                if (ipNameInfo.ClearanceNameType != null)
                {
                    try
                    {
                        Biz.ClientNameType temp = bcsLookup.ClientNameTypes.FindByNameType(ipNameInfo.ClearanceNameType);
                        bcsDM.NewClientCoreNameClientNameType(Convert.ToInt32(newNameIdsList[i]), bizclient, temp);
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Exception at evaluating names {0} {1}", clientCoreClientId, ipNameInfo.BusinessName);

                        Console.WriteLine(newNameIdsList.Count);
                        Console.WriteLine(nameInfos.Length);
                        LogCentral.Current.LogError(
                            string.Format("Exception at evaluating names {0} {1}. newNameIds.Count is {2}, nameInfos.Length is {3}. First client name is {7}.",
                            clientCoreClientId, ipNameInfo.BusinessName, newNameIdsList.Count, nameInfos.Length, nameInfos[0].BusinessName));

                        // TODO: eat up the exception for now
                        //throw;
                    }
                }
            }

            if (!Properties.Settings.Default.BatchCommit)
                bcsDM.CommitAll();
            #endregion
                       
            DateTime endTime = DateTime.Now;
            Common.LogTimeTaken("bcs client add", startTime, endTime);
        }

        private static bool NameIsEqual(BTS.ClientCore.Wrapper.Structures.Info.Name newName, BTS.ClientCore.Wrapper.Structures.Import.NameInfo ipNameInfo)
        {
            bool b = false;

            if (newName.NameBusiness != null)
            {
                if (newName.NameBusiness.Trim() == ipNameInfo.BusinessName.Trim())
                    b = true;
            }
            else
            {
                if (newName.NameFirst == ipNameInfo.FirstName && newName.NameLast == ipNameInfo.LastName && newName.NameMiddle == ipNameInfo.MiddleName &&
                     newName.NamePrefix == ipNameInfo.NamePrefix && newName.NameSuffix == ipNameInfo.NameSuffix)
                {
                    //return true; 
                    b = true;
                }
            }
            return b;
        }

        private static bool AddressIsEqual(BTS.ClientCore.Wrapper.Structures.Info.Address newAddr, BTS.ClientCore.Wrapper.Structures.Import.AddressInfo ipAddrInfo)
        {
            bool b = false;


            if (newAddr.Address1 == null)
                newAddr.Address1 = string.Empty;
            if (newAddr.Address2 == null)
                newAddr.Address2 = string.Empty;
            if (newAddr.City == null)
                newAddr.City = string.Empty;
            if (newAddr.HouseNumber == null)
                newAddr.HouseNumber = string.Empty;
            if (newAddr.StateProvinceId == null)
                newAddr.StateProvinceId = string.Empty;
            if (newAddr.PostalCode == null)
                newAddr.PostalCode = string.Empty;


            if (ipAddrInfo.ADDR1 == null)
                ipAddrInfo.ADDR1 = string.Empty;
            if (ipAddrInfo.ADDR2 == null)
                ipAddrInfo.ADDR2 = string.Empty;
            if (ipAddrInfo.CityName == null)
                ipAddrInfo.CityName = string.Empty;
            if (ipAddrInfo.HouseNumber == null)
                ipAddrInfo.HouseNumber = string.Empty;
            if (ipAddrInfo.StateCode == null)
                ipAddrInfo.StateCode = string.Empty;
            if (ipAddrInfo.Zip == null)
                ipAddrInfo.Zip = string.Empty;



            if (newAddr.Address1.Trim() == ipAddrInfo.ADDR1.Trim() &&
                newAddr.Address2.Trim() == ipAddrInfo.ADDR2.Trim() &&
                newAddr.City.Trim() == ipAddrInfo.CityName.Trim() &&
                newAddr.HouseNumber.Trim() == ipAddrInfo.HouseNumber.Trim() &&
                newAddr.StateProvinceId.Trim() == ipAddrInfo.StateCode.Trim() &&
                newAddr.PostalCode.Trim() == ipAddrInfo.Zip.Trim())
            {
                //return true; 
                b = true;
            }
            return b;
        }
    }
}
