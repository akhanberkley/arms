namespace BTS.CacheEngine.V2
{
    public interface ICacheable
    {
        /// <summary>
        /// Signature for method to determine time remaining in cache.
        /// </summary>
        void TimeRemainingInCache(string key);
    }
}
