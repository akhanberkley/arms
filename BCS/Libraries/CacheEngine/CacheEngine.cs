using System;
using System.Web;
using System.Web.Caching;

namespace BTS.CacheEngine.V2
{
    /// <summary>
    /// CacheEngine Class.
    /// </summary>
    [Serializable()]
    public class CacheEngine
    {
        #region Private Member Variables
        /// <summary>
        /// 
        /// </summary>
        private string _Key;

        /// <summary>
        /// 
        /// </summary>
        private static HttpContext _Context = HttpContext.Current;

        /// <summary>
        /// 
        /// </summary>
        private static CacheItemRemovedCallback _ItemRemoved = null;

        /// <summary>
        /// 
        /// </summary>
        private int _MinutesToKeepObjectInCache;
        #endregion

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public CacheEngine() { }

        /// <summary>
        /// Initializes a new instance of the CacheEngine class using the
        /// specified key.
        /// </summary>
        /// <param name="key">Initializes Key property</param>
        public CacheEngine(string key)
        {
            this.Key = key;
        }

        /// <summary>
        /// Initializes a new instance of the CacheEngine class using the
        /// specified minutes to keep object in cache.
        /// </summary>
        /// <param name="minutesToKeepObjectInCache">Initializes MinutesToKeepObjectInCache property</param>
        /// <remarks>If minutesToKeepObjectInCache is never passed when initializing the CacheEngine
        /// object, we will default in sixty minutes.</remarks>
        public CacheEngine(int minutesToKeepObjectInCache)
        {
            this.MinutesToKeepObjectInCache = minutesToKeepObjectInCache;
        }

        /// <summary>
        /// Initializes a new instance of the CacheEngine class using the
        /// specified key and minutes to keep object in cache.
        /// </summary>
        /// <param name="key">Initializes Key property</param>
        /// <param name="minutesToKeepObjectInCache">Initializes MinutesToKeepObjectInCache property</param>
        /// <remarks>If minutesToKeepObjectInCache is never passed when initializing the CacheEngine
        /// object, we will default in sixty minutes.</remarks>
        public CacheEngine(string key, int minutesToKeepObjectInCache)
        {
            this.Key = key;
            this.MinutesToKeepObjectInCache = minutesToKeepObjectInCache;
        }
        #endregion

        #region Public Members
        /// <summary>
        /// Determines if item exists in cache
        /// </summary>
        /// <param name="cache">Cache object identifier of item to check for its existance</param>
        /// <returns>Boolean, true indicates item exists in cache</returns>
        /// <exception cref="System.InvalidCastException">Unable to cast cache item as
        /// an object</exception>
        public bool CachedItemExist(ref CacheEngine cache)
        {
            try
            {
                object cacheItem = (object)_Context.Cache[cache.Key];

                if (cacheItem == null)
                {
                    return false;
                }

                return true;
            }
            catch (InvalidCastException ic)
            {
                throw (new InvalidCastException("Invalid cast.", ic));
            }
        }

        /// <summary>
        /// Determines if item exists in cache
        /// </summary>
        /// <param name="key">identifier of item to check for its existance</param>
        /// <returns>Boolean, true indicates item exists in cache</returns>
        /// <exception cref="System.InvalidCastException">Unable to cast cache item as
        /// an object</exception>
        public bool CachedItemExist(string key)
        {
            try
            {
                object cacheItem = (object)_Context.Cache[key];

                if (cacheItem == null)
                {
                    return false;
                }

                return true;
            }
            catch (InvalidCastException ic)
            {
                throw (new InvalidCastException("Invalid cast.", ic));
            }
        }

        /// <summary>
        /// Get an object from cache
        /// </summary>
        /// <param name="key">Identifier of item to get from cache</param>
        /// <returns>Cached item identified by [key]</returns>
        /// <exception cref="System.InvalidCastException">Unable to cast cache item as
        /// an object</exception>
        /// <exception cref="System.Exception">Unable to get object from cache</exception>
        public object GetCachedItem(string key)
        {
            try
            {
                object cacheItem = (object)_Context.Cache[key];
                //_ItemRemoved = new
                //    CacheItemRemovedCallback(this.ItemRemovedFromCache);

                //Trace.WriteLine("retrieved object from cache identified by key: " + key);

                return cacheItem;
            }
            catch (InvalidCastException ic)
            {
                throw (new InvalidCastException("Invalid cast.", ic));
            }
            catch (Exception ex)
            {
                throw (new Exception("Error retrieving object from cache.", ex));
            }
        }

        /// <summary>
        /// Get an object from cache, if [item] is null then we will add it to cache
        /// </summary>
        /// <param name="key">Identifier of item to get from cache</param>
        /// <param name="item">Item to get from cache</param>
        /// <returns>Cached item identified by [key]</returns>
        /// <exception cref="System.InvalidCastException">Unable to cast cache item as
        /// an object</exception>
        /// <exception cref="System.Exception">Unable to get object from cache</exception>
        public object GetCachedItem(string key, object item)
        {
            try
            {
                object cacheItem = (object)_Context.Cache[key];
                //_ItemRemoved = new
                //    CacheItemRemovedCallback(this.ItemRemovedFromCache);

                if (cacheItem == null)
                {
                    AddItemToCache(key, item);
                    cacheItem = (object)_Context.Cache[key];
                }

                return cacheItem;
            }
            catch (InvalidCastException ic)
            {
                throw (new InvalidCastException("Invalid cast.", ic));
            }
            catch (Exception ex)
            {
                throw (new Exception("Error retrieving object from cache.", ex));
            }
        }

        /// <summary>
        /// Add item to cache
        /// </summary>
        /// <param name="key">Identifier of item being added to cache</param>
        /// <param name="item">Item to be added to cache</param>
        /// <exception cref="System.Exception">Unable to add object to cache</exception>
        public void AddItemToCache(string key, object item)
        {
            AddItemToCache(key, item, null);
        }

        /// <summary>
        /// Add item to cache
        /// </summary>
        /// <param name="key">Identifier of item being added to cache</param>
        /// <param name="item">Item to be added to cache</param>
        /// <param name="dependency">Item dependent on</param>
        /// <exception cref="System.Exception">Unable to add object to cache</exception>
        public void AddItemToCache(string key, object item, CacheDependency dependency)
        {
            try
            {
                lock (typeof(Cache))
                {
                    _Context.Cache.Insert(
                        key,
                        item,
                        dependency,
                        Cache.NoAbsoluteExpiration,
                        new TimeSpan(0, this.MinutesToKeepObjectInCache, 0),
                        CacheItemPriority.High,
                        _ItemRemoved
                        );
                }
            }
            catch (Exception ex)
            {
                throw (new Exception("Error adding object to cache.", ex));
            }
        }

        /// <summary>
        /// Remove item from cache
        /// </summary>
        /// <param name="key">Identifier of item to be removed</param>
        public void RemoveItemFromCache(string key)
        {
            try
            {
                lock (typeof(Cache))
                {
                    _Context.Cache.Remove(key);
                }
            }
            catch (Exception ex)
            {
                throw (new Exception("Error removing object from cache.", ex));
            }
        }
        #endregion

        #region Private Members
        /// <summary>
        /// Resets item in cache (removes)
        /// </summary>
        /// <param name="key">Identifier of item in cache to be reset</param>
        /// <exception cref="System.Exception">Unable to reset item in cache</exception>
        private void ResetCachedItem(string key)
        {
            try
            {
                if (_Context.Cache[key] != null)
                {
                    lock (typeof(Cache))
                    {
                        _Context.Cache.Remove(key);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new Exception("Error resetting object in cache.", ex));
            }
        }

        /// <summary>
        /// Callback method to call when object is removed from cache
        /// </summary>
        /// <param name="key">Unique key to identify object in cache</param>
        /// <param name="item">Item to be added to cache</param>
        /// <param name="reason"></param>
        /// <remarks>This method will be called when ResetCachedItem is called</remarks>
        private void ItemRemovedFromCache(string key, object item, System.Web.Caching.CacheItemRemovedReason reason)
        {
            if (reason == CacheItemRemovedReason.Expired)
                this.AddItemToCache(key, item);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Unique key identifying object in cache
        /// </summary>
        /// <value>Key accesses the value of the _Key member variable</value>
        public string Key
        {
            get
            {
                return this._Key;
            }
            set
            {
                this._Key = value;
            }
        }

        /// <summary>
        /// Number of minutes to keep object in cache
        /// </summary>
        /// <remarks>If MinutesToKeepObjectInCache is never set when adding items to cache, we
        /// default in sixty minutes.</remarks>
        /// <value>MinutesToKeepObjectInCache accesses the value of the _MinutesToKeepObjectInCache member variable</value>
        public int MinutesToKeepObjectInCache
        {
            get
            {
                return this._MinutesToKeepObjectInCache;
            }
            set
            {
                this._MinutesToKeepObjectInCache = value;
            }
        }
        #endregion
    }
}
