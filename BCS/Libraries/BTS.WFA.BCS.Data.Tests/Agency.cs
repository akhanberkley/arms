﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Diagnostics;

namespace BTS.WFA.BCS.Data.Tests
{
    [TestClass]
    public class Agency
    {
        [TestMethod]
        public void TestSqlDatabase()
        {
            using (var db = new SqlDb())
            {
                db.Database.Log = (s => Debug.Write(s));

                var agency = db.Agency.FirstOrDefault(a => a.AgencyNumber == "03276" && a.CompanyNumberId == 6);
                var lob = agency.AgencyLineOfBusiness.First();
                var personnel = lob.AgencyLineOfBusinessPersonUnderwritingRolePerson.ToList();
                
                Assert.IsTrue(agency != null);
             
                Assert.IsFalse(db.EntityHasChanges(agency));
                agency.AgencyNumber = "12345";
                Assert.IsTrue(db.EntityHasChanges(agency));
            }
        }

        [TestMethod]
        public void TestMockDatabase()
        {
            using (var db = new MockDb())
            {
                var agency = db.Agency.FirstOrDefault();

                Assert.IsTrue(agency == null);
            }
        }
    }
}
