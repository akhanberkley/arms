﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS.Data.Tests
{
    [TestClass]
    public class CompanyNumberCodeType
    {
        [TestMethod]
        public void CascadingDelete()
        {
            using (var db = new SqlDb())
            {
                db.Database.Log = (s => Debug.Write(s));

                using (var trans = db.GetTransaction())
                {
                    try
                    {
                        var codeTypeToRemove = db.CompanyNumberCodeType.FirstOrDefault(c => c.Id == 196);
                        codeTypeToRemove.DefaultCompanyNumberCodeId = null;
                        db.SaveChanges();

                        foreach (var c in codeTypeToRemove.CompanyNumberCode.ToArray())
                            db.CompanyNumberCode.Remove(c);
                        db.SaveChanges();

                        db.CompanyNumberCodeType.Remove(codeTypeToRemove);
                        db.SaveChanges();
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                        throw ex;
                    }
                }
            }
        }
    }
}
