using System;
using System.Collections.Generic;
using System.Text;
using BCS.Biz;

namespace BCS.APS
{
    public class BizHelper
    {
        public Company GetCompany(int id)
        {
            DataManager dm = GetDataManager();
            dm.QueryCriteria.And(JoinPath.Company.Columns.Id, id);
            return dm.GetCompany();
        }
        public Company GetCompany(string companyNumber)
        {
            DataManager dm = GetDataManager();
            dm.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            return dm.GetCompany();
        }

        private DataManager GetDataManager()
        {
            DataManager dm = new DataManager(Common.DSN);
            return dm;
        }
    }
}
