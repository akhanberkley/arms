using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.APS
{
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" +
        "", TypeName = "Security")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" +
        "", IsNullable = false)]
    public partial class Security : System.Web.Services.Protocols.SoapHeader
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://bts.services.wrberkley.com", ElementName = "dsik")]
        public string dsik;

        /// <remarks/>
        [System.Xml.Serialization.XmlAnyAttributeAttribute()]
        public System.Xml.XmlAttribute[] AnyAttr;

        public Security()
        {
        }

        public Security(string dsik, System.Xml.XmlAttribute[] anyAttr)
        {
            this.dsik = dsik;
            this.AnyAttr = anyAttr;
        }
    }
}
