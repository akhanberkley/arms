using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using System.Data;

namespace BCS.APS
{
    public enum AgencyTypes
    {
        MastersOnly,
        NonMastersOnly,
        Both
    }

    [ToolboxData("<{0}:AgencyDropDownList runat=server></{0}:AgencyDropDownList>")]
    public class AgencyDropDownList : BaseDropDown
    {
        #region Properties

        /// <summary>
        /// {0} - agency name, {1} - agency number
        /// </summary>        
        [Description("ex: number - name")]
        [Category("BCS")]
        public string DisplayFormat
        {
            get
            {
                return ViewState["DisplayFormat"] == null ? "{0} ({1})" : (string)ViewState["DisplayFormat"];
            }
            set
            {
                ViewState["DisplayFormat"] = value;
            }
        }

        [Category("BCS")]
        public int CompanyId
        {
            get
            {
                return ViewState["CompanyId"] == null ? 0 : (int)ViewState["CompanyId"];
            }
            set
            {
                ViewState["CompanyId"] = value;
            }
        }

        [Category("BCS")]
        public DateTime CancelDate
        {
            get
            {
                return ViewState["CancelDate"] == null ? DateTime.Now : (DateTime)ViewState["CancelDate"];
            }
            set
            {
                ViewState["CancelDate"] = value;
            }
        }

        [Description(
            "whether to add empty item at the beginning with empty value or '0' value. True adds empty value." +
            " Needed when used within data controls.")]
        [Category("BCS")]
        public bool ItemAsEmpty
        {
            set
            { ViewState["ItemAsEmpty"] = value; }
            get
            { return (bool?)ViewState["ItemAsEmpty"] ?? false; }
        }

        [Description("whether to load inactive agencies")]
        [Category("BCS")]
        public bool ActiveOnly
        {
            set
            { ViewState["ActiveOnly"] = value; }
            get
            { return (bool?)ViewState["ActiveOnly"] ?? false; }
        }

        [Description("whether to identify inactive agencies")]
        [Category("BCS")]
        public bool IdentifyInactive
        {
            set
            { ViewState["IdentifyInactive"] = value; }
            get
            { return (bool?)ViewState["IdentifyInactive"] ?? false; }
        }
        
        [Description("whether to exclude master agencies")]
        [Category("BCS")]
        public AgencyTypes AgencyType
        {
            set
            { ViewState["AgencyType"] = value; }
            get
            { return (AgencyTypes?)ViewState["AgencyType"] ?? AgencyTypes.NonMastersOnly; }
        }

        [Description("Gets or sets whether to bind for agencies associated for this person in PersonTypeRole.")]
        [Category("BCS")]
        public string PersonId
        {
            set
            { ViewState["PersonId"] = value; }
            get
            { return (string)ViewState["PersonId"]; }
        }

        [Description("Gets or sets the person type for which to binds agencies for. Currently only supports APSProxy.companyPersonnel, underwriter, analyst etc.")]
        [Category("BCS")]
        public PersonTypes PersonType
        {
            set
            { ViewState["PersonType"] = value; }
            get
            { return (PersonTypes?)ViewState["PersonType"] ?? PersonTypes.None; }
        }
        #endregion

        public override void DataBind()
        {
            base.Items.Clear();
            List<ListItem> listItems = new List<ListItem>();
            listItems.Add(ItemAsEmpty ? new ListItem(string.Empty, string.Empty) : new ListItem("", "0"));

            if (string.IsNullOrEmpty(PersonId)) // implying to bind a normal agency dropdown
            {
                DataView dtAgencies = new DataSources().GetAgencies(CompanyId, string.Empty, string.Empty, string.Empty, ActiveOnly ? CancelDate : DateTime.MinValue);
                foreach (DataRow var in dtAgencies.Table.Rows)
                {
                    if (CountParams(DisplayFormat) == 2)
                        listItems.Add(new ListItem(string.Format(DisplayFormat, var["shortName"], var["agencyCode"]), var["id"].ToString()));

                    if (CountParams(DisplayFormat) == 4)
                        listItems.Add(new ListItem(string.Format(DisplayFormat, var["shortName"], var["agencyCode"], var["homeCity"], var["homeState"]),
                        var["id"].ToString()));

                    if (CountParams(DisplayFormat) == 5) // TODO: zip
                        listItems.Add(new ListItem(string.Format(DisplayFormat, var["shortName"], var["agencyCode"], var["homeCity"], var["homeState"]),
                        var["id"].ToString()));

                    if (IsInactive(var))
                    {
                        listItems[listItems.Count - 1].Attributes.Add("class", "inactiveListItem");
                    }
                }
            }
            else // implying to bind agencies associated with a person
            {
                DataView dtAgencies = new DataSources().GetAgenciesForPerson(CompanyId, PersonId, PersonType);
                foreach (DataRow var in dtAgencies.Table.Rows)
                {
                    if (CountParams(DisplayFormat) == 2)
                        listItems.Add(new ListItem(string.Format(DisplayFormat, var["shortName"], var["agencyCode"]), var["id"].ToString()));

                    if (CountParams(DisplayFormat) == 4)
                        listItems.Add(new ListItem(string.Format(DisplayFormat, var["shortName"], var["agencyCode"], var["homeCity"], var["homeState"]),
                        var["id"].ToString()));

                    if (CountParams(DisplayFormat) == 5) // TODO: zip
                        listItems.Add(new ListItem(string.Format(DisplayFormat, var["shortName"], var["agencyCode"], var["homeCity"], var["homeState"]),
                        var["id"].ToString()));

                    if (IsInactive(var))
                    {
                        listItems[listItems.Count - 1].Attributes.Add("class", "inactiveListItem");
                    }
                }
            }
            base.Items.AddRange(UIHelper.SortListItems(listItems));
            //base.DataBind();
        }

        private bool IsInactive(DataRow var)
        {
            // TODO: using a trick (trying to use DataTable.Select, query already used in DataSources.GetActiveAgencies), instead of if statements, hope it works
            DataTable dt = var.Table.Clone();
            dt.Rows.Add(var.ItemArray);

            string space = " ";
            // appointmentDate expression, (appointmentDate not specified) OR appointmentDate specified AND is earlier than as of date
            string effectDateExpression = string.Format("({0} = 0 OR ({0} = 1 AND {1} <= '{2}')){3}", "appointmentDateSpecified", "appointmentDate", CancelDate, space);
            // renewalCancelDate expression, (renewalCancelDate not specified) OR renewalCancelDate specified AND is farther than as of date
            string renewlDateExpression = string.Format("({0} = 0 OR ({0} = 1 AND {1} >= '{2}')){3}", "renewalCancelDateSpecified", "renewalCancelDate", CancelDate, space);
            // newCancelDate expression, (newCancelDate not specified) OR newCancelDate specified AND is farther than as of date
            string cancelDateExpression = string.Format("({0} = 0 OR ({0} = 1 AND {1} >= '{2}')){3}", "newCancelDateSpecified", "newCancelDate", CancelDate, space);
            string filterExpression = string.Format("{0} AND ({1} OR {2})", effectDateExpression, renewlDateExpression, cancelDateExpression);
            DataRow[] filteredRows = dt.Select(filterExpression);

            return filteredRows.Length == 0;
        }

        private int CountParams(string formatString)
        {
            System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(@"{\d*}", System.Text.RegularExpressions.RegexOptions.None);
            System.Text.RegularExpressions.MatchCollection matches = r.Matches(formatString);
            return matches.Count;
        }
    }
}
