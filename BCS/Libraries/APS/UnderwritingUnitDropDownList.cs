using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCS.APS
{
    [ToolboxData("<{0}:UnderwritingUnitDropDownList runat=server></{0}:UnderwritingUnitDropDownList>")]
    public class UnderwritingUnitDropDownList : BaseDropDown
    {
        [Category("BCS")]
        public int CompanyId
        {
            get
            {
                return ViewState["CompanyId"] == null ? 0 : (int)ViewState["CompanyId"];
            }
            set
            {
                ViewState["CompanyId"] = value;
            }
        }

        [Category("BCS")]
        public long AgencyId
        {
            get
            { return (long?)ViewState["AgencyId"] ?? 0; }
            set
            {
                ViewState["AgencyId"] = value;
            }
        }
        public override void DataBind()
        {

            base.Items.Clear();

            List<ListItem> listItems = new List<ListItem>();
            listItems.Add(new ListItem("", "0"));

            agencyUnderwritingUnit[] agencyUnderwritingUnits = new DataSources().GetUnderwritingUnits(CompanyId, AgencyId);;
            if (agencyUnderwritingUnits != null)
            {
                foreach (agencyUnderwritingUnit var in agencyUnderwritingUnits)
                {
                    listItems.Add(new ListItem(string.Format("{0}-{1}", var.UnderwritingUnit.Code, var.UnderwritingUnit.Description), var.UnderwritingUnit.Id.ToString()));
                }
            }
            base.Items.AddRange(UIHelper.SortListItems(listItems.ToArray()));
            base.DataBind();
        }
    }
}
