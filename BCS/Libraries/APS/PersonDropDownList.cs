using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCS.APS
{
    public enum PersonTypes
    {
        [StringValue("Producer")]
        Agent,
        [StringValue("Assistant UW")]
        Analyst,
        [StringValue("Security Contact")]
        Contact,
        [StringValue("")]
        None,
        [StringValue("Technician")]
        Technician,
        [StringValue("Underwriter")]
        Underwriter
    }
    public enum BindTypes
    {
        AgencyPersons,
        AgencyUnderwritingUnitPersons,
        CompanyPersons
    }
    [ToolboxData("<{0}:PersonDropDownList runat=server></{0}:PersonDropDownList>")]
    public class PersonDropDownList : BaseDropDown
    {
        [Category("BCS")]
        public PersonTypes PersonType
        {
            set
            { ViewState["PersonType"] = value; }
            get
            { return (PersonTypes?)ViewState["PersonType"] ?? PersonTypes.None; }
        }
        [Category("BCS")]
        public BindTypes BindType
        {
            set
            { ViewState["BindType"] = value; }
            get
            { return (BindTypes?)ViewState["BindType"] ?? BindTypes.AgencyUnderwritingUnitPersons; }
        }
        
        [Category("BCS")]
        public int CompanyId
        {
            get
            {
                return ViewState["CompanyId"] == null ? 0 : (int)ViewState["CompanyId"];
            }
            set
            {
                ViewState["CompanyId"] = value;
            }
        }

        [Category("BCS")]
        public long AgencyId
        {
            get
            { return (long?)ViewState["AgencyId"] ?? 0; }
            set
            {
                ViewState["AgencyId"] = value;
            }
        }

        [Category("BCS")]
        public long UnderwritingUnitId
        {
            get
            { return (long?)ViewState["UnderwritingUnitId"] ?? 0; }
            set
            {
                ViewState["UnderwritingUnitId"] = value;
            }
        }

        public override void DataBind()
        {
            base.Items.Clear();
            List<ListItem> listItems = new List<ListItem>();
            listItems.Add(new ListItem(string.Empty, string.Empty));

            #region validation

            #region company unknown
            if (CompanyId == 0)
            {
                base.Items.AddRange(listItems.ToArray());
                base.DataBind();
                return;
            }
            #endregion
            #region role unknown
            if (PersonType == PersonTypes.None)
            {
                base.Items.AddRange(listItems.ToArray());
                base.DataBind();
                return;
            } 
            #endregion

            switch (BindType)
            {
                case BindTypes.AgencyPersons:
                    {
                        if(AgencyId ==0)
                        {
                            base.Items.AddRange(listItems.ToArray());
                            base.DataBind();
                            return;
                        }
                    }
                    break;
                case BindTypes.AgencyUnderwritingUnitPersons:
                    {
                        if (AgencyId == 0 || UnderwritingUnitId == 0)
                        {
                            base.Items.AddRange(listItems.ToArray());
                            base.DataBind();
                            return;
                        }
                    }
                    break;
                case BindTypes.CompanyPersons:
                    { }
                    break;
                default:
                    {
                        base.Items.AddRange(listItems.ToArray());
                        base.DataBind();
                        return;
                    }
            }


            //if (
            //        CompanyId == 0 // if company not initialized
            //        || AgencyId == 0 // OR agency id not initialized
            //        || PersonType == PersonTypes.None  // person type is not passed
            //        || ((PersonType != PersonTypes.Agent && PersonType != PersonTypes.Contact) && UnderwritingUnitId == 0) // underwriters and analysts are underwriting unit dependent, whereas agents and contacts are not
            //        )
            //{
            //    base.Items.AddRange(listItems.ToArray());
            //    return;
            //}
            #endregion
            List<Objects.Person> personnel = new DataSources().GetPersons(CompanyId, AgencyId, UnderwritingUnitId, PersonType);
            if (null != personnel)
            {
                foreach (Objects.Person var in personnel)
                {
                    listItems.Add(new ListItem(string.Format("{0}, {1}", var.LastName, var.FirstName), var.Id));
                }
            }
            base.Items.AddRange(UIHelper.SortListItems(listItems));
            base.DataBind();
        }
    }
}
