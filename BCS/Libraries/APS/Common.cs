using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using BCS.Biz;
using System.Net;

namespace BCS.APS
{
    static class Common
    {
        public const string DateFormat = "MM/dd/yyyy";
        internal static string DSN
        {
            get
            {
                return GetSafeConnectionString();
            }
        }
        internal static bool DoLog
        {
            get
            {
                return GetSafeBooleanAppSetting("LogAPSResponseTimes");
            }
        }

        private static bool GetSafeBooleanAppSetting(string key)
        {
            try
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings[key]);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static string GetSafeConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DSN"].ConnectionString;
        }

        internal static APSHelper GetAPSProxy(Company company)
        {
            if (company == null || string.IsNullOrEmpty(company.APSURL))
                return null;
            return new APSHelper(company.APSURL, company.APSDSIK);
        }

        internal static APSHelper GetAPSProxy(int companyId)
        {
            Company company = new BizHelper().GetCompany(companyId);
            return GetAPSProxy(company);
        }
        internal static APSHelper GetAPSProxy(string companyNumber)
        {
            Company company = new BizHelper().GetCompany(companyNumber);
            return GetAPSProxy(company);
        }
    }

    public class APSHelper : BCS.APS.APSService
    {
        public APSHelper(string endPointURL, string dsik)
        {
            base.Url = endPointURL;
            base.SecurityValue = new Security();
            base.SecurityValue.dsik = dsik;
        }

        protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
        {
            //return base.GetWebRequest(uri);
            WebRequest request = base.GetWebRequest(uri);
            (request as HttpWebRequest).KeepAlive = false;
            return request;
        }
    }
}
