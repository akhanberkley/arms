using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace BCS.APS
{
    public class UIHelper
    {
        public static ListItem[] SortListItems(List<ListItem> listItems)
        {
            listItems.Sort(new ListItemComparer());
            return listItems.ToArray();
        }
        public static ListItem[] SortListItems(ListItem[] listItems)
        {
            List<ListItem> gList = new List<ListItem>(listItems);
            return SortListItems(gList);
        }

        public static ListItem[] SortListItems(ListItemCollection lic)
        {
            List<ListItem> gList = new List<ListItem>(lic.Count);
            ListItem[] listItems = new ListItem[lic.Count];
            lic.CopyTo(listItems, 0);
            gList.AddRange(listItems);

            return SortListItems(gList);
        }
    }
    public class ListItemComparer : IComparer<ListItem>
    {
        #region IComparer<ListItem> Members

        public int Compare(ListItem xli, ListItem yli)
        {
            int res = ((new System.Collections.CaseInsensitiveComparer()).Compare(xli.Text, yli.Text));
            return res;
        }

        #endregion
    }
}
