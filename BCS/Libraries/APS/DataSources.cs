using System;
using System.Collections.Generic;
using System.Text;
using BCS.Biz;
using System.Data;
using System.Reflection;
using BTS.LogFramework;

namespace BCS.APS
{
    public class DataSources
    {
        #region Agency
        private DataTable BuildAgencyDataTable()
        {
            DataTable dtAgency = new DataTable("Agency");
            Type agencyType = typeof(agency);
            PropertyInfo[] properties = agencyType.GetProperties();
            foreach (PropertyInfo pInfo in properties)
            {
                DataColumn dCol = new DataColumn(pInfo.Name, pInfo.PropertyType);
                if (pInfo.PropertyType == typeof(usState))
                {
                    // exception case - agency state, none others stand out
                    dCol.DataType = typeof(string);
                }
                dtAgency.Columns.Add(dCol);
            }
            return dtAgency;
        }
        private DataTable BuildAgencyViewDataTable()
        {
            DataTable dtAgencyView = new DataTable("AgencyView");
            Type agencyViewType = typeof(agencyView);
            PropertyInfo[] properties = agencyViewType.GetProperties();
            foreach (PropertyInfo pInfo in properties)
            {
                DataColumn dCol = new DataColumn(pInfo.Name, pInfo.PropertyType);
                if (pInfo.PropertyType == typeof(usState))
                {
                    // exception case - agency state, none others stand out
                    dCol.DataType = typeof(string);
                }
                dtAgencyView.Columns.Add(dCol);
            }
            DataColumn col4Comments = new DataColumn("Comments", typeof(string));
            dtAgencyView.Columns.Add(col4Comments);
            return dtAgencyView;
        }
        private DataTable PopulateAgencies(DataTable dtAgency, agency[] agencies)
        {
            foreach (agency agencyVar in agencies)
            {
                List<object> objArray = new List<object>(dtAgency.Columns.Count);
                foreach (DataColumn var in dtAgency.Columns)
                {
                    PropertyInfo pInfo = agencyVar.GetType().GetProperty(var.ColumnName);
                    if (pInfo == null)
                        continue;
                    if (pInfo.PropertyType == typeof(usState))
                    {
                        object oVar = pInfo.GetValue(agencyVar, null);
                        usState stateInfo = oVar as usState;
                        objArray.Add(oVar == null ? string.Empty : stateInfo.Label);
                    }
                    else
                    {
                        object oVar = pInfo.GetValue(agencyVar, null);
                        objArray.Add(oVar);
                    }
                }
                dtAgency.Rows.Add(objArray.ToArray());
            }
            return dtAgency;
        }


        // TODO: Note: returning as datatables, better might be to return objects themselves. see hint below
        // hint : after populating, add a rowindex col to the, fill it sequentially. After filtering based on row index pull that agency at that index
        // contd..: add it to a new array/collection and return. Applies to all Get/Fetch/Search methods
        public DataView GetAgencies(int companyId, string shortName, string agencyCode)
        {
            DataTable dtAgency = BuildAgencyDataTable();

            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return dtAgency.DefaultView;

            agency[] agencyArray = apsService.getAllAgencies();

            DataTable dtAgencyPopulated = PopulateAgencies(dtAgency, agencyArray);

            if (string.IsNullOrEmpty(shortName) && string.IsNullOrEmpty(agencyCode))
            {
                dtAgencyPopulated.DefaultView.Sort = Mappings.AgencyName;
                // if no filters are passed, return
                return dtAgencyPopulated.DefaultView;
            }

            DataTable dtFiltered = dtAgencyPopulated.Clone();
            string filterExpression = string.Format("shortName like '%{0}%' AND agencyCode like '%{1}%'", shortName, agencyCode);
            DataRow[] filteredRows = dtAgencyPopulated.Select(filterExpression);
            foreach (DataRow var in filteredRows)
            {
                dtFiltered.Rows.Add(var.ItemArray);
            }
            dtFiltered.DefaultView.Sort = Mappings.AgencyName;
            return dtFiltered.DefaultView;
        }
        public DataView GetAgencies(int companyId, string agencyCode, string city, string state, DateTime asOfDate)
        {
            DataTable dtAgency = BuildAgencyDataTable();

            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return dtAgency.DefaultView;

            agency[] agencyArray = apsService.getAllAgencies();

            DataTable dtAgencyPopulated = PopulateAgencies(dtAgency, agencyArray);

            if (string.IsNullOrEmpty(agencyCode) && string.IsNullOrEmpty(state) && string.IsNullOrEmpty(state) && asOfDate == DateTime.MinValue)
            {
                dtAgencyPopulated.DefaultView.Sort = Mappings.AgencyName;
                // if no filters are passed, return
                return dtAgencyPopulated.DefaultView;
            }

            DataTable dtFiltered = dtAgencyPopulated.Clone();
            //string filterExpression = string.Format("agencyCode like '%{0}%' AND ( (homeCity IS NULL) OR (homeCity like '%{1}%' ) ) AND ( (homeState IS NULL) OR (homeState like '%{2}%') ) ", agencyCode, city, state);
            string cityExpression = string.Format("{0}", string.IsNullOrEmpty(city) ? "( (homeCity IS NULL) OR (homeCity like '%%' ) )" : string.Format("(homeCity like '%{0}%' )", city));
            string stateExpression = string.Format("{0}", string.IsNullOrEmpty(state) ? "( (homeState IS NULL) OR (homeState like '%%' ) )" : string.Format("(homeState like '%{0}%' )", state));
            string filterExpression = string.Format("agencyCode like '%{0}%' AND {1} AND {2} ", agencyCode, cityExpression, stateExpression);
            if (asOfDate > DateTime.MinValue)
            {
                string space = " ";

                // appointmentDate expression, (appointmentDate not specified) OR appointmentDate specified AND is earlier than as of date
                string effectDateExpression = string.Format("({0} = 0 OR ({0} = 1 AND {1} <= '{2}')){3}", "appointmentDateSpecified", "appointmentDate", asOfDate, space);
                // renewalCancelDate expression, (renewalCancelDate not specified) OR renewalCancelDate specified AND is father than as of date
                string renewlDateExpression = string.Format("({0} = 0 OR ({0} = 1 AND {1} >= '{2}')){3}", "renewalCancelDateSpecified", "renewalCancelDate", asOfDate, space);
                // newCancelDate expression, (newCancelDate not specified) OR newCancelDate specified AND is father than as of date
                string cancelDateExpression = string.Format("({0} = 0 OR ({0} = 1 AND {1} >= '{2}')){3}", "newCancelDateSpecified", "newCancelDate", asOfDate, space);

                string dateExpression = string.Format("{0} AND ({1} OR {2})", effectDateExpression, renewlDateExpression, cancelDateExpression);
                filterExpression = string.Format(" ({0}) AND ({1})", filterExpression, dateExpression);
            }
            DataRow[] filteredRows = dtAgencyPopulated.Select(filterExpression);
            foreach (DataRow var in filteredRows)
            {
                dtFiltered.Rows.Add(var.ItemArray);
            }
            dtFiltered.DefaultView.Sort = Mappings.AgencyName;
            return dtFiltered.DefaultView;
        }
        public DataView GetActiveAgencies(int companyId, DateTime asOfDate)
        {
            DataTable dtAgency = BuildAgencyDataTable();
            dtAgency.DefaultView.Sort = Mappings.AgencyName;

            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return dtAgency.DefaultView;

            agency[] agencyArray = apsService.getAllAgencies();

            DataTable dtAgencyPopulated = PopulateAgencies(dtAgency, agencyArray);

            if (asOfDate == DateTime.MinValue)
            {
                // if no filters are passed, return
                dtAgencyPopulated.DefaultView.Sort = Mappings.AgencyName;
                return dtAgencyPopulated.DefaultView;
            }

            string space = " ";

            DataTable dtFiltered = dtAgencyPopulated.Clone();
            // appointmentDate expression, (appointmentDate not specified) OR appointmentDate specified AND is earlier than as of date
            string effectDateExpression = string.Format("({0} = 0 OR ({0} = 1 AND {1} <= '{2}')){3}", "appointmentDateSpecified", "appointmentDate", asOfDate, space);
            // renewalCancelDate expression, (renewalCancelDate not specified) OR renewalCancelDate specified AND is farther than as of date
            string renewlDateExpression = string.Format("({0} = 0 OR ({0} = 1 AND {1} >= '{2}')){3}", "renewalCancelDateSpecified", "renewalCancelDate", asOfDate, space);
            // newCancelDate expression, (newCancelDate not specified) OR newCancelDate specified AND is farther than as of date
            string cancelDateExpression = string.Format("({0} = 0 OR ({0} = 1 AND {1} >= '{2}')){3}", "newCancelDateSpecified", "newCancelDate", asOfDate, space);
            string filterExpression = string.Format("{0} AND ({1} OR {2})", effectDateExpression, renewlDateExpression, cancelDateExpression);
            DataRow[] filteredRows = dtAgencyPopulated.Select(filterExpression);
            foreach (DataRow var in filteredRows)
            {
                dtFiltered.Rows.Add(var.ItemArray);
            }
            dtFiltered.DefaultView.Sort = Mappings.AgencyName;
            return dtFiltered.DefaultView;
        }

        public agency[] GetAgencyWithSlaves(string companyNumber, long agencyId)
        {
            DataTable dtAgency = BuildAgencyDataTable();

            APSHelper apsService = Common.GetAPSProxy(companyNumber);
            if (apsService == null)
                return null;

            agency[] agencyArray = apsService.getAgenciesUnderMasterAgency(agencyId, false);
            return agencyArray;
        }

        public string GetAgencyLicensedStates(int companyId, long agencyId)
        {
            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (apsService == null)
                return null;

            if (agencyId == 0)
                return string.Empty;

            DateTime startDTgetAgencyLicensedStates = DateTime.Now;
            agencyLicense[] licenses = apsService.getAgencyLicensedStates(agencyId.ToString());
            DateTime endDTgetAgencyLicensedStates = DateTime.Now;
            TimeSpan tsgetAgencyLicensedStates = endDTgetAgencyLicensedStates.Subtract(startDTgetAgencyLicensedStates);
            if (Common.DoLog)
            {
                LogCentral.Current.LogInfo(string.Format("tsgetAgencyLicensedStates {0}", tsgetAgencyLicensedStates.TotalSeconds)); 
            }
						 
            List<string> stateCodes = new List<string>(licenses.Length);
            foreach (agencyLicense var in licenses)
            {
                stateCodes.Add(var.LicenseState.Label);
            }
            return string.Join(",", stateCodes.ToArray());
        }

        //public agency GetReferredAgency(int companyId, long agencyId)
        //{
        //    APSHelper apsService = Common.GetAPSProxy(companyId);
        //    if (apsService == null)
        //        return null;

        //    agency referredAgency = apsService.getReferredAgency(agencyId.ToString());
        //    return referredAgency;
        //}

        public agency GetAgency(int companyId, long id)
        {
            DateTime startDTGetAgency = DateTime.Now;
            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return null;
            agency retAgency = apsService.getAgencyById(id);
            DateTime endDTGetAgency = DateTime.Now;
            TimeSpan tsGetAgency = endDTGetAgency.Subtract(startDTGetAgency);
            if (Common.DoLog)
            {
                LogCentral.Current.LogInfo(string.Format("tsGetAgency {0}", tsGetAgency.TotalSeconds)); 
            }
						 
            return retAgency;
        }

        public DataView GetAgenciesForPerson(int companyId, string personId, PersonTypes personType)
        {
            DataTable dtAgency = BuildAgencyDataTable();
            dtAgency.DefaultView.Sort = Mappings.AgencyName;

            if (string.IsNullOrEmpty(personId))
            { return dtAgency.DefaultView; }

            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return dtAgency.DefaultView;
            
            long roleId = GetRoleId(apsService, personType);
            agency[] agencies = apsService.getAgencyByRoleAndCompanyUserUW(0, personId, roleId, 0);
            DataTable dtAgencyPopulated = PopulateAgencies(dtAgency, agencies);
            dtAgencyPopulated.DefaultView.Sort = Mappings.AgencyName;
            return dtAgencyPopulated.DefaultView;
        }

        public agency[] GetMasterAgency(string companyNumber, long agencyId)
        {
            APSHelper apsService = Common.GetAPSProxy(companyNumber);
            if (null == apsService)
                return null;

            agency[] agency = apsService.getMasterAgencies(agencyId);
            return agency;
        }
        #endregion

        #region agencies paged
        public DataView GetAgenciesPaged(int companyId, string agencyCode, string agencyName, string city, string state, DateTime asOfDate, int pageNumber, int pageSize)
        {
            DateTime startDTGetAgenciesPaged = DateTime.Now;
            DataTable dtAgency = BuildAgencyDataTable();

            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return dtAgency.DefaultView;
            agency[] agencyArray = apsService.searchAgencies(agencyCode, agencyName, city, state, 0);
            //agency[] agencyArray = apsService.getAllAgenciesPaged(pageNumber, pageSize + 1);
            DataTable dtAgencyPopulated = PopulateAgencies(dtAgency, agencyArray);
            DateTime endDTGetAgenciesPaged = DateTime.Now;
            TimeSpan tsGetAgenciesPaged = endDTGetAgenciesPaged.Subtract(startDTGetAgenciesPaged);
            if (Common.DoLog)
            {
                LogCentral.Current.LogInfo(string.Format("tsGetAgenciesPaged {0}.", tsGetAgenciesPaged.TotalSeconds)); 
            }
						 
            return dtAgencyPopulated.DefaultView;
        }
        public DataView GetAgenciesViewPaged(int companyId, string agencyCode, string agencyName, string city, string state, DateTime asOfDate, int pageNumber, int pageSize)
        {
            DataTable dtAgency = BuildAgencyViewDataTable();

            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return dtAgency.DefaultView;
            DateTime startDTGetAgenciesViewPaged = DateTime.Now;
            agencyView[] agencyArray = apsService.searchAgenciesView(agencyCode, agencyName, city, state, 0, asOfDate, true, (pageNumber * pageSize) + 1, pageSize);
            DateTime endDTGetAgenciesViewPaged = DateTime.Now;
            TimeSpan tsGetAgenciesViewPaged = endDTGetAgenciesViewPaged.Subtract(startDTGetAgenciesViewPaged);
            if (Common.DoLog)
            {
                LogCentral.Current.LogInfo(string.Format("tsGetAgenciesViewPaged {0}", tsGetAgenciesViewPaged.TotalSeconds));
            }
            foreach (agencyView var in agencyArray)
            {
                if (var.AgencyNotes == null || var.AgencyNotes.Length == 0)
                {
                    var.AgencyNotes = new string[] { string.Empty };
                }
                else
                {
                    var.AgencyNotes = new string[] { var.AgencyNotes[0] };
                }
            }
            DataTable dtAgencyPopulated = PopulateAgencies(dtAgency, agencyArray);
						 
            return dtAgencyPopulated.DefaultView;
        }

        private DataTable PopulateAgencies(DataTable dtAgency, agencyView[] agencies)
                {
                    foreach (agencyView agencyVar in agencies)
                    {
                        List<object> objArray = new List<object>(dtAgency.Columns.Count);
                        foreach (DataColumn var in dtAgency.Columns)
                        {
                            PropertyInfo pInfo = agencyVar.GetType().GetProperty(var.ColumnName);
                            if (pInfo == null)
                            {
                                if (var.ColumnName == "Comments")
                                {
                                    objArray.Add(agencyVar.AgencyNotes[0]);
                                }
                                continue;
                            }
                            if (pInfo.PropertyType == typeof(usState))
                            {
                                object oVar = pInfo.GetValue(agencyVar, null);
                                usState stateInfo = oVar as usState;
                                objArray.Add(oVar == null ? string.Empty : stateInfo.Label);
                            }
                            else
                            {
                                object oVar = pInfo.GetValue(agencyVar, null);
                                objArray.Add(oVar);
                            }
                        }
                        dtAgency.Rows.Add(objArray.ToArray());
                    }
                    return dtAgency;
                }
        #endregion

        #region Underwriting Unit
        public agencyUnderwritingUnit[] GetUnderwritingUnits(int companyId, long agencyId)
        {
            if (companyId == 0 || agencyId == 0)
                return null;
            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return null;
            try
            {
                // this call has issues when invalid/non-existing ids are sent, so try catch
                return apsService.getAgencyUnderwritingUnits(agencyId.ToString());
            }
            catch (Exception)
            {
                return null;
            }
        }
        public underwritingUnit GetUnderwritingUnit(int companyId, long uwUnitId)
        {
            if (companyId == 0 || uwUnitId == 0)
                return null;
            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return null;
            try
            {
                // this call has issues when invalid/non-existing ids are sent, so try catch
                return apsService.searchUnderwritingUnit(uwUnitId, string.Empty, string.Empty)[0];
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion

        #region Persons

        public Objects.Person GetPerson(int companyId, string id, PersonTypes pType)
        {
            if (pType == PersonTypes.None)
                return null;
            string roleLabel = StringEnum.GetStringValue(pType);
            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return null;

            if (pType == PersonTypes.Agent || pType == PersonTypes.Contact)
            {
                personnel apersons = apsService.getPersonnelByUserId(id);
                return Objects.Person.Convert(new personnel[] { apersons })[0];
            }
            else
            {
                return new BCS.APS.Objects.Person("TODO", "TODO", "TODO");
                ////companyPersonnel[] persons = apsService.getCompanyPersons(agencyId, underwritingUnitId, roleLabel, string.Empty);
                //long personalRoleTypeId = GetRoleId(apsService, pType);
                //companyPersonnelView[] persons = apsService.searchCompanyPersonnel(agencyId, personalRoleTypeId, underwritingUnitId);
                //return Objects.Person.Convert(persons);
            }
        }
        public List<Objects.Person> GetPersons(int companyId, long agencyId, long underwritingUnitId, PersonTypes pType)
        {
            if (pType == PersonTypes.None)
                return null;
            string roleLabel = StringEnum.GetStringValue(pType);
            // TODO: hard coding, not sure at this time which method to use
            bool activeOnly = false;
            APSHelper apsService = Common.GetAPSProxy(companyId);
            if (null == apsService)
                return null;

            if (pType == PersonTypes.Agent || pType == PersonTypes.Contact)
            {
                personnel[] apersons = apsService.getPersonnelByRole(agencyId, roleLabel, activeOnly);
                return Objects.Person.Convert(apersons);
            }
            else
            {
                //companyPersonnel[] persons = apsService.getCompanyPersons(agencyId, underwritingUnitId, roleLabel, string.Empty);
                long personalRoleTypeId = GetRoleId(apsService, pType);
                companyPersonnelView[] persons = apsService.searchCompanyPersonnel(agencyId, personalRoleTypeId, underwritingUnitId);
                return Objects.Person.Convert(persons);
            }
        }
        #endregion


        private long GetRoleId(APSHelper service, PersonTypes personType)
        {
            companyPersonnelRoleType[] roleDataTypes = service.getCompanyPersonnelRoleType();
            if (null == roleDataTypes || 0 == roleDataTypes.Length)
            {
                return 0;
            }
            foreach (companyPersonnelRoleType roleDataType in roleDataTypes)
            {
                if (roleDataType.Label == StringEnum.GetStringValue(personType))
                {
                    return roleDataType.Id;
                }
            }
            return 0;
        }
    }
}
