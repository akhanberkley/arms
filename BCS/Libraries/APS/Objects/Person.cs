using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.APS.Objects
{
    public class Person
    {
        private string id, lastName, firstName;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public Person(string id, string firstName, string lastName)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
        }

        internal static List<Person> Convert(BCS.APS.companyPersonnel[] persons)
        {
            List<companyPersonnel> list = new List<companyPersonnel>(persons.Length);
            list.AddRange(persons);
            List<Person> converted = list.ConvertAll<Person>(new Converter<companyPersonnel, Person>(APSPersonnelToPerson));
            return converted;
        }
        internal static List<Person> Convert(BCS.APS.companyPersonnelView[] persons)
        {
            List<companyPersonnelView> list = new List<companyPersonnelView>(persons.Length);
            list.AddRange(persons);
            List<Person> converted = list.ConvertAll<Person>(new Converter<companyPersonnelView, Person>(APSPersonnelToPerson));
            return converted;
        }
        internal static List<Person> Convert(BCS.APS.personnel[] persons)
        {
            List<personnel> list = new List<personnel>(persons.Length);
            list.AddRange(persons);
            List<Person> converted = list.ConvertAll<Person>(new Converter<personnel, Person>(APSPersonnelToPerson));
            return converted;
        }
        public static Person APSPersonnelToPerson(personnel personnel)
        {
            return new Person(personnel.UserId, personnel.FirstName, personnel.LastName);
        }
        public static Person APSPersonnelToPerson(companyPersonnel personnel)
        {
            return new Person(personnel.UserId, personnel.FirstName, personnel.LastName);
        }
        public static Person APSPersonnelToPerson(companyPersonnelView personnel)
        {
            return new Person(personnel.UserId, personnel.FirstName, personnel.LastName);
        }
    }
}
