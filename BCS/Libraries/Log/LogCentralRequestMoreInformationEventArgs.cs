using System;
using System.Configuration;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

using log4net;
using log4net.spi;
using log4net.Appender;
using log4net.Repository.Hierarchy;

namespace BTS.LogFramework
{
	/// <summary>
	/// Arguments to the event that tries to query additional information
	/// about the environment or the current session info.
	/// </summary>
	/// <remarks>	
	/// Please contact the author Uwe Keim (mailto:uwe.keim@zeta-software.de)
	/// for questions regarding this class.
	/// </remarks>
	public class LogCentralRequestMoreInformationEventArgs :
		LogCentralLogEventArgs
	{
		#region Constructors.
		// ------------------------------------------------------------------

		/// <summary>
		/// Constructor.
		/// </summary>
		public LogCentralRequestMoreInformationEventArgs( 
			LogType type,
			object message,
			Exception t ) :
			base( type, message, t )
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public LogCentralRequestMoreInformationEventArgs( 
			LogType type,
			object message ) :
			base( type, message )
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public LogCentralRequestMoreInformationEventArgs( 
			LogType type ) :
			base( type )
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public LogCentralRequestMoreInformationEventArgs() :
			base()
		{
		}

		// ------------------------------------------------------------------
		#endregion

		#region Public properties.
		// ------------------------------------------------------------------

		/// <summary>
		/// The event handler that gets this object passed can
		/// place additional information here.
		/// </summary>
		public string MoreInformationMessage
		{
			get
			{
				return moreInformationMessage;
			}
			set
			{
				moreInformationMessage = value;
			}
		}

		// ------------------------------------------------------------------
		#endregion

		#region Private member.
		// ------------------------------------------------------------------

		/// <summary>
		/// The event handler that gets this object passed can
		/// place additional information here.
		/// </summary>
		private string moreInformationMessage = null;

		// ------------------------------------------------------------------
		#endregion
	}

}
