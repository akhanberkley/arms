using System;
using System.Configuration;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

using log4net;
using log4net.spi;
using log4net.Appender;
using log4net.Repository.Hierarchy;

namespace BTS.LogFramework
{
	/// <summary>
	/// Arguments to the log central event.
	/// </summary>
	/// <remarks>	
	/// Please contact the author Uwe Keim (mailto:uwe.keim@zeta-software.de)
	/// for questions regarding this class.
	/// </remarks>
	public class LogCentralLogEventArgs : EventArgs
	{
		#region Constructors.
		// ------------------------------------------------------------------

		/// <summary>
		/// Constructor.
		/// </summary>
		public LogCentralLogEventArgs( 
			LogType type,
			object message,
			Exception t )
		{
			this.type = type;
			this.message = message;
			this.error = t;
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public LogCentralLogEventArgs( 
			LogType type,
			object message )
		{
			this.type = type;
			this.message = message;
			this.error = null;
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public LogCentralLogEventArgs( 
			LogType type )
		{
			this.type = type;
			this.message = null;
			this.error = null;
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public LogCentralLogEventArgs()
		{
			this.type = LogType.Unknown;
			this.message = null;
			this.error = null;
		}

		// ------------------------------------------------------------------
		#endregion

		#region Public properties.
		// ------------------------------------------------------------------

		/// <summary>
		/// The type of the texts to log.
		/// </summary>
		public enum LogType
		{
			/// <summary>
			/// Unknown type.
			/// </summary>
			Unknown,

			/// <summary>
			/// Info.
			/// </summary>
			Info,

			/// <summary>
			/// Error.
			/// </summary>
			Error,

			/// <summary>
			/// Debug.
			/// </summary>
			Debug,

			/// <summary>
			/// Warning.
			/// </summary>
			Warn,

			/// <summary>
			/// Fatal.
			/// </summary>
			Fatal
		}

		/// <summary>
		/// The type of log event.
		/// </summary>
		public LogType Type
		{
			get
			{
				return type;
			}
		}

		/// <summary>
		/// The message that is logged. You cannot modify the
		/// message in your event handler, because the logging
		/// already has occured.
		/// </summary>
		public object Message
		{
			get
			{
				return message;
			}
		}

		/// <summary>
		/// The exception that occured.
		/// </summary>
		public Exception Error
		{
			get
			{
				return error;
			}
		}

		// ------------------------------------------------------------------
		#endregion

		#region Private members.
		// ------------------------------------------------------------------

		/// <summary>
		/// Make them private for read-only.
		/// </summary>
		private readonly LogType type;
		private readonly object message;
		private readonly Exception error;

		// ------------------------------------------------------------------
		#endregion
	}

	/// <summary>
	/// Delegate that is called upon logging from LogXxx().
	/// </summary>
	public delegate void LogCentralLogEventHandler( 
	object sender,
	LogCentralLogEventArgs e );

	
	/// <summary>
	/// Delegate that is called when the logging framework tries to query 
	/// additional information about the environment or the current session 
	/// info. Useful e.g. for passing infos about the currently logged in
	/// user or other things.
	/// </summary>
	public delegate void LogCentralRequestMoreInformationEventHandler( 
	object sender,
	LogCentralRequestMoreInformationEventArgs e );
}
