SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetLogEntriesByApplication]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetLogEntriesByApplication]
GO

CREATE PROCEDURE GetLogEntriesByApplication
@Application varchar(255)
AS

SELECT * FROM LogEntry WHERE Application = @Application 
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

