/// <summary>
/// Application class for testing.
/// </summary>
class Application
{
	#region Static routines.
	// ------------------------------------------------------------------

	/// <summary>
	/// Main entry point of the application.
	/// </summary>
	[STAThread]
	private static void Main(
		string[] args )
	{
		// Add handler.
		LogCentral.Current.Log += 
			new LogCentralLogEventHandler(Current_Log);

		// Add handler.
		LogCentral.Current.RequestMoreInformation += 
			new LogCentralRequestMoreInformationEventHandler(
			Current_RequestMoreInformation);

		// --
		// Do some logging.

		LogCentral.Current.LogInfo( 
			"An informational message." );

		counter++;

		try
		{
			LogCentral.Current.LogInfo(
				"About to throw an exception." );

			throw new ApplicationException(
				"This error was thrown intentionally." );
		}
		catch ( Exception x )
		{
			LogCentral.Current.LogError(
				"An exception has occured.",
				x );
		}

		// --

		// Remove handler.
		LogCentral.Current.Log -= 
			new LogCentralLogEventHandler(Current_Log);

		// Remove handler.
		LogCentral.Current.RequestMoreInformation -= 
			new LogCentralRequestMoreInformationEventHandler(
			Current_RequestMoreInformation);
	}

	/// <summary>
	/// A field that counts something.
	/// </summary>
	private static int counter = 0;

	// ------------------------------------------------------------------
	#endregion

	#region Event handler.
	// ------------------------------------------------------------------

	/// <summary>
	/// This handler is called after each call to one of the 
	/// LogError, LogDebug, etc. function.
	/// </summary>
	private static void Current_Log(
		object sender, 
		LogCentralLogEventArgs e )
	{
		Debug.Write( "Log occured." );
	}

	/// <summary>
	/// This handler is called before a message is actually
	/// being logged. Use this handler to provide more detailed 
	/// information to the message being logged.
	/// </summary>
	private static void Current_RequestMoreInformation(
		object sender, 
		LogCentralRequestMoreInformationEventArgs e )
	{
		// Only add for certain types.
		if ( e.Type==LogCentralRequestMoreInformationEventArgs.LogType.Error ||
			e.Type==LogCentralRequestMoreInformationEventArgs.LogType.Fatal ||
			e.Type==LogCentralRequestMoreInformationEventArgs.LogType.Warn )
		{
			// Add information that would otherwise not be visible
			// to the logging class.
			e.MoreInformationMessage += 
				string.Format(
				"The current counter value is '{0}'.",
				counter );
		}
	}