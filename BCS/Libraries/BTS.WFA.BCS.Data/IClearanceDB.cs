using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using BTS.WFA.BCS.Data.Models;

namespace BTS.WFA.BCS.Data
{
    public interface IClearanceDb : IDisposable
    {
        DbSet<AddressType> AddressType { get; set; } // AddressType
        DbSet<Agency> Agency { get; set; } // Agency
        DbSet<AgencyBranch> AgencyBranch { get; set; } // AgencyBranch
        DbSet<AgencyLicensedState> AgencyLicensedState { get; set; } // AgencyLicensedState
        DbSet<AgencyLineOfBusiness> AgencyLineOfBusiness { get; set; } // AgencyLineOfBusiness
        DbSet<AgencyLineOfBusinessPersonUnderwritingRolePerson> AgencyLineOfBusinessPersonUnderwritingRolePerson { get; set; } // AgencyLineOfBusinessPersonUnderwritingRolePerson
        DbSet<Agent> Agent { get; set; } // Agent
        DbSet<AgentUnspecifiedReason> AgentUnspecifiedReason { get; set; } // AgentUnspecifiedReason
        DbSet<ApsxmlLog> ApsxmlLog { get; set; } // APSXMLLog
        DbSet<APSSync> ApsSync { get; set; } // APSSync
        DbSet<AttributeDataType> AttributeDataType { get; set; } // AttributeDataType
        DbSet<ClassCode> ClassCode { get; set; } // ClassCode
        DbSet<ClassCodeCompanyNumberCode> ClassCodeCompanyNumberCode { get; set; } // ClassCodeCompanyNumberCode
        DbSet<ClearSync> ClearSync { get; set; } // ClearSync
        DbSet<Client> Client { get; set; } // Client
        DbSet<ClientClassCode> ClientClassCode { get; set; } // ClientClassCode
        DbSet<ClientClassCodeHazardGradeValue> ClientClassCodeHazardGradeValue { get; set; } // ClientClassCodeHazardGradeValue
        DbSet<ClientCompanyNumberAttributeValue> ClientCompanyNumberAttributeValue { get; set; } // ClientCompanyNumberAttributeValue
        DbSet<ClientCoreAddressAddressType> ClientCoreAddressAddressType { get; set; } // ClientCoreAddressAddressType
        DbSet<ClientCoreNameClientNameType> ClientCoreNameClientNameType { get; set; } // ClientCoreNameClientNameType
        DbSet<ClientNameType> ClientNameType { get; set; } // ClientNameType
        DbSet<ClientSystemType> ClientSystemType { get; set; } // ClientSystemType
        DbSet<CobraAddressCounty> CobraAddressCounty { get; set; } // CobraAddressCounty
        DbSet<Company> Company { get; set; } // Company
        DbSet<CompanyClientAdvancedSearch> CompanyClientAdvancedSearch { get; set; } // CompanyClientAdvancedSearch
        DbSet<CompanyNumber> CompanyNumber { get; set; } // CompanyNumber
        DbSet<CompanyNumberAgentUnspecifiedReason> CompanyNumberAgentUnspecifiedReason { get; set; } // CompanyNumberAgentUnspecifiedReason
        DbSet<CompanyNumberAttribute> CompanyNumberAttribute { get; set; } // CompanyNumberAttribute
        DbSet<CompanyNumberClassCodeHazardGradeValue> CompanyNumberClassCodeHazardGradeValue { get; set; } // CompanyNumberClassCodeHazardGradeValue
        DbSet<CompanyNumberCode> CompanyNumberCode { get; set; } // CompanyNumberCode
        DbSet<CompanyNumberCodeType> CompanyNumberCodeType { get; set; } // CompanyNumberCodeType
        DbSet<CompanyNumberLobGrid> CompanyNumberLobGrid { get; set; } // CompanyNumberLOBGrid
        DbSet<CompanyNumberOtherCarrier> CompanyNumberOtherCarrier { get; set; } // CompanyNumberOtherCarrier
        DbSet<CompanyNumberSubmissionStatus> CompanyNumberSubmissionStatus { get; set; } // CompanyNumberSubmissionStatus
        DbSet<CompanyNumberSubmissionType> CompanyNumberSubmissionType { get; set; } // CompanyNumberSubmissionType
        DbSet<CompanyParameter> CompanyParameter { get; set; } // CompanyParameter
        DbSet<Contact> Contact { get; set; } // Contact
        DbSet<Country> Country { get; set; } // Country
        DbSet<DssException> DssException { get; set; } // DSSException
        DbSet<ExceptionEmailFilterString> ExceptionEmailFilterString { get; set; } // ExceptionEmailFilterString
        DbSet<ExceptionLog> ExceptionLog { get; set; } // ExceptionLog
        DbSet<ExceptionReturnMessage> ExceptionReturnMessage { get; set; } // ExceptionReturnMessage
        DbSet<Faq> Faq { get; set; } // FAQ
        DbSet<GeographicDirectional> GeographicDirectional { get; set; } // GeographicDirectional
        DbSet<HazardGradeType> HazardGradeType { get; set; } // HazardGradeType
        DbSet<LineOfBusiness> LineOfBusiness { get; set; } // LineOfBusiness
        DbSet<Link> Link { get; set; } // Link
        DbSet<NaicsCodeList> NaicsCodeList { get; set; } // NaicsCodeList
        DbSet<NaicsDivision> NaicsDivision { get; set; } // NaicsDivision
        DbSet<NaicsGroup> NaicsGroup { get; set; } // NaicsGroup
        DbSet<NamePrefix> NamePrefix { get; set; } // NamePrefix
        DbSet<NameSuffixTitle> NameSuffixTitle { get; set; } // NameSuffixTitle
        DbSet<Notification> Notification { get; set; } // Notification
        DbSet<NumberControl> NumberControl { get; set; } // NumberControl
        DbSet<NumberControlType> NumberControlType { get; set; } // NumberControlType
        DbSet<OtherCarrier> OtherCarrier { get; set; } // OtherCarrier
        DbSet<Person> Person { get; set; } // Person
        DbSet<PolicySystem> PolicySystem { get; set; } // PolicySystem
        DbSet<Role> Role { get; set; } // Role
        DbSet<SecondaryUnitDesignator> SecondaryUnitDesignator { get; set; } // SecondaryUnitDesignator
        DbSet<SicCodeList> SicCodeList { get; set; } // SicCodeList
        DbSet<SicDivision> SicDivision { get; set; } // SicDivision
        DbSet<SicGroup> SicGroup { get; set; } // SicGroup
        DbSet<State> State { get; set; } // State
        DbSet<StreetSuffix> StreetSuffix { get; set; } // StreetSuffix
        DbSet<Submission> Submission { get; set; } // Submission
        DbSet<SubmissionClientCoreClientAddress> SubmissionClientCoreClientAddress { get; set; } // SubmissionClientCoreClientAddress
        DbSet<SubmissionClientCoreClientName> SubmissionClientCoreClientName { get; set; } // SubmissionClientCoreClientName
        DbSet<SubmissionComment> SubmissionComment { get; set; } // SubmissionComment
        DbSet<SubmissionCompanyNumberAttributeValue> SubmissionCompanyNumberAttributeValue { get; set; } // SubmissionCompanyNumberAttributeValue
        DbSet<SubmissionCompanyNumberAttributeValueHistory> SubmissionCompanyNumberAttributeValueHistory { get; set; } // SubmissionCompanyNumberAttributeValueHistory
        DbSet<SubmissionCompanyNumberCodeHistory> SubmissionCompanyNumberCodeHistory { get; set; } // SubmissionCompanyNumberCodeHistory
        DbSet<SubmissionCopyExclusion> SubmissionCopyExclusion { get; set; } // SubmissionCopyExclusion
        DbSet<SubmissionDisplayGrid> SubmissionDisplayGrid { get; set; } // SubmissionDisplayGrid
        DbSet<SubmissionHistory> SubmissionHistory { get; set; } // SubmissionHistory
        DbSet<SubmissionLineOfBusinessChildren> SubmissionLineOfBusinessChildren { get; set; } // SubmissionLineOfBusinessChildren
        DbSet<SubmissionNumberControl> SubmissionNumberControl { get; set; } // SubmissionNumberControl
        DbSet<SubmissionPage> SubmissionPage { get; set; } // SubmissionPage
        DbSet<SubmissionSearchPage> SubmissionSearchPage { get; set; } // SubmissionSearchPage
        DbSet<SubmissionStatus> SubmissionStatus { get; set; } // SubmissionStatus
        DbSet<SubmissionStatusHistory> SubmissionStatusHistory { get; set; } // SubmissionStatusHistory
        DbSet<SubmissionStatusReason> SubmissionStatusReason { get; set; } // SubmissionStatusReason
        DbSet<SubmissionStatusSubmissionStatusReason> SubmissionStatusSubmissionStatusReason { get; set; } // SubmissionStatusSubmissionStatusReason
        DbSet<SubmissionToken> SubmissionToken { get; set; } // SubmissionToken
        DbSet<SubmissionType> SubmissionType { get; set; } // SubmissionType
        DbSet<UnderwritingRole> UnderwritingRole { get; set; } // UnderwritingRole
        DbSet<User> User { get; set; } // User
        DbSet<Validation> Validation { get; set; } // Validation
        DbSet<ValidationCondition> ValidationCondition { get; set; } // ValidationCondition
        DbSet<ValidationField> ValidationField { get; set; } // ValidationField
        DbSet<ValidationOperator> ValidationOperator { get; set; } // ValidationOperator
        DbSet<ValidationRule> ValidationRule { get; set; } // ValidationRule
        DbSet<ValidationRuleValue> ValidationRuleValue { get; set; } // ValidationRuleValue
        DbSet<SubmissionField> SubmissionField { get; set; }
        DbSet<SubmissionViewType> SubmissionViewType { get; set; }
        DbSet<SubmissionViewField> SubmissionViewField { get; set; }
        DbSet<ImportSubmission> ImportSubmission { get; set; }
        DbSet<PolicySymbol> PolicySymbol { get; set; }
        DbSet<ClientField> ClientField { get; set; }
        DbSet<CompanyNumberClientField> CompanyNumberClientField { get; set; }
        DbSet<SubmissionAddress> SubmissionAddress { get; set; }
        DbSet<SubmissionAdditionalName> SubmissionAdditionalName { get; set; }
        DbSet<UserType> UserType { get; set; }
        DbSet<CompanyNumberSubmissionField> CompanyNumberSubmissionField { get; set; }
        DbSet<CompanyNumberClientCoreSegment> CompanyNumberClientCoreSegment { get; set; }
        DbSet<CompanyNumberCrossClearance> CompanyNumberCrossClearance { get; set; }

        DbContextTransaction GetTransaction(System.Data.IsolationLevel isoLevel = System.Data.IsolationLevel.ReadCommitted);
        int SaveChanges();
        bool HasChanges();
        bool EntityHasChanges<T>(T item) where T : class;
        bool EntityIsNew<T>(T item) where T : class;
        void Reload<T>(T item) where T : class;
    }
}
