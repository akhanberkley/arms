using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // AgencyLineOfBusinessPersonUnderwritingRolePerson
    public class AgencyLineOfBusinessPersonUnderwritingRolePerson
    {
        public int AgencyId { get; set; } // AgencyId (Primary key)
        public int LineOfBusinessId { get; set; } // LineOfBusinessId (Primary key)
        public int UnderwritingRoleId { get; set; } // UnderwritingRoleId (Primary key)
        public int PersonId { get; set; } // PersonId (Primary key)
        public bool Primary { get; set; } // Primary
        public long? ApsId { get; set; } // APSId

        // Foreign keys
        public virtual AgencyLineOfBusiness AgencyLineOfBusiness { get; set; }
        public virtual Agency Agency { get; set; } //  FK_AgencyLineOfBusinessPerson_Agency
        public virtual LineOfBusiness LineOfBusiness { get; set; } //  FK_AgencyLineOfBusinessPerson_LineOfBusiness
        public virtual UnderwritingRole UnderwritingRole { get; set; } //  FK_AgencyLineOfBusinessPerson_UnderwritingRole
        public virtual Person Person { get; set; } //  FK_AgencyLineOfBusinessPerson_Person

        public AgencyLineOfBusinessPersonUnderwritingRolePerson()
        {
            Primary = false;
        }
    }

    internal class AgencyLineOfBusinessPersonUnderwritingRolePersonConfiguration : EntityTypeConfiguration<AgencyLineOfBusinessPersonUnderwritingRolePerson>
    {
        public AgencyLineOfBusinessPersonUnderwritingRolePersonConfiguration()
        {
            ToTable("dbo.AgencyLineOfBusinessPersonUnderwritingRolePerson");
            HasKey(x => new { x.AgencyId, x.LineOfBusinessId, x.UnderwritingRoleId, x.PersonId });

            Property(x => x.AgencyId).HasColumnName("AgencyId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.LineOfBusinessId).HasColumnName("LineOfBusinessId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.UnderwritingRoleId).HasColumnName("UnderwritingRoleId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.PersonId).HasColumnName("PersonId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Primary).HasColumnName("Primary").IsRequired();
            Property(x => x.ApsId).HasColumnName("APSId").IsOptional();

            // Foreign keys
            HasRequired(a => a.AgencyLineOfBusiness).WithMany().HasForeignKey(c => new { c.AgencyId, c.LineOfBusinessId });
            HasRequired(a => a.Agency).WithMany().HasForeignKey(c => c.AgencyId); // FK_AgencyLineOfBusinessPerson_Agency
            HasRequired(a => a.LineOfBusiness).WithMany().HasForeignKey(c => c.LineOfBusinessId); // FK_AgencyLineOfBusinessPerson_LineOfBusiness
            HasRequired(a => a.UnderwritingRole).WithMany(b => b.AgencyLineOfBusinessPersonUnderwritingRolePerson).HasForeignKey(c => c.UnderwritingRoleId); // FK_AgencyLineOfBusinessPerson_UnderwritingRole
            HasRequired(a => a.Person).WithMany(b => b.AgencyLineOfBusinessPersonUnderwritingRolePerson).HasForeignKey(c => c.PersonId); // FK_AgencyLineOfBusinessPerson_Person
        }
    }
}
