using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // AttributeDataType
    public class AttributeDataType
    {
        public int Id { get; set; } // Id (Primary key)
        public string DataTypeName { get; set; } // DataTypeName
        public string UserControl { get; set; } // UserControl

        // Reverse navigation
        public virtual ICollection<CompanyNumberAttribute> CompanyNumberAttribute { get; set; } // CompanyNumberAttribute.FK_CompanyNumberAttribute_AttributeDataType
        public virtual ICollection<SubmissionDisplayGrid> SubmissionDisplayGrid { get; set; } // SubmissionDisplayGrid.FK_SubmissionDisplayGrid_AttributeDataType

        public AttributeDataType()
        {
            CompanyNumberAttribute = new List<CompanyNumberAttribute>();
            SubmissionDisplayGrid = new List<SubmissionDisplayGrid>();
        }
    }

    internal class AttributeDataTypeConfiguration : EntityTypeConfiguration<AttributeDataType>
    {
        public AttributeDataTypeConfiguration()
        {
            ToTable("dbo.AttributeDataType");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.DataTypeName).HasColumnName("DataTypeName").IsRequired().HasMaxLength(100);
            Property(x => x.UserControl).HasColumnName("UserControl").IsRequired().HasMaxLength(255);
        }
    }
}
