using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionCompanyNumberAttributeValueHistory
    public class SubmissionCompanyNumberAttributeValueHistory
    {
        public int Id { get; set; } // Id (Primary key)
        public int SubmissionId { get; set; } // SubmissionId
        public int CompanyNumberAttributeId { get; set; } // CompanyNumberAttributeId
        public string AttributeValue { get; set; } // AttributeValue
        public string By { get; set; } // By
        public DateTime HistoryDt { get; set; } // HistoryDt

        // Foreign keys
        public virtual Submission Submission { get; set; } //  FK_SubmissionCompanyNumberAttributeValueHistory_Submission
        public virtual CompanyNumberAttribute CompanyNumberAttribute { get; set; } //  FK_SubmissionCompanyNumberAttributeValueHistory_CompanyNumberAttribute

        public SubmissionCompanyNumberAttributeValueHistory()
        {
            By = "Initialize";
            HistoryDt = System.DateTime.Now;
        }
    }

    internal class SubmissionCompanyNumberAttributeValueHistoryConfiguration : EntityTypeConfiguration<SubmissionCompanyNumberAttributeValueHistory>
    {
        public SubmissionCompanyNumberAttributeValueHistoryConfiguration()
        {
            ToTable("dbo.SubmissionCompanyNumberAttributeValueHistory");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired();
            Property(x => x.CompanyNumberAttributeId).HasColumnName("CompanyNumberAttributeId").IsRequired();
            Property(x => x.AttributeValue).HasColumnName("AttributeValue").IsOptional().HasMaxLength(255);
            Property(x => x.By).HasColumnName("By").IsRequired().HasMaxLength(50);
            Property(x => x.HistoryDt).HasColumnName("HistoryDt").IsRequired();

            // Foreign keys
            HasRequired(a => a.Submission).WithMany(b => b.SubmissionCompanyNumberAttributeValueHistory).HasForeignKey(c => c.SubmissionId); // FK_SubmissionCompanyNumberAttributeValueHistory_Submission
            HasRequired(a => a.CompanyNumberAttribute).WithMany(b => b.SubmissionCompanyNumberAttributeValueHistory).HasForeignKey(c => c.CompanyNumberAttributeId); // FK_SubmissionCompanyNumberAttributeValueHistory_CompanyNumberAttribute
        }
    }
}
