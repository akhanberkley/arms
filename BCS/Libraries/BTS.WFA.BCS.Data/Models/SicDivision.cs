using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SicDivision
    public class SicDivision
    {
        public int Id { get; set; } // Id (Primary key)
        public string DivisionDescription { get; set; } // DivisionDescription
        public int SicGroupRangeLow { get; set; } // SicGroupRangeLow
        public int SicGroupRangeHigh { get; set; } // SicGroupRangeHigh

        // Reverse navigation
        public virtual ICollection<SicGroup> SicGroup { get; set; } // SicGroup.FK_SicGroup_SicDivision

        public SicDivision()
        {
            SicGroup = new List<SicGroup>();
        }
    }

    internal class SicDivisionConfiguration : EntityTypeConfiguration<SicDivision>
    {
        public SicDivisionConfiguration()
        {
            ToTable("dbo.SicDivision");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.DivisionDescription).HasColumnName("DivisionDescription").IsRequired().HasMaxLength(255);
            Property(x => x.SicGroupRangeLow).HasColumnName("SicGroupRangeLow").IsRequired();
            Property(x => x.SicGroupRangeHigh).HasColumnName("SicGroupRangeHigh").IsRequired();
        }
    }
}
