using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ClientNameType
    public class ClientNameType
    {
        public int Id { get; set; } // Id (Primary key)
        public string NameType { get; set; } // NameType

        // Reverse navigation
        public virtual ICollection<ClientCoreNameClientNameType> ClientCoreNameClientNameType { get; set; } // Many to many mapping

        public ClientNameType()
        {
            ClientCoreNameClientNameType = new List<ClientCoreNameClientNameType>();
        }
    }

    internal class ClientNameTypeConfiguration : EntityTypeConfiguration<ClientNameType>
    {
        public ClientNameTypeConfiguration()
        {
            ToTable("dbo.ClientNameType");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.NameType).HasColumnName("NameType").IsRequired().HasMaxLength(100);
        }
    }
}
