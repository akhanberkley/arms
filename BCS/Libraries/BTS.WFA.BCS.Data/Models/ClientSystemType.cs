using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ClientSystemType
    public class ClientSystemType
    {
        public int Id { get; set; } // Id (Primary key)
        public string Code { get; set; } // Code
        public string Description { get; set; } // Description
    }

    internal class ClientSystemTypeConfiguration : EntityTypeConfiguration<ClientSystemType>
    {
        public ClientSystemTypeConfiguration()
        {
            ToTable("dbo.ClientSystemType");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Code).HasColumnName("Code").IsRequired().HasMaxLength(50);
            Property(x => x.Description).HasColumnName("Description").IsRequired().HasMaxLength(50);
        }
    }
}
