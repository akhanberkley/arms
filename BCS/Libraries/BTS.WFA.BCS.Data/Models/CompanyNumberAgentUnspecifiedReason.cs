using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberAgentUnspecifiedReason
    public class CompanyNumberAgentUnspecifiedReason
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public int AgentUnspecifiedReasonId { get; set; } // AgentUnspecifiedReasonId

        // Reverse navigation
        public virtual ICollection<Submission> Submission { get; set; } // Many to many mapping

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_CompanyNumberAgentUnspecifiedReason_CompanyNumber
        public virtual AgentUnspecifiedReason AgentUnspecifiedReason { get; set; } //  FK_CompanyNumberAgentUnspecifiedReason_AgentUnspecifiedReason

        public CompanyNumberAgentUnspecifiedReason()
        {
            Submission = new List<Submission>();
        }
    }

    internal class CompanyNumberAgentUnspecifiedReasonConfiguration : EntityTypeConfiguration<CompanyNumberAgentUnspecifiedReason>
    {
        public CompanyNumberAgentUnspecifiedReasonConfiguration()
        {
            ToTable("dbo.CompanyNumberAgentUnspecifiedReason");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.AgentUnspecifiedReasonId).HasColumnName("AgentUnspecifiedReasonId").IsRequired();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.CompanyNumberAgentUnspecifiedReason).HasForeignKey(c => c.CompanyNumberId); // FK_CompanyNumberAgentUnspecifiedReason_CompanyNumber
            HasRequired(a => a.AgentUnspecifiedReason).WithMany(b => b.CompanyNumberAgentUnspecifiedReason).HasForeignKey(c => c.AgentUnspecifiedReasonId); // FK_CompanyNumberAgentUnspecifiedReason_AgentUnspecifiedReason
            HasMany(t => t.Submission).WithMany(t => t.CompanyNumberAgentUnspecifiedReason).Map(m =>
            {
                m.ToTable("SubmissionCompanyNumberAgentUnspecifiedReason");
                m.MapLeftKey("CompanyNumberAgentUnspecifiedReasonId");
                m.MapRightKey("SubmissionId");
            });
        }
    }
}
