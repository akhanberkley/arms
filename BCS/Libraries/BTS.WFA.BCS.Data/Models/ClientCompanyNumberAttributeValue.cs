using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ClientCompanyNumberAttributeValue
    public class ClientCompanyNumberAttributeValue
    {
        public int ClientId { get; set; } // ClientId (Primary key)
        public int CompanyNumberAttributeId { get; set; } // CompanyNumberAttributeId (Primary key)
        public string AttributeValue { get; set; } // AttributeValue

        // Foreign keys
        public virtual Client Client { get; set; } //  FK_ClientCompanyNumberAttributeValue_Client
        public virtual CompanyNumberAttribute CompanyNumberAttribute { get; set; } //  FK_ClientCompanyNumberAttributeValue_CompanyNumberAttribute
    }

    internal class ClientCompanyNumberAttributeValueConfiguration : EntityTypeConfiguration<ClientCompanyNumberAttributeValue>
    {
        public ClientCompanyNumberAttributeValueConfiguration()
        {
            ToTable("dbo.ClientCompanyNumberAttributeValue");
            HasKey(x => new { x.ClientId, x.CompanyNumberAttributeId });

            Property(x => x.ClientId).HasColumnName("ClientId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyNumberAttributeId).HasColumnName("CompanyNumberAttributeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.AttributeValue).HasColumnName("AttributeValue").IsOptional().HasMaxLength(255);

            // Foreign keys
            HasRequired(a => a.Client).WithMany(b => b.ClientCompanyNumberAttributeValue).HasForeignKey(c => c.ClientId); // FK_ClientCompanyNumberAttributeValue_Client
            HasRequired(a => a.CompanyNumberAttribute).WithMany(b => b.ClientCompanyNumberAttributeValue).HasForeignKey(c => c.CompanyNumberAttributeId); // FK_ClientCompanyNumberAttributeValue_CompanyNumberAttribute
        }
    }
}
