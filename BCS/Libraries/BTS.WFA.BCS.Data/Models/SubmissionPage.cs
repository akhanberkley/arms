using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionPage
    public class SubmissionPage
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyId { get; set; } // CompanyId
        public string AddInitialFocusField { get; set; } // AddInitialFocusField
        public string EditInitialFocusField { get; set; } // EditInitialFocusField
        public string AddInvisibleFields { get; set; } // AddInvisibleFields
        public string EditInvisibleFields { get; set; } // EditInvisibleFields
        public string AddDisabledFields { get; set; } // AddDisabledFields
        public string EditDisabledFields { get; set; } // EditDisabledFields
        public string CopyExcludedFields { get; set; } // CopyExcludedFields
        public string DefaultText { get; set; } // DefaultText
        public bool SnapShotEditable { get; set; } // SnapShotEditable
        public bool RequiresInsuredName { get; set; } // RequiresInsuredName

        // Foreign keys
        public virtual Company Company { get; set; } //  FK_SubmissionPage_Company

        public SubmissionPage()
        {
            AddInitialFocusField = "SubmissionTypeDropDownList1";
            EditDisabledFields = "SubmissionTypeDropDownList1";
            DefaultText = "(not specified)";
            SnapShotEditable = false;
            RequiresInsuredName = true;
        }
    }

    internal class SubmissionPageConfiguration : EntityTypeConfiguration<SubmissionPage>
    {
        public SubmissionPageConfiguration()
        {
            ToTable("dbo.SubmissionPage");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyId).HasColumnName("CompanyId").IsRequired();
            Property(x => x.AddInitialFocusField).HasColumnName("AddInitialFocusField").IsOptional().HasMaxLength(255);
            Property(x => x.EditInitialFocusField).HasColumnName("EditInitialFocusField").IsOptional().HasMaxLength(255);
            Property(x => x.AddInvisibleFields).HasColumnName("AddInvisibleFields").IsOptional().HasMaxLength(1073741823);
            Property(x => x.EditInvisibleFields).HasColumnName("EditInvisibleFields").IsOptional().HasMaxLength(1073741823);
            Property(x => x.AddDisabledFields).HasColumnName("AddDisabledFields").IsOptional().HasMaxLength(1073741823);
            Property(x => x.EditDisabledFields).HasColumnName("EditDisabledFields").IsOptional().HasMaxLength(1073741823);
            Property(x => x.CopyExcludedFields).HasColumnName("CopyExcludedFields").IsOptional().HasMaxLength(1073741823);
            Property(x => x.DefaultText).HasColumnName("DefaultText").IsRequired().HasMaxLength(50);
            Property(x => x.SnapShotEditable).HasColumnName("SnapShotEditable").IsRequired();
            Property(x => x.RequiresInsuredName).HasColumnName("RequiresInsuredName").IsRequired();

            // Foreign keys
            HasRequired(a => a.Company).WithMany(b => b.SubmissionPage).HasForeignKey(c => c.CompanyId); // FK_SubmissionPage_Company
        }
    }
}
