using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberSubmissionStatus
    public class CompanyNumberSubmissionStatus
    {
        public int CompanyNumberId { get; set; } // CompanyNumberId (Primary key)
        public int SubmissionStatusId { get; set; } // SubmissionStatusId (Primary key)
        public int Order { get; set; } // Order
        public bool DefaultSelection { get; set; } // DefaultSelection
        public int HierarchicalOrder { get; set; } // HierarchicalOrder
        public bool IsClearable { get; set; } // IsClearable
        public int? DaysStatusIsActive { get; set; } // DaysStatusIsActive

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_CompanyNumberSubmissionStatus_CompanyNumber
        public virtual SubmissionStatus SubmissionStatus { get; set; } //  FK_CompanyNumberSubmissionStatus_SubmissionStatus

        public CompanyNumberSubmissionStatus()
        {
            Order = 0;
            DefaultSelection = false;
            HierarchicalOrder = 0;
            IsClearable = true;
        }
    }

    internal class CompanyNumberSubmissionStatusConfiguration : EntityTypeConfiguration<CompanyNumberSubmissionStatus>
    {
        public CompanyNumberSubmissionStatusConfiguration()
        {
            ToTable("dbo.CompanyNumberSubmissionStatus");
            HasKey(x => new { x.CompanyNumberId, x.SubmissionStatusId });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SubmissionStatusId).HasColumnName("SubmissionStatusId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Order).HasColumnName("Order").IsRequired();
            Property(x => x.DefaultSelection).HasColumnName("DefaultSelection").IsRequired();
            Property(x => x.HierarchicalOrder).HasColumnName("HierarchicalOrder").IsRequired();
            Property(x => x.IsClearable).HasColumnName("IsClearable").IsRequired();
            Property(x => x.DaysStatusIsActive).HasColumnName("DaysStatusIsActive").IsOptional();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.CompanyNumberSubmissionStatus).HasForeignKey(c => c.CompanyNumberId); // FK_CompanyNumberSubmissionStatus_CompanyNumber
            HasRequired(a => a.SubmissionStatus).WithMany(b => b.CompanyNumberSubmissionStatus).HasForeignKey(c => c.SubmissionStatusId); // FK_CompanyNumberSubmissionStatus_SubmissionStatus
        }
    }
}
