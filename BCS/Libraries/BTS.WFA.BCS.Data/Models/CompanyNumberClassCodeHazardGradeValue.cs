using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberClassCodeHazardGradeValue
    public class CompanyNumberClassCodeHazardGradeValue
    {
        public int CompanyNumberId { get; set; } // CompanyNumberId (Primary key)
        public int ClassCodeId { get; set; } // ClassCodeId (Primary key)
        public int HazardGradeTypeId { get; set; } // HazardGradeTypeId (Primary key)
        public string HazardGradeValue { get; set; } // HazardGradeValue

        // Foreign keys
        public virtual ClassCode ClassCode { get; set; } //  FK_CompanyNumberClassCodeHazardGradeValue_ClassCode
        public virtual CompanyNumberHazardGradeType CompanyNumberHazardGradeType { get; set;}
    }

    internal class CompanyNumberClassCodeHazardGradeValueConfiguration : EntityTypeConfiguration<CompanyNumberClassCodeHazardGradeValue>
    {
        public CompanyNumberClassCodeHazardGradeValueConfiguration()
        {
            ToTable("dbo.CompanyNumberClassCodeHazardGradeValue");
            HasKey(x => new { x.CompanyNumberId, x.ClassCodeId, x.HazardGradeTypeId });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ClassCodeId).HasColumnName("ClassCodeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.HazardGradeTypeId).HasColumnName("HazardGradeTypeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.HazardGradeValue).HasColumnName("HazardGradeValue").IsOptional().HasMaxLength(50);

            // Foreign keys
            HasRequired(a => a.ClassCode).WithMany(b => b.CompanyNumberClassCodeHazardGradeValue).HasForeignKey(c => c.ClassCodeId); // FK_CompanyNumberClassCodeHazardGradeValue_ClassCode
            HasRequired(a => a.CompanyNumberHazardGradeType).WithMany().HasForeignKey(c => new { c.CompanyNumberId, c.HazardGradeTypeId });
        }
    }
}
