using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionStatusSubmissionStatusReason
    public class SubmissionStatusSubmissionStatusReason
    {
        public int SubmissionStatusId { get; set; } // SubmissionStatusId (Primary key)
        public int SubmissionStatusReasonId { get; set; } // SubmissionStatusReasonId (Primary key)
        public bool Active { get; set; } // Active

        // Foreign keys
        public virtual SubmissionStatus SubmissionStatus { get; set; } //  FK_SubmissioNStatusSubmissioNStatusReason_SubmissionStatus
        public virtual SubmissionStatusReason SubmissionStatusReason { get; set; } //  FK_SubmissioNStatusSubmissioNStatusReason_SubmissionStatusReason

        public SubmissionStatusSubmissionStatusReason()
        {
            Active = true;
        }
    }

    internal class SubmissionStatusSubmissionStatusReasonConfiguration : EntityTypeConfiguration<SubmissionStatusSubmissionStatusReason>
    {
        public SubmissionStatusSubmissionStatusReasonConfiguration()
        {
            ToTable("dbo.SubmissionStatusSubmissionStatusReason");
            HasKey(x => new { x.SubmissionStatusId, x.SubmissionStatusReasonId });

            Property(x => x.SubmissionStatusId).HasColumnName("SubmissionStatusId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SubmissionStatusReasonId).HasColumnName("SubmissionStatusReasonId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Active).HasColumnName("Active").IsRequired();

            // Foreign keys
            HasRequired(a => a.SubmissionStatus).WithMany(b => b.SubmissionStatusSubmissionStatusReason).HasForeignKey(c => c.SubmissionStatusId); // FK_SubmissioNStatusSubmissioNStatusReason_SubmissionStatus
            HasRequired(a => a.SubmissionStatusReason).WithMany(b => b.SubmissionStatusSubmissionStatusReason).HasForeignKey(c => c.SubmissionStatusReasonId); // FK_SubmissioNStatusSubmissioNStatusReason_SubmissionStatusReason
        }
    }
}
