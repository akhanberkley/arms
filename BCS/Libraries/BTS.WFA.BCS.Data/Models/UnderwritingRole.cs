using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // UnderwritingRole
    public class UnderwritingRole
    {
        public int Id { get; set; } // Id (Primary key)
        public string RoleCode { get; set; } // RoleCode
        public string RoleName { get; set; } // RoleName

        // Reverse navigation
        public virtual ICollection<CompanyNumber> CompanyNumber { get; set; } // Many to many mapping
        public virtual ICollection<AgencyLineOfBusinessPersonUnderwritingRolePerson> AgencyLineOfBusinessPersonUnderwritingRolePerson { get; set; } // Many to many mapping
        public virtual ICollection<Company> Company { get; set; } // Company.FK_Company_UnderwritingRole1
        public virtual ICollection<Company> Company1 { get; set; } // Company.FK_Company_UnderwritingRole2
        public virtual ICollection<Company> Company2 { get; set; } // Company.FK_Company_UnderwritingRole

        public UnderwritingRole()
        {
            CompanyNumber = new List<CompanyNumber>();
            AgencyLineOfBusinessPersonUnderwritingRolePerson = new List<AgencyLineOfBusinessPersonUnderwritingRolePerson>();
            Company = new List<Company>();
            Company1 = new List<Company>();
            Company2 = new List<Company>();
        }
    }

    internal class UnderwritingRoleConfiguration : EntityTypeConfiguration<UnderwritingRole>
    {
        public UnderwritingRoleConfiguration()
        {
            ToTable("dbo.UnderwritingRole");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RoleCode).HasColumnName("RoleCode").IsOptional().HasMaxLength(50);
            Property(x => x.RoleName).HasColumnName("RoleName").IsOptional().HasMaxLength(50);
        }
    }
}
