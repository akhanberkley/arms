using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionClientCoreClientName
    public class SubmissionClientCoreClientName
    {
        public int SubmissionId { get; set; } // SubmissionId (Primary key)
        public int ClientCoreNameId { get; set; } // ClientCoreNameId (Primary key)

        // Foreign keys
        public virtual Submission Submission { get; set; } //  FK_SubmissionClientCoreName_Submission
    }

    internal class SubmissionClientCoreClientNameConfiguration : EntityTypeConfiguration<SubmissionClientCoreClientName>
    {
        public SubmissionClientCoreClientNameConfiguration()
        {
            ToTable("dbo.SubmissionClientCoreClientName");
            HasKey(x => new { x.SubmissionId, x.ClientCoreNameId });

            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ClientCoreNameId).HasColumnName("ClientCoreNameId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Foreign keys
            HasRequired(a => a.Submission).WithMany(b => b.SubmissionClientCoreClientName).HasForeignKey(c => c.SubmissionId); // FK_SubmissionClientCoreName_Submission
        }
    }
}
