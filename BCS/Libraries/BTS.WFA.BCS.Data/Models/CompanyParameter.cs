using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyParameter
    public class CompanyParameter
    {
        public int Id { get; set; } // ID (Primary key)
        public int CompanyId { get; set; } // CompanyID
        public string SearchFilter { get; set; } // SearchFilter
        public int SearchFilterService { get; set; } // SearchFilterService
        public string SearchFilterServiceDescription { get; set; } // SearchFilterServiceDescription
        public bool FuzzySearchEnabled { get; set; } // FuzzySearchEnabled
        public bool PolicyValidation { get; set; } // PolicyValidation
        public bool AlwaysDisplayPortfolio { get; set; } // AlwaysDisplayPortfolio
        public int SubmissionDisplayYears { get; set; } // SubmissionDisplayYears
        public bool DietSearch { get; set; } // DietSearch
        public string DietSearchOptions { get; set; } // DietSearchOptions
        public bool? UwReferralAddressIndicator { get; set; } // UWReferralAddressIndicator
        public bool? RequiresUw { get; set; } // RequiresUW
        public bool? RequiresUwAssistant { get; set; } // RequiresUWAssistant
        public bool? ClearUwOnLobChange { get; set; } // ClearUWOnLOBChange
        public bool FilterPerformClientSearch { get; set; } // FilterPerformClientSearch
        public bool FilterGetClientInfo { get; set; } // FilterGetClientInfo
        public string PrimaryUwRole { get; set; } // PrimaryUWRole
        public string PrimaryAnRole { get; set; } // PrimaryANRole
        public bool UseApsForAssignments { get; set; } // UseAPSForAssignments
        public bool FillAllAssignments { get; set; } // FillAllAssignments
        public bool RequiresInsuredOrDba { get; set; } // RequiresInsuredOrDBA
        public string ApsService { get; set; } // APSService
        public string MqName { get; set; } // MQName
        public string MqHostName { get; set; } // MQHostName
        public string MqChannel { get; set; } // MQChannel
        public string MqPort { get; set; } // MQPort
        public string MqManager { get; set; }
        public bool FilterInactiveStatusReasons { get; set; } // FilterInactiveStatusReasons
        public bool CopySubmissionStatusOption { get; set; } // CopySubmissionStatusOption
        public int? SubmissionStatusDateTbd { get; set; } // SubmissionStatusDateTBD
        public bool EnsureDefaultStatusOnAdd { get; set; } // EnsureDefaultStatusOnAdd
        public bool UsesFaq { get; set; } // UsesFAQ
        public bool LinkAfterSubStatus { get; set; } // LinkAfterSubStatus
        public string ClientTypeForSearch { get; set; } // ClientTypeForSearch
        public bool SegmentWithGetClientInfo { get; set; } // SegmentWithGetClientInfo
        public bool OptionToViewDeletedSubmissions { get; set; } // OptionToViewDeletedSubmissions
        public bool UseApsNewBusinessCancelDate { get; set; } // UseAPSNewBusinessCancelDate
        public bool UseApsRenewalCancelDate { get; set; } // UseAPSRenewalCancelDate
        public bool DefaultApsAssignments { get; set; } // DefaultAPSAssignments
        public string APSAgencyUrl { get; set; }
        public string DuplicateSubmissionQuery { get; set; }

        // Reverse navigation
        public virtual ICollection<SubmissionSearchPage> SubmissionSearchPage { get; set; } // SubmissionSearchPage.FK_SubmissionSearchFilter_CompanyParameter

        // Foreign keys
        public virtual Company Company { get; set; } //  FK_CompanyParameter_Company

        public CompanyParameter()
        {
            SearchFilterService = 0;
            FuzzySearchEnabled = true;
            PolicyValidation = false;
            AlwaysDisplayPortfolio = true;
            SubmissionDisplayYears = 10;
            DietSearch = false;
            UwReferralAddressIndicator = false;
            RequiresUw = true;
            RequiresUwAssistant = false;
            ClearUwOnLobChange = false;
            FilterPerformClientSearch = true;
            FilterGetClientInfo = true;
            UseApsForAssignments = false;
            FillAllAssignments = false;
            RequiresInsuredOrDba = false;
            FilterInactiveStatusReasons = false;
            CopySubmissionStatusOption = false;
            EnsureDefaultStatusOnAdd = false;
            UsesFaq = true;
            LinkAfterSubStatus = false;
            SegmentWithGetClientInfo = false;
            OptionToViewDeletedSubmissions = false;
            UseApsNewBusinessCancelDate = false;
            UseApsRenewalCancelDate = false;
            DefaultApsAssignments = false;
            SubmissionSearchPage = new List<SubmissionSearchPage>();
        }
    }

    internal class CompanyParameterConfiguration : EntityTypeConfiguration<CompanyParameter>
    {
        public CompanyParameterConfiguration()
        {
            ToTable("dbo.CompanyParameter");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.SearchFilter).HasColumnName("SearchFilter").IsOptional().HasMaxLength(200);
            Property(x => x.SearchFilterService).HasColumnName("SearchFilterService").IsRequired();
            Property(x => x.SearchFilterServiceDescription).HasColumnName("SearchFilterServiceDescription").IsOptional().HasMaxLength(200);
            Property(x => x.FuzzySearchEnabled).HasColumnName("FuzzySearchEnabled").IsRequired();
            Property(x => x.PolicyValidation).HasColumnName("PolicyValidation").IsRequired();
            Property(x => x.AlwaysDisplayPortfolio).HasColumnName("AlwaysDisplayPortfolio").IsRequired();
            Property(x => x.SubmissionDisplayYears).HasColumnName("SubmissionDisplayYears").IsRequired();
            Property(x => x.DietSearch).HasColumnName("DietSearch").IsRequired();
            Property(x => x.DietSearchOptions).HasColumnName("DietSearchOptions").IsOptional().HasMaxLength(200);
            Property(x => x.UwReferralAddressIndicator).HasColumnName("UWReferralAddressIndicator").IsOptional();
            Property(x => x.RequiresUw).HasColumnName("RequiresUW").IsOptional();
            Property(x => x.RequiresUwAssistant).HasColumnName("RequiresUWAssistant").IsOptional();
            Property(x => x.ClearUwOnLobChange).HasColumnName("ClearUWOnLOBChange").IsOptional();
            Property(x => x.FilterPerformClientSearch).HasColumnName("FilterPerformClientSearch").IsRequired();
            Property(x => x.FilterGetClientInfo).HasColumnName("FilterGetClientInfo").IsRequired();
            Property(x => x.PrimaryUwRole).HasColumnName("PrimaryUWRole").IsOptional().HasMaxLength(50);
            Property(x => x.PrimaryAnRole).HasColumnName("PrimaryANRole").IsOptional().HasMaxLength(50);
            Property(x => x.UseApsForAssignments).HasColumnName("UseAPSForAssignments").IsRequired();
            Property(x => x.FillAllAssignments).HasColumnName("FillAllAssignments").IsRequired();
            Property(x => x.RequiresInsuredOrDba).HasColumnName("RequiresInsuredOrDBA").IsRequired();
            Property(x => x.ApsService).HasColumnName("APSService").IsOptional().HasMaxLength(50);
            Property(x => x.MqName).HasColumnName("MQName").IsOptional().HasMaxLength(50);
            Property(x => x.MqHostName).HasColumnName("MQHostName").IsOptional().HasMaxLength(50);
            Property(x => x.MqChannel).HasColumnName("MQChannel").IsOptional().HasMaxLength(50);
            Property(x => x.MqPort).HasColumnName("MQPort").IsOptional().HasMaxLength(5);
            Property(x => x.MqManager).HasColumnName("MQManager").IsOptional().HasMaxLength(50);
            Property(x => x.FilterInactiveStatusReasons).HasColumnName("FilterInactiveStatusReasons").IsRequired();
            Property(x => x.CopySubmissionStatusOption).HasColumnName("CopySubmissionStatusOption").IsRequired();
            Property(x => x.SubmissionStatusDateTbd).HasColumnName("SubmissionStatusDateTBD").IsOptional();
            Property(x => x.EnsureDefaultStatusOnAdd).HasColumnName("EnsureDefaultStatusOnAdd").IsRequired();
            Property(x => x.UsesFaq).HasColumnName("UsesFAQ").IsRequired();
            Property(x => x.LinkAfterSubStatus).HasColumnName("LinkAfterSubStatus").IsRequired();
            Property(x => x.ClientTypeForSearch).HasColumnName("ClientTypeForSearch").IsOptional().HasMaxLength(15);
            Property(x => x.SegmentWithGetClientInfo).HasColumnName("SegmentWithGetClientInfo").IsRequired();
            Property(x => x.OptionToViewDeletedSubmissions).HasColumnName("OptionToViewDeletedSubmissions").IsRequired();
            Property(x => x.UseApsNewBusinessCancelDate).HasColumnName("UseAPSNewBusinessCancelDate").IsRequired();
            Property(x => x.UseApsRenewalCancelDate).HasColumnName("UseAPSRenewalCancelDate").IsRequired();
            Property(x => x.DefaultApsAssignments).HasColumnName("DefaultAPSAssignments").IsRequired();
            Property(x => x.APSAgencyUrl).HasColumnName("APSAgencyUrl").IsOptional().HasMaxLength(200);
            Property(x => x.DuplicateSubmissionQuery).HasColumnName("DuplicateSubmissionQuery").IsOptional().HasMaxLength(2000);

            // Foreign keys
            HasRequired(a => a.Company).WithMany(b => b.CompanyParameter).HasForeignKey(c => c.CompanyId); // FK_CompanyParameter_Company
        }
    }
}
