using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ValidationOperator
    public class ValidationOperator
    {
        public string ValidationOperatorCode { get; set; } // ValidationOperatorCode (Primary key)
        public string ValidationOperatorName { get; set; } // ValidationOperatorName
        public bool ForStringsOnly { get; set; } // ForStringsOnly
        public string DisplayName { get; set; } // DisplayName
        public string DisplayShortName { get; set; } // DisplayShortName
        public int DisplayOrder { get; set; } // DisplayOrder

        // Reverse navigation
        public virtual ICollection<ValidationCondition> ValidationCondition { get; set; } // ValidationCondition.FK_ValidationCondition_ValidationOperator

        public ValidationOperator()
        {
            ValidationCondition = new List<ValidationCondition>();
        }
    }

    internal class ValidationOperatorConfiguration : EntityTypeConfiguration<ValidationOperator>
    {
        public ValidationOperatorConfiguration()
        {
            ToTable("dbo.ValidationOperator");
            HasKey(x => x.ValidationOperatorCode);

            Property(x => x.ValidationOperatorCode).HasColumnName("ValidationOperatorCode").IsRequired().HasMaxLength(2);
            Property(x => x.ValidationOperatorName).HasColumnName("ValidationOperatorName").IsRequired().HasMaxLength(50);
            Property(x => x.ForStringsOnly).HasColumnName("ForStringsOnly").IsRequired();
            Property(x => x.DisplayName).HasColumnName("DisplayName").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayShortName).HasColumnName("DisplayShortName").IsRequired().HasMaxLength(20);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
