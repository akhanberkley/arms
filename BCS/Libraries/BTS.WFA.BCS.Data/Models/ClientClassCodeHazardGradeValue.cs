using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ClientClassCodeHazardGradeValue
    public class ClientClassCodeHazardGradeValue
    {
        public int Id { get; set; } // Id (Primary key)
        public int ClientClassCodeId { get; set; } // ClientClassCodeId
        public int HazardGradeTypeId { get; set; } // HazardGradeTypeId
        public string HazardGradeValue { get; set; } // HazardGradeValue

        // Foreign keys
        public virtual ClientClassCode ClientClassCode { get; set; } //  FK_ClientClassCodeHazardGradeValue_ClientClassCode
        public virtual HazardGradeType HazardGradeType { get; set; } //  FK_ClientClassCodeHazardGradeValue_HazardGradeType
    }

    internal class ClientClassCodeHazardGradeValueConfiguration : EntityTypeConfiguration<ClientClassCodeHazardGradeValue>
    {
        public ClientClassCodeHazardGradeValueConfiguration()
        {
            ToTable("dbo.ClientClassCodeHazardGradeValue");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ClientClassCodeId).HasColumnName("ClientClassCodeId").IsRequired();
            Property(x => x.HazardGradeTypeId).HasColumnName("HazardGradeTypeId").IsRequired();
            Property(x => x.HazardGradeValue).HasColumnName("HazardGradeValue").IsOptional().HasMaxLength(50);

            // Foreign keys
            HasRequired(a => a.ClientClassCode).WithMany(b => b.ClientClassCodeHazardGradeValue).HasForeignKey(c => c.ClientClassCodeId); // FK_ClientClassCodeHazardGradeValue_ClientClassCode
            HasRequired(a => a.HazardGradeType).WithMany(b => b.ClientClassCodeHazardGradeValue).HasForeignKey(c => c.HazardGradeTypeId); // FK_ClientClassCodeHazardGradeValue_HazardGradeType
        }
    }
}
