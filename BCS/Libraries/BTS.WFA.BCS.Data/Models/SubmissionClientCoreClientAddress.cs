using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionClientCoreClientAddress
    public class SubmissionClientCoreClientAddress
    {
        public int SubmissionId { get; set; } // SubmissionId (Primary key)
        public int ClientCoreClientAddressId { get; set; } // ClientCoreClientAddressId (Primary key)

        // Foreign keys
        public virtual Submission Submission { get; set; } //  FK_SubmissionClientCoreClientAddress_Submission
    }

    internal class SubmissionClientCoreClientAddressConfiguration : EntityTypeConfiguration<SubmissionClientCoreClientAddress>
    {
        public SubmissionClientCoreClientAddressConfiguration()
        {
            ToTable("dbo.SubmissionClientCoreClientAddress");
            HasKey(x => new { x.SubmissionId, x.ClientCoreClientAddressId });

            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ClientCoreClientAddressId).HasColumnName("ClientCoreClientAddressId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Foreign keys
            HasRequired(a => a.Submission).WithMany(b => b.SubmissionClientCoreClientAddress).HasForeignKey(c => c.SubmissionId); // FK_SubmissionClientCoreClientAddress_Submission
        }
    }
}
