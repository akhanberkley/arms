using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionStatusReason
    public class SubmissionStatusReason
    {
        public int Id { get; set; } // Id (Primary key)
        public string ReasonCode { get; set; } // ReasonCode

        // Reverse navigation
        public virtual ICollection<SubmissionLineOfBusinessChildren> SubmissionLineOfBusinessChildren { get; set; } // SubmissionLineOfBusinessChildren.FK_SubmissionLineOfBusinessChildren_SubmissionStatusReason
        public virtual ICollection<SubmissionStatusSubmissionStatusReason> SubmissionStatusSubmissionStatusReason { get; set; } // Many to many mapping

        public SubmissionStatusReason()
        {
            SubmissionLineOfBusinessChildren = new List<SubmissionLineOfBusinessChildren>();
            SubmissionStatusSubmissionStatusReason = new List<SubmissionStatusSubmissionStatusReason>();
        }
    }

    internal class SubmissionStatusReasonConfiguration : EntityTypeConfiguration<SubmissionStatusReason>
    {
        public SubmissionStatusReasonConfiguration()
        {
            ToTable("dbo.SubmissionStatusReason");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ReasonCode).HasColumnName("ReasonCode").IsRequired().HasMaxLength(50);
        }
    }
}
