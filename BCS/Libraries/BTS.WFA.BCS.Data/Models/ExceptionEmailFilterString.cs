
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ExceptionEmailFilterString
    public class ExceptionEmailFilterString
    {
        public int Id { get; set; } // Id (Primary key)
        public string FilterString { get; set; } // FilterString
        public bool Active { get; set; } // Active

        public ExceptionEmailFilterString()
        {
            Active = true;
        }
    }

    internal class ExceptionEmailFilterStringConfiguration : EntityTypeConfiguration<ExceptionEmailFilterString>
    {
        public ExceptionEmailFilterStringConfiguration()
        {
            ToTable("dbo.ExceptionEmailFilterString");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.FilterString).HasColumnName("FilterString").IsRequired().HasMaxLength(200);
            Property(x => x.Active).HasColumnName("Active").IsRequired();
        }
    }
}
