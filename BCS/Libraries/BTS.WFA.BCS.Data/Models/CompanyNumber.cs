using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumber
    public class CompanyNumber
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyId { get; set; } // CompanyId
        public string CompanyNumberValue { get; set; } // CompanyNumber

        // Reverse navigation
        public virtual ICollection<CompanyNumberHazardGradeType> CompanyNumberHazardGradeType { get; set; } // Many to many mapping
        public virtual ICollection<LineOfBusiness> LineOfBusiness { get; set; } // Many to many mapping
        public virtual ICollection<NumberControlType> NumberControlType { get; set; } // Many to many mapping
        public virtual ICollection<PolicySystem> PolicySystem { get; set; } // Many to many mapping
        public virtual ICollection<UnderwritingRole> UnderwritingRole { get; set; } // Many to many mapping
        public virtual ICollection<Agency> Agency { get; set; } // Agency.FK_Agency_CompanyNumber
        public virtual ICollection<ClassCode> ClassCode { get; set; } // ClassCode.FK_ClassCode_CompanyNumber
        public virtual ICollection<Client> Client { get; set; } // Client.FK_Client_CompanyNumber
        public virtual ICollection<CompanyNumberAgentUnspecifiedReason> CompanyNumberAgentUnspecifiedReason { get; set; } // CompanyNumberAgentUnspecifiedReason.FK_CompanyNumberAgentUnspecifiedReason_CompanyNumber
        public virtual ICollection<CompanyNumberAttribute> CompanyNumberAttribute { get; set; } // CompanyNumberAttribute.FK_CompanyNumberAttribute_CompanyNumber
        public virtual ICollection<CompanyNumberCode> CompanyNumberCode { get; set; } // CompanyNumberCode.FK_CompanyNumberCode_CompanyNumber
        public virtual ICollection<CompanyNumberCodeType> CompanyNumberCodeType { get; set; } // CompanyNumberCodeType.FK_CompanyNumberCodeType_CompanyNumber
        public virtual ICollection<CompanyNumberLobGrid> CompanyNumberLobGrid { get; set; } // CompanyNumberLOBGrid.FK_CompanyNumberLOBGrid_CompanyNumber
        public virtual ICollection<CompanyNumberOtherCarrier> CompanyNumberOtherCarrier { get; set; } // Many to many mapping
        public virtual ICollection<CompanyNumberSubmissionStatus> CompanyNumberSubmissionStatus { get; set; } // Many to many mapping
        public virtual ICollection<CompanyNumberSubmissionType> CompanyNumberSubmissionType { get; set; } // Many to many mapping
        public virtual ICollection<Faq> Faq { get; set; } // FAQ.FK_FAQ_CompanyNumber
        public virtual ICollection<NumberControl> NumberControl { get; set; } // NumberControl.FK_NumberControl_CompanyNumber
        public virtual ICollection<Person> Person { get; set; } // Person.FK_Person_CompanyNumber
        public virtual ICollection<SubmissionDisplayGrid> SubmissionDisplayGrid { get; set; } // SubmissionDisplayGrid.FK_SubmissionDisplayGrid_CompanyNumber
        public virtual ICollection<SubmissionNumberControl> SubmissionNumberControl { get; set; } // SubmissionNumberControl.FK_SubmissionNumberControl_CompanyNumber
        public virtual ICollection<SubmissionSearchPage> SubmissionSearchPage { get; set; } // SubmissionSearchPage.FK_SubmissionSearchFilter_CompanyNumber
        public virtual ICollection<CompanyNumberClientCoreSegment> CompanyNumberClientCoreSegment { get; set; }
        public virtual ICollection<AgencyBranch> AgencyBranch { get; set; }

        // Foreign keys
        public virtual Company Company { get; set; } //  FK_CompanyNumber_Company
        public virtual List<CompanyNumberCrossClearance> ClearsCompanyNumbers { get; set; }

        public CompanyNumber()
        {
            CompanyNumberHazardGradeType = new List<CompanyNumberHazardGradeType>();
            LineOfBusiness = new List<LineOfBusiness>();
            AgencyBranch = new List<AgencyBranch>();
            NumberControlType = new List<NumberControlType>();
            PolicySystem = new List<PolicySystem>();
            UnderwritingRole = new List<UnderwritingRole>();
            Agency = new List<Agency>();
            ClassCode = new List<ClassCode>();
            Client = new List<Client>();
            CompanyNumberAgentUnspecifiedReason = new List<CompanyNumberAgentUnspecifiedReason>();
            CompanyNumberAttribute = new List<CompanyNumberAttribute>();
            CompanyNumberCode = new List<CompanyNumberCode>();
            CompanyNumberCodeType = new List<CompanyNumberCodeType>();
            CompanyNumberLobGrid = new List<CompanyNumberLobGrid>();
            CompanyNumberOtherCarrier = new List<CompanyNumberOtherCarrier>();
            CompanyNumberSubmissionStatus = new List<CompanyNumberSubmissionStatus>();
            CompanyNumberSubmissionType = new List<CompanyNumberSubmissionType>();
            Faq = new List<Faq>();
            NumberControl = new List<NumberControl>();
            Person = new List<Person>();
            SubmissionDisplayGrid = new List<SubmissionDisplayGrid>();
            SubmissionNumberControl = new List<SubmissionNumberControl>();
            SubmissionSearchPage = new List<SubmissionSearchPage>();
            CompanyNumberClientCoreSegment = new List<CompanyNumberClientCoreSegment>();
            ClearsCompanyNumbers = new List<CompanyNumberCrossClearance>();
        }
    }

    internal class CompanyNumberConfiguration : EntityTypeConfiguration<CompanyNumber>
    {
        public CompanyNumberConfiguration()
        {
            ToTable("dbo.CompanyNumber");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyId).HasColumnName("CompanyId").IsRequired();
            Property(x => x.CompanyNumberValue).HasColumnName("CompanyNumber").IsRequired().HasMaxLength(3);

            // Foreign keys
            HasRequired(a => a.Company).WithMany(b => b.CompanyNumber).HasForeignKey(c => c.CompanyId); // FK_CompanyNumber_Company
            HasMany(t => t.ClearsCompanyNumbers).WithRequired(t => t.CompanyNumber);
            HasMany(t => t.AgencyBranch).WithRequired(t => t.CompanyNumber);
            HasMany(t => t.LineOfBusiness).WithMany(t => t.CompanyNumber).Map(m =>
            {
                m.ToTable("CompanyNumberLineOfBusiness");
                m.MapLeftKey("CompanyNumberId");
                m.MapRightKey("LineOfBusinessId");
            });
            HasMany(t => t.NumberControlType).WithMany(t => t.CompanyNumber).Map(m =>
            {
                m.ToTable("CompanyNumberNumberControlType");
                m.MapLeftKey("CompanyNumberId");
                m.MapRightKey("NumberControlTypeId");
            });
            HasMany(t => t.PolicySystem).WithMany(t => t.CompanyNumber).Map(m =>
            {
                m.ToTable("CompanyNumberPolicySystem");
                m.MapLeftKey("CompanyNumberId");
                m.MapRightKey("PolicySystemId");
            });
            HasMany(t => t.UnderwritingRole).WithMany(t => t.CompanyNumber).Map(m =>
            {
                m.ToTable("CompanyNumberUnderwritingRole");
                m.MapLeftKey("CompanyNumberId");
                m.MapRightKey("UnderwritingRoleId");
            });
        }
    }
}
