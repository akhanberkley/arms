using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberCode
    public class CompanyNumberCode
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public int CompanyCodeTypeId { get; set; } // CompanyCodeTypeId
        public string Code { get; set; } // Code
        public string Description { get; set; } // Description
        public DateTime? ExpirationDate { get; set; } // ExpirationDate

        // Reverse navigation
        public virtual ICollection<ClientClassCode> ClientClassCode { get; set; } // Many to many mapping
        public virtual ICollection<Client> Client { get; set; } // Many to many mapping
        public virtual ICollection<Submission> Submission { get; set; } // Many to many mapping
        public virtual ICollection<ClassCodeCompanyNumberCode> ClassCodeCompanyNumberCode { get; set; } // ClassCodeCompanyNumberCode.FK_ClassCodeCompanyNumberCode_CompanyNumberCode
        public virtual ICollection<SubmissionCompanyNumberCodeHistory> SubmissionCompanyNumberCodeHistory { get; set; } // SubmissionCompanyNumberCodeHistory.FK_SubmissionCompanyNumberCodeHistory_CompanyNumberCode

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_CompanyNumberCode_CompanyNumber
        public virtual CompanyNumberCodeType CompanyNumberCodeType { get; set; } //  FK_CompanyCode_CompanyCodeType

        public CompanyNumberCode()
        {
            ClientClassCode = new List<ClientClassCode>();
            Client = new List<Client>();
            Submission = new List<Submission>();
            ClassCodeCompanyNumberCode = new List<ClassCodeCompanyNumberCode>();
            SubmissionCompanyNumberCodeHistory = new List<SubmissionCompanyNumberCodeHistory>();
        }
    }

    internal class CompanyNumberCodeConfiguration : EntityTypeConfiguration<CompanyNumberCode>
    {
        public CompanyNumberCodeConfiguration()
        {
            ToTable("dbo.CompanyNumberCode");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.CompanyCodeTypeId).HasColumnName("CompanyCodeTypeId").IsRequired();
            Property(x => x.Code).HasColumnName("Code").IsOptional().HasMaxLength(100);
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(100);
            Property(x => x.ExpirationDate).HasColumnName("ExpirationDate").IsOptional();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.CompanyNumberCode).HasForeignKey(c => c.CompanyNumberId); // FK_CompanyNumberCode_CompanyNumber
            HasRequired(a => a.CompanyNumberCodeType).WithMany(b => b.CompanyNumberCode).HasForeignKey(c => c.CompanyCodeTypeId); // FK_CompanyCode_CompanyCodeType
            HasMany(t => t.Submission).WithMany(t => t.CompanyNumberCode).Map(m =>
            {
                m.ToTable("SubmissionCompanyNumberCode");
                m.MapLeftKey("CompanyNumberCodeId");
                m.MapRightKey("SubmissionId");
            });
            HasMany(t => t.Client).WithMany(t => t.CompanyNumberCode).Map(m =>
            {
                m.ToTable("ClientCompanyNumberCode");
                m.MapLeftKey("CompanyNumberCodeId");
                m.MapRightKey("ClientId");
                
            });
        }
    }
}
