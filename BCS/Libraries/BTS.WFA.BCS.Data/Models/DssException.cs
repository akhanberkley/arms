using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // DSSException
    public class DssException
    {
        public int Id { get; set; } // Id (Primary key)
        public DateTime ActivityDate { get; set; } // ActivityDate
        public string Reason { get; set; } // Reason
        public string PolicyNumber { get; set; } // PolicyNumber
        public string PolicyMod { get; set; } // PolicyMod
        public DateTime? EffectiveDate { get; set; } // EffectiveDate
        public decimal? EstimatedWrittenPremium { get; set; } // EstimatedWrittenPremium
        public DateTime? ExpirationDate { get; set; } // ExpirationDate
        public DateTime? CancellationDate { get; set; } // CancellationDate
    }

    internal class DssExceptionConfiguration : EntityTypeConfiguration<DssException>
    {
        public DssExceptionConfiguration()
        {
            ToTable("dbo.DSSException");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ActivityDate).HasColumnName("ActivityDate").IsRequired();
            Property(x => x.Reason).HasColumnName("Reason").IsRequired().HasMaxLength(50);
            Property(x => x.PolicyNumber).HasColumnName("PolicyNumber").IsOptional().HasMaxLength(50);
            Property(x => x.PolicyMod).HasColumnName("PolicyMod").IsOptional().HasMaxLength(50);
            Property(x => x.EffectiveDate).HasColumnName("EffectiveDate").IsOptional();
            Property(x => x.EstimatedWrittenPremium).HasColumnName("EstimatedWrittenPremium").IsOptional().HasPrecision(19, 4);
            Property(x => x.ExpirationDate).HasColumnName("ExpirationDate").IsOptional();
            Property(x => x.CancellationDate).HasColumnName("CancellationDate").IsOptional();
        }
    }
}
