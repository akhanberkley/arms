using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // AgencyLicensedState
    public class AgencyLicensedState
    {
        public int Id { get; set; } // Id (Primary key)
        public int AgencyId { get; set; } // AgencyId
        public int StateId { get; set; } // StateId
        public long? ApsId { get; set; } // APSId

        // Foreign keys
        public virtual Agency Agency { get; set; } //  FK_AgencyLicensedState_Agency
        public virtual State State { get; set; } //  FK_AgencyLicensedState_State
    }

    internal class AgencyLicensedStateConfiguration : EntityTypeConfiguration<AgencyLicensedState>
    {
        public AgencyLicensedStateConfiguration()
        {
            ToTable("dbo.AgencyLicensedState");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.AgencyId).HasColumnName("AgencyId").IsRequired();
            Property(x => x.StateId).HasColumnName("StateId").IsRequired();
            Property(x => x.ApsId).HasColumnName("APSId").IsOptional();

            // Foreign keys
            HasRequired(a => a.Agency).WithMany(b => b.AgencyLicensedState).HasForeignKey(c => c.AgencyId); // FK_AgencyLicensedState_Agency
            HasRequired(a => a.State).WithMany(b => b.AgencyLicensedState).HasForeignKey(c => c.StateId); // FK_AgencyLicensedState_State
        }
    }
}
