using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // AgentUnspecifiedReason
    public class AgentUnspecifiedReason
    {
        public int Id { get; set; } // Id (Primary key)
        public string Reason { get; set; } // Reason

        // Reverse navigation
        public virtual ICollection<CompanyNumberAgentUnspecifiedReason> CompanyNumberAgentUnspecifiedReason { get; set; } // CompanyNumberAgentUnspecifiedReason.FK_CompanyNumberAgentUnspecifiedReason_AgentUnspecifiedReason

        public AgentUnspecifiedReason()
        {
            CompanyNumberAgentUnspecifiedReason = new List<CompanyNumberAgentUnspecifiedReason>();
        }
    }

    internal class AgentUnspecifiedReasonConfiguration : EntityTypeConfiguration<AgentUnspecifiedReason>
    {
        public AgentUnspecifiedReasonConfiguration()
        {
            ToTable("dbo.AgentUnspecifiedReason");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Reason).HasColumnName("Reason").IsRequired().HasMaxLength(50);
        }
    }
}
