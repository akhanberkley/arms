using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ClientClassCode
    public class ClientClassCode
    {
        public int Id { get; set; } // Id (Primary key)
        public int ClientId { get; set; } // ClientId
        public int ClassCodeId { get; set; } // ClassCodeId

        // Reverse navigation
        public virtual ICollection<CompanyNumberCode> CompanyNumberCode { get; set; } // Many to many mapping
        public virtual ICollection<ClientClassCodeHazardGradeValue> ClientClassCodeHazardGradeValue { get; set; } // ClientClassCodeHazardGradeValue.FK_ClientClassCodeHazardGradeValue_ClientClassCode

        // Foreign keys
        public virtual Client Client { get; set; } //  FK_ClientClassCode_Client
        public virtual ClassCode ClassCode { get; set; } //  FK_ClientClassCode_ClassCode

        public ClientClassCode()
        {
            CompanyNumberCode = new List<CompanyNumberCode>();
            ClientClassCodeHazardGradeValue = new List<ClientClassCodeHazardGradeValue>();
        }
    }

    internal class ClientClassCodeConfiguration : EntityTypeConfiguration<ClientClassCode>
    {
        public ClientClassCodeConfiguration()
        {
            ToTable("dbo.ClientClassCode");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ClientId).HasColumnName("ClientId").IsRequired();
            Property(x => x.ClassCodeId).HasColumnName("ClassCodeId").IsRequired();

            // Foreign keys
            HasRequired(a => a.Client).WithMany(b => b.ClientClassCode).HasForeignKey(c => c.ClientId); // FK_ClientClassCode_Client
            HasRequired(a => a.ClassCode).WithMany().HasForeignKey(c => c.ClassCodeId); // FK_ClientClassCode_ClassCode
            HasMany(t => t.CompanyNumberCode).WithMany(t => t.ClientClassCode).Map(m =>
            {
                m.ToTable("ClientClassCodeCompanyNumberCode");
                m.MapLeftKey("ClientClassCodeId");
                m.MapRightKey("CompanyNumberCodeId");
            });
        }
    }
}
