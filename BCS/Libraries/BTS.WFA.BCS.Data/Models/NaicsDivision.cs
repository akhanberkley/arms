using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // NaicsDivision
    public class NaicsDivision
    {
        public int Id { get; set; } // Id (Primary key)
        public string DivisionDescription { get; set; } // DivisionDescription
        public int NaicsGroupRangeLow { get; set; } // NaicsGroupRangeLow
        public int NaicsGroupRangeHigh { get; set; } // NaicsGroupRangeHigh

        // Reverse navigation
        public virtual NaicsGroup NaicsGroup { get; set; } // NaicsGroup.FK_NaicsGroup_NaicsDivision
    }

    internal class NaicsDivisionConfiguration : EntityTypeConfiguration<NaicsDivision>
    {
        public NaicsDivisionConfiguration()
        {
            ToTable("dbo.NaicsDivision");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.DivisionDescription).HasColumnName("DivisionDescription").IsRequired().HasMaxLength(255);
            Property(x => x.NaicsGroupRangeLow).HasColumnName("NaicsGroupRangeLow").IsRequired();
            Property(x => x.NaicsGroupRangeHigh).HasColumnName("NaicsGroupRangeHigh").IsRequired();
        }
    }
}
