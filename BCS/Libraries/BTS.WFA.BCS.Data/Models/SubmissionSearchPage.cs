using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionSearchPage
    public class SubmissionSearchPage
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public int CompanyParameterId { get; set; } // CompanyParameterId
        public bool? DisplayrowUnderwriterPrimary { get; set; } // DisplayrowUnderwriterPrimary
        public bool? DisplayCreatedBy { get; set; } // DisplayCreatedBy
        public bool? DisplayLastUpdated { get; set; } // DisplayLastUpdated
        public bool? DisplayModifiedBy { get; set; } // DisplayModifiedBy
        public bool? DisplaySubmissionType { get; set; } // DisplaySubmissionType
        public bool? DisplaySubmissionStatus { get; set; } // DisplaySubmissionStatus
        public bool? DisplayAgency { get; set; } // DisplayAgency
        public bool? DisplayUnderwriter { get; set; } // DisplayUnderwriter
        public bool? DisplayUnderwritingUnit { get; set; } // DisplayUnderwritingUnit
        public bool? DisplayEffectiveFrom { get; set; } // DisplayEffectiveFrom
        public bool? DisplayExpirationFrom { get; set; } // DisplayExpirationFrom
        public bool? DisplayPremium { get; set; } // DisplayPremium

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_SubmissionSearchFilter_CompanyNumber
        public virtual CompanyParameter CompanyParameter { get; set; } //  FK_SubmissionSearchFilter_CompanyParameter

        public SubmissionSearchPage()
        {
            DisplayrowUnderwriterPrimary = true;
            DisplayCreatedBy = true;
            DisplayLastUpdated = true;
            DisplayModifiedBy = true;
            DisplaySubmissionType = true;
            DisplaySubmissionStatus = true;
            DisplayAgency = true;
            DisplayUnderwriter = true;
            DisplayUnderwritingUnit = true;
            DisplayEffectiveFrom = true;
            DisplayExpirationFrom = true;
            DisplayPremium = true;
        }
    }

    internal class SubmissionSearchPageConfiguration : EntityTypeConfiguration<SubmissionSearchPage>
    {
        public SubmissionSearchPageConfiguration()
        {
            ToTable("dbo.SubmissionSearchPage");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.CompanyParameterId).HasColumnName("CompanyParameterId").IsRequired();
            Property(x => x.DisplayrowUnderwriterPrimary).HasColumnName("DisplayrowUnderwriterPrimary").IsOptional();
            Property(x => x.DisplayCreatedBy).HasColumnName("DisplayCreatedBy").IsOptional();
            Property(x => x.DisplayLastUpdated).HasColumnName("DisplayLastUpdated").IsOptional();
            Property(x => x.DisplayModifiedBy).HasColumnName("DisplayModifiedBy").IsOptional();
            Property(x => x.DisplaySubmissionType).HasColumnName("DisplaySubmissionType").IsOptional();
            Property(x => x.DisplaySubmissionStatus).HasColumnName("DisplaySubmissionStatus").IsOptional();
            Property(x => x.DisplayAgency).HasColumnName("DisplayAgency").IsOptional();
            Property(x => x.DisplayUnderwriter).HasColumnName("DisplayUnderwriter").IsOptional();
            Property(x => x.DisplayUnderwritingUnit).HasColumnName("DisplayUnderwritingUnit").IsOptional();
            Property(x => x.DisplayEffectiveFrom).HasColumnName("DisplayEffectiveFrom").IsOptional();
            Property(x => x.DisplayExpirationFrom).HasColumnName("DisplayExpirationFrom").IsOptional();
            Property(x => x.DisplayPremium).HasColumnName("DisplayPremium").IsOptional();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.SubmissionSearchPage).HasForeignKey(c => c.CompanyNumberId); // FK_SubmissionSearchFilter_CompanyNumber
            HasRequired(a => a.CompanyParameter).WithMany(b => b.SubmissionSearchPage).HasForeignKey(c => c.CompanyParameterId); // FK_SubmissionSearchFilter_CompanyParameter
        }
    }
}
