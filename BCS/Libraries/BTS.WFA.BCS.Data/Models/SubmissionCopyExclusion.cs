using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionCopyExclusion
    public class SubmissionCopyExclusion
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyId { get; set; } // CompanyId
        public string ControlId { get; set; } // ControlId
        public string Header { get; set; } // Header
        public string Method { get; set; } // Method
        public string MethodParams { get; set; } // MethodParams
        public bool Visible { get; set; } // Visible
        public bool Enabled { get; set; } // Enabled

        // Foreign keys
        public virtual Company Company { get; set; } //  FK_SubmissionCopyExclusion_Company

        public SubmissionCopyExclusion()
        {
            Visible = true;
            Enabled = true;
        }
    }

    internal class SubmissionCopyExclusionConfiguration : EntityTypeConfiguration<SubmissionCopyExclusion>
    {
        public SubmissionCopyExclusionConfiguration()
        {
            ToTable("dbo.SubmissionCopyExclusion");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyId).HasColumnName("CompanyId").IsRequired();
            Property(x => x.ControlId).HasColumnName("ControlId").IsRequired().HasMaxLength(255);
            Property(x => x.Header).HasColumnName("Header").IsRequired().HasMaxLength(255);
            Property(x => x.Method).HasColumnName("Method").IsOptional().HasMaxLength(255);
            Property(x => x.MethodParams).HasColumnName("MethodParams").IsOptional().HasMaxLength(1000);
            Property(x => x.Visible).HasColumnName("Visible").IsRequired();
            Property(x => x.Enabled).HasColumnName("Enabled").IsRequired();

            // Foreign keys
            HasRequired(a => a.Company).WithMany(b => b.SubmissionCopyExclusion).HasForeignKey(c => c.CompanyId); // FK_SubmissionCopyExclusion_Company
        }
    }
}
