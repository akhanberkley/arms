using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Client
    public class Client
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public long ClientCoreClientId { get; set; } // ClientCoreClientId
        public int? SicCodeId { get; set; } // SicCodeId
        public string BusinessDescription { get; set; } // BusinessDescription
        public string EntryBy { get; set; } // EntryBy
        public DateTime EntryDt { get; set; } // EntryDt
        public string ModifiedBy { get; set; } // ModifiedBy
        public string ModifiedDt { get; set; } // ModifiedDt
        public bool RequestToDelete { get; set; } // RequestToDelete
        public int? NaicsCodeId { get; set; } // NaicsCodeId
        public string PhoneNumber { get; set; } // PhoneNumber
        public long? ExperianId { get; set; }
        public Guid? CmsClientId { get; set; }

        // Reverse navigation
        public virtual ICollection<CompanyNumberCode> CompanyNumberCode { get; set; } // Many to many mapping
        public virtual ICollection<ClientClassCode> ClientClassCode { get; set; } // ClientClassCode.FK_ClientClassCode_Client
        public virtual ICollection<ClientCompanyNumberAttributeValue> ClientCompanyNumberAttributeValue { get; set; } // Many to many mapping
        public virtual ICollection<ClientCoreAddressAddressType> ClientCoreAddressAddressType { get; set; } // Many to many mapping
        public virtual ICollection<ClientCoreNameClientNameType> ClientCoreNameClientNameType { get; set; } // Many to many mapping
        public virtual ICollection<CobraAddressCounty> CobraAddressCounty { get; set; } // Many to many mapping

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_Client_CompanyNumber
        public virtual SicCodeList SicCodeList { get; set; } //  FK_Client_SicCodeList
        public virtual NaicsCodeList NaicsCodeList { get; set; }

        public Client()
        {
            EntryBy = "Initialize";
            EntryDt = System.DateTime.Now;
            RequestToDelete = false;
            CompanyNumberCode = new List<CompanyNumberCode>();
            ClientClassCode = new List<ClientClassCode>();
            ClientCompanyNumberAttributeValue = new List<ClientCompanyNumberAttributeValue>();
            ClientCoreAddressAddressType = new List<ClientCoreAddressAddressType>();
            ClientCoreNameClientNameType = new List<ClientCoreNameClientNameType>();
            CobraAddressCounty = new List<CobraAddressCounty>();
        }
    }

    internal class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            ToTable("dbo.Client");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.ClientCoreClientId).HasColumnName("ClientCoreClientId").IsRequired();
            Property(x => x.SicCodeId).HasColumnName("SicCodeId").IsOptional();
            Property(x => x.BusinessDescription).HasColumnName("BusinessDescription").IsOptional().HasMaxLength(500);
            Property(x => x.EntryBy).HasColumnName("EntryBy").IsRequired().HasMaxLength(50);
            Property(x => x.EntryDt).HasColumnName("EntryDt").IsRequired();
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasMaxLength(50);
            Property(x => x.ModifiedDt).HasColumnName("ModifiedDt").IsOptional().HasMaxLength(50);
            Property(x => x.RequestToDelete).HasColumnName("RequestToDelete").IsRequired();
            Property(x => x.NaicsCodeId).HasColumnName("NaicsCodeId").IsOptional();
            Property(x => x.PhoneNumber).HasColumnName("PhoneNumber").IsOptional().HasMaxLength(20);
            Property(x => x.ExperianId).HasColumnName("ExperianId").IsOptional();
            Property(x => x.CmsClientId).HasColumnName("CmsClientId").IsOptional();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.Client).HasForeignKey(c => c.CompanyNumberId); // FK_Client_CompanyNumber
            HasOptional(a => a.SicCodeList).WithMany().HasForeignKey(c => c.SicCodeId); // FK_Client_SicCodeList
            HasOptional(a => a.NaicsCodeList).WithMany().HasForeignKey(c => c.NaicsCodeId); // FK_Client_SicCodeList
        }
    }
}
