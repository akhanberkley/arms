using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    public class ClientField
    {
        public Guid Id { get; set; }
        public string PropertyName { get; set; }
        public string DisplayLabel { get; set; }
        public string ColumnHeading { get; set; }
    }

    internal class ClientFieldConfiguration : EntityTypeConfiguration<ClientField>
    {
        public ClientFieldConfiguration()
        {
            ToTable("dbo.ClientField");
            HasKey(x => x.Id);

            Property(x => x.PropertyName).HasColumnName("PropertyName").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayLabel).HasColumnName("DisplayLabel").IsRequired().HasMaxLength(50);
            Property(x => x.ColumnHeading).HasColumnName("ColumnHeading").IsRequired().HasMaxLength(50);
        }
    }
}
