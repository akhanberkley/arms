using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Contact
    public class Contact
    {
        public int Id { get; set; } // Id (Primary key)
        public long? ApsId { get; set; } // APSId
        public int AgencyId { get; set; } // AgencyId
        public string Name { get; set; } // Name
        public DateTime? DateOfBirth { get; set; } // DateOfBirth
        public string PrimaryPhone { get; set; } // PrimaryPhone
        public string PrimaryPhoneExtension { get; set; } // PrimaryPhoneExtension
        public string SecondaryPhone { get; set; } // SecondaryPhone
        public string SecondaryPhoneExtension { get; set; } // SecondaryPhoneExtension
        public string Fax { get; set; } // Fax
        public string Email { get; set; } // Email
        public DateTime? RetirementDate { get; set; } // RetirementDate

        // Foreign keys
        public virtual Agency Agency { get; set; } //  FK_Contact_Agency
    }

    internal class ContactConfiguration : EntityTypeConfiguration<Contact>
    {
        public ContactConfiguration()
        {
            ToTable("dbo.Contact");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ApsId).HasColumnName("APSId").IsOptional();
            Property(x => x.AgencyId).HasColumnName("AgencyId").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(50);
            Property(x => x.DateOfBirth).HasColumnName("DateOfBirth").IsOptional();
            Property(x => x.PrimaryPhone).HasColumnName("PrimaryPhone").IsOptional().HasMaxLength(10);
            Property(x => x.PrimaryPhoneExtension).HasColumnName("PrimaryPhoneExtension").IsOptional().HasMaxLength(10);
            Property(x => x.SecondaryPhone).HasColumnName("SecondaryPhone").IsOptional().HasMaxLength(10);
            Property(x => x.SecondaryPhoneExtension).HasColumnName("SecondaryPhoneExtension").IsOptional().HasMaxLength(10);
            Property(x => x.Fax).HasColumnName("Fax").IsOptional().HasMaxLength(10);
            Property(x => x.Email).HasColumnName("Email").IsOptional().HasMaxLength(100);
            Property(x => x.RetirementDate).HasColumnName("RetirementDate").IsOptional();

            // Foreign keys
            HasRequired(a => a.Agency).WithMany(b => b.Contact).HasForeignKey(c => c.AgencyId); // FK_Contact_Agency
        }
    }
}
