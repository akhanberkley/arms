using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SecondaryUnitDesignator
    public class SecondaryUnitDesignator
    {
        public string SecondaryUnitDesignator_ { get; set; } // SecondaryUnitDesignator (Primary key)
        public string ApprovedAbbreviation { get; set; } // ApprovedAbbreviation
    }

    internal class SecondaryUnitDesignatorConfiguration : EntityTypeConfiguration<SecondaryUnitDesignator>
    {
        public SecondaryUnitDesignatorConfiguration()
        {
            ToTable("dbo.SecondaryUnitDesignator");
            HasKey(x => x.SecondaryUnitDesignator_);

            Property(x => x.SecondaryUnitDesignator_).HasColumnName("SecondaryUnitDesignator").IsRequired().HasMaxLength(50);
            Property(x => x.ApprovedAbbreviation).HasColumnName("ApprovedAbbreviation").IsOptional().HasMaxLength(50);
        }
    }
}
