using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionDisplayGrid
    public class SubmissionDisplayGrid
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public int AttributeDataTypeId { get; set; } // AttributeDataTypeId
        public string FieldName { get; set; } // FieldName
        public string HeaderText { get; set; } // HeaderText
        public string SupportField { get; set; } // SupportField
        public int DisplayOrder { get; set; } // DisplayOrder
        public int SortOrder { get; set; } // SortOrder
        public bool SortDirection { get; set; } // SortDirection
        public bool RemoteAgencyVisible { get; set; } // RemoteAgencyVisible
        public bool DisplayOnClientSearchScreen { get; set; } // DisplayOnClientSearchScreen
        public bool DisplayOnSubmissionSearchScreen { get; set; } // DisplayOnSubmissionSearchScreen
        public bool DisplayOnSubmissionScreen { get; set; } // DisplayOnSubmissionScreen

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_SubmissionDisplayGrid_CompanyNumber
        public virtual AttributeDataType AttributeDataType { get; set; } //  FK_SubmissionDisplayGrid_AttributeDataType

        public SubmissionDisplayGrid()
        {
            DisplayOrder = 0;
            SortOrder = 0;
            SortDirection = false;
            RemoteAgencyVisible = true;
            DisplayOnSubmissionScreen = false;
        }
    }

    internal class SubmissionDisplayGridConfiguration : EntityTypeConfiguration<SubmissionDisplayGrid>
    {
        public SubmissionDisplayGridConfiguration()
        {
            ToTable("dbo.SubmissionDisplayGrid");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.AttributeDataTypeId).HasColumnName("AttributeDataTypeId").IsRequired();
            Property(x => x.FieldName).HasColumnName("FieldName").IsRequired().HasMaxLength(100);
            Property(x => x.HeaderText).HasColumnName("HeaderText").IsOptional().HasMaxLength(100);
            Property(x => x.SupportField).HasColumnName("SupportField").IsOptional().HasMaxLength(255);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.SortOrder).HasColumnName("SortOrder").IsRequired();
            Property(x => x.SortDirection).HasColumnName("SortDirection").IsRequired();
            Property(x => x.RemoteAgencyVisible).HasColumnName("RemoteAgencyVisible").IsRequired();
            Property(x => x.DisplayOnClientSearchScreen).HasColumnName("DisplayOnClientSearchScreen").IsRequired();
            Property(x => x.DisplayOnSubmissionSearchScreen).HasColumnName("DisplayOnSubmissionSearchScreen").IsRequired();
            Property(x => x.DisplayOnSubmissionScreen).HasColumnName("DisplayOnSubmissionScreen").IsRequired();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.SubmissionDisplayGrid).HasForeignKey(c => c.CompanyNumberId); // FK_SubmissionDisplayGrid_CompanyNumber
            HasRequired(a => a.AttributeDataType).WithMany(b => b.SubmissionDisplayGrid).HasForeignKey(c => c.AttributeDataTypeId); // FK_SubmissionDisplayGrid_AttributeDataType
        }
    }
}
