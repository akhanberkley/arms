using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // NaicsCodeList
    public class NaicsCodeList
    {
        public int Id { get; set; } // Id (Primary key)
        public int NaicsGroupId { get; set; } // NaicsGroupId
        public int SicCodeListId { get; set; } // SicCodeListId
        public string Code { get; set; } // Code
        public string CodeDescription { get; set; } // CodeDescription

        // Foreign keys
        public virtual NaicsGroup NaicsGroup { get; set; } //  FK_NaicsCodeList_NaicsGroup
        public virtual SicCodeList SicCodeList { get; set; } //  FK_NaicsCodeList_SicCodeList
    }

    internal class NaicsCodeListConfiguration : EntityTypeConfiguration<NaicsCodeList>
    {
        public NaicsCodeListConfiguration()
        {
            ToTable("dbo.NaicsCodeList");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.NaicsGroupId).HasColumnName("NaicsGroupId").IsRequired();
            Property(x => x.SicCodeListId).HasColumnName("SicCodeListId").IsRequired();
            Property(x => x.Code).HasColumnName("Code").IsRequired().HasMaxLength(6);
            Property(x => x.CodeDescription).HasColumnName("CodeDescription").IsRequired().HasMaxLength(255);

            // Foreign keys
            HasRequired(a => a.NaicsGroup).WithMany(b => b.NaicsCodeList).HasForeignKey(c => c.NaicsGroupId); // FK_NaicsCodeList_NaicsGroup
            HasRequired(a => a.SicCodeList).WithMany(b => b.NaicsCodeList).HasForeignKey(c => c.SicCodeListId); // FK_NaicsCodeList_SicCodeList
        }
    }
}
