using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    public class SubmissionAddress
    {
        public Guid Id { get; set; }
        public int SubmissionId { get; set; }
        public int? ClientCoreAddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County { get; set; }
        public string PostalCode { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int? CountryId { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDt { get; set; }

        public virtual Submission Submission { get; set; }
        public virtual Country Country { get; set; }
    }

    internal class SubmissionAddressConfiguration : EntityTypeConfiguration<SubmissionAddress>
    {
        public SubmissionAddressConfiguration()
        {
            ToTable("dbo.SubmissionAddress");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired();
            Property(x => x.ClientCoreAddressId).HasColumnName("ClientCoreAddressId").IsOptional();
            Property(x => x.CountryId).HasColumnName("CountryId").IsOptional();
            Property(x => x.Address1).HasColumnName("Address1").IsOptional().HasMaxLength(200);
            Property(x => x.Address2).HasColumnName("Address2").IsOptional().HasMaxLength(200);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(200);
            Property(x => x.State).HasColumnName("State").IsOptional().HasMaxLength(200);
            Property(x => x.County).HasColumnName("County").IsOptional().HasMaxLength(200);
            Property(x => x.PostalCode).HasColumnName("PostalCode").IsOptional().HasMaxLength(200);
            Property(x => x.Latitude).HasColumnName("Latitude").IsOptional().HasPrecision(9, 6);
            Property(x => x.Longitude).HasColumnName("Longitude").IsOptional().HasPrecision(9, 6);
            Property(x => x.EntryBy).HasColumnName("EntryBy").IsRequired().HasMaxLength(50);
            Property(x => x.EntryDt).HasColumnName("EntryDt").IsRequired();
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasMaxLength(50);
            Property(x => x.ModifiedDt).HasColumnName("ModifiedDt").IsOptional();

            HasRequired(a => a.Submission).WithMany(b => b.Addresses).HasForeignKey(c => c.SubmissionId);
            HasOptional(a => a.Country).WithMany().HasForeignKey(c => c.CountryId);
        }
    }
}
