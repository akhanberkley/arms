using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Role
    public class Role
    {
        public int Id { get; set; } // Id (Primary key)
        public string RoleName { get; set; } // RoleName
        public bool ReadOnly { get; set; } // ReadOnly
        public bool UniversalAccess { get; set; } // UniversalAccess
        public string Description { get; set; }

        // Reverse navigation
        public virtual ICollection<Faq> Faq { get; set; } // Many to many mapping
        public virtual ICollection<User> User { get; set; } // Many to many mapping

        public Role()
        {
            ReadOnly = false;
            UniversalAccess = false;
            Faq = new List<Faq>();
            User = new List<User>();
        }
    }

    internal class RoleConfiguration : EntityTypeConfiguration<Role>
    {
        public RoleConfiguration()
        {
            ToTable("dbo.Role");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RoleName).HasColumnName("RoleName").IsRequired().HasMaxLength(255);
            Property(x => x.ReadOnly).HasColumnName("ReadOnly").IsRequired();
            Property(x => x.UniversalAccess).HasColumnName("UniversalAccess").IsRequired();
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(200);
            HasMany(t => t.User).WithMany(t => t.Role).Map(m =>
            {
                m.ToTable("UserRole");
                m.MapLeftKey("RoldId");
                m.MapRightKey("UserId");
            });
        }
    }
}
