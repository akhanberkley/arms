using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SicCodeList
    public class SicCodeList
    {
        public int Id { get; set; } // Id (Primary key)
        public int SicGroupId { get; set; } // SicGroupId
        public string Code { get; set; } // Code
        public string CodeDescription { get; set; } // CodeDescription

        // Reverse navigation
        public virtual ICollection<ClassCode> ClassCode { get; set; } // ClassCode.FK_ClassCode_SicCodeList
        public virtual ICollection<NaicsCodeList> NaicsCodeList { get; set; } // NaicsCodeList.FK_NaicsCodeList_SicCodeList

        // Foreign keys
        public virtual SicGroup SicGroup { get; set; } //  FK_SicCodeList_SicGroup

        public SicCodeList()
        {
            ClassCode = new List<ClassCode>();
            NaicsCodeList = new List<NaicsCodeList>();
        }
    }

    internal class SicCodeListConfiguration : EntityTypeConfiguration<SicCodeList>
    {
        public SicCodeListConfiguration()
        {
            ToTable("dbo.SicCodeList");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.SicGroupId).HasColumnName("SicGroupId").IsRequired();
            Property(x => x.Code).HasColumnName("Code").IsRequired().HasMaxLength(4);
            Property(x => x.CodeDescription).HasColumnName("CodeDescription").IsRequired().HasMaxLength(255);

            // Foreign keys
            HasRequired(a => a.SicGroup).WithMany(b => b.SicCodeList).HasForeignKey(c => c.SicGroupId); // FK_SicCodeList_SicGroup
        }
    }
}
