using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Agency
    public class Agency
    {
        public int Id { get; set; } // Id (Primary key)
        public long? ApsId { get; set; } // APSId
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public int? MasterAgencyId { get; set; } // MasterAgencyId
        public bool IsMaster { get; set; } // IsMaster
        public string Fein { get; set; } // FEIN
        public string AgencyNumber { get; set; } // AgencyNumber
        public string AgencyName { get; set; } // AgencyName
        public string ReferralAgencyNumber { get; set; } // ReferralAgencyNumber
        public DateTime? ReferralDate { get; set; } // ReferralDate
        public string Address { get; set; } // Address
        public string Address2 { get; set; } // Address2
        public string City { get; set; } // City
        public string State { get; set; } // State
        public string Zip { get; set; } // Zip
        public decimal? Phone { get; set; } // Phone
        public decimal? Fax { get; set; } // Fax
        public string ImageFax { get; set; } // ImageFax
        public string Email { get; set; } // Email
        public string ContactPerson { get; set; } // ContactPerson
        public string ContactPersonPhone { get; set; } // ContactPersonPhone
        public string ContactPersonExtension { get; set; } // ContactPersonExtension
        public string ContactPersonFax { get; set; } // ContactPersonFax
        public string ContactPersonEmail { get; set; } // ContactPersonEmail
        public DateTime EffectiveDate { get; set; } // EffectiveDate
        public DateTime? CancelDate { get; set; } // CancelDate
        public DateTime? RenewalCancelDate { get; set; } // RenewalCancelDate
        public string StateOfIncorporation { get; set; } // StateOfIncorporation
        public string StateTaxId { get; set; } // StateTaxId
        public string Comments { get; set; } // Comments
        public bool Active { get; set; } // Active
        public string EntryBy { get; set; } // EntryBy
        public DateTime EntryDt { get; set; } // EntryDt
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedDt { get; set; } // ModifiedDt
        public string CaptiveBroker { get; set; } // CaptiveBroker

        // Reverse navigation
        public virtual ICollection<AgencyLicensedState> AgencyLicensedState { get; set; } // AgencyLicensedState.FK_AgencyLicensedState_Agency
        public virtual ICollection<AgencyLineOfBusiness> AgencyLineOfBusiness { get; set; } // Many to many mapping
        public virtual ICollection<Agent> Agent { get; set; } // Agent.FK_Agent_Agency
        public virtual ICollection<Contact> Contact { get; set; } // Contact.FK_Contact_Agency
        public virtual ICollection<AgencyBranch> AgencyBranch { get; set; }

        public virtual ICollection<Agency> Children { get; set; }

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_Agency_CompanyNumber
        public virtual Agency MasterAgency { get; set; } //  FK_Agency_Agency

        public Agency()
        {
            IsMaster = false;
            Active = true;
            EffectiveDate = new DateTime(2005, 1, 1);
            EntryDt = System.DateTime.Now;
            AgencyLicensedState = new List<AgencyLicensedState>();
            AgencyLineOfBusiness = new List<AgencyLineOfBusiness>();
            Agent = new List<Agent>();
            Contact = new List<Contact>();
            AgencyBranch = new List<AgencyBranch>();
        }
    }

    internal class AgencyConfiguration : EntityTypeConfiguration<Agency>
    {
        public AgencyConfiguration()
        {
            ToTable("dbo.Agency");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ApsId).HasColumnName("APSId").IsOptional();
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.MasterAgencyId).HasColumnName("MasterAgencyId").IsOptional();
            Property(x => x.IsMaster).HasColumnName("IsMaster").IsRequired();
            Property(x => x.Fein).HasColumnName("FEIN").IsOptional().HasMaxLength(50);
            Property(x => x.AgencyNumber).HasColumnName("AgencyNumber").IsRequired().HasMaxLength(15);
            Property(x => x.AgencyName).HasColumnName("AgencyName").IsRequired().HasMaxLength(255);
            Property(x => x.ReferralAgencyNumber).HasColumnName("ReferralAgencyNumber").IsOptional().HasMaxLength(9);
            Property(x => x.ReferralDate).HasColumnName("ReferralDate").IsOptional();
            Property(x => x.Address).HasColumnName("Address").IsOptional().HasMaxLength(50);
            Property(x => x.Address2).HasColumnName("Address2").IsOptional().HasMaxLength(50);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(50);
            Property(x => x.State).HasColumnName("State").IsOptional().HasMaxLength(2);
            Property(x => x.Zip).HasColumnName("Zip").IsOptional().HasMaxLength(10);
            Property(x => x.Phone).HasColumnName("Phone").IsOptional();
            Property(x => x.Fax).HasColumnName("Fax").IsOptional();
            Property(x => x.ImageFax).HasColumnName("ImageFax").IsOptional().HasMaxLength(16);
            Property(x => x.Email).HasColumnName("Email").IsOptional().HasMaxLength(100);
            Property(x => x.ContactPerson).HasColumnName("ContactPerson").IsOptional().HasMaxLength(100);
            Property(x => x.ContactPersonPhone).HasColumnName("ContactPersonPhone").IsOptional().HasMaxLength(10);
            Property(x => x.ContactPersonExtension).HasColumnName("ContactPersonExtension").IsOptional().HasMaxLength(10);
            Property(x => x.ContactPersonFax).HasColumnName("ContactPersonFax").IsOptional().HasMaxLength(10);
            Property(x => x.ContactPersonEmail).HasColumnName("ContactPersonEmail").IsOptional().HasMaxLength(100);
            Property(x => x.EffectiveDate).HasColumnName("EffectiveDate").IsRequired();
            Property(x => x.CancelDate).HasColumnName("CancelDate").IsOptional();
            Property(x => x.RenewalCancelDate).HasColumnName("RenewalCancelDate").IsOptional();
            Property(x => x.StateOfIncorporation).HasColumnName("StateOfIncorporation").IsOptional().HasMaxLength(2);
            Property(x => x.StateTaxId).HasColumnName("StateTaxId").IsOptional().HasMaxLength(11);
            Property(x => x.Comments).HasColumnName("Comments").IsOptional().HasMaxLength(255);
            Property(x => x.Active).HasColumnName("Active").IsRequired();
            Property(x => x.EntryBy).HasColumnName("EntryBy").IsRequired().HasMaxLength(50);
            Property(x => x.EntryDt).HasColumnName("EntryDt").IsRequired();
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasMaxLength(50);
            Property(x => x.ModifiedDt).HasColumnName("ModifiedDt").IsOptional();
            Property(x => x.CaptiveBroker).HasColumnName("CaptiveBroker").IsOptional().HasMaxLength(50);

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.Agency).HasForeignKey(c => c.CompanyNumberId); // FK_Agency_CompanyNumber
            HasOptional(a => a.MasterAgency).WithMany().HasForeignKey(c => c.MasterAgencyId); // FK_Agency_Agency
            HasMany(t => t.AgencyBranch).WithMany().Map(m =>
            {
                m.ToTable("AgencyAgencyBranch");
                m.MapLeftKey("AgencyId");
                m.MapRightKey("AgencyBranchId");
            });

            HasMany(p => p.Children).WithOptional(a => a.MasterAgency);
        }
    }
}
