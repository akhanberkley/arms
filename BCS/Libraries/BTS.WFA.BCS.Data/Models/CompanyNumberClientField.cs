using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberSubmissionType
    public class CompanyNumberClientField
    {
        public int CompanyNumberId { get; set; }
        public Guid ClientFieldId { get; set; }
        public bool Required { get; set; }
        public bool VisibleOnAdd { get; set; }
        public string DisplayLabel { get; set; }

        public virtual CompanyNumber CompanyNumber { get; set; }
        public virtual ClientField ClientField { get; set; }
    }

    internal class CompanyNumberClientFieldConfiguration : EntityTypeConfiguration<CompanyNumberClientField>
    {
        public CompanyNumberClientFieldConfiguration()
        {
            ToTable("dbo.CompanyNumberClientField");
            HasKey(x => new { x.CompanyNumberId, x.ClientFieldId });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ClientFieldId).HasColumnName("ClientFieldId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Required).HasColumnName("Required").IsRequired();
            Property(x => x.VisibleOnAdd).HasColumnName("VisibleOnAdd").IsRequired();
            Property(x => x.DisplayLabel).HasColumnName("DisplayLabel").HasMaxLength(50).IsOptional();

            HasRequired(a => a.CompanyNumber).WithMany().HasForeignKey(c => c.CompanyNumberId);
            HasRequired(a => a.ClientField).WithMany().HasForeignKey(c => c.ClientFieldId);
        }
    }
}
