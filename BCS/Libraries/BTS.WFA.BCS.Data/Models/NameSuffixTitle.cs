using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // NameSuffixTitle
    public class NameSuffixTitle
    {
        public string SuffixTitle { get; set; } // SuffixTitle
        public int Id { get; set; } // Id (Primary key)
    }

    internal class NameSuffixTitleConfiguration : EntityTypeConfiguration<NameSuffixTitle>
    {
        public NameSuffixTitleConfiguration()
        {
            ToTable("dbo.NameSuffixTitle");
            HasKey(x => x.Id);

            Property(x => x.SuffixTitle).HasColumnName("SuffixTitle").IsRequired().HasMaxLength(50);
            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
