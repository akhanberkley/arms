using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberCodeType
    public class CompanyNumberCodeType
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public string CodeName { get; set; } // CodeName
        public bool ClientSpecific { get; set; } // ClientSpecific
        public bool Required { get; set; } // Required
        public short DisplayOrder { get; set; } // DisplayOrder
        public int? DefaultCompanyNumberCodeId { get; set; } // DefaultCompanyNumberCodeId
        public bool VisibleInLobGridSection { get; set; } // VisibleInLOBGridSection
        public bool ClassCodeSpecific { get; set; } // ClassCodeSpecific
        public bool OtherCarrierSpecific { get; set; } // OtherCarrierSpecific
        public bool LineOfBusinessSpecific { get; set; } // LineOfBusinessSpecific
        public bool VisibleOnAdd { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public bool ExcludeValueOnCopy { get; set; }
        public bool VisibleOnBPM { get; set; }

        // Reverse navigation
        public virtual ICollection<ClassCodeCompanyNumberCode> ClassCodeCompanyNumberCode { get; set; } // Many to many mapping
        public virtual ICollection<CompanyNumberCode> CompanyNumberCode { get; set; } // CompanyNumberCode.FK_CompanyCode_CompanyCodeType

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_CompanyNumberCodeType_CompanyNumber
        public virtual CompanyNumberCode DefaultCompanyNumberCode { get; set; } //  FK_CompanyNumberCodeType_CompanyNumberCode

        public CompanyNumberCodeType()
        {
            ClientSpecific = false;
            Required = false;
            DisplayOrder = 0;
            VisibleInLobGridSection = false;
            ClassCodeSpecific = false;
            OtherCarrierSpecific = false;
            LineOfBusinessSpecific = true;
            ClassCodeCompanyNumberCode = new List<ClassCodeCompanyNumberCode>();
            CompanyNumberCode = new List<CompanyNumberCode>();
        }
    }

    internal class CompanyNumberCodeTypeConfiguration : EntityTypeConfiguration<CompanyNumberCodeType>
    {
        public CompanyNumberCodeTypeConfiguration()
        {
            ToTable("dbo.CompanyNumberCodeType");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.CodeName).HasColumnName("CodeName").IsRequired().HasMaxLength(50);
            Property(x => x.ClientSpecific).HasColumnName("ClientSpecific").IsRequired();
            Property(x => x.Required).HasColumnName("Required").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.DefaultCompanyNumberCodeId).HasColumnName("DefaultCompanyNumberCodeId").IsOptional();
            Property(x => x.VisibleInLobGridSection).HasColumnName("VisibleInLOBGridSection").IsRequired();
            Property(x => x.ClassCodeSpecific).HasColumnName("ClassCodeSpecific").IsRequired();
            Property(x => x.OtherCarrierSpecific).HasColumnName("OtherCarrierSpecific").IsRequired();
            Property(x => x.LineOfBusinessSpecific).HasColumnName("LineOfBusinessSpecific").IsRequired();
            Property(x => x.VisibleOnAdd).HasColumnName("VisibleOnAdd").IsRequired();
            Property(x => x.ExpirationDate).HasColumnName("ExpirationDate").IsOptional();
            Property(x => x.ExcludeValueOnCopy).HasColumnName("ExcludeValueOnCopy").IsRequired();
            Property(x => x.VisibleOnBPM).HasColumnName("VisibleOnBPM").IsRequired();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.CompanyNumberCodeType).HasForeignKey(c => c.CompanyNumberId); // FK_CompanyNumberCodeType_CompanyNumber
            HasOptional(a => a.DefaultCompanyNumberCode).WithMany().HasForeignKey(c => c.DefaultCompanyNumberCodeId); // FK_CompanyNumberCodeType_CompanyNumberCode
        }
    }
}
