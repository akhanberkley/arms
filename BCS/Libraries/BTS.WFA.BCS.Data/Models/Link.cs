using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Link
    public class Link
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyId { get; set; } // CompanyId
        public string Url { get; set; } // URL
        public string Description { get; set; } // Description
        public int DisplayOrder { get; set; } // DisplayOrder

        // Foreign keys
        public virtual Company Company { get; set; } //  FK_Link_Company

        public Link()
        {
            DisplayOrder = 0;
        }
    }

    internal class LinkConfiguration : EntityTypeConfiguration<Link>
    {
        public LinkConfiguration()
        {
            ToTable("dbo.Link");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyId).HasColumnName("CompanyId").IsRequired();
            Property(x => x.Url).HasColumnName("URL").IsRequired().HasMaxLength(255);
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(510);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            // Foreign keys
            HasRequired(a => a.Company).WithMany(b => b.Link).HasForeignKey(c => c.CompanyId); // FK_Link_Company
        }
    }
}
