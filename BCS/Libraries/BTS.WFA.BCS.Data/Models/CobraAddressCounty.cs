using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CobraAddressCounty
    public class CobraAddressCounty
    {
        public int ClientId { get; set; } // ClientId (Primary key)
        public int ClientAddressId { get; set; } // ClientAddressId (Primary key)
        public string County { get; set; } // County

        // Foreign keys
        public virtual Client Client { get; set; } //  FK_CobraAddressCounty_Client
    }

    internal class CobraAddressCountyConfiguration : EntityTypeConfiguration<CobraAddressCounty>
    {
        public CobraAddressCountyConfiguration()
        {
            ToTable("dbo.CobraAddressCounty");
            HasKey(x => new { x.ClientId, x.ClientAddressId });

            Property(x => x.ClientId).HasColumnName("ClientId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ClientAddressId).HasColumnName("ClientAddressId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.County).HasColumnName("County").IsOptional().HasMaxLength(100);

            // Foreign keys
            HasRequired(a => a.Client).WithMany(b => b.CobraAddressCounty).HasForeignKey(c => c.ClientId); // FK_CobraAddressCounty_Client
        }
    }
}
