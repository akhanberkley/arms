using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    public class PolicySymbol
    {
        public Guid Id { get; set; }
        public int CompanyNumberId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime? RetirementDate { get; set; }
        public bool PrimaryOption { get; set; }
        public bool PackageOption { get; set; }
        public int? NumberControlId { get; set; }
        public string PolicyNumberPrefix { get; set; }

        public NumberControl NumberControl { get; set; }
    }

    internal class PolicySymbolConfiguration : EntityTypeConfiguration<PolicySymbol>
    {
        public PolicySymbolConfiguration()
        {
            ToTable("dbo.PolicySymbol");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.Code).HasColumnName("Code").IsRequired().HasMaxLength(25);
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(100);
            Property(x => x.RetirementDate).HasColumnName("RetirementDate").IsOptional();
            Property(x => x.PrimaryOption).HasColumnName("PrimaryOption").IsRequired();
            Property(x => x.PackageOption).HasColumnName("PackageOption").IsRequired();
            Property(x => x.NumberControlId).HasColumnName("NumberControlId").IsOptional();
            Property(x => x.PolicyNumberPrefix).HasColumnName("PolicyNumberPrefix").IsOptional().HasMaxLength(10);

            HasOptional(a => a.NumberControl).WithMany().HasForeignKey(c => c.NumberControlId);
        }
    }
}
