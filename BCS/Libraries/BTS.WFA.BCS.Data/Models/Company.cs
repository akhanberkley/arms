using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Company
    public class Company
    {
        public int Id { get; set; } // Id (Primary key)
        public string CompanyName { get; set; } // CompanyName
        public string CompanyInitials { get; set; } // CompanyInitials
        public bool? AllowSubmissionsOnError { get; set; } // AllowSubmissionsOnError
        public int ClientSystemTypeId { get; set; } // ClientSystemTypeId
        public string Dsik { get; set; } // DSIK
        public string ClientCoreEndPointUrl { get; set; } // ClientCoreEndPointUrl
        public string ClientCoreSystemCd { get; set; } // ClientCoreSystemCd
        public string ClientCoreUsername { get; set; } // ClientCoreUsername
        public string ClientCorePassword { get; set; } // ClientCorePassword
        public string ClientCoreLocalId { get; set; } // ClientCoreLocalId
        public string ClientCoreCorpId { get; set; } // ClientCoreCorpId
        public string ClientCoreCorpCode { get; set; } // ClientCoreCorpCode
        public string ClientCoreOwner { get; set; } // ClientCoreOwner
        public string ClientCoreClientType { get; set; } // ClientCoreClientType
        public int? UnderwriterRoleId { get; set; } // UnderwriterRoleId
        public int? AnalystRoleId { get; set; } // AnalystRoleId
        public int? TechnicianRoleId { get; set; } // TechnicianRoleId
        public string LogoUrl { get; set; } // LogoUrl
        public string ThemeStyleSheet { get; set; } // ThemeStyleSheet
        public string JavascriptFile { get; set; } // JavascriptFile
        public string SearchFieldOrder { get; set; } // SearchFieldOrder
        public string SubmissionScreenFieldOrder { get; set; } // SubmissionScreenFieldOrder
        public string SubmissionScreenCodeTypeOrder { get; set; } // SubmissionScreenCodeTypeOrder
        public string SubmissionScreenAttributeOrder { get; set; } // SubmissionScreenAttributeOrder
        public string ClientScreenFieldOrder { get; set; } // ClientScreenFieldOrder
        public string ClientScreenCodeTypeOrder { get; set; } // ClientScreenCodeTypeOrder
        public string ClientScreenAttributeOrder { get; set; } // ClientScreenAttributeOrder
        public bool SupportsIndividualClients { get; set; } // SupportsIndividualClients
        public bool SupportsInternationalAddresses { get; set; } // SupportsInternationalAddresses
        public bool SupportsClassCodes { get; set; } // SupportsClassCodes
        public bool SupportsDuplicates { get; set; } // SupportsDuplicates
        public bool EnforceAgencyLicensedState { get; set; } // EnforceAgencyLicensedState
        public string LobGridUpdateStatuses { get; set; } // LOBGridUpdateStatuses
        public string LobGridUpdateExcludedStatuses { get; set; } // LOBGridUpdateExcludedStatuses
        public bool SupportsMultipleLineOfBusinesses { get; set; } // SupportsMultipleLineOfBusinesses
        public bool SupportsAgentUnspecifiedReasons { get; set; } // SupportsAgentUnspecifiedReasons
        public bool RequiresAddressType { get; set; } // RequiresAddressType
        public bool RequiresNameType { get; set; } // RequiresNameType
        public bool DesiresInsuredAutoSelectWhenOnlyOne { get; set; } // DesiresInsuredAutoSelectWhenOnlyOne
        public bool DesiresDbaAutoSelectWhenOnlyOne { get; set; } // DesiresDBAAutoSelectWhenOnlyOne
        public bool SearchOnPolicyNumberOrSubmissonNumber { get; set; } // SearchOnPolicyNumberOrSubmissonNumber
        public bool FillAllUnderwriters { get; set; } // FillAllUnderwriters
        public bool DesiresDefaultStatusOnSubmissionCopy { get; set; } // DesiresDefaultStatusOnSubmissionCopy
        public bool RequiresPolicyNumber { get; set; } // RequiresPolicyNumber
        public bool GenerateOnlyPolicyNumber { get; set; } // GenerateOnlyPolicyNumber
        public bool FreehandOnlyPolicyNumber { get; set; } // FreehandOnlyPolicyNumber
        public bool SupportsShellSubmission { get; set; } // SupportsShellSubmission
        public bool UsesAps { get; set; } // UsesAPS
        public string Apsurl { get; set; } // APSURL
        public string Apsdsik { get; set; } // APSDSIK
        public bool AgenciesInGrid { get; set; } // AgenciesInGrid
        public bool AutoSelectNamesOnSubmissionAdd { get; set; } // AutoSelectNamesOnSubmissionAdd
        public string PolicyNumberRegex { get; set; } // PolicyNumberRegex
        public bool LoadClientBeforeStep { get; set; } // LoadClientBeforeStep
        public int? AgencyPortMatchLength { get; set; } // AgencyPortMatchLength
        public bool AllowClientPrimaryChanges { get; set; } // AllowClientPrimaryChanges
        public bool Active { get; set; }

        // Reverse navigation
        public virtual ICollection<User> User { get; set; } // Many to many mapping
        public virtual ICollection<CompanyClientAdvancedSearch> CompanyClientAdvancedSearch { get; set; } // CompanyClientAdvancedSearch.FK_CompanyClientAdvancedSearch_Company
        public virtual ICollection<CompanyNumber> CompanyNumber { get; set; } // CompanyNumber.FK_CompanyNumber_Company
        public virtual ICollection<CompanyParameter> CompanyParameter { get; set; } // CompanyParameter.FK_CompanyParameter_Company
        public virtual ICollection<Link> Link { get; set; } // Link.FK_Link_Company
        public virtual ICollection<SubmissionCopyExclusion> SubmissionCopyExclusion { get; set; } // SubmissionCopyExclusion.FK_SubmissionCopyExclusion_Company
        public virtual ICollection<SubmissionPage> SubmissionPage { get; set; } // SubmissionPage.FK_SubmissionPage_Company
        public virtual ICollection<Validation> Validation { get; set; } // Validation.FK_Validation_Company
        public virtual ValidationCondition ValidationCondition { get; set; } // ValidationCondition.FK_ValidationCondition_Company
        public virtual ICollection<ValidationField> ValidationField { get; set; } // ValidationField.FK_ValidationField_Company

        // Foreign keys
        public virtual UnderwritingRole UnderwritingRole { get; set; }
        public virtual UnderwritingRole AnalystRole { get; set; }
        public virtual UnderwritingRole TechnicianRole { get; set; }

        public Company()
        {
            ClientSystemTypeId = 1;
            SupportsIndividualClients = false;
            SupportsInternationalAddresses = true;
            SupportsClassCodes = true;
            SupportsDuplicates = true;
            EnforceAgencyLicensedState = false;
            SupportsMultipleLineOfBusinesses = false;
            SupportsAgentUnspecifiedReasons = false;
            RequiresAddressType = false;
            RequiresNameType = false;
            DesiresInsuredAutoSelectWhenOnlyOne = true;
            DesiresDbaAutoSelectWhenOnlyOne = true;
            SearchOnPolicyNumberOrSubmissonNumber = false;
            FillAllUnderwriters = false;
            DesiresDefaultStatusOnSubmissionCopy = false;
            RequiresPolicyNumber = false;
            GenerateOnlyPolicyNumber = false;
            FreehandOnlyPolicyNumber = false;
            SupportsShellSubmission = true;
            UsesAps = false;
            AgenciesInGrid = false;
            AutoSelectNamesOnSubmissionAdd = false;
            PolicyNumberRegex = ".*";
            LoadClientBeforeStep = false;
            AllowClientPrimaryChanges = false;
            User = new List<User>();
            CompanyClientAdvancedSearch = new List<CompanyClientAdvancedSearch>();
            CompanyNumber = new List<CompanyNumber>();
            CompanyParameter = new List<CompanyParameter>();
            Link = new List<Link>();
            SubmissionCopyExclusion = new List<SubmissionCopyExclusion>();
            SubmissionPage = new List<SubmissionPage>();
            Validation = new List<Validation>();
            ValidationField = new List<ValidationField>();
        }
    }

    internal class CompanyConfiguration : EntityTypeConfiguration<Company>
    {
        public CompanyConfiguration()
        {
            ToTable("dbo.Company");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyName).HasColumnName("CompanyName").IsRequired().HasMaxLength(255);
            Property(x => x.CompanyInitials).HasColumnName("CompanyInitials").IsRequired().HasMaxLength(4);
            Property(x => x.AllowSubmissionsOnError).HasColumnName("AllowSubmissionsOnError").IsOptional();
            Property(x => x.ClientSystemTypeId).HasColumnName("ClientSystemTypeId").IsRequired();
            Property(x => x.Dsik).HasColumnName("DSIK").IsOptional().HasMaxLength(50);
            Property(x => x.ClientCoreEndPointUrl).HasColumnName("ClientCoreEndPointUrl").IsOptional().HasMaxLength(1000);
            Property(x => x.ClientCoreSystemCd).HasColumnName("ClientCoreSystemCd").IsOptional().HasMaxLength(100);
            Property(x => x.ClientCoreUsername).HasColumnName("ClientCoreUsername").IsOptional().HasMaxLength(100);
            Property(x => x.ClientCorePassword).HasColumnName("ClientCorePassword").IsOptional().HasMaxLength(100);
            Property(x => x.ClientCoreLocalId).HasColumnName("ClientCoreLocalId").IsOptional().HasMaxLength(100);
            Property(x => x.ClientCoreCorpId).HasColumnName("ClientCoreCorpId").IsOptional().HasMaxLength(100);
            Property(x => x.ClientCoreCorpCode).HasColumnName("ClientCoreCorpCode").IsOptional().HasMaxLength(100);
            Property(x => x.ClientCoreOwner).HasColumnName("ClientCoreOwner").IsOptional().HasMaxLength(100);
            Property(x => x.ClientCoreClientType).HasColumnName("ClientCoreClientType").IsOptional().HasMaxLength(100);
            Property(x => x.UnderwriterRoleId).HasColumnName("UnderwriterRoleId").IsOptional();
            Property(x => x.AnalystRoleId).HasColumnName("AnalystRoleId").IsOptional();
            Property(x => x.TechnicianRoleId).HasColumnName("TechnicianRoleId").IsOptional();
            Property(x => x.LogoUrl).HasColumnName("LogoUrl").IsOptional().HasMaxLength(500);
            Property(x => x.ThemeStyleSheet).HasColumnName("ThemeStyleSheet").IsOptional().HasMaxLength(500);
            Property(x => x.JavascriptFile).HasColumnName("JavascriptFile").IsOptional().HasMaxLength(500);
            Property(x => x.SearchFieldOrder).HasColumnName("SearchFieldOrder").IsOptional().HasMaxLength(500);
            Property(x => x.SubmissionScreenFieldOrder).HasColumnName("SubmissionScreenFieldOrder").IsOptional().HasMaxLength(500);
            Property(x => x.SubmissionScreenCodeTypeOrder).HasColumnName("SubmissionScreenCodeTypeOrder").IsOptional().HasMaxLength(500);
            Property(x => x.SubmissionScreenAttributeOrder).HasColumnName("SubmissionScreenAttributeOrder").IsOptional().HasMaxLength(500);
            Property(x => x.ClientScreenFieldOrder).HasColumnName("ClientScreenFieldOrder").IsOptional().HasMaxLength(500);
            Property(x => x.ClientScreenCodeTypeOrder).HasColumnName("ClientScreenCodeTypeOrder").IsOptional().HasMaxLength(500);
            Property(x => x.ClientScreenAttributeOrder).HasColumnName("ClientScreenAttributeOrder").IsOptional().HasMaxLength(500);
            Property(x => x.SupportsIndividualClients).HasColumnName("SupportsIndividualClients").IsRequired();
            Property(x => x.SupportsInternationalAddresses).HasColumnName("SupportsInternationalAddresses").IsRequired();
            Property(x => x.SupportsClassCodes).HasColumnName("SupportsClassCodes").IsRequired();
            Property(x => x.SupportsDuplicates).HasColumnName("SupportsDuplicates").IsRequired();
            Property(x => x.EnforceAgencyLicensedState).HasColumnName("EnforceAgencyLicensedState").IsRequired();
            Property(x => x.LobGridUpdateStatuses).HasColumnName("LOBGridUpdateStatuses").IsOptional().HasMaxLength(255);
            Property(x => x.LobGridUpdateExcludedStatuses).HasColumnName("LOBGridUpdateExcludedStatuses").IsOptional().HasMaxLength(255);
            Property(x => x.SupportsMultipleLineOfBusinesses).HasColumnName("SupportsMultipleLineOfBusinesses").IsRequired();
            Property(x => x.SupportsAgentUnspecifiedReasons).HasColumnName("SupportsAgentUnspecifiedReasons").IsRequired();
            Property(x => x.RequiresAddressType).HasColumnName("RequiresAddressType").IsRequired();
            Property(x => x.RequiresNameType).HasColumnName("RequiresNameType").IsRequired();
            Property(x => x.DesiresInsuredAutoSelectWhenOnlyOne).HasColumnName("DesiresInsuredAutoSelectWhenOnlyOne").IsRequired();
            Property(x => x.DesiresDbaAutoSelectWhenOnlyOne).HasColumnName("DesiresDBAAutoSelectWhenOnlyOne").IsRequired();
            Property(x => x.SearchOnPolicyNumberOrSubmissonNumber).HasColumnName("SearchOnPolicyNumberOrSubmissonNumber").IsRequired();
            Property(x => x.FillAllUnderwriters).HasColumnName("FillAllUnderwriters").IsRequired();
            Property(x => x.DesiresDefaultStatusOnSubmissionCopy).HasColumnName("DesiresDefaultStatusOnSubmissionCopy").IsRequired();
            Property(x => x.RequiresPolicyNumber).HasColumnName("RequiresPolicyNumber").IsRequired();
            Property(x => x.GenerateOnlyPolicyNumber).HasColumnName("GenerateOnlyPolicyNumber").IsRequired();
            Property(x => x.FreehandOnlyPolicyNumber).HasColumnName("FreehandOnlyPolicyNumber").IsRequired();
            Property(x => x.SupportsShellSubmission).HasColumnName("SupportsShellSubmission").IsRequired();
            Property(x => x.UsesAps).HasColumnName("UsesAPS").IsRequired();
            Property(x => x.Apsurl).HasColumnName("APSURL").IsOptional().HasMaxLength(255);
            Property(x => x.Apsdsik).HasColumnName("APSDSIK").IsOptional().HasMaxLength(50);
            Property(x => x.AgenciesInGrid).HasColumnName("AgenciesInGrid").IsRequired();
            Property(x => x.AutoSelectNamesOnSubmissionAdd).HasColumnName("AutoSelectNamesOnSubmissionAdd").IsRequired();
            Property(x => x.PolicyNumberRegex).HasColumnName("PolicyNumberRegex").IsRequired().HasMaxLength(50);
            Property(x => x.LoadClientBeforeStep).HasColumnName("LoadClientBeforeStep").IsRequired();
            Property(x => x.AgencyPortMatchLength).HasColumnName("AgencyPortMatchLength").IsOptional();
            Property(x => x.AllowClientPrimaryChanges).HasColumnName("AllowClientPrimaryChanges").IsRequired();
            Property(x => x.Active).HasColumnName("Active").IsRequired();
            
            // Foreign keys
            HasOptional(a => a.UnderwritingRole).WithMany(b => b.Company2).HasForeignKey(c => c.UnderwriterRoleId);
            HasOptional(a => a.AnalystRole).WithMany(b => b.Company).HasForeignKey(c => c.AnalystRoleId);
            HasOptional(a => a.TechnicianRole).WithMany(b => b.Company1).HasForeignKey(c => c.TechnicianRoleId);
            HasMany(t => t.User).WithMany(t => t.Company).Map(m =>
            {
                m.ToTable("UserCompany");
                m.MapLeftKey("CompanyId");
                m.MapRightKey("UserId");
            });
        }
    }
}
