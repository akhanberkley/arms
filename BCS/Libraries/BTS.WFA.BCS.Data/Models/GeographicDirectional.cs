using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // GeographicDirectional
    public class GeographicDirectional
    {
        public int Id { get; set; } // Id (Primary key)
        public string GeographicDirectional_ { get; set; } // GeographicDirectional
        public string Abbreviation { get; set; } // Abbreviation
    }

    internal class GeographicDirectionalConfiguration : EntityTypeConfiguration<GeographicDirectional>
    {
        public GeographicDirectionalConfiguration()
        {
            ToTable("dbo.GeographicDirectional");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.GeographicDirectional_).HasColumnName("GeographicDirectional").IsRequired().HasMaxLength(50);
            Property(x => x.Abbreviation).HasColumnName("Abbreviation").IsRequired().HasMaxLength(10);
        }
    }
}
