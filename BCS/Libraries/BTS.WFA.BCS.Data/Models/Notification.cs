
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Notification
    public class Notification
    {
        public int Id { get; set; } // Id (Primary key)
        public string Description { get; set; } // Description
        public DateTime StartDate { get; set; } // StartDate
        public DateTime EndDate { get; set; } // EndDate
        public string Link { get; set; } // Link
        public int? Type { get; set; } // Type
        public int? Severity { get; set; } // Severity
    }

    internal class NotificationConfiguration : EntityTypeConfiguration<Notification>
    {
        public NotificationConfiguration()
        {
            ToTable("dbo.Notification");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Description).HasColumnName("Description").IsRequired().HasMaxLength(1500);
            Property(x => x.StartDate).HasColumnName("StartDate").IsRequired();
            Property(x => x.EndDate).HasColumnName("EndDate").IsRequired();
            Property(x => x.Link).HasColumnName("Link").IsOptional().HasMaxLength(200);
            Property(x => x.Type).HasColumnName("Type").IsOptional();
            Property(x => x.Severity).HasColumnName("Severity").IsOptional();
        }
    }
}
