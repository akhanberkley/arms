using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    public class SubmissionViewField
    {
        public Guid SubmissionViewTypeId { get; set; }
        public SubmissionViewType SubmissionViewType { get; set; }
        public int CompanyNumberId { get; set; }
        public CompanyNumber CompanyNumber { get; set; }

        public Guid SubmissionFieldId { get; set; }
        public SubmissionField SubmissionField { get; set; }
        
        public short Sequence { get; set; }
        public string ColumnHeading { get; set; }
    }

    internal class SubmissionViewFieldConfiguration : EntityTypeConfiguration<SubmissionViewField>
    {
        public SubmissionViewFieldConfiguration()
        {
            ToTable("dbo.SubmissionViewField");
            HasKey(x => new { x.CompanyNumberId, x.SubmissionViewTypeId, x.SubmissionFieldId });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            HasRequired(a => a.CompanyNumber).WithMany().HasForeignKey(c => c.CompanyNumberId);
            Property(x => x.SubmissionViewTypeId).HasColumnName("SubmissionViewTypeId").IsRequired();
            HasRequired(a => a.SubmissionViewType).WithMany().HasForeignKey(c => c.SubmissionViewTypeId);
            Property(x => x.SubmissionFieldId).HasColumnName("SubmissionFieldId").IsRequired();
            HasRequired(a => a.SubmissionField).WithMany().HasForeignKey(c => c.SubmissionFieldId);

            Property(x => x.ColumnHeading).HasColumnName("ColumnHeading").IsOptional().HasMaxLength(50);
            Property(x => x.Sequence).HasColumnName("Sequence").IsRequired();
        }
    }
}
