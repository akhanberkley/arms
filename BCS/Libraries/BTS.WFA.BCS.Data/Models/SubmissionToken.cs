using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionToken
    public class SubmissionToken
    {
        public int Id { get; set; } // Id (Primary key)
        public Guid TokenNumber { get; set; } // TokenNumber
        public int UserId { get; set; } // UserId
        public bool Expired { get; set; } // Expired
        public DateTime CreateDt { get; set; } // CreateDt

        public SubmissionToken()
        {
            TokenNumber = System.Guid.NewGuid();
            Expired = false;
        }
    }

    internal class SubmissionTokenConfiguration : EntityTypeConfiguration<SubmissionToken>
    {
        public SubmissionTokenConfiguration()
        {
            ToTable("dbo.SubmissionToken");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.TokenNumber).HasColumnName("TokenNumber").IsRequired();
            Property(x => x.UserId).HasColumnName("UserId").IsRequired();
            Property(x => x.Expired).HasColumnName("Expired").IsRequired();
            Property(x => x.CreateDt).HasColumnName("CreateDt").IsRequired();
        }
    }
}
