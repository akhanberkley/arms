using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberLOBGrid
    public class CompanyNumberLobGrid
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public string MulitLobCode { get; set; } // MulitLOBCode
        public string MultiLobDesc { get; set; } // MultiLOBDesc
        public bool Active { get; set; } // Active
        public int Order { get; set; } // Order

        // Reverse navigation
        public virtual ICollection<SubmissionLineOfBusinessChildren> SubmissionLineOfBusinessChildren { get; set; } // SubmissionLineOfBusinessChildren.FK_SubmissionLineOfBusinessChildren_CompanyNumberLOBGrid

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_CompanyNumberLOBGrid_CompanyNumber

        public CompanyNumberLobGrid()
        {
            Active = true;
            Order = 0;
            SubmissionLineOfBusinessChildren = new List<SubmissionLineOfBusinessChildren>();
        }
    }

    internal class CompanyNumberLobGridConfiguration : EntityTypeConfiguration<CompanyNumberLobGrid>
    {
        public CompanyNumberLobGridConfiguration()
        {
            ToTable("dbo.CompanyNumberLOBGrid");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.MulitLobCode).HasColumnName("MulitLOBCode").IsOptional().HasMaxLength(50);
            Property(x => x.MultiLobDesc).HasColumnName("MultiLOBDesc").IsRequired().HasMaxLength(50);
            Property(x => x.Active).HasColumnName("Active").IsRequired();
            Property(x => x.Order).HasColumnName("Order").IsRequired();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.CompanyNumberLobGrid).HasForeignKey(c => c.CompanyNumberId); // FK_CompanyNumberLOBGrid_CompanyNumber
        }
    }
}
