using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionStatusHistory
    public class SubmissionStatusHistory
    {
        public int Id { get; set; } // Id (Primary key)
        public int SubmissionId { get; set; } // SubmissionId
        public int? PreviousSubmissionStatusId { get; set; } // PreviousSubmissionStatusId
        public DateTime? SubmissionStatusDate { get; set; } // SubmissionStatusDate
        public DateTime StatusChangeDt { get; set; } // StatusChangeDt
        public string StatusChangeBy { get; set; } // StatusChangeBy
        public bool Active { get; set; } // Active
        public int? PreviousSubmissionStatusReasonId { get; set; }
        public string PreviousSubmissionStatusReasonOther { get; set; }

        // Foreign keys
        public virtual Submission Submission { get; set; }
        public virtual SubmissionStatus PreviousSubmissionStatus { get; set; }
        public virtual SubmissionStatusReason PreviousSubmissionStatusReason { get; set; }

        public SubmissionStatusHistory()
        {
            StatusChangeDt = System.DateTime.Now;
            Active = true;
        }
    }

    internal class SubmissionStatusHistoryConfiguration : EntityTypeConfiguration<SubmissionStatusHistory>
    {
        public SubmissionStatusHistoryConfiguration()
        {
            ToTable("dbo.SubmissionStatusHistory");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired();
            Property(x => x.PreviousSubmissionStatusId).HasColumnName("PreviousSubmissionStatusId").IsOptional();
            Property(x => x.SubmissionStatusDate).HasColumnName("SubmissionStatusDate").IsOptional();
            Property(x => x.StatusChangeDt).HasColumnName("StatusChangeDt").IsRequired();
            Property(x => x.StatusChangeBy).HasColumnName("StatusChangeBy").IsRequired().HasMaxLength(50);
            Property(x => x.Active).HasColumnName("Active").IsRequired();
            Property(x => x.PreviousSubmissionStatusReasonId).HasColumnName("PreviousSubmissionStatusReasonId").IsOptional();
            Property(x => x.PreviousSubmissionStatusReasonOther).HasColumnName("PreviousSubmissionStatusReasonOther").IsOptional().HasMaxLength(255);

            // Foreign keys
            HasRequired(a => a.Submission).WithMany(b => b.SubmissionStatusHistory).HasForeignKey(c => c.SubmissionId);
            HasOptional(a => a.PreviousSubmissionStatus).WithMany(b => b.SubmissionStatusHistory).HasForeignKey(c => c.PreviousSubmissionStatusId);
            HasOptional(a => a.PreviousSubmissionStatusReason).WithMany().HasForeignKey(c => c.PreviousSubmissionStatusReasonId);
        }
    }
}
