using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberAttribute
    public class CompanyNumberClientCoreSegment
    {
        public int CompanyNumberId { get; set; }
        public string Segment { get; set; }
        public string Description { get; set; }
        public bool IsDefault { get; set; }

        public CompanyNumber CompanyNumber { get; set; }

        public CompanyNumberClientCoreSegment()
        {
        }
    }

    internal class CompanyNumberClientCoreSegmentConfiguration : EntityTypeConfiguration<CompanyNumberClientCoreSegment>
    {
        public CompanyNumberClientCoreSegmentConfiguration()
        {
            ToTable("dbo.CompanyNumberClientCoreSegment");
            HasKey(x => new { x.CompanyNumberId, x.Segment });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.Segment).HasColumnName("Segment").IsRequired().HasMaxLength(50);
            Property(x => x.Description).HasColumnName("Description").IsRequired().HasMaxLength(50);
            Property(x => x.IsDefault).HasColumnName("IsDefault").IsRequired();

            HasRequired(a => a.CompanyNumber).WithMany(b => b.CompanyNumberClientCoreSegment).HasForeignKey(c => c.CompanyNumberId);
        }
    }
}
