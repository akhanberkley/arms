using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Submission
    public class Submission
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public int SubmissionTypeId { get; set; } // SubmissionTypeId
        public int? SubmissionTokenId { get; set; } // SubmissionTokenId
        public long ClientCoreClientId { get; set; } // ClientCoreClientId
        public long? ClientCorePortfolioId { get; set; } // ClientCorePortfolioId
        public int? AgencyId { get; set; } // AgencyId
        public int? AgentId { get; set; } // AgentId
        public int? ContactId { get; set; } // ContactId
        public int? SubmissionStatusId { get; set; } // SubmissionStatusId
        public DateTime? SubmissionStatusDate { get; set; } // SubmissionStatusDate
        public DateTime? CancellationDate { get; set; } // CancellationDate
        public int? SubmissionStatusReasonId { get; set; } // SubmissionStatusReasonId
        public string SubmissionStatusReasonOther { get; set; } // SubmissionStatusReasonOther
        public int? UnderwriterPersonId { get; set; } // UnderwriterPersonId
        public int? AnalystPersonId { get; set; } // AnalystPersonId
        public int? TechnicianPersonId { get; set; } // TechnicianPersonId
        public int? PolicySystemId { get; set; } // PolicySystemId
        public long SubmissionNumber { get; set; } // SubmissionNumber
        public int Sequence { get; set; } // Sequence
        public string PolicyNumber { get; set; } // PolicyNumber
        public short? PolicyMod { get; set; }
        public long? PolicyNumberDigitsOnly { get; set; } // PolicyNumberDigitsOnly
        public decimal? EstimatedWrittenPremium { get; set; } // EstimatedWrittenPremium
        public DateTime? EffectiveDate { get; set; } // EffectiveDate
        public DateTime? ExpirationDate { get; set; } // ExpirationDate
        public string InsuredName { get; set; } // InsuredName
        public string Dba { get; set; } // Dba
        public string Comments { get; set; } // Comments
        public int? OtherCarrierId { get; set; } // OtherCarrierId
        public decimal? OtherCarrierPremium { get; set; } // OtherCarrierPremium
        public string SubmissionBy { get; set; } // SubmissionBy
        public DateTime SubmissionDt { get; set; } // SubmissionDt
        public string UpdatedBy { get; set; } // UpdatedBy
        public DateTime? UpdatedDt { get; set; } // UpdatedDt
        public DateTime SystemDt { get; set; } // SystemDt
        public string SystemId { get; set; } // SystemId
        public bool Deleted { get; set; } // Deleted
        public DateTime? DeletedDt { get; set; } // DeletedDt
        public string DeletedBy { get; set; } // DeletedBy
        public bool CreatedWithOFACHit { get; set; } // CreatedWithOFACHit
        public int? DuplicateOfSubmissionId { get; set; }
        public Guid? PolicySymbolId { get; set; }
        public int? PriorCarrierId { get; set; }
        public decimal? PriorCarrierPremium { get; set; }
        public int? AssociationSubmissionId { get; set; }

        public virtual CompanyNumber CompanyNumber { get; set; }
        public virtual Agency Agency { get; set; }
        public virtual SubmissionStatus SubmissionStatus { get; set; }
        public virtual SubmissionStatusReason SubmissionStatusReason { get; set; }
        public virtual SubmissionType SubmissionType { get; set; }
        public virtual Person UnderwriterPerson { get; set; }
        public virtual Person AnalystPerson { get; set; }
        public virtual Person TechnicianPerson { get; set; }
        public virtual Agent Agent { get; set; }
        public virtual OtherCarrier OtherCarrier { get; set; }
        public virtual OtherCarrier PriorCarrier { get; set; }
        public virtual PolicySymbol PolicySymbol { get; set; }
        public virtual PolicySystem PolicySystem { get; set; }
        public virtual Submission DuplicateOfSubmission { get; set; }
        public virtual Submission AssociationSubmission { get; set; }

        // Reverse navigation
        public virtual ICollection<CompanyNumberAgentUnspecifiedReason> CompanyNumberAgentUnspecifiedReason { get; set; } // Many to many mapping
        public virtual ICollection<CompanyNumberCode> CompanyNumberCode { get; set; } // Many to many mapping
        public virtual ICollection<LineOfBusiness> LineOfBusiness { get; set; } // Many to many mapping
        public virtual ICollection<SubmissionClientCoreClientAddress> SubmissionClientCoreClientAddress { get; set; } // Many to many mapping
        public virtual ICollection<SubmissionClientCoreClientName> SubmissionClientCoreClientName { get; set; } // Many to many mapping
        public virtual ICollection<SubmissionComment> SubmissionComment { get; set; } // SubmissionComment.FK_SubmissionComment_Submission
        public virtual ICollection<SubmissionCompanyNumberAttributeValue> SubmissionCompanyNumberAttributeValue { get; set; } // Many to many mapping
        public virtual ICollection<SubmissionCompanyNumberAttributeValueHistory> SubmissionCompanyNumberAttributeValueHistory { get; set; } // SubmissionCompanyNumberAttributeValueHistory.FK_SubmissionCompanyNumberAttributeValueHistory_Submission
        public virtual ICollection<SubmissionCompanyNumberCodeHistory> SubmissionCompanyNumberCodeHistory { get; set; } // SubmissionCompanyNumberCodeHistory.FK_SubmissionCompanyNumberCodeHistory_Submission
        public virtual ICollection<SubmissionHistory> SubmissionHistory { get; set; } // SubmissionHistory.FK_SubmissionHistory_Submission
        public virtual ICollection<SubmissionLineOfBusinessChildren> SubmissionLineOfBusinessChildren { get; set; } // SubmissionLineOfBusinessChildren.FK_SubmissionLineOfBusinessChildren_Submission
        public virtual ICollection<SubmissionStatusHistory> SubmissionStatusHistory { get; set; } // SubmissionStatusHistory.FK_SubmissionStatusHistory_Submission
        public virtual ICollection<ClassCode> ClassCodes { get; set; }
        public virtual ICollection<SubmissionAddress> Addresses { get; set; }
        public virtual ICollection<SubmissionAdditionalName> AdditionalNames { get; set; }
        public virtual ICollection<PolicySymbol> PackagePolicySymbols { get; set; }

        public Submission()
        {
            Sequence = 0;
            SubmissionDt = System.DateTime.Now;
            SystemDt = System.DateTime.Now;
            Deleted = false;
            CompanyNumberAgentUnspecifiedReason = new List<CompanyNumberAgentUnspecifiedReason>();
            CompanyNumberCode = new List<CompanyNumberCode>();
            LineOfBusiness = new List<LineOfBusiness>();
            SubmissionClientCoreClientAddress = new List<SubmissionClientCoreClientAddress>();
            SubmissionClientCoreClientName = new List<SubmissionClientCoreClientName>();
            SubmissionComment = new List<SubmissionComment>();
            SubmissionCompanyNumberAttributeValue = new List<SubmissionCompanyNumberAttributeValue>();
            SubmissionCompanyNumberAttributeValueHistory = new List<SubmissionCompanyNumberAttributeValueHistory>();
            SubmissionCompanyNumberCodeHistory = new List<SubmissionCompanyNumberCodeHistory>();
            SubmissionHistory = new List<SubmissionHistory>();
            SubmissionLineOfBusinessChildren = new List<SubmissionLineOfBusinessChildren>();
            SubmissionStatusHistory = new List<SubmissionStatusHistory>();
            ClassCodes = new List<ClassCode>();
            Addresses = new List<SubmissionAddress>();
            AdditionalNames = new List<SubmissionAdditionalName>();
            PackagePolicySymbols = new List<PolicySymbol>();
            CreatedWithOFACHit = false;
        }
    }

    internal class SubmissionConfiguration : EntityTypeConfiguration<Submission>
    {
        public SubmissionConfiguration()
        {
            ToTable("dbo.Submission");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.SubmissionTypeId).HasColumnName("SubmissionTypeId").IsRequired();
            Property(x => x.SubmissionTokenId).HasColumnName("SubmissionTokenId").IsOptional();
            Property(x => x.ClientCoreClientId).HasColumnName("ClientCoreClientId").IsRequired();
            Property(x => x.ClientCorePortfolioId).HasColumnName("ClientCorePortfolioId").IsOptional();
            Property(x => x.AgencyId).HasColumnName("AgencyId").IsOptional();
            Property(x => x.AgentId).HasColumnName("AgentId").IsOptional();
            Property(x => x.ContactId).HasColumnName("ContactId").IsOptional();
            Property(x => x.SubmissionStatusId).HasColumnName("SubmissionStatusId").IsOptional();
            Property(x => x.SubmissionStatusDate).HasColumnName("SubmissionStatusDate").IsOptional();
            Property(x => x.CancellationDate).HasColumnName("CancellationDate").IsOptional();
            Property(x => x.SubmissionStatusReasonId).HasColumnName("SubmissionStatusReasonId").IsOptional();
            Property(x => x.SubmissionStatusReasonOther).HasColumnName("SubmissionStatusReasonOther").IsOptional().HasMaxLength(255);
            Property(x => x.UnderwriterPersonId).HasColumnName("UnderwriterPersonId").IsOptional();
            Property(x => x.AnalystPersonId).HasColumnName("AnalystPersonId").IsOptional();
            Property(x => x.TechnicianPersonId).HasColumnName("TechnicianPersonId").IsOptional();
            Property(x => x.PolicySystemId).HasColumnName("PolicySystemId").IsOptional();
            Property(x => x.SubmissionNumber).HasColumnName("SubmissionNumber").IsRequired();
            Property(x => x.Sequence).HasColumnName("Sequence").IsRequired();
            Property(x => x.PolicyNumber).HasColumnName("PolicyNumber").IsOptional().HasMaxLength(50);
            Property(x => x.PolicyMod).HasColumnName("PolicyMod").IsOptional();
            Property(x => x.PolicyNumberDigitsOnly).HasColumnName("PolicyNumberDigitsOnly").IsOptional();
            Property(x => x.EstimatedWrittenPremium).HasColumnName("EstimatedWrittenPremium").IsOptional().HasPrecision(19, 4);
            Property(x => x.EffectiveDate).HasColumnName("EffectiveDate").IsOptional();
            Property(x => x.ExpirationDate).HasColumnName("ExpirationDate").IsOptional();
            Property(x => x.InsuredName).HasColumnName("InsuredName").IsOptional().HasMaxLength(255);
            Property(x => x.Dba).HasColumnName("Dba").IsOptional().HasMaxLength(255);
            Property(x => x.Comments).HasColumnName("Comments").IsOptional().HasMaxLength(8000);
            Property(x => x.OtherCarrierId).HasColumnName("OtherCarrierId").IsOptional();
            Property(x => x.OtherCarrierPremium).HasColumnName("OtherCarrierPremium").IsOptional().HasPrecision(19, 4);
            Property(x => x.SubmissionBy).HasColumnName("SubmissionBy").IsRequired().HasMaxLength(50);
            Property(x => x.SubmissionDt).HasColumnName("SubmissionDt").IsRequired();
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.UpdatedDt).HasColumnName("UpdatedDt").IsOptional();
            Property(x => x.SystemDt).HasColumnName("SystemDt").IsRequired();
            Property(x => x.SystemId).HasColumnName("SystemId").IsOptional().HasMaxLength(50);
            Property(x => x.Deleted).HasColumnName("Deleted").IsRequired();
            Property(x => x.DeletedDt).HasColumnName("DeletedDt").IsOptional();
            Property(x => x.DeletedBy).HasColumnName("DeletedBy").IsOptional().HasMaxLength(50);
            Property(x => x.CreatedWithOFACHit).HasColumnName("CreatedWithOFACHit").IsRequired();
            Property(x => x.DuplicateOfSubmissionId).HasColumnName("DuplicateOfSubmissionId").IsOptional();
            Property(x => x.PolicySymbolId).HasColumnName("PolicySymbolId").IsOptional();
            Property(x => x.PriorCarrierId).HasColumnName("PriorCarrierId").IsOptional();
            Property(x => x.PriorCarrierPremium).HasColumnName("PriorCarrierPremium").IsOptional().HasPrecision(19, 4);
            Property(x => x.AssociationSubmissionId).HasColumnName("AssociationSubmissionId").IsOptional();

            HasRequired(a => a.CompanyNumber).WithMany().HasForeignKey(c => c.CompanyNumberId);
            HasRequired(a => a.Agency).WithMany().HasForeignKey(c => c.AgencyId);
            HasRequired(a => a.SubmissionStatus).WithMany().HasForeignKey(c => c.SubmissionStatusId);
            HasOptional(a => a.SubmissionStatusReason).WithMany().HasForeignKey(c => c.SubmissionStatusReasonId);
            HasRequired(a => a.SubmissionType).WithMany().HasForeignKey(c => c.SubmissionTypeId);
            HasOptional(a => a.UnderwriterPerson).WithMany().HasForeignKey(c => c.UnderwriterPersonId);
            HasOptional(a => a.AnalystPerson).WithMany().HasForeignKey(c => c.AnalystPersonId);
            HasOptional(a => a.TechnicianPerson).WithMany().HasForeignKey(c => c.TechnicianPersonId);
            HasOptional(a => a.Agent).WithMany().HasForeignKey(c => c.AgentId);
            HasOptional(a => a.OtherCarrier).WithMany().HasForeignKey(c => c.OtherCarrierId);
            HasOptional(a => a.PriorCarrier).WithMany().HasForeignKey(c => c.PriorCarrierId);
            HasOptional(a => a.PolicySymbol).WithMany().HasForeignKey(c => c.PolicySymbolId);
            HasOptional(a => a.PolicySystem).WithMany().HasForeignKey(c => c.PolicySystemId);
            HasOptional(a => a.DuplicateOfSubmission).WithMany().HasForeignKey(c => c.DuplicateOfSubmissionId);
            HasOptional(a => a.AssociationSubmission).WithMany().HasForeignKey(c => c.AssociationSubmissionId);
            HasMany(t => t.ClassCodes).WithMany().Map(m =>
            {
                m.ToTable("SubmissionClassCode");
                m.MapLeftKey("SubmissionId");
                m.MapRightKey("ClassCodeId");
            });
            HasMany(t => t.LineOfBusiness).WithMany().Map(m =>
            {
                m.ToTable("SubmissionLineOfBusiness");
                m.MapLeftKey("SubmissionId");
                m.MapRightKey("LineOfBusinessId");
            });
            HasMany(t => t.PackagePolicySymbols).WithMany().Map(m =>
            {
                m.ToTable("SubmissionPackagePolicySymbol");
                m.MapLeftKey("SubmissionId");
                m.MapRightKey("PolicySymbolId");
            });
        }
    }
}
