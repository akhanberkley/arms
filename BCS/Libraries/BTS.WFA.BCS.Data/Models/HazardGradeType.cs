using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // HazardGradeType
    public class HazardGradeType
    {
        public int Id { get; set; } // Id (Primary key)
        public string TypeName { get; set; } // TypeName

        // Reverse navigation
        public virtual ICollection<ClientClassCodeHazardGradeValue> ClientClassCodeHazardGradeValue { get; set; } // ClientClassCodeHazardGradeValue.FK_ClientClassCodeHazardGradeValue_HazardGradeType
        public virtual ICollection<CompanyNumberClassCodeHazardGradeValue> CompanyNumberClassCodeHazardGradeValue { get; set; } // Many to many mapping

        public HazardGradeType()
        {
            ClientClassCodeHazardGradeValue = new List<ClientClassCodeHazardGradeValue>();
            CompanyNumberClassCodeHazardGradeValue = new List<CompanyNumberClassCodeHazardGradeValue>();
        }
    }

    internal class HazardGradeTypeConfiguration : EntityTypeConfiguration<HazardGradeType>
    {
        public HazardGradeTypeConfiguration()
        {
            ToTable("dbo.HazardGradeType");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.TypeName).HasColumnName("TypeName").IsRequired().HasMaxLength(50);
        }
    }
}
