using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // StreetSuffix
    public class StreetSuffix
    {
        public int Id { get; set; } // Id (Primary key)
        public string StreetSuffix_ { get; set; } // StreetSuffix
        public string CommonAbbreviation { get; set; } // CommonAbbreviation
        public string ApprovedAbbreviation { get; set; } // ApprovedAbbreviation
    }

    internal class StreetSuffixConfiguration : EntityTypeConfiguration<StreetSuffix>
    {
        public StreetSuffixConfiguration()
        {
            ToTable("dbo.StreetSuffix");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.StreetSuffix_).HasColumnName("StreetSuffix").IsRequired().HasMaxLength(50);
            Property(x => x.CommonAbbreviation).HasColumnName("CommonAbbreviation").IsRequired().HasMaxLength(50);
            Property(x => x.ApprovedAbbreviation).HasColumnName("ApprovedAbbreviation").IsRequired().HasMaxLength(50);
        }
    }
}
