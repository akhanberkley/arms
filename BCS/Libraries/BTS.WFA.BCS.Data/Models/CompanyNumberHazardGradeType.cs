using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    public class CompanyNumberHazardGradeType
    {
        public int CompanyNumberId { get; set; }
        public int HazardGradeTypeId { get; set; }
        public short DisplayOrder { get; set; }

        public virtual CompanyNumber CompanyNumber { get; set; }
        public virtual HazardGradeType HazardGradeType { get; set; }
    }

    internal class CompanyNumberHazardGradeTypeConfiguration : EntityTypeConfiguration<CompanyNumberHazardGradeType>
    {
        public CompanyNumberHazardGradeTypeConfiguration()
        {
            ToTable("dbo.CompanyNumberHazardGradeType");
            HasKey(x => new { x.CompanyNumberId, x.HazardGradeTypeId });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.HazardGradeTypeId).HasColumnName("HazardGradeTypeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.CompanyNumber).WithMany(b => b.CompanyNumberHazardGradeType).HasForeignKey(c => c.CompanyNumberId);
            HasRequired(a => a.HazardGradeType).WithMany().HasForeignKey(c => c.HazardGradeTypeId);
        }
    }
}
