using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // PolicySystem
    public class PolicySystem
    {
        public int Id { get; set; } // Id (Primary key)
        public int AssociatedCompanyNumberId { get; set; } // AssociatedCompanyNumberId
        public string Description { get; set; } // PolicySystem
        public string Abbreviation { get; set; } // Abbreviation

        // Reverse navigation
        public virtual ICollection<CompanyNumber> CompanyNumber { get; set; } // Many to many mapping

        public PolicySystem()
        {
            CompanyNumber = new List<CompanyNumber>();
        }
    }

    internal class PolicySystemConfiguration : EntityTypeConfiguration<PolicySystem>
    {
        public PolicySystemConfiguration()
        {
            ToTable("dbo.PolicySystem");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.AssociatedCompanyNumberId).HasColumnName("AssociatedCompanyNumberId").IsRequired();
            Property(x => x.Description).HasColumnName("PolicySystem").IsRequired().HasMaxLength(200);
            Property(x => x.Abbreviation).HasColumnName("Abbreviation").IsOptional().HasMaxLength(50);
        }
    }
}
