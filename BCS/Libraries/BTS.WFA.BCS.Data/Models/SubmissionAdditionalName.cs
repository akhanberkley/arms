using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    public class SubmissionAdditionalName
    {
        public Guid Id { get; set; }
        public int SubmissionId { get; set; }
        public int? ClientCoreSequenceNumber { get; set; }
        public string BusinessName { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDt { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDt { get; set; }

        public virtual Submission Submission { get; set; }
    }

    internal class SubmissionAdditionalNameConfiguration : EntityTypeConfiguration<SubmissionAdditionalName>
    {
        public SubmissionAdditionalNameConfiguration()
        {
            ToTable("dbo.SubmissionAdditionalName");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired();
            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired();
            Property(x => x.ClientCoreSequenceNumber).HasColumnName("ClientCoreSequenceNumber").IsOptional();
            Property(x => x.BusinessName).HasColumnName("BusinessName").IsOptional().HasMaxLength(500);
            Property(x => x.EntryBy).HasColumnName("EntryBy").IsRequired().HasMaxLength(50);
            Property(x => x.EntryDt).HasColumnName("EntryDt").IsRequired();
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasMaxLength(50);
            Property(x => x.ModifiedDt).HasColumnName("ModifiedDt").IsOptional();

            HasRequired(a => a.Submission).WithMany(b => b.AdditionalNames).HasForeignKey(c => c.SubmissionId);
        }
    }
}
