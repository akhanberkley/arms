using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberAttribute
    public class CompanyNumberAttribute
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public int AttributeDataTypeId { get; set; } // AttributeDataTypeId
        public string Label { get; set; } // Label
        public bool ClientSpecific { get; set; } // ClientSpecific
        public bool Required { get; set; } // Required
        public bool ReadOnlyProperty { get; set; } // ReadOnlyProperty
        public short DisplayOrder { get; set; } // DisplayOrder
        public string UiAttributes { get; set; } // UIAttributes
        public bool? ViewWithSubmissionAddress { get; set; } // ViewWithSubmissionAddress
        public bool OtherCarrierSpecific { get; set; } // OtherCarrierSpecific
        public bool VisibleOnAdd { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public bool ExcludeValueOnCopy { get; set; }
        public bool VisibleOnBPM { get; set; }

        // Reverse navigation
        public virtual ICollection<ClientCompanyNumberAttributeValue> ClientCompanyNumberAttributeValue { get; set; } // Many to many mapping
        public virtual ICollection<SubmissionCompanyNumberAttributeValue> SubmissionCompanyNumberAttributeValue { get; set; } // Many to many mapping
        public virtual ICollection<SubmissionCompanyNumberAttributeValueHistory> SubmissionCompanyNumberAttributeValueHistory { get; set; } // SubmissionCompanyNumberAttributeValueHistory.FK_SubmissionCompanyNumberAttributeValueHistory_CompanyNumberAttribute

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_CompanyNumberAttribute_CompanyNumber
        public virtual AttributeDataType AttributeDataType { get; set; } //  FK_CompanyNumberAttribute_AttributeDataType

        public CompanyNumberAttribute()
        {
            ClientSpecific = false;
            Required = false;
            ReadOnlyProperty = false;
            DisplayOrder = 0;
            ViewWithSubmissionAddress = false;
            OtherCarrierSpecific = false;
            ClientCompanyNumberAttributeValue = new List<ClientCompanyNumberAttributeValue>();
            SubmissionCompanyNumberAttributeValue = new List<SubmissionCompanyNumberAttributeValue>();
            SubmissionCompanyNumberAttributeValueHistory = new List<SubmissionCompanyNumberAttributeValueHistory>();
        }
    }

    internal class CompanyNumberAttributeConfiguration : EntityTypeConfiguration<CompanyNumberAttribute>
    {
        public CompanyNumberAttributeConfiguration()
        {
            ToTable("dbo.CompanyNumberAttribute");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.AttributeDataTypeId).HasColumnName("AttributeDataTypeId").IsRequired();
            Property(x => x.Label).HasColumnName("Label").IsRequired().HasMaxLength(100);
            Property(x => x.ClientSpecific).HasColumnName("ClientSpecific").IsRequired();
            Property(x => x.Required).HasColumnName("Required").IsRequired();
            Property(x => x.ReadOnlyProperty).HasColumnName("ReadOnlyProperty").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.UiAttributes).HasColumnName("UIAttributes").IsOptional().HasMaxLength(510);
            Property(x => x.ViewWithSubmissionAddress).HasColumnName("ViewWithSubmissionAddress").IsOptional();
            Property(x => x.OtherCarrierSpecific).HasColumnName("OtherCarrierSpecific").IsRequired();
            Property(x => x.VisibleOnAdd).HasColumnName("VisibleOnAdd").IsRequired();
            Property(x => x.ExpirationDate).HasColumnName("ExpirationDate").IsOptional();
            Property(x => x.ExcludeValueOnCopy).HasColumnName("ExcludeValueOnCopy").IsRequired();
            Property(x => x.VisibleOnBPM).HasColumnName("VisibleOnBPM").IsRequired();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.CompanyNumberAttribute).HasForeignKey(c => c.CompanyNumberId); // FK_CompanyNumberAttribute_CompanyNumber
            HasRequired(a => a.AttributeDataType).WithMany(b => b.CompanyNumberAttribute).HasForeignKey(c => c.AttributeDataTypeId); // FK_CompanyNumberAttribute_AttributeDataType
        }
    }
}
