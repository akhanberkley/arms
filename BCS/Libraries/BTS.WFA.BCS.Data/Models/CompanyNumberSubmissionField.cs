using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberSubmissionType
    public class CompanyNumberSubmissionField
    {
        public int CompanyNumberId { get; set; }
        public Guid SubmissionFieldId { get; set; }
        public bool Enabled { get; set; }
        public string DisplayLabel { get; set; }

        public virtual CompanyNumber CompanyNumber { get; set; }
        public virtual SubmissionField SubmissionField { get; set; }
    }

    internal class CompanyNumberSubmissionFieldConfiguration : EntityTypeConfiguration<CompanyNumberSubmissionField>
    {
        public CompanyNumberSubmissionFieldConfiguration()
        {
            ToTable("dbo.CompanyNumberSubmissionField");
            HasKey(x => new { x.CompanyNumberId, x.SubmissionFieldId });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SubmissionFieldId).HasColumnName("SubmissionFieldId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Enabled).HasColumnName("Enabled").IsRequired();
            Property(x => x.DisplayLabel).HasColumnName("DisplayLabel").IsOptional().HasMaxLength(50);

            HasRequired(a => a.CompanyNumber).WithMany().HasForeignKey(c => c.CompanyNumberId);
            HasRequired(a => a.SubmissionField).WithMany().HasForeignKey(c => c.SubmissionFieldId);
        }
    }
}
