using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // NaicsGroup
    public class NaicsGroup
    {
        public int Id { get; set; } // Id (Primary key)
        public int NaicsDivisionId { get; set; } // NaicsDivisionId
        public string GroupDescription { get; set; } // GroupDescription

        // Reverse navigation
        public virtual ICollection<NaicsCodeList> NaicsCodeList { get; set; } // NaicsCodeList.FK_NaicsCodeList_NaicsGroup

        // Foreign keys
        public virtual NaicsDivision NaicsDivision { get; set; } //  FK_NaicsGroup_NaicsDivision

        public NaicsGroup()
        {
            NaicsCodeList = new List<NaicsCodeList>();
        }
    }

    internal class NaicsGroupConfiguration : EntityTypeConfiguration<NaicsGroup>
    {
        public NaicsGroupConfiguration()
        {
            ToTable("dbo.NaicsGroup");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.NaicsDivisionId).HasColumnName("NaicsDivisionId").IsRequired();
            Property(x => x.GroupDescription).HasColumnName("GroupDescription").IsRequired().HasMaxLength(255);

            // Foreign keys
            HasRequired(a => a.NaicsDivision).WithOptional(b => b.NaicsGroup); // FK_NaicsGroup_NaicsDivision
        }
    }
}
