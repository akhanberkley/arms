using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // NumberControlType
    public class NumberControlType
    {
        public int Id { get; set; } // Id (Primary key)
        public string TypeName { get; set; } // TypeName

        // Reverse navigation
        public virtual ICollection<CompanyNumber> CompanyNumber { get; set; } // Many to many mapping
        public virtual ICollection<NumberControl> NumberControl { get; set; } // NumberControl.FK_NumberControl_NumberControlType

        public NumberControlType()
        {
            CompanyNumber = new List<CompanyNumber>();
            NumberControl = new List<NumberControl>();
        }
    }

    internal class NumberControlTypeConfiguration : EntityTypeConfiguration<NumberControlType>
    {
        public NumberControlTypeConfiguration()
        {
            ToTable("dbo.NumberControlType");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.TypeName).HasColumnName("TypeName").IsRequired().HasMaxLength(100);
        }
    }
}
