using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ClientCoreAddressAddressType
    public class ClientCoreAddressAddressType
    {
        public int ClientId { get; set; } // ClientId (Primary key)
        public int ClientCoreAddressId { get; set; } // ClientCoreAddressId (Primary key)
        public int AddressTypeId { get; set; } // AddressTypeId (Primary key)
        
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }

        // Foreign keys
        public virtual Client Client { get; set; } //  FK_ClientCoreAddressAddressType_Client
        public virtual AddressType AddressType { get; set; } //  FK_ClientCoreAddressAddressType_AddressType
    }

    internal class ClientCoreAddressAddressTypeConfiguration : EntityTypeConfiguration<ClientCoreAddressAddressType>
    {
        public ClientCoreAddressAddressTypeConfiguration()
        {
            ToTable("dbo.ClientCoreAddressAddressType");
            HasKey(x => new { x.ClientId, x.ClientCoreAddressId });

            Property(x => x.ClientId).HasColumnName("ClientId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ClientCoreAddressId).HasColumnName("ClientCoreAddressId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.AddressTypeId).HasColumnName("AddressTypeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Latitude).HasColumnName("Latitude").IsOptional().HasPrecision(9, 6);
            Property(x => x.Longitude).HasColumnName("Longitude").IsOptional().HasPrecision(9, 6);

            // Foreign keys
            HasRequired(a => a.Client).WithMany(b => b.ClientCoreAddressAddressType).HasForeignKey(c => c.ClientId); // FK_ClientCoreAddressAddressType_Client
            HasRequired(a => a.AddressType).WithMany().HasForeignKey(c => c.AddressTypeId); // FK_ClientCoreAddressAddressType_AddressType
        }
    }
}
