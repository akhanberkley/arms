using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // OtherCarrier
    public class OtherCarrier
    {
        public int Id { get; set; } // Id (Primary key)
        public string Code { get; set; } // Code
        public string Description { get; set; } // Description

        // Reverse navigation
        public virtual ICollection<CompanyNumberOtherCarrier> CompanyNumberOtherCarrier { get; set; } // Many to many mapping

        public OtherCarrier()
        {
            CompanyNumberOtherCarrier = new List<CompanyNumberOtherCarrier>();
        }
    }

    internal class OtherCarrierConfiguration : EntityTypeConfiguration<OtherCarrier>
    {
        public OtherCarrierConfiguration()
        {
            ToTable("dbo.OtherCarrier");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Code).HasColumnName("Code").IsOptional().HasMaxLength(100);
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(100);
        }
    }
}
