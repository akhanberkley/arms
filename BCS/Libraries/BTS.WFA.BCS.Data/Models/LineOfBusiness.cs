using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // LineOfBusiness
    public class LineOfBusiness
    {
        public int Id { get; set; } // Id (Primary key)
        public long? ApsId { get; set; } // APSId
        public string Code { get; set; } // Code
        public string Description { get; set; } // Description
        public string Line { get; set; } // Line

        // Reverse navigation
        public virtual ICollection<CompanyNumber> CompanyNumber { get; set; } // Many to many mapping
        public virtual ICollection<AgencyLineOfBusiness> AgencyLineOfBusiness { get; set; } // Many to many mapping

        public LineOfBusiness()
        {
            CompanyNumber = new List<CompanyNumber>();
            AgencyLineOfBusiness = new List<AgencyLineOfBusiness>();
        }
    }

    internal class LineOfBusinessConfiguration : EntityTypeConfiguration<LineOfBusiness>
    {
        public LineOfBusinessConfiguration()
        {
            ToTable("dbo.LineOfBusiness");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ApsId).HasColumnName("APSId").IsOptional();
            Property(x => x.Code).HasColumnName("Code").IsRequired().HasMaxLength(25);
            Property(x => x.Description).HasColumnName("Description").IsRequired().HasMaxLength(50);
            Property(x => x.Line).HasColumnName("Line").IsRequired().HasMaxLength(2);
        }
    }
}
