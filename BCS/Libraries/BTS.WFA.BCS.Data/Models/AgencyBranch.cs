using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    public class AgencyBranch
    {
        public int Id { get; set; } // Id (Primary key)
        public long? ApsId { get; set; } // APSId
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public string Code { get; set; }
        public string Name { get; set; }

        public CompanyNumber CompanyNumber { get; set; }
    }

    internal class AgencyBranchConfiguration : EntityTypeConfiguration<AgencyBranch>
    {
        public AgencyBranchConfiguration()
        {
            ToTable("dbo.AgencyBranch");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ApsId).HasColumnName("APSId").IsOptional();
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.Code).HasColumnName("Code").IsRequired().HasMaxLength(50);
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasMaxLength(255);
        }
    }
}
