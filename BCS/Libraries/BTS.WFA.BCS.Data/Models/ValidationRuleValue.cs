using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ValidationRuleValue
    public class ValidationRuleValue
    {
        public int Id { get; set; } // ID (Primary key)
        public int ValidationRuleId { get; set; } // ValidationRuleID
        public string Value { get; set; } // Value
        public bool Display { get; set; } // Display

        // Foreign keys
        public virtual ValidationRule ValidationRule { get; set; } //  FK_ValidationRuleValue_ValidationRule

        public ValidationRuleValue()
        {
            Display = true;
        }
    }

    internal class ValidationRuleValueConfiguration : EntityTypeConfiguration<ValidationRuleValue>
    {
        public ValidationRuleValueConfiguration()
        {
            ToTable("dbo.ValidationRuleValue");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ValidationRuleId).HasColumnName("ValidationRuleID").IsRequired();
            Property(x => x.Value).HasColumnName("Value").IsRequired().HasMaxLength(50);
            Property(x => x.Display).HasColumnName("Display").IsRequired();

            // Foreign keys
            HasRequired(a => a.ValidationRule).WithMany(b => b.ValidationRuleValue).HasForeignKey(c => c.ValidationRuleId); // FK_ValidationRuleValue_ValidationRule
        }
    }
}
