using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Agent
    public class Agent
    {
        public int Id { get; set; } // Id (Primary key)
        public long? ApsId { get; set; } // APSId
        public long? APSPersonnelId { get; set; } // APSPersonnelId
        public int AgencyId { get; set; } // AgencyId
        public int? UserId { get; set; } // UserId
        public string BrokerNo { get; set; } // BrokerNo
        public int? AddressId { get; set; } // AddressId
        public string Prefix { get; set; } // Prefix
        public string FirstName { get; set; } // FirstName
        public string Middle { get; set; } // Middle
        public string Surname { get; set; } // Surname
        public string SuffixTitle { get; set; } // SuffixTitle
        public string ProfessionalTitle { get; set; } // ProfessionalTitle
        public string FunctionalTitle { get; set; } // FunctionalTitle
        public string DepartmentName { get; set; } // DepartmentName
        public string MailStopCode { get; set; } // MailStopCode
        public string PrimaryPhoneAreaCode { get; set; } // PrimaryPhoneAreaCode
        public string PrimaryPhone { get; set; } // PrimaryPhone
        public string SecondaryPhoneAreaCode { get; set; } // SecondaryPhoneAreaCode
        public string SecondaryPhone { get; set; } // SecondaryPhone
        public string FaxAreaCode { get; set; } // FaxAreaCode
        public string Fax { get; set; } // Fax
        public string EmailAddress { get; set; } // EmailAddress
        public DateTime? Dob { get; set; } // DOB
        public string TaxId { get; set; } // TaxId
        public DateTime? EffectiveDate { get; set; } // EffectiveDate
        public DateTime? CancelDate { get; set; } // CancelDate
        public bool Active { get; set; } // Active
        public string EntryBy { get; set; } // EntryBy
        public DateTime EntryDt { get; set; } // EntryDt
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedDt { get; set; } // ModifiedDt

        // Foreign keys
        public virtual Agency Agency { get; set; } //  FK_Agent_Agency

        public Agent()
        {
            Active = true;
            EntryDt = DateTime.Now;
        }
    }

    internal class AgentConfiguration : EntityTypeConfiguration<Agent>
    {
        public AgentConfiguration()
        {
            ToTable("dbo.Agent");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ApsId).HasColumnName("APSId").IsOptional();
            Property(x => x.APSPersonnelId).HasColumnName("APSPersonnelId").IsOptional();
            Property(x => x.AgencyId).HasColumnName("AgencyId").IsRequired();
            Property(x => x.UserId).HasColumnName("UserId").IsOptional();
            Property(x => x.BrokerNo).HasColumnName("BrokerNo").IsRequired().HasMaxLength(50);
            Property(x => x.AddressId).HasColumnName("AddressId").IsOptional();
            Property(x => x.Prefix).HasColumnName("Prefix").IsOptional().HasMaxLength(50);
            Property(x => x.FirstName).HasColumnName("FirstName").IsRequired().HasMaxLength(50);
            Property(x => x.Middle).HasColumnName("Middle").IsOptional().HasMaxLength(50);
            Property(x => x.Surname).HasColumnName("Surname").IsRequired().HasMaxLength(50);
            Property(x => x.SuffixTitle).HasColumnName("SuffixTitle").IsOptional().HasMaxLength(50);
            Property(x => x.ProfessionalTitle).HasColumnName("ProfessionalTitle").IsOptional().HasMaxLength(255);
            Property(x => x.FunctionalTitle).HasColumnName("FunctionalTitle").IsOptional().HasMaxLength(255);
            Property(x => x.DepartmentName).HasColumnName("DepartmentName").IsOptional().HasMaxLength(255);
            Property(x => x.MailStopCode).HasColumnName("MailStopCode").IsOptional().HasMaxLength(255);
            Property(x => x.PrimaryPhoneAreaCode).HasColumnName("PrimaryPhoneAreaCode").IsOptional().HasMaxLength(3);
            Property(x => x.PrimaryPhone).HasColumnName("PrimaryPhone").IsOptional().HasMaxLength(7);
            Property(x => x.SecondaryPhoneAreaCode).HasColumnName("SecondaryPhoneAreaCode").IsOptional().HasMaxLength(3);
            Property(x => x.SecondaryPhone).HasColumnName("SecondaryPhone").IsOptional().HasMaxLength(7);
            Property(x => x.FaxAreaCode).HasColumnName("FaxAreaCode").IsOptional().HasMaxLength(3);
            Property(x => x.Fax).HasColumnName("Fax").IsOptional().HasMaxLength(7);
            Property(x => x.EmailAddress).HasColumnName("EmailAddress").IsOptional().HasMaxLength(255);
            Property(x => x.Dob).HasColumnName("DOB").IsOptional();
            Property(x => x.TaxId).HasColumnName("TaxId").IsOptional().HasMaxLength(11);
            Property(x => x.EffectiveDate).HasColumnName("EffectiveDate").IsOptional();
            Property(x => x.CancelDate).HasColumnName("CancelDate").IsOptional();
            Property(x => x.Active).HasColumnName("Active").IsRequired();
            Property(x => x.EntryBy).HasColumnName("EntryBy").IsRequired().HasMaxLength(50);
            Property(x => x.EntryDt).HasColumnName("EntryDt").IsRequired();
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasMaxLength(50);
            Property(x => x.ModifiedDt).HasColumnName("ModifiedDt").IsOptional();

            // Foreign keys
            HasRequired(a => a.Agency).WithMany(b => b.Agent).HasForeignKey(c => c.AgencyId); // FK_Agent_Agency
        }
    }
}
