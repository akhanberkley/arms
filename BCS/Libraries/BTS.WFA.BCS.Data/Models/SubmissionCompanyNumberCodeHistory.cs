using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionCompanyNumberCodeHistory
    public class SubmissionCompanyNumberCodeHistory
    {
        public int Id { get; set; } // Id (Primary key)
        public int SubmissionId { get; set; } // SubmissionId
        public string Action { get; set; } // Action
        public int CompanyNumberCodeId { get; set; } // CompanyNumberCodeId
        public string By { get; set; } // By
        public DateTime HistoryDt { get; set; } // HistoryDt

        // Foreign keys
        public virtual Submission Submission { get; set; } //  FK_SubmissionCompanyNumberCodeHistory_Submission
        public virtual CompanyNumberCode CompanyNumberCode { get; set; } //  FK_SubmissionCompanyNumberCodeHistory_CompanyNumberCode

        public SubmissionCompanyNumberCodeHistory()
        {
            By = "Initialize";
            HistoryDt = System.DateTime.Now;
        }
    }

    internal class SubmissionCompanyNumberCodeHistoryConfiguration : EntityTypeConfiguration<SubmissionCompanyNumberCodeHistory>
    {
        public SubmissionCompanyNumberCodeHistoryConfiguration()
        {
            ToTable("dbo.SubmissionCompanyNumberCodeHistory");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired();
            Property(x => x.Action).HasColumnName("Action").IsRequired().HasMaxLength(10);
            Property(x => x.CompanyNumberCodeId).HasColumnName("CompanyNumberCodeId").IsRequired();
            Property(x => x.By).HasColumnName("By").IsRequired().HasMaxLength(50);
            Property(x => x.HistoryDt).HasColumnName("HistoryDt").IsRequired();

            // Foreign keys
            HasRequired(a => a.Submission).WithMany(b => b.SubmissionCompanyNumberCodeHistory).HasForeignKey(c => c.SubmissionId); // FK_SubmissionCompanyNumberCodeHistory_Submission
            HasRequired(a => a.CompanyNumberCode).WithMany(b => b.SubmissionCompanyNumberCodeHistory).HasForeignKey(c => c.CompanyNumberCodeId); // FK_SubmissionCompanyNumberCodeHistory_CompanyNumberCode
        }
    }
}
