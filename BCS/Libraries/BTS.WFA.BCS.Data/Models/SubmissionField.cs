using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    public class SubmissionField
    {
        public Guid Id { get; set; }
        public string PropertyName { get; set; }
        public string DisplayLabel { get; set; }
        public string ColumnHeading { get; set; }
        public int? CompanyNumberAttributeId { get; set; }
        public int? CompanyNumberCodeTypeId { get; set; }

        public virtual CompanyNumberAttribute CompanyNumberAttribute { get; set; }
        public virtual CompanyNumberCodeType CompanyNumberCodeType { get; set; }
    }

    internal class SubmissionFieldConfiguration : EntityTypeConfiguration<SubmissionField>
    {
        public SubmissionFieldConfiguration()
        {
            ToTable("dbo.SubmissionField");
            HasKey(x => x.Id);

            Property(x => x.PropertyName).HasColumnName("PropertyName").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayLabel).HasColumnName("DisplayLabel").IsRequired().HasMaxLength(50);
            Property(x => x.ColumnHeading).HasColumnName("ColumnHeading").IsRequired().HasMaxLength(50);
            Property(x => x.CompanyNumberAttributeId).HasColumnName("CompanyNumberAttributeId").IsOptional();
            Property(x => x.CompanyNumberCodeTypeId).HasColumnName("CompanyNumberCodeTypeId").IsOptional();

            HasOptional(a => a.CompanyNumberAttribute).WithMany().HasForeignKey(c => c.CompanyNumberAttributeId);
            HasOptional(a => a.CompanyNumberCodeType).WithMany().HasForeignKey(c => c.CompanyNumberCodeTypeId);
        }
    }
}
