using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    public class ClearSync
    {
        public int Id { get; set; }
        public int CompanyNumberId { get; set; }
        public string Name { get; set; }
        public DateTime? LastRunDate { get; set; }
        public int LastRunStatus { get; set; }

        public ClearSync()
        {
            LastRunStatus = 0;
        }
    }

    internal class ClearSyncConfiguration : EntityTypeConfiguration<ClearSync>
    {
        public ClearSyncConfiguration()
        {
            ToTable("dbo.ClearSync");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasMaxLength(50);
            Property(x => x.LastRunDate).HasColumnName("LastRunDate").IsOptional();
            Property(x => x.LastRunStatus).HasColumnName("LastRunStatus").IsRequired();
        }
    }
}
