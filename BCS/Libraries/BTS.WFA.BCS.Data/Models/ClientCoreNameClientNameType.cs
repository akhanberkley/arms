using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ClientCoreNameClientNameType
    public class ClientCoreNameClientNameType
    {
        public int ClientId { get; set; } // ClientId (Primary key)
        public int ClientCoreNameId { get; set; } // ClientCoreNameId (Primary key)
        public int ClientNameTypeId { get; set; } // ClientNameTypeId (Primary key)

        // Foreign keys
        public virtual Client Client { get; set; } //  FK_ClientCoreNameClientNameType_Client
        public virtual ClientNameType ClientNameType { get; set; } //  FK_ClientCoreNameClientNameType_ClientNameType
    }

    internal class ClientCoreNameClientNameTypeConfiguration : EntityTypeConfiguration<ClientCoreNameClientNameType>
    {
        public ClientCoreNameClientNameTypeConfiguration()
        {
            ToTable("dbo.ClientCoreNameClientNameType");
            HasKey(x => new { x.ClientId, x.ClientCoreNameId });

            Property(x => x.ClientId).HasColumnName("ClientId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ClientCoreNameId).HasColumnName("ClientCoreNameId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ClientNameTypeId).HasColumnName("ClientNameTypeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Foreign keys
            HasRequired(a => a.Client).WithMany(b => b.ClientCoreNameClientNameType).HasForeignKey(c => c.ClientId); // FK_ClientCoreNameClientNameType_Client
            HasRequired(a => a.ClientNameType).WithMany(b => b.ClientCoreNameClientNameType).HasForeignKey(c => c.ClientNameTypeId); // FK_ClientCoreNameClientNameType_ClientNameType
        }
    }
}
