using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberSubmissionType
    public class CompanyNumberSubmissionFieldSubmissionType
    {
        public int CompanyNumberId { get; set; }
        public Guid SubmissionFieldId { get; set; }
        public int SubmissionTypeId { get; set; }
        public bool Required { get; set; }
        public bool VisibleOnAdd { get; set; }
        public bool VisibleOnBPM { get; set; }

        public virtual CompanyNumberSubmissionField CompanyNumberSubmissionField { get; set; }
        public virtual CompanyNumberSubmissionType CompanyNumberSubmissionType { get; set; }
    }

    internal class CompanyNumberSubmissionFieldSubmissionTypeConfiguration : EntityTypeConfiguration<CompanyNumberSubmissionFieldSubmissionType>
    {
        public CompanyNumberSubmissionFieldSubmissionTypeConfiguration()
        {
            ToTable("dbo.CompanyNumberSubmissionFieldSubmissionType");
            HasKey(x => new { x.CompanyNumberId, x.SubmissionFieldId, x.SubmissionTypeId });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SubmissionTypeId).HasColumnName("SubmissionTypeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SubmissionFieldId).HasColumnName("SubmissionFieldId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Required).HasColumnName("Required").IsRequired();
            Property(x => x.VisibleOnAdd).HasColumnName("VisibleOnAdd").IsRequired();
            Property(x => x.VisibleOnBPM).HasColumnName("VisibleOnBPM").IsRequired();

            HasRequired(a => a.CompanyNumberSubmissionField).WithMany().HasForeignKey(c => new { c.CompanyNumberId, c.SubmissionFieldId });
            HasRequired(a => a.CompanyNumberSubmissionType).WithMany().HasForeignKey(c => new { c.CompanyNumberId, c.SubmissionTypeId });
        }
    }
}
