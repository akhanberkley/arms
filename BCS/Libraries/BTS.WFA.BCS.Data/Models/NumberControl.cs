using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // NumberControl
    public class NumberControl
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public int NumberControlTypeId { get; set; } // NumberControlTypeId
        public long ControlNumber { get; set; } // ControlNumber
        public string PaddingChar { get; set; } // PaddingChar
        public bool PadHead { get; set; } // PadHead
        public int TotalLength { get; set; } // TotalLength

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_NumberControl_CompanyNumber
        public virtual NumberControlType NumberControlType { get; set; } //  FK_NumberControl_NumberControlType

        public NumberControl()
        {
            PadHead = false;
            TotalLength = 0;
        }
    }

    internal class NumberControlConfiguration : EntityTypeConfiguration<NumberControl>
    {
        public NumberControlConfiguration()
        {
            ToTable("dbo.NumberControl");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.NumberControlTypeId).HasColumnName("NumberControlTypeId").IsRequired();
            Property(x => x.ControlNumber).HasColumnName("ControlNumber").IsRequired().IsConcurrencyToken();
            Property(x => x.PaddingChar).HasColumnName("PaddingChar").IsOptional().HasMaxLength(1);
            Property(x => x.PadHead).HasColumnName("PadHead").IsRequired();
            Property(x => x.TotalLength).HasColumnName("TotalLength").IsRequired();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.NumberControl).HasForeignKey(c => c.CompanyNumberId); // FK_NumberControl_CompanyNumber
            HasRequired(a => a.NumberControlType).WithMany(b => b.NumberControl).HasForeignKey(c => c.NumberControlTypeId); // FK_NumberControl_NumberControlType
        }
    }
}
