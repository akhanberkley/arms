using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionType
    public class SubmissionType
    {
        public int Id { get; set; } // Id (Primary key)
        public string TypeName { get; set; } // TypeName
        public string Abbreviation { get; set; } // Abbreviation

        // Reverse navigation
        public virtual ICollection<CompanyNumberSubmissionType> CompanyNumberSubmissionType { get; set; } // Many to many mapping

        public SubmissionType()
        {
            CompanyNumberSubmissionType = new List<CompanyNumberSubmissionType>();
        }
    }

    internal class SubmissionTypeConfiguration : EntityTypeConfiguration<SubmissionType>
    {
        public SubmissionTypeConfiguration()
        {
            ToTable("dbo.SubmissionType");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.TypeName).HasColumnName("TypeName").IsRequired().HasMaxLength(50);
            Property(x => x.Abbreviation).HasColumnName("Abbreviation").IsOptional().HasMaxLength(10);
        }
    }
}
