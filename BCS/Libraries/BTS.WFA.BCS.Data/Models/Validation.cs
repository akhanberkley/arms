using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Validation
    public class Validation
    {
        public int Id { get; set; } // ID (Primary key)
        public int CompanyId { get; set; } // CompanyID

        // Reverse navigation
        public virtual ICollection<ValidationCondition> ValidationCondition { get; set; } // ValidationCondition.FK_ValidationCondition_ValidationCondition
        public virtual ICollection<ValidationRule> ValidationRule { get; set; } // ValidationRule.FK_ValidationRule_ValidationCondition

        // Foreign keys
        public virtual Company Company { get; set; } //  FK_Validation_Company

        public Validation()
        {
            ValidationCondition = new List<ValidationCondition>();
            ValidationRule = new List<ValidationRule>();
        }
    }

    internal class ValidationConfiguration : EntityTypeConfiguration<Validation>
    {
        public ValidationConfiguration()
        {
            ToTable("dbo.Validation");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();

            // Foreign keys
            HasRequired(a => a.Company).WithMany(b => b.Validation).HasForeignKey(c => c.CompanyId); // FK_Validation_Company
        }
    }
}
