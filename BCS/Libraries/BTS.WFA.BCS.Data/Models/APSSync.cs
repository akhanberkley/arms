using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // APSXMLLog
    public class APSSync
    {
        public int CompanyNumberID { get; set; }
        public long AgencyAPSID { get; set; }
        public string AgencyCode { get; set; }
        public DateTime? ProcessedOn { get; set; }
        public string RollbackScript { get; set; }
        public bool CanProcess { get; set; }
        public bool? LastRunSuccessful { get; set; }

        public APSSync()
        {
            AgencyAPSID = 0;
            CanProcess = true;
        }
    }

    internal class APSSyncConfiguration : EntityTypeConfiguration<APSSync>
    {
        public APSSyncConfiguration()
        {
            ToTable("dbo.APSSync");
            HasKey(x => new { x.CompanyNumberID, x.AgencyCode });

            Property(x => x.CompanyNumberID).HasColumnName("CompanyNumberID").IsRequired();
            Property(x => x.AgencyCode).HasColumnName("AgencyCode").IsRequired().HasMaxLength(15);
            Property(x => x.AgencyAPSID).HasColumnName("AgencyAPSID").IsRequired();
            Property(x => x.ProcessedOn).HasColumnName("ProcessedOn").IsOptional();
            Property(x => x.RollbackScript).HasColumnName("RollbackScript").IsOptional();
            Property(x => x.CanProcess).HasColumnName("CanProcess").IsRequired();
            Property(x => x.LastRunSuccessful).HasColumnName("LastRunSuccessful").IsOptional();
        }
    }
}
