using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionComment
    public class SubmissionComment
    {
        public int Id { get; set; } // Id (Primary key)
        public int SubmissionId { get; set; } // SubmissionId
        public string Comment { get; set; } // Comment
        public string EntryBy { get; set; } // EntryBy
        public DateTime EntryDt { get; set; } // EntryDt
        public string ModifiedBy { get; set; } // ModifiedBy
        public DateTime? ModifiedDt { get; set; } // ModifiedDt

        // Foreign keys
        public virtual Submission Submission { get; set; } //  FK_SubmissionComment_Submission

        public SubmissionComment()
        {
            EntryDt = System.DateTime.Now;
        }
    }

    internal class SubmissionCommentConfiguration : EntityTypeConfiguration<SubmissionComment>
    {
        public SubmissionCommentConfiguration()
        {
            ToTable("dbo.SubmissionComment");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired();
            Property(x => x.Comment).HasColumnName("Comment").IsRequired().HasMaxLength(5000);
            Property(x => x.EntryBy).HasColumnName("EntryBy").IsRequired().HasMaxLength(50);
            Property(x => x.EntryDt).HasColumnName("EntryDt").IsRequired();
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasMaxLength(50);
            Property(x => x.ModifiedDt).HasColumnName("ModifiedDt").IsOptional();

            // Foreign keys
            HasRequired(a => a.Submission).WithMany(b => b.SubmissionComment).HasForeignKey(c => c.SubmissionId); // FK_SubmissionComment_Submission
        }
    }
}
