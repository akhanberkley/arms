using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // State
    public class State
    {
        public int Id { get; set; } // Id (Primary key)
        public string StateName { get; set; } // StateName
        public string Abbreviation { get; set; } // Abbreviation

        // Reverse navigation
        public virtual ICollection<AgencyLicensedState> AgencyLicensedState { get; set; } // AgencyLicensedState.FK_AgencyLicensedState_State

        public State()
        {
            AgencyLicensedState = new List<AgencyLicensedState>();
        }
    }

    internal class StateConfiguration : EntityTypeConfiguration<State>
    {
        public StateConfiguration()
        {
            ToTable("dbo.State");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.StateName).HasColumnName("StateName").IsOptional().HasMaxLength(255);
            Property(x => x.Abbreviation).HasColumnName("Abbreviation").IsOptional().HasMaxLength(2);
        }
    }
}
