using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyClientAdvancedSearch
    public class CompanyClientAdvancedSearch
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyId { get; set; } // CompanyId
        public string Dsik { get; set; } // DSIK
        public string ClientAdvSearchEndPointUrl { get; set; } // ClientAdvSearchEndPointURL
        public bool UsesReversePhoneLookup { get; set; } // UsesReversePhoneLookup
        public bool UsesOfacCheck { get; set; } // UsesOfacCheck
        public bool IsPhoneRequiredField { get; set; } // IsPhoneRequiredField
        public string Username { get; set; } // Username
        public string Password { get; set; } // Password
        public bool PopulateBusinessName { get; set; } // PopulateBusinessName
        public bool PopulateCity { get; set; } // PopulateCity
        public bool PopulateState { get; set; } // PopulateState
        public bool UsesOfacSubmissionComment { get; set; } // UsesOfacSubmissionComment
        public string ClientExperianSearchEndPointUrl { get; set; }

        // Foreign keys
        public virtual Company Company { get; set; } //  FK_CompanyClientAdvancedSearch_Company

        public CompanyClientAdvancedSearch()
        {
            UsesReversePhoneLookup = false;
            UsesOfacCheck = false;
            IsPhoneRequiredField = false;
            PopulateBusinessName = true;
            PopulateCity = false;
            PopulateState = false;
        }
    }

    internal class CompanyClientAdvancedSearchConfiguration : EntityTypeConfiguration<CompanyClientAdvancedSearch>
    {
        public CompanyClientAdvancedSearchConfiguration()
        {
            ToTable("dbo.CompanyClientAdvancedSearch");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyId).HasColumnName("CompanyId").IsRequired();
            Property(x => x.Dsik).HasColumnName("DSIK").IsOptional().HasMaxLength(50);
            Property(x => x.ClientAdvSearchEndPointUrl).HasColumnName("ClientAdvSearchEndPointURL").IsOptional().HasMaxLength(255);
            Property(x => x.UsesReversePhoneLookup).HasColumnName("UsesReversePhoneLookup").IsRequired();
            Property(x => x.UsesOfacCheck).HasColumnName("UsesOfacCheck").IsRequired();
            Property(x => x.IsPhoneRequiredField).HasColumnName("IsPhoneRequiredField").IsRequired();
            Property(x => x.Username).HasColumnName("Username").IsOptional().HasMaxLength(50);
            Property(x => x.Password).HasColumnName("Password").IsOptional().HasMaxLength(50);
            Property(x => x.PopulateBusinessName).HasColumnName("PopulateBusinessName").IsRequired();
            Property(x => x.PopulateCity).HasColumnName("PopulateCity").IsRequired();
            Property(x => x.PopulateState).HasColumnName("PopulateState").IsRequired();
            Property(x => x.UsesOfacSubmissionComment).HasColumnName("UsesOfacSubmissionComment").IsRequired();
            Property(x => x.ClientExperianSearchEndPointUrl).HasColumnName("ClientExperianSearchEndPointUrl").IsOptional().HasMaxLength(255);
            
            // Foreign keys
            HasRequired(a => a.Company).WithMany(b => b.CompanyClientAdvancedSearch).HasForeignKey(c => c.CompanyId); // FK_CompanyClientAdvancedSearch_Company
        }
    }
}
