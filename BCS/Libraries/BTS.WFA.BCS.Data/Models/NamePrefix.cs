using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // NamePrefix
    public class NamePrefix
    {
        public string Prefix { get; set; } // Prefix
        public int Id { get; set; } // Id (Primary key)
    }

    internal class NamePrefixConfiguration : EntityTypeConfiguration<NamePrefix>
    {
        public NamePrefixConfiguration()
        {
            ToTable("dbo.NamePrefix");
            HasKey(x => x.Id);

            Property(x => x.Prefix).HasColumnName("Prefix").IsRequired().HasMaxLength(50);
            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
