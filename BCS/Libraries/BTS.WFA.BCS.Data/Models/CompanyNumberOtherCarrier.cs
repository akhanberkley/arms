using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberOtherCarrier
    public class CompanyNumberOtherCarrier
    {
        public int CompanyNumberId { get; set; } // CompanyNumberId (Primary key)
        public int OtherCarrierId { get; set; } // OtherCarrierId (Primary key)
        public int Order { get; set; } // Order
        public bool DefaultSelection { get; set; } // DefaultSelection

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_CompanyNumberOtherCarrier_CompanyNumber
        public virtual OtherCarrier OtherCarrier { get; set; } //  FK_CompanyNumberOtherCarrier_OtherCarrier

        public CompanyNumberOtherCarrier()
        {
            Order = 0;
            DefaultSelection = false;
        }
    }

    internal class CompanyNumberOtherCarrierConfiguration : EntityTypeConfiguration<CompanyNumberOtherCarrier>
    {
        public CompanyNumberOtherCarrierConfiguration()
        {
            ToTable("dbo.CompanyNumberOtherCarrier");
            HasKey(x => new { x.CompanyNumberId, x.OtherCarrierId });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.OtherCarrierId).HasColumnName("OtherCarrierId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Order).HasColumnName("Order").IsRequired();
            Property(x => x.DefaultSelection).HasColumnName("DefaultSelection").IsRequired();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.CompanyNumberOtherCarrier).HasForeignKey(c => c.CompanyNumberId); // FK_CompanyNumberOtherCarrier_CompanyNumber
            HasRequired(a => a.OtherCarrier).WithMany(b => b.CompanyNumberOtherCarrier).HasForeignKey(c => c.OtherCarrierId); // FK_CompanyNumberOtherCarrier_OtherCarrier
        }
    }
}
