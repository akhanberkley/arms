using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ValidationField
    public class ValidationField
    {
        public int Id { get; set; } // ID (Primary key)
        public int? CompanyId { get; set; } // CompanyID
        public bool? CompanySpecific { get; set; } // CompanySpecific
        public string TableName { get; set; } // TableName
        public string ColumnName { get; set; } // ColumnName
        public string DisplayName { get; set; } // DisplayName
        public string DotNetDataType { get; set; } // DotNetDataType
        public string ControlId { get; set; } // ControlID
        public string ControlType { get; set; } // ControlType
        public bool NullAllowed { get; set; } // NullAllowed

        // Reverse navigation
        public virtual ICollection<ValidationCondition> ValidationCondition { get; set; } // ValidationCondition.FK_ValidationCondition_ValidationField
        public virtual ICollection<ValidationRule> ValidationRule { get; set; } // ValidationRule.FK_ValidationRule_ValidationField1

        // Foreign keys
        public virtual Company Company { get; set; } //  FK_ValidationField_Company

        public ValidationField()
        {
            CompanySpecific = false;
            NullAllowed = true;
            ValidationCondition = new List<ValidationCondition>();
            ValidationRule = new List<ValidationRule>();
        }
    }

    internal class ValidationFieldConfiguration : EntityTypeConfiguration<ValidationField>
    {
        public ValidationFieldConfiguration()
        {
            ToTable("dbo.ValidationField");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsOptional();
            Property(x => x.CompanySpecific).HasColumnName("CompanySpecific").IsOptional();
            Property(x => x.TableName).HasColumnName("TableName").IsOptional().HasMaxLength(50);
            Property(x => x.ColumnName).HasColumnName("ColumnName").IsOptional().HasMaxLength(50);
            Property(x => x.DisplayName).HasColumnName("DisplayName").IsOptional().HasMaxLength(50);
            Property(x => x.DotNetDataType).HasColumnName("DotNetDataType").IsOptional().HasMaxLength(50);
            Property(x => x.ControlId).HasColumnName("ControlID").IsOptional().HasMaxLength(50);
            Property(x => x.ControlType).HasColumnName("ControlType").IsOptional().HasMaxLength(50);
            Property(x => x.NullAllowed).HasColumnName("NullAllowed").IsRequired();

            // Foreign keys
            HasOptional(a => a.Company).WithMany(b => b.ValidationField).HasForeignKey(c => c.CompanyId); // FK_ValidationField_Company
        }
    }
}
