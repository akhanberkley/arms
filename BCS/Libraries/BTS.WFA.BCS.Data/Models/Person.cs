using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Person
    public class Person
    {
        public int Id { get; set; } // Id (Primary key)
        public long? ApsId { get; set; } // APSId
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public string UserName { get; set; } // UserName
        public string FullName { get; set; } // FullName
        public string EmailAddress { get; set; } // EmailAddress
        public string Initials { get; set; } // Initials
        public bool Active { get; set; } // Active
        public DateTime? RetirementDate { get; set; } // RetirementDate

        // Reverse navigation
        public virtual ICollection<AgencyLineOfBusinessPersonUnderwritingRolePerson> AgencyLineOfBusinessPersonUnderwritingRolePerson { get; set; } // Many to many mapping
        public virtual ICollection<UnderwritingRole> UnderwritingRoles { get; set; }

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_Person_CompanyNumber

        public Person()
        {
            Active = true;
            AgencyLineOfBusinessPersonUnderwritingRolePerson = new List<AgencyLineOfBusinessPersonUnderwritingRolePerson>();
        }
    }

    internal class PersonConfiguration : EntityTypeConfiguration<Person>
    {
        public PersonConfiguration()
        {
            ToTable("dbo.Person");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ApsId).HasColumnName("APSId").IsOptional();
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.UserName).HasColumnName("UserName").IsRequired().HasMaxLength(255);
            Property(x => x.FullName).HasColumnName("FullName").IsOptional().HasMaxLength(255);
            Property(x => x.EmailAddress).HasColumnName("EmailAddress").IsOptional().HasMaxLength(255);
            Property(x => x.Initials).HasColumnName("Initials").IsOptional().HasMaxLength(3);
            Property(x => x.Active).HasColumnName("Active").IsRequired();
            Property(x => x.RetirementDate).HasColumnName("RetirementDate").IsOptional();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.Person).HasForeignKey(c => c.CompanyNumberId); // FK_Person_CompanyNumber

            HasMany(t => t.UnderwritingRoles).WithMany().Map(m =>
            {
                m.ToTable("PersonUnderwritingRole");
                m.MapLeftKey("PersonId");
                m.MapRightKey("UnderwritingRoleId");
            });
        }
    }
}
