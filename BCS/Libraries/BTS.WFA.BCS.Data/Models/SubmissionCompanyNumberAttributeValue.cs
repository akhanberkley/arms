using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionCompanyNumberAttributeValue
    public class SubmissionCompanyNumberAttributeValue
    {
        public int SubmissionId { get; set; } // SubmissionId (Primary key)
        public int CompanyNumberAttributeId { get; set; } // CompanyNumberAttributeId (Primary key)
        public string AttributeValue { get; set; } // AttributeValue

        // Foreign keys
        public virtual Submission Submission { get; set; } //  FK_SubmissionCompanyNumberAttributeValue_Submission
        public virtual CompanyNumberAttribute CompanyNumberAttribute { get; set; } //  FK_SubmissionCompanyNumberAttributeValue_CompanyNumberAttribute
    }

    internal class SubmissionCompanyNumberAttributeValueConfiguration : EntityTypeConfiguration<SubmissionCompanyNumberAttributeValue>
    {
        public SubmissionCompanyNumberAttributeValueConfiguration()
        {
            ToTable("dbo.SubmissionCompanyNumberAttributeValue");
            HasKey(x => new { x.SubmissionId, x.CompanyNumberAttributeId });

            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyNumberAttributeId).HasColumnName("CompanyNumberAttributeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.AttributeValue).HasColumnName("AttributeValue").IsOptional().HasMaxLength(255);

            // Foreign keys
            HasRequired(a => a.Submission).WithMany(b => b.SubmissionCompanyNumberAttributeValue).HasForeignKey(c => c.SubmissionId); // FK_SubmissionCompanyNumberAttributeValue_Submission
            HasRequired(a => a.CompanyNumberAttribute).WithMany(b => b.SubmissionCompanyNumberAttributeValue).HasForeignKey(c => c.CompanyNumberAttributeId); // FK_SubmissionCompanyNumberAttributeValue_CompanyNumberAttribute
        }
    }
}
