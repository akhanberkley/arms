using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Submission
    public class ImportSubmission
    {
        public int Id { get; set; }
        public Guid BatchId { get; set; }
        public int CompanyNumberId { get; set; }
        public long ClientCoreClientId { get; set; }
        public string SubmissionType { get; set; }
        public string SubmissionStatus { get; set; }
        public string SubmissionStatusReason { get; set; }
        public string SubmissionStatusReasonOther { get; set; }
        public long? SubmissionNumber { get; set; }
        public int? Sequence { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string InsuredName { get; set; }
        public string AgencyNumber { get; set; }
        public string Agent { get; set; }
        public string UnderwritingUnit { get; set; }
        public string Underwriter { get; set; }
        public string Analyst { get; set; }
        public string Technician { get; set; }
        public decimal? EstimatedWrittenPremium { get; set; }
        public DateTime? SubmissionDt { get; set; }
        public string CompanyCustomizations { get; set; }
        public string CreatedSubmissionNumber { get; set; }
        public string ErrorMessage { get; set; }
    }

    internal class ImportSubmissionConfiguration : EntityTypeConfiguration<ImportSubmission>
    {
        public ImportSubmissionConfiguration()
        {
            ToTable("dbo.ImportSubmission");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.BatchId).HasColumnName("BatchId").IsRequired();
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.ClientCoreClientId).HasColumnName("ClientCoreClientId").IsRequired();
            Property(x => x.SubmissionType).HasColumnName("SubmissionType").IsRequired().HasMaxLength(50);
            Property(x => x.SubmissionStatus).HasColumnName("SubmissionStatus").IsRequired().HasMaxLength(100);
            Property(x => x.SubmissionStatusReason).HasColumnName("SubmissionStatusReason").IsOptional().HasMaxLength(50);
            Property(x => x.SubmissionStatusReasonOther).HasColumnName("SubmissionStatusReasonOther").IsOptional().HasMaxLength(255);
            Property(x => x.SubmissionNumber).HasColumnName("SubmissionNumber").IsOptional();
            Property(x => x.Sequence).HasColumnName("Sequence").IsOptional();
            Property(x => x.PolicyNumber).HasColumnName("PolicyNumber").IsOptional().HasMaxLength(50);
            Property(x => x.EffectiveDate).HasColumnName("EffectiveDate").IsOptional();
            Property(x => x.ExpirationDate).HasColumnName("ExpirationDate").IsOptional();
            Property(x => x.InsuredName).HasColumnName("InsuredName").IsOptional().HasMaxLength(255);
            Property(x => x.AgencyNumber).HasColumnName("AgencyNumber").IsOptional().HasMaxLength(15);
            Property(x => x.Agent).HasColumnName("Agent").IsOptional().HasMaxLength(100);
            Property(x => x.UnderwritingUnit).HasColumnName("UnderwritingUnit").IsOptional().HasMaxLength(100);
            Property(x => x.Underwriter).HasColumnName("Underwriter").IsOptional().HasMaxLength(100);
            Property(x => x.Analyst).HasColumnName("Analyst").IsOptional().HasMaxLength(100);
            Property(x => x.Technician).HasColumnName("Technician").IsOptional().HasMaxLength(100);
            Property(x => x.EstimatedWrittenPremium).HasColumnName("EstimatedWrittenPremium").IsOptional().HasPrecision(19, 4);
            Property(x => x.SubmissionDt).HasColumnName("SubmissionDt").IsOptional();
            Property(x => x.CompanyCustomizations).HasColumnName("CompanyCustomizations").HasColumnType("xml").IsOptional();
            Property(x => x.CreatedSubmissionNumber).HasColumnName("CreatedSubmissionNumber").IsOptional().HasMaxLength(20);
            Property(x => x.ErrorMessage).HasColumnName("ErrorMessage").IsOptional().HasMaxLength(255);
        }
    }
}
