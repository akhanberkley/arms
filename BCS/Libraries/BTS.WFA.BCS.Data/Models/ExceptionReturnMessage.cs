using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ExceptionReturnMessage
    public class ExceptionReturnMessage
    {
        public int Id { get; set; } // Id (Primary key)
        public string Message { get; set; } // Message
        public string ExceptionMessageContains { get; set; } // ExceptionMessageContains
        public string ExceptionTraceContains { get; set; } // ExceptionTraceContains
        public bool Active { get; set; } // Active

        public ExceptionReturnMessage()
        {
            Active = true;
        }
    }

    internal class ExceptionReturnMessageConfiguration : EntityTypeConfiguration<ExceptionReturnMessage>
    {
        public ExceptionReturnMessageConfiguration()
        {
            ToTable("dbo.ExceptionReturnMessage");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Message).HasColumnName("Message").IsRequired().HasMaxLength(500);
            Property(x => x.ExceptionMessageContains).HasColumnName("ExceptionMessageContains").IsOptional().HasMaxLength(500);
            Property(x => x.ExceptionTraceContains).HasColumnName("ExceptionTraceContains").IsOptional().HasMaxLength(500);
            Property(x => x.Active).HasColumnName("Active").IsRequired();
        }
    }
}
