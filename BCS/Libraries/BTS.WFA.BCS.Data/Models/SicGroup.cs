using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SicGroup
    public class SicGroup
    {
        public int Id { get; set; } // Id (Primary key)
        public int SicDivisionId { get; set; } // SicDivisionId
        public string GroupDescription { get; set; } // GroupDescription

        // Reverse navigation
        public virtual ICollection<SicCodeList> SicCodeList { get; set; } // SicCodeList.FK_SicCodeList_SicGroup

        // Foreign keys
        public virtual SicDivision SicDivision { get; set; } //  FK_SicGroup_SicDivision

        public SicGroup()
        {
            SicCodeList = new List<SicCodeList>();
        }
    }

    internal class SicGroupConfiguration : EntityTypeConfiguration<SicGroup>
    {
        public SicGroupConfiguration()
        {
            ToTable("dbo.SicGroup");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.SicDivisionId).HasColumnName("SicDivisionId").IsRequired();
            Property(x => x.GroupDescription).HasColumnName("GroupDescription").IsRequired().HasMaxLength(255);

            // Foreign keys
            HasRequired(a => a.SicDivision).WithMany(b => b.SicGroup).HasForeignKey(c => c.SicDivisionId); // FK_SicGroup_SicDivision
        }
    }
}
