using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // User
    public class User
    {
        public int Id { get; set; } // Id (Primary key)
        public int? PrimaryCompanyId { get; set; } // PrimaryCompanyId
        public int? AgencyId { get; set; } // AgencyId
        public string Username { get; set; } // Username
        public string Password { get; set; } // Password
        public bool Active { get; set; } // Active
        public string Notes { get; set; } // Notes
        public string Segmentation { get; set; } // Segmentation
        public Guid UserTypeId { get; set; }

        public virtual Company PrimaryCompany { get; set; }
        public virtual UserType UserType { get; set; }
        public virtual Agency Agency { get; set; }

        // Reverse navigation
        public virtual ICollection<Company> Company { get; set; } // Many to many mapping
        public virtual ICollection<Role> Role { get; set; } // Many to many mapping

        public User()
        {
            Active = true;
            Company = new List<Company>();
            Role = new List<Role>();
        }
    }

    internal class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("dbo.User");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PrimaryCompanyId).HasColumnName("PrimaryCompanyId").IsOptional();
            Property(x => x.AgencyId).HasColumnName("AgencyId").IsOptional();
            Property(x => x.Username).HasColumnName("Username").IsRequired().HasMaxLength(255);
            Property(x => x.Password).HasColumnName("Password").IsOptional().HasMaxLength(255);
            Property(x => x.Active).HasColumnName("Active").IsRequired();
            Property(x => x.Notes).HasColumnName("Notes").IsOptional().HasMaxLength(1073741823);
            Property(x => x.Segmentation).HasColumnName("Segmentation").IsOptional().HasMaxLength(20);

            HasOptional(x => x.PrimaryCompany).WithMany().HasForeignKey(c => c.PrimaryCompanyId);
            HasRequired(x => x.UserType).WithMany().HasForeignKey(c => c.UserTypeId);
            HasOptional(x => x.Agency).WithMany().HasForeignKey(c => c.AgencyId);
        }
    }
}
