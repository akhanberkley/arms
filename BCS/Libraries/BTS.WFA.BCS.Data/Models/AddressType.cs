using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // AddressType
    public class AddressType
    {
        public int Id { get; set; } // Id (Primary key)
        public string AddressTypeValue { get; set; } // AddressType
    }

    internal class AddressTypeConfiguration : EntityTypeConfiguration<AddressType>
    {
        public AddressTypeConfiguration()
        {
            ToTable("dbo.AddressType");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.AddressTypeValue).HasColumnName("AddressType").IsRequired().HasMaxLength(100);
        }
    }
}
