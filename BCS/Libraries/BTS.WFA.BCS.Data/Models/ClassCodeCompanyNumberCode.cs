using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ClassCodeCompanyNumberCode
    public class ClassCodeCompanyNumberCode
    {
        public int ClassCodeId { get; set; } // ClassCodeId (Primary key)
        public int CompanyNumberCodeTypeId { get; set; } // CompanyNumberCodeTypeId (Primary key)
        public int CompanyNumberCodeId { get; set; } // CompanyNumberCodeId

        // Foreign keys
        public virtual ClassCode ClassCode { get; set; } //  FK_ClassCodeCompanyNumberCode_ClassCode
        public virtual CompanyNumberCodeType CompanyNumberCodeType { get; set; } //  FK_ClassCodeCompanyNumberCode_CompanyNumberCodeType
        public virtual CompanyNumberCode CompanyNumberCode { get; set; } //  FK_ClassCodeCompanyNumberCode_CompanyNumberCode
    }

    internal class ClassCodeCompanyNumberCodeConfiguration : EntityTypeConfiguration<ClassCodeCompanyNumberCode>
    {
        public ClassCodeCompanyNumberCodeConfiguration()
        {
            ToTable("dbo.ClassCodeCompanyNumberCode");
            HasKey(x => new { x.ClassCodeId, x.CompanyNumberCodeTypeId });

            Property(x => x.ClassCodeId).HasColumnName("ClassCodeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyNumberCodeTypeId).HasColumnName("CompanyNumberCodeTypeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyNumberCodeId).HasColumnName("CompanyNumberCodeId").IsRequired();

            // Foreign keys
            HasRequired(a => a.ClassCode).WithMany(b => b.ClassCodeCompanyNumberCode).HasForeignKey(c => c.ClassCodeId); // FK_ClassCodeCompanyNumberCode_ClassCode
            HasRequired(a => a.CompanyNumberCodeType).WithMany(b => b.ClassCodeCompanyNumberCode).HasForeignKey(c => c.CompanyNumberCodeTypeId); // FK_ClassCodeCompanyNumberCode_CompanyNumberCodeType
            HasRequired(a => a.CompanyNumberCode).WithMany(b => b.ClassCodeCompanyNumberCode).HasForeignKey(c => c.CompanyNumberCodeId); // FK_ClassCodeCompanyNumberCode_CompanyNumberCode
        }
    }
}
