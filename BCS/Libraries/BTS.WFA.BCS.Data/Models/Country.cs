using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // Country
    public class Country
    {
        public int Id { get; set; } // Id (Primary key)
        public string CountryCode { get; set; } // CountryCode
        public string CountryDescription { get; set; } // CountryDescription
        public int Order { get; set; } // Order

        public Country()
        {
            Order = 0;
        }
    }

    internal class CountryConfiguration : EntityTypeConfiguration<Country>
    {
        public CountryConfiguration()
        {
            ToTable("dbo.Country");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CountryCode).HasColumnName("CountryCode").IsRequired().HasMaxLength(50);
            Property(x => x.CountryDescription).HasColumnName("CountryDescription").IsRequired().HasMaxLength(100);
            Property(x => x.Order).HasColumnName("Order").IsRequired();
        }
    }
}
