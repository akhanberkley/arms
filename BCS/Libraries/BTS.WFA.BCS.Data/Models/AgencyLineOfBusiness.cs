using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // AgencyLineOfBusiness
    public class AgencyLineOfBusiness
    {
        public int AgencyId { get; set; } // AgencyId (Primary key)
        public int LineOfBusinessId { get; set; } // LineOfBusinessId (Primary key)
        public long? ApsId { get; set; } // APSId

        // Foreign keys
        public virtual Agency Agency { get; set; } //  FK_AgencyLineOfBusiness_Agency
        public virtual LineOfBusiness LineOfBusiness { get; set; } //  FK_AgencyLineOfBusiness_LineOfBusiness
        public virtual ICollection<AgencyLineOfBusinessPersonUnderwritingRolePerson> AgencyLineOfBusinessPersonUnderwritingRolePerson { get; set; } // Many to many mapping

        public AgencyLineOfBusiness()
        {
            AgencyLineOfBusinessPersonUnderwritingRolePerson = new List<AgencyLineOfBusinessPersonUnderwritingRolePerson>();
        }
    }

    internal class AgencyLineOfBusinessConfiguration : EntityTypeConfiguration<AgencyLineOfBusiness>
    {
        public AgencyLineOfBusinessConfiguration()
        {
            ToTable("dbo.AgencyLineOfBusiness");
            HasKey(x => new { x.AgencyId, x.LineOfBusinessId });

            Property(x => x.AgencyId).HasColumnName("AgencyId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.LineOfBusinessId).HasColumnName("LineOfBusinessId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ApsId).HasColumnName("APSId").IsOptional();

            // Foreign keys
            HasRequired(a => a.Agency).WithMany(b => b.AgencyLineOfBusiness).HasForeignKey(c => c.AgencyId); // FK_AgencyLineOfBusiness_Agency
            HasRequired(a => a.LineOfBusiness).WithMany(b => b.AgencyLineOfBusiness).HasForeignKey(c => c.LineOfBusinessId); // FK_AgencyLineOfBusiness_LineOfBusiness

            HasMany(p => p.AgencyLineOfBusinessPersonUnderwritingRolePerson).WithRequired(c => c.AgencyLineOfBusiness);
        }
    }
}
