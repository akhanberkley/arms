using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionLineOfBusinessChildren
    public class SubmissionLineOfBusinessChildren
    {
        public int Id { get; set; } // Id (Primary key)
        public int SubmissionId { get; set; } // SubmissionId
        public int CompanyNumberLobGridId { get; set; } // CompanyNumberLOBGridId
        public int? SubmissionStatusId { get; set; } // SubmissionStatusId
        public int? SubmissionStatusReasonId { get; set; } // SubmissionStatusReasonId

        // Foreign keys
        public virtual Submission Submission { get; set; } //  FK_SubmissionLineOfBusinessChildren_Submission
        public virtual CompanyNumberLobGrid CompanyNumberLobGrid { get; set; } //  FK_SubmissionLineOfBusinessChildren_CompanyNumberLOBGrid
        public virtual SubmissionStatus SubmissionStatus { get; set; } //  FK_SubmissionLineOfBusinessChildren_SubmissionStatus
        public virtual SubmissionStatusReason SubmissionStatusReason { get; set; } //  FK_SubmissionLineOfBusinessChildren_SubmissionStatusReason
    }

    internal class SubmissionLineOfBusinessChildrenConfiguration : EntityTypeConfiguration<SubmissionLineOfBusinessChildren>
    {
        public SubmissionLineOfBusinessChildrenConfiguration()
        {
            ToTable("dbo.SubmissionLineOfBusinessChildren");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired();
            Property(x => x.CompanyNumberLobGridId).HasColumnName("CompanyNumberLOBGridId").IsRequired();
            Property(x => x.SubmissionStatusId).HasColumnName("SubmissionStatusId").IsOptional();
            Property(x => x.SubmissionStatusReasonId).HasColumnName("SubmissionStatusReasonId").IsOptional();

            // Foreign keys
            HasRequired(a => a.Submission).WithMany(b => b.SubmissionLineOfBusinessChildren).HasForeignKey(c => c.SubmissionId); // FK_SubmissionLineOfBusinessChildren_Submission
            HasRequired(a => a.CompanyNumberLobGrid).WithMany(b => b.SubmissionLineOfBusinessChildren).HasForeignKey(c => c.CompanyNumberLobGridId); // FK_SubmissionLineOfBusinessChildren_CompanyNumberLOBGrid
            HasOptional(a => a.SubmissionStatus).WithMany(b => b.SubmissionLineOfBusinessChildren).HasForeignKey(c => c.SubmissionStatusId); // FK_SubmissionLineOfBusinessChildren_SubmissionStatus
            HasOptional(a => a.SubmissionStatusReason).WithMany(b => b.SubmissionLineOfBusinessChildren).HasForeignKey(c => c.SubmissionStatusReasonId); // FK_SubmissionLineOfBusinessChildren_SubmissionStatusReason
        }
    }
}
