using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionNumberControl
    public class SubmissionNumberControl
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public long ControlNumber { get; set; } // ControlNumber

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_SubmissionNumberControl_CompanyNumber
    }

    internal class SubmissionNumberControlConfiguration : EntityTypeConfiguration<SubmissionNumberControl>
    {
        public SubmissionNumberControlConfiguration()
        {
            ToTable("dbo.SubmissionNumberControl");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.ControlNumber).HasColumnName("ControlNumber").IsRequired().IsConcurrencyToken();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.SubmissionNumberControl).HasForeignKey(c => c.CompanyNumberId); // FK_SubmissionNumberControl_CompanyNumber
        }
    }
}
