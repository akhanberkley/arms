using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionHistory
    public class SubmissionHistory
    {
        public int Id { get; set; } // Id (Primary key)
        public int SubmissionId { get; set; } // SubmissionId
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public int SubmissionTypeId { get; set; } // SubmissionTypeId
        public int? SubmissionTokenId { get; set; } // SubmissionTokenId
        public long ClientCoreClientId { get; set; } // ClientCoreClientId
        public long? ClientCorePortfolioId { get; set; } // ClientCorePortfolioId
        public int AgencyId { get; set; } // AgencyId
        public int? AgentId { get; set; } // AgentId
        public int? ContactId { get; set; } // ContactId
        public int? SubmissionStatusId { get; set; } // SubmissionStatusId
        public DateTime? SubmissionStatusDate { get; set; } // SubmissionStatusDate
        public DateTime? CancellationDate { get; set; } // CancellationDate
        public int? SubmissionStatusReasonId { get; set; } // SubmissionStatusReasonId
        public string SubmissionStatusReasonOther { get; set; } // SubmissionStatusReasonOther
        public int? UnderwriterPersonId { get; set; } // UnderwriterPersonId
        public int? AnalystPersonId { get; set; } // AnalystPersonId
        public int? PolicySystemId { get; set; } // PolicySystemId
        public long SubmissionNumber { get; set; } // SubmissionNumber
        public int Sequence { get; set; } // Sequence
        public string PolicyNumber { get; set; } // PolicyNumber
        public decimal? EstimatedWrittenPremium { get; set; } // EstimatedWrittenPremium
        public DateTime? EffectiveDate { get; set; } // EffectiveDate
        public DateTime? ExpirationDate { get; set; } // ExpirationDate
        public string InsuredName { get; set; } // InsuredName
        public string Dba { get; set; } // Dba
        public string Comments { get; set; } // Comments
        public int? OtherCarrierId { get; set; } // OtherCarrierId
        public decimal? OtherCarrierPremium { get; set; } // OtherCarrierPremium
        public string SubmissionBy { get; set; } // SubmissionBy
        public DateTime SubmissionDt { get; set; } // SubmissionDt
        public string UpdatedBy { get; set; } // UpdatedBy
        public DateTime? UpdatedDt { get; set; } // UpdatedDt
        public DateTime? HistoryDt { get; set; } // HistoryDt

        // Foreign keys
        public virtual Submission Submission { get; set; } //  FK_SubmissionHistory_Submission
    }

    internal class SubmissionHistoryConfiguration : EntityTypeConfiguration<SubmissionHistory>
    {
        public SubmissionHistoryConfiguration()
        {
            ToTable("dbo.SubmissionHistory");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.SubmissionId).HasColumnName("SubmissionId").IsRequired();
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.SubmissionTypeId).HasColumnName("SubmissionTypeId").IsRequired();
            Property(x => x.SubmissionTokenId).HasColumnName("SubmissionTokenId").IsOptional();
            Property(x => x.ClientCoreClientId).HasColumnName("ClientCoreClientId").IsRequired();
            Property(x => x.ClientCorePortfolioId).HasColumnName("ClientCorePortfolioId").IsOptional();
            Property(x => x.AgencyId).HasColumnName("AgencyId").IsRequired();
            Property(x => x.AgentId).HasColumnName("AgentId").IsOptional();
            Property(x => x.ContactId).HasColumnName("ContactId").IsOptional();
            Property(x => x.SubmissionStatusId).HasColumnName("SubmissionStatusId").IsOptional();
            Property(x => x.SubmissionStatusDate).HasColumnName("SubmissionStatusDate").IsOptional();
            Property(x => x.CancellationDate).HasColumnName("CancellationDate").IsOptional();
            Property(x => x.SubmissionStatusReasonId).HasColumnName("SubmissionStatusReasonId").IsOptional();
            Property(x => x.SubmissionStatusReasonOther).HasColumnName("SubmissionStatusReasonOther").IsOptional().HasMaxLength(255);
            Property(x => x.UnderwriterPersonId).HasColumnName("UnderwriterPersonId").IsOptional();
            Property(x => x.AnalystPersonId).HasColumnName("AnalystPersonId").IsOptional();
            Property(x => x.PolicySystemId).HasColumnName("PolicySystemId").IsOptional();
            Property(x => x.SubmissionNumber).HasColumnName("SubmissionNumber").IsRequired();
            Property(x => x.Sequence).HasColumnName("Sequence").IsRequired();
            Property(x => x.PolicyNumber).HasColumnName("PolicyNumber").IsOptional().HasMaxLength(50);
            Property(x => x.EstimatedWrittenPremium).HasColumnName("EstimatedWrittenPremium").IsOptional().HasPrecision(19, 4);
            Property(x => x.EffectiveDate).HasColumnName("EffectiveDate").IsOptional();
            Property(x => x.ExpirationDate).HasColumnName("ExpirationDate").IsOptional();
            Property(x => x.InsuredName).HasColumnName("InsuredName").IsOptional().HasMaxLength(255);
            Property(x => x.Dba).HasColumnName("Dba").IsOptional().HasMaxLength(255);
            Property(x => x.Comments).HasColumnName("Comments").IsOptional().HasMaxLength(8000);
            Property(x => x.OtherCarrierId).HasColumnName("OtherCarrierId").IsOptional();
            Property(x => x.OtherCarrierPremium).HasColumnName("OtherCarrierPremium").IsOptional().HasPrecision(19, 4);
            Property(x => x.SubmissionBy).HasColumnName("SubmissionBy").IsRequired().HasMaxLength(50);
            Property(x => x.SubmissionDt).HasColumnName("SubmissionDt").IsRequired();
            Property(x => x.UpdatedBy).HasColumnName("UpdatedBy").IsOptional().HasMaxLength(50);
            Property(x => x.UpdatedDt).HasColumnName("UpdatedDt").IsOptional();
            Property(x => x.HistoryDt).HasColumnName("HistoryDt").IsOptional();

            // Foreign keys
            HasRequired(a => a.Submission).WithMany(b => b.SubmissionHistory).HasForeignKey(c => c.SubmissionId); // FK_SubmissionHistory_Submission
        }
    }
}
