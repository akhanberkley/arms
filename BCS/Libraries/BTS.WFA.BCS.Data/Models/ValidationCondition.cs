using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ValidationCondition
    public class ValidationCondition
    {
        public int Id { get; set; } // ID (Primary key)
        public int ValidationId { get; set; } // ValidationID
        public int? FieldId { get; set; } // FieldID
        public string OperatorCode { get; set; } // OperatorCode
        public string CompareValue { get; set; } // CompareValue
        public int DisplayOrder { get; set; } // DisplayOrder
        public bool OnLoad { get; set; } // OnLoad
        public bool OnEdit { get; set; } // OnEdit
        public DateTime DateCreated { get; set; } // DateCreated
        public DateTime? DateModified { get; set; } // DateModified
        public int? ModifiedBy { get; set; } // ModifiedBy

        // Foreign keys
        public virtual Company Company { get; set; } //  FK_ValidationCondition_Company
        public virtual Validation Validation { get; set; } //  FK_ValidationCondition_ValidationCondition
        public virtual ValidationField ValidationField { get; set; } //  FK_ValidationCondition_ValidationField
        public virtual ValidationOperator ValidationOperator { get; set; } //  FK_ValidationCondition_ValidationOperator

        public ValidationCondition()
        {
            DisplayOrder = 1;
            OnLoad = false;
            OnEdit = false;
            DateCreated = System.DateTime.Now;
        }
    }

    internal class ValidationConditionConfiguration : EntityTypeConfiguration<ValidationCondition>
    {
        public ValidationConditionConfiguration()
        {
            ToTable("dbo.ValidationCondition");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ValidationId).HasColumnName("ValidationID").IsRequired();
            Property(x => x.FieldId).HasColumnName("FieldID").IsOptional();
            Property(x => x.OperatorCode).HasColumnName("OperatorCode").IsRequired().HasMaxLength(2);
            Property(x => x.CompareValue).HasColumnName("CompareValue").IsRequired().HasMaxLength(100);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.OnLoad).HasColumnName("OnLoad").IsRequired();
            Property(x => x.OnEdit).HasColumnName("OnEdit").IsRequired();
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsRequired();
            Property(x => x.DateModified).HasColumnName("DateModified").IsOptional();
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional();

            // Foreign keys
            HasRequired(a => a.Company).WithOptional(b => b.ValidationCondition); // FK_ValidationCondition_Company
            HasRequired(a => a.Validation).WithMany(b => b.ValidationCondition).HasForeignKey(c => c.ValidationId); // FK_ValidationCondition_ValidationCondition
            HasOptional(a => a.ValidationField).WithMany(b => b.ValidationCondition).HasForeignKey(c => c.FieldId); // FK_ValidationCondition_ValidationField
            HasRequired(a => a.ValidationOperator).WithMany(b => b.ValidationCondition).HasForeignKey(c => c.OperatorCode); // FK_ValidationCondition_ValidationOperator
        }
    }
}
