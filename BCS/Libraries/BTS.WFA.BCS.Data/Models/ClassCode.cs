using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ClassCode
    public class ClassCode
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public int? SicCodeId { get; set; } // SicCodeId
        public string CodeValue { get; set; } // CodeValue
        public string GLCodeValue { get; set; }
        public string Description { get; set; } // Description
        public int? NaicsCodeId { get; set; } // NaicsCodeId

        // Reverse navigation
        public virtual ICollection<ClassCodeCompanyNumberCode> ClassCodeCompanyNumberCode { get; set; } // Many to many mapping
        public virtual ICollection<CompanyNumberClassCodeHazardGradeValue> CompanyNumberClassCodeHazardGradeValue { get; set; } // Many to many mapping

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_ClassCode_CompanyNumber
        public virtual SicCodeList SicCodeList { get; set; } //  FK_ClassCode_SicCodeList
        public virtual NaicsCodeList NaicsCodeList { get; set; }

        public ClassCode()
        {
            ClassCodeCompanyNumberCode = new List<ClassCodeCompanyNumberCode>();
            CompanyNumberClassCodeHazardGradeValue = new List<CompanyNumberClassCodeHazardGradeValue>();
        }
    }

    internal class ClassCodeConfiguration : EntityTypeConfiguration<ClassCode>
    {
        public ClassCodeConfiguration()
        {
            ToTable("dbo.ClassCode");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.SicCodeId).HasColumnName("SicCodeId").IsOptional();
            Property(x => x.CodeValue).HasColumnName("CodeValue").IsRequired().HasMaxLength(50);
            Property(x => x.GLCodeValue).HasColumnName("GLCodeValue").IsOptional().HasMaxLength(50);
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(255);
            Property(x => x.NaicsCodeId).HasColumnName("NaicsCodeId").IsOptional();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.ClassCode).HasForeignKey(c => c.CompanyNumberId); // FK_ClassCode_CompanyNumber
            HasOptional(a => a.SicCodeList).WithMany(b => b.ClassCode).HasForeignKey(c => c.SicCodeId); // FK_ClassCode_SicCodeList
            HasOptional(a => a.NaicsCodeList).WithMany().HasForeignKey(c => c.NaicsCodeId);
        }
    }
}
