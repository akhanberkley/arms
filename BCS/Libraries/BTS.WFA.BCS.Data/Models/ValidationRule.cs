using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ValidationRule
    public class ValidationRule
    {
        public int Id { get; set; } // ID (Primary key)
        public int ValidationId { get; set; } // ValidationID
        public int? FieldId { get; set; } // FieldID
        public bool? TrueCondition { get; set; } // TrueCondition
        public bool? DisplayControl { get; set; } // DisplayControl
        public bool? EnableControl { get; set; } // EnableControl
        public bool? SetNewValue { get; set; } // SetNewValue
        public string NewValue { get; set; } // NewValue
        public int DisplayOrder { get; set; } // DisplayOrder
        public DateTime DateCreated { get; set; } // DateCreated
        public DateTime? DateModified { get; set; } // DateModified
        public int? ModifiedBy { get; set; } // ModifiedBy

        // Reverse navigation
        public virtual ICollection<ValidationRuleValue> ValidationRuleValue { get; set; } // ValidationRuleValue.FK_ValidationRuleValue_ValidationRule

        // Foreign keys
        public virtual Validation Validation { get; set; } //  FK_ValidationRule_ValidationCondition
        public virtual ValidationField ValidationField { get; set; } //  FK_ValidationRule_ValidationField1

        public ValidationRule()
        {
            TrueCondition = true;
            DisplayControl = true;
            EnableControl = true;
            SetNewValue = false;
            DisplayOrder = 1;
            DateCreated = System.DateTime.Now;
            ValidationRuleValue = new List<ValidationRuleValue>();
        }
    }

    internal class ValidationRuleConfiguration : EntityTypeConfiguration<ValidationRule>
    {
        public ValidationRuleConfiguration()
        {
            ToTable("dbo.ValidationRule");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ValidationId).HasColumnName("ValidationID").IsRequired();
            Property(x => x.FieldId).HasColumnName("FieldID").IsOptional();
            Property(x => x.TrueCondition).HasColumnName("TrueCondition").IsOptional();
            Property(x => x.DisplayControl).HasColumnName("DisplayControl").IsOptional();
            Property(x => x.EnableControl).HasColumnName("EnableControl").IsOptional();
            Property(x => x.SetNewValue).HasColumnName("SetNewValue").IsOptional();
            Property(x => x.NewValue).HasColumnName("NewValue").IsOptional().HasMaxLength(100);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsRequired();
            Property(x => x.DateModified).HasColumnName("DateModified").IsOptional();
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional();

            // Foreign keys
            HasRequired(a => a.Validation).WithMany(b => b.ValidationRule).HasForeignKey(c => c.ValidationId); // FK_ValidationRule_ValidationCondition
            HasOptional(a => a.ValidationField).WithMany(b => b.ValidationRule).HasForeignKey(c => c.FieldId); // FK_ValidationRule_ValidationField1
        }
    }
}
