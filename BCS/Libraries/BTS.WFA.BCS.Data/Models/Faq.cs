using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // FAQ
    public class Faq
    {
        public int Id { get; set; } // Id (Primary key)
        public int CompanyNumberId { get; set; } // CompanyNumberId
        public string Question { get; set; } // Question
        public string Answer { get; set; } // Answer
        public bool DisplayUsersWithAccess { get; set; } // DisplayUsersWithAccess
        public bool Active { get; set; } // Active
        public DateTime DateCreated { get; set; } // DateCreated
        public DateTime? DateModified { get; set; } // DateModified

        // Reverse navigation
        public virtual ICollection<Role> Role { get; set; } // Many to many mapping

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_FAQ_CompanyNumber

        public Faq()
        {
            DisplayUsersWithAccess = true;
            Active = true;
            DateCreated = System.DateTime.Now;
            Role = new List<Role>();
        }
    }

    internal class FaqConfiguration : EntityTypeConfiguration<Faq>
    {
        public FaqConfiguration()
        {
            ToTable("dbo.FAQ");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.Question).HasColumnName("Question").IsRequired().HasMaxLength(500);
            Property(x => x.Answer).HasColumnName("Answer").IsRequired().HasMaxLength(1000);
            Property(x => x.DisplayUsersWithAccess).HasColumnName("DisplayUsersWithAccess").IsRequired();
            Property(x => x.Active).HasColumnName("Active").IsRequired();
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsRequired();
            Property(x => x.DateModified).HasColumnName("DateModified").IsOptional();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.Faq).HasForeignKey(c => c.CompanyNumberId); // FK_FAQ_CompanyNumber
            HasMany(t => t.Role).WithMany(t => t.Faq).Map(m =>
            {
                m.ToTable("FAQRole");
                m.MapLeftKey("FAQId");
                m.MapRightKey("RoleId");
            });
        }
    }
}
