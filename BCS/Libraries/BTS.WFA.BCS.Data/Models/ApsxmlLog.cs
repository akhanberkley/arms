using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // APSXMLLog
    public class ApsxmlLog
    {
        public int Id { get; set; } // Id (Primary key)
        public string CompanyNumber { get; set; } // CompanyNumber
        public bool Errored { get; set; } // Errored
        public string Xml { get; set; } // XML
        public DateTime? DateCreated { get; set; } // DateCreated

        public ApsxmlLog()
        {
            Errored = false;
            DateCreated = System.DateTime.Now;
        }
    }

    internal class ApsxmlLogConfiguration : EntityTypeConfiguration<ApsxmlLog>
    {
        public ApsxmlLogConfiguration()
        {
            ToTable("dbo.APSXMLLog");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyNumber).HasColumnName("CompanyNumber").IsOptional().HasMaxLength(10);
            Property(x => x.Errored).HasColumnName("Errored").IsRequired();
            Property(x => x.Xml).HasColumnName("XML").IsOptional();
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsOptional();
        }
    }
}
