using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // SubmissionStatus
    public class SubmissionStatus
    {
        public int Id { get; set; } // Id (Primary key)
        public string StatusCode { get; set; } // StatusCode

        // Reverse navigation
        public virtual ICollection<CompanyNumberSubmissionStatus> CompanyNumberSubmissionStatus { get; set; } // Many to many mapping
        public virtual ICollection<SubmissionLineOfBusinessChildren> SubmissionLineOfBusinessChildren { get; set; } // SubmissionLineOfBusinessChildren.FK_SubmissionLineOfBusinessChildren_SubmissionStatus
        public virtual ICollection<SubmissionStatusHistory> SubmissionStatusHistory { get; set; } // SubmissionStatusHistory.FK_SubmissionStatusHistory_SubmissionStatus
        public virtual ICollection<SubmissionStatusSubmissionStatusReason> SubmissionStatusSubmissionStatusReason { get; set; } // Many to many mapping

        public SubmissionStatus()
        {
            CompanyNumberSubmissionStatus = new List<CompanyNumberSubmissionStatus>();
            SubmissionLineOfBusinessChildren = new List<SubmissionLineOfBusinessChildren>();
            SubmissionStatusHistory = new List<SubmissionStatusHistory>();
            SubmissionStatusSubmissionStatusReason = new List<SubmissionStatusSubmissionStatusReason>();
        }
    }

    internal class SubmissionStatusConfiguration : EntityTypeConfiguration<SubmissionStatus>
    {
        public SubmissionStatusConfiguration()
        {
            ToTable("dbo.SubmissionStatus");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.StatusCode).HasColumnName("StatusCode").IsRequired().HasMaxLength(100);
        }
    }
}
