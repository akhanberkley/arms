using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // CompanyNumberSubmissionType
    public class CompanyNumberSubmissionType
    {
        public int CompanyNumberId { get; set; } // CompanyNumberId (Primary key)
        public int SubmissionTypeId { get; set; } // SubmissionTypeId (Primary key)
        public int Order { get; set; } // Order
        public bool DefaultSelection { get; set; } // DefaultSelection
        public Guid SummarySubmissionFieldId { get; set; }

        // Foreign keys
        public virtual CompanyNumber CompanyNumber { get; set; } //  FK_CompanyNumberSubmissionType_CompanyNumber
        public virtual SubmissionType SubmissionType { get; set; } //  FK_CompanyNumberSubmissionType_SubmissionType
        public virtual CompanyNumberSubmissionField SummarySubmissionField { get; set; }
        public virtual ICollection<CompanyNumberSubmissionFieldSubmissionType> CompanyNumberSubmissionFieldSubmissionType { get; set; } // Many to many mapping

        public CompanyNumberSubmissionType()
        {
            Order = 0;
            DefaultSelection = false;
        }
    }

    internal class CompanyNumberSubmissionTypeConfiguration : EntityTypeConfiguration<CompanyNumberSubmissionType>
    {
        public CompanyNumberSubmissionTypeConfiguration()
        {
            ToTable("dbo.CompanyNumberSubmissionType");
            HasKey(x => new { x.CompanyNumberId, x.SubmissionTypeId });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SubmissionTypeId).HasColumnName("SubmissionTypeId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Order).HasColumnName("Order").IsRequired();
            Property(x => x.DefaultSelection).HasColumnName("DefaultSelection").IsRequired();
            Property(x => x.SummarySubmissionFieldId).HasColumnName("SummarySubmissionFieldId").IsOptional();

            // Foreign keys
            HasRequired(a => a.CompanyNumber).WithMany(b => b.CompanyNumberSubmissionType).HasForeignKey(c => c.CompanyNumberId); // FK_CompanyNumberSubmissionType_CompanyNumber
            HasRequired(a => a.SubmissionType).WithMany(b => b.CompanyNumberSubmissionType).HasForeignKey(c => c.SubmissionTypeId); // FK_CompanyNumberSubmissionType_SubmissionType
            HasOptional(a => a.SummarySubmissionField).WithMany().HasForeignKey(c => new { c.CompanyNumberId, c.SummarySubmissionFieldId });
            HasMany(p => p.CompanyNumberSubmissionFieldSubmissionType).WithRequired(c => c.CompanyNumberSubmissionType);
        }
    }
}
