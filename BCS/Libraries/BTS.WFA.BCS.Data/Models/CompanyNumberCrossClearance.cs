using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.BCS.Data.Models
{
    // ClassCodeCompanyNumberCode
    public class CompanyNumberCrossClearance
    {
        public int CompanyNumberId { get; set; }
        public int ClearsCompanyNumberId { get; set; }

        public virtual CompanyNumber CompanyNumber { get; set; }
        public virtual CompanyNumber ClearsCompanyNumber { get; set; }
    }

    internal class CompanyNumberCrossClearanceConfiguration : EntityTypeConfiguration<CompanyNumberCrossClearance>
    {
        public CompanyNumberCrossClearanceConfiguration()
        {
            ToTable("dbo.CompanyNumberCrossClearance");
            HasKey(x => new { x.CompanyNumberId, x.ClearsCompanyNumberId });

            Property(x => x.CompanyNumberId).HasColumnName("CompanyNumberId").IsRequired();
            Property(x => x.ClearsCompanyNumberId).HasColumnName("ClearsCompanyNumberId").IsRequired();

            HasRequired(a => a.CompanyNumber).WithMany().HasForeignKey(c => c.CompanyNumberId);
            HasRequired(a => a.ClearsCompanyNumber).WithMany().HasForeignKey(c => c.ClearsCompanyNumberId);
        }
    }
}
