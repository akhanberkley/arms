using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Reflection;
using BTS.WFA.BCS.Data.Models;
using System.Linq;
using System.Configuration;

namespace BTS.WFA.BCS.Data
{
    public class SqlDb : BTS.WFA.Data.BaseDbContext, IClearanceDb
    {
        public DbSet<AddressType> AddressType { get; set; } // AddressType
        public DbSet<Agency> Agency { get; set; } // Agency
        public DbSet<AgencyBranch> AgencyBranch { get; set; } // AgencyBranch
        public DbSet<AgencyLicensedState> AgencyLicensedState { get; set; } // AgencyLicensedState
        public DbSet<AgencyLineOfBusiness> AgencyLineOfBusiness { get; set; } // AgencyLineOfBusiness
        public DbSet<AgencyLineOfBusinessPersonUnderwritingRolePerson> AgencyLineOfBusinessPersonUnderwritingRolePerson { get; set; } // AgencyLineOfBusinessPersonUnderwritingRolePerson
        public DbSet<Agent> Agent { get; set; } // Agent
        public DbSet<AgentUnspecifiedReason> AgentUnspecifiedReason { get; set; } // AgentUnspecifiedReason
        public DbSet<ApsxmlLog> ApsxmlLog { get; set; } // APSXMLLog
        public DbSet<APSSync> ApsSync { get; set; } // APSSync
        public DbSet<AttributeDataType> AttributeDataType { get; set; } // AttributeDataType
        public DbSet<ClassCode> ClassCode { get; set; } // ClassCode
        public DbSet<ClassCodeCompanyNumberCode> ClassCodeCompanyNumberCode { get; set; } // ClassCodeCompanyNumberCode
        public DbSet<Client> Client { get; set; } // Client
        public DbSet<ClientClassCode> ClientClassCode { get; set; } // ClientClassCode
        public DbSet<ClearSync> ClearSync { get; set; } // ClearSync
        public DbSet<ClientClassCodeHazardGradeValue> ClientClassCodeHazardGradeValue { get; set; } // ClientClassCodeHazardGradeValue
        public DbSet<ClientCompanyNumberAttributeValue> ClientCompanyNumberAttributeValue { get; set; } // ClientCompanyNumberAttributeValue
        public DbSet<ClientCoreAddressAddressType> ClientCoreAddressAddressType { get; set; } // ClientCoreAddressAddressType
        public DbSet<ClientCoreNameClientNameType> ClientCoreNameClientNameType { get; set; } // ClientCoreNameClientNameType
        public DbSet<ClientNameType> ClientNameType { get; set; } // ClientNameType
        public DbSet<ClientSystemType> ClientSystemType { get; set; } // ClientSystemType
        public DbSet<CobraAddressCounty> CobraAddressCounty { get; set; } // CobraAddressCounty
        public DbSet<Company> Company { get; set; } // Company
        public DbSet<CompanyClientAdvancedSearch> CompanyClientAdvancedSearch { get; set; } // CompanyClientAdvancedSearch
        public DbSet<CompanyNumber> CompanyNumber { get; set; } // CompanyNumber
        public DbSet<CompanyNumberAgentUnspecifiedReason> CompanyNumberAgentUnspecifiedReason { get; set; } // CompanyNumberAgentUnspecifiedReason
        public DbSet<CompanyNumberAttribute> CompanyNumberAttribute { get; set; } // CompanyNumberAttribute
        public DbSet<CompanyNumberClassCodeHazardGradeValue> CompanyNumberClassCodeHazardGradeValue { get; set; } // CompanyNumberClassCodeHazardGradeValue
        public DbSet<CompanyNumberCode> CompanyNumberCode { get; set; } // CompanyNumberCode
        public DbSet<CompanyNumberCodeType> CompanyNumberCodeType { get; set; } // CompanyNumberCodeType
        public DbSet<CompanyNumberLobGrid> CompanyNumberLobGrid { get; set; } // CompanyNumberLOBGrid
        public DbSet<CompanyNumberOtherCarrier> CompanyNumberOtherCarrier { get; set; } // CompanyNumberOtherCarrier
        public DbSet<CompanyNumberSubmissionStatus> CompanyNumberSubmissionStatus { get; set; } // CompanyNumberSubmissionStatus
        public DbSet<CompanyNumberSubmissionType> CompanyNumberSubmissionType { get; set; } // CompanyNumberSubmissionType
        public DbSet<CompanyParameter> CompanyParameter { get; set; } // CompanyParameter
        public DbSet<Contact> Contact { get; set; } // Contact
        public DbSet<Country> Country { get; set; } // Country
        public DbSet<DssException> DssException { get; set; } // DSSException
        public DbSet<ExceptionEmailFilterString> ExceptionEmailFilterString { get; set; } // ExceptionEmailFilterString
        public DbSet<ExceptionLog> ExceptionLog { get; set; } // ExceptionLog
        public DbSet<ExceptionReturnMessage> ExceptionReturnMessage { get; set; } // ExceptionReturnMessage
        public DbSet<Faq> Faq { get; set; } // FAQ
        public DbSet<GeographicDirectional> GeographicDirectional { get; set; } // GeographicDirectional
        public DbSet<HazardGradeType> HazardGradeType { get; set; } // HazardGradeType
        public DbSet<LineOfBusiness> LineOfBusiness { get; set; } // LineOfBusiness
        public DbSet<Link> Link { get; set; } // Link
        public DbSet<NaicsCodeList> NaicsCodeList { get; set; } // NaicsCodeList
        public DbSet<NaicsDivision> NaicsDivision { get; set; } // NaicsDivision
        public DbSet<NaicsGroup> NaicsGroup { get; set; } // NaicsGroup
        public DbSet<NamePrefix> NamePrefix { get; set; } // NamePrefix
        public DbSet<NameSuffixTitle> NameSuffixTitle { get; set; } // NameSuffixTitle
        public DbSet<Notification> Notification { get; set; } // Notification
        public DbSet<NumberControl> NumberControl { get; set; } // NumberControl
        public DbSet<NumberControlType> NumberControlType { get; set; } // NumberControlType
        public DbSet<OtherCarrier> OtherCarrier { get; set; } // OtherCarrier
        public DbSet<Person> Person { get; set; } // Person
        public DbSet<PolicySystem> PolicySystem { get; set; } // PolicySystem
        public DbSet<Role> Role { get; set; } // Role
        public DbSet<SecondaryUnitDesignator> SecondaryUnitDesignator { get; set; } // SecondaryUnitDesignator
        public DbSet<SicCodeList> SicCodeList { get; set; } // SicCodeList
        public DbSet<SicDivision> SicDivision { get; set; } // SicDivision
        public DbSet<SicGroup> SicGroup { get; set; } // SicGroup
        public DbSet<State> State { get; set; } // State
        public DbSet<StreetSuffix> StreetSuffix { get; set; } // StreetSuffix
        public DbSet<Submission> Submission { get; set; } // Submission
        public DbSet<SubmissionClientCoreClientAddress> SubmissionClientCoreClientAddress { get; set; } // SubmissionClientCoreClientAddress
        public DbSet<SubmissionClientCoreClientName> SubmissionClientCoreClientName { get; set; } // SubmissionClientCoreClientName
        public DbSet<SubmissionComment> SubmissionComment { get; set; } // SubmissionComment
        public DbSet<SubmissionCompanyNumberAttributeValue> SubmissionCompanyNumberAttributeValue { get; set; } // SubmissionCompanyNumberAttributeValue
        public DbSet<SubmissionCompanyNumberAttributeValueHistory> SubmissionCompanyNumberAttributeValueHistory { get; set; } // SubmissionCompanyNumberAttributeValueHistory
        public DbSet<SubmissionCompanyNumberCodeHistory> SubmissionCompanyNumberCodeHistory { get; set; } // SubmissionCompanyNumberCodeHistory
        public DbSet<SubmissionCopyExclusion> SubmissionCopyExclusion { get; set; } // SubmissionCopyExclusion
        public DbSet<SubmissionDisplayGrid> SubmissionDisplayGrid { get; set; } // SubmissionDisplayGrid
        public DbSet<SubmissionHistory> SubmissionHistory { get; set; } // SubmissionHistory
        public DbSet<SubmissionLineOfBusinessChildren> SubmissionLineOfBusinessChildren { get; set; } // SubmissionLineOfBusinessChildren
        public DbSet<SubmissionNumberControl> SubmissionNumberControl { get; set; } // SubmissionNumberControl
        public DbSet<SubmissionPage> SubmissionPage { get; set; } // SubmissionPage
        public DbSet<SubmissionSearchPage> SubmissionSearchPage { get; set; } // SubmissionSearchPage
        public DbSet<SubmissionStatus> SubmissionStatus { get; set; } // SubmissionStatus
        public DbSet<SubmissionStatusHistory> SubmissionStatusHistory { get; set; } // SubmissionStatusHistory
        public DbSet<SubmissionStatusReason> SubmissionStatusReason { get; set; } // SubmissionStatusReason
        public DbSet<SubmissionStatusSubmissionStatusReason> SubmissionStatusSubmissionStatusReason { get; set; } // SubmissionStatusSubmissionStatusReason
        public DbSet<SubmissionToken> SubmissionToken { get; set; } // SubmissionToken
        public DbSet<SubmissionType> SubmissionType { get; set; } // SubmissionType
        public DbSet<UnderwritingRole> UnderwritingRole { get; set; } // UnderwritingRole
        public DbSet<User> User { get; set; } // User
        public DbSet<Validation> Validation { get; set; } // Validation
        public DbSet<ValidationCondition> ValidationCondition { get; set; } // ValidationCondition
        public DbSet<ValidationField> ValidationField { get; set; } // ValidationField
        public DbSet<ValidationOperator> ValidationOperator { get; set; } // ValidationOperator
        public DbSet<ValidationRule> ValidationRule { get; set; } // ValidationRule
        public DbSet<ValidationRuleValue> ValidationRuleValue { get; set; } // ValidationRuleValue
        public DbSet<SubmissionField> SubmissionField { get; set; }
        public DbSet<SubmissionViewType> SubmissionViewType { get; set; }
        public DbSet<SubmissionViewField> SubmissionViewField { get; set; }
        public DbSet<ImportSubmission> ImportSubmission { get; set; }
        public DbSet<PolicySymbol> PolicySymbol { get; set; }
        public DbSet<ClientField> ClientField { get; set; }
        public DbSet<CompanyNumberClientField> CompanyNumberClientField { get; set; }
        public DbSet<SubmissionAddress> SubmissionAddress { get; set; }
        public DbSet<SubmissionAdditionalName> SubmissionAdditionalName { get; set; }
        public DbSet<UserType> UserType { get; set; }
        public DbSet<CompanyNumberSubmissionField> CompanyNumberSubmissionField { get; set; }
        public DbSet<CompanyNumberClientCoreSegment> CompanyNumberClientCoreSegment { get; set; }
        public DbSet<CompanyNumberCrossClearance> CompanyNumberCrossClearance { get; set; }

        static SqlDb()
        {
            Database.SetInitializer<SqlDb>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
