﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using BTS.WFA.BCS.Data.Models;
using System.Reflection;
using BTS.WFA.Data;

namespace BTS.WFA.BCS.Data
{
    public class MockDb : BTS.WFA.Data.BaseMockContext, IClearanceDb
    {
        static MockDb()
        {
            LoadStandardDomainData();
        }

        public DbSet<AddressType> AddressType { get; set; } // AddressType
        public DbSet<Agency> Agency { get; set; } // Agency
        public DbSet<AgencyBranch> AgencyBranch { get; set; } // AgencyBranch
        public DbSet<AgencyLicensedState> AgencyLicensedState { get; set; } // AgencyLicensedState
        public DbSet<AgencyLineOfBusiness> AgencyLineOfBusiness { get; set; } // AgencyLineOfBusiness
        public DbSet<AgencyLineOfBusinessPersonUnderwritingRolePerson> AgencyLineOfBusinessPersonUnderwritingRolePerson { get; set; } // AgencyLineOfBusinessPersonUnderwritingRolePerson
        public DbSet<Agent> Agent { get; set; } // Agent
        public DbSet<AgentUnspecifiedReason> AgentUnspecifiedReason { get; set; } // AgentUnspecifiedReason
        public DbSet<ApsxmlLog> ApsxmlLog { get; set; } // APSXMLLog
        public DbSet<APSSync> ApsSync { get; set; } // APSSync
        public DbSet<AttributeDataType> AttributeDataType { get; set; } // AttributeDataType
        public DbSet<ClassCode> ClassCode { get; set; } // ClassCode
        public DbSet<ClassCodeCompanyNumberCode> ClassCodeCompanyNumberCode { get; set; } // ClassCodeCompanyNumberCode
        public DbSet<ClearSync> ClearSync { get; set; } // ClearSync
        public DbSet<Client> Client { get; set; } // Client
        public DbSet<ClientClassCode> ClientClassCode { get; set; } // ClientClassCode
        public DbSet<ClientClassCodeHazardGradeValue> ClientClassCodeHazardGradeValue { get; set; } // ClientClassCodeHazardGradeValue
        public DbSet<ClientCompanyNumberAttributeValue> ClientCompanyNumberAttributeValue { get; set; } // ClientCompanyNumberAttributeValue
        public DbSet<ClientCoreAddressAddressType> ClientCoreAddressAddressType { get; set; } // ClientCoreAddressAddressType
        public DbSet<ClientCoreNameClientNameType> ClientCoreNameClientNameType { get; set; } // ClientCoreNameClientNameType
        public DbSet<ClientNameType> ClientNameType { get; set; } // ClientNameType
        public DbSet<ClientSystemType> ClientSystemType { get; set; } // ClientSystemType
        public DbSet<CobraAddressCounty> CobraAddressCounty { get; set; } // CobraAddressCounty
        public DbSet<Company> Company { get; set; } // Company
        public DbSet<CompanyClientAdvancedSearch> CompanyClientAdvancedSearch { get; set; } // CompanyClientAdvancedSearch
        public DbSet<CompanyNumber> CompanyNumber { get; set; } // CompanyNumber
        public DbSet<CompanyNumberAgentUnspecifiedReason> CompanyNumberAgentUnspecifiedReason { get; set; } // CompanyNumberAgentUnspecifiedReason
        public DbSet<CompanyNumberAttribute> CompanyNumberAttribute { get; set; } // CompanyNumberAttribute
        public DbSet<CompanyNumberClassCodeHazardGradeValue> CompanyNumberClassCodeHazardGradeValue { get; set; } // CompanyNumberClassCodeHazardGradeValue
        public DbSet<CompanyNumberCode> CompanyNumberCode { get; set; } // CompanyNumberCode
        public DbSet<CompanyNumberCodeType> CompanyNumberCodeType { get; set; } // CompanyNumberCodeType
        public DbSet<CompanyNumberLobGrid> CompanyNumberLobGrid { get; set; } // CompanyNumberLOBGrid
        public DbSet<CompanyNumberOtherCarrier> CompanyNumberOtherCarrier { get; set; } // CompanyNumberOtherCarrier
        public DbSet<CompanyNumberSubmissionStatus> CompanyNumberSubmissionStatus { get; set; } // CompanyNumberSubmissionStatus
        public DbSet<CompanyNumberSubmissionType> CompanyNumberSubmissionType { get; set; } // CompanyNumberSubmissionType
        public DbSet<CompanyParameter> CompanyParameter { get; set; } // CompanyParameter
        public DbSet<Contact> Contact { get; set; } // Contact
        public DbSet<Country> Country { get; set; } // Country
        public DbSet<DssException> DssException { get; set; } // DSSException
        public DbSet<ExceptionEmailFilterString> ExceptionEmailFilterString { get; set; } // ExceptionEmailFilterString
        public DbSet<ExceptionLog> ExceptionLog { get; set; } // ExceptionLog
        public DbSet<ExceptionReturnMessage> ExceptionReturnMessage { get; set; } // ExceptionReturnMessage
        public DbSet<Faq> Faq { get; set; } // FAQ
        public DbSet<GeographicDirectional> GeographicDirectional { get; set; } // GeographicDirectional
        public DbSet<HazardGradeType> HazardGradeType { get; set; } // HazardGradeType
        public DbSet<LineOfBusiness> LineOfBusiness { get; set; } // LineOfBusiness
        public DbSet<Link> Link { get; set; } // Link
        public DbSet<NaicsCodeList> NaicsCodeList { get; set; } // NaicsCodeList
        public DbSet<NaicsDivision> NaicsDivision { get; set; } // NaicsDivision
        public DbSet<NaicsGroup> NaicsGroup { get; set; } // NaicsGroup
        public DbSet<NamePrefix> NamePrefix { get; set; } // NamePrefix
        public DbSet<NameSuffixTitle> NameSuffixTitle { get; set; } // NameSuffixTitle
        public DbSet<Notification> Notification { get; set; } // Notification
        public DbSet<NumberControl> NumberControl { get; set; } // NumberControl
        public DbSet<NumberControlType> NumberControlType { get; set; } // NumberControlType
        public DbSet<OtherCarrier> OtherCarrier { get; set; } // OtherCarrier
        public DbSet<Person> Person { get; set; } // Person
        public DbSet<PolicySystem> PolicySystem { get; set; } // PolicySystem
        public DbSet<Role> Role { get; set; } // Role
        public DbSet<SecondaryUnitDesignator> SecondaryUnitDesignator { get; set; } // SecondaryUnitDesignator
        public DbSet<SicCodeList> SicCodeList { get; set; } // SicCodeList
        public DbSet<SicDivision> SicDivision { get; set; } // SicDivision
        public DbSet<SicGroup> SicGroup { get; set; } // SicGroup
        public DbSet<State> State { get; set; } // State
        public DbSet<StreetSuffix> StreetSuffix { get; set; } // StreetSuffix
        public DbSet<Submission> Submission { get; set; } // Submission
        public DbSet<SubmissionClientCoreClientAddress> SubmissionClientCoreClientAddress { get; set; } // SubmissionClientCoreClientAddress
        public DbSet<SubmissionClientCoreClientName> SubmissionClientCoreClientName { get; set; } // SubmissionClientCoreClientName
        public DbSet<SubmissionComment> SubmissionComment { get; set; } // SubmissionComment
        public DbSet<SubmissionCompanyNumberAttributeValue> SubmissionCompanyNumberAttributeValue { get; set; } // SubmissionCompanyNumberAttributeValue
        public DbSet<SubmissionCompanyNumberAttributeValueHistory> SubmissionCompanyNumberAttributeValueHistory { get; set; } // SubmissionCompanyNumberAttributeValueHistory
        public DbSet<SubmissionCompanyNumberCodeHistory> SubmissionCompanyNumberCodeHistory { get; set; } // SubmissionCompanyNumberCodeHistory
        public DbSet<SubmissionCopyExclusion> SubmissionCopyExclusion { get; set; } // SubmissionCopyExclusion
        public DbSet<SubmissionDisplayGrid> SubmissionDisplayGrid { get; set; } // SubmissionDisplayGrid
        public DbSet<SubmissionHistory> SubmissionHistory { get; set; } // SubmissionHistory
        public DbSet<SubmissionLineOfBusinessChildren> SubmissionLineOfBusinessChildren { get; set; } // SubmissionLineOfBusinessChildren
        public DbSet<SubmissionNumberControl> SubmissionNumberControl { get; set; } // SubmissionNumberControl
        public DbSet<SubmissionPage> SubmissionPage { get; set; } // SubmissionPage
        public DbSet<SubmissionSearchPage> SubmissionSearchPage { get; set; } // SubmissionSearchPage
        public DbSet<SubmissionStatus> SubmissionStatus { get; set; } // SubmissionStatus
        public DbSet<SubmissionStatusHistory> SubmissionStatusHistory { get; set; } // SubmissionStatusHistory
        public DbSet<SubmissionStatusReason> SubmissionStatusReason { get; set; } // SubmissionStatusReason
        public DbSet<SubmissionStatusSubmissionStatusReason> SubmissionStatusSubmissionStatusReason { get; set; } // SubmissionStatusSubmissionStatusReason
        public DbSet<SubmissionToken> SubmissionToken { get; set; } // SubmissionToken
        public DbSet<SubmissionType> SubmissionType { get; set; } // SubmissionType
        public DbSet<UnderwritingRole> UnderwritingRole { get; set; } // UnderwritingRole
        public DbSet<User> User { get; set; } // User
        public DbSet<Validation> Validation { get; set; } // Validation
        public DbSet<ValidationCondition> ValidationCondition { get; set; } // ValidationCondition
        public DbSet<ValidationField> ValidationField { get; set; } // ValidationField
        public DbSet<ValidationOperator> ValidationOperator { get; set; } // ValidationOperator
        public DbSet<ValidationRule> ValidationRule { get; set; } // ValidationRule
        public DbSet<ValidationRuleValue> ValidationRuleValue { get; set; } // ValidationRuleValue
        public DbSet<SubmissionField> SubmissionField { get; set; }
        public DbSet<SubmissionViewType> SubmissionViewType { get; set; }
        public DbSet<SubmissionViewField> SubmissionViewField { get; set; }
        public DbSet<ImportSubmission> ImportSubmission { get; set; }
        public DbSet<PolicySymbol> PolicySymbol { get; set; }
        public DbSet<ClientField> ClientField { get; set; }
        public DbSet<CompanyNumberClientField> CompanyNumberClientField { get; set; }
        public DbSet<SubmissionAddress> SubmissionAddress { get; set; }
        public DbSet<SubmissionAdditionalName> SubmissionAdditionalName { get; set; }
        public DbSet<UserType> UserType { get; set; }
        public DbSet<CompanyNumberSubmissionField> CompanyNumberSubmissionField { get; set; }
        public DbSet<CompanyNumberClientCoreSegment> CompanyNumberClientCoreSegment { get; set; }
        public DbSet<CompanyNumberCrossClearance> CompanyNumberCrossClearance { get; set; }

        private static void LoadStandardDomainData()
        {
            var db = new MockDb();

            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 3, CompanyId = 1, CompanyNumberValue = "40", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 5, CompanyId = 2, CompanyNumberValue = "44", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 6, CompanyId = 3, CompanyNumberValue = "52", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 8, CompanyId = 4, CompanyNumberValue = "12", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 12, CompanyId = 5, CompanyNumberValue = "57", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 15, CompanyId = 6, CompanyNumberValue = "80", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 16, CompanyId = 7, CompanyNumberValue = "060", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 19, CompanyId = 8, CompanyNumberValue = "045", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 20, CompanyId = 9, CompanyNumberValue = "050", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 21, CompanyId = 10, CompanyNumberValue = "078", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 22, CompanyId = 11, CompanyNumberValue = "178", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 23, CompanyId = 12, CompanyNumberValue = "085", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 24, CompanyId = 12, CompanyNumberValue = "185", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 25, CompanyId = 13, CompanyNumberValue = "024", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 26, CompanyId = 14, CompanyNumberValue = "084", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 27, CompanyId = 15, CompanyNumberValue = "063", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 28, CompanyId = 16, CompanyNumberValue = "278", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 29, CompanyId = 17, CompanyNumberValue = "076", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 30, CompanyId = 18, CompanyNumberValue = "058", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 31, CompanyId = 20, CompanyNumberValue = "056", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 32, CompanyId = 21, CompanyNumberValue = "018", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 34, CompanyId = 22, CompanyNumberValue = "000", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 35, CompanyId = 23, CompanyNumberValue = "09", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 36, CompanyId = 24, CompanyNumberValue = "021", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 37, CompanyId = 25, CompanyNumberValue = "079", Company = new Company() { CompanyName = "", CompanyInitials = "" } });
            db.CompanyNumber.Add(new Models.CompanyNumber() { Id = 38, CompanyId = 26, CompanyNumberValue = "026", Company = new Company() { CompanyName = "", CompanyInitials = "" } });

            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 1, CompanyId = 3, Dsik = "e8d1c216-01f8-432a-87fe-661c23264327 ", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 2, CompanyId = 1, Dsik = "2710ff04-de7e-4e63-a470-e2068b5859e1", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 3, CompanyId = 4, Dsik = "84efd6f3-cae3-4df8-8ed9-377343870379 ", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 4, CompanyId = 6, Dsik = "7b8a807f-efe7-4d03-9821-4f79b8c30ee9", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 5, CompanyId = 7, Dsik = "e0753355-dacd-40fd-bcea-13c169cc6098", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 6, CompanyId = 9, Dsik = "1f88f9fb-7be5-4742-b6ff-c21718aea213", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 7, CompanyId = 13, Dsik = "c4b095b4-53b0-400a-88eb-1c67059377f0", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 8, CompanyId = 17, Dsik = "3e8b4879-6d33-4be5-88ff-ee10442bf26d", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 9, CompanyId = 18, Dsik = "c3dc9331-0703-4cdf-8de5-45a245921ac9", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 10, CompanyId = 21, Dsik = "d0fc729a-e787-46e3-8d97-af4eaae7dc43", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 11, CompanyId = 23, Dsik = "12803e2e-8637-428e-ac1b-782a90b1c307", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 12, CompanyId = 24, Dsik = "2c476153-fdda-4565-91c0-f893426998c9", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 13, CompanyId = 25, Dsik = "a9c20dee-a8f8-439d-a217-7863149e0d0f", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });
            db.CompanyClientAdvancedSearch.Add(new CompanyClientAdvancedSearch() { Id = 14, CompanyId = 26, Dsik = "7b8a807f-efe7-4d03-9821-4f79b8c30ee9", ClientAdvSearchEndPointUrl = "http://eip-tst/eip/post/AdvCliSearchWS", UsesOfacCheck = true, PopulateBusinessName = true });

            db.AddressType.Add(new AddressType() { Id = 1, AddressTypeValue = "Mailing" });
            db.AddressType.Add(new AddressType() { Id = 2, AddressTypeValue = "Physical" });
            db.AddressType.Add(new AddressType() { Id = 3, AddressTypeValue = "Both" });

            db.ClientNameType.Add(new ClientNameType() { Id = 1, NameType = "Insured" });
            db.ClientNameType.Add(new ClientNameType() { Id = 2, NameType = "DBA" });
        }
    }
}
