namespace BTS.ClientCore.WS
{
    using System;
    using System.IO;
    using System.Web.Services.Protocols;

    public class LogExtension : System.Web.Services.Protocols.SoapExtension
    {
        private Stream originalStream;
        private Stream internalStream;

        public override Stream ChainStream(Stream stream)
        {
            // Save the original stream
            originalStream = stream;
            // Create and return our own in its place
            internalStream = new MemoryStream();
            return internalStream;
        }
        public override void Initialize(object initializer)
        {
            return;
        }
        public override object GetInitializer(Type serviceType)
        {
            return null;
        }
        public override object GetInitializer(System.Web.Services.Protocols.LogicalMethodInfo methodInfo, System.Web.Services.Protocols.SoapExtensionAttribute attribute)
        {
            return null;
        }
        // The ProcessMessage() method is where we do our work
        public override void ProcessMessage(System.Web.Services.Protocols.SoapMessage message)
        {
            // Determine the stage and take appropriate action
            switch (message.Stage)
            {
                case SoapMessageStage.BeforeSerialize:
                    // About to prepare a SOAP Response
                    break;
                case SoapMessageStage.AfterSerialize:
                    // SOAP response is all prepared

                    WriteToSoapLog(internalStream, "Request");

                    // Copy the passed message to the other stream
                    CopyStream(internalStream, originalStream);
                    internalStream.Position = 0;
                    break;

                case SoapMessageStage.BeforeDeserialize:
                    // About to handle a SOAP request
                    // Copy the passed message to the other stream
                    CopyStream(originalStream, internalStream);

                    WriteToSoapLog(internalStream, "Response");

                    break;

                case SoapMessageStage.AfterDeserialize:
                    // SOAP request has been deserialized
                    break;
            }
        }

        private void WriteToSoapLog(Stream streamToLog, string sMessage)
        {
            StreamWriter sw = null;
            FileStream fs = null;

            try
            {
               string strLogFileName = "c:\\ccfiles\\" + string.Format("{0}{1}.xml", sMessage, DateTime.Now.ToString("MMddyyyyHHmmss"));
                               

                // Open a log file and write a status line
                fs = new FileStream(strLogFileName, FileMode.Append, FileAccess.Write);
                sw = new StreamWriter(fs);
                sw.Flush();

                // Copy the passed SOAP message to the file
                streamToLog.Position = 0;

                // Load the raw XML into an XML document
                System.Xml.XmlDocument xd = new System.Xml.XmlDocument();
                xd.Load(streamToLog);
                MemoryStream ms = new MemoryStream();
                ms.Write(System.Text.Encoding.UTF8.GetBytes(xd.InnerXml), 0, xd.InnerXml.Length);
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);
                sw.WriteLine(sr.ReadToEnd());
                sw.Flush();

                streamToLog.Position = 0;
            }
            catch
            {
                //do nothing
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        private void CopyStream(Stream fromStream, Stream toStream)
        {
            try
            {
                StreamReader sr = new StreamReader(fromStream);
                StreamWriter sw = new StreamWriter(toStream);
                sw.WriteLine(sr.ReadToEnd());
                sw.Flush();
            }
            catch (Exception ex)
            {

            }
        }

    }
}
