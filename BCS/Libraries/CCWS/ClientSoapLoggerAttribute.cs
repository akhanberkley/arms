namespace BTS.ClientCore.WS
{
    using System;

    [AttributeUsage(AttributeTargets.Method)]
    public class ClientSoapLoggerAttribute : System.Web.Services.Protocols.SoapExtensionAttribute
    {
        private int m_Priority = 0;

        public override Type ExtensionType
        {
            get { return typeof(LogExtension); }
        }

        public override int Priority
        {
            get { return m_Priority; }
            set { m_Priority = value; }
        }
    }
    
}
