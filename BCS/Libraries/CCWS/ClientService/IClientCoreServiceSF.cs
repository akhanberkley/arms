namespace BTS.ClientCore.WS.SF
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;


    public interface IClientCoreServiceSF
    {

        string performClientSearch(string basicXML, string clientSearchXML, BTS.ClientCore.WS.SF.processingOption[] processingOptions);

        string performPortfolioSearch(string basicXML, string portfolioSearchXML);

        string getClientInfo(string basicXML, string clientInfoDOXML, BTS.ClientCore.WS.SF.processingOption[] processingOptions);

        string updatePortfolio(string basicXML, string portfolioInfoXML);

        string importClient(string basicXML, string importClientXML);
    }
}
