#region WSCF
//------------------------------------------------------------------------------
// <autogenerated code>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated code>
//------------------------------------------------------------------------------
// File time 27-01-09 02:33 PM
//
// This source code was auto-generated by WSCF, Version=0.6.6034.1
#endregion


namespace BTS.ClientCore.WS
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    
    
    ///// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Web.Services.WebServiceBindingAttribute(Name="ClientCoreServiceSoapBinding", Namespace="http://webservice.clientcore.bts.com/")]
    public partial class ClientCoreService //: System.Web.Services.Protocols.SoapHttpClientProtocol, IClientCoreService
    {
        /*
        [System.Xml.Serialization.XmlElementAttribute(ElementName="SecurityValue")]
        public Security SecurityValue;
        
        private System.Threading.SendOrPostCallback performClientSearchOperationCompleted;
        
        private System.Threading.SendOrPostCallback performPortfolioSearchOperationCompleted;
        
        private System.Threading.SendOrPostCallback getClientInfoOperationCompleted;
        
        private System.Threading.SendOrPostCallback updatePortfolioOperationCompleted;
        
        private System.Threading.SendOrPostCallback importClientOperationCompleted;
        
        /// <remarks/>
        public ClientCoreService()
        {
            this.Url = "http://localhost:9080/ClientCoreService/services/ClientCoreService";
        }
        
        /// <remarks/>
        //public event performClientSearchCompletedEventHandler performClientSearchCompleted;
        
        /// <remarks/>
        public event performPortfolioSearchCompletedEventHandler performPortfolioSearchCompleted;
        
        /// <remarks/>
        //public event getClientInfoCompletedEventHandler getClientInfoCompleted;
        
        /// <remarks/>
        public event updatePortfolioCompletedEventHandler updatePortfolioCompleted;
        
        /// <remarks/>
        //public event importClientCompletedEventHandler importClientCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("SecurityValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://webservice.clientcore.bts.com/", ResponseNamespace="http://webservice.clientcore.bts.com/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("performClientSearchReturn", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string performClientSearch([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName="basicXML")] string basicXML, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName="clientSearchXML")] string clientSearchXML)
        {
            object[] results = this.Invoke("performClientSearch", new object[] {
                        basicXML,
                        clientSearchXML});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void performClientSearchAsync(string basicXML, string clientSearchXML)
        {
            this.performClientSearchAsync(basicXML, clientSearchXML, null);
        }
        
        /// <remarks/>
        public void performClientSearchAsync(string basicXML, string clientSearchXML, object userState)
        {
            if ((this.performClientSearchOperationCompleted == null))
            {
                this.performClientSearchOperationCompleted = new System.Threading.SendOrPostCallback(this.OnperformClientSearchOperationCompleted);
            }
            this.InvokeAsync("performClientSearch", new object[] {
                        basicXML,
                        clientSearchXML}, this.performClientSearchOperationCompleted, userState);
        }
        
        private void OnperformClientSearchOperationCompleted(object arg)
        {
            if ((this.performClientSearchCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.performClientSearchCompleted(this, new performClientSearchCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("SecurityValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://webservice.clientcore.bts.com/", ResponseNamespace="http://webservice.clientcore.bts.com/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("performPortfolioSearchReturn", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string performPortfolioSearch([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName="basicXML")] string basicXML, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName="portfolioSearchXML")] string portfolioSearchXML)
        {
            object[] results = this.Invoke("performPortfolioSearch", new object[] {
                        basicXML,
                        portfolioSearchXML});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void performPortfolioSearchAsync(string basicXML, string portfolioSearchXML)
        {
            this.performPortfolioSearchAsync(basicXML, portfolioSearchXML, null);
        }
        
        /// <remarks/>
        public void performPortfolioSearchAsync(string basicXML, string portfolioSearchXML, object userState)
        {
            if ((this.performPortfolioSearchOperationCompleted == null))
            {
                this.performPortfolioSearchOperationCompleted = new System.Threading.SendOrPostCallback(this.OnperformPortfolioSearchOperationCompleted);
            }
            this.InvokeAsync("performPortfolioSearch", new object[] {
                        basicXML,
                        portfolioSearchXML}, this.performPortfolioSearchOperationCompleted, userState);
        }
        
        private void OnperformPortfolioSearchOperationCompleted(object arg)
        {
            if ((this.performPortfolioSearchCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.performPortfolioSearchCompleted(this, new performPortfolioSearchCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("SecurityValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://webservice.clientcore.bts.com/", ResponseNamespace="http://webservice.clientcore.bts.com/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("getClientInfoReturn", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string getClientInfo([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName="basicXML")] string basicXML, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName="clientInfoDOXML")] string clientInfoDOXML)
        {
            object[] results = this.Invoke("getClientInfo", new object[] {
                        basicXML,
                        clientInfoDOXML});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void getClientInfoAsync(string basicXML, string clientInfoDOXML)
        {
            this.getClientInfoAsync(basicXML, clientInfoDOXML, null);
        }
        
        /// <remarks/>
        public void getClientInfoAsync(string basicXML, string clientInfoDOXML, object userState)
        {
            if ((this.getClientInfoOperationCompleted == null))
            {
                this.getClientInfoOperationCompleted = new System.Threading.SendOrPostCallback(this.OngetClientInfoOperationCompleted);
            }
            this.InvokeAsync("getClientInfo", new object[] {
                        basicXML,
                        clientInfoDOXML}, this.getClientInfoOperationCompleted, userState);
        }
        
        private void OngetClientInfoOperationCompleted(object arg)
        {
            if ((this.getClientInfoCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.getClientInfoCompleted(this, new getClientInfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("SecurityValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://webservice.clientcore.bts.com/", ResponseNamespace="http://webservice.clientcore.bts.com/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("updatePortfolioReturn", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string updatePortfolio([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName="basicXML")] string basicXML, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName="portfolioInfoXML")] string portfolioInfoXML)
        {
            object[] results = this.Invoke("updatePortfolio", new object[] {
                        basicXML,
                        portfolioInfoXML});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void updatePortfolioAsync(string basicXML, string portfolioInfoXML)
        {
            this.updatePortfolioAsync(basicXML, portfolioInfoXML, null);
        }
        
        /// <remarks/>
        public void updatePortfolioAsync(string basicXML, string portfolioInfoXML, object userState)
        {
            if ((this.updatePortfolioOperationCompleted == null))
            {
                this.updatePortfolioOperationCompleted = new System.Threading.SendOrPostCallback(this.OnupdatePortfolioOperationCompleted);
            }
            this.InvokeAsync("updatePortfolio", new object[] {
                        basicXML,
                        portfolioInfoXML}, this.updatePortfolioOperationCompleted, userState);
        }
        
        private void OnupdatePortfolioOperationCompleted(object arg)
        {
            if ((this.updatePortfolioCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.updatePortfolioCompleted(this, new updatePortfolioCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("SecurityValue")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("", RequestNamespace="http://webservice.clientcore.bts.com/", ResponseNamespace="http://webservice.clientcore.bts.com/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute("importClientReturn", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string importClient([System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName="basicXML")] string basicXML, [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, ElementName="importClientXML")] string importClientXML)
        {
            object[] results = this.Invoke("importClient", new object[] {
                        basicXML,
                        importClientXML});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void importClientAsync(string basicXML, string importClientXML)
        {
            this.importClientAsync(basicXML, importClientXML, null);
        }
        
        /// <remarks/>
        public void importClientAsync(string basicXML, string importClientXML, object userState)
        {
            if ((this.importClientOperationCompleted == null))
            {
                this.importClientOperationCompleted = new System.Threading.SendOrPostCallback(this.OnimportClientOperationCompleted);
            }
            this.InvokeAsync("importClient", new object[] {
                        basicXML,
                        importClientXML}, this.importClientOperationCompleted, userState);
        }
        
        private void OnimportClientOperationCompleted(object arg)
        {
            if ((this.importClientCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.importClientCompleted(this, new importClientCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }*/
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" +
        "", TypeName="Security")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" +
        "", IsNullable=false)]
    public partial class Security : System.Web.Services.Protocols.SoapHeader
    {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://bts.services.wrberkley.com", ElementName="dsik")]
        public string dsik;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAnyAttributeAttribute()]
        public System.Xml.XmlAttribute[] AnyAttr;
        
        public Security()
        {
        }
        
        public Security(string dsik, System.Xml.XmlAttribute[] anyAttr)
        {
            this.dsik = dsik;
            this.AnyAttr = anyAttr;
        }
    }
    
    /// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    //public delegate void performClientSearchCompletedEventHandler(object sender, performClientSearchCompletedEventArgs e);
    
    
    /// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //public partial class performClientSearchCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    //{
        
    //    private object[] results;
        
    //    internal performClientSearchCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
    //            base(exception, cancelled, userState)
    //    {
    //        this.results = results;
    //    }
        
    //    /// <remarks/>
    //    public string Result
    //    {
    //        get
    //        {
    //            this.RaiseExceptionIfNecessary();
    //            return ((string)(this.results[0]));
    //        }
    //    }
    //}
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    public delegate void performPortfolioSearchCompletedEventHandler(object sender, performPortfolioSearchCompletedEventArgs e);
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class performPortfolioSearchCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        
        private object[] results;
        
        internal performPortfolioSearchCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState)
        {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
   // [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    //public delegate void getClientInfoCompletedEventHandler(object sender, getClientInfoCompletedEventArgs e);
    
    
    ///// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //public partial class getClientInfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    //{
        
    //    private object[] results;
        
    //    internal getClientInfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
    //            base(exception, cancelled, userState)
    //    {
    //        this.results = results;
    //    }
        
    //    /// <remarks/>
    //    public string Result
    //    {
    //        get
    //        {
    //            this.RaiseExceptionIfNecessary();
    //            return ((string)(this.results[0]));
    //        }
    //    }
    //}
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    public delegate void updatePortfolioCompletedEventHandler(object sender, updatePortfolioCompletedEventArgs e);
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class updatePortfolioCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        
        private object[] results;
        
        internal updatePortfolioCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState)
        {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    //public delegate void importClientCompletedEventHandler(object sender, importClientCompletedEventArgs e);
    
    
    ///// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.42")]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //public partial class importClientCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    //{
        
    //    private object[] results;
        
    //    internal importClientCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
    //            base(exception, cancelled, userState)
    //    {
    //        this.results = results;
    //    }
        
    //    /// <remarks/>
    //    public string Result
    //    {
    //        get
    //        {
    //            this.RaiseExceptionIfNecessary();
    //            return ((string)(this.results[0]));
    //        }
    //    }
    //}
}
