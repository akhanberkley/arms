#region WSCF
//------------------------------------------------------------------------------
// <autogenerated code>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated code>
//------------------------------------------------------------------------------
// File time 27-01-09 02:33 PM
//
// This source code was auto-generated by WSCF, Version=0.6.6034.1
#endregion


namespace BTS.ClientCore.WS
{
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    
    
    public interface IClientCoreService
    {
        
        string performClientSearch(string basicXML, string clientSearchXML);
        
        string performPortfolioSearch(string basicXML, string portfolioSearchXML);
        
        string getClientInfo(string basicXML, string clientInfoDOXML);
        
        string updatePortfolio(string basicXML, string portfolioInfoXML);
        
        string importClient(string basicXML, string importClientXML);
    }
}
