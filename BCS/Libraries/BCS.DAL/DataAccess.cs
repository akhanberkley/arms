﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.Objects;
using System.Linq.Expressions;
using System.Reflection;

namespace BCS.DAL
{
    public sealed class EFilter
    {
        public EFilter()
        {

        }
        public EFilter(string columnName, object value, MatchType matchType, BinaryType binaryType)
        {
            ColumnName = columnName;
            Value = value;
            MatchType = matchType;
            BinaryType = binaryType;
        }

        public EFilter(string columnName, object value, MatchType matchType)
        {
            ColumnName = columnName;
            Value = value;
            MatchType = matchType;
        }

        public EFilter(string columnName, object value)
        {
            ColumnName = columnName;
            Value = value;
        }

        public string ColumnName;
        public object Value;
        public MatchType MatchType;
        public BinaryType BinaryType;
    }

    public enum MatchType
    {
        Exact = 1,
        Partial,
        In,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        EndsWith,
        StartsWith
    }

    public enum BinaryType
    {
        And,
        AndAlso,
        Or,
        OrElse
    }
    public static class DataAccess
    {
        public static BCS.DAL.BCSContext NewBCSContext()
        {
            return new BCS.DAL.BCSContext(ConfigurationManager.ConnectionStrings["BCSConnectionString"].ConnectionString);
        }

        public static string ConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["BCSConnectionString"].ConnectionString;
        }

        public static T GetEntityById<T>(int id)
        {
            string name = string.Format("BCSContext.{0}", typeof(T).Name);
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                System.Data.EntityKey key = new System.Data.EntityKey(name, "Id", id);
                return (T)bc.GetObjectByKey(key);
              
            }
        }

        public static T GetEntityByColumnValue<T>(string columnName, object value)
        {
            string name = string.Format("BCSContext.{0}", typeof(T).Name);
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                System.Data.EntityKey key = new System.Data.EntityKey(name, columnName, value);
                return (T)bc.GetObjectByKey(key);
            }
        }

        public static T GetEntityByColumnValue<T>(string columnName, object value, MatchType match, params string[] includeEntities)
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
            	ObjectQuery<T> q = new ObjectQuery<T>(typeof(T).Name,bc);
                string whereClause = BuildWhere(columnName, match);

	            q = q.Where (whereClause,new ObjectParameter(columnName,value));

	            foreach (string item in includeEntities)
	            {
		            q = q.Include(item);
	            }

                q.MergeOption = MergeOption.NoTracking;

                return q.FirstOrDefault();
            }
        }

        public static List<T> GetEntityListByColumnValue<T>(string columnName, object value, MatchType match, params string[] includeEntities)
        {
            //Expression exp = null;

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                ObjectQuery<T> q = new ObjectQuery<T>(typeof(T).Name, bc);
                string whereClause = BuildWhere(columnName, match);

                q = q.Where(whereClause, new ObjectParameter(columnName, value));


                foreach (string item in includeEntities)
                {
                    q = q.Include(item);
                }

                q.MergeOption = MergeOption.NoTracking;

                return q.ToList();
            }
        }

        //private static string BuildWhere(string columnName, MatchType mType)
        //{
        //    string op = string.Empty;
        //    string pred = string.Empty;

        //    switch (mType)
        //    {
        //        default:
        //        case MatchType.Exact :
        //            op = "=";
        //            pred = string.Format("@{0}", columnName);
        //            break;
        //        case MatchType.Partial :
        //            op = "LIKE";
        //            pred = string.Format("'%' + @{0} + '%'", columnName);
        //            break;
        //        case MatchType.GreaterThan:
        //            op = ">";
        //            pred = string.Format("@{0}", columnName);
        //            break;
        //        case MatchType.GreaterThanOrEqual:
        //            op = ">=";
        //            pred = string.Format("@{0}", columnName);
        //            break;
        //        case MatchType.LessThan:
        //            op = "<";
        //            pred = string.Format("@{0}", columnName);
        //            break;
        //        case MatchType.LessThanOrEqual:
        //            op = "<=";
        //            pred = string.Format("@{0}", columnName);
        //            break;
        //    }

        //    return string.Format("it.{0} {1} {2}", columnName, op, pred);
        //}

        private static string BuildWhere(string columnName, MatchType mType)
        {
            string op = string.Empty;
            string pred = string.Empty;

            switch (mType)
            {
                default:
                case MatchType.Exact:
                    op = "=";
                    pred = string.Format("@{0}", columnName);
                    break;
                case MatchType.Partial:
                    op = "LIKE";
                    pred = string.Format("'%' + @{0} + '%'", columnName);
                    break;
                case MatchType.EndsWith:
                    op = "LIKE";
                    pred = string.Format("'%' + @{0}", columnName);
                    break;
                case MatchType.StartsWith:
                    op = "LIKE";
                    pred = string.Format("@{0} + '%'", columnName);
                    break;
                case MatchType.GreaterThan:
                    op = ">";
                    pred = string.Format("@{0}", columnName);
                    break;
                case MatchType.GreaterThanOrEqual:
                    op = ">=";
                    pred = string.Format("@{0}", columnName);
                    break;
                case MatchType.LessThan:
                    op = "<";
                    pred = string.Format("@{0}", columnName);
                    break;
                case MatchType.LessThanOrEqual:
                    op = "<=";
                    pred = string.Format("@{0}", columnName);
                    break;
            }

            return string.Format("it.{0} {1} {2}", columnName, op, pred);
        }

        public static List<T> GetEntityCollectionById<T>(int[] ids)
        {
            return GetEntityList<T>("Id", ids,MatchType.In);
        }
        public static List<T> GetEntityCollectionById<T>(string[] ids)
        {
            List<int> intIds = new List<int>();
            foreach (string id in ids)
            {
                intIds.Add(int.Parse(id));      
            }

            return GetEntityList<T>("Id", ids.ToArray(), MatchType.In);
           
        }

        public static List<T> GetEntityList<T>(string columnName, object value, MatchType matchType)
        {
            return GetEntityList<T>(columnName, value, matchType, 0);
        }

        public static List<T> GetEntityList<T>(string columnName, object value)
        {
            return GetEntityList<T>(columnName, value, MatchType.Exact, 0);
        }

        public static List<T> GetEntityList<T>(string columnName, object value, int rows)
        {
            return GetEntityList<T>(columnName, value, MatchType.Exact, rows);
        }

        public static List<T> GetEntityList<T>(string columnName, object value, params string[] includeEntities)
        {
            return GetEntityList<T>(columnName, value, MatchType.Exact, 0,includeEntities);
        }

        public static List<T> GetEntityList<T>(string columnName, object value, MatchType matchType, params string[] includeEntities)
        {
            return GetEntityList<T>(columnName, value, matchType, 0,includeEntities);
        }

        public static List<T> GetEntityList<T>(string columnName, object value,MatchType matchType,int rows, params string[] includeEntities)
        {
            EFilter filter = new EFilter() {ColumnName = columnName, Value = value, MatchType = matchType};
            return GetEntityList<T>(new List<EFilter>() { filter }, rows, includeEntities); 
        }

        public static List<T> GetEntityList<T>(List<EFilter> filters)
        {
            return GetEntityList<T>(filters, 0); 
        }

        public static List<T> GetEntityList<T>(List<EFilter> filters, int rows)
        {
            return GetEntityList<T>(filters, rows);
        }

        public static List<T> GetEntityList<T>(List<EFilter> filters, params string[] includeEntities)
        {
            return GetEntityList<T>(filters, 0,includeEntities);
        }

        public static List<T> GetEntityList<T>(List<EFilter> filters, int rows, params string[] includeEntities)
        {
            List<T> result = null;
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                ObjectQuery<T> q = new ObjectQuery<T>(typeof(T).Name, bc, MergeOption.NoTracking);
                //add entities to be loaded immediately
                foreach (string item in includeEntities)
                {
                    q = q.Include(item);
                }
                q.MergeOption = MergeOption.NoTracking;
                var whereExpression = BuildExpression<T>(filters);
                DateTime start = DateTime.Now;
                var test = q.Where(whereExpression);
                result = q.Where(whereExpression).ToList(); 
                string res2 = (DateTime.Now - start).TotalMilliseconds.ToString();                
            }
            
            return result;
        }

        public static List<T> GetEntityListWithContext<T>(BCSContext bc, List<EFilter> filters, params string[] includeEntities)
        {
            List<T> result = null;
            ObjectQuery<T> q = new ObjectQuery<T>(typeof(T).Name, bc, MergeOption.NoTracking);
            //add entities to be loaded immediately
            foreach (string item in includeEntities)
            {
                q = q.Include(item);
            }
            q.MergeOption = MergeOption.NoTracking;
            var whereExpression = BuildExpression<T>(filters);
            DateTime start = DateTime.Now;
            var test = q.Where(whereExpression);
            result = q.Where(whereExpression).ToList();
            string res2 = (DateTime.Now - start).TotalMilliseconds.ToString();
            return result;
        }

        public static List<int> GetEntityListIds<T>(List<EFilter> filters, params string[] includeEntities)
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                ObjectQuery<T> q = new ObjectQuery<T>(typeof(T).Name, bc,MergeOption.NoTracking);
                //foreach (string item in includeEntities)
                //{
                //    q = q.Include(item);
                //}
                var whereExpression = BuildExpression<T>(filters);
                DateTime start = DateTime.Now;

                ParameterExpression param = Expression.Parameter(typeof(T),typeof(T).Name);
                Expression exp = Expression.Property(param,"Id");

                var selectExp = Expression.Lambda<Func<T, int>>(Expression.Convert(exp, typeof(int)), param);

                List<int> result = q.Where(whereExpression).Select(selectExp).AsParallel().ToList();
                string res2 = (DateTime.Now - start).TotalMilliseconds.ToString();
                return result;
            }
        }

        public static Expression<Func<T, bool>> BuildExpression<T>(EFilter filter)
        {
            return BuildExpression<T>(new List<EFilter>() { filter });
        }

        public static Expression<Func<T, bool>> BuildExpression<T>(List<EFilter> filters)
        {
            Expression mExp = null;
            List<ParameterExpression> parameters = new List<ParameterExpression>();

            foreach (EFilter filter in filters)
            {
                if (string.IsNullOrWhiteSpace(filter.ColumnName))
                    return null;
                //build expression
                string[] columns = filter.ColumnName.Split('.');

                Expression exp = null;

                for (int i = 0; i < columns.Length; i++)
                {
                    if (i == 0)
                    {
                        parameters.Add(Expression.Parameter(typeof(T), typeof(T).Name));
                        exp = Expression.Property(parameters[i], columns[i]);
                    }
                    else
                    {
                        int index = 0;
                        if (i > 1)
                            index = parameters.Count - 1;

                        //if (exp.Type.Name.Contains("EntityCollection");
                        parameters.Add(Expression.Parameter(parameters[index].Type.GetProperty(columns[index]).PropertyType, parameters[index].Type.GetProperty(columns[index]).Name));
                        exp = Expression.Property(exp, parameters[index].Type.GetProperty(columns[index]).PropertyType, columns[i]);
                    }
                }
                switch (filter.MatchType)
                {
                    default:
                    case MatchType.Exact:
                        exp = Expression.Equal(exp, Expression.Constant(filter.Value, parameters[columns.Length - 1].Type.GetProperty(columns[columns.Length - 1]).PropertyType));
                        break;
                    case MatchType.GreaterThan:
                        exp = Expression.GreaterThan(exp, Expression.Constant(filter.Value, parameters[columns.Length - 1].Type.GetProperty(columns[columns.Length - 1]).PropertyType));
                        break;
                    case MatchType.GreaterThanOrEqual:
                        exp = Expression.GreaterThanOrEqual(exp, Expression.Constant(filter.Value, parameters[columns.Length - 1].Type.GetProperty(columns[columns.Length - 1]).PropertyType));
                        break;
                    case MatchType.LessThan:
                        exp = Expression.LessThan(exp, Expression.Constant(filter.Value, parameters[columns.Length - 1].Type.GetProperty(columns[columns.Length - 1]).PropertyType));
                        break;
                    case MatchType.LessThanOrEqual:
                        exp = Expression.LessThanOrEqual(exp, Expression.Constant(filter.Value, parameters[columns.Length - 1].Type.GetProperty(columns[columns.Length - 1]).PropertyType));
                        break;
                    case MatchType.Partial:
                        exp = Expression.Call(exp, parameters[columns.Length - 1].Type.GetProperty(columns[columns.Length - 1]).PropertyType.GetMethod("Contains"), Expression.Constant(filter.Value, parameters[columns.Length - 1].Type.GetProperty(columns[columns.Length - 1]).PropertyType));
                        break;
                    case MatchType.In:                        
                        if (filter.Value.GetType() == typeof(System.Int32[]))
                        {
                            filter.Value = (filter.Value as int[]).ToList();
                        }
                        else if (filter.Value.GetType() == typeof(System.Int64[]))
                        {
                            filter.Value = (filter.Value as long[]).ToList();
                        }
                        else if (filter.Value.GetType() == typeof(System.String[]))
                        {
                            filter.Value = (filter.Value as string[]).ToList();
                        }
                        exp = Expression.Call(Expression.Constant(filter.Value, filter.Value.GetType()), filter.Value.GetType().GetMethod("Contains"), exp);
                        break;
                }

                if (mExp == null)
                    mExp = exp;
                else
                {
                    switch (filter.BinaryType)
                    {
                        case BinaryType.And:
                            mExp = Expression.And(mExp, exp);
                            break;
                        case BinaryType.Or:
                            mExp = Expression.Or(mExp, exp);
                            break;
                        case BinaryType.OrElse:
                            mExp = Expression.OrElse(mExp, exp);
                            break;
                        default:
                        case BinaryType.AndAlso:
                            mExp = Expression.AndAlso(mExp, exp);
                            break;
                    }
                }
            }

            return Expression.Lambda<Func<T, bool>>(mExp, parameters[0]);
        }
        
    }
}
