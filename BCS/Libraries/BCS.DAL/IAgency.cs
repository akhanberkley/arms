﻿using System;
namespace BCS.DAL
{
    interface IAgency
    {
        bool Active { get; set; }
        string Address { get; set; }
        string Address2 { get; set; }
        System.Data.Objects.DataClasses.EntityCollection<Agency> Agency1 { get; set; }
        Agency Agency2 { get; set; }
        System.Data.Objects.DataClasses.EntityReference<Agency> Agency2Reference { get; set; }
        System.Data.Objects.DataClasses.EntityCollection<AgencyLicensedState> AgencyLicensedState { get; set; }
        System.Data.Objects.DataClasses.EntityCollection<AgencyLineOfBusiness> AgencyLineOfBusiness { get; set; }
        System.Data.Objects.DataClasses.EntityCollection<AgencyLineOfBusinessPersonUnderwritingRolePerson> AgencyLineOfBusinessPersonUnderwritingRolePerson { get; set; }
        string AgencyName { get; set; }
        string AgencyNumber { get; set; }
        System.Data.Objects.DataClasses.EntityCollection<Agent> Agent { get; set; }
        long? APSId { get; set; }
        string BranchId { get; set; }
        DateTime? CancelDate { get; set; }
        string CaptiveBroker { get; set; }
        string City { get; set; }
        string Comments { get; set; }
        CompanyNumber CompanyNumber { get; set; }
        int CompanyNumberId { get; set; }
        System.Data.Objects.DataClasses.EntityReference<CompanyNumber> CompanyNumberReference { get; set; }
        System.Data.Objects.DataClasses.EntityCollection<Contact> Contact { get; set; }
        string ContactPerson { get; set; }
        string ContactPersonEmail { get; set; }
        string ContactPersonExtension { get; set; }
        string ContactPersonFax { get; set; }
        string ContactPersonPhone { get; set; }
        DateTime EffectiveDate { get; set; }
        string Email { get; set; }
        string EntryBy { get; set; }
        DateTime EntryDt { get; set; }
        decimal? Fax { get; set; }
        string FEIN { get; set; }
        int Id { get; set; }
        string ImageFax { get; set; }
        bool IsMaster { get; set; }
        int? MasterAgencyId { get; set; }
        string ModifiedBy { get; set; }
        DateTime? ModifiedDt { get; set; }
        decimal? Phone { get; set; }
        string ReferralAgencyNumber { get; set; }
        DateTime? ReferralDate { get; set; }
        string State { get; set; }
        string StateOfIncorporation { get; set; }
        string StateTaxId { get; set; }
        System.Data.Objects.DataClasses.EntityCollection<Submission> Submission { get; set; }
        string Zip { get; set; }
    }
}
