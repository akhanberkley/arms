﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCS.DAL
{
    public partial class Submission
    {
        public static Submission GetById(long id, params string[] relations)
        {
            return DataAccess.GetEntityList<Submission>("Id",id,relations).FirstOrDefault();
        }
    }
}
