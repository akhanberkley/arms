﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BCS.DAL;

namespace BCS.DAL
{
    public partial class Company
    {
        public static Company GetCompanyByCompanyNumber(string companyNumber)
        {
            Company company = null;
            int companyId = DAL.CompanyNumber.GetCompanyNumberId(companyNumber);

            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                company = bc.Company.Include("CompanyParameter").Where(c => c.Id == companyId).SingleOrDefault();
            }

            return company;
        }
    }
}
