﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BCS.DAL
{
    public partial class CompanyNumber
    {
        public static int GetCompanyNumberId(string companyNumber)
        {
            using (BCSContext bc = DataAccess.NewBCSContext())
            {
                CompanyNumber cn = bc.CompanyNumber.Where(c=>c.CompanyNumber1 == companyNumber).FirstOrDefault();

                if (cn != null)
                    return cn.Id;
                else return 0;
            }
        }
    }
}
