﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Data.Objects;
using System.Reflection;

namespace BCS.DAL
{
    public static class Extensions
    {
        public static object IfNull<T>(this object entity, T returnValue)
        {
            try
            {
                if (entity == null)
                    return returnValue;
                else
                    return null;
            }
            catch (Exception ex)
            {

            }
            return returnValue;
        }

        public static T GetSafe<T>(this object obj)
        {
            try
            {
                return (T)Convert.ChangeType(obj, typeof(T));
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }

        public static DateTime GetSafeDateTime(this object obj)
        {
            return obj.GetSafe<DateTime>();
        }

        public static Int32 GetSafeInt32(this object obj)
        {
            return obj.GetSafe<Int32>();
        }

        public static Int64 GetSafeInt64(this object obj)
        {
            return obj.GetSafe<Int64>();
        }

        public static decimal GetSafeDecimal(this object obj)
        {
            return obj.GetSafe<decimal>();
        }

        public static T IfNotNull<T>(this object entity, T returnValue)
        {
            try
            {
                if (entity != null)
                    return returnValue;
                else
                    return default(T);
            }
            catch (Exception ex)
            {

            }
            return returnValue;
        }
    }
}
