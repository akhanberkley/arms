﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.Providers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS._Tests.ManualRetry
{
    [TestClass]
    public class Submission
    {
#if DEBUG
        [TestMethod]
#endif
        public void OfacCheck()
        {
            int logId = 11;
            Application.Initialize<Data.SqlDb>(Application.GetStandardDebugHandlers());

            var context = WFA.ExceptionHandling.SqlErrorHandler.GetLoggedExceptionContext(logId);

            long? submissionNumber = HealthMonitorProvider.Deserialize<long?>(context.SubmissionNumber);
            CompanyDetail cd = HealthMonitorProvider.Deserialize<CompanyDetail>(context.CompanyNumber);
            var serviceSettings = HealthMonitorProvider.Deserialize<Integration.Models.AdvancedClientSettings>(context.ServiceSettings);
            var searchCriteria = HealthMonitorProvider.Deserialize<Integration.Models.OFACSearchCriteria>(context.Criteria);
            var searchDescription = HealthMonitorProvider.Deserialize<string>(context.SearchDescription);

            var ofacResults = Integration.Services.ClientService.SearchOFAC(serviceSettings, searchDescription, searchCriteria);

            if (ofacResults.Succeeded && ofacResults.Matches.Count > 0)
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    var dbSubmission = db.Submission.FirstOrDefault(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber);
                    if (dbSubmission != null)
                    {
                        dbSubmission.CreatedWithOFACHit = true;
                        if (cd.SystemSettings.UsesOfacSubmissionNote)
                            dbSubmission.SubmissionComment.Add(new Data.Models.SubmissionComment() { Submission = dbSubmission, Comment = BCS.Services.LegacyService.OFACMessage, EntryBy = "OFAC Check" });
                    }
                }
            }
        }
    }
}
