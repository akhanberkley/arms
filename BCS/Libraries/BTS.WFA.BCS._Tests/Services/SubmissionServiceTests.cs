﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS._Tests.Services
{
    [TestClass]
    public class SubmissionServiceTests : StandardBaseTest
    {
        #region AIC
        [TestMethod]
        public void AICNoDupPolicyNumbers()
        {
            var cn = Application.CompanyMap["AIC"];
            string p1 = null, p2 = null;

            var t1 = Task.Factory.StartNew(() => p1 = BCS.Services.SubmissionService.GetNextPolicyNumber(cn, null));
            var t2 = Task.Factory.StartNew(() => p2 = BCS.Services.SubmissionService.GetNextPolicyNumber(cn, null));

            Task.WaitAll(t1, t2);
            Assert.AreNotEqual(p1, p2);
        }

        [TestMethod]
        public void AICPerformanceTest()
        {
            var cn = Application.CompanyMap["AIC"];
            var submission = BCS.Services.SubmissionQueryService.GetByNumber(cn, new SubmissionNumberViewModel { Number = 431535, Sequence = 1 }, null);
        }

        [TestMethod]
        public void AICSubmissionNumbersTest()
        {
            var cn = Application.CompanyMap["AIC"];
            List<long> numbers = new List<long>();

            List<Task> tasks = new List<Task>();
            tasks.Add(Task.Factory.StartNew(() => numbers.Add(BCS.Services.SubmissionService.GetNextSubmissionNumber(cn))));
            tasks.Add(Task.Factory.StartNew(() => numbers.Add(BCS.Services.SubmissionService.GetNextSubmissionNumber(cn))));
            tasks.Add(Task.Factory.StartNew(() => numbers.Add(BCS.Services.SubmissionService.GetNextSubmissionNumber(cn))));

            Task.WaitAll(tasks.ToArray());
        }
        [TestMethod]
        public void AICPolicyNumbersTest()
        {
            var cn = Application.CompanyMap["AIC"];
            List<string> numbers = new List<string>();

            List<Task> tasks = new List<Task>();
            tasks.Add(Task.Factory.StartNew(() => numbers.Add(BCS.Services.SubmissionService.GetNextPolicyNumber(cn, null))));
            tasks.Add(Task.Factory.StartNew(() => numbers.Add(BCS.Services.SubmissionService.GetNextPolicyNumber(cn, null))));
            tasks.Add(Task.Factory.StartNew(() => numbers.Add(BCS.Services.SubmissionService.GetNextPolicyNumber(cn, null))));

            Task.WaitAll(tasks.ToArray());
        }
        #endregion

        #region CWG
        [TestMethod]
        public void CWGPerformanceTestSave()
        {
            var cn = Application.CompanyMap["CWG"];
            var submission = BCS.Services.SubmissionQueryService.GetByNumber(cn, new SubmissionNumberViewModel { Number = 190856, Sequence = 1 }, null);

            BCS.Services.SubmissionService.Save(cn, new SubmissionNumberViewModel { Number = 190856, Sequence = 1 }, submission, "wrbts\\jyoungers", new BCS.Services.SubmissionSaveOptions() { ReturnFullSubmission = true }, null);
        }

        [TestMethod]
        public void CWGAppPerformanceTest()
        {
            var cn = Application.CompanyMap["CWG"];
            var submission = BCS.Services.SubmissionQueryService.GetByNumber(cn, new SubmissionNumberViewModel { Number = 190873, Sequence = 1 }, null);
            submission.DuplicateOfSubmission = new SubmissionSummaryViewModel() { Id = "190874-1" };

            var timer = new System.Diagnostics.Stopwatch();
            timer.Start();
            BCS.Services.SubmissionService.Save(cn, new SubmissionNumberViewModel { Number = 190873, Sequence = 1 }, submission, "wrbts\\jyoungers", new BCS.Services.SubmissionSaveOptions() { }, null);
            timer.Stop();
            var totalTime = timer.Elapsed;
            //Benchmark:  {00:00:05.8383300}
            //Dup Index:  {00:00:03.5755850}
            //Refactored: {00:00:02.6352686}
        }
        #endregion
    }
}
