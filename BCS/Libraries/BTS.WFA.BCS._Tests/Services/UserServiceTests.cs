﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS._Tests.Services
{
    [TestClass]
    public class UserServiceTests : StandardBaseTest
    {
        [TestMethod]
        public void GetUser()
        {
            var user = BCS.Services.UserService.GetActiveUser(@"wrbts\jyoungers");
            Assert.IsTrue(user != null);

        }
        #region AIC
        [TestMethod]
        public void GetUserById()
        {
            var user = BCS.Services.UserService.GetUser(Application.CompanyMap["AIC"], 1191, true);
            Assert.IsTrue(user != null);
        }
        #endregion
    }
}
