﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS._Tests.Services
{
    [TestClass]
    public class AddressServiceTests : StandardBaseTest
    {
        [TestInitialize]
        public new void Initialization()
        {
            Application.Initialize<Data.MockDb>(Application.GetStandardDebugHandlers());
        }

        [TestMethod]
        public void ClientCoreAddressSplit()
        {
            var test = BCS.Services.AddressService.SplitBuildNumberAddressLine("8300 Twana Dr");
            Assert.IsTrue(test.Item1 == "8300");
            Assert.IsTrue(test.Item2 == "Twana Dr");

            test = BCS.Services.AddressService.SplitBuildNumberAddressLine("8300B Twana Dr");
            Assert.IsTrue(test.Item1 == "8300B");
            Assert.IsTrue(test.Item2 == "Twana Dr");

            test = BCS.Services.AddressService.SplitBuildNumberAddressLine("Twana Dr");
            Assert.IsTrue(test.Item1 == "");
            Assert.IsTrue(test.Item2 == "Twana Dr");

            test = BCS.Services.AddressService.SplitBuildNumberAddressLine("Test Address 1");
            Assert.IsTrue(test.Item1 == "");
            Assert.IsTrue(test.Item2 == "Test Address 1");
        }

        #region AIC
        [TestMethod]
        public void AICSearch()
        {
            var cn = Application.CompanyMap["AIC"];
            var results = BCS.Services.AddressService.Lookup(cn, new ViewModels.AddressLookupCriteriaViewModel() { Address1 = "3840 109th", City = "Urbandale", State = "IA", PostalCode = "50322" });
            Assert.IsTrue(results.Status == ViewModels.AddressLookupResultsType.Succeeded);

            results = BCS.Services.AddressService.Lookup(cn, new ViewModels.AddressLookupCriteriaViewModel() { City = "Urbandale", State = "IA" });
            Assert.IsTrue(results.Status == ViewModels.AddressLookupResultsType.Succeeded);

            results = BCS.Services.AddressService.Lookup(cn, new ViewModels.AddressLookupCriteriaViewModel() { PostalCode = "50322" });
            Assert.IsTrue(results.Status == ViewModels.AddressLookupResultsType.Succeeded);
        }
        #endregion

    }
}
