﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS._Tests.Services
{
    [TestClass]
    public class ClientServiceTests : StandardBaseTest
    {
        #region AIC
        [TestMethod]
        public void AICSearch()
        {
            var criteria = new ViewModels.ClientSearchCriteriaViewModel() { ClientCoreId = "212724" };
            var results = BCS.Services.ClientService.Search(Application.CompanyMap["AIC"], criteria);
            Assert.IsTrue(results.Succeeded);
        }

        [TestMethod]
        public void AICSearchPhoneNumber()
        {
            var criteria = new ViewModels.ClientSearchCriteriaViewModel()
            {
                PhoneNumber = "5159990373"
            };
            var results = BCS.Services.ClientService.Search(Application.CompanyMap["AIC"], criteria);
            Assert.IsTrue(results.Succeeded);
        }

        [TestMethod]
        public void AICSearchMultiple()
        {
            var criteria = new ViewModels.ClientSearchCriteriaViewModel()
            {
                BusinessName = "Youngers Consulting",
                City = "Urbandale",
                State = "IA",
                PhoneNumber = "5159990373"
            };

            var results = BCS.Services.ClientService.Search(Application.CompanyMap["AIC"], criteria);
            Assert.IsTrue(results.Succeeded);
        }
        #endregion

        #region BARS
        [TestMethod]
        public void BARSSearch()
        {
            var criteria = new ViewModels.ClientSearchCriteriaViewModel() { BusinessName = "biozyme" };
            var results = BCS.Services.ClientService.Search(Application.CompanyMap["BARS"], criteria);
            Assert.IsTrue(results.Succeeded);
        }
        #endregion

        #region CCIC
        [TestMethod]
        public void CCICSearchPhoneNumber()
        {
            var criteria = new ViewModels.ClientSearchCriteriaViewModel()
            {
                //PhoneNumber = "5159990373"
                //PhoneNumber = "5159990375"
                PhoneNumber = "5154404960"
            };
            var results = BCS.Services.ClientService.Search(Application.CompanyMap["CCIC"], criteria);
            Assert.IsTrue(results.Succeeded);
        }
        #endregion

        #region CWG
        [TestMethod]
        public void CWGClassCodes()
        {
            var classCodes = BCS.Services.ClientService.GetClassCodes(Application.CompanyMap["CWG"], new ClientSystemId { Id = 689033 }, null);
            Assert.IsTrue(classCodes.Count > 0);
        }
        #endregion

        #region RIC
        [TestMethod]
        public void RICSearch()
        {
            var criteria = new ViewModels.ClientSearchCriteriaViewModel() { BusinessName = "scottz" };
            var results = BCS.Services.ClientService.Search(Application.CompanyMap["RIC"], criteria);
            Assert.IsTrue(results.Succeeded);
        }
        #endregion

        #region BMAG
        [TestMethod]
        public void BMAGSearch()
        {
            var criteria = new ViewModels.ClientSearchCriteriaViewModel() { ClientCoreId = "77884" };
            var results = BCS.Services.ClientService.Search(Application.CompanyMap["BMG"], criteria);
            Assert.IsTrue(results.Succeeded);
        }
        #endregion
    }
}
