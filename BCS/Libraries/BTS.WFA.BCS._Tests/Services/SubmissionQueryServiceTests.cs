﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS._Tests.Services
{
    [TestClass]
    public class SubmissionQueryServiceTests : StandardBaseTest
    {
        [TestMethod]
        public void DynamicDupTest()
        {
            var cn = Application.CompanyMap["BFM"];
            var criteria = new SubmissionDuplicateSearchCriteriaViewModel()
            {
                Clients = new List<SubmissionDuplicateSearchCriteriaClientViewModel>() { new SubmissionDuplicateSearchCriteriaClientViewModel { ClientId = 5558303 } },
                EffectiveDate = DateTime.Now.Date,
                AgencyCode = "04089",
                PolicySymbolCode = "BR"
            };
            var submissions = BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null);

        }

        [TestMethod]
        public void RegionalsDupTest()
        {
            var cn = Application.CompanyMap["CWG"];
            var criteria = new SubmissionDuplicateSearchCriteriaViewModel()
            {
                Clients = new List<SubmissionDuplicateSearchCriteriaClientViewModel>() { new SubmissionDuplicateSearchCriteriaClientViewModel() },
                EffectiveDate = DateTime.Now.Date,
            };
            //CWG - General Date/Expired Checks
            criteria.Clients[0].ClientId = 690846;//Has 1 that's too old, 1 new that's expired
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 0);

            //CWG - Client/Policy Specific
            criteria.Clients[0].ClientId = 697264;
            criteria.PolicySymbolCode = null;
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "ADV";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "ABC";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 0);

            //AIC - Client/Policy Specific
            cn = Application.CompanyMap["AIC"];
            criteria.Clients[0].ClientId = 217247;
            criteria.PolicySymbolCode = null;
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "CPA";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "ABC";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 0);

            //BMG - Client/Policy Specific
            cn = Application.CompanyMap["BMG"];
            criteria.Clients[0].ClientId = 6090037109;
            criteria.PolicySymbolCode = null;
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "ADL";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "ABC";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 0);

            //USG - Client/Policy Specific
            cn = Application.CompanyMap["USG"];
            criteria.Clients[0].ClientId = 391854;
            criteria.PolicySymbolCode = null;
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "CA";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "ABC";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 0);

            //BRS - Client/Policy Specific
            cn = Application.CompanyMap["BRS"];
            criteria.Clients[0].ClientId = 74575;
            criteria.PolicySymbolCode = null;
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "BPK";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "ABC";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 0);

            //BNP - Client/Policy Specific
            cn = Application.CompanyMap["BNP"];
            criteria.Clients[0].ClientId = 599232;
            criteria.PolicySymbolCode = null;
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 8);
            criteria.PolicySymbolCode = "GA";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 2);
            criteria.PolicySymbolCode = "ABC";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 0);

            //BARS - Client/Policy Specific
            cn = Application.CompanyMap["BARS"];
            criteria.Clients[0].ClientId = 1100096640;
            criteria.PolicySymbolCode = null;
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "XCP";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "ABC";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 0);

            //BFM - Client/Policy Specific
            cn = Application.CompanyMap["BFM"];
            criteria.Clients[0].ClientId = 1100096521;
            criteria.PolicySymbolCode = null;
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "CO";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 1);
            criteria.PolicySymbolCode = "ABC";
            Assert.IsTrue(BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null).Count == 0);
        }

        #region CWG
        [TestMethod]
        public void CWGPerformanceTestGet()
        {
            var cn = Application.CompanyMap["CWG"];
            var timer = new System.Diagnostics.Stopwatch();
            timer.Start();
            var submission = BCS.Services.SubmissionQueryService.GetByNumber(cn, new SubmissionNumberViewModel { Number = 190887, Sequence = 1 }, null);
            timer.Stop();

            var timer2 = new System.Diagnostics.Stopwatch();
            timer2.Start();
            var submission2 = BCS.Services.SubmissionQueryService.GetByNumber(cn, new SubmissionNumberViewModel { Number = 190886, Sequence = 1 }, null);
            timer2.Stop();

            var timer3 = new System.Diagnostics.Stopwatch();
            timer3.Start();
            var submission3 = BCS.Services.SubmissionQueryService.GetByNumber(cn, new SubmissionNumberViewModel { Number = 190885, Sequence = 1 }, null);
            timer3.Stop();

            var totalTime = timer.Elapsed;//{00:00:01.1152049}
            var totalTime2 = timer2.Elapsed;//{00:00:00.5312440}
            var totalTime3 = timer3.Elapsed;//{00:00:00.6956169}
        }

        [TestMethod]
        public void CWGPerformanceTestVariousGets()
        {
            var cn = Application.CompanyMap["CWG"];
            //warmup
            BCS.Services.SubmissionQueryService.GetByNumber(cn, new SubmissionNumberViewModel { Number = 12345, Sequence = 1 }, null);

            var timerN = new System.Diagnostics.Stopwatch();
            timerN.Start();
            var submissionN = BCS.Services.SubmissionQueryService.GetByNumber(cn, new SubmissionNumberViewModel { Number = 190884, Sequence = 1 }, null);
            timerN.Stop();

            var timerA = new System.Diagnostics.Stopwatch();
            timerA.Start();
            var submissionA = BCS.Services.SubmissionQueryService.GetApplicationSubmissions(cn, new SubmissionNumberViewModel { Number = 190884, Sequence = 1 }, null);
            timerA.Stop();

            var timerANonC = new System.Diagnostics.Stopwatch();
            timerANonC.Start();
            var submissionANonC = BCS.Services.SubmissionQueryService.GetClientsNonApplicationSubmissions(cn, new SubmissionNumberViewModel { Number = 190884, Sequence = 1 }, null, null);
            timerANonC.Stop();

            var totalTimeN = timerN.Elapsed;//threaded: {00:00:00.9077550}
            var totalTimeA = timerA.Elapsed;//threaded: {00:00:00.7003863}
            var totalTimeANonC = timerANonC.Elapsed;//tuned threaded: {00:00:00.9263314}
        }

        [TestMethod]
        public void CWGPerformanceClientTest()
        {
            var cn = Application.CompanyMap["CWG"];
            var submissions = BCS.Services.SubmissionQueryService.GetClientsSubmissions(cn, new ClientSystemId { Id = 689033 }, null, null);
        }

        [TestMethod]
        public void CWGApplicationPerformanceTest()
        {
            var cn = Application.CompanyMap["CWG"];
            var submissions = BCS.Services.SubmissionQueryService.GetApplicationSubmissions(cn, new SubmissionNumberViewModel() { Number = 190873, Sequence = 1 }, null);
        }

        [TestMethod]
        public void CWGDupCheck()
        {
            var cn = Application.CompanyMap["CWG"];
            var criteria = new SubmissionDuplicateSearchCriteriaViewModel()
            {
                EffectiveDate = new DateTime(2014, 11, 1),
                Clients = new List<SubmissionDuplicateSearchCriteriaClientViewModel>(new[] 
                { 
                    new SubmissionDuplicateSearchCriteriaClientViewModel() { ClientId = 689033L } 
                }),
                IgnoreApplicationId = "190864"
            };
            var results = BCS.Services.SubmissionQueryService.DuplicateSearch(cn, criteria, null);
        }
        #endregion

        #region USG
        [TestMethod]
        public void USGCmsTest()
        {
            var cn = Application.CompanyMap["USG"];
            var results = BCS.Services.SubmissionQueryService.GetClientsSubmissions(cn, new ClientSystemId { CmsId = new Guid("0eca64a5-6885-4627-8dc7-9d48f39898bc") }, null, null);
        }
        #endregion
    }
}
