﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS._Tests.Services
{
    [TestClass]
    public class DomainServiceTests : StandardBaseTest
    {
        #region AIC
        [TestMethod]
        public void AICSicCodeList()
        {
            var sic = BCS.Services.DomainService.SicCodeList(a => a);
        }

        [TestMethod]
        public void AICNaicsCodeList()
        {
            var naics = BCS.Services.DomainService.NaicsCodeList(a => a);
        }

        [TestMethod]
        public void AICSubmissionTypes()
        {
            var types = Application.CompanyMap["AIC"].Configuration.SubmissionTypes;
        }
        #endregion
    }
}
