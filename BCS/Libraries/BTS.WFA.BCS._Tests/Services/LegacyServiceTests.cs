﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS._Tests.Services
{
    [TestClass]
    public class LegacyServiceTests
    {
        [TestInitialize]
        public void Initialization()
        {
            Application.Initialize<Data.MockDb>(Application.GetStandardDebugHandlers());
        }

        #region AIC
        [TestMethod]
        public void AICInsert()
        {
            //Need a new key for clientcore info; this should change every 6 seconds
            int offset = (int)((DateTime.Now - new DateTime(2013, 12, 26)).TotalMinutes * 10);

            var client = new ViewModels.LegacyClientViewModel();
            client.ClientCoreClientId = 500000 + offset;
            client.CompanyNumber = "52";
            client.BusinessDescription = "Test Insert";
            client.PhoneNumber = "5159990373";
            client.Addresses.Add(new ViewModels.LegacyClientAddressViewModel() { ClientCoreAddressId = 600000 + offset, ClearanceAddressType = "Both", BuildingNumber = "3840", Address1 = "109th St", City = "Des Moines", State = "IA", PostalCode = "50322" });
            client.Names.Add(new ViewModels.LegacyClientNameViewModel() { SequenceNumber = 1, ClientNameType = "Insured" });

            BCS.Services.LegacyService.StoreClearanceClient("WRBTS\\jyoungers", 0, 0, client);
        }

        [TestMethod]
        public void AICUpdate()
        {
            var client = new ViewModels.LegacyClientViewModel();
            client.ClientCoreClientId = 216353;
            client.CompanyNumber = "52";
            client.BusinessDescription = "test";
            client.PhoneNumber = "5159990373";
            client.Addresses.Add(new ViewModels.LegacyClientAddressViewModel() { ClientCoreAddressId = 342477, ClearanceAddressType = "Both" });
            client.Names.Add(new ViewModels.LegacyClientNameViewModel() { SequenceNumber = 1, ClientNameType = "Insured" });

            BCS.Services.LegacyService.StoreClearanceClient("WRBTS\\jyoungers", 0, 0, client);
        }
        #endregion
    }
}
