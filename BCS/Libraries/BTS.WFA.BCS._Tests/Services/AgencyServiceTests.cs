﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS._Tests.Services
{
    [TestClass]
    public class AgencyServiceTests : StandardBaseTest
    {
        #region AIC
        [TestMethod]
        public void AICSync()
        {
            var results = BCS.Services.AgencyService.SyncAgency(Application.CompanyMap["AIC"], "99521", false);
        }
        [TestMethod]
        public void AICSyncPersonnel()
        {
            BCS.Services.UnderwritingService.SyncAllPersonnel(Application.CompanyMap["AIC"], (s) => Debug.WriteLine(s));
        }
        #endregion

        #region BARS
        [TestMethod]
        public void BARSSync()
        {
            var results = BCS.Services.AgencyService.SyncAgency(Application.CompanyMap["BARS"], "41359", false);
        }
        #endregion

        #region BLS
        [TestMethod]
        public void BLSSync()
        {
            var results = BCS.Services.AgencyService.SyncAgency(Application.CompanyMap["BLS"], "01005ME-1", false);
        }
        #endregion

        #region BRS
        [TestMethod]
        public void BRSSync()
        {
            var results = BCS.Services.AgencyService.SyncAgency(Application.CompanyMap["BRS"], "00429", false);
        }
        #endregion

        #region CWG
        [TestMethod]
        public void CWGSync()
        {
            var cn = Application.CompanyMap["CWG"];
            var results = BCS.Services.AgencyService.SyncAgency(cn, "00139", false);
        }

        [TestMethod]
        public void CWGPerformanceTest()
        {
            var cn = Application.CompanyMap["CWG"];
            var effectiveDate = DateTime.Now;

            var sw = new Stopwatch();
            sw.Start();
            var a = BCS.Services.AgencyService.Get(cn, "00120", null);
            sw.Stop();
            var result1 = sw.ElapsedMilliseconds;

            //sw.Restart();
            //BCS.Services.AgencyService.GetListPerformance(cn, "01", null, effectiveDate);
            //sw.Stop();
            //var result2 = sw.ElapsedMilliseconds;
        }
        #endregion

        #region RIC
        [TestMethod]
        public void RICSync()
        {
            var cn = Application.CompanyMap["RIC"];
            var results = BCS.Services.AgencyService.SyncAgency(cn, "80880", true);
        }
        #endregion

        #region USG
        [TestMethod]
        public void USGSync()
        {
            var cn = Application.CompanyMap["USG"];
            var results = BCS.Services.AgencyService.SyncAgency(cn, "04066", true);
        }
        #endregion

        #region BMG
        [TestMethod]
        public void BMGSync()
        {
            var results = BCS.Services.AgencyService.SyncAgency(Application.CompanyMap["BMG"], "00112", false);
        }
        #endregion
    }
}
