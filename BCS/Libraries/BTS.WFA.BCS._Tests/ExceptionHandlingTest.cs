﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.BCS._Tests.Services.AddressService
{
    [TestClass]
    public class ExceptionHandlingTest
    {
        [TestMethod]
        public void SqlException()
        {
            Application.Initialize<Data.SqlDb>(new ExceptionHandling.SqlErrorHandler());

            Application.HandleException(new Exception("SQL Exception Handling Test"), new { Info = "Test Data 1 2 3" });
        }
    }
}
