﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class ClientCoreSegmentViewModel : CodeListItemViewModel
    {
        public bool IsDefault { get; set; }
    }
}
