﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class SubmissionDisplayFieldViewModel
    {
        public string Header { get; set; }
        public string PropertyName { get; set; }
        public string DisplayName { get; set; }
        public bool IsCustomization { get; set; }
    }
}
