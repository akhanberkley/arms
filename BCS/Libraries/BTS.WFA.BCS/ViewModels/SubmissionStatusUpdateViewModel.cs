﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class SubmissionStatusUpdateViewModel : BaseViewModel
    {
        public string Status { get; set; }
        public string StatusReason { get; set; }
        public string StatusNotes { get; set; }
        public DateTime? StatusDate { get; set; }
    }
}
