﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class PersonnelViewModel : ApsBaseViewModel
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Initials { get; set; }
        public DateTime? RetirementDate { get; set; }

        public IQueryable<Data.Models.Person> ToQuery(IQueryable<Data.Models.Person> query)
        {
            bool isQueryable = false; long apsId;

            if (ApsId != null && long.TryParse(ApsId, out apsId))
            {
                query = query.Where(p => p.ApsId == apsId);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(FullName))
            {
                query = query.Where(p => p.FullName == FullName);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(Initials))
            {
                query = query.Where(p => p.Initials == Initials);
                isQueryable = true;
            }

            return (isQueryable) ? query : null;
        }
    }

    public class AgencyPersonnelViewModel : PersonnelViewModel
    {
        public bool IsPrimary { get; set; }
    }
}
