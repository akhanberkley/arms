﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class ClientSearchCriteriaViewModel
    {
        public DateTime? EffectiveDate { get; set; }
        public string ClientCoreId { get; set; }
        public string FEIN { get; set; }

        public string BusinessName { get; set; }
        public string PhoneNumber { get; set; }

        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        public long? PortfolioId { get; set; }
        public string PortfolioName { get; set; }

        public bool HasFullAddress { get { return !String.IsNullOrEmpty(Address) && !String.IsNullOrEmpty(City) && !String.IsNullOrEmpty(State) && !String.IsNullOrEmpty(PostalCode); } }
    }

    public class ClientCrossCompanySearchResultsViewModel
    {
        public CompanySummaryViewModel Company { get; set; }
        public long ClientId { get; set; }
    }

    public class ClientSearchResultsViewModel
    {
        public ClientSearchResultsViewModel()
        {
            Failures = new List<ClientSearchFailureViewModel>();
            Matches = new List<ClientSummaryViewModel>();
        }

        public List<ClientSearchFailureViewModel> Failures { get; set; }
        public bool CriteriaNotSpecific { get; set; }
        public bool Succeeded { get { return Failures.Count == 0 && !CriteriaNotSpecific; } }

        public List<ClientSummaryViewModel> Matches { get; set; }
    }

    public class ClientSearchFailureViewModel
    {
        public ClientSearchResultsType Status { get; set; }
        public string Criteria { get; set; }
        public string StatusMessage { get; set; }
    }

    public enum ClientSearchResultsType
    {
        Succeeded,
        TooManyResults,
        TimedOut,
        Failed
    }
}
