﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class PolicySystemViewModel : CodeListItemViewModel
    {
        public IQueryable<Data.Models.PolicySystem> ToQuery(IQueryable<Data.Models.PolicySystem> query)
        {
            bool isQueryable = false;

            if (!String.IsNullOrEmpty(Code))
            {
                query = query.Where(c => c.Abbreviation == Code);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(Description))
            {
                query = query.Where(c => c.Description == Description);
                isQueryable = true;
            }

            return (isQueryable) ? query : null;
        }
    }
}
