﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class OtherCarrierViewModel : CodeListItemViewModel
    {
        public IQueryable<Data.Models.OtherCarrier> ToQuery(IQueryable<Data.Models.OtherCarrier> query)
        {
            bool isQueryable = false;

            if (!String.IsNullOrEmpty(Code))
            {
                query = query.Where(c => c.Code == Code);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(Description))
            {
                query = query.Where(c => c.Description == Description);
                isQueryable = true;
            }

            return (isQueryable) ? query : null;
        }
    }
}
