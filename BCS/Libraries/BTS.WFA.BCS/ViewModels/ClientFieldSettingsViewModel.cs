﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.BCS.ViewModels
{
    public class ClientFieldSettingsViewModel
    {
        private ClientFieldViewModel contactInformation = new ClientFieldViewModel();
        public ClientFieldViewModel ContactInformation { get { return contactInformation; } }
        private ClientFieldViewModel fein = new ClientFieldViewModel();
        public ClientFieldViewModel FEIN { get { return fein; } }
        private ClientFieldViewModel sicCode = new ClientFieldViewModel();
        public ClientFieldViewModel SicCode { get { return sicCode; } }
        private ClientFieldViewModel naicsCode = new ClientFieldViewModel();
        public ClientFieldViewModel NaicsCode { get { return naicsCode; } }
        private ClientFieldViewModel accountPremium = new ClientFieldViewModel();
        public ClientFieldViewModel AccountPremium { get { return accountPremium; } }
        private ClientFieldViewModel businessDescription = new ClientFieldViewModel();
        public ClientFieldViewModel BusinessDescription { get { return businessDescription; } }
        private ClientFieldViewModel portfolioInformation = new ClientFieldViewModel();
        public ClientFieldViewModel PortfolioInformation { get { return portfolioInformation; } }
        private ClientFieldViewModel clientCoreSegment = new ClientFieldViewModel();
        public ClientFieldViewModel ClientCoreSegment { get { return clientCoreSegment; } }
        private ClientFieldViewModel experianId = new ClientFieldViewModel();
        public ClientFieldViewModel ExperianId { get { return experianId; } }
    }

    public class ClientFieldViewModel
    {
        public bool Enabled { get; set; }
        public bool VisibleOnAdd { get; set; }
        public bool Required { get; set; }
        public string Label { get; set; }
    }
}
