﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class ClassCodeViewModel : BaseViewModel
    {
        public ClassCodeViewModel()
        {
            HazardGrades = new List<CodeListItemViewModel>();
            CompanyCustomizations = new List<CompanyCustomizationValueViewModel>();
        }

        public string CodeValue { get; set; }
        public string GLCodeValue { get; set; }
        public string Description { get; set; }

        public SicCodeViewModel SicCode { get; set; }
        public NaicsCodeViewModel NaicsCode { get; set; }

        public List<CodeListItemViewModel> HazardGrades { get; set; }
        public List<CompanyCustomizationValueViewModel> CompanyCustomizations { get; set; }

        public IQueryable<Data.Models.ClassCode> ToQuery(IQueryable<Data.Models.ClassCode> query)
        {
            bool isQueryable = false;

            if (!String.IsNullOrEmpty(CodeValue))
            {
                query = query.Where(c => c.CodeValue == CodeValue);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(GLCodeValue))
            {
                query = query.Where(c => c.GLCodeValue == GLCodeValue);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(Description))
            {
                query = query.Where(c => c.Description == Description);
                isQueryable = true;
            }

            return (isQueryable) ? query : null;
        }
    }
}
