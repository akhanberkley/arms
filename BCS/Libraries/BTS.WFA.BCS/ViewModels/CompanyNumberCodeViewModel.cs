﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class CompanyNumberCodeViewModel : BaseViewModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
