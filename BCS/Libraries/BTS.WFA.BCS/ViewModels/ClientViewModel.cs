﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.Domains;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class ClientSummaryViewModel : BaseViewModel
    {
        public ClientSummaryViewModel()
        {
            AdditionalNames = new List<ClientNameViewModel>();
            Addresses = new List<ClientAddressViewModel>();
            Contacts = new List<ClientContactViewModel>();
            Portfolio = new ClientPortfolioViewModel();
        }

        public ClientNameViewModel PrimaryName { get; set; }
        public List<ClientNameViewModel> AdditionalNames { get; set; }

        private string companyNumber;
        [IgnoreDataMember]
        public string CompanyNumber
        {
            get { return companyNumber; }
            set
            {
                companyNumber = value;
                Company = Application.CompanyMap.Values.First(cn => cn.Summary.CompanyNumber == value).Summary;
            }
        }

        public CompanySummaryViewModel Company { get; set; }

        public Guid? CmsId { get; set; }
        public long? ClientCoreId { get; set; }
        public CodeListItemViewModel ClientCoreSegment { get; set; }

        public string FEIN { get; set; }
        public ClientPortfolioViewModel Portfolio { get; set; }

        public List<ClientAddressViewModel> Addresses { get; set; }
        public List<ClientContactViewModel> Contacts { get; set; }
    }

    [KnownType(typeof(ClientViewModel))]
    public class ClientViewModel : ClientSummaryViewModel
    {
        public ClientViewModel()
        {
            CompanyCustomizations = new List<CompanyCustomizationValueViewModel>();
        }

        public long? ExperianId { get; set; }
        public string BusinessDescription { get; set; }
        public SicCodeViewModel SicCode { get; set; }
        public NaicsCodeViewModel NaicsCode { get; set; }

        public decimal? AccountPremium { get; set; }

        public List<CompanyCustomizationValueViewModel> CompanyCustomizations { get; set; }
    }

    public class ClientNameViewModel
    {
        public Guid? CmsId { get; set; }
        public int? SequenceNumber { get; set; }
        public string NameType { get; set; }
        public string BusinessName { get; set; }
        public DateTime? EffectiveDate { get; set; }
    }

    public class ClientAddressViewModel : AddressViewModel
    {
        public ClientAddressViewModel()
        {
            AddressTypes = new List<string>();
        }

        public int? ClientCoreAddressId { get; set; }
        public Guid? CmsId { get; set; }
        public bool IsPrimary { get; set; }
        public List<string> AddressTypes { get; set; }
    }

    public class ClientContactViewModel
    {
        public int? SequenceNumber { get; set; }
        public string Value { get; set; }
        public ContactType ContactType { get; set; }
    }

    public class ClientPortfolioViewModel
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public int? ClientCount { get; set; }
    }
}
