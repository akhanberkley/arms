﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class CompanyCustomizationReorderViewModel
    {
        public int? CodeTypeId { get; set; }
        public int? AttributeId { get; set; }
    }
}
