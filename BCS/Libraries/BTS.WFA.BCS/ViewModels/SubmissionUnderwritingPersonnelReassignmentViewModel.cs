﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class SubmissionUnderwritingPersonnelReassignmentViewModel : BaseViewModel
    {
        public SubmissionUnderwritingPersonnelReassignmentViewModel()
        {
            Submissions = new List<SubmissionIdViewModel>();
        }

        public List<SubmissionIdViewModel> Submissions { get; set; }
        public PersonnelViewModel Underwriter { get; set; }
        public PersonnelViewModel UnderwritingAnalyst { get; set; }
        public PersonnelViewModel UnderwritingTechnician { get; set; }
    }
}
