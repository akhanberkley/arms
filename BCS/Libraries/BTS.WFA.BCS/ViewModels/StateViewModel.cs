﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class StateViewModel
    {
        public string StateName { get; set; }
        public string Abbreviation { get; set; }
    }
}
