﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class ApsLogItemViewModel
    {
        public int Id { get; set; }
        public string CompanyNumber { get; set; }
        public string Xml { get; set; }
    }
}
