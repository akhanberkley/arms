﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class AgencySyncResultViewModel
    {
        public bool Succeeded { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }

        public AgencyViewModel Agency { get; set; }
        public string ApsId { get; set; }
    }
}
