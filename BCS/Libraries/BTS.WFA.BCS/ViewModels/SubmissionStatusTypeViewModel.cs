﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class SubmissionStatusTypeViewModel
    {
        public SubmissionStatusTypeViewModel()
        {
            StatusCodes = new List<string>();
        }

        public string Name { get; set; }
        public List<string> StatusCodes { get; set; }
    }
}
