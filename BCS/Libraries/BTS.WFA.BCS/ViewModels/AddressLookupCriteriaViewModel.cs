﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class AddressLookupCriteriaViewModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }

        public bool IsEmpty { get { return String.IsNullOrEmpty(Address1) && String.IsNullOrEmpty(Address2) && String.IsNullOrEmpty(City) && String.IsNullOrEmpty(State) && String.IsNullOrEmpty(PostalCode) && String.IsNullOrEmpty(County); } }
    }
}
