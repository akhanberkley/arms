﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class SicCodeViewModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Group { get; set; }
        public string Division { get; set; }
    }
}
