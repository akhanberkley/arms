﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class AgentViewModel : ApsBaseViewModel
    {
        public string BrokerNo { get; set; }
        public DateTime? RetirementDate { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }

        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public string Fax { get; set; }
        public string EmailAddress { get; set; }

        public AgencySummaryViewModel Agency { get; set; }

        public IQueryable<Data.Models.Agent> ToQuery(IQueryable<Data.Models.Agent> query)
        {
            bool isQueryable = false; long apsId;

            if (ApsId != null && long.TryParse(ApsId, out apsId))
            {
                query = query.Where(a => a.ApsId == apsId);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(BrokerNo))
            {
                query = query.Where(a => a.BrokerNo == BrokerNo);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(FullName))
            {
                query = query.Where(a => a.FirstName + " " + a.Surname == FullName);
                isQueryable = true;
            }

            return (isQueryable) ? query : null;
        }
    }
}
