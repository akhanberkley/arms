﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class ApsBaseViewModel
    {
        public string ApsId { get; set; }

        //SQL Server can not correctly convert these long numbers into strings, so we'll do it in code
        [IgnoreDataMember]
        public long? ApsIdAsLong { set { ApsId = value.ToString(); } }
    }

    public class AgencyBranchViewModel : ApsBaseViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class AgencySummaryViewModel : ApsBaseViewModel
    {
        [IgnoreDataMember]
        public int Id { get; set; }

        public string Code { get; set; }
        public string Name { get; set; }
        public AddressViewModel Address { get; set; }
        
        public DateTime EffectiveDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? RenewalCancelDate { get; set; }

        public DateTime? ReferralDate { get; set; }
        public string ReferralAgencyCode { get; set; }
        
        public IQueryable<Data.Models.Agency> ToQuery(IQueryable<Data.Models.Agency> query, List<int> limitingAgencyIds)
        {
            bool isQueryable = false; long apsId;

            if (!String.IsNullOrEmpty(ApsId) && long.TryParse(ApsId, out apsId))
            {
                query = query.Where(a => a.ApsId == apsId);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(Code))
            {
                query = query.Where(a => a.AgencyNumber == Code);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(Name))
            {
                query = query.Where(a => a.AgencyName == Name);
                isQueryable = true;
            }

            if(limitingAgencyIds != null && limitingAgencyIds.Count > 0)
                query = query.Where(a => limitingAgencyIds.Contains(a.Id));

            return (isQueryable) ? query : null;
        }
    }
    public class AgencyViewModel : AgencySummaryViewModel
    {
        public AgencyViewModel()
        {
            Children = new List<AgencySummaryViewModel>();
            Branches = new List<AgencyBranchViewModel>();
        }

        public AgencySummaryViewModel Parent { get; set; }
        public List<AgencySummaryViewModel> Children { get; set; }

        public List<AgencyBranchViewModel> Branches { get; set; }

        public ContactViewModel Contact { get; set; }
        public string Note { get; set; }
    }
}
