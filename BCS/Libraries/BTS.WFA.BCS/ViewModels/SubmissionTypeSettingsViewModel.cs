﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class SubmissionTypeSettingsViewModel
    {
        [IgnoreDataMember]
        public int Id { get; set; }

        public string TypeName { get; set; }
        public bool TypeIsDefault { get; set; }
        public bool TypeIsRenewal { get { return TypeName == "Renewal"; } }

        public SubmissionDisplayFieldViewModel SummaryField { get; set; }

        private SubmissionTypeFieldViewModel allowMultipleSubmissions = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel AllowMultipleSubmissions { get { return allowMultipleSubmissions; } }
        private SubmissionTypeFieldViewModel sequenceNumber = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel SequenceNumber { get { return sequenceNumber; } }
        private SubmissionTypeFieldViewModel submittedDate = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel SubmittedDate { get { return submittedDate; } }
        private SubmissionTypeFieldViewModel effectiveDate = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel EffectiveDate { get { return effectiveDate; } }
        private SubmissionTypeFieldViewModel expirationDate = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel ExpirationDate { get { return expirationDate; } }
        private SubmissionTypeFieldViewModel agency = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel Agency { get { return agency; } }
        private SubmissionTypeFieldViewModel agent = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel Agent { get { return agent; } }
        private SubmissionTypeFieldViewModel classCodes = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel ClassCodes { get { return classCodes; } }
        private SubmissionTypeFieldViewModel underwritingUnit = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel UnderwritingUnit { get { return underwritingUnit; } }
        private SubmissionTypeFieldViewModel underwriter = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel Underwriter { get { return underwriter; } }
        private SubmissionTypeFieldViewModel underwritingAnalyst = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel UnderwritingAnalyst { get { return underwritingAnalyst; } }
        private SubmissionTypeFieldViewModel underwritingTechnician = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel UnderwritingTechnician { get { return underwritingTechnician; } }
        private SubmissionTypeFieldViewModel policyNumber = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel PolicyNumber { get { return policyNumber; } }
        private SubmissionTypeFieldViewModel policyMod = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel PolicyMod { get { return policyMod; } }
        private SubmissionTypeFieldViewModel policySymbol = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel PolicySymbol { get { return policySymbol; } }
        private SubmissionTypeFieldViewModel packagePolicySymbols = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel PackagePolicySymbols { get { return packagePolicySymbols; } }
        private SubmissionTypeFieldViewModel policyPremium = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel PolicyPremium { get { return policyPremium; } }
        private SubmissionTypeFieldViewModel policySystem = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel PolicySystem { get { return policySystem; } }
        private SubmissionTypeFieldViewModel status = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel Status { get { return status; } }
        private SubmissionTypeFieldViewModel statusNotes = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel StatusNotes { get { return statusNotes; } }
        private SubmissionTypeFieldViewModel priorCarrier = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel PriorCarrier { get { return priorCarrier; } }
        private SubmissionTypeFieldViewModel priorCarrierPremium = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel PriorCarrierPremium { get { return priorCarrierPremium; } }
        private SubmissionTypeFieldViewModel acquiringCarrier = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel AcquiringCarrier { get { return acquiringCarrier; } }
        private SubmissionTypeFieldViewModel acquiringCarrierPremium = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel AcquiringCarrierPremium { get { return acquiringCarrierPremium; } }
        private SubmissionTypeFieldViewModel notes = new SubmissionTypeFieldViewModel();
        public SubmissionTypeFieldViewModel Notes { get { return notes; } }
    }

    public class SubmissionTypeFieldViewModel
    {
        public bool Enabled { get; set; }
        public bool VisibleOnAdd { get; set; }
        public bool VisibleOnBPM { get; set; }
        public bool Required { get; set; }
        public string Label { get; set; }
    }
}
