﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.BCS.ViewModels
{
    public class SubmissionDuplicateSearchCriteriaViewModel
    {
        public SubmissionDuplicateSearchCriteriaViewModel()
        {
            Clients = new List<SubmissionDuplicateSearchCriteriaClientViewModel>();
        }

        public string SubmissionType { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public List<SubmissionDuplicateSearchCriteriaClientViewModel> Clients { get; set; }

        public string PolicySymbolCode { get; set; }
        public string AgencyCode { get; set; }
        public string IgnoreApplicationId { get; set; }
    }
    public class SubmissionDuplicateSearchCriteriaClientViewModel
    {
        public CompanySummaryViewModel Company { get; set; }
        public long ClientId { get; set; }
    }
}
