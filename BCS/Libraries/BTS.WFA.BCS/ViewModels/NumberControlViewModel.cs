﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class NumberControlViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string CurrentNumber
        {
            get
            {
                var number = ControlNumber.ToString();
                if (!String.IsNullOrEmpty(PaddingChar))
                    number = (PadHead) ? number.PadLeft(TotalLength, PaddingChar[0]) : number.PadRight(TotalLength, PaddingChar[0]);

                return number;
            }
        }

        [IgnoreDataMember]
        public string PaddingChar { get; set; }
        [IgnoreDataMember]
        public bool PadHead { get; set; }
        [IgnoreDataMember]
        public int TotalLength { get; set; }
        [IgnoreDataMember]
        public long ControlNumber { get; set; }
    }
}
