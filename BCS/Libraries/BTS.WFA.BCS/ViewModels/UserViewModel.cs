﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class UserSummaryViewModel : BaseViewModel
    {
        public int? Id { get; set; }
        public string UserName { get; set; }
        public bool Active { get; set; }
    }

    public class UserViewModel : UserSummaryViewModel
    {
        public UserViewModel()
        {
            Companies = new List<CodeListItemViewModel>();
            Roles = new List<string>();
        }

        public string Notes { get; set; }
        public string UserType { get; set; }

        public string ClientCoreSegmentCode { get; set; }

        public List<string> Roles { get; set; }
        public AgencySummaryViewModel LimitingAgency { get; set; }

        public CodeListItemViewModel DefaultCompany { get; set; }
        public List<CodeListItemViewModel> Companies { get; set; }
    }
}
