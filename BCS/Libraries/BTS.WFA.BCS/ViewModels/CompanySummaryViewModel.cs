﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.BCS.ViewModels
{
    public class CompanySummaryViewModel
    {
        public string CompanyNumber { get; set; }
        public string CompanyName { get; set; }
        public string Abbreviation { get; set; }
    }
}