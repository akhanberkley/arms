﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class UnderwritingUnitViewModel : ApsBaseViewModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Line { get; set; }

        public IQueryable<Data.Models.LineOfBusiness> ToQuery(IQueryable<Data.Models.LineOfBusiness> query)
        {
            bool isQueryable = false; long apsId;

            if (!String.IsNullOrEmpty(ApsId) && long.TryParse(ApsId, out apsId))
            {
                query = query.Where(lob => lob.ApsId == apsId);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(Code))
            {
                query = query.Where(lob => lob.Code == Code);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(Description))
            {
                query = query.Where(lob => lob.Description == Description);
                isQueryable = true;
            }

            return (isQueryable) ? query : null;
        }
    }

    public class AgencyUnderwritingUnitViewModel : UnderwritingUnitViewModel
    {
        public AgencyUnderwritingUnitViewModel()
        {
            Underwriters = new List<AgencyPersonnelViewModel>();
            Analysts = new List<AgencyPersonnelViewModel>();
            Technicians = new List<AgencyPersonnelViewModel>();
        }

        public List<AgencyPersonnelViewModel> Underwriters { get; set; }
        public List<AgencyPersonnelViewModel> Analysts { get; set; }
        public List<AgencyPersonnelViewModel> Technicians { get; set; }
    }
}
