﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class AddressViewModel
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }

        [IgnoreDataMember]
        public bool IsBlank
        {
            get
            {
                return String.IsNullOrEmpty(Address1) && String.IsNullOrEmpty(City) && String.IsNullOrEmpty(State) && String.IsNullOrEmpty(PostalCode);
            }
        }
    }
}
