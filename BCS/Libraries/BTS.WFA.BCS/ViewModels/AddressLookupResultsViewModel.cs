﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.ViewModels
{
    public class AddressLookupResultsViewModel
    {
        public AddressLookupResultsViewModel()
        {
            Matches = new List<AddressViewModel>();
        }

        public AddressLookupResultsType Status { get; set; }
        public string StatusMessage { get; set; }

        public List<AddressViewModel> Matches { get; set; }
    }

    public enum AddressLookupResultsType
    {
        Succeeded,
        Failed
    }
}
