﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class PolicySymbolSummaryViewModel : BaseViewModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public IQueryable<Data.Models.PolicySymbol> ToQuery(IQueryable<Data.Models.PolicySymbol> query)
        {
            bool isQueryable = false;

            if (!String.IsNullOrEmpty(Code))
            {
                query = query.Where(p => p.Code == Code);
                isQueryable = true;
            }
            if (!String.IsNullOrEmpty(Description))
            {
                query = query.Where(p => p.Description == Description);
                isQueryable = true;
            }

            return (isQueryable) ? query : null;
        }
    }

    public class PolicySymbolViewModel : PolicySymbolSummaryViewModel
    {
        public DateTime? RetirementDate { get; set; }
        public bool IsPrimaryOption { get; set; }
        public bool IsPackageOption { get; set; }
        public NumberControlViewModel NumberControl { get; set; }
        public string PolicyNumberPrefix { get; set; }
    }
}
