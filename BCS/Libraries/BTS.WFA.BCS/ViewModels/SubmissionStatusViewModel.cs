﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.BCS.ViewModels
{
    public class SubmissionStatusViewModel
    {
        public SubmissionStatusViewModel()
        {
            Reasons = new List<SubmissionStatusReasonViewModel>();
        }

        public string Code { get; set; }
        public bool IsDefault { get; set; }
        public List<SubmissionStatusReasonViewModel> Reasons { get; set; }
    }

    public class SubmissionStatusReasonViewModel
    {
        public string Code { get; set; }
        public bool IsActive { get; set; }
    }
}
