﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class LegacyClientViewModel : BaseViewModel
    {
        public LegacyClientViewModel()
        {
            Names = new List<LegacyClientNameViewModel>();
            Addresses = new List<LegacyClientAddressViewModel>();
            CompanyAttributes = new List<Tuple<int, string>>();
            CompanyCodes = new List<int>();
        }

        public long? ClientCoreClientId { get; set; }
        public string CompanyNumber { get; set; }
        public string BusinessDescription { get; set; }
        public string PhoneNumber { get; set; }

        public List<LegacyClientNameViewModel> Names { get; set; }
        public List<LegacyClientAddressViewModel> Addresses { get; set; }
        public List<Tuple<int, string>> CompanyAttributes { get; set; }
        public List<int> CompanyCodes { get; set; }
    }

    public class LegacyClientNameViewModel
    {
        public int? SequenceNumber { get; set; }
        public string ClientNameType { get; set; }

        public string BusinessName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }

        public DateTime? EffectiveDate { get; set; }
    }

    public class LegacyClientAddressViewModel : AddressViewModel
    {
        public string BuildingNumber { get; set; }
        public int ClearanceClientId { get; set; }
        public int? ClientCoreAddressId { get; set; }
        public DateTime? ClientCoreEffectiveDate { get; set; }

        public bool IsPrimary { get; set; }
        public string ClearanceAddressType { get; set; }
    }
}
