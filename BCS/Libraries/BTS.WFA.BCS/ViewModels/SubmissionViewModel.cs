﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class SubmissionNumberViewModel
    {
        public long Number { get; set; }
        public int Sequence { get; set; }

        public static SubmissionNumberViewModel Parse(string submissionNumber)
        {
            if (String.IsNullOrEmpty(submissionNumber))
                throw new ArgumentException("Submission Number can not be null");

            var numParts = submissionNumber.Split('-');
            return new SubmissionNumberViewModel { Number = long.Parse(numParts[0]), Sequence = (numParts.Length > 1) ? int.Parse(numParts[1]) : 1 };
        }

        public override string ToString()
        {
            return Number.ToString() + ((Sequence == 1) ? "" : "-" + Sequence.ToString());
        }
    }

    [KnownType(typeof(SubmissionIdViewModel))]
    public class SubmissionIdViewModel : BaseViewModel
    {
        public string Id
        {
            get { return (new SubmissionNumberViewModel() { Number = SubmissionNumber ?? 0, Sequence = SequenceNumber ?? 1 }).ToString(); }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    var num = SubmissionNumberViewModel.Parse(value);
                    SubmissionNumber = num.Number;
                    SequenceNumber = num.Sequence;
                }
            }
        }
        public long? SubmissionNumber { get; set; }
        public int? SequenceNumber { get; set; }
    }

    public class SubmissionSummaryViewModel : SubmissionIdViewModel
    {
        public SubmissionSummaryViewModel()
        {
        }

        [IgnoreDataMember]
        public int SubmissionDbId { get; set; }
        [IgnoreDataMember]
        public int? DuplicateOfSubmissionDbId { get; set; }
        [IgnoreDataMember]
        public int? AssociationSubmissionDbId { get; set; }
        [IgnoreDataMember]
        public Guid? PolicySymbolId { get; set; }
        [IgnoreDataMember]
        public int? PolicySystemId { get; set; }
        [IgnoreDataMember]
        public int? AcquiringCarrierId { get; set; }
        [IgnoreDataMember]
        public int? PriorCarrierId { get; set; }

        private string companyNumber;
        [IgnoreDataMember]
        public string CompanyNumber
        {
            get { return companyNumber; }
            set
            {
                companyNumber = value;
                Company = Application.CompanyMap.Values.First(cn => cn.Summary.CompanyNumber == value).Summary;
            }
        }

        public CompanySummaryViewModel Company { get; set; }

        public string SubmissionType { get; set; }

        public long? ClientCoreId { get; set; }
        public long? ClientCorePortfolioId { get; set; }
        public string InsuredName { get; set; }
        public string InsuredDBA { get; set; }

        public string PolicyNumber { get; set; }
        public short? PolicyMod { get; set; }

        public DateTime SubmittedDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string EffectivePeriod { get { return ((EffectiveDate != null) ? EffectiveDate.Value.ToString("MM/dd/yy") : "N/A") + " - " + ((ExpirationDate != null) ? ExpirationDate.Value.ToString("MM/dd/yy") : "N/A"); } }
    }

    [KnownType(typeof(SubmissionViewModel))]
    public class SubmissionViewModel : SubmissionSummaryViewModel
    {
        public SubmissionViewModel()
        {
            CompanyCustomizations = new List<CompanyCustomizationValueViewModel>();
            ClassCodes = new List<ClassCodeViewModel>();
            StatusHistory = new List<SubmissionStatusHistoryViewModel>();
            AssociatedSubmissions = new List<SubmissionSummaryViewModel>();
            AdditionalNames = new List<SubmissionAdditionalNameViewModel>();
            Addresses = new List<SubmissionAddressViewModel>();
            Notes = new List<SubmissionNoteViewModel>();
            PackagePolicySymbols = new List<PolicySymbolSummaryViewModel>();
        }

        public bool IsDeleted { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string Status { get; set; }
        public string StatusReason { get; set; }
        public string StatusNotes { get; set; }
        public DateTime? StatusDate { get; set; }

        public AgencySummaryViewModel Agency { get; set; }
        public AgentViewModel Agent { get; set; }
        public string AgentUnspecifiedReason { get; set; }

        public OtherCarrierViewModel PriorCarrier { get; set; }
        public decimal? PriorCarrierPremium { get; set; }
        public OtherCarrierViewModel AcquiringCarrier { get; set; }
        public decimal? AcquiringCarrierPremium { get; set; }

        public List<SubmissionNoteViewModel> Notes { get; set; }

        public bool CreatedWithOFACHit { get; set; }

        public decimal? PolicyPremium { get; set; }
        public PolicySystemViewModel PolicySystem { get; set; }
        public PolicySymbolSummaryViewModel PolicySymbol { get; set; }
        public List<PolicySymbolSummaryViewModel> PackagePolicySymbols { get; set; }

        public UnderwritingUnitViewModel UnderwritingUnit { get; set; }
        public PersonnelViewModel Underwriter { get; set; }
        public PersonnelViewModel UnderwritingAnalyst { get; set; }
        public PersonnelViewModel UnderwritingTechnician { get; set; }

        public List<CompanyCustomizationValueViewModel> CompanyCustomizations { get; set; }
        public List<ClassCodeViewModel> ClassCodes { get; set; }

        public List<SubmissionAdditionalNameViewModel> AdditionalNames { get; set; }
        public List<SubmissionAddressViewModel> Addresses { get; set; }
        public List<SubmissionStatusHistoryViewModel> StatusHistory { get; set; }

        public SubmissionSummaryViewModel DuplicateOfSubmission { get; set; }
        public List<SubmissionSummaryViewModel> AssociatedSubmissions { get; set; }
    }

    public class SubmissionAdditionalNameViewModel
    {
        public Guid Id { get; set; }
        public int? SequenceNumber { get; set; }
        public string BusinessName { get; set; }
    }
    public class SubmissionAddressViewModel : AddressViewModel
    {
        public Guid Id { get; set; }
        public int? ClientCoreAddressId { get; set; }
    }
    public class SubmissionNoteViewModel
    {
        public int? Id { get; set; }
        public string Text { get; set; }
        public string Author { get; set; }
        public DateTime Date { get; set; }
    }
    public class SubmissionStatusHistoryViewModel : BaseViewModel
    {
        public int? Id { get; set; }
        public DateTime? Date { get; set; }
        public string Status { get; set; }
        public string StatusReason { get; set; }
        public string StatusNote { get; set; }
        public string ChangeUser { get; set; }
        public DateTime? ChangeDate { get; set; }
        public bool IsActive { get; set; }
    }
}
