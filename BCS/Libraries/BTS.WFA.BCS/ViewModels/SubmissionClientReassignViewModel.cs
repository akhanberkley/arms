﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    public class SubmissionClientReassignViewModel : BaseViewModel
    {
        public long ClientCoreId { get; set; }
        public long? ClientCorePortfolioId { get; set; }
        public string InsuredName { get; set; }
        public string InsuredDBA { get; set; }
    }
}
