﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.ViewModels
{
    //Need to let the serializer know about these other classes it could be serializing when asked to serialize this class
    [KnownType(typeof(CompanyCustomizationAttributeViewModel))]
    [KnownType(typeof(CompanyCustomizationCodeTypeViewModel))]
    public class CompanyCustomizationViewModel : BaseViewModel
    {
        public CompanyCustomizationViewModel()
        {
            Options = new List<CompanyNumberCodeViewModel>();
        }

        public string Description { get; set; }
        public CompanyCustomizationUsage Usage { get; set; }
        public CompanyCustomizationDataType DataType { get; set; }
        public bool Required { get; set; }
        public bool ReadOnly { get; set; }
        public bool VisibleOnAdd { get; set; }
        public bool VisibleOnBPM { get; set; }
        public bool ExcludeValueOnCopy { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public List<CompanyNumberCodeViewModel> Options { get; set; }
        public CompanyNumberCodeViewModel DefaultOption { get; set; }
    }

    [KnownType(typeof(CompanyCustomizationTextValueViewModel))]
    [KnownType(typeof(CompanyCustomizationCodeValueViewModel))]
    public class CompanyCustomizationValueViewModel
    {
        public string Customization { get; set; }
        public string TextValue { get; set; }
        public CompanyNumberCodeViewModel CodeValue { get; set; }
    }

    public enum CompanyCustomizationUsage
    {
        Submission,
        Client,
        ClassCode
    }
    public enum CompanyCustomizationDataType
    {
        List = 0,
        Text = 1,
        Date = 2,
        Boolean = 3,
        Money = 4
    }

    #region Entity Framework Mapper Helper Classes
    public class CompanyCustomizationAttributeViewModel : CompanyCustomizationViewModel
    {
        [IgnoreDataMember]
        public Data.Models.CompanyNumberAttribute Attribute
        {
            set
            {
                Description = value.Label;
                Usage = value.ClientSpecific ? CompanyCustomizationUsage.Client : CompanyCustomizationUsage.Submission;
                DataType = (CompanyCustomizationDataType)value.AttributeDataTypeId;
                Required = value.Required;
                ReadOnly = value.ReadOnlyProperty;
                VisibleOnAdd = value.VisibleOnAdd;
                ExpirationDate = value.ExpirationDate;
                ExcludeValueOnCopy = value.ExcludeValueOnCopy;
                VisibleOnBPM = value.VisibleOnBPM;
            }
        }
    }
    public class CompanyCustomizationCodeTypeViewModel : CompanyCustomizationViewModel
    {
        [IgnoreDataMember]
        public Data.Models.CompanyNumberCodeType CodeType
        {
            set
            {
                Description = value.CodeName;
                Usage = value.ClientSpecific ? CompanyCustomizationUsage.Client : (value.ClassCodeSpecific) ? CompanyCustomizationUsage.ClassCode : CompanyCustomizationUsage.Submission;
                DataType = CompanyCustomizationDataType.List;
                Required = value.Required;
                ReadOnly = false;
                VisibleOnAdd = value.VisibleOnAdd;
                ExpirationDate = value.ExpirationDate;
                ExcludeValueOnCopy = value.ExcludeValueOnCopy;
                VisibleOnBPM = value.VisibleOnBPM;
            }
        }
    }

    public class CompanyCustomizationTextValueViewModel : CompanyCustomizationValueViewModel { }
    public class CompanyCustomizationCodeValueViewModel : CompanyCustomizationValueViewModel { }
    #endregion
}
