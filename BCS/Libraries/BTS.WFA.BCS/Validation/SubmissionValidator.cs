﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BTS.WFA.BCS.Services;
using BTS.WFA.BCS.ViewModels;

namespace BTS.WFA.BCS.Validation
{
    public class SubmissionValidator
    {
        public static void Validate(CompanyDetail cd, ViewModels.SubmissionNumberViewModel submissionNumber, ViewModels.SubmissionViewModel item, bool generatePolicyNumber)
        {
            bool isNew = (submissionNumber == null);

            if (item.ClientCoreId == null && !isNew)
                item.AddValidation("Client", "Client is required");

            var type = cd.Configuration.SubmissionTypes.FirstOrDefault(t => t.TypeName == item.SubmissionType);
            if (type == null)
                item.AddValidation("SubmissionType", "Submission Type is required");
            else
            {
                if (String.IsNullOrEmpty(item.Status))
                    item.AddValidation("Status", "Status is required");
                if (String.IsNullOrEmpty(item.InsuredName))
                    item.AddValidation("InsuredName", "Insured Name is required");
                if (item.Addresses.Count == 0)
                    item.AddValidation("Address", "An Address is required.");
                if (type.EffectiveDate.Required && item.EffectiveDate == null)
                    item.AddValidation("EffectiveDate", "{0} is required", type.EffectiveDate.Label);
                if (type.ExpirationDate.Required && item.ExpirationDate == null)
                    item.AddValidation("ExpirationDate", "{0} is required", type.ExpirationDate.Label);
                if (type.Agency.Required && item.Agency == null)
                    item.AddValidation("Agency", "{0} is required", type.Agency.Label);
                if (type.Agent.Required && !(item.Agent != null || !String.IsNullOrEmpty(item.AgentUnspecifiedReason)))
                    item.AddValidation("Agent", "{0} is required", type.Agent.Label);
                if (type.UnderwritingUnit.Required && item.UnderwritingUnit == null)
                    item.AddValidation("UnderwritingUnit", "{0} is required", type.UnderwritingUnit.Label);
                if (type.Underwriter.Required && item.Underwriter == null)
                    item.AddValidation("Underwriter", "{0} is required", type.Underwriter.Label);
                if (type.UnderwritingAnalyst.Required && item.UnderwritingAnalyst == null)
                    item.AddValidation("UnderwritingAnalyst", "{0} is required", type.UnderwritingAnalyst.Label);
                if (type.UnderwritingTechnician.Required && item.UnderwritingTechnician == null)
                    item.AddValidation("UnderwritingTechnician", "{0} is required", type.UnderwritingTechnician.Label);
                if (type.PolicySymbol.Required && item.PolicySymbol == null)
                    item.AddValidation("PolicySymbol", "{0} is required", type.PolicySymbol.Label);
                if (type.PackagePolicySymbols.Required && (item.PackagePolicySymbols == null  || item.PackagePolicySymbols.Count == 0))
                    item.AddValidation("PackagePolicySymbols", "{0} is required", type.PackagePolicySymbols.Label);
                if (type.PolicySystem.Required && item.PolicySystem == null)
                    item.AddValidation("PolicySystem", "{0} is required", type.PolicySystem.Label);

                if (type.PriorCarrier.Required && item.PriorCarrier == null)
                    item.AddValidation("PriorCarrier", "{0} is required", type.PriorCarrier.Label);
                if (type.PriorCarrierPremium.Required && item.PriorCarrierPremium == null)
                    item.AddValidation("PriorCarrierPremium", "{0} is required", type.PriorCarrierPremium.Label);
                if (type.AcquiringCarrier.Required && item.AcquiringCarrier == null)
                    item.AddValidation("AcquiringCarrier", "{0} is required", type.AcquiringCarrier.Label);
                if (type.AcquiringCarrierPremium.Required && item.AcquiringCarrierPremium == null)
                    item.AddValidation("AcquiringCarrierPremium", "{0} is required", type.AcquiringCarrierPremium.Label);

                var requiredCustomizations = CompanyCustomizationService.GetCustomizations(cd).Where(c => c.Usage == CompanyCustomizationUsage.Submission && c.Required && (c.ExpirationDate == null || c.ExpirationDate >= DateTime.Now));
                foreach (var customization in requiredCustomizations)
                {
                    var vmCustomization = item.CompanyCustomizations.FirstOrDefault(c => c.Customization == customization.Description);
                    if (vmCustomization == null || (customization.DataType == CompanyCustomizationDataType.List && vmCustomization.CodeValue == null) || (customization.DataType != CompanyCustomizationDataType.List && String.IsNullOrEmpty(vmCustomization.TextValue)))
                        item.AddValidation("Customization-" + customization.Description, "{0} is required", customization.Description);
                }

                using (var db = Application.GetDatabaseInstance())
                {
                    if (type.PolicyNumber.Required && !generatePolicyNumber)
                    {
                        if (String.IsNullOrEmpty(item.PolicyNumber))
                            item.AddValidation("PolicyNumber", "Policy Number is required");
                        else
                        {
                            if (cd.CompanyId == (int)Domains.Companies.CWG)
                            {
                                long policyNumber = 0;
                                if (!long.TryParse(item.PolicyNumber, out policyNumber))
                                    item.AddValidation("PolicyNumber", "Policy Number is not numeric");
                                else
                                {
                                    var dbNumberControl = db.NumberControl.FirstOrDefault(n => n.CompanyNumberId == cd.CompanyNumberId && n.NumberControlTypeId == 1);
                                    if (dbNumberControl.ControlNumber < policyNumber)
                                        item.AddValidation("PolicyNumber", "Policy Number should be greater than or equal to {0}", dbNumberControl.ControlNumber);
                                    else
                                    {
                                        var number = (submissionNumber != null) ? submissionNumber.Number : (long?)null;
                                        if (db.Submission.Any(s => s.CompanyNumberId == cd.CompanyNumberId && s.PolicyNumber == item.PolicyNumber && s.SubmissionNumber != number))
                                            item.AddValidation("PolicyNumber", "Policy Number already exists in clearance");
                                    }
                                }
                            }
                            else if (!(new Regex(cd.Configuration.PolicyNumberRegex)).IsMatch(item.PolicyNumber ?? ""))
                                item.AddValidation("PolicyNumber", "Please enter a valid policy number");
                        }
                    }

                    //If they changed the submission number, make sure there isn't already one w/ the new number
                    if (!isNew && submissionNumber.ToString() != item.Id)
                    {
                        if (item.SubmissionNumber == default(long))
                            item.AddValidation("SubmissionNumber", "Submission Number is required");
                        if (item.SequenceNumber == default(int))
                            item.AddValidation("SequenceNumber", "Sequence Number is required");
                        if (db.Submission.Any(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == item.SubmissionNumber && s.Sequence == item.SequenceNumber))
                            item.AddValidation("SubmissionNumber", "A submission with the number {0} and sequence {1} already exists", item.SubmissionNumber, item.SequenceNumber);
                    }
                }
            }
        }
    }

}
