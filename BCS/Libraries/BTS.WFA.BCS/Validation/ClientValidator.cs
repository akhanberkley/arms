﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.Validation
{
    public class ClientValidator
    {
        public static void Validate(CompanyDetail cd, ViewModels.ClientViewModel item, CodeListItemViewModel segmentOverride)
        {
            if (item.PrimaryName == null || String.IsNullOrEmpty(item.PrimaryName.BusinessName))
                item.AddValidation("BusinessName", "Business Name is required");
            else
                for (int i = 0; i < item.AdditionalNames.Count; i++)
                    if (String.IsNullOrEmpty(item.AdditionalNames[i].BusinessName))
                        item.AddValidation("AdditionalNames[" + i + "]-BusinessName", "Name can not be blank");

            if (item.Addresses.Count == 0 || item.Addresses.All(a => a.IsBlank))
                item.AddValidation("Address", "At least one address is required");
            else
                for (int i = 0; i < item.Addresses.Count; i++)
                    if (item.Addresses[i].IsBlank)
                        item.AddValidation("Address[" + i + "]", "Address can not be blank");
                    else
                    {
                        //CMS Logic - Not used for now
                        //if (String.IsNullOrEmpty(item.Addresses[i].Address1))
                        //    item.AddValidation("Address[" + i + "]-Address1", "Address Line 1 is required");
                        //if (String.IsNullOrEmpty(item.Addresses[i].PostalCode))
                        //    item.AddValidation("Address[" + i + "]-PostalCode", "Postal Code is required");
                        //if (String.IsNullOrEmpty(item.Addresses[i].City))
                        //    item.AddValidation("Address[" + i + "]-City", "City is required");
                        //if (String.IsNullOrEmpty(item.Addresses[i].State))
                        //    item.AddValidation("Address[" + i + "]-State", "State is required");
                    }

            var cf = cd.Configuration.ClientFields;
            if (cf.BusinessDescription.Required && String.IsNullOrEmpty(item.BusinessDescription))
                item.AddValidation("BusinessDescription", "Business Description is required");
            if (cf.FEIN.Required && String.IsNullOrEmpty(item.FEIN))
                item.AddValidation("FEIN", "FEIN is required");
            if (!String.IsNullOrEmpty(item.FEIN) && item.FEIN.Replace("-", "").Length != 9)
                item.AddValidation("FEIN", "FEIN must be 9 digits");
            if (cf.NaicsCode.Required && item.NaicsCode == null)
                item.AddValidation("NaicsCode", "NAICS Code is required");
            if (cf.SicCode.Required && item.SicCode == null)
                item.AddValidation("SicCode", "SIC Code is required");
            if (item.ClientCoreId != null && cf.PortfolioInformation.Enabled)
            {
                if (item.Portfolio == null || item.Portfolio.Id == null)
                    item.AddValidation("PortfolioId", "Portfolio Id is required");
            }
            if (segmentOverride != null && !cd.Configuration.ClientCoreSegments.Any(s => s.Code == segmentOverride.Code))
                item.AddValidation("ClientCoreSegment", "{0} is not a valid segment", segmentOverride.Code);

            if (cd.HasValidationSetting(Validators.ClientPhoneRequired))
            {
                var phoneContact = item.Contacts.FirstOrDefault(c => c.ContactType == Domains.ContactType.Phone);
                if (phoneContact == null || String.IsNullOrEmpty(phoneContact.Value))
                    item.AddValidation("PhoneNumber", "Please enter a Business Phone Number");
            }
        }
    }


}
