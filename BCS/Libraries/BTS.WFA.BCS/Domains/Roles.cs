﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.Domains
{
    public class Roles
    {
        public const string SystemAdministrator = "System Administrator";
        public const string Administrator_Users = "Administrator - Users";
        public const string Administrator_CustomFields = "Administrator - Custom Fields";
        public const string Administrator_ClassCodes = "Administrator - Class Codes";
        public const string Administrator_PolicySymbols = "Administrator - Policy Symbols";
        public const string Administrator_SubmissionTables = "Administrator - Submission Tables";
        public const string Administrator_OtherCarriers = "Administrator - Other Carriers";
        public const string ReadOnly = "Read Only";
        public const string Tools_PolicyNumberGenerator = "Tools - Policy Number Generator";
        public const string Action_DeleteSubmission = "Action - Delete Submission";
        public const string Action_RenumberSubmission = "Action - Renumber Submission";
        public const string Action_SwitchClient = "Action - Switch Client";
        public const string Action_SetClientSegment = "Action - Set Client Segment";
        public const string Action_SwitchUnderwritingPersonnel = "Action - Switch Underwriting Personnel";
    }
}
