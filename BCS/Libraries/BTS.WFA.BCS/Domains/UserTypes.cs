﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.Domains
{
    public class UserTypes
    {
        public const string SystemAdministrator = "System Administrator";
        public const string Read_Only = "Read Only";
        public const string User = "User";
    }
}
