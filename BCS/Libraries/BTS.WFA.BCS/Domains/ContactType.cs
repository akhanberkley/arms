﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.Domains
{
    public enum ContactType
    {
        Phone,
        Email
    }
}
