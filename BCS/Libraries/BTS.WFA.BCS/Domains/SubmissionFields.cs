﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.BCS.Domains
{
    class SubmissionFields
    {
        public static Guid AllowMultipleSubmissions = new Guid("D2386053-4423-428F-A828-81481008440D");
        public static Guid SequenceNumber = new Guid("D4540EDC-0D67-4B26-88EF-0C9F2BD7A038");

        public static Guid SubmittedDate = new Guid("9939ED4E-9F79-4017-92B7-B86A3988466A");
        public static Guid EffectiveDate = new Guid("71A16357-3F9C-4B07-AE2F-ABA712B55E07");
        public static Guid ExpirationDate = new Guid("A65D45E4-3B8B-4A27-A6A9-918B46AA7D1D");
        public static Guid EffectivePeriod = new Guid("9BCD2298-4625-4677-B8A7-7B0D34810B98");

        public static Guid Agency = new Guid("7231DF59-2459-46AF-A0C4-0A451C42D224");
        public static Guid Agent = new Guid("B9AF602C-8E96-475F-833E-FD8890DB2A10");

        public static Guid ClassCodes = new Guid("26DCA514-74C2-4E4E-9D35-EC120B468C32");

        public static Guid UnderwritingUnit = new Guid("4A776A4B-4D75-4666-9A59-A8C1402039D6");
        public static Guid Underwriter = new Guid("CDC4CA63-505E-4211-870C-B3849620C8AA");
        public static Guid UnderwritingAnalyst = new Guid("D0845F70-535C-41B7-A1F8-0A96574F43A6");
        public static Guid UnderwritingTechnician = new Guid("69377AC1-D496-460D-88E5-B40A29034775");
        
        public static Guid PolicyNumber = new Guid("7BC42623-1667-473C-AA99-16630327A75D");
        public static Guid PolicyMod = new Guid("36F65F08-6278-4A9A-B690-E8A04FDA445D");
        public static Guid PolicySymbol = new Guid("E88FA757-4B2A-4B37-B5BA-2B7B61D769F5");
        public static Guid PackagePolicySymbols = new Guid("BBBAF5B0-7072-440C-90D2-058DD3A79C8D");
        public static Guid PolicyPremium = new Guid("F2CB0BAC-5492-4C7B-838F-C6C50A22C70B");
        public static Guid PolicySystem = new Guid("575D0B44-E33F-40BC-8A61-DE0D4449587F");

        public static Guid Status = new Guid("4C4DDDF5-045F-46D6-83FC-151E8C70F30A");
        public static Guid StatusNotes = new Guid("266AD587-7D7D-42C7-BF10-F990EF46063C");

        public static Guid PriorCarrier = new Guid("FF0807F4-3009-43D3-8A3A-C24811F9B648");
        public static Guid PriorCarrierPremium = new Guid("76888306-BE7B-4F44-BA99-C860D3DEADFB");
        public static Guid AcquiringCarrier = new Guid("ABBDD884-BAAD-4164-9814-80D631B6E001");
        public static Guid AcquiringCarrierPremium = new Guid("B5D40301-24AB-4694-915B-51DFFAB51221");
        
        public static Guid Notes = new Guid("2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9");
    }
}
