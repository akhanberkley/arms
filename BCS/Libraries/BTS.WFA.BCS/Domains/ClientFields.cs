﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.BCS.Domains
{
    class ClientFields
    {
        public static Guid ContactInformation = new Guid("FD8DB0A2-3BB1-41B7-A692-9FE20647C9DB");
        public static Guid FEIN = new Guid("552893D4-C6EF-44C9-B295-307692BC0490");
        public static Guid SICCode = new Guid("6737EC59-7532-4806-A62C-1F78B3040DAB");
        public static Guid NAICSCode = new Guid("E343A002-1BEE-4094-94FC-E1750FCCAF73");
        public static Guid AccountPremium = new Guid("B1713A27-05B7-4B95-A36F-2788DAE81233");
        public static Guid BusinessDescription = new Guid("A236ADD0-728A-4741-81FA-FA17EBA22907");
        public static Guid PortfolioInformation = new Guid("8FBFB4CC-6682-4BFC-ADF7-3A9FEB597F34");
        public static Guid ClientCoreSegment = new Guid("F88E6069-8912-4B4D-835B-BC2C076B1E5A");
        public static Guid ExperianId = new Guid("D9C97462-C3C2-4116-9612-F635E5CEFEFE");
    }
}
