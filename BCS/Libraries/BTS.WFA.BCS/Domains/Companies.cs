﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.BCS.Domains
{
    public enum Companies
    {
        CWG = 1,
        NIC = 2,
        AIC = 3,
        BMG = 4,
        BMU = 5,
        USG = 6,
        CCIC = 7,
        PEI = 8,
        CSM = 9,
        BSA = 10,
        BSD = 11,
        SSR = 12,
        BRS = 13,
        VELA = 14,
        BAH = 15,
        BSE = 16,
        BSEL = 17,
        BLS = 18,
        BAV = 20,
        RIC = 21,
        XXX = 22,
        BNP = 23,
        BARS = 24,
        BTU = 25,
        BFM = 26,
        FIN = 27,
        BRAC = 28
    }
}
