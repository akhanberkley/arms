﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using System.Data.Entity;
using Int = BTS.WFA.Integration;
using System.Text.RegularExpressions;
using BTS.WFA.ViewModels;
using LinqKit;
using BTS.WFA.BCS.Validation;

namespace BTS.WFA.BCS.Services
{
    public class SubmissionStatusHistoryService
    {
        public static SubmissionStatusHistoryViewModel RevertStatus(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, SubmissionStatusHistoryViewModel item, SubmissionStatusUpdateViewModel update, string updateUser)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbSubmission = db.Submission.Include(s => s.SubmissionStatusHistory).FirstOrDefault(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence);
                if (dbSubmission == null)
                    item.AddValidation("Submission", "Could not find submission {0}", submissionNumber.ToString());
                else
                {
                    var dbHistoryItem = dbSubmission.SubmissionStatusHistory.FirstOrDefault(h => h.Id == item.Id);
                    if (dbHistoryItem == null)
                        item.AddValidation("Id", "Could not find a history entry with the id of {0}", item.Id);
                    else
                    {
                        foreach (var dbDeactiveHistory in dbSubmission.SubmissionStatusHistory.Where(h => h.Id > dbHistoryItem.Id).ToArray())
                            dbDeactiveHistory.Active = false;

                        var mapper = new Mapping.SubmissionToData(cd, db, dbSubmission, update.ValidationResults, null, false);
                        mapper.SetStatus(update, null, false, updateUser, DateTime.Now);

                        db.SaveChanges();
                    }
                }
            }

            return item;
        }
    }
}
