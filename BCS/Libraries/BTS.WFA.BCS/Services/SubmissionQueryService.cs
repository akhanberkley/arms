﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using System.Data.Entity;
using Int = BTS.WFA.Integration;
using System.Text.RegularExpressions;
using BTS.WFA.ViewModels;
using BTS.WFA.BCS.Mapping;
using LinqKit;
using System.Linq.Dynamic;

namespace BTS.WFA.BCS.Services
{
    public class SubmissionQueryService
    {
        public static List<CodeListItemViewModel> GetSummaryTables(CompanyDetail cd)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.SubmissionViewType.OrderBy(t => t.Name)
                                            .Select(t => new CodeListItemViewModel { Code = t.Name, Description = t.Description })
                                            .ToList();
            }
        }
        public static BaseViewModel SaveSummaryTable(CompanyDetail cd, string viewName, List<SubmissionDisplayFieldViewModel> fields)
        {
            var results = new BaseViewModel();
            using (var db = Application.GetDatabaseInstance())
            {
                var dbSubmissionViewType = db.SubmissionViewType.Where(t => t.Name == viewName).FirstOrDefault();
                if (dbSubmissionViewType == null)
                    results.AddValidation("ViewName", "Could not find submission table {0}", viewName);
                else
                {
                    var fieldNames = fields.Select(f => f.PropertyName).ToArray();
                    var dbSubmissionViewFields = db.SubmissionViewField.Include(vf => vf.SubmissionField).Where(vf => vf.CompanyNumberId == cd.CompanyNumberId && vf.SubmissionViewTypeId == dbSubmissionViewType.Id).ToList();
                    var selectedFields = db.CompanyNumberSubmissionField.Include(f => f.SubmissionField).Where(f => f.CompanyNumberId == cd.CompanyNumberId && fieldNames.Contains(f.SubmissionField.PropertyName)).ToArray();

                    //Remove those that are no longer in the view
                    foreach (var vf in dbSubmissionViewFields.Where(vf => !fieldNames.Contains(vf.SubmissionField.PropertyName)).ToArray())
                    {
                        db.SubmissionViewField.Remove(vf);
                        dbSubmissionViewFields.Remove(vf);
                    }

                    //Add the new ones
                    foreach (var f in selectedFields.Where(f => !dbSubmissionViewFields.Any(vf => vf.SubmissionFieldId == f.SubmissionFieldId)))
                    {
                        var dbNewField = db.SubmissionViewField.Add(new Data.Models.SubmissionViewField() { SubmissionField = f.SubmissionField, CompanyNumberId = cd.CompanyNumberId, SubmissionViewType = dbSubmissionViewType });
                        dbSubmissionViewFields.Add(dbNewField);
                    }

                    for (short i = 0; i < fieldNames.Length; i++)
                    {
                        var dbField = dbSubmissionViewFields.FirstOrDefault(vf => vf.SubmissionField.PropertyName == fieldNames[i]);
                        dbField.Sequence = i;
                    }

                    db.SaveChanges();
                }

                return results;
            }
        }

        public static List<SubmissionDisplayFieldViewModel> GetAllSummaryFields(CompanyDetail cd)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.CompanyNumberSubmissionField.Where(sf => sf.CompanyNumberId == cd.CompanyNumberId && sf.Enabled)
                                                      .OrderBy(sf => sf.SubmissionField.ColumnHeading)
                                                      .Select(f => new SubmissionDisplayFieldViewModel()
                                                      {
                                                          PropertyName = f.SubmissionField.PropertyName,
                                                          DisplayName = f.DisplayLabel ?? f.SubmissionField.DisplayLabel,
                                                          Header = f.SubmissionField.ColumnHeading,
                                                          IsCustomization = (f.SubmissionField.CompanyNumberAttributeId != null || f.SubmissionField.CompanyNumberCodeTypeId != null)
                                                      }).ToList();
            }
        }
        public static List<SubmissionDisplayFieldViewModel> GetSummaryTableFields(CompanyDetail cd, string viewName)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.SubmissionViewField.Where(f => f.CompanyNumberId == cd.CompanyNumberId && f.SubmissionViewType.Name == viewName)
                                             .OrderBy(f => f.Sequence)
                                             .Select(f => new SubmissionDisplayFieldViewModel()
                                             {
                                                 PropertyName = f.SubmissionField.PropertyName,
                                                 DisplayName = f.SubmissionField.DisplayLabel,
                                                 Header = f.ColumnHeading ?? f.SubmissionField.ColumnHeading,
                                                 IsCustomization = (f.SubmissionField.CompanyNumberAttributeId != null || f.SubmissionField.CompanyNumberCodeTypeId != null)
                                             }).ToList();
            }
        }

        public static IEnumerable<SubmissionIdViewModel> Search(CompanyDetail cd, bool crossCompany, Func<IQueryable, IQueryable> odataFilter, List<int> limitingAgencyIds, bool idsOnly)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var cnIds = (new[] { cd.CompanyNumberId }).ToList();
                if (crossCompany)
                    cnIds.AddRange(cd.SystemSettings.CrossClearanceCompanyNumbers.Select(c => Application.CompanyMap[c.Abbreviation].CompanyNumberId));
                var query = db.Submission.Where(a => cnIds.Contains(a.CompanyNumberId));

                if (idsOnly)
                {
                    query = PrepQuery(query, limitingAgencyIds, false);
                    query = odataFilter(query) as IQueryable<Data.Models.Submission>;
                    return query.Select(s => new SubmissionIdViewModel { SubmissionNumber = s.SubmissionNumber, SequenceNumber = s.Sequence }).ToList();
                }
                else
                    return SubmissionQueryService.GetByQuery(query, limitingAgencyIds, false, odataFilter);
            }
        }
        public static List<SubmissionSummaryViewModel> SearchAutoComplete(CompanyDetail cd, bool crossCompany, Func<IQueryable, IQueryable> odataFilter, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var cnIds = (new[] { cd.CompanyNumberId }).ToList();
                if (crossCompany)
                    cnIds.AddRange(cd.SystemSettings.CrossClearanceCompanyNumbers.Select(c => Application.CompanyMap[c.Abbreviation].CompanyNumberId));

                var query = db.Submission.Where(s => cnIds.Contains(s.CompanyNumberId));
                query = PrepQuery(query, limitingAgencyIds, false);
                query = odataFilter(query) as IQueryable<Data.Models.Submission>;

                return query.Select(DataToViewModel.SubmissionSummary()).ToList();
            }
        }

        public static SubmissionViewModel GetById(CompanyDetail cd, int submissionId, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
                return GetByQuery(db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.Id == submissionId), limitingAgencyIds, true).FirstOrDefault();
        }
        public static SubmissionViewModel GetByNumber(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
                return GetByQuery(db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence), limitingAgencyIds, true).FirstOrDefault();
        }

        public static List<SubmissionViewModel> GetClientsSubmissions(CompanyDetail cd, ClientSystemId clientSystemId, Func<IQueryable, IQueryable> odataFilter, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId);
                query = clientSystemId.AsSubmissionFilter(cd, db, query);

                return GetByQuery(query, limitingAgencyIds, false, odataFilter);
            }
        }

        public static List<SubmissionViewModel> GetAgencysSubmissions(CompanyDetail cd, string agencyCode, Func<IQueryable, IQueryable> odataFilter, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.Agency.AgencyNumber == agencyCode);
                return GetByQuery(query, limitingAgencyIds, false, odataFilter);
            }
        }

        public static List<SubmissionViewModel> DuplicateSearch(CompanyDetail cd, SubmissionDuplicateSearchCriteriaViewModel criteria, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                if (criteria == null || criteria.Clients.Count == 0)
                    return new List<SubmissionViewModel>();
                else
                {
                    Dictionary<CompanyDetail, List<long>> clientsByCompany = new Dictionary<CompanyDetail, List<long>>();
                    foreach (var client in criteria.Clients)
                    {
                        var clientCN = cd;
                        if (client.Company != null && !String.IsNullOrEmpty(client.Company.Abbreviation) && client.Company.Abbreviation != cd.Summary.Abbreviation)
                        {
                            clientCN = cd.SystemSettings.CrossClearanceCompanyNumbers.Where(c => c.Abbreviation == client.Company.Abbreviation).Select(c => Application.CompanyMap[c.Abbreviation]).FirstOrDefault();
                            if (clientCN == null)
                                throw new AccessViolationException(String.Format("You do not have access to company {0}", client.Company.Abbreviation));
                        }

                        if (clientsByCompany.ContainsKey(clientCN))
                            clientsByCompany[clientCN].Add(client.ClientId);
                        else
                            clientsByCompany.Add(clientCN, new List<long>(new[] { client.ClientId }));
                    }

                    int? ignoreAppId = null;
                    if (!String.IsNullOrEmpty(criteria.IgnoreApplicationId))
                    {
                        var ignoreAppSubmissionNumber = SubmissionNumberViewModel.Parse(criteria.IgnoreApplicationId);
                        ignoreAppId = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == ignoreAppSubmissionNumber.Number && s.Sequence == ignoreAppSubmissionNumber.Sequence)
                                                   .Select(s => s.AssociationSubmissionId)
                                                   .FirstOrDefault();
                    }

                    //Coalesce some values to "" since some companies compare the variable to a list (which will throw an exception if variable is null)
                    var queryDictionary = new Dictionary<string, object>()
                    {
                        {"_SubmissionType", criteria.SubmissionType},
                        {"_Today", DateTime.Now.Date},
                        {"_EffectiveDate", criteria.EffectiveDate ?? DateTime.Now.Date},
                        {"_PolicySymbolCode", criteria.PolicySymbolCode ?? ""},
                        {"_AgencyCode", criteria.AgencyCode ?? ""}
                    };

                    List<SubmissionViewModel> submissions = new List<SubmissionViewModel>();
                    foreach (var kvp in clientsByCompany)
                    {
                        var query = db.Submission.Where(s => s.CompanyNumberId == kvp.Key.CompanyNumberId && kvp.Value.Contains(s.ClientCoreClientId) && s.DuplicateOfSubmissionId == null);
                        if (ignoreAppId != null)
                            query = query.Where(s => s.AssociationSubmissionId != ignoreAppId);

                        //Dynamic linq pulled in from database
                        query = query.Where(kvp.Key.SystemSettings.DuplicateSubmissionQuery, queryDictionary);

                        submissions.AddRange(GetByQuery(query, limitingAgencyIds, false));
                    }

                    return submissions;
                }
            }
        }

        public static List<SubmissionViewModel> GetApplicationSubmissions(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var associationIdQuery = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence)
                                                      .Select(s => s.AssociationSubmissionId);
                var query = db.Submission.Where(s => associationIdQuery.Contains(s.AssociationSubmissionId));
                return GetByQuery(query.OrderBy(s => s.EffectiveDate).ThenBy(s => s.PolicyNumber), limitingAgencyIds, true);
            }
        }

        public static List<SubmissionViewModel> GetClientsNonApplicationSubmissions(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, List<int> limitingAgencyIds, Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var associationIdQuery = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence)
                                                      .Select(s => s.AssociationSubmissionId);
                var clientCoreIds = db.Submission.Where(s => associationIdQuery.Contains(s.AssociationSubmissionId))
                                                 .Select(s => s.ClientCoreClientId);
                var query = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId
                                                     && clientCoreIds.Contains(s.ClientCoreClientId)
                                                     && !associationIdQuery.Contains(s.AssociationSubmissionId));

                return GetByQuery(query, limitingAgencyIds, false, odataFilter);
            }
        }

        private static IQueryable<Data.Models.Submission> PrepQuery(IQueryable<Data.Models.Submission> query, List<int> limitingAgencyIds, bool allowDeletedSubmissions)
        {
            if (!allowDeletedSubmissions)
                query = query.Where(s => !s.Deleted);
            if (limitingAgencyIds != null && limitingAgencyIds.Count > 0)
                query = query.Where(s => limitingAgencyIds.Contains(s.AgencyId.Value));

            return query;
        }
        public static List<SubmissionViewModel> GetByQuery(IQueryable<Data.Models.Submission> query, List<int> limitingAgencyIds, bool allowDeletedSubmissions, Func<IQueryable, IQueryable> odataFilter = null)
        {
            query = PrepQuery(query, limitingAgencyIds, allowDeletedSubmissions);
            if (odataFilter != null)
                query = odataFilter(query) as IQueryable<Data.Models.Submission>;

            var submissions = query.AsExpandable().Select(DataToViewModel.Submission()).ToList();
            var submissionMap = submissions.ToDictionary(s => s.SubmissionDbId);

            var submissionIds = submissions.Select(matches => matches.SubmissionDbId).ToArray();
            if (submissionIds.Length > 0)
            {
                var companysSubmissionTypes = submissions.Select(s => Application.CompanyMap[s.Company.Abbreviation]).SelectMany(c => c.Configuration.SubmissionTypes).ToArray();

                List<Action> additionalQueries = new List<Action>();
                //Linked submissions
                additionalQueries.Add(() =>
                {
                    var dupSubmissionIds = submissions.Where(s => s.DuplicateOfSubmissionDbId.HasValue).Select(s => s.DuplicateOfSubmissionDbId.Value).ToArray();
                    var associationIds = submissions.Where(s => s.AssociationSubmissionDbId.HasValue).Select(s => s.AssociationSubmissionDbId.Value).ToArray();
                    if (dupSubmissionIds.Length > 0 || associationIds.Length > 0)
                    {
                        using (var db = Application.GetDatabaseInstance())
                        {

                            var summaries = db.Submission.Where(bs => dupSubmissionIds.Contains(bs.Id) || associationIds.Contains(bs.AssociationSubmissionId.Value))
                                                         .Select(DataToViewModel.SubmissionSummary())
                                                         .ToDictionary(s => s.SubmissionDbId);
                            foreach (var s in submissions)
                            {
                                if (s.DuplicateOfSubmissionDbId != null)
                                    s.DuplicateOfSubmission = summaries[s.DuplicateOfSubmissionDbId.Value];
                                if (s.AssociationSubmissionDbId != null)
                                    s.AssociatedSubmissions.AddRange(summaries.Values.Where(bs => bs.SubmissionDbId != s.SubmissionDbId && bs.AssociationSubmissionDbId == s.AssociationSubmissionDbId));
                            }
                        }
                    }
                });
                //Additional Names
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var nameMapper = DataToViewModel.SubmissionAdditionalName();
                        var data = db.SubmissionAdditionalName.AsExpandable().Where(n => submissionIds.Contains(n.SubmissionId))
                                                                             .OrderBy(n => n.BusinessName)
                                                                             .ThenBy(n => n.EntryDt)
                                                                             .Select(n => new
                                                                             {
                                                                                 SubmissionId = n.SubmissionId,
                                                                                 Name = nameMapper.Invoke(n)
                                                                             });
                        foreach (var n in data)
                            submissionMap[n.SubmissionId].AdditionalNames.Add(n.Name);
                    }
                });
                //Addresses
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var addressMapper = DataToViewModel.SubmissionAddress();
                        var data = db.SubmissionAddress.AsExpandable().Where(a => submissionIds.Contains(a.SubmissionId))
                                                                      .OrderBy(a => a.ClientCoreAddressId)
                                                                      .ThenBy(a => a.EntryDt)
                                                                      .Select(a => new
                                                                      {
                                                                          SubmissionId = a.SubmissionId,
                                                                          Address = addressMapper.Invoke(a)
                                                                      });
                        foreach (var a in data)
                            submissionMap[a.SubmissionId].Addresses.Add(a.Address);
                    }
                });
                //Underwriting
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var underwritingUnitMapper = DataToViewModel.UnderwritingUnit();
                        var personnelMapper = DataToViewModel.Personnel();
                        var data = db.Submission.AsExpandable().Where(s => submissionIds.Contains(s.Id))
                                                .Select(s => new
                                                {
                                                    Id = s.Id,
                                                    UnderwritingUnit = (s.LineOfBusiness.Any()) ? underwritingUnitMapper.Invoke(s.LineOfBusiness.FirstOrDefault()) : null,
                                                    Underwriter = (s.UnderwriterPersonId == null) ? null : personnelMapper.Invoke(s.UnderwriterPerson),
                                                    UnderwritingAnalyst = (s.AnalystPersonId == null) ? null : personnelMapper.Invoke(s.AnalystPerson),
                                                    UnderwritingTechnician = (s.TechnicianPersonId == null) ? null : personnelMapper.Invoke(s.TechnicianPerson)
                                                });

                        foreach (var d in data)
                        {
                            var s = submissionMap[d.Id];
                            s.UnderwritingUnit = d.UnderwritingUnit;
                            s.Underwriter = d.Underwriter;
                            s.UnderwritingAnalyst = d.UnderwritingAnalyst;
                            s.UnderwritingTechnician = d.UnderwritingTechnician;
                        }
                    }
                });
                //Agency
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var agencyMapper = DataToViewModel.AgencySummary();
                        var agentMapper = DataToViewModel.Agent(false);
                        var data = db.Submission.AsExpandable().Where(s => submissionIds.Contains(s.Id))
                                                .Select(s => new
                                                {
                                                    Id = s.Id,
                                                    Agency = (s.AgencyId == null) ? null : agencyMapper.Invoke(s.Agency),
                                                    Agent = (s.AgentId == null) ? null : agentMapper.Invoke(s.Agent),
                                                    AgentUnspecifiedReason = s.CompanyNumberAgentUnspecifiedReason.Select(r => r.AgentUnspecifiedReason.Reason).FirstOrDefault()
                                                });

                        foreach (var d in data)
                        {
                            var s = submissionMap[d.Id];
                            s.Agency = d.Agency;
                            s.Agent = d.Agent;
                            s.AgentUnspecifiedReason = d.AgentUnspecifiedReason;
                        }
                    }
                });
                //Customizations
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var codeMapper = DataToViewModel.CompanyNumberCode();
                        var data = db.Submission.AsExpandable().Select(s => new
                        {
                            Id = s.Id,
                            Codes = s.CompanyNumberCode.Select(c => new CompanyCustomizationCodeValueViewModel()
                                                                        {
                                                                            Customization = c.CompanyNumberCodeType.CodeName,
                                                                            CodeValue = codeMapper.Invoke(c)
                                                                        }),
                            Attributes = s.SubmissionCompanyNumberAttributeValue.Select(a => new CompanyCustomizationTextValueViewModel()
                                                                        {
                                                                            Customization = a.CompanyNumberAttribute.Label,
                                                                            TextValue = a.AttributeValue
                                                                        })
                        }).Where(s => submissionIds.Contains(s.Id));

                        foreach (var s in data)
                        {
                            submissionMap[s.Id].CompanyCustomizations.AddRange(s.Codes);
                            submissionMap[s.Id].CompanyCustomizations.AddRange(s.Attributes);
                        }
                    }
                });
                //Status History
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var statusHistoryMapper = DataToViewModel.SubmissionStatusHistory(DateTime.Now);
                        var data = db.SubmissionStatusHistory.AsExpandable()
                                                             .Where(h => submissionIds.Contains(h.SubmissionId))
                                                             .OrderByDescending(h => h.Id)
                                                             .Select(h => new
                                                                  {
                                                                      SubmissionId = h.SubmissionId,
                                                                      StatusHistory = statusHistoryMapper.Invoke(h)
                                                                  });
                        foreach (var h in data)
                            submissionMap[h.SubmissionId].StatusHistory.Add(h.StatusHistory);
                    }
                });
                //Status Notes
                if (companysSubmissionTypes.Any(t => t.StatusNotes.Enabled))
                {
                    additionalQueries.Add(() =>
                    {
                        using (var db = Application.GetDatabaseInstance())
                        {
                            var notesMapper = DataToViewModel.SubmissionNote();
                            var data = db.SubmissionComment.AsExpandable()
                                                           .Where(c => submissionIds.Contains(c.SubmissionId))
                                                           .OrderBy(c => c.EntryDt)
                                                           .Select(c => new
                                                           {
                                                               SubmissionId = c.SubmissionId,
                                                               Note = notesMapper.Invoke(c),
                                                           });
                            foreach (var c in data)
                                submissionMap[c.SubmissionId].Notes.Add(c.Note);
                        }
                    });
                }
                //Class Codes
                if (companysSubmissionTypes.Any(t => t.ClassCodes.Enabled))
                {
                    additionalQueries.Add(() =>
                    {
                        using (var db = Application.GetDatabaseInstance())
                        {
                            var classCodeMapper = DataToViewModel.ClassCode();
                            foreach (var cc in db.Submission.Where(s => submissionIds.Contains(s.Id)).AsExpandable().Select(s => new { Id = s.Id, ClassCodes = s.ClassCodes.OrderBy(cc => cc.CodeValue).Select(cc => classCodeMapper.Invoke(cc)) }))
                                submissionMap[cc.Id].ClassCodes.AddRange(cc.ClassCodes);
                        }
                    });
                }
                //Prior and Acquiring Carrier
                if (companysSubmissionTypes.Any(t => t.AcquiringCarrier.Enabled || t.PriorCarrier.Enabled))
                {
                    var carrierIds = submissions.Where(s => s.AcquiringCarrierId.HasValue).Select(s => s.AcquiringCarrierId.Value)
                                                .Union(submissions.Where(s => s.PriorCarrierId.HasValue).Select(s => s.PriorCarrierId.Value))
                                                .Distinct()
                                                .ToArray();
                    if (carrierIds.Length > 0)
                        additionalQueries.Add(() =>
                        {
                            using (var db = Application.GetDatabaseInstance())
                            {
                                var data = db.OtherCarrier.Where(c => carrierIds.Contains(c.Id))
                                                          .ToDictionary((c) => c.Id, (c) => new OtherCarrierViewModel() { Code = c.Code, Description = c.Description });
                                foreach (var s in submissions)
                                {
                                    if (s.PriorCarrierId.HasValue)
                                        s.PriorCarrier = data[s.PriorCarrierId.Value];
                                    if (s.AcquiringCarrierId.HasValue)
                                        s.AcquiringCarrier = data[s.AcquiringCarrierId.Value];
                                }
                            }
                        });
                }

                //Policy System
                if (companysSubmissionTypes.Any(t => t.PolicySystem.Enabled))
                {
                    var policySystemIds = submissions.Where(s => s.PolicySystemId.HasValue).Select(s => s.PolicySystemId.Value).Distinct().ToArray();
                    if (policySystemIds.Length > 0)
                        additionalQueries.Add(() =>
                        {
                            using (var db = Application.GetDatabaseInstance())
                            {
                                var data = db.PolicySystem.Where(p => policySystemIds.Contains(p.Id))
                                                          .ToDictionary((p) => p.Id, (p) => new PolicySystemViewModel { Code = p.Abbreviation, Description = p.Description });
                                foreach (var s in submissions.Where(s => s.PolicySystemId.HasValue))
                                    s.PolicySystem = data[s.PolicySystemId.Value];
                            }
                        });
                }

                //Policy Symbol
                if (companysSubmissionTypes.Any(t => t.PolicySymbol.Enabled))
                {
                    var policySymbolIds = submissions.Where(s => s.PolicySymbolId.HasValue).Select(s => s.PolicySymbolId.Value).Distinct().ToArray();
                    if (policySymbolIds.Length > 0)
                        additionalQueries.Add(() =>
                        {
                            using (var db = Application.GetDatabaseInstance())
                            {
                                var psMapper = DataToViewModel.PolicySymbolSummary();
                                var data = db.PolicySymbol.AsExpandable().Where(p => policySymbolIds.Contains(p.Id))
                                                                         .Select(p => new { Id = p.Id, Symbol = psMapper.Invoke(p) })
                                                                         .ToDictionary((p) => p.Id, (p) => p.Symbol);
                                foreach (var s in submissions.Where(s => s.PolicySymbolId.HasValue))
                                    s.PolicySymbol = data[s.PolicySymbolId.Value];
                            }
                        });
                }

                //Package Policy Symbols
                if (companysSubmissionTypes.Any(t => t.PackagePolicySymbols.Enabled))
                {
                    additionalQueries.Add(() =>
                    {
                        using (var db = Application.GetDatabaseInstance())
                        {
                            var psMapper = DataToViewModel.PolicySymbolSummary();
                            var data = db.Submission.AsExpandable().Where(s => submissionIds.Contains(s.Id))
                                                                    .Select(s => new
                                                                    {
                                                                        Id = s.Id,
                                                                        PolicySymbols = s.PackagePolicySymbols.OrderBy(ps => ps.Code).Select(ps => psMapper.Invoke(ps)).ToList()
                                                                    });
                            foreach (var c in data)
                                submissionMap[c.Id].PackagePolicySymbols = c.PolicySymbols;
                        }
                    });
                }

                //Threaded work
                Task[] threads = new Task[additionalQueries.Count];
                for (var i = 0; i < additionalQueries.Count; i++)
                    threads[i] = (Task.Factory.StartNew(additionalQueries[i]));
                Task.WaitAll(threads);

                //Nonthreaded (for debugging/performance tuning)
                //foreach (var q in additionalQueries)
                //    q();
            }

            return submissions;
        }
    }
}
