﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using System.Data.Entity;
using Int = BTS.WFA.Integration;
using System.IO;
using BTS.WFA.ViewModels;
using LinqKit;
using BTS.WFA.BCS.Mapping;
using System.Linq.Expressions;

namespace BTS.WFA.BCS.Services
{
    public class AgencyService
    {
        public static List<AgencyViewModel> Search(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.Agency.Where(a => a.CompanyNumberId == cd.CompanyNumberId);
                query = odataFilter(query) as IQueryable<Data.Models.Agency>;

                return GetHelper<AgencyViewModel>(cd, db, query, DataToViewModel.Agency(), limitingAgencyIds);
            }
        }

        public static List<AgencySummaryViewModel> SearchAutoComplete(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.Agency.Where(a => a.CompanyNumberId == cd.CompanyNumberId);
                query = odataFilter(query) as IQueryable<Data.Models.Agency>;

                return GetHelper<AgencySummaryViewModel>(cd, db, query, DataToViewModel.AgencySummary(), limitingAgencyIds);
            }
        }

        public static List<int> GetChildrenAgencyIds(int masterAgencyId)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Agency.Where(a => a.MasterAgencyId == masterAgencyId).Select(a => a.Id).ToList();
            }
        }

        public static AgencyViewModel Get(CompanyDetail cd, string agencyCode, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return GetHelper<AgencyViewModel>(cd, db, db.Agency.Where(a => a.CompanyNumberId == cd.CompanyNumberId && a.AgencyNumber == agencyCode), DataToViewModel.Agency(), limitingAgencyIds).FirstOrDefault();
            }
        }
        public static AgencyViewModel Get(CompanyDetail cd, long apsId, List<int> limitingAgencyIds)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return GetHelper<AgencyViewModel>(cd, db, db.Agency.Where(a => a.CompanyNumberId == cd.CompanyNumberId && a.ApsId == apsId), DataToViewModel.Agency(), limitingAgencyIds).FirstOrDefault();
            }
        }
        private static List<T> GetHelper<T>(CompanyDetail cd, Data.IClearanceDb db, IQueryable<Data.Models.Agency> query, Expression<Func<Data.Models.Agency, T>> projection, List<int> limitingAgencyIds) where T : AgencySummaryViewModel
        {
            if (limitingAgencyIds != null && limitingAgencyIds.Count > 0)
                query = query.Where(a => limitingAgencyIds.Contains(a.Id));

            return query.AsExpandable().Select(projection).ToList();
        }

        public static List<AgencyUnderwritingUnitViewModel> GetUnderwritingUnits(CompanyDetail cd, string agencyCode)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Agency.AsExpandable()
                                .Where(a => a.CompanyNumberId == cd.CompanyNumberId && a.AgencyNumber == agencyCode)
                                .SelectMany(a => a.AgencyLineOfBusiness)
                                .OrderBy(alob => alob.LineOfBusiness.Description)
                                .Select(DataToViewModel.UnderwritingUnitFromAgency(cd.SystemSettings))
                                .ToList();
            }
        }
        public static List<AgentViewModel> GetAgents(CompanyDetail cd, string agencyCode)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Agency.Where(a => a.CompanyNumberId == cd.CompanyNumberId && a.AgencyNumber == agencyCode)
                                .SelectMany(a => a.Agent)
                                .OrderBy(ag => ag.FirstName)
                                .ThenBy(ag => ag.Surname)
                                .Select(DataToViewModel.Agent(false))
                                .ToList();
            }
        }
        public static List<StateViewModel> GetLicensedStates(CompanyDetail cd, string agencyCode)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Agency.Where(a => a.CompanyNumberId == cd.CompanyNumberId && a.AgencyNumber == agencyCode)
                                .SelectMany(a => a.AgencyLicensedState)
                                .OrderBy(l => l.State.StateName)
                                .Select(l => l.State)
                                .Select(DataToViewModel.State())
                                .ToList();
            }
        }

        public static AgencySyncResultViewModel SyncAgency(CompanyDetail cd, string agencyCode, bool returnRefreshedAgency)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var companyRoles = db.UnderwritingRole.Where(ur => cd.SystemSettings.AllPersonnelRoles.Contains(ur.RoleCode)).ToDictionary(p => p.RoleCode.ToUpper(), p => p.Id);
                var personnel = db.Person.Where(p => p.CompanyNumberId == cd.CompanyNumberId).ToList();
                var states = db.State.ToDictionary(s => s.StateName.ToUpper(), s => s.Id);
                return SyncAgency(cd, agencyCode, companyRoles, personnel, states, returnRefreshedAgency);
            }
        }
        public static AgencySyncResultViewModel SyncAgency(CompanyDetail cd, string agencyCode, Dictionary<string, int> companyRoles, List<Data.Models.Person> underwritingPersonnel, Dictionary<string, int> states, bool returnRefreshedAgency)
        {
            var result = new AgencySyncResultViewModel() { Succeeded = true };
            string userName = "Agency Sync";
            bool useParentLicensedStates = (cd.CompanyId == (int)Domains.Companies.BRS);

            try
            {
                Func<IQueryable<Data.Models.Agency>, Data.Models.Agency> getAgency = (query) =>
                {
                    return query.Where(a => a.CompanyNumberId == cd.CompanyNumberId)
                                .Include(a => a.AgencyLineOfBusiness)
                                .Include(a => a.AgencyLineOfBusiness.Select(l => l.LineOfBusiness))
                                .Include(a => a.AgencyLineOfBusiness.Select(l => l.AgencyLineOfBusinessPersonUnderwritingRolePerson))
                                .Include(a => a.AgencyLineOfBusiness.Select(l => l.AgencyLineOfBusinessPersonUnderwritingRolePerson.Select(assn => assn.Person)))
                                .Include(a => a.AgencyLineOfBusiness.Select(l => l.AgencyLineOfBusinessPersonUnderwritingRolePerson.Select(assn => assn.UnderwritingRole)))
                                .Include(a => a.AgencyLicensedState)
                                .Include(a => a.AgencyLicensedState.Select(l => l.State))
                                .Include(a => a.AgencyBranch)
                                .Include(a => a.Agent)
                                .FirstOrDefault();
                };

                using (var db = Application.GetDatabaseInstance())
                {
                    Data.Models.CompanyNumber dbCompanyNumber = db.CompanyNumber.Include(cn => cn.LineOfBusiness)
                                                                                .Include(cn => cn.AgencyBranch)
                                                                                .FirstOrDefault(cn => cn.Id == cd.CompanyNumberId);
                    Data.Models.Agency dbAgency = null;
                    Integration.Models.AgencySearchResult agencyRequest = null;

                    //Get the agency from APS and from the clearance DB by agency code
                    var tasks = new Task[] {
                        Task.Factory.StartNew(() => { dbAgency = getAgency(db.Agency.Where(a => a.AgencyNumber == agencyCode)); }),
                        Task.Factory.StartNew(() => { agencyRequest = Integration.Services.AgencyService.GetAgency(cd.SystemSettings.ApsServiceUrl, agencyCode, useParentLicensedStates, companyRoles.Keys.ToArray()); })
                    };
                    Task.WaitAll(tasks);

                    if (!agencyRequest.Succeeded)
                    {
                        result.Succeeded = false;
                        result.Exception = agencyRequest.Exception;
                        result.Message = agencyRequest.Exception.Message;
                    }
                    else if (agencyRequest.Agency == null)
                    {
                        result.Succeeded = false;
                        result.Message = "Agency was not found";
                    }
                    else
                    {
                        if (agencyRequest.SecondaryExceptions.Count > 0)
                            result.Exception = new AggregateException(agencyRequest.SecondaryExceptions);

                        var apsAgency = agencyRequest.Agency;
                        result.ApsId = apsAgency.Id.ToString();

                        if (dbAgency == null)
                            dbAgency = getAgency(db.Agency.Where(a => a.ApsId == apsAgency.Id));
                        if (dbAgency == null)
                            dbAgency = db.Agency.Add(new Data.Models.Agency() { CompanyNumberId = cd.CompanyNumberId, EntryBy = userName });

                        dbAgency.ApsId = apsAgency.Id;
                        dbAgency.Fein = apsAgency.FEIN;
                        dbAgency.AgencyNumber = apsAgency.AgencyCode;
                        dbAgency.AgencyName = apsAgency.AgencyName;
                        dbAgency.EffectiveDate = apsAgency.EffectiveDate ?? dbAgency.EffectiveDate;
                        dbAgency.RenewalCancelDate = apsAgency.RenewalCancelDate;
                        dbAgency.CancelDate = apsAgency.NewCancelDate ?? apsAgency.RenewalCancelDate;

                        dbAgency.ReferralAgencyNumber = apsAgency.ReferralAgencyCode;
                        dbAgency.ReferralDate = apsAgency.ReferralDate;

                        if (apsAgency.Parent == null)
                            dbAgency.MasterAgencyId = null;
                        else
                            dbAgency.MasterAgency = db.Agency.FirstOrDefault(a => a.CompanyNumberId == cd.CompanyNumberId && (a.ApsId == apsAgency.Parent.Id || a.AgencyNumber == apsAgency.Parent.AgencyCode));

                        dbAgency.IsMaster = apsAgency.HasChildren;

                        var addr = apsAgency.Locations.FirstOrDefault(l => l.IsPrimary);
                        dbAgency.Address = (addr == null) ? null : addr.Address1;
                        dbAgency.Address2 = (addr == null) ? null : addr.Address2;
                        dbAgency.City = (addr == null) ? null : addr.City;
                        dbAgency.State = (addr == null) ? null : addr.State;
                        dbAgency.Zip = (addr == null) ? null : addr.PostalCode;
                        dbAgency.Phone = (addr == null) ? null : addr.Phone;
                        dbAgency.Fax = (addr == null) ? null : addr.Fax;
                        dbAgency.Email = (addr == null) ? null : addr.Email;

                        var clearanceNotes = apsAgency.Notes.Where(n => String.Equals(n.Category, "Clearance", StringComparison.CurrentCultureIgnoreCase)).OrderBy(n => n.CreatedDate);
                        if (clearanceNotes.Any())
                            dbAgency.Comments = String.Join(", ", clearanceNotes.Select(n => n.Details ?? n.Summary));

                        if (apsAgency.Branches.Any())
                        {
                            //the CompanyNumberId check should be redundant, but at one point we were adding branches from other companies)
                            foreach (var dbBranch in dbAgency.AgencyBranch.Where(b => b.CompanyNumberId != cd.CompanyNumberId || !apsAgency.Branches.Any(apsB => apsB.Id == b.ApsId)).ToArray())
                                dbAgency.AgencyBranch.Remove(dbBranch);

                            foreach (var apsBranch in apsAgency.Branches)
                            {
                                var dbBranch = dbCompanyNumber.AgencyBranch.FirstOrDefault(b => b.ApsId == apsBranch.Id);
                                if (dbBranch == null)
                                {
                                    dbBranch = db.AgencyBranch.Add(new Data.Models.AgencyBranch() { CompanyNumberId = cd.CompanyNumberId, ApsId = apsBranch.Id });
                                    dbCompanyNumber.AgencyBranch.Add(dbBranch);
                                }

                                dbBranch.Code = apsBranch.Code;
                                dbBranch.Name = apsBranch.Name;

                                if (!dbAgency.AgencyBranch.Contains(dbBranch))
                                    dbAgency.AgencyBranch.Add(dbBranch);
                            }
                        }
                        else
                            dbAgency.AgencyBranch.Clear();


                        if (db.EntityHasChanges(dbAgency))
                        {
                            dbAgency.ModifiedBy = userName;
                            dbAgency.ModifiedDt = DateTime.Now;
                        }

                        var existingAgentsToRemove = dbAgency.Agent.ToList();
                        //only process agents with a broker number
                        foreach (var agent in apsAgency.Agents.Where(a => a.AgentNumber != null))
                        {
                            var dbAgent = dbAgency.Agent.FirstOrDefault(a => a.ApsId == agent.Id);
                            if (dbAgent == null)
                                dbAgent = dbAgency.Agent.FirstOrDefault(a => a.BrokerNo == agent.AgentNumber);
                            if (dbAgent == null)
                                dbAgent = dbAgency.Agent.FirstOrDefault(a => a.FirstName == agent.FirstName && a.Surname == agent.LastName);

                            if (dbAgent == null)
                            {
                                dbAgent = new Data.Models.Agent() { ApsId = agent.Id, Agency = dbAgency, EntryBy = userName };
                                dbAgency.Agent.Add(dbAgent);
                            }

                            dbAgent.APSPersonnelId = agent.PersonnelId;
                            dbAgent.BrokerNo = agent.AgentNumber;
                            dbAgent.Prefix = agent.NamePrefix;
                            dbAgent.FirstName = agent.FirstName;
                            dbAgent.Middle = agent.MiddleName;
                            dbAgent.Surname = agent.LastName;
                            dbAgent.SuffixTitle = agent.NameSuffix;
                            if (agent.PrimaryPhone != null && agent.PrimaryPhone.ToString().Length == 10)
                            {
                                dbAgent.PrimaryPhoneAreaCode = agent.PrimaryPhone.ToString().Substring(0, 3);
                                dbAgent.PrimaryPhone = agent.PrimaryPhone.ToString().Substring(3);
                            }
                            if (agent.SecondaryPhone != null && agent.SecondaryPhone.ToString().Length == 10)
                            {
                                dbAgent.SecondaryPhoneAreaCode = agent.SecondaryPhone.ToString().Substring(0, 3);
                                dbAgent.SecondaryPhone = agent.SecondaryPhone.ToString().Substring(3);
                            }
                            if (agent.Fax != null && agent.Fax.ToString().Length == 10)
                            {
                                dbAgent.FaxAreaCode = agent.Fax.ToString().Substring(0, 3);
                                dbAgent.Fax = agent.Fax.ToString().Substring(3);
                            }
                            dbAgent.EmailAddress = agent.EmailAddress;
                            dbAgent.Dob = (agent.DOB != null && agent.DOB.Value.Year < 1753) ? null : agent.DOB;
                            dbAgent.TaxId = agent.TaxId;
                            //dbAgent.EffectiveDate = agent.EffectiveDate;
                            dbAgent.CancelDate = agent.CancelDate;
                            dbAgent.Active = (agent.CancelDate == null || agent.CancelDate.Value >= DateTime.Now.Date);

                            existingAgentsToRemove.Remove(dbAgent);
                            if (db.EntityHasChanges(dbAgent))
                            {
                                dbAgent.ModifiedBy = userName;
                                dbAgent.ModifiedDt = DateTime.Now;
                            }
                        }
                        foreach (var dbAgent in existingAgentsToRemove.Where(a => a.Active || a.CancelDate == null))
                        {
                            dbAgent.Active = false;
                            dbAgent.CancelDate = DateTime.Now.AddDays(-1).Date;
                            dbAgent.ModifiedBy = userName;
                            dbAgent.ModifiedDt = DateTime.Now;
                        }

                        //Line of Business
                        foreach (var existingLob in dbAgency.AgencyLineOfBusiness.ToArray())
                        {
                            if (!apsAgency.UnderwritingUnits.Any(uw => existingLob.LineOfBusiness.ApsId == uw.UnderwritingUnitId
                                                                    || String.Equals(uw.Code, existingLob.LineOfBusiness.Code, StringComparison.CurrentCultureIgnoreCase)
                                                                    || String.Equals(uw.Description, existingLob.LineOfBusiness.Description, StringComparison.CurrentCultureIgnoreCase)))
                            {
                                foreach (var assn in existingLob.AgencyLineOfBusinessPersonUnderwritingRolePerson.ToArray())
                                    db.AgencyLineOfBusinessPersonUnderwritingRolePerson.Remove(assn);

                                db.AgencyLineOfBusiness.Remove(existingLob);
                            }
                        }

                        foreach (var underwritingUnit in apsAgency.UnderwritingUnits)
                        {
                            var dbLob = dbCompanyNumber.LineOfBusiness.FirstOrDefault(l => l.ApsId == underwritingUnit.UnderwritingUnitId
                                                                         || String.Equals(l.Code, underwritingUnit.Code, StringComparison.CurrentCultureIgnoreCase)
                                                                         || String.Equals(l.Description, underwritingUnit.Description, StringComparison.CurrentCultureIgnoreCase));

                            if (dbLob == null)
                            {
                                dbLob = db.LineOfBusiness.Add(new Data.Models.LineOfBusiness() { Line = "CL" });
                                dbCompanyNumber.LineOfBusiness.Add(dbLob);

                                //This might go into the main chunk of code if we ever want to update the names after creation
                                dbLob.Code = underwritingUnit.Code;
                                dbLob.Description = underwritingUnit.Description;
                            }

                            dbLob.ApsId = underwritingUnit.UnderwritingUnitId;

                            var dbAgencyLob = dbAgency.AgencyLineOfBusiness.FirstOrDefault(a => a.LineOfBusiness == dbLob);
                            if (dbAgencyLob == null)
                            {
                                dbAgencyLob = new Data.Models.AgencyLineOfBusiness() { ApsId = underwritingUnit.Id, LineOfBusiness = dbLob, Agency = dbAgency };
                                dbAgency.AgencyLineOfBusiness.Add(dbAgencyLob);
                            }

                            var apsPersonnel = underwritingUnit.Personnel.GroupBy(p => new { p.Id, p.RoleCode }).Select(g => g.First()).ToArray();//We're grouping by Id because APS sends us duplicates sometimes, causes SQL key errors
                            //Remove personnel no longer assigned
                            foreach (var assn in dbAgencyLob.AgencyLineOfBusinessPersonUnderwritingRolePerson.ToArray())
                                if (!apsPersonnel.Any(p => (p.Id == assn.Person.ApsId || p.UserName == assn.Person.UserName) && p.RoleCode == assn.UnderwritingRole.RoleCode))
                                    db.AgencyLineOfBusinessPersonUnderwritingRolePerson.Remove(assn);

                            //Update personnel assignments
                            var dbPersonnel = dbAgencyLob.AgencyLineOfBusinessPersonUnderwritingRolePerson.ToArray();
                            foreach (var p in apsPersonnel)
                            {
                                var roleCode = p.RoleCode.ToUpper();
                                var dbPerson = underwritingPersonnel.FirstOrDefault(dbP => dbP.ApsId == p.Id || dbP.UserName == p.UserName);
                                if (companyRoles.ContainsKey(roleCode) && dbPerson != null)
                                {
                                    var roleCodeId = companyRoles[roleCode];
                                    var dbPersonAssn = dbAgencyLob.AgencyLineOfBusinessPersonUnderwritingRolePerson.FirstOrDefault(a => a.AgencyLineOfBusiness == dbAgencyLob && a.PersonId == dbPerson.Id && a.UnderwritingRoleId == roleCodeId);
                                    if (dbPersonAssn == null)
                                    {
                                        dbPersonAssn = new Data.Models.AgencyLineOfBusinessPersonUnderwritingRolePerson()
                                        {
                                            Agency = dbAgency,
                                            LineOfBusiness = dbLob,
                                            AgencyLineOfBusiness = dbAgencyLob,
                                            PersonId = dbPerson.Id,
                                            UnderwritingRoleId = roleCodeId
                                        };
                                        dbAgencyLob.AgencyLineOfBusinessPersonUnderwritingRolePerson.Add(dbPersonAssn);
                                    }

                                    dbPersonAssn.Primary = p.IsPrimaryRole;
                                }
                            }
                        }

                        //Licensed States
                        foreach (var existingLS in dbAgency.AgencyLicensedState.ToArray())
                        {
                            if (!apsAgency.LicensedStates.Any(ls => ls.Id == existingLS.ApsId || ls.State == existingLS.State.Abbreviation))
                                db.AgencyLicensedState.Remove(existingLS);
                        }

                        foreach (var ls in apsAgency.LicensedStates)
                        {
                            var stateAbbr = (ls.State ?? "").ToUpper();
                            if (states.ContainsKey(stateAbbr) && !dbAgency.AgencyLicensedState.Any(l => l.State.Abbreviation == stateAbbr))
                                dbAgency.AgencyLicensedState.Add(new Data.Models.AgencyLicensedState() { ApsId = ls.Id, Agency = dbAgency, StateId = states[stateAbbr] });
                        }

                        if (db.EntityIsNew(dbAgency))
                            result.Message = "Agency Created";
                        else if (db.HasChanges())
                            result.Message = "Agency Updated";
                        else
                            result.Message = "Agency is already up to date";

                        if (apsAgency.Agents.Any(a => a.AgentNumber == null))
                            result.Message += " (agents excluded due to missing broker numbers)";

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Application.HandleException(ex, new { Company = cd, AgencyCode = agencyCode });
                result.Succeeded = false;
                result.Message = Providers.HealthMonitorProvider.ExceptionMessage(ex);
            }

            if (returnRefreshedAgency)
                result.Agency = Get(cd, agencyCode, null);

            return result;
        }

        public static void SyncAgencies(CompanyDetail cd, bool syncAllApsAgencies, Action<string> log)
        {
            List<CompanyDetail> companiesToProcess = new List<CompanyDetail>();

            using (var db = Application.GetDatabaseInstance())
            {
                if (cd != null)
                    companiesToProcess.Add(cd);
                else
                {
                    foreach (var id in db.ApsSync.Select(a => a.CompanyNumberID).Distinct())
                    {
                        var matchedCompanyNumber = Application.CompanyMap.Values.FirstOrDefault(c => c.CompanyNumberId == id);
                        if (matchedCompanyNumber != null)
                            companiesToProcess.Add(matchedCompanyNumber);
                    }
                }

                int agenciesProcessed = 0;
                foreach (var company in companiesToProcess.OrderBy(c => c.Summary.CompanyName))
                {
                    log(String.Format("Starting Company: {0}", company.Summary.CompanyName));

                    //First update all Persons
                    Services.UnderwritingService.SyncAllPersonnel(company, log);

                    var companyRoles = db.UnderwritingRole.Where(ur => cd.SystemSettings.AllPersonnelRoles.Contains(ur.RoleCode)).ToDictionary(p => p.RoleCode.ToUpper(), p => p.Id);
                    var personnel = db.Person.Where(p => p.CompanyNumberId == cd.CompanyNumberId).ToList();
                    var states = db.State.ToDictionary(s => s.StateName.ToUpper(), s => s.Id);

                    if (syncAllApsAgencies)
                    {
                        log("Getting All Agencies");
                        var agencies = Int.Services.AgencyService.GetAllAgencies(company.SystemSettings.ApsServiceUrl);
                        if (!agencies.Succeeded)
                        {
                            log("Failed to get all agencies");
                            Application.HandleException(agencies.Exception, new { Company = company });
                        }
                        else
                        {
                            log(String.Format("Found {0} agencies", agencies.AgencyCodes.Count));
                            foreach (var code in agencies.AgencyCodes)
                            {
                                agenciesProcessed++;
                                var syncResults = SyncAgency(company, code, companyRoles, personnel, states, false);
                                log(String.Format("{0} - {1} - {2} - Sync Complete: {3}", agenciesProcessed.ToString("000000"), company.Summary.Abbreviation, code, syncResults.Message));
                            }
                        }
                    }
                    else
                    {
                        var skippedRecordCount = db.ApsSync.Count(a => !a.CanProcess && a.CompanyNumberID == company.CompanyNumberId);
                        log(String.Format("Skipping {0} record{1} with CanProcess=0", skippedRecordCount, (skippedRecordCount == 1) ? "" : "s"));

                        foreach (var agency in db.ApsSync.Where(a => a.CanProcess && a.CompanyNumberID == company.CompanyNumberId).ToArray())
                        {
                            agenciesProcessed++;
                            var syncResults = SyncAgency(company, agency.AgencyCode, companyRoles, personnel, states, false);
                            log(String.Format("{0} - {1} - {2} - Sync Complete: {3}", agenciesProcessed.ToString("000000"), company.Summary.Abbreviation, agency.AgencyCode, syncResults.Message));

                            agency.AgencyAPSID = (String.IsNullOrEmpty(syncResults.ApsId)) ? 0 : long.Parse(syncResults.ApsId);
                            agency.ProcessedOn = DateTime.Now;
                            agency.RollbackScript = syncResults.Message;
                            agency.LastRunSuccessful = syncResults.Succeeded;
                            agency.CanProcess = false;
                            db.SaveChanges();
                            log(String.Format("{0} - {1} - {2} - Updated Agency Sync Row", agenciesProcessed.ToString("000000"), company.Summary.Abbreviation, agency.AgencyCode));
                        }
                    }

                    log(String.Format("Finished Company: {0}", company.Summary.CompanyName));
                    log("");
                }
            }
        }

        public static ApsLogItemViewModel GetApsLogItem(int logId)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var log = db.ApsxmlLog.First(l => l.Id == logId);

                return new ApsLogItemViewModel() { Id = log.Id, CompanyNumber = log.CompanyNumber, Xml = log.Xml };
            }
        }
        public static int SaveApsLogItem(CompanyDetail cd, string xml)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var log = new Data.Models.ApsxmlLog();
                log.CompanyNumber = cd.Summary.CompanyNumber;
                log.Xml = xml;

                db.ApsxmlLog.Add(log);
                db.SaveChanges();

                return log.Id;
            }
        }
        public static void SaveApsLogItemAsFailure(int logId)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbLog = db.ApsxmlLog.FirstOrDefault(l => l.Id == logId);
                dbLog.Errored = true;
                db.SaveChanges();
            }
        }
    }
}