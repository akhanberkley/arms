﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using Int = BTS.WFA.Integration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.BCS.Services
{
    public class UnderwritingService
    {
        public static List<UnderwritingUnitViewModel> GetUnits(CompanyDetail cd)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.CompanyNumber.Where(c => c.Id == cd.CompanyNumberId)
                                       .SelectMany(c => c.LineOfBusiness)
                                       .OrderBy(c => c.Line)
                                       .ThenBy(c => c.Code)
                                       .Select(DataToViewModel.UnderwritingUnit())
                                       .ToList();
            }
        }

        public static List<PersonnelViewModel> GetUnderwriters(CompanyDetail cd)
        {
            return GetPersonnel(cd, cd.SystemSettings.UnderwritingRoles);
        }
        public static List<PersonnelViewModel> GetUnderwritingAnalysts(CompanyDetail cd)
        {
            if (cd.Configuration.UsesAllUnderwritersForAllAssignments)
                return GetPersonnel(cd, cd.SystemSettings.UnderwritingRoles.Union(cd.SystemSettings.UnderwritingAnalystRoles));
            else
                return GetPersonnel(cd, cd.SystemSettings.UnderwritingAnalystRoles);
        }
        public static List<PersonnelViewModel> GetUnderwritingTechnicians(CompanyDetail cd)
        {
            if (cd.Configuration.UsesAllUnderwritersForAllAssignments)
                return GetPersonnel(cd, cd.SystemSettings.UnderwritingRoles.Union(cd.SystemSettings.UnderwritingTechnicianRoles));
            else
                return GetPersonnel(cd, cd.SystemSettings.UnderwritingTechnicianRoles);
        }
        private static List<PersonnelViewModel> GetPersonnel(CompanyDetail cd, IEnumerable<string> roleCodes)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Person.Where(p => p.CompanyNumberId == cd.CompanyNumberId && p.UnderwritingRoles.Any(r => roleCodes.Contains(r.RoleCode)))
                                .OrderBy(p => p.FullName)
                                .Select(DataToViewModel.Personnel())
                                .ToList();
            }
        }

        public static void SyncPersonnelChanges(CompanyDetail cd, Integration.Models.Personnel person)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbPerson = db.Person.Include(p => p.UnderwritingRoles).FirstOrDefault(p => p.CompanyNumberId == cd.CompanyNumberId && p.ApsId == person.Id);
                SavePerson(db, cd, dbPerson, person, db.UnderwritingRole.ToArray());
            }
        }
        public static void SyncAllPersonnel(CompanyDetail cd, Action<string> log)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                log("Getting All Personnel");
                var allRoles = cd.SystemSettings.UnderwritingRoles.Union(cd.SystemSettings.UnderwritingAnalystRoles).Union(cd.SystemSettings.UnderwritingTechnicianRoles);
                var personnel = Int.Services.AgencyService.GetPersonnelByRole(cd.SystemSettings.ApsServiceUrl, allRoles);
                if (!personnel.Succeeded)
                    Application.HandleException(new Exception("Sync All Personnel Failed", personnel.Exception), cd.Configuration);
                else
                {
                    log(String.Format("Found {0} Persons", personnel.Personnel.Count));
                    int personsSynced = 0;
                    var roles = db.UnderwritingRole.ToArray();

                    var personnelIds = personnel.Personnel.Select(p => p.Id).ToArray();
                    var dbExistingPersonsByIds = db.Person.Include(p => p.UnderwritingRoles).Where(p => p.CompanyNumberId == cd.CompanyNumberId && p.ApsId != null).ToArray();
                    foreach (var person in personnel.Personnel)
                    {
                        SavePerson(db, cd, dbExistingPersonsByIds.FirstOrDefault(p => p.ApsId == person.Id), person, roles);
                        log(String.Format("{0} - {1} - {2}", (++personsSynced).ToString("000000"), person.Id, person.FullName));
                    }
                }

                log("Finished Syncing Personnel");
            }
        }
        private static void SavePerson(Data.IClearanceDb db, CompanyDetail cd, Data.Models.Person dbPerson, Integration.Models.Personnel person, IEnumerable<Data.Models.UnderwritingRole> underwritingRoles)
        {
            if (dbPerson == null)
                dbPerson = db.Person.Include(p => p.UnderwritingRoles).FirstOrDefault(p => p.CompanyNumberId == cd.CompanyNumberId && (p.UserName == person.UserName || (p.ApsId == null && p.FullName == person.FullName)));
            if (dbPerson == null)
                dbPerson = db.Person.Add(new Data.Models.Person() { CompanyNumberId = cd.CompanyNumberId, UnderwritingRoles = new List<Data.Models.UnderwritingRole>() });

            dbPerson.ApsId = person.Id;
            dbPerson.UserName = person.UserName;
            dbPerson.FullName = person.FullName;
            dbPerson.Initials = person.Initials;
            dbPerson.RetirementDate = person.RetirementDate;

            foreach (var removeCode in dbPerson.UnderwritingRoles.Where(ur => !person.RoleCodes.Contains(ur.RoleCode)).ToArray())
                dbPerson.UnderwritingRoles.Remove(removeCode);

            var existingCodes = dbPerson.UnderwritingRoles.Select(ur => ur.RoleCode);
            foreach (var newCode in person.RoleCodes.Where(rc => !existingCodes.Contains(rc)))
            {
                var dbRole = underwritingRoles.FirstOrDefault(r => r.RoleCode == newCode);
                if (dbRole != null)
                    dbPerson.UnderwritingRoles.Add(dbRole);
            }

            db.SaveChanges();
        }
    }
}
