﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using Int = BTS.WFA.Integration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using BTS.WFA.ViewModels;
using LinqKit;
using System.Linq.Expressions;
using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.BCS.Services
{
    public class CompanyCustomizationService
    {
        public static IEnumerable<Data.Models.CompanyNumberCode> GetMatchingCompanyCodes(IQueryable<Data.Models.CompanyNumberCode> query, IEnumerable<CompanyCustomizationValueViewModel> values)
        {
            var validCodesPredicate = PredicateBuilder.False<Data.Models.CompanyNumberCode>();
            foreach (var validCode in values.Where(c => c.CodeValue != null))
                validCodesPredicate = validCodesPredicate.Or(cnc => cnc.CompanyNumberCodeType.CodeName == validCode.Customization && (cnc.Code == validCode.CodeValue.Code || (!String.IsNullOrEmpty(validCode.CodeValue.Description) && cnc.Description == validCode.CodeValue.Description)));

            return query.AsExpandable().Where(validCodesPredicate).Include(c => c.CompanyNumberCodeType).ToArray();
        }
        public static void SetDatabaseCompanyCodes(Data.IClearanceDb db, CompanyDetail cd, IEnumerable<CompanyCustomizationValueViewModel> values, ICollection<Data.Models.CompanyNumberCode> companyNumberCodes)
        {
            //CompanyCodes
            if (!values.Any(c => c.CodeValue != null))
                companyNumberCodes.Clear();
            else
            {
                var dbSelectedCodes = GetMatchingCompanyCodes(db.CompanyNumberCode.Where(c => c.CompanyNumberId == cd.CompanyNumberId), values);
                var dbExistingCodes = companyNumberCodes.ToArray();
                var codesToDelete = dbExistingCodes.Where(e => !dbSelectedCodes.Select(s => s.Id).Contains(e.Id)).Select(e => e.Id).ToArray();

                foreach (var existingCode in companyNumberCodes.Where(c => codesToDelete.Contains(c.Id)).ToArray())
                    companyNumberCodes.Remove(existingCode);
                foreach (var codeToAdd in dbSelectedCodes.Where(c => !dbExistingCodes.Any(e => e.Id == c.Id)))
                    companyNumberCodes.Add(codeToAdd);
            }
        }

        public static List<CompanyCustomizationViewModel> GetCustomizations(CompanyDetail cd)
        {
            var customizations = new List<CompanyCustomizationViewModel>();
            using (var db = Application.GetDatabaseInstance())
            {
                var codeMapper = DataToViewModel.CompanyNumberCode();
                var companyNumber = db.CompanyNumber.AsExpandable()
                                                    .Where(c => c.Id == cd.CompanyNumberId)
                                                    .Select(cnum => new
                                                    {
                                                        CodeTypes = cnum.CompanyNumberCodeType.Select(ct => new
                                                        {
                                                            DisplayOrder = ct.DisplayOrder,
                                                            Customization = new CompanyCustomizationCodeTypeViewModel()
                                                            {
                                                                CodeType = ct,
                                                                Options = ct.CompanyNumberCode.OrderBy(c => c.Code).Select(c => codeMapper.Invoke(c)).ToList(),
                                                                DefaultOption = (ct.DefaultCompanyNumberCode == null) ? null : codeMapper.Invoke(ct.DefaultCompanyNumberCode)
                                                            }
                                                        }),
                                                        Attributes = cnum.CompanyNumberAttribute.Select(at => new
                                                        {
                                                            DisplayOrder = at.DisplayOrder,
                                                            Customization = new CompanyCustomizationAttributeViewModel() { Attribute = at }
                                                        })
                                                    })
                                                    .FirstOrDefault();

                if (companyNumber != null)
                {
                    List<short> sortNumbers = companyNumber.CodeTypes.Select(ct => ct.DisplayOrder).Union(companyNumber.Attributes.Select(a => a.DisplayOrder)).ToList();
                    sortNumbers.Sort();

                    foreach (var sequence in sortNumbers)
                    {
                        foreach (var customization in companyNumber.CodeTypes.Where(ct => ct.DisplayOrder == sequence).Select(ct => ct.Customization).Cast<CompanyCustomizationViewModel>()
                                                      .Union(companyNumber.Attributes.Where(a => a.DisplayOrder == sequence).Select(a => a.Customization)))
                        {
                            customizations.Add(customization);
                        }
                    }
                }
            }

            return customizations;
        }

        public static void Reorder(CompanyDetail cd, IEnumerable<string> items)
        {
            short curSequence = 0;
            if (items != null)
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    foreach (var item in items)
                    {
                        var codeType = db.CompanyNumberCodeType.FirstOrDefault(ct => ct.CompanyNumberId == cd.CompanyNumberId && ct.CodeName == item);
                        if (codeType != null)
                            codeType.DisplayOrder = curSequence;
                        else
                        {
                            var dbAttr = db.CompanyNumberAttribute.First(a => a.CompanyNumberId == cd.CompanyNumberId && a.Label == item);
                            dbAttr.DisplayOrder = curSequence;
                        }

                        curSequence++;
                    }

                    db.SaveChanges();
                }
            }
        }

        public static CompanyCustomizationViewModel SaveCustomization(CompanyDetail cd, CompanyCustomizationViewModel customization, string description)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                Data.Models.CompanyNumberCodeType dbCodeType = null;
                Data.Models.CompanyNumberAttribute dbAttribute = null;
                if (!String.IsNullOrEmpty(description))
                {
                    dbCodeType = db.CompanyNumberCodeType.Include(ct => ct.DefaultCompanyNumberCode)
                                                         .FirstOrDefault(ct => ct.CompanyNumberId == cd.CompanyNumberId && ct.CodeName == description);
                    dbAttribute = db.CompanyNumberAttribute.FirstOrDefault(a => a.CompanyNumberId == cd.CompanyNumberId && a.Label == description);
                }

                if (!String.IsNullOrEmpty(description) && dbCodeType == null && dbAttribute == null)
                    customization.ItemNotFound = true;
                else
                {
                    int? codeTypeId = (dbCodeType == null) ? null : (int?)dbCodeType.Id;
                    int? attributeId = (dbAttribute == null) ? null : (int?)dbAttribute.Id;

                    if (String.IsNullOrEmpty(customization.Description))
                        customization.AddValidation("Description", "Description is required");
                    else if (db.CompanyNumberAttribute.Any(cna => cna.CompanyNumberId == cd.CompanyNumberId && cna.Label == customization.Description && attributeId != cna.Id)
                             || db.CompanyNumberCodeType.Any(cnct => cnct.CompanyNumberId == cd.CompanyNumberId && cnct.CodeName == customization.Description && codeTypeId != cnct.Id))
                        customization.AddValidation("Description", "A customizations with this description ({0}) already exists.", customization.Description);

                    if (dbCodeType != null && customization.DataType != CompanyCustomizationDataType.List)
                        customization.AddValidation("General", "Lists can not change data types");
                    else if (dbAttribute != null && customization.DataType == CompanyCustomizationDataType.List)
                        customization.AddValidation("General", "This customization can not be changed to a list");
                    else if (customization.DataType != CompanyCustomizationDataType.List && customization.Usage == CompanyCustomizationUsage.ClassCode)
                        customization.AddValidation("General", "At this time the selected type ({0}) can not be used for class codes", customization.DataType.ToString());

                    if (customization.ValidationResults.Count == 0)
                    {
                        Data.Models.SubmissionField dbSubmissionField = null;
                        if (customization.DataType == CompanyCustomizationDataType.List)
                        {
                            if (dbCodeType == null)
                            {
                                customization.ItemCreated = true;
                                dbCodeType = db.CompanyNumberCodeType.Add(new Data.Models.CompanyNumberCodeType() { CompanyNumberId = cd.CompanyNumberId });
                                dbCodeType.DisplayOrder = (short)(db.CompanyNumberAttribute.Count(a => a.CompanyNumberId == cd.CompanyNumberId) + db.CompanyNumberCodeType.Count(c => c.CompanyNumberId == cd.CompanyNumberId));
                                switch (customization.Usage)
                                {
                                    case CompanyCustomizationUsage.Client: dbCodeType.ClientSpecific = true; break;
                                    case CompanyCustomizationUsage.ClassCode: dbCodeType.ClassCodeSpecific = true; break;
                                    case CompanyCustomizationUsage.Submission: dbCodeType.LineOfBusinessSpecific = true; break;
                                }

                                if (customization.Usage == CompanyCustomizationUsage.Submission)
                                {
                                    dbSubmissionField = db.SubmissionField.Add(new Data.Models.SubmissionField() { Id = Guid.NewGuid(), CompanyNumberCodeType = dbCodeType });
                                    db.CompanyNumberSubmissionField.Add(new Data.Models.CompanyNumberSubmissionField() { CompanyNumberId = cd.CompanyNumberId, SubmissionField = dbSubmissionField, Enabled = true });
                                }
                            }
                            else if (customization.Usage == CompanyCustomizationUsage.Submission)
                                dbSubmissionField = db.SubmissionField.FirstOrDefault(sf => sf.CompanyNumberCodeTypeId == dbCodeType.Id);

                            dbCodeType.CodeName = customization.Description;
                            dbCodeType.Required = customization.Required;
                            dbCodeType.VisibleOnAdd = customization.VisibleOnAdd;
                            dbCodeType.VisibleOnBPM = customization.VisibleOnBPM;
                            dbCodeType.ExpirationDate = (customization.ExpirationDate.HasValue) ? customization.ExpirationDate.Value.Date : (DateTime?)null;
                            dbCodeType.ExcludeValueOnCopy = (customization.Usage != CompanyCustomizationUsage.Submission) ? false : customization.ExcludeValueOnCopy;
                            dbCodeType.DefaultCompanyNumberCode = (customization.DefaultOption == null) ? null : db.CompanyNumberCode.FirstOrDefault(c => c.CompanyNumberId == cd.CompanyNumberId && c.CompanyCodeTypeId == dbCodeType.Id && c.Code == customization.DefaultOption.Code);
                        }
                        else
                        {
                            if (dbAttribute == null)
                            {
                                customization.ItemCreated = true;
                                dbAttribute = db.CompanyNumberAttribute.Add(new Data.Models.CompanyNumberAttribute() { CompanyNumberId = cd.CompanyNumberId });
                                dbAttribute.DisplayOrder = (short)(db.CompanyNumberAttribute.Count(a => a.CompanyNumberId == cd.CompanyNumberId) + db.CompanyNumberCodeType.Count(c => c.CompanyNumberId == cd.CompanyNumberId));
                                switch (customization.Usage)
                                {
                                    case CompanyCustomizationUsage.Client: dbAttribute.ClientSpecific = true; break;
                                }

                                if (customization.Usage == CompanyCustomizationUsage.Submission)
                                {
                                    dbSubmissionField = db.SubmissionField.Add(new Data.Models.SubmissionField() { Id = Guid.NewGuid(), CompanyNumberAttribute = dbAttribute });
                                    db.CompanyNumberSubmissionField.Add(new Data.Models.CompanyNumberSubmissionField() { CompanyNumberId = cd.CompanyNumberId, SubmissionField = dbSubmissionField, Enabled = true });
                                }
                            }
                            else if (customization.Usage == CompanyCustomizationUsage.Submission)
                                dbSubmissionField = db.SubmissionField.FirstOrDefault(sf => sf.CompanyNumberAttributeId == dbAttribute.Id);

                            dbAttribute.AttributeDataTypeId = (int)customization.DataType;
                            dbAttribute.Label = customization.Description;
                            dbAttribute.Required = customization.Required;
                            dbAttribute.VisibleOnAdd = customization.VisibleOnAdd;
                            dbAttribute.VisibleOnBPM = customization.VisibleOnBPM;
                            dbAttribute.ExpirationDate = (customization.ExpirationDate.HasValue) ? customization.ExpirationDate.Value.Date : (DateTime?)null;
                            dbAttribute.ExcludeValueOnCopy = (customization.Usage != CompanyCustomizationUsage.Submission) ? false : customization.ExcludeValueOnCopy;
                            dbAttribute.ReadOnlyProperty = customization.ReadOnly;
                        }

                        if(dbSubmissionField != null)
                        {
                            dbSubmissionField.DisplayLabel = customization.Description;
                            dbSubmissionField.PropertyName = customization.Description;
                            dbSubmissionField.ColumnHeading = customization.Description;
                        }

                        db.SaveChanges();
                    }
                }
            }

            return customization;
        }

        public static CompanyNumberCodeViewModel SaveCustomizationOption(CompanyDetail cd, string codeTypeDescription, CompanyNumberCodeViewModel option, string code)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbCodeType = db.CompanyNumberCodeType.Include(c => c.CompanyNumberCode)
                                                         .FirstOrDefault(ct => ct.CompanyNumberId == cd.CompanyNumberId && ct.CodeName == codeTypeDescription);
                if (dbCodeType == null)
                    option.ItemNotFound = true;
                else
                {
                    var dbOption = dbCodeType.CompanyNumberCode.FirstOrDefault(c => c.Code == code);
                    if (!String.IsNullOrEmpty(code) && dbOption == null)
                        option.ItemNotFound = true;
                    else
                    {
                        if (String.IsNullOrEmpty(option.Code))
                            option.AddValidation("Code", "Code is required");
                        else if (dbCodeType.CompanyNumberCode.Any(c => c.Code == option.Code && (dbOption == null || dbOption.Code != c.Code)))
                            option.AddValidation("Code", "An option with this code ({0}) already exists.", option.Code);

                        if (option.ValidationResults.Count == 0)
                        {
                            if (dbOption == null)
                            {
                                dbOption = db.CompanyNumberCode.Add(new Data.Models.CompanyNumberCode() { CompanyNumberId = cd.CompanyNumberId });
                                dbCodeType.CompanyNumberCode.Add(dbOption);
                            }

                            dbOption.Code = option.Code;
                            dbOption.Description = option.Description;
                            dbOption.ExpirationDate = option.ExpirationDate;

                            db.SaveChanges();
                            option.ItemCreated = (String.IsNullOrEmpty(code));
                        }
                    }
                }
            }

            return option;
        }
    }
}
