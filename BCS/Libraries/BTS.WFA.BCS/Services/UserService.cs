﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using System.Data.Entity;
using Int = BTS.WFA.Integration;
using BTS.WFA.ViewModels;
using LinqKit;
using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.BCS.Services
{
    public class UserService
    {
        public static UserViewModel GetActiveUser(string userName)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.User.AsExpandable()
                              .Where(u => u.Active && u.Username == userName)
                              .Select(DataToViewModel.User())
                              .FirstOrDefault();
            }
        }
        public static void UpdateDefaultCompany(int userId, CompanyDetail defaultCompany)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var user = db.User.FirstOrDefault(u => u.Id == userId && u.Company.Any(c => c.Id == defaultCompany.CompanyId));
                if (user != null)
                {
                    user.PrimaryCompanyId = defaultCompany.CompanyId;
                    db.SaveChanges();
                }
            }
        }

        public static List<UserSummaryViewModel> SearchSummary(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter, bool showEveryone)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.User.AsQueryable();
                if (!showEveryone)
                    query = query.Where(u => u.Company.Any(c => c.Id == cd.CompanyId))
                                 .Where(u => u.UserType.Name != Domains.UserTypes.SystemAdministrator);
                query = odataFilter(query) as IQueryable<Data.Models.User>;
                return query.AsExpandable().Select(DataToViewModel.UserSummary()).ToList();
            }
        }
        public static List<UserViewModel> SearchFull(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter, bool showEveryone)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.User.AsQueryable();
                if (!showEveryone)
                    query = query.Where(u => u.Company.Any(c => c.Id == cd.CompanyId))
                                 .Where(u => u.UserType.Name != Domains.UserTypes.SystemAdministrator);
                query = odataFilter(query) as IQueryable<Data.Models.User>;
                return query.AsExpandable().Select(DataToViewModel.User()).ToList();
            }
        }

        public static UserViewModel GetUser(CompanyDetail cd, int userId, bool showEveryone)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.User.Where(u => u.Id == userId);
                if (!showEveryone)
                    query = query.Where(u => u.Company.Any(c => c.Id == cd.CompanyId));

                return query.AsExpandable().Select(DataToViewModel.User()).FirstOrDefault();
            }
        }

        public static UserViewModel SaveUser(CompanyDetail cd, int? userId, UserViewModel user, bool allowCompanyChanges, bool showEveryone)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbUserType = db.UserType.FirstOrDefault(t => t.Name == user.UserType);

                if (userId != null && !showEveryone && !db.User.Any(u => u.Id == userId && u.Company.Any(c => c.Id == cd.CompanyId)))
                    user.AddValidation("Id", "The user could not be found or you do not have access to edit them");
                if (String.IsNullOrEmpty(user.UserName))
                    user.AddValidation("UserName", "User name is required");
                else if (db.User.Any(u => u.Id != userId && u.Username == user.UserName))
                    user.AddValidation("UserName", "The user name '{0}' already exists.", user.UserName);
                if (user.Companies.Count == 0)
                    user.AddValidation("Companies", "The user must be associated with at least one company");
                else if (user.DefaultCompany == null || !user.Companies.Any(c => c.Description == user.DefaultCompany.Description))
                    user.AddValidation("DefaultCompany", "The selected default company must be a company associated with the user");
                if (dbUserType == null)
                    user.AddValidation("UserType", "The user type was missing or could not be found");
                if (!String.IsNullOrEmpty(user.ClientCoreSegmentCode) && !cd.Configuration.ClientCoreSegments.Any(s => s.Code == user.ClientCoreSegmentCode))
                    user.AddValidation("ClientCoreSegment", "The client core segment specified is not valid");

                if (user.ValidationResults.Count == 0)
                {
                    var companies = db.Company.ToArray();
                    var dbUser = db.User.Include(u => u.Company).Include(u => u.Role).FirstOrDefault(u => u.Id == user.Id);
                    if (dbUser == null)
                    {
                        dbUser = db.User.Add(new Data.Models.User());
                        if (!allowCompanyChanges)
                        {
                            var defaultCompany = companies.First(c => c.Id == cd.CompanyId);
                            dbUser.Company.Add(defaultCompany);
                            dbUser.PrimaryCompanyId = defaultCompany.Id;
                        }
                    }

                    dbUser.Username = user.UserName;
                    dbUser.Active = user.Active;
                    dbUser.UserType = dbUserType;
                    dbUser.Segmentation = user.ClientCoreSegmentCode;
                    dbUser.Notes = user.Notes;

                    if (allowCompanyChanges)
                    {
                        var defaultCompany = companies.FirstOrDefault(c => c.CompanyName == user.DefaultCompany.Description);
                        if (defaultCompany == null)
                            user.AddValidation("DefaultCompany", "The selected default company '{0}' could not be found", user.DefaultCompany.Description);
                        else
                            dbUser.PrimaryCompanyId = defaultCompany.Id;

                        var selectedCompanyNames = user.Companies.Select(c => c.Description);
                        foreach (var c in dbUser.Company.Where(c => !selectedCompanyNames.Contains(c.CompanyName)).ToArray())
                            dbUser.Company.Remove(c);

                        var existingCompanyNames = dbUser.Company.Select(c => c.CompanyName);
                        foreach (var companyName in selectedCompanyNames.Where(n => !existingCompanyNames.Contains(n)))
                            dbUser.Company.Add(companies.First(c => c.CompanyName == companyName));
                    }

                    if (user.LimitingAgency == null)
                        dbUser.AgencyId = null;
                    else
                    {
                        var agencies = user.LimitingAgency.ToQuery(db.Agency.Where(a => a.CompanyNumberId == cd.CompanyNumberId), null);
                        if (agencies == null)
                            user.AddValidation("LimitingAgency", "Data sent for Limiting Agency was not specific enough");
                        else
                        {
                            var dbAgency = agencies.FirstOrDefault();
                            if (dbAgency == null)
                                user.AddValidation("LimitingAgency", "Limiting Agency was not found");
                            else
                                dbUser.AgencyId = dbAgency.Id;
                        }
                    }

                    //Roles
                    var selectedRoles = db.Role.Where(r => user.Roles.Contains(r.RoleName)).ToArray();
                    foreach (var r in dbUser.Role.Where(r => !selectedRoles.Contains(r)).ToArray())
                        dbUser.Role.Remove(r);

                    var existingRoles = dbUser.Role.ToArray();
                    foreach (var r in selectedRoles.Where(r => !existingRoles.Contains(r)))
                        dbUser.Role.Add(r);

                    //Save user and send a fresh copy from the database
                    if (user.ValidationResults.Count == 0)
                    {
                        db.SaveChanges();
                        user = GetUser(cd, dbUser.Id, true);
                    }
                }
            }

            return user;
        }
    }
}