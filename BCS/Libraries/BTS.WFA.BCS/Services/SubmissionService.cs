﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using System.Data.Entity;
using Int = BTS.WFA.Integration;
using System.Text.RegularExpressions;
using BTS.WFA.ViewModels;
using LinqKit;
using BTS.WFA.BCS.Validation;
using System.Runtime.Caching;

namespace BTS.WFA.BCS.Services
{
    public class SubmissionService
    {
        public static void Validate(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, SubmissionViewModel submission, bool generatePolicyNumber)
        {
            SubmissionValidator.Validate(cd, submissionNumber, submission, generatePolicyNumber);
        }

        private static bool HasOfacMatches(CompanyDetail cd, string submissionId, string insuredName, IEnumerable<SubmissionAddressViewModel> addresses)
        {
            var searchCriteria = new Int.Models.OFACSearchCriteria() { Name = insuredName };
            if (addresses.Any())
            {
                var sa = addresses.First();
                searchCriteria.Address = (sa == null) ? null : (sa.Address1 + ' ' + sa.Address2).Trim();
                searchCriteria.City = (sa == null) ? null : sa.City;
                searchCriteria.State = (sa == null) ? null : sa.State;
                searchCriteria.PostalCode = (sa == null) ? null : sa.PostalCode;
                searchCriteria.Country = (sa == null) ? null : sa.County;
            }

            var searchDesc = String.Format("Submission #: {0}", submissionId);
            var serviceSettings = cd.SystemSettings.ToAdvancedClientSettings();
            var ofacResults = Int.Services.ClientService.SearchOFAC(serviceSettings, searchDesc, searchCriteria);
            if (!ofacResults.Succeeded)
                Application.HandleException(new Exception("OFAC Search Failed", ofacResults.Exception), new { Company = cd.Summary, ServiceSettings = serviceSettings, SubmissionNumber = submissionId, SearchDescription = searchDesc, Criteria = searchCriteria });
            else if (ofacResults.Matches.Count > 0)
                return true;

            return false;
        }
        public static SubmissionIdViewModel Save(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, SubmissionViewModel submission, string updateUser, SubmissionSaveOptions options, List<int> limitingAgencyIds)
        {
            SubmissionIdViewModel result = submission;

            if (!options.IgnoreValidations)
                Validate(cd, submissionNumber, submission, options.GeneratePolicyNumber);

            if (submission.ValidationResults.Count == 0)
            {
                List<Task> threadedTasks = new List<Task>();
                using (var db = Application.GetDatabaseInstance())
                {
                    using (var trans = db.GetTransaction())
                    {
                        try
                        {
                            Data.Models.Submission dbSubmission = null;
                            var now = DateTime.Now;
                            var isNew = (submissionNumber == null);
                            if (isNew)
                            {
                                dbSubmission = db.Submission.Add(new Data.Models.Submission());
                                dbSubmission.CompanyNumberId = cd.CompanyNumberId;
                                dbSubmission.SubmissionDt = now;
                                dbSubmission.SubmissionBy = updateUser;

                                if (submission.SubmissionNumber != null)
                                {
                                    if (submission.SequenceNumber.HasValue && submission.SequenceNumber < 1)
                                        submission.AddValidation("SequenceNumber", "The sequence number should be 1 or greater", submission.SequenceNumber);
                                    if (db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence).Any())
                                        submission.AddValidation("Submission", "The submission {0} already exists", submission.Id);
                                    else
                                    {
                                        dbSubmission.SubmissionNumber = submission.SubmissionNumber.Value;
                                        dbSubmission.Sequence = submission.SequenceNumber ?? 1;
                                    }
                                }
                                else
                                {
                                    dbSubmission.SubmissionNumber = GetNextSubmissionNumber(cd);
                                    dbSubmission.Sequence = 1;
                                }

                                submissionNumber = new SubmissionNumberViewModel() { Number = dbSubmission.SubmissionNumber, Sequence = dbSubmission.Sequence };

                                //OFAC Search
                                threadedTasks.Add(Task.Factory.StartNew(() =>
                                {
                                    if (HasOfacMatches(cd, submissionNumber.ToString(), submission.InsuredName, submission.Addresses))
                                    {
                                        dbSubmission.CreatedWithOFACHit = true;
                                        if (cd.SystemSettings.UsesOfacSubmissionNote)
                                            dbSubmission.SubmissionComment.Add(new Data.Models.SubmissionComment() { Submission = dbSubmission, Comment = LegacyService.OFACMessage, EntryBy = "OFAC Check" });
                                    }
                                }));
                            }
                            else
                            {
                                var submissionQuery = db.Submission.Include(s => s.CompanyNumberAgentUnspecifiedReason)
                                                            .Include(s => s.LineOfBusiness)
                                                            .Include(s => s.ClassCodes)
                                                            .Include(s => s.SubmissionCompanyNumberAttributeValue)
                                                            .Include(s => s.CompanyNumberCode)
                                                            .Include(s => s.SubmissionType)
                                                            .Include(s => s.SubmissionComment)
                                                            .Include(s => s.PackagePolicySymbols)
                                                            .Include(s => s.Addresses)
                                                            .Include(s => s.AdditionalNames)
                                                            .Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence);
                                if (limitingAgencyIds != null && limitingAgencyIds.Count > 0)
                                    submissionQuery = submissionQuery.Where(s => limitingAgencyIds.Contains(s.AgencyId.Value));

                                dbSubmission = submissionQuery.FirstOrDefault();

                                //Update the submission number if needed
                                if (dbSubmission != null && submissionNumber.ToString() != submission.Id)
                                {
                                    dbSubmission.SubmissionNumber = submission.SubmissionNumber.Value;
                                    dbSubmission.Sequence = submission.SequenceNumber.Value;
                                }
                            }

                            if (dbSubmission == null)
                                submission.AddValidation("Submission", "Submission {0} could not be found", submission.Id);
                            else
                            {
                                var type = cd.Configuration.SubmissionTypes.FirstOrDefault(t => t.TypeName == submission.SubmissionType);
                                var mapper = new Mapping.SubmissionToData(cd, db, dbSubmission, submission.ValidationResults, type, isNew);
                                //We need to determine this before a policy # can be generated
                                var dbPolicySymbol = mapper.SetPolicySymbol(submission.PolicySymbol);

                                List<Action> actions = new List<Action>();
                                //These steps either don't use the database, or all need the same connection/transaction
                                actions.Add(() =>
                                {
                                    if (mapper.IsEnabled(type.PolicyNumber) && options.GeneratePolicyNumber)
                                    {
                                        var companyToGenerateNumberFor = cd;
                                        if (mapper.IsEnabled(type.PolicySystem) && submission.PolicySystem != null)
                                        {
                                            var policySystemCompany = Application.CompanyMap.Values.FirstOrDefault(c => c.Summary.Abbreviation == submission.PolicySystem.Code);
                                            if (policySystemCompany == null)
                                                submission.AddValidation("PolicySystem", "Could not generate policy number for policy system (company not found)");
                                            else if (policySystemCompany.Configuration.PolicyNumberGeneratable)
                                                companyToGenerateNumberFor = policySystemCompany;
                                        }

                                        submission.PolicyNumber = GetNextPolicyNumber(companyToGenerateNumberFor, dbPolicySymbol);
                                    }

                                    //Db not required
                                    mapper.SetBasicDetails(type, submission.SubmissionType, submission.EffectiveDate, submission.ExpirationDate, submission.PolicyNumber, submission.PolicyMod, submission.PolicyPremium);
                                    mapper.SetInsuredDetails(submission.ClientCoreId.Value, submission.ClientCorePortfolioId, submission.InsuredName, submission.InsuredDBA);
                                    mapper.SetNotes(submission.Notes, updateUser);

                                    //Requires same transaction
                                    mapper.SetSubmissionAssociation(submission.AssociatedSubmissions);
                                    mapper.SetCustomizations(submission.CompanyCustomizations);
                                    mapper.SetDuplicate(submission.DuplicateOfSubmission);
                                    mapper.SetPackagePolicySymbols(submission.PackagePolicySymbols);
                                });
                                //Thread safe steps
                                actions.Add(() => { mapper.SetAgencyAndUnderwriting(updateUser, submission.Agency, submission.Addresses.Select(a => a.State), submission.Agent, submission.AgentUnspecifiedReason, submission.UnderwritingUnit, submission.Underwriter, submission.UnderwritingAnalyst, submission.UnderwritingTechnician, limitingAgencyIds, options.IgnoreValidations); });
                                actions.Add(() => { mapper.SetClassCodes(submission.ClassCodes); });
                                actions.Add(() => { mapper.SetNames(submission.AdditionalNames, updateUser, now); });
                                actions.Add(() => { mapper.SetAddresses(submission.Addresses, updateUser, now); });
                                actions.Add(() => { mapper.SetCarrierDetails(submission.PriorCarrier, submission.PriorCarrierPremium, submission.AcquiringCarrier, submission.AcquiringCarrierPremium); });
                                actions.Add(() => { mapper.SetPolicySystem(submission.PolicySystem); });
                                actions.Add(() =>
                                {
                                    mapper.SetStatus(new SubmissionStatusUpdateViewModel { Status = submission.Status, StatusReason = submission.StatusReason, StatusNotes = submission.StatusNotes, StatusDate = submission.StatusDate },
                                                     submission.StatusHistory.Where(h => h.Id == null),
                                                     true,
                                                     updateUser,
                                                     now);
                                });

                                //Threaded work
                                foreach (var a in actions)
                                    threadedTasks.Add(Task.Factory.StartNew(a));

                                //Nonthreaded (for debugging/performance tuning)
                                //foreach (var a in actions)
                                //    a();

                                //Wait for everything to finish before committing
                                Task.WaitAll(threadedTasks.ToArray());

                                //Add items that were found to need to be added during threaded process (couldn't do it on a separate context before)
                                mapper.AddNewEntries();

                                if (submission.ValidationResults.Count == 0)
                                {
                                    dbSubmission.UpdatedBy = updateUser;
                                    dbSubmission.UpdatedDt = now;

                                    db.SaveChanges();
                                    trans.Commit();

                                    var savedNumber = new SubmissionNumberViewModel() { Number = dbSubmission.SubmissionNumber, Sequence = dbSubmission.Sequence };
                                    if (options.ReturnFullSubmission)
                                        result = SubmissionQueryService.GetByNumber(cd, savedNumber, null);
                                    else
                                        result = new SubmissionIdViewModel() { Id = savedNumber.ToString() };

                                    result.ItemCreated = isNew;
                                }
                                else
                                    trans.Rollback();
                            }
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            throw ex;
                        }
                    }
                }
            }

            return result;
        }

        public static SubmissionStatusUpdateViewModel UpdateStatus(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, SubmissionStatusUpdateViewModel update, string updateUser)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbSubmission = db.Submission.FirstOrDefault(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence);
                if (dbSubmission == null)
                    update.AddValidation("SubmissionNumber", "Submission {0} was not found", submissionNumber.Number.ToString());
                else
                    UpdateStatuses(cd, db, new[] { dbSubmission }, update, updateUser);
            }
            return update;
        }
        public static SubmissionStatusUpdateViewModel UpdateApplicationStatus(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, SubmissionStatusUpdateViewModel update, string updateUser)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var associationIdQuery = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence)
                                                      .Select(s => s.AssociationSubmissionId);
                var dbSubmissions = db.Submission.Where(s => associationIdQuery.Contains(s.AssociationSubmissionId)).ToArray();
                if (dbSubmissions.Length == 0)
                    update.AddValidation("SubmissionNumber", "Application {0} was not found", submissionNumber.Number.ToString());
                else
                    UpdateStatuses(cd, db, dbSubmissions, update, updateUser);
            }
            return update;
        }
        private static void UpdateStatuses(CompanyDetail cd, Data.IClearanceDb db, IEnumerable<Data.Models.Submission> dbSubmissions, SubmissionStatusUpdateViewModel update, string updateUser)
        {
            foreach (var dbSubmission in dbSubmissions)
            {
                var mapper = new Mapping.SubmissionToData(cd, db, dbSubmission, update.ValidationResults, null, false);
                mapper.SetStatus(update, null, true, updateUser, DateTime.Now);

                dbSubmission.UpdatedBy = updateUser;
                dbSubmission.UpdatedDt = DateTime.Now;
            }

            if (update.ValidationResults.Count == 0)
                db.SaveChanges();
        }

        public static BaseViewModel UpdateAssociatedSubmissions(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, IEnumerable<SubmissionSummaryViewModel> submissions)
        {
            var result = new BaseViewModel();
            using (var db = Application.GetDatabaseInstance())
            {
                var dbSubmission = db.Submission.FirstOrDefault(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence);
                if (dbSubmission == null)
                    result.AddValidation("SubmissionNumber", "Submission {0} was not found", submissionNumber.Number.ToString());
                else
                {
                    var mapper = new Mapping.SubmissionToData(cd, db, dbSubmission, result.ValidationResults, null, false);
                    mapper.SetSubmissionAssociation(submissions);

                    if (result.ValidationResults.Count == 0)
                        db.SaveChanges();
                }
            }

            return result;
        }

        public static SubmissionClientReassignViewModel ReassignClient(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, SubmissionClientReassignViewModel update, string updateUser)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbSubmission = db.Submission.FirstOrDefault(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence);
                if (dbSubmission == null)
                    update.AddValidation("SubmissionNumber", "Submission {0} was not found", submissionNumber.Number.ToString());
                else if (update.ClientCoreId == 0)
                    update.AddValidation("ClientCoreId", "Client Core Id is required");
                else if (String.IsNullOrEmpty(update.InsuredName))
                    update.AddValidation("InsuredName", "Insured Name is required");
                else
                {
                    var mapper = new Mapping.SubmissionToData(cd, db, dbSubmission, update.ValidationResults, null, false);
                    mapper.SetInsuredDetails(update.ClientCoreId, update.ClientCorePortfolioId, update.InsuredName, update.InsuredDBA);

                    if (update.ValidationResults.Count == 0)
                    {
                        dbSubmission.UpdatedBy = updateUser;
                        dbSubmission.UpdatedDt = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            return update;
        }

        public static BaseViewModel ReassignUnderwritingPersonnel(CompanyDetail cd, SubmissionUnderwritingPersonnelReassignmentViewModel reassignments)
        {
            var results = new BaseViewModel();
            using (var db = Application.GetDatabaseInstance())
            {
                IEnumerable<Data.Models.Submission> dbSubmissions = null;
                if (reassignments.Submissions == null || reassignments.Submissions.Count == 0)
                    results.AddValidation("Submissions", "Please specify which submissions you'd like to reassign");
                else
                {
                    var submissionsPredicate = PredicateBuilder.False<Data.Models.Submission>();
                    foreach (var submission in reassignments.Submissions)
                        submissionsPredicate = submissionsPredicate.Or(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submission.SubmissionNumber && s.Sequence == submission.SequenceNumber);
                    dbSubmissions = db.Submission.AsExpandable().Where(submissionsPredicate).ToArray();

                    foreach (var submission in reassignments.Submissions)
                    {
                        if(!dbSubmissions.Any(s => s.SubmissionNumber == submission.SubmissionNumber && s.Sequence == submission.SequenceNumber))
                            results.AddValidation("Submissions", "Could not find submission {0} in the database", submission.Id);
                    }
                }

                Data.Models.Person dbUnderwriter = null, dbUnderwritingAnalyst = null, dbUnderwritingTechnician = null;
                if(reassignments.Underwriter == null && reassignments.UnderwritingAnalyst == null && reassignments.UnderwritingTechnician == null)
                    results.AddValidation("Personnel", "Please specify at least one personnel to reassign");
                else
                {
                    Func<PersonnelViewModel, string, Data.Models.Person> getPersonnel = (person, propertyName) =>
                    {
                        Data.Models.Person dbPerson = null;
                        if (person != null)
                        {
                            var validPersonnel = db.Person.Where(p => p.CompanyNumberId == cd.CompanyNumberId);
                            IQueryable<Data.Models.Person> personQuery = null;
                            Data.Models.Person[] personQueryResults = null;

                            personQuery = person.ToQuery(validPersonnel);
                            if (personQuery == null)
                                results.AddValidation(propertyName, "Data sent for {0} was not specific enough", propertyName);
                            else
                            {
                                personQueryResults = personQuery.ToArray();
                                if (personQueryResults.Length == 0)
                                    results.AddValidation("Underwriter", "{0} could not be found in BCS Database", propertyName);
                                else if (personQueryResults.Length > 1)
                                    results.AddValidation("Underwriter", "Matched multiple {0} in BCS Database ({1})", propertyName, String.Join(", ", personQueryResults.Select(p => p.FullName)));
                                else
                                    dbPerson = personQueryResults[0];
                            }
                        }

                        return dbPerson;
                    };

                    dbUnderwriter = getPersonnel(reassignments.Underwriter, "Underwriter");
                    dbUnderwritingAnalyst = getPersonnel(reassignments.UnderwritingAnalyst, "UnderwritingAnalyst");
                    dbUnderwritingTechnician = getPersonnel(reassignments.UnderwritingTechnician, "UnderwritingTechnician");
                }

                if(results.ValidationResults.Count == 0)
                {
                    foreach(var dbSubmission in dbSubmissions)
                    {
                        if (dbUnderwriter != null)
                            dbSubmission.UnderwriterPerson = dbUnderwriter;
                        if (dbUnderwritingAnalyst != null)
                            dbSubmission.AnalystPerson = dbUnderwritingAnalyst;
                        if (dbUnderwritingTechnician != null)
                            dbSubmission.TechnicianPerson = dbUnderwritingTechnician;
                    }

                    db.SaveChanges();
                }
            }

            return results;
        }

        public static BaseViewModel Delete(CompanyDetail cd, SubmissionNumberViewModel submissionNumber, string updateUser)
        {
            BaseViewModel container = new BaseViewModel();
            using (var db = Application.GetDatabaseInstance())
            {
                var dbSubmission = db.Submission.FirstOrDefault(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == submissionNumber.Number && s.Sequence == submissionNumber.Sequence);
                if (dbSubmission == null)
                    container.AddValidation("SubmissionNumber", "Submission {0} was not found", submissionNumber.Number.ToString());
                else
                {
                    dbSubmission.Deleted = true;
                    dbSubmission.DeletedBy = updateUser;
                    dbSubmission.DeletedDt = DateTime.Now;

                    db.SaveChanges();
                }
            }

            return container;
        }

        public static long GetNextSubmissionNumber(CompanyDetail cd)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                long result = 0;
                var dbNumberControl = db.SubmissionNumberControl.First(n => n.CompanyNumberId == cd.CompanyNumberId);
                bool needValidNumber = true;
                while (needValidNumber)
                {
                    dbNumberControl.ControlNumber += 1;
                    result = dbNumberControl.ControlNumber;
                    try
                    {
                        db.SaveChanges();
                        needValidNumber = false;
                    }
                    catch (Exception ex)
                    {
                        if (ex is System.Data.Entity.Infrastructure.DbUpdateConcurrencyException)
                            db.Reload(dbNumberControl);
                        else
                            throw ex;
                    }
                }

                return result;
            }
        }
        public static string GetNextPolicyNumber(CompanyDetail cd, SubmissionViewModel submission)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                Data.Models.PolicySymbol dbPolicySymbol = null;
                if (submission.PolicySymbol != null)
                    dbPolicySymbol = submission.PolicySymbol.ToQuery(db.PolicySymbol.Where(p => p.CompanyNumberId == cd.CompanyNumberId)).FirstOrDefault();

                return GetNextPolicyNumber(cd, dbPolicySymbol);
            }
        }

        private static string GetNextPolicyNumber(CompanyDetail cd, Data.Models.PolicySymbol dbPolicySymbol)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                string result = null;
                var dbNumberControl = (dbPolicySymbol != null && dbPolicySymbol.NumberControlId.HasValue) ? db.NumberControl.First(n => n.Id == dbPolicySymbol.NumberControlId) : db.NumberControl.First(n => n.CompanyNumberId == cd.CompanyNumberId && n.NumberControlTypeId == 1);
                bool needValidNumber = true;
                while (needValidNumber)
                {
                    dbNumberControl.ControlNumber += 1;
                    result = dbNumberControl.ControlNumber.ToString();
                    if (!String.IsNullOrEmpty(dbNumberControl.PaddingChar))
                    {
                        if (dbNumberControl.PadHead)
                            result = result.PadLeft(dbNumberControl.TotalLength, dbNumberControl.PaddingChar[0]);
                        else
                            result = result.PadRight(dbNumberControl.TotalLength, dbNumberControl.PaddingChar[0]);
                    }

                    if (dbPolicySymbol != null)
                        result = dbPolicySymbol.PolicyNumberPrefix + result;

                    try
                    {
                        db.SaveChanges();
                        needValidNumber = false;
                    }
                    catch (Exception ex)
                    {
                        if (ex is System.Data.Entity.Infrastructure.DbUpdateConcurrencyException)
                            db.Reload(dbNumberControl);
                        else
                            throw ex;
                    }
                }

                return result;
            }
        }
    }

    public class SubmissionSaveOptions
    {
        public bool GeneratePolicyNumber { get; set; }
        public bool ReturnFullSubmission { get; set; }
        public bool IgnoreValidations { get; set; }
    }
}
