﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using Int = BTS.WFA.Integration;
using System.Data.Entity;
using LinqKit;
using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.BCS.Services
{
    /// <summary>
    /// Container for enhancements to the original BCS Webforms system
    /// </summary>
    public class LegacyService
    {
        /// <summary>
        /// Used by Core.Clearance.Clients; this should eventually be moved to the ClientService (with the parameters cleaned up)
        /// </summary>
        public static void StoreClearanceClient(string updateUser, int siccodeid, int naicscodeid, LegacyClientViewModel client)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var companyNumber = db.CompanyNumber.Where(cn => cn.CompanyNumberValue == client.CompanyNumber).First();
                int companyId = companyNumber.CompanyId;
                int companyNumberId = companyNumber.Id;

                //Basic Information
                var dbClient = db.Client.FirstOrDefault(c => c.CompanyNumberId == companyNumberId && c.ClientCoreClientId == client.ClientCoreClientId);
                if (dbClient == null)
                {
                    dbClient = db.Client.Add(new Data.Models.Client());
                    dbClient.CompanyNumberId = companyNumberId;
                    dbClient.ClientCoreClientId = client.ClientCoreClientId.Value;
                    dbClient.EntryBy = updateUser;

                    //Make sure we have the Id for the future steps
                    db.SaveChanges();
                }

                dbClient.BusinessDescription = client.BusinessDescription;
                dbClient.PhoneNumber = client.PhoneNumber;
                dbClient.ModifiedBy = updateUser;
                dbClient.ModifiedDt = DateTime.Now.ToString("MMM dd yyyy hh:mmtt");

                dbClient.SicCodeId = (siccodeid == 0) ? (int?)null : siccodeid;
                dbClient.NaicsCodeId = (naicscodeid == 0) ? (int?)null : naicscodeid;

                //Addresses
                foreach (var addr in client.Addresses.Where(a => !String.IsNullOrEmpty(a.ClearanceAddressType)))
                {
                    var dbAddress = db.ClientCoreAddressAddressType.FirstOrDefault(aDB => aDB.ClientId == dbClient.Id && aDB.ClientCoreAddressId == addr.ClientCoreAddressId);
                    if (dbAddress == null)
                    {
                        dbAddress = db.ClientCoreAddressAddressType.Add(new Data.Models.ClientCoreAddressAddressType());
                        dbAddress.ClientCoreAddressId = addr.ClientCoreAddressId.Value;
                        dbAddress.ClientId = dbClient.Id;
                    }

                    var dbAddressType = db.AddressType.First(aDB => aDB.AddressTypeValue == addr.ClearanceAddressType);
                    dbAddress.AddressTypeId = dbAddressType.Id;

                    var addr1 = addr.Address1;
                    if (!String.IsNullOrEmpty(addr.BuildingNumber))
                        addr1 = addr.BuildingNumber + " " + addr1;
                    var addrSearch = new ViewModels.AddressLookupCriteriaViewModel() { Address1 = addr1, Address2 = addr.Address2, City = addr.City, State = addr.State, PostalCode = addr.PostalCode, County = addr.County };

                    dbAddress.Longitude = null;
                    dbAddress.Latitude = null;
                    var searchResults = AddressService.Lookup(Application.CompanyMap.Values.FirstOrDefault(cn => cn.Summary.CompanyNumber == client.CompanyNumber), addrSearch);
                    if (searchResults.Matches.Count > 0)
                    {
                        var primaryMatch = searchResults.Matches[0];
                        if (String.Equals(primaryMatch.Address1 ?? "", addrSearch.Address1 ?? "", StringComparison.CurrentCultureIgnoreCase)
                            && String.Equals(primaryMatch.Address2 ?? "", addrSearch.Address2 ?? "", StringComparison.CurrentCultureIgnoreCase)
                            && String.Equals(primaryMatch.City ?? "", addrSearch.City ?? "", StringComparison.CurrentCultureIgnoreCase)
                            && String.Equals(primaryMatch.State ?? "", addrSearch.State ?? "", StringComparison.CurrentCultureIgnoreCase)
                            && String.Equals(primaryMatch.PostalCode ?? "", addrSearch.PostalCode ?? "", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dbAddress.Latitude = primaryMatch.Latitude;
                            dbAddress.Longitude = primaryMatch.Longitude;
                        }
                    }
                }

                //Names
                foreach (var name in client.Names.Where(n => !String.IsNullOrEmpty(n.ClientNameType)))
                {
                    var dbName = db.ClientCoreNameClientNameType.FirstOrDefault(nDB => nDB.ClientId == dbClient.Id && nDB.ClientCoreNameId == name.SequenceNumber);
                    if (dbName == null)
                    {
                        dbName = db.ClientCoreNameClientNameType.Add(new Data.Models.ClientCoreNameClientNameType());
                        dbName.ClientCoreNameId = name.SequenceNumber.Value;
                        dbName.ClientId = dbClient.Id;
                    }

                    var nameType = db.ClientNameType.FirstOrDefault(cnt => cnt.NameType == name.ClientNameType);
                    dbName.ClientNameTypeId = nameType.Id;
                }

                //Attributes & CompanyCodes
                dbClient.CompanyNumberCode.Clear();
                foreach (var v in client.CompanyCodes)
                {
                    var code = db.CompanyNumberCode.FirstOrDefault(cnc => cnc.Id == v);
                    dbClient.CompanyNumberCode.Add(code);
                }
                foreach (var v in client.CompanyAttributes)
                {
                    var dbAttribute = db.ClientCompanyNumberAttributeValue.FirstOrDefault(ca => ca.ClientId == dbClient.Id && ca.CompanyNumberAttributeId == v.Item1);
                    if (dbAttribute == null)
                    {
                        dbAttribute = db.ClientCompanyNumberAttributeValue.Add(new Data.Models.ClientCompanyNumberAttributeValue());
                        dbAttribute.ClientId = dbClient.Id;
                        dbAttribute.CompanyNumberAttributeId = v.Item1;
                    }

                    dbAttribute.AttributeValue = v.Item2;
                }

                db.SaveChanges();
            }
        }

        /// <summary>
        /// Used by the ClientAddress web control to determine whether an address has geocoding information
        /// </summary>
        public static Tuple<decimal?, decimal?> GetAddressCoordinates(long clientCoreId, int clientCoreAddressId)
        {
            var results = new Tuple<decimal?, decimal?>(null, null);
            using (var db = Application.GetDatabaseInstance())
            {
                var address = db.ClientCoreAddressAddressType.FirstOrDefault(a => a.Client.ClientCoreClientId == clientCoreId && a.ClientCoreAddressId == clientCoreAddressId);
                if (address != null)
                    results = new Tuple<decimal?, decimal?>(address.Latitude, address.Longitude);
            }

            return results;
        }

        public static string OFACMessage { get { return "Potential OFAC match found for this client.  Please check with compliance before proceeding with this account"; } }
        /// <summary>
        /// Used to perform the OFAC search; returns true if there was a match
        /// </summary>
        public static bool IsOFACMatch(int companyId, int submissionId, string searchDescription, string name, string address, string city, string state, string zip, string country)
        {
            bool isMatch = false;
            using (var db = Application.GetDatabaseInstance())
            {
                var companyAdvSearch = db.CompanyClientAdvancedSearch.Where(adv => adv.CompanyId == companyId).First();

                var searchCriteria = new Int.Models.OFACSearchCriteria()
                {
                    Name = name,
                    Address = address,
                    City = city,
                    State = state,
                    PostalCode = zip,
                    Country = country
                };

                var ofacResults = Int.Services.ClientService.SearchOFAC(new Int.Models.AdvancedClientSettings() { Url = companyAdvSearch.ClientAdvSearchEndPointUrl, Username = companyAdvSearch.Username, Dsik = companyAdvSearch.Dsik }, searchDescription, searchCriteria);
                if (!ofacResults.Succeeded)
                    Application.HandleException(new Exception("OFAC Search Failed", ofacResults.Exception), new { CompanyId = companyId, SearchDescription = searchDescription, Criteria = searchCriteria });
                else
                    isMatch = (ofacResults.Matches.Count > 0);

                if (isMatch)
                {
                    var submission = db.Submission.Include(s => s.SubmissionComment).First(s => s.Id == submissionId);
                    submission.CreatedWithOFACHit = isMatch;

                    if (companyAdvSearch.UsesOfacSubmissionComment)
                        submission.SubmissionComment.Add(new Data.Models.SubmissionComment() { SubmissionId = submission.Id, Comment = OFACMessage, EntryBy = "OFAC Check" });

                    db.SaveChanges();
                }
            }

            return isMatch;
        }

        /// <summary>
        /// Used to determine if a company can edit the primary flag on client names
        /// </summary>
        public static bool AllowPrimaryNameChange(int companyId)
        {
            bool allow = false;
            using (var db = Application.GetDatabaseInstance())
            {
                var company = db.Company.FirstOrDefault(c => c.Id == companyId);
                allow = company.AllowClientPrimaryChanges;
            }

            return allow;
        }

        public static void SetClientPrimaryAddress(CompanyDetail cd, long clientId, long addressId)
        {
            if (cd.Configuration.AllowClientPrimaryChanges)
                Integration.Services.ClientService.UpdatePrimaryAddress(cd.SystemSettings.ToClientSystemSettings(), clientId, addressId);
        }

        public static List<ClassCodeViewModel> ClassCodeSearch(CompanyDetail cd, string term, int maxResults)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.ClassCode.AsExpandable()//DataMappers.ClassCode calls other mappers
                                   .Where(cc => cc.CompanyNumberId == cd.CompanyNumberId && (cc.CodeValue.Contains(term) || cc.Description.Contains(term)))
                                   .OrderBy(cc => cc.CodeValue)
                                   .Take(maxResults)
                                   .Select(DataToViewModel.ClassCode())
                                   .ToList();
            }
        }
    }
}
