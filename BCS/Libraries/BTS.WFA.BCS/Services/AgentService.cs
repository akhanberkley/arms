﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using System.Data.Entity;
using Int = BTS.WFA.Integration;
using System.IO;
using BTS.WFA.ViewModels;
using LinqKit;
using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.BCS.Services
{
    public class AgentService
    {
        public static List<AgentViewModel> Search(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.Agent.Where(ag => ag.Agency.CompanyNumberId == cd.CompanyNumberId);
                query = odataFilter(query) as IQueryable<Data.Models.Agent>;

                return query.AsExpandable().Select(DataToViewModel.Agent(true)).ToList();
            }
        }

        public static List<string> UnspecifiedReasons(CompanyDetail cd)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.CompanyNumberAgentUnspecifiedReason.Where(r => r.CompanyNumberId == cd.CompanyNumberId)
                                                             .OrderBy(r => r.AgentUnspecifiedReason.Reason)
                                                             .Select(r => r.AgentUnspecifiedReason.Reason)
                                                             .ToList();
            }
        }
    }
}