﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using System.Data.Entity;
using Int = BTS.WFA.Integration;
using BTS.WFA.ViewModels;
using LinqKit;
using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.BCS.Services
{
    public class CarrierService
    {
        public static List<CodeListItemViewModel> AllOptions()
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.OtherCarrier.AsExpandable()
                                      .OrderBy(o => o.Description)
                                      .Select(DataToViewModel.Carrier())
                                      .ToList();
            }
        }
        public static List<CodeListItemViewModel> CompanyOptions(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.CompanyNumberOtherCarrier.Where(c => c.CompanyNumberId == cd.CompanyNumberId)
                                                        .Select(c => c.OtherCarrier);
                if (odataFilter != null)
                    query = odataFilter(query) as IQueryable<Data.Models.OtherCarrier>;

                return query.Select(DataToViewModel.Carrier()).ToList();
            }
        }

        public static BaseViewModel SaveOptions(CompanyDetail cd, List<CodeListItemViewModel> options)
        {
            var results = new BaseViewModel();
            using (var db = Application.GetDatabaseInstance())
            {
                var allOptions = db.OtherCarrier.ToArray();
                var existingCompanyOptions = db.CompanyNumberOtherCarrier.Where(c => c.CompanyNumberId == cd.CompanyNumberId).Include(c => c.OtherCarrier).ToArray();

                var upToDateOptions = new List<Data.Models.OtherCarrier>();
                foreach (var o in options)
                {
                    var query = allOptions.AsQueryable();
                    if (!String.IsNullOrEmpty(o.Code))
                        query = query.Where(ao => ao.Code == o.Code);
                    if (!String.IsNullOrEmpty(o.Description))
                        query = query.Where(ao => ao.Description == o.Description);

                    var dbOption = query.FirstOrDefault();
                    if (dbOption == null)
                        results.AddValidation("Carrier", "Could not find Carrier {0}", o.Description);
                    else
                    {
                        upToDateOptions.Add(dbOption);
                        if (!existingCompanyOptions.Any(co => co.OtherCarrier == dbOption))
                            db.CompanyNumberOtherCarrier.Add(new Data.Models.CompanyNumberOtherCarrier() { CompanyNumberId = cd.CompanyNumberId, OtherCarrier = dbOption });
                    }
                }

                foreach (var existing in existingCompanyOptions)
                {
                    if (!upToDateOptions.Any(o => o == existing.OtherCarrier))
                        db.CompanyNumberOtherCarrier.Remove(existing);
                }

                if (results.ValidationResults.Count == 0)
                    db.SaveChanges();

                return results;
            }
        }
    }
}