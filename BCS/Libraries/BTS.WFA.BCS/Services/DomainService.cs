﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using System.Data.Entity;
using Int = BTS.WFA.Integration;
using BTS.WFA.ViewModels;
using LinqKit;
using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.BCS.Services
{
    public class DomainService
    {
        public static List<SicCodeViewModel> SicCodeList(Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.SicCodeList.AsQueryable();
                query = odataFilter(query) as IQueryable<Data.Models.SicCodeList>;

                return query.Select(DataToViewModel.SicCode()).ToList();
            }
        }

        public static List<NaicsCodeViewModel> NaicsCodeList(Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.NaicsCodeList.AsQueryable();
                query = odataFilter(query) as IQueryable<Data.Models.NaicsCodeList>;

                return query.Select(DataToViewModel.NaicsCode()).ToList();
            }
        }

        public static List<StateViewModel> FullStateList()
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.State.OrderBy(s => s.Abbreviation)
                               .Select(DataToViewModel.State())
                               .ToList();
            }
        }

        public static List<CodeListItemViewModel> FullCountryList()
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Country.Where(c => !String.IsNullOrEmpty(c.CountryCode))
                                 .OrderBy(c => c.Order)
                                 .Select(c => new CodeListItemViewModel() { Code = c.CountryCode, Description = c.CountryDescription })
                                 .ToList();
            }
        }

        public static List<string> ClientNameTypeList()
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.ClientNameType.OrderBy(t => t.NameType)
                                        .Select(t => t.NameType)
                                        .ToList();
            }
        }

        public static List<string> AddressTypeList()
        {   //Address types for the new client core (todo)
            using (var db = Application.GetDatabaseInstance())
            {
                return new List<string> { "Billing", "Mailing", "Physical" };
            }
        }

        public static List<string> LegacyAddressTypeList()
        {   //Address types for the old client core
            using (var db = Application.GetDatabaseInstance())
            {
                return db.AddressType.OrderBy(a => a.AddressTypeValue)
                                     .Select(a => a.AddressTypeValue)
                                     .ToList();
            }
        }

        public static List<string> ContactTypeList()
        {
            var list = new List<string>();
            list.Add(BTS.WFA.BCS.Domains.ContactType.Phone.ToString());
            list.Add(BTS.WFA.BCS.Domains.ContactType.Email.ToString());

            return list;
        }

        public static List<SubmissionStatusViewModel> SubmissionStatuses(CompanyDetail cd)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.CompanyNumberSubmissionStatus.Where(cs => cs.CompanyNumberId == cd.CompanyNumberId)
                                                       .OrderBy(cs => cs.Order)
                                                       .Select(cs => new SubmissionStatusViewModel()
                                                       {
                                                           Code = cs.SubmissionStatus.StatusCode,
                                                           IsDefault = cs.DefaultSelection,
                                                           Reasons = cs.SubmissionStatus.SubmissionStatusSubmissionStatusReason.OrderBy(r => r.SubmissionStatusReason.ReasonCode)
                                                                                                                               .Select(r => new SubmissionStatusReasonViewModel()
                                                                                                                               {
                                                                                                                                   Code = r.SubmissionStatusReason.ReasonCode,
                                                                                                                                   IsActive = r.Active
                                                                                                                               }).ToList()
                                                       }).ToList();
            }
        }

        public static List<string> HazardGradeTypes(CompanyDetail cn)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.CompanyNumber.Where(c => c.Id == cn.CompanyNumberId)
                                       .SelectMany(c => c.CompanyNumberHazardGradeType)
                                       .OrderBy(h => h.DisplayOrder)
                                       .Select(h => h.HazardGradeType.TypeName)
                                       .ToList();
            }
        }

        public static List<CodeListItemViewModel> PolicySystems(CompanyDetail cd)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.CompanyNumber.Where(c => c.Id == cd.CompanyNumberId)
                                       .SelectMany(c => c.PolicySystem)
                                       .OrderBy(p => p.Description)
                                       .Select(p => new CodeListItemViewModel { Code = p.Abbreviation, Description = p.Description })
                                       .ToList();
            }
        }

        public static List<NumberControlViewModel> NumberControls(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.NumberControl.Where(nc => nc.CompanyNumberId == cd.CompanyNumberId);
                query = odataFilter(query) as IQueryable<Data.Models.NumberControl>;

                return query.Select(DataToViewModel.NumberControl()).ToList();
            }
        }

        public static List<string> UserTypeList()
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.UserType.OrderBy(t => t.Name)
                                  .Select(t => t.Name)
                                  .ToList();
            }
        }

        public static List<CodeListItemViewModel> UserRoles(CompanyDetail cd, bool showSystemAdmin)
        {
            using (var db = Application.GetDatabaseInstance())
            {   //filtering out anything w/o a description as a way to just show the new system roles (can be taken out once the old system is retired)
                return db.Role.Where(r => (showSystemAdmin || r.RoleName != Domains.Roles.SystemAdministrator) && !String.IsNullOrEmpty(r.Description))
                              .OrderBy(r => r.RoleName)
                              .Select(r => new CodeListItemViewModel() { Code = r.RoleName, Description = r.Description })
                              .ToList();
            }
        }
    }
}