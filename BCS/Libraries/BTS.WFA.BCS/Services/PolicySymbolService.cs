﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using System.Data.Entity;
using Int = BTS.WFA.Integration;
using BTS.WFA.ViewModels;
using LinqKit;
using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.BCS.Services
{
    public class PolicySymbolService
    {
        public static List<PolicySymbolViewModel> Search(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.PolicySymbol.AsExpandable().Where(c => c.CompanyNumberId == cd.CompanyNumberId);
                query = odataFilter(query) as IQueryable<Data.Models.PolicySymbol>;

                return query.OrderBy(p => p.Code).Select(DataToViewModel.PolicySymbol()).ToList();
            }
        }

        public static PolicySymbolViewModel Save(CompanyDetail cd, string code, PolicySymbolViewModel policySymbol)
        {
            bool isNew = String.IsNullOrEmpty(code);

            using (var db = Application.GetDatabaseInstance())
            {
                if (String.IsNullOrEmpty(policySymbol.Code))
                    policySymbol.AddValidation("Code", "Code is required");
                else if (db.PolicySymbol.Any(ps => ps.CompanyNumberId == cd.CompanyNumberId && ps.Code == policySymbol.Code && (isNew || code != policySymbol.Code)))
                    policySymbol.AddValidation("Code", "The code {0} already exists", policySymbol.Code);

                if (String.IsNullOrEmpty(policySymbol.Description))
                    policySymbol.AddValidation("Description", "Description is required");

                if(policySymbol.ValidationResults.Count == 0)
                {
                    Data.Models.PolicySymbol dbPolicySymbol = null;
                    if (isNew)
                        dbPolicySymbol = db.PolicySymbol.Add(new Data.Models.PolicySymbol() { Id = Guid.NewGuid(), CompanyNumberId = cd.CompanyNumberId });
                    else
                        dbPolicySymbol = db.PolicySymbol.FirstOrDefault(ps => ps.CompanyNumberId == cd.CompanyNumberId && ps.Code == code);

                    if (dbPolicySymbol == null)
                        policySymbol.AddValidation("Code", "Policy Symbol {0} could not be found", code);
                    else
                    {
                        dbPolicySymbol.Code = policySymbol.Code;
                        dbPolicySymbol.Description = policySymbol.Description;
                        dbPolicySymbol.RetirementDate = policySymbol.RetirementDate;
                        dbPolicySymbol.PrimaryOption = policySymbol.IsPrimaryOption;
                        dbPolicySymbol.PackageOption = policySymbol.IsPackageOption;
                        dbPolicySymbol.PolicyNumberPrefix = policySymbol.PolicyNumberPrefix;

                        var dbNumberControl = (policySymbol.NumberControl == null) ? null : db.NumberControl.FirstOrDefault(nc => nc.CompanyNumberId == cd.CompanyNumberId && nc.Id == policySymbol.NumberControl.Id);
                        if(policySymbol.NumberControl != null && dbNumberControl == null)
                            policySymbol.AddValidation("NumberControl", "Number Control {0} could not be found", policySymbol.NumberControl.Id);
                        else
                        {
                            dbPolicySymbol.NumberControlId = (dbNumberControl == null) ? (int?)null : dbNumberControl.Id;

                            db.SaveChanges();
                        }
                    }
                }

                return policySymbol;
            }
        }

        public static BaseViewModel Delete(CompanyDetail cd, string code)
        {
            var validationResults = new BaseViewModel();

            using (var db = Application.GetDatabaseInstance())
            {
                var dbPolicySymbol = db.PolicySymbol.FirstOrDefault(ps => ps.CompanyNumberId == cd.CompanyNumberId && ps.Code == code);
                if (dbPolicySymbol == null)
                    validationResults.AddValidation("Code", "Policy Symbol {0} could not be found", code);
                else
                {
                    if(db.Submission.Any(s => s.CompanyNumberId == cd.CompanyNumberId && (s.PolicySymbolId == dbPolicySymbol.Id || s.PackagePolicySymbols.Any(pps => pps.Id == dbPolicySymbol.Id))))
                        validationResults.AddValidation("PolicySymbol", "Policy Symbol {0} can not be deleted because it is associated with existing submissions", code);
                    else
                    {
                        db.PolicySymbol.Remove(dbPolicySymbol);
                        db.SaveChanges();
                    }
                }


                return validationResults;
            }
        }
    }
}