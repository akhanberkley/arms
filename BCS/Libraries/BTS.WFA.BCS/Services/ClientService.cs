﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using System.Data.Entity;
using Int = BTS.WFA.Integration;
using System.Net.Mail;
using BTS.WFA.ViewModels;
using LinqKit;
using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.BCS.Services
{
    public class ClientService
    {
        public static ClientSearchResultsViewModel GetByClientSystemId(CompanyDetail cd, ClientSystemId clientSystemId, bool populateBCSData = true)
        {
            var result = new ClientSearchResultsViewModel();
            List<ClientClearanceData> clearanceData = null;
            decimal? totalPremium = null;

            Int.Models.ClientSearchResult searchResult = null;
            var ccCriteria = cd.SystemSettings.ToClientSearchCriteria();
            ccCriteria.ClientId = clientSystemId.ToString();

            List<Task> tasks = new List<Task>();
            //Get from Client System
            tasks.Add(Task.Factory.StartNew(() =>
            {
                searchResult = Int.Services.ClientService.Search(cd.SystemSettings.ToClientSystemSettings(), ccCriteria, 300);
            }));
            //Get from Clearance
            if (populateBCSData)
            {
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        clearanceData = db.Client.AsExpandable()
                                                 .Where(c => c.CompanyNumberId == cd.CompanyNumberId && c.ClientCoreClientId == clientSystemId.Id)
                                                 .Select(ClientToViewModel.FromDbAdvanced())
                                                 .ToList();
                    }
                }));
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var now = DateTime.Now;
                        var dbActiveSubmissions = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.ClientCoreClientId == clientSystemId.Id && (s.ExpirationDate == null || s.ExpirationDate > now))
                                                               .Select(s => new
                                                               {
                                                                   Premium = s.EstimatedWrittenPremium ?? 0
                                                               }).ToList();
                        if (dbActiveSubmissions.Any())
                            totalPremium = dbActiveSubmissions.Sum(s => s.Premium);
                    }
                }));
            }

            Task.WaitAll(tasks.ToArray());
            if (!searchResult.Succeeded)
            {
                Application.HandleException(new AggregateException("Client Get By Id Failed", searchResult.Exception), new { Company = cd.Summary, Criteria = ccCriteria });
                result.Failures.Add(MapSearchFailure("Id: " + clientSystemId.ToString(), searchResult.Exception));
            }
            else if (searchResult.Matches.Count > 0)
            {
                result.Matches.AddRange(ClientToViewModel.FromClientSystem<ClientViewModel>(cd, searchResult.Matches));
                var vmClient = result.Matches[0] as ClientViewModel;
                if (populateBCSData)
                {
                    var dbClient = clearanceData.FirstOrDefault(c => c.ClientCoreClientId == vmClient.ClientCoreId.Value);
                    if (dbClient != null)
                    {
                        ClientToViewModel.PopulateSimpleData(vmClient, dbClient);

                        vmClient.ExperianId = dbClient.ExperianId;
                        vmClient.BusinessDescription = dbClient.BusinessDescription;
                        vmClient.SicCode = dbClient.SicCode;
                        vmClient.NaicsCode = dbClient.NaicsCode;
                        vmClient.CompanyCustomizations.AddRange(dbClient.CompanyNumberCode);
                        vmClient.CompanyCustomizations.AddRange(dbClient.ClientCompanyNumberAttributeValue);
                        vmClient.AccountPremium = totalPremium;
                    }
                }
            }

            return result;
        }

        public static List<ClientCrossCompanySearchResultsViewModel> GetSimilarClients(CompanyDetail cd, string insuredName, string state)
        {
            if (String.IsNullOrEmpty(insuredName))
                return null;
            else
            {
                var results = new List<ClientCrossCompanySearchResultsViewModel>();
                var companiesToSearch = new List<CompanyDetail>();
                companiesToSearch.Add(cd);
                companiesToSearch.AddRange(cd.SystemSettings.CrossClearanceCompanyNumbers.Select(c => Application.CompanyMap[c.Abbreviation]));

                var tasks = new List<Task>();
                foreach (var c in companiesToSearch)
                {
                    tasks.Add(Task.Factory.StartNew(() =>
                    {
                        var searchResults = SearchClientSystem(c, new ClientSearchCriteriaViewModel() { BusinessName = insuredName, State = state });
                        foreach (var m in searchResults.Matches)
                            results.Add(new ClientCrossCompanySearchResultsViewModel() { Company = c.Summary, ClientId = m.ClientCoreId.Value });
                    }));
                }

                Task.WaitAll(tasks.ToArray());
                return results;
            }
        }
        public static ClientSearchResultsViewModel Search(CompanyDetail cd, ClientSearchCriteriaViewModel criteria)
        {
            var result = SearchClientSystem(cd, criteria);
            if (!result.CriteriaNotSpecific)
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    var clientIds = result.Matches.Select(c => c.ClientCoreId).ToArray();
                    if (clientIds.Length > 0)
                    {
                        var dbClients = db.Client.AsExpandable()
                                                 .Where(c => c.CompanyNumberId == cd.CompanyNumberId && clientIds.Contains(c.ClientCoreClientId))
                                                 .Select(ClientToViewModel.FromDbSimple())
                                                 .ToList();

                        foreach (var dbClient in dbClients)
                        {
                            var vmClient = result.Matches.First(m => m.ClientCoreId == dbClient.ClientCoreClientId);
                            ClientToViewModel.PopulateSimpleData(vmClient, dbClient);
                        }
                    }
                }
            }

            return result;
        }
        private static ClientSearchResultsViewModel SearchClientSystem(CompanyDetail cd, ClientSearchCriteriaViewModel criteria)
        {
            var exceptions = new List<Exception>();
            var result = new ClientSearchResultsViewModel();
            var ccSettings = cd.SystemSettings.ToClientSystemSettings();

            List<ClientSearchCriteriaViewModel> searchCriterias = new List<ClientSearchCriteriaViewModel>();
            searchCriterias.Add(criteria);

            if (!String.IsNullOrEmpty(criteria.PhoneNumber))
            {
                var phResults = Int.Services.ClientService.PhoneNumberSearch(cd.SystemSettings.ToAdvancedClientSettings(), criteria.PhoneNumber);

                if (phResults.Succeeded)
                {
                    foreach (var c in phResults.ClientCoreIds)
                        searchCriterias.Add(new ClientSearchCriteriaViewModel() { ClientCoreId = c.ToString(), EffectiveDate = criteria.EffectiveDate });
                    if (phResults.MatchedBusiness != null)
                    {
                        var mb = phResults.MatchedBusiness;
                        searchCriterias.Add(new ClientSearchCriteriaViewModel()
                            {
                                BusinessName = mb.Name,
                                Address = mb.Address1,
                                City = mb.City,
                                State = mb.State,
                                PostalCode = mb.PostalCode,
                                EffectiveDate = criteria.EffectiveDate
                            });
                    }
                }
                else
                {
                    exceptions.Add(phResults.Exception);
                    result.Failures.Add(MapSearchFailure("Phone Number: " + criteria.PhoneNumber, phResults.Exception));
                }
            }

            Func<object, Int.Models.ClientSearchResult> searchAction = ((o) => Int.Services.ClientService.Search(ccSettings, o as Int.Models.ClientSearchCriteria, 300));
            List<Task<Int.Models.ClientSearchResult>> searches = new List<Task<Int.Models.ClientSearchResult>>();
            foreach (var c in searchCriterias)
            {
                if (!String.IsNullOrEmpty(c.ClientCoreId))
                {
                    var ccClientIdCriteria = cd.SystemSettings.ToClientSearchCriteria();
                    ccClientIdCriteria.ClientId = c.ClientCoreId;
                    ccClientIdCriteria.EffectiveDate = c.EffectiveDate;
                    searches.Add(Task.Factory.StartNew(searchAction, ccClientIdCriteria));
                }

                if (!String.IsNullOrEmpty(c.FEIN))
                {
                    var ccTaxIdCriteria = cd.SystemSettings.ToClientSearchCriteria();
                    ccTaxIdCriteria.TaxId = String.Join("*", c.FEIN.Replace("-", "").ToCharArray()) + "*";
                    ccTaxIdCriteria.EffectiveDate = c.EffectiveDate;
                    searches.Add(Task.Factory.StartNew(searchAction, ccTaxIdCriteria));
                }

                if (!String.IsNullOrEmpty(c.BusinessName))
                {
                    var ccNameCriteria = cd.SystemSettings.ToClientSearchCriteria();
                    if (!String.IsNullOrEmpty(c.BusinessName))
                        ccNameCriteria.BusinessName = (cd.SystemSettings.DisableClientWildcardSearch) ? c.BusinessName : string.Format("*{0}*", c.BusinessName);
                    //We will restrict by state or postal code if available
                    if (!String.IsNullOrEmpty(c.State))
                        ccNameCriteria.StateName = c.State;
                    else if (!String.IsNullOrEmpty(c.PostalCode))
                        ccNameCriteria.PostalCode = c.PostalCode;

                    ccNameCriteria.EffectiveDate = c.EffectiveDate;
                    searches.Add(Task.Factory.StartNew(searchAction, ccNameCriteria));
                }

                if (!String.IsNullOrEmpty(c.Address) && ((!String.IsNullOrEmpty(c.City) && !String.IsNullOrEmpty(c.State)) || !String.IsNullOrEmpty(c.PostalCode)))
                {
                    var ccAddressCriteria = cd.SystemSettings.ToClientSearchCriteria();

                    var splitAddressLine = AddressService.SplitBuildNumberAddressLine(c.Address);
                    if (!String.IsNullOrEmpty(splitAddressLine.Item1))
                        ccAddressCriteria.HouseNumber = splitAddressLine.Item1;
                    if (!String.IsNullOrEmpty(splitAddressLine.Item2))
                        ccAddressCriteria.Address1 = splitAddressLine.Item2 + "*";

                    if (!String.IsNullOrEmpty(c.City))
                        ccAddressCriteria.CityName = c.City + "*";
                    if (!String.IsNullOrEmpty(c.State))
                        ccAddressCriteria.StateName = c.State;
                    if (!String.IsNullOrEmpty(c.PostalCode))
                        ccAddressCriteria.PostalCode = c.PostalCode;

                    ccAddressCriteria.EffectiveDate = c.EffectiveDate;
                    searches.Add(Task.Factory.StartNew(searchAction, ccAddressCriteria));
                }

                if (cd.Configuration.ClientFields.PortfolioInformation.Enabled)
                {
                    if (c.PortfolioId != null || !String.IsNullOrEmpty(c.PortfolioName))
                    {
                        var ccPortfolioCriteria = cd.SystemSettings.ToClientSearchCriteria();
                        ccPortfolioCriteria.PortfolioId = c.PortfolioId;
                        if (!String.IsNullOrEmpty(c.PortfolioName))
                            ccPortfolioCriteria.PortfolioName = c.PortfolioName;

                        searches.Add(Task.Factory.StartNew(searchAction, ccPortfolioCriteria));
                    }
                }
            }

            //It's possible they entered a ph # search, but it didn't return anything
            if (searches.Count == 0 && String.IsNullOrEmpty(criteria.PhoneNumber))
                result.CriteriaNotSpecific = true;
            else
            {
                //Wait for client searches to all complete
                Task.WaitAll(searches.ToArray());

                //Populate results
                Dictionary<long, Tuple<int, Int.Models.Client>> matches = new Dictionary<long, Tuple<int, Int.Models.Client>>();
                foreach (var s in searches)
                {
                    var r = s.Result;
                    if (!r.Succeeded)
                    {
                        result.Failures.Add(MapSearchFailure((s.AsyncState as Int.Models.ClientSearchCriteria).Stringify(), r.Exception));
                        //Too Many Result messages don't really need to be logged
                        if (!(r.Exception is Int.Models.TooManyResultsException))
                            exceptions.Add(r.Exception);
                    }
                    else
                    {
                        //Some of these queries return multiple instances of the same client; only use the first instance
                        foreach (var match in r.Matches.GroupBy(c => c.Id).Select(g => g.First()))
                        {
                            if (matches.ContainsKey(match.Id.Value))
                            {
                                var m = matches[match.Id.Value];
                                matches[match.Id.Value] = new Tuple<int, Int.Models.Client>(m.Item1 + 1, m.Item2);
                            }
                            else
                                matches.Add(match.Id.Value, new Tuple<int, Int.Models.Client>(0, match));
                        }
                    }
                }

                if (exceptions.Count > 0)
                    Application.HandleException(new AggregateException("Client Search Failed", exceptions), new { Company = cd.Summary, Criteria = criteria });

                var orderedMatches = matches.OrderByDescending(m => m.Value.Item1);
                if (!String.IsNullOrEmpty(criteria.PostalCode))
                    orderedMatches = orderedMatches.ThenByDescending(m => m.Value.Item2.Addresses.Any(a => (a.PostalCode ?? "").IndexOf(criteria.PostalCode, StringComparison.CurrentCultureIgnoreCase) >= 0));
                if (!String.IsNullOrEmpty(criteria.State))
                    orderedMatches = orderedMatches.ThenByDescending(m => m.Value.Item2.Addresses.Any(a => (a.State ?? "").Equals(criteria.State, StringComparison.CurrentCultureIgnoreCase)));
                if (!String.IsNullOrEmpty(criteria.City))
                    orderedMatches = orderedMatches.ThenByDescending(m => m.Value.Item2.Addresses.Any(a => (a.City ?? "").IndexOf(criteria.City, StringComparison.CurrentCultureIgnoreCase) >= 0));

                result.Matches.AddRange(ClientToViewModel.FromClientSystem<ClientSummaryViewModel>(cd, orderedMatches.ThenBy(m => m.Value.Item2.Id).Select(m => m.Value.Item2)));
            }

            return result;
        }
        private static ClientSearchFailureViewModel MapSearchFailure(string criteria, Exception ex)
        {
            var exceptionType = ClientSearchResultsType.Failed;
            if (ex is Int.Models.TooManyResultsException)
                exceptionType = ClientSearchResultsType.TooManyResults;
            else if (ex is TimeoutException)
                exceptionType = ClientSearchResultsType.TimedOut;
            return new ClientSearchFailureViewModel()
            {
                Status = exceptionType,
                StatusMessage = ex.Message,
                Criteria = criteria
            };
        }

        public static List<ClassCodeViewModel> GetClassCodes(CompanyDetail cd, ClientSystemId clientSystemId, Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var submissionQuery = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId);
                submissionQuery = clientSystemId.AsSubmissionFilter(cd, db, submissionQuery);

                var query = submissionQuery.SelectMany(s => s.ClassCodes).Distinct();
                if (odataFilter != null)
                    query = odataFilter(query) as IQueryable<Data.Models.ClassCode>;

                return query.AsExpandable().Select(DataToViewModel.ClassCode()).ToList();
            }
        }

        public static void AddClassCode(CompanyDetail cd, ClientSystemId clientSystemId, string classCodeValue)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbClassCode = db.ClassCode.Where(cc => cc.CompanyNumberId == cd.CompanyNumberId && cc.CodeValue == classCodeValue).FirstOrDefault();
                if (dbClassCode != null)
                {
                    var dbSubmissions = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && !s.ClassCodes.Any(cc => cc.Id == dbClassCode.Id));
                    dbSubmissions = clientSystemId.AsSubmissionFilter(cd, db, dbSubmissions);

                    foreach (var dbSubmission in dbSubmissions.Include(s => s.ClassCodes).ToArray())
                        dbSubmission.ClassCodes.Add(dbClassCode);

                    db.SaveChanges();
                }
            }
        }

        public static void DeleteClassCode(CompanyDetail cd, ClientSystemId clientSystemId, string classCodeValue)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbSubmissions = db.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId);
                dbSubmissions = clientSystemId.AsSubmissionFilter(cd, db, dbSubmissions);

                foreach (var dbSubmission in dbSubmissions.Include(s => s.ClassCodes).ToArray())
                {
                    foreach (var dbClassCode in dbSubmission.ClassCodes.Where(cc => cc.CodeValue == classCodeValue).ToArray())
                        dbSubmission.ClassCodes.Remove(dbClassCode);
                }

                db.SaveChanges();
            }
        }

        public static void Validate(CompanyDetail cd, ClientViewModel client, CodeListItemViewModel segmentOverride)
        {
            Validation.ClientValidator.Validate(cd, client, segmentOverride);
        }
        public static ClientViewModel Save(CompanyDetail cd, ClientSystemId clientSystemId, ClientViewModel client, ClientSaveOptions options, string updateUser)
        {
            Validate(cd, client, options.SegmentOverride);

            if (client.ValidationResults.Count == 0)
            {
                bool isNew = (clientSystemId == null);
                var mapper = new ClientToData(cd, isNew, client);
                Int.Models.ClientSaveResult ccResults = null;

                List<Task> tasks = new List<Task>();
                //Send to Client System
                tasks.Add(Task.Factory.StartNew(() =>
                {
                    ccResults = mapper.SaveInClientSystem(clientSystemId, options.SegmentOverride);
                    if (ccResults.Succeeded && options.UpdatePorftolioName)
                    {
                        var updateResults = mapper.UpdatePortfolioNameInClientSystem(client.Portfolio.Id.Value, client.Portfolio.Name);
                        ccResults.Exception = updateResults.Exception;
                    }
                }));

                //If Ids are missing, wait until the previous call finishes so we can populate them
                if (client.ClientCoreId == null || client.PrimaryName.SequenceNumber == null || client.AdditionalNames.Any(n => n.SequenceNumber == null) || client.Addresses.Any(a => a.ClientCoreAddressId == null))
                {
                    Task.WaitAll(tasks.ToArray());
                    if (ccResults.Succeeded)
                    {
                        client.ClientCoreId = ccResults.Id.Value;
                        var searchResults = GetByClientSystemId(cd, new ClientSystemId { Id = ccResults.Id, CmsId = ccResults.CmsId }, false);
                        if (searchResults.Matches.Count == 1)
                            mapper.SetSystemIds(searchResults.Matches[0]);
                    }
                }

                //Update in BCS db
                using (var db = Application.GetDatabaseInstance())
                {
                    using (var trans = db.GetTransaction())
                    {
                        try
                        {
                            var dbClient = db.Client.Include(c => c.ClientClassCode)
                                                    .Include(c => c.ClientClassCode.Select(cc => cc.ClassCode))
                                                    .Include(c => c.ClientCoreNameClientNameType)
                                                    .Include(c => c.ClientCoreAddressAddressType)
                                                    .Include(c => c.ClientCompanyNumberAttributeValue)
                                                    .FirstOrDefault(c => c.CompanyNumberId == cd.CompanyNumberId && c.ClientCoreClientId == client.ClientCoreId);
                            if (dbClient == null && client.ClientCoreId.HasValue)
                            {
                                dbClient = db.Client.Add(new Data.Models.Client());
                                dbClient.CompanyNumberId = cd.CompanyNumberId;
                                dbClient.ClientCoreClientId = client.ClientCoreId.Value;
                                dbClient.EntryBy = updateUser;

                                //Make sure we have the Id for the future steps
                                db.SaveChanges();
                            }

                            //This will be null if it's a new client AND the client core save failed
                            if (dbClient != null)
                            {
                                dbClient.ModifiedBy = updateUser;
                                dbClient.ModifiedDt = DateTime.Now.ToString("MMM dd yyyy hh:mmtt");

                                tasks.Add(Task.Factory.StartNew(() => mapper.SetSicCode(dbClient)));
                                tasks.Add(Task.Factory.StartNew(() => mapper.SetNaicsCode(dbClient)));
                                tasks.Add(Task.Factory.StartNew(() => mapper.SetNames(dbClient)));
                                tasks.Add(Task.Factory.StartNew(() => mapper.SetAddresses(dbClient)));
                                mapper.SetBasicDetails(dbClient);
                                mapper.SetCustomizations(db, dbClient);
                            }

                            Task.WaitAll(tasks.ToArray());
                            if (ccResults.Succeeded)
                            {
                                db.SaveChanges();
                                trans.Commit();

                                if (options.ReturnFullClient)
                                {
                                    var updatedClient = GetByClientSystemId(cd, new ClientSystemId { Id = client.ClientCoreId, CmsId = client.CmsId });
                                    if (updatedClient.Matches.Count > 0)
                                        client = (ClientViewModel)updatedClient.Matches[0];
                                }

                                client.ItemCreated = isNew;
                            }
                            else
                            {
                                throw new Exception("Client System Failure", ccResults.Exception);
                            }
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            throw ex;
                        }
                    }
                }

                if (!ccResults.Succeeded)
                {
                    Application.HandleException(ccResults.Exception, new { Settings = cd.Configuration, Company = cd.Summary, ClientSystemId = clientSystemId, Client = client });
                    client.AddValidation("ClientCore", "BCS was unable to store the client.");
                }
            }

            return client;
        }
    }

    public class ClientSaveOptions
    {
        public bool UpdatePorftolioName { get; set; }
        public bool ReturnFullClient { get; set; }

        public CodeListItemViewModel SegmentOverride { get; set; }
    }
}
