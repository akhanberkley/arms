﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using Int = BTS.WFA.Integration;
using System.Data.Entity;

namespace BTS.WFA.BCS.Services
{
    public class CompanyService
    {
        public static Dictionary<string, CompanyDetail> GetCompanyMap()
        {
            var map = new Dictionary<string, CompanyDetail>();
            using (var db = Application.GetDatabaseInstance())
            {
                var cns = db.CompanyNumber.Where(cn => cn.Company.Active)
                                          .Select(cn => new CompanyDetail()
                                                        {
                                                            CompanyNumberId = cn.Id,
                                                            CompanyId = cn.CompanyId,
                                                            Summary = new CompanySummaryViewModel()
                                                            {
                                                                CompanyNumber = cn.CompanyNumberValue,
                                                                CompanyName = cn.Company.CompanyName,
                                                                Abbreviation = cn.Company.CompanyInitials.ToUpper()
                                                            }
                                                        });

                foreach (var cn in cns)
                    map.Add(cn.Summary.Abbreviation, cn);


                SetupSettings(db, map);
                SetupClientFields(db, map);
                SetupSubmissionFields(db, map);
            }

            return map;
        }
        private static void SetupSettings(Data.IClearanceDb db, Dictionary<string, CompanyDetail> map)
        {

            foreach (var c in db.Company.Where(c => c.Active).Select(c => new
                                                                    {
                                                                        co = c,
                                                                        cs = c.CompanyClientAdvancedSearch.FirstOrDefault(),
                                                                        cp = c.CompanyParameter.FirstOrDefault(),
                                                                        UnderwritingRole = c.UnderwritingRole.RoleCode,
                                                                        AnalystRole = c.AnalystRole.RoleCode,
                                                                        TechnicianRole = c.TechnicianRole.RoleCode,
                                                                        PolicyNumberGeneratable = c.CompanyNumber.Any(cn => cn.NumberControl.Any(nc => nc.NumberControlTypeId == 1)),
                                                                        ClientCoreSegments = c.CompanyNumber.SelectMany(cn => cn.CompanyNumberClientCoreSegment).Select(s => new ClientCoreSegmentViewModel { Code = s.Segment, Description = s.Description, IsDefault = s.IsDefault }).ToList(),
                                                                        CrossClearanceCompanyAbbreviations = c.CompanyNumber.SelectMany(cn => cn.ClearsCompanyNumbers).Select(cn => cn.ClearsCompanyNumber.Company.CompanyInitials).ToList()
                                                                    }))
            {
                foreach (var cn in map.Values.Where(cn => cn.CompanyId == c.co.Id))
                {
                    cn.SystemSettings = new CompanySystemSettings()
                    {
                        ClientCoreSystemCd = c.co.ClientCoreSystemCd,
                        ClientCoreUsername = c.co.ClientCoreUsername,
                        ClientCorePassword = c.co.ClientCorePassword,
                        ClientCoreLocalId = c.co.ClientCoreLocalId,
                        ClientCoreCorpId = c.co.ClientCoreCorpId,
                        ClientCoreCorpCode = c.co.ClientCoreCorpCode,
                        ClientCoreSegment = c.co.ClientCoreOwner,
                        ClientCoreClientType = c.co.ClientCoreClientType,
                        ClientCoreSearchClientType = (c.cp != null) ? c.cp.ClientTypeForSearch : null,
                        ClientCoreEndPointUrl = c.co.ClientCoreEndPointUrl,
                        SearchFilter = (c.cp != null && !String.IsNullOrEmpty(c.cp.SearchFilter)) ? c.cp.SearchFilter.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries) : null,
                        DisableClientWildcardSearch = (c.cp != null) ? c.cp.FuzzySearchEnabled : false,
                        SearchFilterService = (c.cp != null) ? c.cp.SearchFilterService : 0,
                        ClientAdvSearchEndPointUrl = (c.cs != null) ? c.cs.ClientAdvSearchEndPointUrl : null,
                        ClientExperianSearchEndPointUrl = (c.cs != null) ? c.cs.ClientExperianSearchEndPointUrl : null,
                        ClientAdvSearchUserName = (c.cs != null) ? c.cs.Username : null,
                        ClientAdvSearchDsik = (c.cs != null && c.cs.Dsik != null) ? c.cs.Dsik.Trim() : null,
                        DuplicateSubmissionQuery = (c.cp != null) ? c.cp.DuplicateSubmissionQuery : null,
                        ApsServiceUrl = (c.cp != null) ? c.cp.ApsService : null,
                        ApsQueueName = (c.cp != null) ? c.cp.MqName : null,
                        ApsQueueHost = (c.cp != null) ? c.cp.MqHostName : null,
                        ApsQueueChannel = (c.cp != null) ? c.cp.MqChannel : null,
                        ApsQueueManager = (c.cp != null) ? c.cp.MqManager : null,
                        ApsQueuePort = (c.cp != null && !String.IsNullOrEmpty(c.cp.MqPort)) ? int.Parse(c.cp.MqPort) : (int?)null,
                        UsesOfacSubmissionNote = (c.cs != null) ? c.cs.UsesOfacSubmissionComment : false,
                        UsesDuplicateSubmissionNote = c.co.SupportsDuplicates,
                        CrossClearanceCompanyNumbers = c.CrossClearanceCompanyAbbreviations.Select(ccc => map[ccc].Summary).ToList()
                    };
                    if (!String.IsNullOrEmpty(c.UnderwritingRole))
                        cn.SystemSettings.UnderwritingRoles.Add(c.UnderwritingRole);
                    if (!String.IsNullOrEmpty(c.AnalystRole))
                        cn.SystemSettings.UnderwritingAnalystRoles.Add(c.AnalystRole);
                    if (!String.IsNullOrEmpty(c.TechnicianRole))
                        cn.SystemSettings.UnderwritingTechnicianRoles.Add(c.TechnicianRole);

                    if (c.cp != null && !String.IsNullOrEmpty(c.cp.PrimaryUwRole))
                        cn.SystemSettings.UnderwritingRoles.Add(c.cp.PrimaryUwRole);
                    if (c.cp != null && !String.IsNullOrEmpty(c.cp.PrimaryAnRole))
                        cn.SystemSettings.UnderwritingAnalystRoles.Add(c.cp.PrimaryAnRole);

                    if (c.cs != null && c.cs.IsPhoneRequiredField)
                        cn.AddValidationSetting(Validation.Validators.ClientPhoneRequired);

                    cn.Configuration = new CompanyConfiguration()
                    {
                        AgentsEnabled = (c.co.Id != (int)Domains.Companies.AIC),
                        AnalystEnabled = (cn.SystemSettings.UnderwritingAnalystRoles.Count > 0),
                        TechnicianEnabled = (cn.SystemSettings.UnderwritingTechnicianRoles.Count > 0),
                        UsesAllUnderwritersForAllAssignments = (c.cp != null) ? c.cp.FillAllAssignments : false,
                        FutureEffectiveDateDayCount = (c.cp != null) ? c.cp.SubmissionStatusDateTbd : null,
                        AllowClientPrimaryChanges = c.co.AllowClientPrimaryChanges,
                        APSAgencyUrl = (c.cp != null) ? c.cp.APSAgencyUrl : null,
                        PolicyNumberRegex = c.co.PolicyNumberRegex,
                        PolicyNumberGeneratable = c.PolicyNumberGeneratable,
                        ClientCoreSegments = c.ClientCoreSegments,
                        SubmissionEnforceAgencyLicensedState = c.co.EnforceAgencyLicensedState,
                        UsesCms = (c.cp != null && c.cp.SearchFilterService == 3),
                    };

                }
            }
        }
        private static void SetupClientFields(Data.IClearanceDb db, Dictionary<string, CompanyDetail> map)
        {
            var clientFields = db.CompanyNumberClientField.Select(t => new
                                                                 {
                                                                     CompanyNumberId = t.CompanyNumberId,
                                                                     Id = t.ClientFieldId,
                                                                     Label = t.ClientField.DisplayLabel ?? t.DisplayLabel,
                                                                     Required = t.Required,
                                                                     VisibleOnAdd = t.VisibleOnAdd
                                                                 }).ToArray();

            foreach (var cn in map.Values)
            {
                foreach (var f in clientFields.Where(t => t.CompanyNumberId == cn.CompanyNumberId))
                {
                    ClientFieldViewModel field = null;
                    if (f.Id == Domains.ClientFields.ContactInformation)
                        field = cn.Configuration.ClientFields.ContactInformation;
                    else if (f.Id == Domains.ClientFields.FEIN)
                        field = cn.Configuration.ClientFields.FEIN;
                    else if (f.Id == Domains.ClientFields.SICCode)
                        field = cn.Configuration.ClientFields.SicCode;
                    else if (f.Id == Domains.ClientFields.NAICSCode)
                        field = cn.Configuration.ClientFields.NaicsCode;
                    else if (f.Id == Domains.ClientFields.AccountPremium)
                        field = cn.Configuration.ClientFields.AccountPremium;
                    else if (f.Id == Domains.ClientFields.BusinessDescription)
                        field = cn.Configuration.ClientFields.BusinessDescription;
                    else if (f.Id == Domains.ClientFields.PortfolioInformation)
                        field = cn.Configuration.ClientFields.PortfolioInformation;
                    else if (f.Id == Domains.ClientFields.ClientCoreSegment)
                        field = cn.Configuration.ClientFields.ClientCoreSegment;
                    else if (f.Id == Domains.ClientFields.ExperianId)
                        field = cn.Configuration.ClientFields.ExperianId;

                    if(field != null)
                    {
                        field.Enabled = true;
                        field.Label = f.Label;
                        field.Required = f.Required;
                        field.VisibleOnAdd = f.VisibleOnAdd;
                    }
                }
            }
        }
        private static void SetupSubmissionFields(Data.IClearanceDb db, Dictionary<string, CompanyDetail> map)
        {
            var submissionTypes = db.CompanyNumberSubmissionType.OrderBy(st => st.CompanyNumberId)
                                                                .ThenBy(st => st.Order)
                                                                .Select(st => new
                                                                {
                                                                    Id = st.SubmissionTypeId,
                                                                    CompanyNumberId = st.CompanyNumberId,
                                                                    Name = st.SubmissionType.TypeName,
                                                                    IsDefault = st.DefaultSelection,
                                                                    Summary = (st.SummarySubmissionField == null) ? null : new SubmissionDisplayFieldViewModel()
                                                                    {
                                                                        PropertyName = st.SummarySubmissionField.SubmissionField.PropertyName,
                                                                        DisplayName = st.SummarySubmissionField.SubmissionField.DisplayLabel ?? st.SummarySubmissionField.SubmissionField.DisplayLabel,
                                                                        Header = st.SummarySubmissionField.SubmissionField.ColumnHeading,
                                                                        IsCustomization = (st.SummarySubmissionField.SubmissionField.CompanyNumberAttributeId != null || st.SummarySubmissionField.SubmissionField.CompanyNumberCodeTypeId != null)
                                                                    },
                                                                    Fields = st.CompanyNumberSubmissionFieldSubmissionType.Where(f => f.CompanyNumberSubmissionField.Enabled)
                                                                                                                          .Select(f => new
                                                                                                                            {
                                                                                                                                Id = f.CompanyNumberSubmissionField.SubmissionField.Id,
                                                                                                                                Label = f.CompanyNumberSubmissionField.DisplayLabel ?? f.CompanyNumberSubmissionField.SubmissionField.DisplayLabel,
                                                                                                                                Required = f.Required,
                                                                                                                                VisibleOnAdd = f.VisibleOnAdd,
                                                                                                                                VisibleOnBPM = f.VisibleOnBPM
                                                                                                                            }).ToList()
                                                                }).ToArray();

            foreach (var cn in map.Values)
            {
                foreach (var t in submissionTypes.Where(t => t.CompanyNumberId == cn.CompanyNumberId))
                {
                    var settings = new SubmissionTypeSettingsViewModel()
                    {
                        Id = t.Id,
                        TypeName = t.Name,
                        TypeIsDefault = t.IsDefault,
                        SummaryField = t.Summary
                    };

                    foreach (var f in t.Fields)
                    {
                        SubmissionTypeFieldViewModel field = null;
                        if (f.Id == Domains.SubmissionFields.AllowMultipleSubmissions)
                            field = settings.AllowMultipleSubmissions;
                        else if (f.Id == Domains.SubmissionFields.SequenceNumber)
                            field = settings.SequenceNumber;
                        else if (f.Id == Domains.SubmissionFields.SubmittedDate)
                            field = settings.SubmittedDate;
                        else if (f.Id == Domains.SubmissionFields.EffectiveDate)
                            field = settings.EffectiveDate;
                        else if (f.Id == Domains.SubmissionFields.ExpirationDate)
                            field = settings.ExpirationDate;
                        else if (f.Id == Domains.SubmissionFields.Agency)
                            field = settings.Agency;
                        else if (f.Id == Domains.SubmissionFields.Agent)
                            field = settings.Agent;
                        else if (f.Id == Domains.SubmissionFields.ClassCodes)
                            field = settings.ClassCodes;
                        else if (f.Id == Domains.SubmissionFields.UnderwritingUnit)
                            field = settings.UnderwritingUnit;
                        else if (f.Id == Domains.SubmissionFields.Underwriter)
                            field = settings.Underwriter;
                        else if (f.Id == Domains.SubmissionFields.UnderwritingAnalyst)
                            field = settings.UnderwritingAnalyst;
                        else if (f.Id == Domains.SubmissionFields.UnderwritingTechnician)
                            field = settings.UnderwritingTechnician;
                        else if (f.Id == Domains.SubmissionFields.PolicyNumber)
                            field = settings.PolicyNumber;
                        else if (f.Id == Domains.SubmissionFields.PolicyMod)
                            field = settings.PolicyMod;
                        else if (f.Id == Domains.SubmissionFields.PolicySymbol)
                            field = settings.PolicySymbol;
                        else if (f.Id == Domains.SubmissionFields.PackagePolicySymbols)
                            field = settings.PackagePolicySymbols;
                        else if (f.Id == Domains.SubmissionFields.PolicyPremium)
                            field = settings.PolicyPremium;
                        else if (f.Id == Domains.SubmissionFields.PolicySystem)
                            field = settings.PolicySystem;
                        else if (f.Id == Domains.SubmissionFields.Status)
                            field = settings.Status;
                        else if (f.Id == Domains.SubmissionFields.StatusNotes)
                            field = settings.StatusNotes;
                        else if (f.Id == Domains.SubmissionFields.PriorCarrier)
                            field = settings.PriorCarrier;
                        else if (f.Id == Domains.SubmissionFields.PriorCarrierPremium)
                            field = settings.PriorCarrierPremium;
                        else if (f.Id == Domains.SubmissionFields.AcquiringCarrier)
                            field = settings.AcquiringCarrier;
                        else if (f.Id == Domains.SubmissionFields.AcquiringCarrierPremium)
                            field = settings.AcquiringCarrierPremium;
                        else if (f.Id == Domains.SubmissionFields.Notes)
                            field = settings.Notes;

                        if(field != null)
                        {
                            field.Enabled = true;
                            field.Label = f.Label;
                            field.Required = f.Required;
                            field.VisibleOnAdd = f.VisibleOnAdd;
                            field.VisibleOnBPM = f.VisibleOnBPM;
                        }
                    }

                    cn.Configuration.SubmissionTypes.Add(settings);
                }
            }
        }
    }
}
