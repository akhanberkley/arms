﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using Int = BTS.WFA.Integration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using BTS.WFA.ViewModels;
using LinqKit;
using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.BCS.Services
{
    public class ClassCodeService
    {
        public static List<ClassCodeViewModel> Search(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.ClassCode.Where(cc => cc.CompanyNumberId == cd.CompanyNumberId);
                query = odataFilter(query) as IQueryable<Data.Models.ClassCode>;

                return query.AsExpandable().Select(DataToViewModel.ClassCode()).ToList();
            }
        }

        public static ClassCodeViewModel SaveClassCode(CompanyDetail cd, ClassCodeViewModel classCode, string codeValue)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                if (String.IsNullOrEmpty(classCode.CodeValue))
                    classCode.AddValidation("CodeValue", "Code is required");
                else if (db.ClassCode.Any(cc => cc.CompanyNumberId == cd.CompanyNumberId && cc.CodeValue == classCode.CodeValue && (String.IsNullOrEmpty(codeValue) || codeValue != classCode.CodeValue)))
                    classCode.AddValidation("CodeValue", "The code {0} already exists", classCode.CodeValue);

                if (String.IsNullOrEmpty(classCode.Description))
                    classCode.AddValidation("Description", "Description is required");

                if (classCode.ValidationResults.Count == 0)
                {
                    var dbClassCode = db.ClassCode.Include(cc => cc.ClassCodeCompanyNumberCode)
                                                  .Include(cc => cc.ClassCodeCompanyNumberCode.Select(nc => nc.CompanyNumberCode))
                                                  .Include(cc => cc.CompanyNumberClassCodeHazardGradeValue)
                                                  .Include(cc => cc.CompanyNumberClassCodeHazardGradeValue.Select(h => h.CompanyNumberHazardGradeType))
                                                  .Include(cc => cc.CompanyNumberClassCodeHazardGradeValue.Select(h => h.CompanyNumberHazardGradeType.HazardGradeType))
                                                  .Include(cc => cc.SicCodeList)
                                                  .Include(cc => cc.NaicsCodeList)
                                                  .FirstOrDefault(c => c.CompanyNumberId == cd.CompanyNumberId && c.CodeValue == codeValue);

                    if (dbClassCode == null && codeValue != null)
                        classCode.ItemNotFound = true;
                    else
                    {
                        if (dbClassCode == null)
                            dbClassCode = db.ClassCode.Add(new Data.Models.ClassCode() { CompanyNumberId = cd.CompanyNumberId });

                        dbClassCode.CodeValue = classCode.CodeValue;
                        dbClassCode.Description = classCode.Description;
                        dbClassCode.GLCodeValue = (String.IsNullOrEmpty(classCode.GLCodeValue)) ? null : classCode.GLCodeValue;
                        dbClassCode.NaicsCodeList = (classCode.NaicsCode == null) ? null : db.NaicsCodeList.FirstOrDefault(n => n.Code == classCode.NaicsCode.Code);
                        dbClassCode.SicCodeList = (classCode.SicCode == null) ? null : db.SicCodeList.FirstOrDefault(s => s.Code == classCode.SicCode.Code);

                        if (!classCode.CompanyCustomizations.Any(c => c.CodeValue != null))
                            dbClassCode.ClassCodeCompanyNumberCode.Clear();
                        else
                        {
                            var dbSelectedCodes = CompanyCustomizationService.GetMatchingCompanyCodes(db.CompanyNumberCode.Where(c => c.CompanyNumberId == cd.CompanyNumberId), classCode.CompanyCustomizations);
                            var dbExistingCodes = dbClassCode.ClassCodeCompanyNumberCode.Select(c => c.CompanyNumberCode).ToArray();
                            var codesToDelete = dbExistingCodes.Where(e => !dbSelectedCodes.Select(s => s.Id).Contains(e.Id)).Select(e => e.Id).ToArray();

                            foreach (var existingCode in dbClassCode.ClassCodeCompanyNumberCode.Where(c => codesToDelete.Contains(c.CompanyNumberCode.Id)).ToArray())
                                dbClassCode.ClassCodeCompanyNumberCode.Remove(existingCode);
                            foreach (var codeToAdd in dbSelectedCodes.Where(c => !dbExistingCodes.Any(e => e.Id == c.Id)))
                                dbClassCode.ClassCodeCompanyNumberCode.Add(new Data.Models.ClassCodeCompanyNumberCode()
                                {
                                    CompanyNumberCode = codeToAdd,
                                    CompanyNumberCodeType = codeToAdd.CompanyNumberCodeType,
                                    ClassCodeId = dbClassCode.Id
                                });
                        }

                        var hazardGradesWithValues = classCode.HazardGrades.Where(h => !String.IsNullOrEmpty(h.Description)).ToArray();
                        var validHazardGradeCodes = hazardGradesWithValues.Select(h => h.Code).ToArray();
                        foreach (var hazardGradeToRemove in dbClassCode.CompanyNumberClassCodeHazardGradeValue.Where(h => !validHazardGradeCodes.Contains(h.CompanyNumberHazardGradeType.HazardGradeType.TypeName)).ToArray())
                            dbClassCode.CompanyNumberClassCodeHazardGradeValue.Remove(hazardGradeToRemove);

                        var hazardGradeTypes = db.CompanyNumber.Where(c => c.Id == cd.CompanyNumberId).SelectMany(c => c.CompanyNumberHazardGradeType).Include(h => h.HazardGradeType).ToArray();
                        foreach (var h in hazardGradesWithValues)
                        {
                            var dbHazardGradeType = hazardGradeTypes.FirstOrDefault(gt => gt.HazardGradeType.TypeName == h.Code);
                            var dbHazardGrade = dbClassCode.CompanyNumberClassCodeHazardGradeValue.FirstOrDefault(hg => hg.HazardGradeTypeId == dbHazardGradeType.HazardGradeTypeId);
                            if (dbHazardGrade == null)
                            {
                                dbHazardGrade = new Data.Models.CompanyNumberClassCodeHazardGradeValue() { CompanyNumberHazardGradeType = dbHazardGradeType, ClassCode = dbClassCode };
                                dbClassCode.CompanyNumberClassCodeHazardGradeValue.Add(dbHazardGrade);
                            }

                            dbHazardGrade.HazardGradeValue = h.Description;
                        }

                        db.SaveChanges();

                        classCode = db.ClassCode.AsExpandable()
                                                .Where(cc => cc.Id == dbClassCode.Id)
                                                .Select(DataToViewModel.ClassCode())
                                                .First();
                        if (String.IsNullOrEmpty(codeValue))
                            classCode.ItemCreated = true;
                    }
                }
            }

            return classCode;
        }

        public static BaseViewModel DeleteClassCode(CompanyDetail cd, string code)
        {
            var results = new BaseViewModel();
            using (var db = Application.GetDatabaseInstance())
            {
                try
                {
                    var dbClassCode = db.ClassCode.Include(c => c.CompanyNumberClassCodeHazardGradeValue)
                                                  .Include(c => c.ClassCodeCompanyNumberCode)
                                                  .FirstOrDefault(cc => cc.CompanyNumberId == cd.CompanyNumberId && cc.CodeValue == code);

                    if (dbClassCode == null)
                        results.ItemNotFound = true;
                    else
                    {
                        if (db.Submission.Any(s => s.ClassCodes.Any(cc => cc.Id == dbClassCode.Id)))
                            results.AddValidation("General", "Submissions are tied to this class code");

                        if (results.ValidationResults.Count == 0)
                        {
                            foreach (var h in dbClassCode.CompanyNumberClassCodeHazardGradeValue.ToArray())
                                db.CompanyNumberClassCodeHazardGradeValue.Remove(h);
                            foreach (var c in dbClassCode.ClassCodeCompanyNumberCode.ToArray())
                                db.ClassCodeCompanyNumberCode.Remove(c);

                            db.ClassCode.Remove(dbClassCode);
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex is DbUpdateException)
                    {
                        results.AddValidation("General", "This class code could not be deleted, likely because it is associated with clients");
                        Application.HandleException(ex, new { Company = cd.Summary, ClassCode = code });
                    }
                    else
                        throw ex;
                }

            }

            return results;
        }
    }
}
