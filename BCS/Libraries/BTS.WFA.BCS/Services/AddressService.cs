﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using Int = BTS.WFA.Integration;

namespace BTS.WFA.BCS.Services
{
    public class AddressService
    {
        public static AddressLookupResultsViewModel Lookup(CompanyDetail cd, AddressLookupCriteriaViewModel criteria)
        {
            var results = new AddressLookupResultsViewModel() { Status = AddressLookupResultsType.Succeeded };
            if (!criteria.IsEmpty)
            {
                bool useAdvancedSearch = (!String.IsNullOrEmpty(criteria.Address1));
                Int.Models.GeoCodingSearchResult serviceResults = null;
                if (useAdvancedSearch)
                {
                    serviceResults = Int.Services.GeoCodingService.ComplexSearch(cd.SystemSettings.ToAdvancedClientSettings(), new Int.Models.GeoCodingAddress() { Addr1 = criteria.Address1, Addr2 = criteria.Address2, City = criteria.City, State = criteria.State, PostalCode = criteria.PostalCode, County = criteria.County });
                }

                if (serviceResults == null || serviceResults.Matches.Count == 0)
                {
                    serviceResults = Int.Services.GeoCodingService.SimpleSearch(ConfigurationManager.AppSettings["GISProxy.GISWS"], criteria.City, criteria.State, criteria.PostalCode);
                    useAdvancedSearch = false;
                }

                if (!serviceResults.Succeeded)
                {
                    results.Status = AddressLookupResultsType.Failed;
                    results.StatusMessage = serviceResults.Exception.Message;
                    Application.HandleException(new Exception("Address Lookup Failed", serviceResults.Exception), new { Company = cd.Summary, Criteria = criteria });
                }
                else
                {
                    var ti = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo;
                    foreach (var s in serviceResults.Matches)
                    {
                        var match = new AddressViewModel()
                            {
                                Address1 = s.Addr1,
                                Address2 = s.Addr2,
                                City = s.City,
                                State = s.State,
                                PostalCode = s.PostalCode,
                                County = (s.County == null) ? null : Regex.Replace(s.County, " county", "", RegexOptions.IgnoreCase),
                                Country = s.County,
                                Latitude = s.Latitude,
                                Longitude = s.Longitude
                            };
                        results.Matches.Add(match);

                        match.Address1 = match.Address1;

                        if (useAdvancedSearch)
                        {
                            if (!String.IsNullOrEmpty(match.Address1)) match.Address1 = CapitalizeAddressLine(match.Address1);
                            if (!String.IsNullOrEmpty(match.Address2)) match.Address2 = CapitalizeAddressLine(match.Address2);
                            if (!String.IsNullOrEmpty(match.City)) match.City = ti.ToTitleCase(match.City.ToLower());
                            if (!String.IsNullOrEmpty(match.County)) match.County = ti.ToTitleCase(match.County.ToLower());
                        }
                    }
                }
            }

            return results;
        }

        private static Regex lineSplitRegex = new Regex("(?<Numeric>^[0-9][^\\s]*)?(?<Alpha>.*)");
        public static Tuple<string, string> SplitBuildNumberAddressLine(string address1)
        {
            if (String.IsNullOrEmpty(address1))
                return new Tuple<string, string>(null, null);
            else
            {
                //Numeric:  If the beginning of the string starts with a number, grab it and everything up til the first space (i.e. 801, 801B, 801-C)
                var firstLine = lineSplitRegex.Match(address1);
                return new Tuple<string, string>(firstLine.Groups["Numeric"].Value.Trim(), firstLine.Groups["Alpha"].Value.Trim());
            }
        }

        public static string FormatAddress(string buildingNumber, string address1, string address2, string city, string state, string postalCode)
        {
            return ((String.IsNullOrEmpty(buildingNumber)) ? "" : buildingNumber + " ")
                 + address1 + " "
                 + ((String.IsNullOrEmpty(address2)) ? "" : address2 + " ")
                 + city + ", " + state + " " + postalCode;
        }

        private static HashSet<string> capitalizedWords = new HashSet<string>() { "NE", "SE", "SW", "NW", "PO" };
        private static string CapitalizeAddressLine(string address)
        {
            var words = address.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < words.Length; i++)
            {
                var w = words[i].ToUpper();
                if (capitalizedWords.Contains(w))
                    words[i] = w;
                else if (Char.IsNumber(w[0]))
                    words[i] = w.ToLower();
                else
                    words[i] = w[0] + ((w.Length == 1) ? "" : w.Substring(1).ToLower());
            }

            return String.Join(" ", words);
        }
    }
}
