﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.ViewModels;
using LinqKit;

namespace BTS.WFA.BCS.Mapping
{
    /// <summary>
    /// This class uses the referenced 'LinqKit' to make some magic happen with re-using these mappers inside other mappers
    /// It handles making sure entity framework can correctly go through the expression tree to generate an accurate select statement in those situations
    /// </summary>
    public class DataToViewModel
    {
        public static Expression<Func<Data.Models.User, UserViewModel>> User()
        {
            var agencyMapper = AgencySummary();
            return (i => new UserViewModel()
            {
                Id = i.Id,
                UserName = i.Username,
                DefaultCompany = (i.PrimaryCompany == null) ? null : i.PrimaryCompany.CompanyNumber.Select(cn => new CodeListItemViewModel() { Code = cn.Company.CompanyInitials, Description = i.PrimaryCompany.CompanyName }).FirstOrDefault(),
                Active = i.Active,
                Notes = i.Notes,
                UserType = i.UserType.Name,
                ClientCoreSegmentCode = i.Segmentation,
                Roles = i.Role.Select(r => r.RoleName).ToList(),
                Companies = i.Company.SelectMany(c => c.CompanyNumber.Select(cn => new CodeListItemViewModel() { Code = cn.Company.CompanyInitials, Description = c.CompanyName })).OrderBy(c => c.Description).ToList(),
                LimitingAgency = (i.Agency == null) ? null : agencyMapper.Invoke(i.Agency)
            });
        }
        public static Expression<Func<Data.Models.User, UserSummaryViewModel>> UserSummary()
        {
            return (i => new UserSummaryViewModel()
            {
                Id = i.Id,
                UserName = i.Username,
                Active = i.Active
            });
        }

        public static Expression<Func<Data.Models.LineOfBusiness, UnderwritingUnitViewModel>> UnderwritingUnit()
        {
            return (i => new UnderwritingUnitViewModel()
            {
                ApsIdAsLong = i.ApsId,
                Code = i.Code,
                Description = i.Description,
                Line = i.Line
            });
        }
        public static Expression<Func<Data.Models.Person, PersonnelViewModel>> Personnel()
        {
            return (i => new PersonnelViewModel()
            {
                ApsIdAsLong = i.ApsId,
                FullName = i.FullName,
                UserName = i.UserName,
                Initials = i.Initials,
                RetirementDate = i.RetirementDate,
            });
        }

        public static Expression<Func<Data.Models.OtherCarrier, CodeListItemViewModel>> Carrier()
        {
            return (i => new CodeListItemViewModel()
            {
                Code = i.Code,
                Description = i.Description
            });
        }

        public static Expression<Func<Data.Models.PolicySymbol, PolicySymbolSummaryViewModel>> PolicySymbolSummary()
        {
            var numberControlMapper = NumberControl();
            return (i => new PolicySymbolSummaryViewModel()
            {
                Code = i.Code,
                Description = i.Description
            });
        }
        public static Expression<Func<Data.Models.PolicySymbol, PolicySymbolViewModel>> PolicySymbol()
        {
            var numberControlMapper = NumberControl();
            return (i => new PolicySymbolViewModel()
            {
                Code = i.Code,
                Description = i.Description,
                RetirementDate = i.RetirementDate,
                IsPackageOption = i.PackageOption,
                IsPrimaryOption = i.PrimaryOption,
                NumberControl = (i.NumberControlId == null) ? null : numberControlMapper.Invoke(i.NumberControl),
                PolicyNumberPrefix = i.PolicyNumberPrefix
            });
        }

        public static Expression<Func<Data.Models.State, StateViewModel>> State()
        {
            return (i => new StateViewModel()
            {
                Abbreviation = i.Abbreviation,
                StateName = i.StateName
            });
        }

        public static Expression<Func<Data.Models.CompanyNumberCode, CompanyNumberCodeViewModel>> CompanyNumberCode()
        {
            return (i => new CompanyNumberCodeViewModel()
            {
                Code = i.Code,
                Description = i.Description,
                ExpirationDate = i.ExpirationDate
            });
        }

        public static Expression<Func<Data.Models.SicCodeList, SicCodeViewModel>> SicCode()
        {
            return (i => new SicCodeViewModel()
            {
                Code = i.Code,
                Description = i.CodeDescription,
                Group = i.SicGroup.GroupDescription,
                Division = i.SicGroup.SicDivision.DivisionDescription
            });
        }
        public static Expression<Func<Data.Models.NaicsCodeList, NaicsCodeViewModel>> NaicsCode()
        {
            return (i => new NaicsCodeViewModel()
            {
                Code = i.Code,
                Description = i.CodeDescription,
                Group = i.NaicsGroup.GroupDescription,
                Division = i.NaicsGroup.NaicsDivision.DivisionDescription
            });
        }
        public static Expression<Func<Data.Models.ClassCode, ClassCodeViewModel>> ClassCode()
        {
            var companyNumberCodeMapper = CompanyNumberCode();
            var sicMapper = SicCode();
            var naicsMapper = NaicsCode();
            return (i => new ClassCodeViewModel()
                        {
                            CodeValue = i.CodeValue,
                            GLCodeValue = i.GLCodeValue,
                            Description = i.Description,
                            HazardGrades = i.CompanyNumberClassCodeHazardGradeValue.OrderBy(h => h.CompanyNumberHazardGradeType.DisplayOrder)
                                                                                   .Select(h => new CodeListItemViewModel()
                                                                                    {
                                                                                        Code = h.CompanyNumberHazardGradeType.HazardGradeType.TypeName,
                                                                                        Description = h.HazardGradeValue
                                                                                    }).ToList(),
                            SicCode = (i.SicCodeList == null) ? null : sicMapper.Invoke(i.SicCodeList),
                            NaicsCode = (i.NaicsCodeList == null) ? null : naicsMapper.Invoke(i.NaicsCodeList),
                            CompanyCustomizations = i.ClassCodeCompanyNumberCode.Select(nc => new CompanyCustomizationValueViewModel()
                            {
                                Customization = nc.CompanyNumberCodeType.CodeName,
                                CodeValue = companyNumberCodeMapper.Invoke(nc.CompanyNumberCode)
                            }).ToList()
                        });
        }

        public static Expression<Func<Data.Models.NumberControl, NumberControlViewModel>> NumberControl()
        {
            return (i => new NumberControlViewModel()
            {
                Id = i.Id,
                Type = i.NumberControlType.TypeName,
                ControlNumber = i.ControlNumber,
                PaddingChar = i.PaddingChar,
                PadHead = i.PadHead,
                TotalLength = i.TotalLength
            });
        }

        public static Expression<Func<Data.Models.Agent, AgentViewModel>> Agent(bool includeAgency)
        {
            if (!includeAgency)
                return (i => new AgentViewModel()
                            {
                                ApsIdAsLong = i.ApsId,
                                BrokerNo = i.BrokerNo,
                                RetirementDate = i.CancelDate,
                                FirstName = i.FirstName,
                                MiddleName = i.Middle,
                                LastName = i.Surname,
                                PrimaryPhone = i.PrimaryPhoneAreaCode + i.PrimaryPhone,
                                SecondaryPhone = i.SecondaryPhoneAreaCode + i.SecondaryPhone,
                                Fax = i.FaxAreaCode + i.Fax,
                                EmailAddress = i.EmailAddress
                            });
            else
            {
                var agencyMapper = AgencySummary();
                return (i => new AgentViewModel()
                {
                    ApsIdAsLong = i.ApsId,
                    BrokerNo = i.BrokerNo,
                    RetirementDate = i.CancelDate,
                    FirstName = i.FirstName,
                    MiddleName = i.Middle,
                    LastName = i.Surname,
                    PrimaryPhone = i.PrimaryPhoneAreaCode + i.PrimaryPhone,
                    SecondaryPhone = i.SecondaryPhoneAreaCode + i.SecondaryPhone,
                    Fax = i.FaxAreaCode + i.Fax,
                    EmailAddress = i.EmailAddress,
                    Agency = agencyMapper.Invoke(i.Agency)
                });
            }
        }

        public static Expression<Func<Data.Models.Agency, AgencySummaryViewModel>> AgencySummary()
        {
            return (i => new AgencySummaryViewModel
                {
                    Id = i.Id,
                    Code = i.AgencyNumber,
                    Name = i.AgencyName,
                    Address = new AddressViewModel()
                    {
                        Address1 = i.Address,
                        Address2 = i.Address2,
                        City = i.City,
                        State = i.State,
                        PostalCode = i.Zip
                    },
                    EffectiveDate = i.EffectiveDate,
                    CancelDate = i.CancelDate,
                    RenewalCancelDate = i.RenewalCancelDate,
                    ReferralDate = i.ReferralDate,
                    ReferralAgencyCode = i.ReferralAgencyNumber
                });
        }
        public static Expression<Func<Data.Models.Agency, AgencyViewModel>> Agency()
        {
            var agencyMapper = AgencySummary();
            var stateMapper = State();

            return (i => new AgencyViewModel
            {
                Id = i.Id,
                ApsIdAsLong = i.ApsId,
                Name = i.AgencyName,
                Code = i.AgencyNumber,
                Parent = (i.MasterAgency == null) ? null : agencyMapper.Invoke(i.MasterAgency),
                Children = i.Children.OrderBy(a => a.AgencyName).ThenBy(a => a.AgencyNumber).Select(a => agencyMapper.Invoke(a)).ToList(),
                Address = new AddressViewModel()
                {
                    Address1 = i.Address,
                    Address2 = i.Address2,
                    City = i.City,
                    State = i.State,
                    PostalCode = i.Zip
                },
                Branches = i.AgencyBranch.Select(b => new AgencyBranchViewModel()
                {
                    ApsIdAsLong = b.ApsId,
                    Code = b.Code,
                    Name = b.Name
                }).ToList(),
                Contact = new ContactViewModel()
                {
                    Name = i.ContactPerson,
                    Phone = i.ContactPersonPhone,
                    PhoneExtension = i.ContactPersonExtension,
                    Fax = i.ContactPersonFax,
                    Email = i.ContactPersonEmail
                },
                Note = i.Comments,
                EffectiveDate = i.EffectiveDate,
                CancelDate = i.CancelDate,
                RenewalCancelDate = i.RenewalCancelDate,
                ReferralDate = i.ReferralDate,
                ReferralAgencyCode = i.ReferralAgencyNumber
            });
        }

        public static Expression<Func<Data.Models.AgencyLineOfBusiness, AgencyUnderwritingUnitViewModel>> UnderwritingUnitFromAgency(CompanySystemSettings settings)
        {
            var personnelMapper = AgencyPersonnelMapper();

            return (i => new AgencyUnderwritingUnitViewModel()
            {
                ApsIdAsLong = i.LineOfBusiness.ApsId,
                Code = i.LineOfBusiness.Code,
                Description = i.LineOfBusiness.Description,
                Line = i.LineOfBusiness.Line,
                Underwriters = i.AgencyLineOfBusinessPersonUnderwritingRolePerson.Where(a => settings.UnderwritingRoles.Contains(a.UnderwritingRole.RoleCode))
                                                             .OrderBy(a => a.Person.FullName)
                                                             .Select(a => personnelMapper.Invoke(a))
                                                             .ToList(),
                Analysts = i.AgencyLineOfBusinessPersonUnderwritingRolePerson.Where(a => settings.UnderwritingAnalystRoles.Contains(a.UnderwritingRole.RoleCode))
                                                         .OrderBy(a => a.Person.FullName)
                                                         .Select(a => personnelMapper.Invoke(a))
                                                         .ToList(),
                Technicians = i.AgencyLineOfBusinessPersonUnderwritingRolePerson.Where(a => settings.UnderwritingTechnicianRoles.Contains(a.UnderwritingRole.RoleCode))
                                                            .OrderBy(a => a.Person.FullName)
                                                            .Select(a => personnelMapper.Invoke(a))
                                                            .ToList()
            });
        }
        public static Expression<Func<Data.Models.AgencyLineOfBusinessPersonUnderwritingRolePerson, AgencyPersonnelViewModel>> AgencyPersonnelMapper()
        {
            return (i => new AgencyPersonnelViewModel()
            {
                ApsIdAsLong = i.Person.ApsId,
                FullName = i.Person.FullName,
                UserName = i.Person.UserName,
                Initials = i.Person.Initials,
                RetirementDate = i.Person.RetirementDate,
                IsPrimary = i.Primary
            });
        }

        public static Expression<Func<Data.Models.SubmissionAdditionalName, SubmissionAdditionalNameViewModel>> SubmissionAdditionalName()
        {
            return (i => new SubmissionAdditionalNameViewModel
            {
                Id = i.Id,
                SequenceNumber = i.ClientCoreSequenceNumber,
                BusinessName = i.BusinessName
            });
        }
        public static Expression<Func<Data.Models.SubmissionAddress, SubmissionAddressViewModel>> SubmissionAddress()
        {
            return (i => new SubmissionAddressViewModel
            {
                Id = i.Id,
                ClientCoreAddressId = i.ClientCoreAddressId,
                Address1 = i.Address1,
                Address2 = i.Address2,
                City = i.City,
                State = i.State,
                County = i.County,
                PostalCode = i.PostalCode,
                Longitude = i.Longitude,
                Latitude = i.Latitude,
                Country = i.Country.CountryCode
            });
        }
        public static Expression<Func<Data.Models.Submission, SubmissionSummaryViewModel>> SubmissionSummary()
        {
            return (i => new SubmissionSummaryViewModel
            {
                SubmissionNumber = i.SubmissionNumber,
                SequenceNumber = i.Sequence,
                PolicyNumber = i.PolicyNumber,
                SubmittedDate = i.SubmissionDt,
                EffectiveDate = i.EffectiveDate,
                ExpirationDate = i.ExpirationDate,
                SubmissionType = i.SubmissionType.TypeName,
                ClientCoreId = i.ClientCoreClientId,
                ClientCorePortfolioId = i.ClientCorePortfolioId,
                InsuredName = i.InsuredName,
                InsuredDBA = i.Dba,
                ////helper variables
                SubmissionDbId = i.Id,
                DuplicateOfSubmissionDbId = i.DuplicateOfSubmissionId,
                AssociationSubmissionDbId = i.AssociationSubmissionId,
                CompanyNumber = i.CompanyNumber.CompanyNumberValue
            });
        }
        public static Expression<Func<Data.Models.Submission, SubmissionViewModel>> Submission()
        {
            var policySymbolMapper = PolicySymbol();
            return (i => new SubmissionViewModel
            {
                SubmissionNumber = i.SubmissionNumber,
                SequenceNumber = i.Sequence,
                IsDeleted = i.Deleted,
                DeletedBy = i.DeletedBy,
                DeletedDate = i.DeletedDt,
                PolicyNumber = i.PolicyNumber,
                Status = i.SubmissionStatus.StatusCode,
                StatusReason = i.SubmissionStatusReason.ReasonCode,
                StatusNotes = i.SubmissionStatusReasonOther,
                StatusDate = i.SubmissionStatusDate,
                SubmittedDate = i.SubmissionDt,
                EffectiveDate = i.EffectiveDate,
                ExpirationDate = i.ExpirationDate,
                SubmissionType = i.SubmissionType.TypeName,
                ClientCoreId = i.ClientCoreClientId,
                ClientCorePortfolioId = i.ClientCorePortfolioId,
                InsuredName = i.InsuredName,
                InsuredDBA = i.Dba,
                PolicyPremium = i.EstimatedWrittenPremium,
                PriorCarrierPremium = i.PriorCarrierPremium,
                AcquiringCarrierPremium = i.OtherCarrierPremium,
                CreatedWithOFACHit = i.CreatedWithOFACHit,
                ////helper variables
                SubmissionDbId = i.Id,
                DuplicateOfSubmissionDbId = i.DuplicateOfSubmissionId,
                AssociationSubmissionDbId = i.AssociationSubmissionId,
                PolicySymbolId = i.PolicySymbolId,
                AcquiringCarrierId = i.OtherCarrierId,
                PriorCarrierId = i.PriorCarrierId,
                PolicySystemId = i.PolicySystemId,
                CompanyNumber = i.CompanyNumber.CompanyNumberValue
            });
        }

        public static Expression<Func<Data.Models.SubmissionComment, SubmissionNoteViewModel>> SubmissionNote()
        {
            return (i => new SubmissionNoteViewModel()
            {
                Id = i.Id,
                Author = i.EntryBy,
                Date = i.EntryDt,
                Text = i.Comment
            });
        }
        public static Expression<Func<Data.Models.SubmissionStatusHistory, SubmissionStatusHistoryViewModel>> SubmissionStatusHistory(DateTime statusHistoryDefaultDate)
        {
            return (i => new SubmissionStatusHistoryViewModel()
            {
                Id = i.Id,
                Status = i.PreviousSubmissionStatus.StatusCode,
                StatusReason = i.PreviousSubmissionStatusReason.ReasonCode,
                StatusNote = i.PreviousSubmissionStatusReasonOther,
                Date = i.SubmissionStatusDate ?? statusHistoryDefaultDate,
                ChangeDate = i.StatusChangeDt,
                ChangeUser = i.StatusChangeBy,
                IsActive = i.Active
            });
        }
    }
}
