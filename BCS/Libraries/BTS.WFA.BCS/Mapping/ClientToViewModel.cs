﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using BTS.WFA.BCS.ViewModels;
using Int = BTS.WFA.Integration;
using LinqKit;

namespace BTS.WFA.BCS.Mapping
{
    public class ClientToViewModel
    {
        public static List<T> FromClientSystem<T>(CompanyDetail cd, IEnumerable<Int.Models.Client> sysClients) where T : ClientSummaryViewModel, new()
        {
            var results = new List<T>();
            foreach (var sysClient in sysClients)
            {
                var vmClient = new T()
                {
                    ClientCoreId = sysClient.Id,
                    CmsId = sysClient.CmsId,
                    CompanyNumber = cd.Summary.CompanyNumber,
                    FEIN = sysClient.TaxId
                };

                vmClient.Portfolio.Id = sysClient.PortfolioId;
                vmClient.Portfolio.ClientCount = sysClient.PortfolioClientCount;
                vmClient.Portfolio.Name = sysClient.PorftolioName;

                var segment = cd.Configuration.ClientCoreSegments.FirstOrDefault(s => s.Code == sysClient.Segment);
                if (segment != null)
                    vmClient.ClientCoreSegment = new WFA.ViewModels.CodeListItemViewModel() { Code = segment.Code, Description = segment.Description };

                if (sysClient.Names.Count > 0)
                {
                    var primaryName = sysClient.Names.FirstOrDefault(n => n.IsPrimary) ?? sysClient.Names.First();
                    vmClient.PrimaryName = new ClientNameViewModel() { NameType = "Insured", SequenceNumber = primaryName.SequenceNumber, CmsId = primaryName.CmsId, BusinessName = primaryName.BusinessName };
                    foreach (var n in sysClient.Names.Where(n => n != primaryName))
                        vmClient.AdditionalNames.Add(new ClientNameViewModel() { NameType = n.NameType ?? "DBA", SequenceNumber = n.SequenceNumber, CmsId = n.CmsId, BusinessName = n.BusinessName, });
                }

                sysClient.Addresses.ForEach(a => vmClient.Addresses.Add(new ClientAddressViewModel()
                {
                    ClientCoreAddressId = a.Id,
                    CmsId = a.CmsId,
                    IsPrimary = a.IsPrimary,
                    Address1 = ((String.IsNullOrEmpty(a.HouseNumber)) ? "" : a.HouseNumber + " ") + a.Address1,
                    Address2 = a.Address2,
                    City = a.City,
                    State = a.State,
                    County = a.County,
                    PostalCode = a.PostalCode,
                    Country = a.Country
                }));

                results.Add(vmClient);
            }

            return results;
        }

        public static Expression<Func<Data.Models.Client, ClientClearanceData>> FromDbSimple()
        {
            return (i => new ClientClearanceData()
            {
                ClientCoreClientId = i.ClientCoreClientId,
                PhoneNumber = i.PhoneNumber,
                Names = i.ClientCoreNameClientNameType.Select(n => new ClientClearanceNameData { ClientCoreNameId = n.ClientCoreNameId, NameType = n.ClientNameType.NameType }).ToList(),
                Addresses = i.ClientCoreAddressAddressType.Select(a => new ClientClearanceAddressData { ClientCoreAddressId = a.ClientCoreAddressId, AddressType = a.AddressType.AddressTypeValue, Latitude = a.Latitude, Longitude = a.Longitude }).ToList()
            });

        }
        public static Expression<Func<Data.Models.Client, ClientClearanceData>> FromDbAdvanced()
        {
            var sicMapper = DataToViewModel.SicCode();
            var naicsMapper = DataToViewModel.NaicsCode();
            var codeMapper = DataToViewModel.CompanyNumberCode();
            return (i => new ClientClearanceData()
            {
                ClientCoreClientId = i.ClientCoreClientId,
                PhoneNumber = i.PhoneNumber,
                BusinessDescription = i.BusinessDescription,
                ExperianId = i.ExperianId,
                Names = i.ClientCoreNameClientNameType.Select(n => new ClientClearanceNameData { ClientCoreNameId = n.ClientCoreNameId, NameType = n.ClientNameType.NameType }).ToList(),
                Addresses = i.ClientCoreAddressAddressType.Select(a => new ClientClearanceAddressData { ClientCoreAddressId = a.ClientCoreAddressId, AddressType = a.AddressType.AddressTypeValue, Latitude = a.Latitude, Longitude = a.Longitude }).ToList(),
                SicCode = (i.SicCodeList == null) ? null : sicMapper.Invoke(i.SicCodeList),
                NaicsCode = (i.NaicsCodeList == null) ? null : naicsMapper.Invoke(i.NaicsCodeList),
                CompanyNumberCode = i.CompanyNumberCode.Select(cc => new CompanyCustomizationCodeValueViewModel()
                {
                    Customization = cc.CompanyNumberCodeType.CodeName,
                    CodeValue = codeMapper.Invoke(cc)
                }).ToList(),
                ClientCompanyNumberAttributeValue = i.ClientCompanyNumberAttributeValue.Select(a => new CompanyCustomizationTextValueViewModel()
                {
                    Customization = a.CompanyNumberAttribute.Label,
                    TextValue = a.AttributeValue
                }).ToList()
            });

        }

        public static void PopulateSimpleData(ClientSummaryViewModel vmClient, ClientClearanceData dbClient)
        {
            if (!String.IsNullOrEmpty(dbClient.PhoneNumber))
                vmClient.Contacts.Add(new ClientContactViewModel() { ContactType = Domains.ContactType.Phone, Value = dbClient.PhoneNumber });

            foreach (var bcsName in dbClient.Names)
            {
                var name = vmClient.AdditionalNames.FirstOrDefault(n => n.SequenceNumber == bcsName.ClientCoreNameId);
                if (name != null)
                    name.NameType = bcsName.NameType;
            }
            vmClient.AdditionalNames = vmClient.AdditionalNames.OrderBy(n => n.NameType).ThenBy(n => n.SequenceNumber).ToList();

            foreach (var bcsAddr in dbClient.Addresses)
            {
                var addr = vmClient.Addresses.FirstOrDefault(n => n.ClientCoreAddressId == bcsAddr.ClientCoreAddressId);
                if (addr != null)
                {
                    addr.Latitude = bcsAddr.Latitude;
                    addr.Longitude = bcsAddr.Longitude;
                    addr.AddressTypes.Add(bcsAddr.AddressType);
                }
            }
        }
    }
}
