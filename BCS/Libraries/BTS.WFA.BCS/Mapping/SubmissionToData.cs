﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using BTS.WFA.BCS.Data;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.ViewModels;
using LinqKit;

namespace BTS.WFA.BCS.Mapping
{
    public class SubmissionToData
    {
        private IClearanceDb originalDb;
        private CompanyDetail cd;
        private Data.Models.Submission dbSubmission;
        private List<ValidationResult> validations;
        private SubmissionTypeSettingsViewModel type;
        private bool isNew;
        private Dictionary<string, int> countryCodeToId = new Dictionary<string, int>();

        private List<int> lineOfBusinessIdsToAdd = new List<int>();
        private List<int> agentUnspecifiedReasonIdsToAdd = new List<int>();
        private List<int> classCodeIdsToAdd = new List<int>();
        private List<Data.Models.SubmissionComment> commentsToAdd = new List<Data.Models.SubmissionComment>();//adding these at the end since w/ threading they don't always seem to get added

        public SubmissionToData(CompanyDetail cd, IClearanceDb originalDb, Data.Models.Submission dbSubmission, List<ValidationResult> validations, SubmissionTypeSettingsViewModel type, bool isNew)
        {
            this.cd = cd;
            this.originalDb = originalDb;
            this.dbSubmission = dbSubmission;
            this.validations = validations;
            this.type = type;
            this.isNew = isNew;

            countryCodeToId = originalDb.Country.ToDictionary(c => c.CountryCode, c => c.Id);
        }

        public bool IsEnabled(SubmissionTypeFieldViewModel f)
        {
            //No longer checking if the field is hidden on add - other services are looking to populate these fields on creation
            //return (f.Enabled && (!isNew || f.VisibleOnAdd));
            return f.Enabled;
        }

        #region DB Not Required
        public void SetInsuredDetails(long clientCoreId, long? clientCorePortfolioId, string insuredName, string dba)
        {
            dbSubmission.ClientCoreClientId = clientCoreId;
            dbSubmission.ClientCorePortfolioId = clientCorePortfolioId;
            dbSubmission.InsuredName = insuredName;
            dbSubmission.Dba = dba;
        }

        public void SetBasicDetails(SubmissionTypeSettingsViewModel type, string submissionType, DateTime? effectiveDate, DateTime? expirationDate, string policyNumber, short? policyMod, decimal? policyPremium)
        {
            if (dbSubmission.SubmissionType == null || dbSubmission.SubmissionType.TypeName != submissionType)
                dbSubmission.SubmissionTypeId = type.Id;

            dbSubmission.EffectiveDate = (IsEnabled(type.EffectiveDate) && effectiveDate.HasValue) ? effectiveDate.Value.Date : (DateTime?)null;
            dbSubmission.ExpirationDate = (IsEnabled(type.ExpirationDate) && expirationDate.HasValue) ? expirationDate.Value.Date : (DateTime?)null;
            dbSubmission.PolicyNumber = (IsEnabled(type.PolicyNumber)) ? policyNumber : null;
            dbSubmission.PolicyMod = (IsEnabled(type.PolicyMod)) ? policyMod : null;
            dbSubmission.EstimatedWrittenPremium = IsEnabled(type.PolicyPremium) ? policyPremium : null;
        }

        public void SetNotes(List<SubmissionNoteViewModel> notes, string author)
        {
            if (IsEnabled(type.Notes))
            {
                var validNoteIds = notes.Select(n => n.Id);
                foreach (var comment in dbSubmission.SubmissionComment.Where(c => !validNoteIds.Contains(c.Id)).ToArray())
                    originalDb.SubmissionComment.Remove(comment);

                foreach (var c in notes.Where(c => c.Id == null && !String.IsNullOrEmpty(c.Text)))
                    commentsToAdd.Add(new Data.Models.SubmissionComment() { Comment = c.Text, EntryBy = !String.IsNullOrEmpty(c.Author) ? c.Author : author });
            }
        }
        #endregion

        #region Non-Threadable Database Access
        public void SetSubmissionAssociation(IEnumerable<SubmissionSummaryViewModel> associatedSubmissions)
        {
            //First, get the other submissions linked to this submission
            Data.Models.Submission[] dbAssociatedSubmissions = new Data.Models.Submission[0];
            if (associatedSubmissions.Any())
            {
                var associatedSubmissionsPredicate = PredicateBuilder.False<Data.Models.Submission>();
                foreach (var assn in associatedSubmissions)
                    associatedSubmissionsPredicate = associatedSubmissionsPredicate.Or(s => s.CompanyNumberId == cd.CompanyNumberId && s.SubmissionNumber == assn.SubmissionNumber && s.Sequence == assn.SequenceNumber);
                dbAssociatedSubmissions = originalDb.Submission.AsExpandable().Where(associatedSubmissionsPredicate).ToArray();
            }
            var dbAssociatedSubmissionIds = dbAssociatedSubmissions.Select(s => s.Id).ToArray();

            //If this submission was in an existing group, move the submissions that are no longer in this group into a new group
            if (dbSubmission.AssociationSubmissionId.HasValue)
            {
                var kickedOutSubmissions = originalDb.Submission.Where(s => s.Id != dbSubmission.Id && s.AssociationSubmissionId == dbSubmission.AssociationSubmissionId && !dbAssociatedSubmissionIds.Contains(s.Id)).ToArray();
                foreach (var s in kickedOutSubmissions)
                    s.AssociationSubmissionId = kickedOutSubmissions[0].Id;
            }

            //If this submission is an army of 1 now, the association Id should be itself
            if (dbAssociatedSubmissions.Length == 0)
                dbSubmission.AssociationSubmissionId = (isNew) ? null : (int?)dbSubmission.Id;
            else
            {
                //Determine what Id is used the most and use that going forward
                dbSubmission.AssociationSubmissionId = dbAssociatedSubmissions.GroupBy(s => s.AssociationSubmissionId).OrderByDescending(g => g.Count()).ThenBy(g => g.Key.HasValue).Select(g => g.Key).FirstOrDefault();

                //Finally, make sure all of the other submissions in this group are properly linked together
                foreach (var associatedSubmission in dbAssociatedSubmissions.Where(s => s.AssociationSubmissionId != dbSubmission.AssociationSubmissionId))
                {
                    var previousGroup = originalDb.Submission.Where(s => s.Id != associatedSubmission.Id && !dbAssociatedSubmissionIds.Contains(s.Id) && s.AssociationSubmissionId == associatedSubmission.AssociationSubmissionId).ToArray();
                    if (previousGroup.Any())
                        foreach (var s in previousGroup)
                            s.AssociationSubmissionId = previousGroup[0].Id;

                    associatedSubmission.AssociationSubmissionId = dbSubmission.AssociationSubmissionId;
                }
            }
        }

        public void SetCustomizations(List<CompanyCustomizationValueViewModel> customizations)
        {
            //CompanyCodes
            Services.CompanyCustomizationService.SetDatabaseCompanyCodes(originalDb, cd, customizations, dbSubmission.CompanyNumberCode);

            //Attributes
            List<Data.Models.SubmissionCompanyNumberAttributeValue> validValues = new List<Data.Models.SubmissionCompanyNumberAttributeValue>();
            var dbAttributes = originalDb.CompanyNumberAttribute.Where(a => a.CompanyNumberId == cd.CompanyNumberId).ToArray();
            foreach (var c in customizations.Where(c => c.CodeValue == null))
            {
                var dbAttr = dbAttributes.FirstOrDefault(a => a.Label == c.Customization);
                if (dbAttr != null)
                {
                    var dbSubmissionAttr = dbSubmission.SubmissionCompanyNumberAttributeValue.FirstOrDefault(a => a.CompanyNumberAttributeId == dbAttr.Id);
                    if (dbSubmissionAttr == null)
                    {
                        dbSubmissionAttr = originalDb.SubmissionCompanyNumberAttributeValue.Add(new Data.Models.SubmissionCompanyNumberAttributeValue());
                        dbSubmissionAttr.Submission = dbSubmission;
                        dbSubmissionAttr.CompanyNumberAttributeId = dbAttr.Id;
                    }

                    dbSubmissionAttr.AttributeValue = c.TextValue;
                    validValues.Add(dbSubmissionAttr);
                }
            }

            foreach (var dbSubmissionAttr in dbSubmission.SubmissionCompanyNumberAttributeValue.Where(a => !validValues.Contains(a)).ToArray())
                dbSubmission.SubmissionCompanyNumberAttributeValue.Remove(dbSubmissionAttr);
        }

        public void SetDuplicate(SubmissionSummaryViewModel duplicate)
        {
            if (duplicate == null)
                dbSubmission.DuplicateOfSubmissionId = null;
            else
            {
                var dupCN = cd;
                if (duplicate.Company != null && !String.IsNullOrEmpty(duplicate.Company.Abbreviation) && !String.Equals(duplicate.Company.Abbreviation, dupCN.Summary.Abbreviation, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (cd.SystemSettings.CrossClearanceCompanyNumbers.Any(c => c.Abbreviation == duplicate.Company.Abbreviation))
                        dupCN = Application.CompanyMap[duplicate.Company.Abbreviation];
                    else
                        validations.Add(new ValidationResult("DuplicateSubmission", "You do not have access to company {0}", duplicate.Company.Abbreviation));
                }
                var originalSubmission = originalDb.Submission.Where(s => s.CompanyNumberId == dupCN.CompanyNumberId && s.SubmissionNumber == duplicate.SubmissionNumber && s.Sequence == duplicate.SequenceNumber)
                                                              .Select(Mapping.DataToViewModel.SubmissionSummary())
                                                              .FirstOrDefault();
                if (originalSubmission == null)
                    validations.Add(new ValidationResult("DuplicateSubmission", "Could not find duplicate submission {0} ({1}) in BCS Database", duplicate.SubmissionNumber, dupCN.Summary.Abbreviation));
                else
                {
                    bool dupIdChanged = (dbSubmission.DuplicateOfSubmissionId != originalSubmission.SubmissionDbId);
                    dbSubmission.DuplicateOfSubmissionId = originalSubmission.SubmissionDbId;

                    if (dbSubmission.Id == originalSubmission.SubmissionDbId)
                        validations.Add(new ValidationResult("DuplicateSubmission", "Duplicate submission can not be the same as this submission", duplicate.SubmissionNumber));
                    else if (dupIdChanged)
                    {
                        //If this submission currently has duplicates pointed to it, now that it's a duplicate as well, they all need to point to the same submission
                        var dbSubmissionsToUpdate = originalDb.Submission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.DuplicateOfSubmissionId == dbSubmission.Id);
                        foreach (var s in dbSubmissionsToUpdate)
                            s.DuplicateOfSubmissionId = originalSubmission.SubmissionDbId;

                        if (cd.SystemSettings.UsesDuplicateSubmissionNote)
                        {
                            var duplicates = new List<string>() { (String.IsNullOrEmpty(originalSubmission.PolicyNumber)) ? "Submission #: " + originalSubmission.SubmissionNumber : "Policy #: " + originalSubmission.PolicyNumber };
                            foreach (var otherDup in originalDb.Submission.Where(s => s.DuplicateOfSubmissionId == originalSubmission.SubmissionDbId).Select(s => new { SubmissionNumber = s.SubmissionNumber, PolicyNumber = s.PolicyNumber }))
                                duplicates.Add((String.IsNullOrEmpty(otherDup.PolicyNumber)) ? "Submission #: " + otherDup.SubmissionNumber : "Policy #: " + otherDup.PolicyNumber);

                            var note = String.Format("Possible Duplicate: {0}", String.Join("; ", duplicates));
                            if (!dbSubmission.SubmissionComment.Any(c => c.Comment == note))
                                commentsToAdd.Add(new Data.Models.SubmissionComment() { Comment = note, EntryBy = "Duplicate Check" });
                        }
                    }
                }
            }
        }

        public void SetPackagePolicySymbols(List<PolicySymbolSummaryViewModel> packagePolicySymbols)
        {
            if (IsEnabled(type.PackagePolicySymbols))
            {
                //Determine which are new and remove any that are no longer in the collection
                var policySymbolsToAdd = packagePolicySymbols.ToList();
                foreach (var dbPolicySymbol in dbSubmission.PackagePolicySymbols.ToArray())
                {
                    var dbPolicySymbolAsQueryable = (new[] { dbPolicySymbol }).AsQueryable();
                    var matchingSymbol = packagePolicySymbols.FirstOrDefault(ps => ps.ToQuery(dbPolicySymbolAsQueryable).Any());

                    if (matchingSymbol != null)
                        policySymbolsToAdd.Remove(matchingSymbol);
                    else
                        dbSubmission.PackagePolicySymbols.Remove(dbPolicySymbol);
                }

                var dbPackageSymbols = originalDb.PolicySymbol.Where(ps => ps.CompanyNumberId == cd.CompanyNumberId && ps.PackageOption).ToArray().AsQueryable();
                foreach (var vmPolicySymbol in policySymbolsToAdd)
                {
                    var policySymbol = HandleMatchingElements(vmPolicySymbol.ToQuery(dbPackageSymbols.Where(p => p.CompanyNumberId == cd.CompanyNumberId)), "PackagePolicySymbols", type.PackagePolicySymbols.Label, (matches) => matches.Select(c => c.Description));
                    if (policySymbol != null)
                        dbSubmission.PackagePolicySymbols.Add(policySymbol);
                }
            }
        }
        #endregion

        #region Threadable Database Access
        public void SetAgencyAndUnderwriting(string updateUser, AgencySummaryViewModel agency, IEnumerable<string> submissionStates, AgentViewModel agent, string agentUnspecifiedReason,
                                             UnderwritingUnitViewModel underwritingUnit, PersonnelViewModel underwriter, PersonnelViewModel underwritingAnalyst, PersonnelViewModel underwritingTechnician, List<int> limitingAgencyIds, bool ignoreValidations)
        {
            Data.Models.Agency dbAgency = null;
            using (var db = Application.GetDatabaseInstance())
            {
                //Determine Agency
                if (!IsEnabled(type.Agency) || agency == null)
                {
                    dbSubmission.AgencyId = null;
                    SetUnderwritingUnit(underwritingUnit, db.CompanyNumber.Where(c => c.Id == cd.CompanyNumberId).SelectMany(c => c.LineOfBusiness));
                    dbSubmission.UnderwriterPersonId = GetPersonId("Underwriter", type.Underwriter, underwriter, db.Person.Where(p => p.CompanyNumberId == cd.CompanyNumberId));
                }
                else
                {
                    var agencies = agency.ToQuery(db.Agency.AsNoTracking()
                                                           .Include(a => a.AgencyLineOfBusiness)
                                                           .Include(a => a.AgencyLineOfBusiness.Select(l => l.LineOfBusiness))
                                                           .Include(a => a.Agent)
                                                           .Include(a => a.AgencyLicensedState)
                                                           .Include(a => a.AgencyLicensedState.Select(s => s.State))
                                                           .Where(a => a.CompanyNumberId == cd.CompanyNumberId),
                                                  limitingAgencyIds);

                    dbAgency = HandleMatchingElements(agencies, "Agency", type.Agency.Label, (matches) => matches.Select(c => c.AgencyNumber));
                    if (dbAgency != null)
                    {
                        if (cd.Configuration.SubmissionEnforceAgencyLicensedState && dbSubmission.AgencyId != dbAgency.Id)
                        {
                            var agencyStates = dbAgency.AgencyLicensedState.Select(s => s.State.Abbreviation).OrderBy(s => s);
                            var comment = String.Format("Review state entered. Only these states: ({0}) are authorized for agency {1}", String.Join(",", agencyStates), String.Format("{0} - {1}", dbAgency.AgencyNumber, dbAgency.AgencyName));

                            //Only add the comment if applicable AND it hasn't already been added (due to the submission being copied)
                            if (submissionStates.Any(s => !agencyStates.Contains(s)) && !(isNew && dbSubmission.SubmissionComment.Any(n => n.Comment == comment)))
                                commentsToAdd.Add(new Data.Models.SubmissionComment() { Comment = comment, EntryBy = updateUser });
                        }

                        dbSubmission.AgencyId = dbAgency.Id;

                        var dbUnderwritingUnit = SetUnderwritingUnit(underwritingUnit, dbAgency.AgencyLineOfBusiness.Select(alob => alob.LineOfBusiness));
                        var dbAgencyLob = (dbUnderwritingUnit == null) ? null : dbAgency.AgencyLineOfBusiness.FirstOrDefault(alob => alob.LineOfBusinessId == dbUnderwritingUnit.Id);
                        if (dbAgencyLob == null)
                            validations.Add(new ValidationResult("UnderwritingUnit", "Could not find {2} {1} or it is not associated with Agency {0}", dbAgency.AgencyNumber, (underwritingUnit != null) ? (underwritingUnit.Code ?? underwritingUnit.Description) : "?", type.UnderwritingUnit.Label));
                        else
                        {
                            if (ignoreValidations || cd.Configuration.UsesAllUnderwritersForAllAssignments)
                                dbSubmission.UnderwriterPersonId = GetPersonId("Underwriter", type.Underwriter, underwriter, db.Person.Where(p => p.CompanyNumberId == cd.CompanyNumberId));
                            else
                                dbSubmission.UnderwriterPersonId = GetPersonId("Underwriter", type.Underwriter, underwriter, dbAgencyLob.AgencyLineOfBusinessPersonUnderwritingRolePerson.Where(p => cd.SystemSettings.UnderwritingRoles.Contains(p.UnderwritingRole.RoleCode)).Select(p => p.Person).AsQueryable());
                        }
                    }
                }

                SetAgent(db, dbAgency, agent, agentUnspecifiedReason);
                dbSubmission.AnalystPersonId = GetPersonId("UnderwritingAnalyst", type.UnderwritingAnalyst, underwritingAnalyst, db.Person.Where(p => p.CompanyNumberId == cd.CompanyNumberId));
                dbSubmission.TechnicianPersonId = GetPersonId("UnderwritingTechnician", type.UnderwritingTechnician, underwritingTechnician, db.Person.Where(p => p.CompanyNumberId == cd.CompanyNumberId));
            }
        }
        private void SetAgent(IClearanceDb db, Data.Models.Agency dbAgency, AgentViewModel agent, string unspecifiedReason)
        {
            if (dbAgency == null)
            {
                dbSubmission.AgentId = null;
                dbSubmission.CompanyNumberAgentUnspecifiedReason.Clear();
            }
            else
            {
                if (!IsEnabled(type.Agent) || agent == null)
                    dbSubmission.AgentId = null;
                else
                {
                    var dbAgent = HandleMatchingElements(agent.ToQuery(dbAgency.Agent.AsQueryable()), "Agent", type.Agent.Label, (matches) => matches.Select(c => c.FirstName + " " + c.Surname));
                    if (dbAgent != null)
                        dbSubmission.AgentId = dbAgent.Id;
                }


                if (dbSubmission.AgentId != null || String.IsNullOrEmpty(unspecifiedReason))
                    dbSubmission.CompanyNumberAgentUnspecifiedReason.Clear();
                else if (!String.IsNullOrEmpty(unspecifiedReason))
                {
                    var agentReasonId = db.CompanyNumberAgentUnspecifiedReason.Where(a => a.CompanyNumberId == cd.CompanyNumberId && a.AgentUnspecifiedReason.Reason == unspecifiedReason)
                                                                        .Select(a => a.AgentUnspecifiedReasonId)
                                                                        .FirstOrDefault();
                    if (agentReasonId == default(int))
                        validations.Add(new ValidationResult("Agent", "Unspecified Agent value could not be found"));
                    else if (dbSubmission.CompanyNumberAgentUnspecifiedReason.Count != 1 || dbSubmission.CompanyNumberAgentUnspecifiedReason.First().Id != agentReasonId)
                    {
                        dbSubmission.CompanyNumberAgentUnspecifiedReason.Clear();
                        agentUnspecifiedReasonIdsToAdd.Add(agentReasonId);
                    }
                }
            }
        }
        private Data.Models.LineOfBusiness SetUnderwritingUnit(UnderwritingUnitViewModel underwritingUnit, IEnumerable<Data.Models.LineOfBusiness> availableUnits)
        {
            Data.Models.LineOfBusiness dbUnderwritingUnit = null;
            if (underwritingUnit != null)
            {
                var lobQuery = underwritingUnit.ToQuery(availableUnits.AsQueryable());
                if (lobQuery == null)
                {
                    validations.Add(new ValidationResult("UnderwritingUnit", "Data sent for {0} was not specific enough", type.UnderwritingUnit.Label));
                    return null;
                }
                else
                {
                    dbUnderwritingUnit = lobQuery.FirstOrDefault();
                    if (!IsEnabled(type.UnderwritingUnit) || dbUnderwritingUnit == null)
                        dbSubmission.LineOfBusiness.Clear();
                    else if (dbSubmission.LineOfBusiness.Count != 1 || dbSubmission.LineOfBusiness.First().Id != dbUnderwritingUnit.Id)
                    {
                        dbSubmission.LineOfBusiness.Clear();
                        lineOfBusinessIdsToAdd.Add(dbUnderwritingUnit.Id);
                    }
                }
            }

            return dbUnderwritingUnit;
        }
        private int? GetPersonId(string propertyName, SubmissionTypeFieldViewModel field, PersonnelViewModel personVM, IQueryable<Data.Models.Person> validPersons)
        {
            if (!IsEnabled(field) || personVM == null)
                return null;
            else
            {
                Data.Models.Person matchedPerson = null;

                if (validPersons != null)
                    matchedPerson = HandleMatchingElements(personVM.ToQuery(validPersons), propertyName, field.Label, (matches) => matches.Select(c => c.FullName));

                return (matchedPerson == null) ? null : (int?)matchedPerson.Id;
            }
        }

        public void SetStatus(SubmissionStatusUpdateViewModel update, IEnumerable<SubmissionStatusHistoryViewModel> additionalStatusHistoryEntries, bool addHistoryEntryIfApplicable, string updateUser, DateTime changedDate)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbStatuses = db.CompanyNumberSubmissionStatus.Where(s => s.CompanyNumberId == cd.CompanyNumberId)
                                                                 .Select(s => s.SubmissionStatus)
                                                                 .Include(s => s.SubmissionStatusSubmissionStatusReason)
                                                                 .Include(s => s.SubmissionStatusSubmissionStatusReason.Select(r => r.SubmissionStatusReason))
                                                                 .ToArray();
                var dbStatus = dbStatuses.Where(s => s.StatusCode == update.Status).FirstOrDefault();
                if (dbStatus == null)
                    validations.Add(new ValidationResult("Status", "Status could not be found in BCS Database"));
                else
                {
                    var submissionStatusDate = (update.StatusDate.HasValue) ? (DateTime?)update.StatusDate.Value.Date : null;
                    Data.Models.SubmissionStatusReason dbReason = null;
                    if (dbStatus != null && update.StatusReason != null)
                    {
                        dbReason = dbStatus.SubmissionStatusSubmissionStatusReason.Where(r => r.SubmissionStatusReason.ReasonCode == update.StatusReason).Select(r => r.SubmissionStatusReason).FirstOrDefault();
                        if (dbReason == null)
                            validations.Add(new ValidationResult("StatusReason", "Status Reason could not be found in BCS Database"));
                    }

                    if (additionalStatusHistoryEntries != null)
                        foreach (var sh in additionalStatusHistoryEntries.Reverse())
                        {
                            var dbHistoryStatus = dbStatuses.Where(s => s.StatusCode == sh.Status).FirstOrDefault();
                            if (dbHistoryStatus == null)
                                validations.Add(new ValidationResult("Status", "Status for history item could not be found in BCS Database"));
                            else
                            {
                                Data.Models.SubmissionStatusReason dbHistoryReason = null;
                                if (!String.IsNullOrEmpty(sh.StatusReason))
                                {
                                    dbHistoryReason = dbHistoryStatus.SubmissionStatusSubmissionStatusReason.Select(sr => sr.SubmissionStatusReason).FirstOrDefault(sr => sr.ReasonCode == sh.StatusReason);
                                    if (dbHistoryReason == null)
                                        validations.Add(new ValidationResult("StatusReason", "Status Reason for history item could not be found in BCS Database"));
                                }

                                dbSubmission.SubmissionStatusHistory.Add(new Data.Models.SubmissionStatusHistory()
                                {
                                    PreviousSubmissionStatusId = dbHistoryStatus.Id,
                                    PreviousSubmissionStatusReasonId = (dbHistoryReason != null) ? dbHistoryReason.Id : (int?)null,
                                    PreviousSubmissionStatusReasonOther = sh.StatusNote,
                                    SubmissionStatusDate = sh.Date ?? submissionStatusDate,
                                    StatusChangeDt = sh.ChangeDate ?? changedDate,
                                    StatusChangeBy = updateUser
                                });
                            }
                        }

                    if (addHistoryEntryIfApplicable && dbSubmission.SubmissionStatusId != dbStatus.Id)
                        dbSubmission.SubmissionStatusHistory.Add(new Data.Models.SubmissionStatusHistory()
                        {   //Actually current status
                            PreviousSubmissionStatusId = dbStatus.Id,
                            PreviousSubmissionStatusReasonId = (dbReason != null) ? dbReason.Id : (int?)null,
                            PreviousSubmissionStatusReasonOther = update.StatusNotes,
                            SubmissionStatusDate = submissionStatusDate,
                            StatusChangeDt = changedDate,
                            StatusChangeBy = updateUser
                        });

                    dbSubmission.SubmissionStatusId = dbStatus.Id;
                    dbSubmission.SubmissionStatusDate = submissionStatusDate;
                    dbSubmission.SubmissionStatusReasonId = (dbReason == null) ? (int?)null : dbReason.Id;
                    dbSubmission.SubmissionStatusReasonOther = update.StatusNotes;
                }
            }
        }

        public Data.Models.PolicySymbol SetPolicySymbol(PolicySymbolSummaryViewModel policySymbol)
        {
            Data.Models.PolicySymbol dbPolicySymbol = null;
            if (!IsEnabled(type.PolicySymbol) || policySymbol == null)
                dbSubmission.PolicySymbolId = null;
            else
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    dbPolicySymbol = HandleMatchingElements(policySymbol.ToQuery(db.PolicySymbol.Where(p => p.CompanyNumberId == cd.CompanyNumberId)), "PolicySymbol", type.PolicySymbol.Label, (matches) => matches.Select(c => c.Description));
                    if (dbPolicySymbol != null)
                        dbSubmission.PolicySymbolId = dbPolicySymbol.Id;
                }
            }

            return dbPolicySymbol;
        }

        public void SetCarrierDetails(OtherCarrierViewModel priorCarrier, decimal? priorCarrierPremium, OtherCarrierViewModel acquiringCarrier, decimal? acquiringCarrierPremium)
        {
            var carrier = GetCarrier("PriorCarrier", type.PriorCarrier, priorCarrier);
            dbSubmission.PriorCarrierId = (carrier == null) ? null : (int?)carrier.Id;
            dbSubmission.PriorCarrierPremium = IsEnabled(type.PriorCarrierPremium) ? priorCarrierPremium : null;

            carrier = GetCarrier("AcquiringCarrier", type.AcquiringCarrier, acquiringCarrier);
            dbSubmission.OtherCarrierId = (carrier == null) ? null : (int?)carrier.Id;
            dbSubmission.OtherCarrierPremium = IsEnabled(type.AcquiringCarrierPremium) ? acquiringCarrierPremium : null;
        }
        private Data.Models.OtherCarrier GetCarrier(string propertyName, SubmissionTypeFieldViewModel field, OtherCarrierViewModel carrierVM)
        {
            if (!IsEnabled(field) || carrierVM == null)
                return null;
            else
            {
                using (var db = Application.GetDatabaseInstance())
                    return HandleMatchingElements(carrierVM.ToQuery(db.CompanyNumberOtherCarrier.Where(c => c.CompanyNumberId == cd.CompanyNumberId).Select(c => c.OtherCarrier)), propertyName, field.Label, (matches) => matches.Select(c => c.Description));
            }
        }

        public void SetPolicySystem(PolicySystemViewModel policySystem)
        {
            if (!IsEnabled(type.PolicySystem) || policySystem == null)
                dbSubmission.PolicySystemId = null;
            else
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    var dbPolicySystem = HandleMatchingElements(policySystem.ToQuery(db.CompanyNumber.Where(c => c.Id == cd.CompanyNumberId).SelectMany(c => c.PolicySystem)), "PolicySystem", type.PolicySystem.Label, (matches) => matches.Select(c => c.Description));
                    if (dbPolicySystem != null)
                        dbSubmission.PolicySystemId = dbPolicySystem.Id;
                }
            }
        }

        public void SetNames(List<SubmissionAdditionalNameViewModel> names, string updateUser, DateTime updateDate)
        {
            var currentIds = names.Select(n => n.Id).ToArray();
            foreach (var dbNameToDelete in dbSubmission.AdditionalNames.Where(n => !currentIds.Contains(n.Id)).ToArray())
                originalDb.SubmissionAdditionalName.Remove(dbNameToDelete);

            if (names.Count > 0)
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    for (var i = 0; i < names.Count; i++)
                    {
                        var n = names[i];
                        var dbName = dbSubmission.AdditionalNames.FirstOrDefault(dbAddr => dbAddr.Id == n.Id);
                        if (dbName == null)
                        {
                            dbName = new Data.Models.SubmissionAdditionalName() { Id = Guid.NewGuid(), Submission = dbSubmission, EntryBy = updateUser, EntryDt = updateDate };
                            dbSubmission.AdditionalNames.Add(dbName);
                        }

                        dbName.ClientCoreSequenceNumber = n.SequenceNumber;
                        dbName.BusinessName = n.BusinessName;
                        dbName.ModifiedBy = updateUser;
                        dbName.ModifiedDt = updateDate;
                    }
                }
            }
        }

        public void SetAddresses(List<SubmissionAddressViewModel> addresses, string updateUser, DateTime updateDate)
        {
            var currentIds = addresses.Select(an => an.Id).ToArray();
            foreach (var dbAddressToDelete in dbSubmission.Addresses.Where(n => !currentIds.Contains(n.Id)).ToArray())
                originalDb.SubmissionAddress.Remove(dbAddressToDelete);

            if (addresses.Count > 0)
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    for (var i = 0; i < addresses.Count; i++)
                    {
                        var a = addresses[i];
                        var dbAddress = dbSubmission.Addresses.FirstOrDefault(dbAddr => dbAddr.Id == a.Id);
                        if (dbAddress == null)
                        {
                            dbAddress = new Data.Models.SubmissionAddress() { Id = Guid.NewGuid(), Submission = dbSubmission, EntryBy = updateUser, EntryDt = updateDate };
                            dbSubmission.Addresses.Add(dbAddress);
                        }

                        dbAddress.ClientCoreAddressId = a.ClientCoreAddressId;
                        dbAddress.Address1 = a.Address1;
                        dbAddress.Address2 = a.Address2;
                        dbAddress.City = a.City;
                        dbAddress.State = a.State;
                        dbAddress.County = a.County;
                        dbAddress.PostalCode = a.PostalCode;
                        dbAddress.Longitude = a.Longitude;
                        dbAddress.Latitude = a.Latitude;
                        dbAddress.ModifiedBy = updateUser;
                        dbAddress.ModifiedDt = updateDate;

                        if (String.IsNullOrEmpty(a.Country))
                            dbAddress.CountryId = null;
                        else
                        {
                            if (!countryCodeToId.ContainsKey(a.Country))
                                validations.Add(new ValidationResult("Address[" + i + "]-Country", "Country code {0} could not be found", a.Country));
                            else
                                dbAddress.CountryId = countryCodeToId[a.Country];
                        }
                    }
                }
            }
        }

        public void SetClassCodes(List<ClassCodeViewModel> classCodes)
        {
            if (!IsEnabled(type.ClassCodes) || classCodes.Count == 0)
                dbSubmission.ClassCodes.Clear();
            else
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    List<Data.Models.ClassCode> validClassCodes = new List<Data.Models.ClassCode>();
                    foreach (var classCode in classCodes)
                    {
                        var dbClassCode = HandleMatchingElements(classCode.ToQuery(db.ClassCode.Where(cc => cc.CompanyNumberId == cd.CompanyNumberId)), "ClassCode", type.ClassCodes.Label, (matches) => matches.Select(c => c.CodeValue));
                        if (dbClassCode != null)
                            validClassCodes.Add(dbClassCode);
                    }

                    if (validations.Count == 0)
                    {
                        foreach (var existingCode in dbSubmission.ClassCodes.Where(cc => !validClassCodes.Select(c => c.Id).Contains(cc.Id)).ToArray())
                            dbSubmission.ClassCodes.Remove(existingCode);

                        classCodeIdsToAdd = validClassCodes.Where(cc => !dbSubmission.ClassCodes.Select(c => c.Id).Contains(cc.Id)).Select(cc => cc.Id).ToList();
                    }
                }
            }
        }

        public void AddNewEntries()
        {
            if (lineOfBusinessIdsToAdd.Count > 0)
                foreach (var lob in originalDb.LineOfBusiness.Where(l => lineOfBusinessIdsToAdd.Contains(l.Id)))
                    dbSubmission.LineOfBusiness.Add(lob);
            if (agentUnspecifiedReasonIdsToAdd.Count > 0)
                foreach (var ur in originalDb.CompanyNumberAgentUnspecifiedReason.Where(r => r.CompanyNumberId == cd.CompanyNumberId && agentUnspecifiedReasonIdsToAdd.Contains(r.AgentUnspecifiedReasonId)))
                    dbSubmission.CompanyNumberAgentUnspecifiedReason.Add(ur);
            if (classCodeIdsToAdd.Count > 0)
                foreach (var cc in originalDb.ClassCode.Where(cc => classCodeIdsToAdd.Contains(cc.Id)))
                    dbSubmission.ClassCodes.Add(cc);
            foreach (var c in commentsToAdd)
                dbSubmission.SubmissionComment.Add(c);
        }

        private T HandleMatchingElements<T>(IQueryable<T> query, string propertyName, string displayName, Func<IEnumerable<T>, IEnumerable<string>> multipleMatchDescriptor) where T : class
        {
            T result = null;
            if (query == null)
                validations.Add(new ValidationResult(propertyName, "Data sent for {0} was not specific enough", displayName));
            else
            {
                var dbItems = query.ToArray();
                if (dbItems.Length == 0)
                    validations.Add(new ValidationResult(propertyName, "{0} could not be found in BCS Database", displayName));
                else if (dbItems.Length > 1)
                    validations.Add(new ValidationResult(propertyName, "Matched multiple {0} in BCS Database ({1})", displayName, String.Join(", ", multipleMatchDescriptor(dbItems))));
                else
                    result = dbItems[0];
            }

            return result;
        }
        #endregion
    }
}
