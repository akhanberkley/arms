﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BTS.WFA.BCS.Data;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.ViewModels;
using LinqKit;
using Int = BTS.WFA.Integration;

namespace BTS.WFA.BCS.Mapping
{
    public class ClientToData
    {
        private bool isNew;
        private CompanyDetail cd;
        private ClientFieldSettingsViewModel fields;
        private ClientViewModel client;

        private ConcurrentDictionary<ClientAddressViewModel, AddressLookupResultsViewModel> addressResults = new ConcurrentDictionary<ClientAddressViewModel, AddressLookupResultsViewModel>();
        private List<Task> addressLookups = new List<Task>();

        private static readonly Regex whiteSpaceRegex = new Regex(@"\s+");

        public ClientToData(CompanyDetail cd, bool isNew, ClientViewModel client)
        {
            this.cd = cd;
            this.isNew = isNew;
            this.fields = cd.Configuration.ClientFields;
            this.client = client;

            //We can Look up addresses now
            foreach (var addr in client.Addresses)
            {
                addressLookups.Add(Task.Factory.StartNew(() =>
                {
                    var addrSearch = new ViewModels.AddressLookupCriteriaViewModel() { Address1 = addr.Address1, Address2 = addr.Address2, City = addr.City, State = addr.State, PostalCode = addr.PostalCode, County = addr.County };
                    addressResults.GetOrAdd(addr, Services.AddressService.Lookup(cd, addrSearch));
                }));
            }
        }

        public bool IsEnabled(ClientFieldViewModel f)
        {
            //No longer checking if the field is hidden on add - other services are looking to populate these fields on creation
            //return (f.Enabled && (!isNew || f.VisibleOnAdd));
            return f.Enabled;
        }

        public Int.Models.ClientSaveResult SaveInClientSystem(ClientSystemId clientSystemId, CodeListItemViewModel segmentationOverride)
        {
            var ccClient = new Int.Models.Client();
            if (clientSystemId != null)
            {
                ccClient.Id = clientSystemId.Id;
                ccClient.CmsId = clientSystemId.CmsId;
            }
            ccClient.ClientType = cd.SystemSettings.ClientCoreClientType;
            ccClient.CorporationCode = cd.SystemSettings.ClientCoreCorpCode;
            ccClient.Segment = (segmentationOverride != null) ? segmentationOverride.Code : cd.SystemSettings.ClientCoreSegment;

            ccClient.Names.Add(new Int.Models.ClientName() { SequenceNumber = client.PrimaryName.SequenceNumber, BusinessName = client.PrimaryName.BusinessName, NameType = "Insured", IsPrimary = true });
            foreach (var n in client.AdditionalNames)
                ccClient.Names.Add(new Int.Models.ClientName() { SequenceNumber = n.SequenceNumber, BusinessName = n.BusinessName, NameType = n.NameType, IsPrimary = false });

            foreach (var a in client.Addresses)
            {
                var splitAddr1 = Services.AddressService.SplitBuildNumberAddressLine(a.Address1);
                ccClient.Addresses.Add(new Int.Models.ClientAddress() { Id = a.ClientCoreAddressId, IsPrimary = a.IsPrimary, AddressType = (a.AddressTypes.Count > 0) ? a.AddressTypes[0] : null, HouseNumber = splitAddr1.Item1, Address1 = splitAddr1.Item2, Address2 = a.Address2, City = a.City, State = a.State, County = a.County, Country = a.Country, PostalCode = a.PostalCode });
            }

            if (IsEnabled(fields.PortfolioInformation))
            {
                ccClient.PortfolioId = client.Portfolio.Id;
                ccClient.PorftolioName = client.Portfolio.Name;
            }
            if (IsEnabled(fields.FEIN))
                ccClient.TaxId = client.FEIN;
            if (IsEnabled(fields.ContactInformation))
                foreach (var c in client.Contacts)
                    if (c.ContactType == Domains.ContactType.Phone)
                        ccClient.PhoneNumber = c.Value;

            var ccSettings = cd.SystemSettings.ToClientSystemSettings();
            return Int.Services.ClientService.Save(ccSettings, ccClient, cd.SystemSettings.ClientCoreCorpId);
        }
        public Int.Models.BaseServiceCallResult UpdatePortfolioNameInClientSystem(long portfolioId, string portfolioName)
        {
            var ccSettings = cd.SystemSettings.ToClientSystemSettings();
            return Int.Services.ClientService.UpdatePortfolioName(ccSettings, portfolioId, portfolioName);
        }

        public void SetSystemIds(ClientSummaryViewModel refreshedClient)
        {
            var allNames = client.AdditionalNames.Union(new[] { client.PrimaryName });
            var allRefreshedNames = refreshedClient.AdditionalNames.Union(new[] { refreshedClient.PrimaryName });
            foreach (var name in allNames.Where(a => a.SequenceNumber == null))
            {
                var matchingName = allRefreshedNames.First(n => ((n.BusinessName ?? "").Trim() == (name.BusinessName ?? "").Trim()));
                name.SequenceNumber = matchingName.SequenceNumber;
            }

            foreach (var addr in client.Addresses.Where(a => a.ClientCoreAddressId == null))
            {
                var matchingAddress = refreshedClient.Addresses.First(a =>
                                                                    (whiteSpaceRegex.Replace(a.Address1 ?? "", "") == whiteSpaceRegex.Replace(addr.Address1 ?? "", ""))//removing whitespace as issues could occur due to the number/line1 breakdown (8300  Twana vs 8300 Twana)
                                                                    && ((a.Address2 ?? "").Trim() == (addr.Address2 ?? "").Trim())
                                                                    && ((a.City ?? "").Trim() == (addr.City ?? "").Trim())
                                                                    && ((a.State ?? "").Trim() == (addr.State ?? "").Trim())
                                                                    && ((a.PostalCode ?? "").Trim() == (addr.PostalCode ?? "").Trim()));
                addr.ClientCoreAddressId = matchingAddress.ClientCoreAddressId;
            }
        }

        public void SetBasicDetails(Data.Models.Client dbClient)
        {
            if (IsEnabled(fields.BusinessDescription))
                dbClient.BusinessDescription = client.BusinessDescription;
            if (IsEnabled(fields.ExperianId))
                dbClient.ExperianId = client.ExperianId;

            if (IsEnabled(fields.ContactInformation))
                foreach (var c in client.Contacts)
                    if (c.ContactType == Domains.ContactType.Phone)
                        dbClient.PhoneNumber = c.Value;
        }

        public void SetSicCode(Data.Models.Client dbClient)
        {
            if (!IsEnabled(fields.SicCode) || client.SicCode == null)
            {
                dbClient.SicCodeId = null;
            }
            else
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    var sicCode = db.SicCodeList.FirstOrDefault(s => s.Code == client.SicCode.Code);
                    if (sicCode != null)
                        dbClient.SicCodeId = sicCode.Id;
                }
            }
        }

        public void SetNaicsCode(Data.Models.Client dbClient)
        {
            if (!IsEnabled(fields.NaicsCode) || client.NaicsCode == null)
            {
                dbClient.NaicsCodeId = null;
            }
            else
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    var naicsCode = db.NaicsCodeList.FirstOrDefault(n => n.Code == client.NaicsCode.Code);
                    if (naicsCode != null)
                        dbClient.NaicsCodeId = naicsCode.Id;
                }
            }
        }

        public void SetNames(Data.Models.Client dbClient)
        {
            Dictionary<string, int> nameTypes = null;
            using (var db = Application.GetDatabaseInstance())
                nameTypes = db.ClientNameType.ToDictionary(t => t.NameType, t => t.Id);

            foreach (var name in client.AdditionalNames.Where(n => n.SequenceNumber.HasValue && nameTypes.ContainsKey(n.NameType)))
            {
                var dbName = dbClient.ClientCoreNameClientNameType.FirstOrDefault(nDB => nDB.ClientCoreNameId == name.SequenceNumber);
                if (dbName == null)
                {
                    dbName = new Data.Models.ClientCoreNameClientNameType() { ClientCoreNameId = name.SequenceNumber.Value };
                    dbClient.ClientCoreNameClientNameType.Add(dbName);
                }

                dbName.ClientNameTypeId = nameTypes[name.NameType];
            }
        }

        public void SetAddresses(Data.Models.Client dbClient)
        {
            Dictionary<string, int> addressTypes = null;
            using (var db = Application.GetDatabaseInstance())
                addressTypes = db.AddressType.ToDictionary(t => t.AddressTypeValue, t => t.Id);

            //Wait for the lookups to complete
            Task.WaitAll(addressLookups.ToArray());
            foreach (var addr in client.Addresses.Where(a => a.ClientCoreAddressId.HasValue && a.AddressTypes.Count > 0 && addressTypes.ContainsKey(a.AddressTypes[0])))
            {
                var dbAddress = dbClient.ClientCoreAddressAddressType.FirstOrDefault(aDB => aDB.ClientCoreAddressId == addr.ClientCoreAddressId);
                if (dbAddress == null)
                {
                    dbAddress = new Data.Models.ClientCoreAddressAddressType() { ClientCoreAddressId = addr.ClientCoreAddressId.Value };
                    dbClient.ClientCoreAddressAddressType.Add(dbAddress);
                }

                dbAddress.AddressTypeId = addressTypes[addr.AddressTypes[0]];
                dbAddress.Longitude = null;
                dbAddress.Latitude = null;
                if (addressResults[addr].Matches.Count > 0)
                {
                    var primaryMatch = addressResults[addr].Matches[0];
                    if (String.Equals(primaryMatch.Address1 ?? "", addr.Address1 ?? "", StringComparison.CurrentCultureIgnoreCase)
                        && String.Equals(primaryMatch.Address2 ?? "", addr.Address2 ?? "", StringComparison.CurrentCultureIgnoreCase)
                        && String.Equals(primaryMatch.City ?? "", addr.City ?? "", StringComparison.CurrentCultureIgnoreCase)
                        && String.Equals(primaryMatch.State ?? "", addr.State ?? "", StringComparison.CurrentCultureIgnoreCase)
                        && String.Equals(primaryMatch.PostalCode ?? "", addr.PostalCode ?? "", StringComparison.CurrentCultureIgnoreCase))
                    {
                        dbAddress.Latitude = primaryMatch.Latitude;
                        dbAddress.Longitude = primaryMatch.Longitude;
                        addr.Latitude = dbAddress.Latitude;
                        addr.Longitude = dbAddress.Longitude;
                    }
                }
            }
        }

        public void SetCustomizations(IClearanceDb db, Data.Models.Client dbClient)
        {
            //CompanyCodes
            Services.CompanyCustomizationService.SetDatabaseCompanyCodes(db, cd, client.CompanyCustomizations, dbClient.CompanyNumberCode);
            //Attributes
            foreach (var c in client.CompanyCustomizations.Where(c => c.CodeValue == null))
            {
                var dbAttr = db.CompanyNumberAttribute.FirstOrDefault(a => a.CompanyNumberId == cd.CompanyNumberId && a.Label == c.Customization);
                if (dbAttr != null)
                {
                    var dbClientAttr = dbClient.ClientCompanyNumberAttributeValue.FirstOrDefault(a => a.CompanyNumberAttributeId == dbAttr.Id);
                    if (dbClientAttr == null)
                    {
                        dbClientAttr = db.ClientCompanyNumberAttributeValue.Add(new Data.Models.ClientCompanyNumberAttributeValue());
                        dbClientAttr.ClientId = dbClient.Id;
                        dbClientAttr.CompanyNumberAttributeId = dbAttr.Id;
                    }

                    dbClientAttr.AttributeValue = c.TextValue;
                }
            }
        }
    }
}
