﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.BCS.ViewModels;

namespace BTS.WFA.BCS.Mapping
{
    public class ClientClearanceData
    {
        public long ClientCoreClientId { get; set; }
        public string PhoneNumber { get; set; }
        public string BusinessDescription { get; set; }
        public long? ExperianId { get; set; }
        public List<ClientClearanceNameData> Names { get; set; }
        public List<ClientClearanceAddressData> Addresses { get; set; }

        public SicCodeViewModel SicCode { get; set; }
        public NaicsCodeViewModel NaicsCode { get; set; }
        public List<CompanyCustomizationCodeValueViewModel> CompanyNumberCode { get; set; }
        public List<CompanyCustomizationTextValueViewModel> ClientCompanyNumberAttributeValue { get; set; }
    }
    public class ClientClearanceNameData
    {
        public int ClientCoreNameId { get; set; }
        public string NameType { get; set; }
    }
    public class ClientClearanceAddressData
    {
        public int ClientCoreAddressId { get; set; }
        public string AddressType { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
    }
}
