﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;

namespace BTS.WFA.BCS
{
    public class Application : BTS.WFA.WorkflowApplication<Data.IClearanceDb>
    {
        public static Dictionary<string, CompanyDetail> CompanyMap { get; private set; }

        /// <summary>
        /// This method should be called prior to any use of this Assembly to configure how it should work
        /// </summary>
        public static void Initialize<A>(params object[] handlers) where A : Data.IClearanceDb
        {
            Initialize(typeof(A), handlers);

            CompanyMap = Services.CompanyService.GetCompanyMap();

            //Extending types that Linq.Dynamic queries can use
            System.Linq.Dynamic.GlobalConfig.CustomTypeProvider.GetCustomTypes().Add(typeof(DbFunctions));
        }
    }
}
