﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.ViewModels;
using Int = BTS.WFA.Integration;

namespace BTS.WFA.BCS
{
    [TypeConverter(typeof(CompanyDetailModelConverter))]
    public class CompanyDetail
    {
        public CompanyDetail()
        {
            ValidationSettings = new HashSet<Validation.Validators>();
        }

        public CompanySummaryViewModel Summary { get; set; }

        [IgnoreDataMember]
        public int CompanyNumberId { get; set; }
        [IgnoreDataMember]
        public int CompanyId { get; set; }
        [IgnoreDataMember]
        public CompanyConfiguration Configuration { get; set; }
        [IgnoreDataMember]
        public CompanySystemSettings SystemSettings { get; set; }

        private HashSet<Validation.Validators> ValidationSettings { get; set; }
        public void AddValidationSetting(Validation.Validators validator) { ValidationSettings.Add(validator); }
        public bool HasValidationSetting(Validation.Validators validator)
        {
            return ValidationSettings.Contains(validator);
        }
    }

    #region Company Subclasses
    public class CompanyDetailModelConverter : TypeConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return false;
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            var key = value as string;
            if (!String.IsNullOrEmpty(key))
            {
                key = key.ToUpper();
                if (Application.CompanyMap.ContainsKey(key))
                    return Application.CompanyMap[key];
            }

            return base.ConvertFrom(context, culture, value);
        }
    }

    public class CompanyConfiguration
    {
        public CompanyConfiguration()
        {
            ClientCoreSegments = new List<ClientCoreSegmentViewModel>();
            SubmissionTypes = new List<SubmissionTypeSettingsViewModel>();
            ClientFields = new ClientFieldSettingsViewModel();
        }

        public bool AgentsEnabled { get; set; }
        public bool AnalystEnabled { get; set; }
        public bool TechnicianEnabled { get; set; }
        public bool UsesAllUnderwritersForAllAssignments { get; set; }
        public int? FutureEffectiveDateDayCount { get; set; }

        public string APSAgencyUrl { get; set; }

        public bool UsesCms { get; set; }
        public bool AllowClientPrimaryChanges { get; set; }

        public bool PolicyNumberGeneratable { get; set; }
        public string PolicyNumberRegex { get; set; }
        public bool SubmissionEnforceAgencyLicensedState { get; set; }

        public List<ClientCoreSegmentViewModel> ClientCoreSegments { get; set; }
        public List<SubmissionTypeSettingsViewModel> SubmissionTypes { get; set; }
        public ClientFieldSettingsViewModel ClientFields { get; set; }
    }

    public class CompanySystemSettings
    {
        public CompanySystemSettings()
        {
            UnderwritingRoles = new List<string>();
            UnderwritingAnalystRoles = new List<string>();
            UnderwritingTechnicianRoles = new List<string>();
        }

        public string ClientCoreSystemCd { get; set; }
        public string ClientCoreUsername { get; set; }
        public string ClientCorePassword { get; set; }
        public string ClientCoreLocalId { get; set; }
        public string ClientCoreCorpId { get; set; }
        public string ClientCoreCorpCode { get; set; }
        public string ClientCoreSegment { get; set; }
        public string ClientCoreClientType { get; set; }
        public string ClientCoreSearchClientType { get; set; }
        public string ClientCoreEndPointUrl { get; set; }
        public IEnumerable<string> SearchFilter { get; set; }
        public bool DisableClientWildcardSearch { get; set; }
        public int SearchFilterService { get; set; }
        public string ApsServiceUrl { get; set; }
        public string DuplicateSubmissionQuery { get; set; }

        public string ClientAdvSearchEndPointUrl { get; set; }
        public string ClientAdvSearchUserName { get; set; }
        public string ClientAdvSearchDsik { get; set; }
        public string ClientExperianSearchEndPointUrl { get; set; }

        public string ApsQueueName { get; set; }
        public string ApsQueueHost { get; set; }
        public string ApsQueueChannel { get; set; }
        public string ApsQueueManager { get; set; }
        public int? ApsQueuePort { get; set; }

        public bool UsesOfacSubmissionNote { get; set; }
        public bool UsesDuplicateSubmissionNote { get; set; }

        public List<string> UnderwritingRoles { get; set; }
        public List<string> UnderwritingAnalystRoles { get; set; }
        public List<string> UnderwritingTechnicianRoles { get; set; }

        public IEnumerable<string> AllPersonnelRoles { get { return UnderwritingRoles.Union(UnderwritingAnalystRoles).Union(UnderwritingTechnicianRoles); } }

        public List<CompanySummaryViewModel> CrossClearanceCompanyNumbers { get; set; }

        public Int.Models.ClientSystemSettings ToClientSystemSettings()
        {
            return new Int.Models.ClientSystemSettings()
            {
                Url = ClientCoreEndPointUrl,
                SystemCode = ClientCoreSystemCd,
                Username = ClientCoreUsername,
                Password = ClientCorePassword,
                LocalId = ClientCoreLocalId,
                UsesPortfolioService = (SearchFilterService == 1),
                UsesCmsService = (SearchFilterService == 3),
                DSIK = ClientAdvSearchDsik,
                SearchFilters = SearchFilter
            };
        }
        public Int.Models.AdvancedClientSettings ToAdvancedClientSettings()
        {
            return new Int.Models.AdvancedClientSettings()
            {
                Url = ClientAdvSearchEndPointUrl,
                Dsik = ClientAdvSearchDsik,
                Username = ClientAdvSearchUserName
            };
        }
        public Int.Models.AdvancedClientSettings ToExperianClientSettings()
        {
            return new Int.Models.AdvancedClientSettings()
            {
                Url = ClientExperianSearchEndPointUrl,
                Dsik = ClientAdvSearchDsik,
                Username = ClientAdvSearchUserName
            };
        }
        public Int.Models.ClientSearchCriteria ToClientSearchCriteria()
        {
            return new Int.Models.ClientSearchCriteria()
            {
                CorpId = ClientCoreCorpId,
                ClientType = ClientCoreSearchClientType
            };
        }
    }
    #endregion
}
