﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BTS.WFA.BCS
{
    [TypeConverter(typeof(ClientSystemIdModelConverter))]
    public class ClientSystemId
    {
        public long? Id { get; set; }
        public Guid? CmsId { get; set; }

        public override string ToString()
        {
            return ((object)Id ?? CmsId).ToString();
        }

        public IQueryable<Data.Models.Submission> AsSubmissionFilter(CompanyDetail cd, Data.IClearanceDb db, IQueryable<Data.Models.Submission> query)
        {
            if (Id.HasValue)
                return query.Where(s => s.ClientCoreClientId == Id);
            else if (CmsId.HasValue)
            {
                var cmsId = CmsId.Value;//doing this here since EF will count it as nullable and do a nullable check otherwise
                return query.Where(s => db.Client.Where(c => c.CompanyNumberId == cd.CompanyNumberId && c.CmsClientId == cmsId).Select(c => c.ClientCoreClientId).Contains(s.ClientCoreClientId));
            }
            else
                throw new ArgumentException("Invalid Client System Id");
        }
    }

    public class ClientSystemIdModelConverter : TypeConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return false;
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            string s = (string)value; long id; Guid cmsId;
            if (long.TryParse(s, out id))
                return new ClientSystemId { Id = id };
            else if (Guid.TryParse(s, out cmsId))
                return new ClientSystemId { CmsId = cmsId };
            else
                throw new ArgumentException("Client System Id is invalid");
        }
    }
}
