using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.ClearSync
{
    public enum Status : int
    {
        Active, 
        Reinstated, 
        Cancelled
    }
    public class DssEntry
    {
        #region Private Instance Variables
        //a.sym, a.n_pol, a.n_pol_mod, a.insd_name, a.eff_dt, a.exp_dt, a.canctermination_dt, a.twprem, a.d_tran_proc, a.d_cvg_seq
        private string _policySymbol;
        private string _policyNumber;
        private string _policyMod;
        private string _insuredName;
        private DateTime _effectiveDate;
        private DateTime _expirationDate;
        private DateTime _cancelTerminationDate;
        private double _premium;
        private DateTime _transactionProcessingDate;
        private DateTime _coverageSequenceDate;
        private string _policyStatusString;
        private Status _policyStatus;
        private bool _cancelPendingAuditFlag;
        private DateTime _statusEffectiveDate;
        #endregion

        #region Properties

        public bool CancelPendingAuditFlag
        {
            get { return _cancelPendingAuditFlag; }
            set { _cancelPendingAuditFlag = value; }
        }

        public DateTime StatusEffectiveDate
        {
            get { return _statusEffectiveDate; }
            set { _statusEffectiveDate = value; }
        }

        public Status PolicyStatus
        {
            get { return _policyStatus; }
            set
            {
                _policyStatus = value;
            }
        }

        public string PolicyStatusString
        {
            get
            {
                return _policyStatusString;
            }
            set
            {
                _policyStatusString = value;

                if (value.ToLower().Contains("cancel"))
                    _policyStatus = Status.Cancelled;
                else if (value.ToLower().Contains("reinstat"))
                    _policyStatus = Status.Reinstated;
                else
                    _policyStatus = Status.Active;
            }
        }
        public string PolicySymbol
        {
            get
            {
                return this._policySymbol;
            }
            set
            {
                this._policySymbol = value;
            }
        }

        public string PolicyNumber
        {
            get
            {
                return this._policyNumber;
            }
            set
            {
                this._policyNumber = value;
            }
        }

        public string PolicyMod
        {
            get
            {
                return this._policyMod;
            }
            set
            {
                this._policyMod = value;
            }
        }

        public string InsuredName
        {
            get
            {
                return this._insuredName;
            }
            set
            {
                this._insuredName = value;
            }
        }

        public DateTime EffectiveDate 
        {
            get
            {
                return this._effectiveDate;
            }
            set
            {
                this._effectiveDate = value;
            }
        }

        public DateTime ExpirationDate 
        {
            get
            {
                return this._expirationDate;
            }
            set
            {
                this._expirationDate = value;
            }
        }

        public DateTime CancelTerminationDate 
        {
            get
            {
                return this._cancelTerminationDate;
            }
            set
            {
                this._cancelTerminationDate = value;
            }
        }

        public double Premium 
        {
            get
            {
                return this._premium;
            }
            set
            {
                this._premium = value;
            }
        }

        public DateTime TransactionProcessingDate 
        {
            get
            {
                return this._transactionProcessingDate;
            }
            set
            {
                this._transactionProcessingDate = value;
            }
        }

        public DateTime CoverageSequenceDate 
        {
            get
            {
                return this._coverageSequenceDate;
            }
            set
            {
                this._coverageSequenceDate = value;
            }
        }
        #endregion
    }
}
