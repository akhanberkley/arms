using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
// Read Console input with timeout : http://forums.msdn.microsoft.com/en-US/csharpgeneral/thread/5f954d01-dbaf-410a-9b0d-6bb6f57d0b85/
namespace BCS.ClearSync
{
    class Program
    {
        #region Timeout Helpers
        private static bool timedOut;        

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetStdHandle(int stdHandle);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool PeekConsoleInput(IntPtr consoleHandle,
            [Out] InputRecord[] buffer, uint bufLength, out uint numOfEvents);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool ReadConsoleInput(IntPtr consoleHandle,
            [Out] InputRecord[] buffer, uint bufLength, out uint numOfEvents);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool GetNumberOfConsoleInputEvents(IntPtr consoleHandle,
            out uint numOfEvents); 
        #endregion

        static void Main(string[] args)
        {
            Console.WriteLine("ClearSync application running...");

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;

            #region PreviousDayDifference user pref
            // TODO: remove the line below
            Console.WriteLine(BCS.ClearSync.Properties.Settings.Default.DSN);

            Console.WriteLine("Please enter time to wait for your input :");

            string timeToWaitForUserInputInSeconds = ReadInput(BCS.ClearSync.Properties.Settings.Default.TimeToWaitForUserInputInSeconds, 
                BCS.ClearSync.Properties.Settings.Default.TimeToWaitForUserInputInSeconds.ToString());

            BCS.ClearSync.Properties.Settings.Default.TimeToWaitForUserInputInSeconds = Convert.ToInt32(timeToWaitForUserInputInSeconds);
            #endregion

            SyncController sc = SyncController.Instance();

            #region PreviousDayDifference user pref
            //Console.WriteLine(BCS.ClearSync.Properties.Settings.Default.PreviousDayDifference);

            // calculate default number of days to go back based on the last run date
            DateTime lastRunDate = sc.ClearSync.LastRunDate.IsNull ? DateTime.Now.AddDays(-1) : sc.ClearSync.LastRunDate.Value.Date;
            TimeSpan ts = DateTime.Now.Subtract(lastRunDate);
            int daysToGoBack = ts.Days;

            Console.WriteLine("Please enter number of days to go back to fetch :");

            string prevDayDiff = ReadInput(BCS.ClearSync.Properties.Settings.Default.TimeToWaitForUserInputInSeconds,
                daysToGoBack.ToString());

            BCS.ClearSync.Properties.Settings.Default.PreviousDayDifference = Convert.ToInt32(prevDayDiff);
            #endregion


            sc.Synchronize();
        }

        static string ReadInput(int timeToWait, string defaultValue)
        {
            string input = defaultValue;

            IntPtr stdin = GetStdHandle(Constants.StdInput);

            if (stdin == Constants.InvalidHandle)
                throw new Exception("Unable to aquire the standard input handle.");

            InputRecord[] records = new InputRecord[1];
            uint eventsRead = 0;
            timedOut = false;

            TimerCallback callBack = new TimerCallback(SetTimeOut);
            Timer timer = new Timer(callBack, null, timeToWait * 1000, System.Threading.Timeout.Infinite);

            while (!timedOut)
            {
                // Get number of events pending
                GetNumberOfConsoleInputEvents(stdin, out eventsRead);
                if (eventsRead > 0)
                {
                    if (PeekConsoleInput(stdin, records, (uint)records.Length, out eventsRead))
                    {
                        // Check for a keyborad event
                        if (records[0].eventType == EventTypes.KeyEvent &&
                            records[0].keyEvent.keyDown)
                            break;
                        else
                            // consume non keybord event;
                            ReadConsoleInput(stdin, records, (uint)records.Length, out eventsRead);
                    }
                }
                else
                {
                    Thread.Sleep(100);
                    //Console.WriteLine("Waiting");
                }
            }

            if (timedOut)
            {
                Console.WriteLine("No console input after {0} sec, returning default value of {1}", timeToWait , defaultValue);
            }
            else
            {
                input = Console.ReadLine();
                //Console.WriteLine("You entered: " + input);
            }
            timer.Dispose();
            return input;
        }

        private static void SetTimeOut(object data)
        {
            timedOut = true;
        }
    }

    internal struct COORD {
        internal short X;
        internal short Y;
    }

    [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Unicode)]
    internal struct KeyEventRecord {
        internal bool keyDown;
        internal ushort repeatCount;
        internal ushort virtualKeyCode;
        internal ushort virtualScanCode;
        internal char unicodeChar;
        internal uint controlKeyState;
    }

    internal struct MouseEventRecord {
        internal COORD mousePosition;
        internal uint bottonState;
        internal uint controlKeyState;
        internal uint eventFlags;
    }

    internal struct WindowBufSize {
        internal COORD size;
    }

    internal struct MenuEventREcord {
        internal uint cmdId;
    }

    internal struct FocusEventRecord {
        internal bool setFoucus;
    }

    internal struct InputRecord {
        internal EventTypes eventType;
        internal KeyEventRecord keyEvent;
        internal MouseEventRecord mouseEvent;
        internal WindowBufSize bufSize;
        internal MenuEventREcord menuEvent;
        internal FocusEventRecord focusEvent;
    }

    internal enum EventTypes : ushort {
        KeyEvent = 0x01,
        MouseEvent = 0x02,
        WindowBufSizeEvent = 0x04,
        MenuEvent = 0x08,
        FocusEvent = 0x10
    }

    internal class Constants {
        internal const int StdInput = -10;
        internal const int StdOutput = -11;
        internal const int StdError = -12;

        internal static readonly IntPtr InvalidHandle = new IntPtr(-1);
    }
    
       
}
