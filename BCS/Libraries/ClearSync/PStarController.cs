﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.LogFramework;

namespace BCS.ClearSync
{
    class PStarController
    {
        public static DssEntry[] GetEntriesFromPStar(DateTime lastEntryDate)
        {
            List<DssEntry> results = new List<DssEntry>();

            //lastEntryDate = DateTime.Parse("11/29/2010"); //for testing

            PStarService.PremiumAuditServiceClient service = new PStarService.PremiumAuditServiceClient();

            //set the endpoint address
            service.Endpoint.Address = new System.ServiceModel.EndpointAddress(Properties.Settings.Default.PStarService);

            
            //get all policies that have been modified after last entry date.  
            PStarService.ExpirationPolicies policies = null;
            int i = 0;
            try
            {
                policies = service.getPolicyByDateRange("1/1/2005", "1/1/2020", lastEntryDate.ToString("MM/dd/yyyy"), "1/1/2020", "52");

               
                if (policies != null && policies.policyDo != null)
                {
                    DssEntry entry = null;
                    foreach (PStarService.PolicyDO policy in policies.policyDo)
                    {
                        DateTime start = DateTime.Now;
                        PStarService.PolicyDataDO[] attributes = service.getEstimatedPremium(policy.policyNo, policy.policySeqNo, "52");

                        DateTime stop = DateTime.Now;
                        if ((stop - start).TotalSeconds > 300)
                            stop = DateTime.Now;

                        i += 1;
                        
                        if (attributes != null && attributes.Length > 0)
                        {
                            PStarService.PolicyDataDO attr = attributes[0];
                            entry = new DssEntry();

                            entry.PolicyNumber = "52" + attr.policyNbr.Trim();  //n_pol
                            entry.PolicyMod = attr.policySeqNbr.Trim();     //n_pol_mod
                            //entry.InsuredName = ;  //not in service

                            entry.PolicySymbol = policy.policySymbol;

                            if (!string.IsNullOrWhiteSpace(policy.effectiveDate))  //eff_dt
                            {
                                entry.EffectiveDate = Convert.ToDateTime(policy.effectiveDate);
                            }

                            if (!string.IsNullOrEmpty(attr.expirationDate))  //exp_dt
                            {
                                entry.ExpirationDate = Convert.ToDateTime(attr.expirationDate);
                            }

                            if (!string.IsNullOrEmpty(attr.cancelDate))  //canctermination_dt
                            {
                                entry.CancelTerminationDate = Convert.ToDateTime(attr.cancelDate);
                            }

                            if (!string.IsNullOrWhiteSpace(attr.cancelPendingAuditFlag))
                            {
                                entry.CancelPendingAuditFlag = attr.cancelPendingAuditFlag == "Y";
                                if (entry.CancelPendingAuditFlag)
                                    Console.WriteLine(entry.PolicyNumber);
                            }

                            if (!string.IsNullOrWhiteSpace(attr.statusEffectiveDate))
                            {
                                DateTime val = DateTime.MinValue;
                                DateTime.TryParse(attr.statusEffectiveDate, out val);
                                entry.StatusEffectiveDate = val;
                            }

                            if (!string.IsNullOrEmpty(attr.writtenPremiumToDate))  //twprem
                            {
                                entry.Premium = Convert.ToDouble(attr.writtenPremiumToDate.Trim());
 
                                //if (!string.IsNullOrWhiteSpace(attr.statWrittenTotal))
                                //{
                                //    double statVal = 0D;
                                //    double.TryParse(attr.statWrittenTotal, out statVal);

                                //    if (statVal < 0)
                                //        entry.Premium += statVal;
                                //}

                            }

                            if (!string.IsNullOrWhiteSpace(attr.policyStatus))
                            {
                                entry.PolicyStatusString = attr.policyStatus;
                            }

                            if (!string.IsNullOrEmpty(policy.lastModifiedDate)) //d_tran_proc
                            {
                                entry.TransactionProcessingDate = Convert.ToDateTime(policy.lastModifiedDate);
                            }

                            //if (dssEntriesDs.Tables[0].Rows[i][9] != DBNull.Value)  //d_cvg_seq
                            //{
                            //    entry.CoverageSequenceDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][9]);
                            //}

                            results.Add(entry);
                        }
                    
                    }
                }

            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Unable to retrieve P* Policy Set to Sync.", ex);
                throw ex;
            }

            return results.ToArray();
        }
    }
}
