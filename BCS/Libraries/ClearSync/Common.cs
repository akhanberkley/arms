using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using BCS.Biz;
using System.Net.Mail;

namespace BCS.ClearSync
{
    class Common
    {
        internal enum EmailType
        {
            Success = 0,
            Warning,
            Failure
        }

        internal static Biz.DataManager GetDataManager()
        {
            Biz.DataManager dm = new BCS.Biz.DataManager(Properties.Settings.Default.DSN);
            dm.QueryCriteria.Clear();

            return dm;
        }

        internal static bool pStarOnly()
        {
            bool result = false;

            try
            {
                bool.TryParse(ConfigurationManager.AppSettings["pStarOnly"], out result);
            }
            catch (Exception ex)
            {
                // do nothing
            }
            return result;
        }

        internal static void SendEmail(EmailType emailType, List<string> messages)
        {
            MailAddress messageFrom = new MailAddress(Properties.Settings.Default.MailFrom);
            MailAddress messageTo = new MailAddress(Properties.Settings.Default.MailTo);

            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(messageFrom, messageTo);

            string ccAddresses = Properties.Settings.Default.MailCc;
            string[] ccAddrArray = ccAddresses.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string var in ccAddrArray)
            {
                string trimmedVar = var.Trim();
                if (OrmLib.Email.ValidateEmail(trimmedVar))
                    message.CC.Add(trimmedVar);
            }

            SmtpClient client = new SmtpClient(Properties.Settings.Default.MailServer);

            message.Subject = string.Format(Properties.Settings.Default.MailSubject, emailType);
            message.IsBodyHtml = true;
            if (emailType == EmailType.Failure)
                message.Priority = MailPriority.High;

            message.Body = BuildMessageBody(messages);
            
            client.Send(message);
        }

        private static string BuildMessageBody(List<string> messageList)
        {
            if (messageList == null)
                return string.Empty;
            if (messageList.Count == 0)
                return string.Empty;
            string[] messageArr = new string[messageList.Count];
            messageList.CopyTo(messageArr);
            string body = string.Join("<br /><br />", messageArr);
            return body;
        }        
    }    
}
