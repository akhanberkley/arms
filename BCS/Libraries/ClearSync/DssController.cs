using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;

using BTS.LogFramework;
using IBM.Data.Informix;
using System.Data.OleDb;

namespace BCS.ClearSync
{
    internal class DssController
    {
        #region Singleton Gobble-Di-Goop
        private static DssController instance;

        // Lock synchronization object
        private static object syncLock = new object();

        /// <summary>
        /// Protected ctor
        /// </summary>
        protected DssController()
        {
            
        }

        /// <summary>
        /// Get the singleton instance
        /// </summary>
        /// <returns></returns>
        public static DssController Instance()
        {
            // Support multithreaded applications through
            // 'Double checked locking' pattern which (once
            // the instance exists) avoids locking each
            // time the method is invoked 
            if (instance == null)
            {
                lock (syncLock)
                {
                    if (instance == null)
                    {
                        instance = new DssController();
                    }
                }
            }

            return instance;
        }
        #endregion

        /// <summary>
        /// uses ifx connection
        /// </summary>
        /// <param name="lastRunDate"></param>
        /// <returns></returns>
        public DssEntry[] GetDssEntriesForSyncing(DateTime lastRunDate)
        {
            Console.WriteLine("--> Retrieving entries from DSS");

            DssEntry[] dssEntries;

            #region Build date variables for stored procedure
            string currentDate = DateTime.Now.Month.ToString() + "/" +
                DateTime.Now.Day.ToString() + "/" +
                DateTime.Now.Year.ToString();

            string lastRunDateConv = lastRunDate.Month.ToString() + "/" +
                lastRunDate.Day.ToString() + "/" +
                lastRunDate.Year.ToString();
            #endregion

            IfxCommand command = new IfxCommand("sp_clearance_policy_sync");

            command.Connection = DataAccess.Database.GetOpenIfxConnection(Properties.Settings.Default.DSN_DSS);
            command.CommandTimeout = 600;
            command.CommandType = System.Data.CommandType.StoredProcedure;

            #region Parameters
            IfxParameter[] arParams = new IfxParameter[2];

            arParams[0] = new IfxParameter("d_prev_date", IfxType.Date);
            arParams[0].Value = lastRunDateConv;
            command.Parameters.Add(arParams[0]);

            //arParams[1] = new IfxParameter("d_curr_date", IfxType.Date);
            //arParams[1].Value = currentDate;
            //command.Parameters.Add(arParams[1]);
            #endregion

            DataSet dssEntriesDs = new DataSet();

            IfxDataAdapter da = new IfxDataAdapter(command);

            try
            {
                da.Fill(dssEntriesDs);
                dssEntries = new DssEntry[dssEntriesDs.Tables[0].Rows.Count];

                for (int i = 0; i < dssEntriesDs.Tables[0].Rows.Count; i++)
                {
                    #region Populate Individual DssEntry elements
                    dssEntries[i] = new DssEntry();

                    dssEntries[i].PolicySymbol = dssEntriesDs.Tables[0].Rows[i][0].ToString();  //sym
                    dssEntries[i].PolicyNumber = dssEntriesDs.Tables[0].Rows[i][1].ToString();  //n_pol
                    dssEntries[i].PolicyMod = dssEntriesDs.Tables[0].Rows[i][2].ToString();     //n_pol_mod
                    dssEntries[i].InsuredName = dssEntriesDs.Tables[0].Rows[i][3].ToString();   //insd_name

                    if (dssEntriesDs.Tables[0].Rows[i][4] != DBNull.Value)  //eff_dt
                    {
                        dssEntries[i].EffectiveDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][4]);
                    }

                    if (dssEntriesDs.Tables[0].Rows[i][5] != DBNull.Value)  //exp_dt
                    {
                        dssEntries[i].ExpirationDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][5]);
                    }

                    if (dssEntriesDs.Tables[0].Rows[i][6] != DBNull.Value)  //canctermination_dt
                    {
                        dssEntries[i].CancelTerminationDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][6]);
                    }

                    if (dssEntriesDs.Tables[0].Rows[i][7] != DBNull.Value)  //twprem
                    {
                        dssEntries[i].Premium = Convert.ToDouble(dssEntriesDs.Tables[0].Rows[i][7]);
                    }

                    if (dssEntriesDs.Tables[0].Rows[i][8] != DBNull.Value)  //d_tran_proc
                    {
                        dssEntries[i].TransactionProcessingDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][8]);
                    }

                    if (dssEntriesDs.Tables[0].Rows[i][9] != DBNull.Value)  //d_cvg_seq
                    {
                        dssEntries[i].CoverageSequenceDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][9]);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Unable to retrieve DSS Entries to Sync.", ex);
                throw ex;
            }
            finally
            {
                DataAccess.Database.CloseOpenIfxConnection(command.Connection);
            }

            Console.WriteLine("    (Found " + dssEntries.Length.ToString() + " entries to sync)");
            Console.WriteLine();

            return dssEntries;
        }

        /// <summary>
        /// uses oledb connection
        /// </summary>
        /// <param name="lastRunDate"></param>
        /// <returns></returns>
        public DssEntry[] GetDssEntries(DateTime lastRunDate)
        {
            Console.WriteLine("--> Retrieving entries from DSS");

            DssEntry[] dssEntries;

            #region Build date variables for stored procedure
            string lastRunDateConv = lastRunDate.ToShortDateString();
            #endregion

            OleDbCommand command = new OleDbCommand("sp_clearance_policy_sync");

            command.Connection = DataAccess.Database.GetOpenOleDbConnection(Properties.Settings.Default.DSN_DSSOLEDB);
            command.CommandTimeout = 600;
            command.CommandType = System.Data.CommandType.StoredProcedure;

            #region Parameters
            OleDbParameter oleDbParameter = new OleDbParameter("d_prev_date", OleDbType.Date);
            oleDbParameter.Value = lastRunDateConv;
            command.Parameters.Add(oleDbParameter);
            #endregion

            DataSet dssEntriesDs = new DataSet();

            OleDbDataAdapter da = new OleDbDataAdapter(command);

            try
            {
                da.Fill(dssEntriesDs);
                dssEntries = new DssEntry[dssEntriesDs.Tables[0].Rows.Count];

                for (int i = 0; i < dssEntriesDs.Tables[0].Rows.Count; i++)
                {
                    #region Populate Individual DssEntry elements
                    dssEntries[i] = new DssEntry();

                    dssEntries[i].PolicySymbol = dssEntriesDs.Tables[0].Rows[i][0].ToString();  //sym
                    dssEntries[i].PolicyNumber = dssEntriesDs.Tables[0].Rows[i][1].ToString();  //n_pol
                    dssEntries[i].PolicyMod = dssEntriesDs.Tables[0].Rows[i][2].ToString();     //n_pol_mod
                    dssEntries[i].InsuredName = dssEntriesDs.Tables[0].Rows[i][3].ToString();   //insd_name

                    if (dssEntriesDs.Tables[0].Rows[i][4] != DBNull.Value)  //eff_dt
                    {
                        dssEntries[i].EffectiveDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][4]);
                    }

                    if (dssEntriesDs.Tables[0].Rows[i][5] != DBNull.Value)  //exp_dt
                    {
                        dssEntries[i].ExpirationDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][5]);
                    }

                    if (dssEntriesDs.Tables[0].Rows[i][6] != DBNull.Value)  //canctermination_dt
                    {
                        dssEntries[i].CancelTerminationDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][6]);
                    }

                    if (dssEntriesDs.Tables[0].Rows[i][7] != DBNull.Value)  //twprem
                    {
                        dssEntries[i].Premium = Convert.ToDouble(dssEntriesDs.Tables[0].Rows[i][7]);
                    }

                    if (dssEntriesDs.Tables[0].Rows[i][8] != DBNull.Value)  //d_tran_proc
                    {
                        dssEntries[i].TransactionProcessingDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][8]);
                    }

                    if (dssEntriesDs.Tables[0].Rows[i][9] != DBNull.Value)  //d_cvg_seq
                    {
                        dssEntries[i].CoverageSequenceDate = Convert.ToDateTime(dssEntriesDs.Tables[0].Rows[i][9]);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Unable to retrieve DSS Entries to Sync.", ex);
                throw ex;
            }
            finally
            {
                DataAccess.Database.CloseOpenOleDbConnection(command.Connection);
            }

            Console.WriteLine("    (Found " + dssEntries.Length.ToString() + " entries to sync)");
            Console.WriteLine();

            return dssEntries;
        }
    }
}
