using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Text;

using BCS.Biz;
using BTS.LogFramework;
using System.IO;

namespace BCS.ClearSync
{
    class SyncController
    {
        private DataManager _dm;
        private ClearSyncCollection _csc;   // contains configuration

        #region Properties and Enums
        public Biz.ClearSync ClearSync
        {
            get
            {
                return this._csc[0];
            }
        }

        public DateTime ValidLastPullDate
        {
            get
            {
                return DateTime.Now.AddDays(-1 * (Properties.Settings.Default.PreviousDayDifference + DaysToGoBack));
                //return Convert.ToDateTime("6/15/2008");
            }
        }

        public int DaysToGoBack
        {
            get
            {
                try
                {
                    string value = System.Configuration.ConfigurationManager.AppSettings["DaysToGoBack"];
                    int retVal;
                    Int32.TryParse(value, out retVal);
                    return retVal;
                }
                catch (Exception ex)
                {
                }

                return 0;
            }
        }

        protected List<string> warningList = new List<string>();
        protected List<string> successStatsList = new List<string>();

        public enum RunStatus
        {
            Success = 0,
            Fail = -1
        }
        #endregion

        #region Singleton Gobble-Di-Goop
        private static SyncController instance;

        // Lock synchronization object
        private static object syncLock = new object();

        /// <summary>
        /// Protected ctor
        /// </summary>
        protected SyncController()
        {
            ReloadClearSync();
        }

        /// <summary>
        /// Get the singleton instance
        /// </summary>
        /// <returns></returns>
        public static SyncController Instance()
        {
            // Support multithreaded applications through
            // 'Double checked locking' pattern which (once
            // the instance exists) avoids locking each
            // time the method is invoked 
            if (instance == null)
            {
                lock (syncLock)
                {
                    if (instance == null)
                    {
                        instance = new SyncController();
                    }
                }
            }

            return instance;
        }
        #endregion

        /// <summary>
        /// Synchronize Clearance with Dss
        /// </summary>
        public void Synchronize()
        {
            // helpful line when user enter number of days to go back
            Console.WriteLine(this.ValidLastPullDate);

            int dssExceptionCount = 0;
            int updatedSubmissionsCount = 0;
            #region Start stopwatch and log
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            LogCurrentTime("Sync Started at");
            #endregion
            
            successStatsList.Add(string.Format("<b>Start Time:</b> {0}", DateTime.Now.ToLocalTime()));

            // get a reference to the dss controller
            DssController dss = DssController.Instance();

            try
            {
                // Load status type
                SubmissionStatus submissionStatus = LoadHistorySubmissionStatus();
                #region Load the Dss entries
                LogCurrentTime("Retrieving DSS entries");
                

                //Get P* Policies
                List<DssEntry> entryList = new List<DssEntry>();
                entryList.AddRange(PStarController.GetEntriesFromPStar(this.ValidLastPullDate.AddDays(-1)));
                
                
                if (!Common.pStarOnly())
                {
                    DssEntry[] dssEntries = dss.GetDssEntries(this.ValidLastPullDate);
                    entryList.AddRange(dssEntries);
                }

                DssEntry[] entries = entryList.ToArray();

                LogCurrentTime(string.Format("Retrieved {0} DSS entries", entries.Length));
                #endregion

                #region code to serialize dss entries to xml for analysis
                if (Properties.Settings.Default.SaveDSSEntries)
                {
                    DssEntry[] toSerialize = entries;
                    System.Xml.Serialization.XmlSerializerNamespaces namespaces = new System.Xml.Serialization.XmlSerializerNamespaces();
                    namespaces.Add(string.Empty, string.Empty);

                    System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(toSerialize.GetType());
                    System.Xml.XmlWriterSettings settings = new System.Xml.XmlWriterSettings();
                    settings.OmitXmlDeclaration = true;
                    //settings.NewLineChars = string.Empty;
                    settings.Encoding = Encoding.UTF8;

                    StringBuilder sb = new StringBuilder();
                    System.Xml.XmlWriter w = System.Xml.XmlWriter.Create(sb, settings);

                    try
                    {
                        x.Serialize(w, toSerialize, namespaces);

                        try
                        {
                            string dirName = "XML";
                            string baseDirPath = Directory.GetCurrentDirectory();
                            string xmlDirPath = string.Format(@"{0}\{1}", baseDirPath, dirName);
                            Console.WriteLine(xmlDirPath);
                            DirectoryInfo xmlDir = null;
                            if (!Directory.Exists(xmlDirPath))
                            {
                                xmlDir = Directory.CreateDirectory(xmlDirPath);
                            }
                            else
                            {
                                xmlDir = Directory.CreateDirectory(xmlDirPath);
                            }

                            string fileName = string.Format("{0}.xml", DateTime.Now.ToString("MMddyyyyHHmmss"));
                            string fullFilePath = string.Format(@"{0}\{1}", xmlDirPath, fileName);
                            Console.WriteLine(fullFilePath);

                            File.AppendAllText(string.Format(@"{0}\{1}", xmlDirPath, fileName), sb.ToString());
                        }
                        catch (IOException ioex)
                        {
                            LogCentral.Current.LogWarn("Exception saving dss entries", ioex);
                        }
                    }
                    catch (Exception ex)
                    {
                        // we will not treat this as show stopper and just log
                        LogCentral.Current.LogWarn("Unable to serialize DSS entries", ex);
                    }
                }
                #endregion

                Console.WriteLine("---- SEARCHING FOR MATCHES ----");
                Console.WriteLine();

                string leadingStripChars = Properties.Settings.Default.LeadingStripChars;
                string trailingStripChars = Properties.Settings.Default.TrailingStripChars;

                #region Search DSS entries and begin the sync process
                int currentDssEntryIndex = 1;
                foreach (DssEntry entry in entries)
                {
                    // display helpful message
                    Console.Write("entry {0} of {1}     ", currentDssEntryIndex, entries.Length);
                    currentDssEntryIndex++;

                    #region performance enhancement when a big load of dss entries. Reloads clear sync in reloadHop multiples
                    // TODO: this can be a app.user setting. 2000 seems optimal
                    int reloadHop = 2000;
                    int remainder = 0;
                    int quotient = Math.DivRem(currentDssEntryIndex, reloadHop, out remainder);

                    if (remainder == 0)
                        ReloadClearSync();

                    #endregion
                    
                    entry.PolicyNumber = entry.PolicyNumber.Remove(0, leadingStripChars.Length);
                    if (!string.IsNullOrEmpty(trailingStripChars))
                    {
                        int lastIndex = entry.PolicyNumber.LastIndexOf(trailingStripChars);
                        if (lastIndex > -1)
                            entry.PolicyNumber = entry.PolicyNumber.Remove(lastIndex);
                    }
                    System.Diagnostics.Debug.WriteLine("Entry for Dss policy #" + entry.PolicyNumber);

                    // grab submissions that match on policy number and effective date
                    SubmissionCollection matchingSubmissions = GetMatchingSubmissions(entry);

                    if (matchingSubmissions.Count == 0)
                    {
                        Console.WriteLine("Missing Policy");
                        dssExceptionCount++;
                        AddDSSExceptionEntry("Missing Policy", entry);
                        continue;
                    }
                    if (matchingSubmissions.Count > 1)
                    {
                        Console.WriteLine("Duplicate Policy");
                        dssExceptionCount++;
                        AddDSSExceptionEntry("Duplicate Policy", entry);
                        continue;
                    }

                    foreach (Submission submission in matchingSubmissions)
                    {
                        Console.WriteLine("---> Updating submission #" + submission.SubmissionNumber);

                        // exp_date, premium and status
                        submission.ExpirationDate = entry.ExpirationDate;
                        submission.EstimatedWrittenPremium = (SqlMoney)entry.Premium;

                        submission.UpdatedBy = Properties.Settings.Default.UpdatedBy;
                        submission.UpdatedDt = DateTime.Now;

                        #region Cancelled?
                        // if the dss entry has a cancel date on it, we'll go ahead and update the status to cancelled.
                        // In addition, a submission status history enter will also be created.
                        if ((entry.CancelTerminationDate > SqlDateTime.MinValue.Value && entry.PolicyStatus == Status.Cancelled) || entry.CancelPendingAuditFlag)
                        {
                            if (submission.CancellationDate.IsNull)
                            {
                                if (entry.CancelPendingAuditFlag && entry.StatusEffectiveDate != null)
                                    submission.CancellationDate = entry.StatusEffectiveDate;
                                else
                                    submission.CancellationDate = entry.CancelTerminationDate;
                            }

                            // we only want to update if the submission is not already cancelled
                            if (submission.SubmissionStatusId != Properties.Settings.Default.CancelledSubmissionStatusId
                                    || submission.SubmissionStatusId.IsNull)
                            {
                                Console.WriteLine("     (CANCELLED: Updating status.)");

                                // set status to cancelled and update the status reason to the date specified in dss
                                submission.SubmissionStatusId = Properties.Settings.Default.CancelledSubmissionStatusId;
                                submission.SubmissionStatusDate = entry.TransactionProcessingDate;

                                if (entry.CancelPendingAuditFlag && entry.StatusEffectiveDate != null)
                                    submission.CancellationDate = entry.StatusEffectiveDate;
                                else
                                    submission.CancellationDate = entry.CancelTerminationDate;                                

                                try
                                {
                                    SubmissionStatusHistory submissionStatusHistory = this._dm.NewSubmissionStatusHistory(
                                         submission.SubmissionStatusId, Properties.Settings.Default.UpdatedBy, submission);
                                    submissionStatusHistory.SubmissionStatusDate = entry.TransactionProcessingDate;
                                    submissionStatusHistory.StatusChangeDt = DateTime.Now;
                                    submissionStatusHistory.StatusChangeBy = Properties.Settings.Default.UpdatedBy;

                                }
                                catch (Exception ex)
                                {
                                    string warningString = string.Format("Could not add history entry for canceled submission #{0} sequence #{1}",
                                        submission.SubmissionNumber, submission.Sequence);
                                    LogCentral.Current.LogFatal(warningString, ex);
                                    warningList.Add(warningString);
                                }

                                if (submission.Comments != string.Empty)
                                {
                                    submission.Comments += "\n";
                                }
                                submission.Comments += "Dss cancel date of " + entry.CancelTerminationDate.ToString();
                            }
                        }
                        #endregion
                        #region Reverted from Cancel
                        // we only want to update if the submission is not already issued and was previously canceled
                        else if ((!submission.SubmissionStatusId.IsNull && submission.SubmissionStatusId.Value == Properties.Settings.Default.CancelledSubmissionStatusId) 
                                    && (entry.PolicyStatus == Status.Active || entry.PolicyStatus == Status.Reinstated || !entry.CancelPendingAuditFlag))
 
                        {
                            Console.WriteLine("     (REINISTATED: Updating status.)");
                            submission.SubmissionStatusId = Properties.Settings.Default.IssuedSubmissionStatusId;                            
                            submission.CancellationDate = SqlDateTime.Null;

                            try
                            {
                                SubmissionStatusHistory submissionStatusHistory = this._dm.NewSubmissionStatusHistory(
                                     submission.SubmissionStatusId, Properties.Settings.Default.UpdatedBy, submission);
                                submissionStatusHistory.SubmissionStatusDate = entry.TransactionProcessingDate;
                                submissionStatusHistory.StatusChangeDt = DateTime.Now;
                                submissionStatusHistory.StatusChangeBy = Properties.Settings.Default.UpdatedBy;
                            }
                            catch (Exception ex)
                            {
                                string warningString = string.Format("Could not add history entry for reinistated submission #{0} sequence #{1}",
                                        submission.SubmissionNumber, submission.Sequence);
                                LogCentral.Current.LogFatal(warningString, ex);
                                warningList.Add(warningString);
                            }
                        }

                        #endregion

                        try
                        {
                            this._dm.CommitAll();
                            updatedSubmissionsCount++;
                        }
                        catch (Exception ex)
                        {
                            string warningString = string.Format("Unable to update submission #{0} sequence #{1}",
                                    submission.SubmissionNumber, submission.Sequence);
                            LogCentral.Current.LogFatal(warningString, ex);
                            warningList.Add(warningString);
                        }
                    }
                }
                #endregion

                // complete
                this.UpdateLastRunStatus(RunStatus.Success);

                #region Stop stopwatch and log
                stopwatch.Stop();
                TimeSpan ts = stopwatch.Elapsed;
                LogCentral.Current.LogInfo(String.Format("{0}s - {1}m - {2}h", Math.Round(ts.TotalSeconds, 2), Math.Round(ts.TotalMinutes, 2), Math.Round(ts.TotalHours, 2)));
                LogCurrentTime("Sync ended at");
                #endregion

                successStatsList.Add(string.Format("<b>End Time:</b> {0}", DateTime.Now.ToLocalTime()));


                successStatsList.Add(string.Format("<b># of Records from DSS:</b> {0}", entries.Length));
                successStatsList.Add(string.Format("<b># of Submissions Updated:</b> {0}", updatedSubmissionsCount));
                successStatsList.Add(string.Format("<b># of DSS Exceptions Logged:</b> {0}", dssExceptionCount));


                // indicating overall success
                if ((SyncController.RunStatus)this.ClearSync.LastRunStatus.Value == RunStatus.Success)
                {
                    // finally if warnings exist 
                    if (warningList.Count > 0)
                        Common.SendEmail(Common.EmailType.Warning, warningList);
                    else
                    {
                        Common.SendEmail(Common.EmailType.Success, successStatsList);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorString = string.Format("Synchronization failed with the following message: {0}", ex.Message);
                LogCentral.Current.LogFatal(errorString, ex);
                List<string> errorList = new List<string>();
                errorList.Clear();
                errorList.Add(errorString);
                Common.SendEmail(Common.EmailType.Failure, errorList);
                this.UpdateLastRunStatus(RunStatus.Fail);
            }
        }

        private void AddDSSExceptionEntry(string entryReason, DssEntry dssentry)
        {
            DSSException excentry = this._dm.NewDSSException(DateTime.Now, entryReason);
            excentry.CancellationDate = dssentry.CancelTerminationDate > SqlDateTime.MinValue.Value ? dssentry.CancelTerminationDate : SqlDateTime.Null;
            excentry.EffectiveDate = dssentry.EffectiveDate;
            excentry.EstimatedWrittenPremium = (decimal)dssentry.Premium;
            excentry.ExpirationDate = dssentry.ExpirationDate > SqlDateTime.MinValue.Value ? dssentry.ExpirationDate : SqlDateTime.Null;
            excentry.PolicyMod = dssentry.PolicyMod;
            excentry.PolicyNumber = dssentry.PolicyNumber;

            try
            {
                this._dm.CommitAll();
            }
            catch (Exception ex)
            {
                string warningString = string.Format(
                    "Error adding DSS Entry (policy number - {0}, effective date - {1}, exception reason - {2}) to the DSS Exception table. ",
                                       dssentry.PolicyNumber, dssentry.EffectiveDate, entryReason);
                LogCentral.Current.LogFatal(warningString, ex);
                warningList.Add(warningString);
            }
        }

        private void LogCurrentTime(string p)
        {
            LogCentral.Current.LogInfo(string.Format("{0} : {1}", p, DateTime.Now.ToString()));
        }

        #region Get matching submissions
        /// <summary>
        /// Provides the ability to retrive a submission collection object.
        /// Currently using PolicyNumber and EffectiveDate as the match.
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        public SubmissionCollection GetMatchingSubmissions(DssEntry entry)
        {
            this._dm.QueryCriteria.Clear();

            this._dm.QueryCriteria.And(Biz.JoinPath.Submission.CompanyNumber.Columns.PropertyCompanyNumber, Properties.Settings.Default.CompanyNumber);
            this._dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.PolicyNumber, entry.PolicyNumber);
            this._dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.EffectiveDate, entry.EffectiveDate);

            return this._dm.GetSubmissionCollection();
        }
        #endregion

        #region Get cancelled submission status object
        /// <summary>
        /// Provide a submission status object. Loads via the Id specified in the configuration file.
        /// </summary>
        /// <returns></returns>
        public SubmissionStatus LoadHistorySubmissionStatus()
        {
            this._dm.QueryCriteria.Clear();

            this._dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatus.Columns.Id, Properties.Settings.Default.CancelledSubmissionStatusId);

            return this._dm.GetSubmissionStatus();
        }
        #endregion

        #region Utility Status Functions
        /// <summary>
        /// Private method used to load the ClearSync configuration object
        /// </summary>
        private void ReloadClearSync()
        {
            try
            {
                this._dm = Common.GetDataManager();                
            }
            catch (Exception ex)
            {
                string errorString = string.Format("Unable to Initialize Data Manager using the connection string provided.");
                LogCentral.Current.LogFatal(errorString, ex);
                List<string> errorList = new List<string>();
                errorList.Clear();
                errorList.Add(errorString);
                Common.SendEmail(Common.EmailType.Failure, errorList);

                throw ex;
            }

            this._dm.QueryCriteria.Clear();
            this._dm.QueryCriteria.And(Biz.JoinPath.ClearSync.Columns.Name, Properties.Settings.Default.Identifier);
            this._dm.QueryCriteria.And(Biz.JoinPath.ClearSync.CompanyNumber.Columns.PropertyCompanyNumber, Properties.Settings.Default.CompanyNumber);

            if (this._dm.GetClearSyncCollection()[0] == null)
            {
                throw new ApplicationException("Could not initialize ClearSync configuration for company number " + Properties.Settings.Default.CompanyNumber);
            }

            this._csc = this._dm.GetClearSyncCollection();
        }

        /// <summary>
        /// Update the last run date to now.
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use UpdateLastRunStatus instead.", true)]
        public bool UpdateLastRunDate()
        {
            bool success = true;

            this._csc[0].LastRunDate = DateTime.Now;

            try
            {
                this._dm.CommitAll();
                success = true;

                ReloadClearSync();
            }
            catch (Exception ex)
            {
                string warningString = string.Format("Unable to update last run date for company number {0}", Properties.Settings.Default.CompanyNumber);
                LogCentral.Current.LogFatal(warningString, ex);
                warningList.Add(warningString);
                success = false;
            }

            return success;
        }

        /// <summary>
        /// Update last run status and date
        /// </summary>
        /// <param name="runStatus"></param>
        /// <returns></returns>
        public bool UpdateLastRunStatus(RunStatus runStatus)
        {
            bool success = true;

            this._csc[0].LastRunStatus = (int)runStatus;
            if (runStatus == RunStatus.Success)
                this._csc[0].LastRunDate = DateTime.Now;

            try
            {
                this._dm.CommitAll();
                success = true;

                ReloadClearSync();
            }
            catch (Exception ex)
            {
                string warningString = string.Format("Unable to update last run status for company number {0}", Properties.Settings.Default.CompanyNumber);
                LogCentral.Current.LogFatal(warningString, ex);
                warningList.Add(warningString);
                success = false;
            }

            return success;
        }
        #endregion
    }
}