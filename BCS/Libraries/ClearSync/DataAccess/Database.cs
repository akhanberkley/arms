using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Text;

using BTS.LogFramework;
using IBM.Data.Informix;
using System.Data.OleDb;

namespace BCS.ClearSync.DataAccess
{
    public class Database
    {
        /// <summary>
        /// Private constructor
        /// </summary>
        private Database() { }

        #region Ifx
        /// <summary>
        /// Get an open IfxConnection object
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static IfxConnection GetOpenIfxConnection(string connectionString)
        {
            IfxConnection conn = null;

            try
            {
                conn = new IfxConnection(connectionString);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Count not open connection: " + ex.Message);
            }

            return conn;
        }

        /// <summary>
        /// Close a IfxConnection
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        internal static bool CloseOpenIfxConnection(IfxConnection conn)
        {
            bool isClosed = true;

            try
            {
                // Don't forget:
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Count not close connection: " + ex.Message);
                isClosed = false;
            }

            return isClosed;
        } 
        #endregion

        #region OleDb
        internal static OleDbConnection GetOpenOleDbConnection(string connectionString)
        {
            OleDbConnection conn = null;

            try
            {
                conn = new OleDbConnection(connectionString);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Count not open connection: " + ex.Message);
            }

            return conn;
        }

        internal static bool CloseOpenOleDbConnection(OleDbConnection conn)
        {
            bool isClosed = true;

            try
            {
                // Don't forget:
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                LogCentral.Current.LogFatal("Count not close connection: " + ex.Message);
                isClosed = false;
            }

            return isClosed;
        } 
        #endregion
    } 
}
