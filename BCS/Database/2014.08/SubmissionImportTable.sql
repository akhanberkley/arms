IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'ImportSubmission')
BEGIN
	CREATE TABLE dbo.ImportSubmission
		(
		[Id] [int] IDENTITY(1,1) NOT NULL,
		BatchId uniqueidentifier NOT NULL,
		CompanyNumberId int NOT NULL,
		[ClientCoreClientId] bigint NOT NULL,
		SubmissionType varchar(50) NOT NULL,
		[SubmissionStatus] varchar(100) NOT NULL,
		[SubmissionStatusReason] varchar(50) NULL,
		[SubmissionStatusReasonOther] varchar(255) NULL,
		[SubmissionNumber] bigint NULL,
		[Sequence] int NULL,
		[PolicyNumber] varchar(50) NULL,
		[EffectiveDate] datetime NULL,
		[ExpirationDate] datetime NULL,
		InsuredName varchar(255) NULL,
		[AgencyNumber] varchar(15) NULL,
		[Agent] varchar(100) NULL,
		UnderwritingUnit varchar(100) NULL,
		Underwriter varchar(100) NULL,
		Analyst varchar(100) NULL,
		Technician varchar(100) NULL,
		[EstimatedWrittenPremium] money NULL,
		[SubmissionDt] datetime NULL,
		CompanyCustomizations XML NULL,
		CreatedSubmissionNumber varchar(20) NULL,
		ErrorMessage varchar(255) NULL
	  CONSTRAINT [PK_ImportSubmission] PRIMARY KEY CLUSTERED 
		(
			[Id]
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.ImportSubmission ADD CONSTRAINT FK_ImportSubmission_CompanyNumber FOREIGN KEY (CompanyNumberId) REFERENCES dbo.CompanyNumber (Id)
END