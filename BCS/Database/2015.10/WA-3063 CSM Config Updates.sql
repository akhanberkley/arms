--UA is required
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET [Required] = 1 WHERE CompanyNumberId = 20 AND SubmissionFieldId = 'D0845F70-535C-41B7-A1F8-0A96574F43A6'

--Remove class code
UPDATE [dbo].[CompanyNumberSubmissionField] SET Enabled = 0 WHERE submissionfieldid = '26DCA514-74C2-4E4E-9D35-EC120B468C32' and CompanyNumberId = 20
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE submissionfieldid = '26DCA514-74C2-4E4E-9D35-EC120B468C32' and CompanyNumberId = 20

--Hide policy # on submission creation
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET VisibleOnAdd = 0 WHERE CompanyNumberId = 20 AND SubmissionFieldId = '7BC42623-1667-473C-AA99-16630327A75D'