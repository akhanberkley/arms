IF NOT EXISTS(SELECT * FROM [dbo].[NumberControl] WHERE [CompanyNumberId] = 8 AND ControlNumber >= 4339699)
	UPDATE [dbo].[NumberControl] SET [ControlNumber] = 4339699 WHERE [CompanyNumberId] = 8 AND [NumberControlTypeId] = 1

--Policy Number field should be required
UPDATE [dbo].[Company] SET [RequiresPolicyNumber] = 1 WHERE Id = 4
UPDATE [dbo].[CompanyNumberSubmissionFieldSubmissionType] SET Required = 1 WHERE SubmissionFieldId = '7BC42623-1667-473C-AA99-16630327A75D' AND CompanyNumberId = 8