DECLARE @ReservedStatusId int, @ExpiredStatusId int
SELECT @ReservedStatusId = Id FROM dbo.SubmissionStatus WHERE StatusCode = 'Reserved'
SELECT @ExpiredStatusId = Id FROM dbo.SubmissionStatus WHERE StatusCode = 'Expired'
IF NOT EXISTS(SELECT * FROM [dbo].[CompanyNumberSubmissionStatus] WHERE CompanyNumberId = 8 AND SubmissionStatusId = @ReservedStatusId)
BEGIN
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (8,@ReservedStatusId,0,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (15,@ReservedStatusId,0,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (25,@ReservedStatusId,0,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (35,@ReservedStatusId,0,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (36,@ReservedStatusId,0,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (38,@ReservedStatusId,0,0,0,1,NULL)

	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (6,@ExpiredStatusId,130,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (8,@ExpiredStatusId,40,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (15,@ExpiredStatusId,30,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (25,@ExpiredStatusId,40,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (35,@ExpiredStatusId,40,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (36,@ExpiredStatusId,35,0,0,1,NULL)
	INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable],[DaysStatusIsActive])
        VALUES (38,@ExpiredStatusId,30,0,0,1,NULL)
END