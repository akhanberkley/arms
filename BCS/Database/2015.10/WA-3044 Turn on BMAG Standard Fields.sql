--Enable the acquiring carrier/prior carrier fields
UPDATE [dbo].[CompanyNumberSubmissionField] SET [Enabled] = 1 WHERE CompanyNumberId = 8 AND SubmissionFieldId IN ('FF0807F4-3009-43D3-8A3A-C24811F9B648','76888306-BE7B-4F44-BA99-C860D3DEADFB','ABBDD884-BAAD-4164-9814-80D631B6E001','B5D40301-24AB-4694-915B-51DFFAB51221')
--Add the rows that specify how these should look
IF NOT EXISTS(select * from [dbo].[CompanyNumberSubmissionFieldSubmissionType] WHERE CompanyNumberId = 8 AND SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648')
BEGIN
	INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'FF0807F4-3009-43D3-8A3A-C24811F9B648', 0, 1 FROM [dbo].[CompanyNumberSubmissionType] WHERE CompanyNumberId = 8--PriorCarrier
	INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '76888306-BE7B-4F44-BA99-C860D3DEADFB', 0, 1 FROM [dbo].[CompanyNumberSubmissionType] WHERE CompanyNumberId = 8--PriorCarrierPremium
	INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'ABBDD884-BAAD-4164-9814-80D631B6E001', 0, 0 FROM [dbo].[CompanyNumberSubmissionType] WHERE CompanyNumberId = 8--AcquiringCarrier
	INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'B5D40301-24AB-4694-915B-51DFFAB51221', 0, 0 FROM [dbo].[CompanyNumberSubmissionType] WHERE CompanyNumberId = 8--AcquiringCarrierPremium

	INSERT INTO [dbo].[CompanyNumberOtherCarrier] ([CompanyNumberId],[OtherCarrierId],[Order],[DefaultSelection])
		SELECT
			8
			,Id
			,0
			,0
		FROM
			[dbo].[OtherCarrier]
END