IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'CmsClientId' AND Object_ID = Object_ID(N'Client'))
BEGIN
	ALTER TABLE [dbo].[Client] ADD CmsClientId uniqueidentifier NULL
	CREATE NONCLUSTERED INDEX [IX_Client_CompanyNumberCmsClientId] ON [dbo].[Client] ([CompanyNumberId],[CmsClientId]) INCLUDE ([ClientCoreClientId])
END