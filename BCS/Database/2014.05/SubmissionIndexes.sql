IF NOT EXISTS(SELECT * FROM sys.indexes  WHERE name='IX_Submission_UpdatedBy_SubmissionBy' AND object_id = OBJECT_ID('Submission'))
	CREATE NONCLUSTERED INDEX [IX_Submission_UpdatedBy_SubmissionBy] ON [dbo].[Submission]
	(
		[CompanyNumberId] ASC,
		[UpdatedBy] ASC,
		[SubmissionBy] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]