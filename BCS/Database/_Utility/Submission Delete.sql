DECLARE @SubmissionId INT = 4035466

DELETE FROM dbo.submissionhistory where submissionid = @SubmissionId
DELETE FROM [dbo].[SubmissionClientCoreClientName] where submissionid = @SubmissionId
DELETE FROM [dbo].[SubmissionClientCoreClientAddress] where submissionid = @SubmissionId
DELETE FROM [dbo].[SubmissionLineOfBusiness] where submissionid = @SubmissionId
DELETE FROM [dbo].[SubmissionCompanyNumberAttributeValue] where submissionid = @SubmissionId
DELETE FROM [dbo].[SubmissionStatusHistory] where submissionid = @SubmissionId

ALTER TABLE [dbo].[SubmissionCompanyNumberCode] DISABLE TRIGGER [Delete_SubmissionCompanyNumberCodeHistory]
DELETE FROM [dbo].[SubmissionCompanyNumberCode] where submissionid = @SubmissionId
ALTER TABLE [dbo].[SubmissionCompanyNumberCode] ENABLE TRIGGER [Delete_SubmissionCompanyNumberCodeHistory]

DELETE FROM dbo.submission where id = @SubmissionId