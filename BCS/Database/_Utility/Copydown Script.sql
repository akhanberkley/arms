UPDATE
	dbo.companyparameter
SET
	MQPort = 1416
	,MQHostName = 'WMQDWS01'
	,MQManager = 'BTSU01I'
WHERE
	MQName IS NOT NULL

UPDATE dbo.companyparameter SET APSService = 'http://cwg-int/apsws/services/APSService' WHERE APSService = 'http://cwg-prd/apsws/services/APSService'
UPDATE dbo.companyparameter SET APSService = 'http://aic-int/apsws/services/APSService' WHERE APSService = 'http://aic-tst/apsws/services/APSService'
UPDATE dbo.companyparameter SET APSService = 'http://bmg-int/apsws/services/APSService' WHERE APSService = 'http://bmg-prd/apsws/services/APSService'
UPDATE dbo.companyparameter SET APSService = 'http://usg-int/apsws/services/APSService' WHERE APSService = 'http://usg-tst/apsws/services/APSService'
UPDATE dbo.companyparameter SET APSService = 'http://csm-int/apsws/services/APSService' WHERE APSService = 'http://csm-tst/apsws/services/APSService'
UPDATE dbo.companyparameter SET APSService = 'http://brs-int/apsws/services/APSService' WHERE APSService = 'http://brs-tst/apsws/services/APSService'
UPDATE dbo.companyparameter SET APSService = 'http://bls-int/apsws/services/APSService' WHERE APSService = 'http://bls-tst/apsws/services/APSService'
UPDATE dbo.companyparameter SET APSService = 'http://bnp-int/apsws/services/APSService' WHERE APSService = 'http://bnp-prd/apsws/services/APSService'
UPDATE dbo.companyparameter SET APSService = 'http://btu-int/apsws/services/APSService' WHERE APSService = 'http://btu-tst/apsws/services/APSService'
UPDATE dbo.companyparameter SET APSService = 'http://bfm-int/apsws/services/APSService' WHERE APSService = 'http://bfm-tst/apsws/services/APSService'

--UPDATE dbo.Person SET APSId = null
UPDATE dbo.Agency SET APSId = null
UPDATE dbo.Agent SET APSId = null, APSPersonnelId = null
UPDATE dbo.Contact SET APSId = null
UPDATE dbo.AgencyBranch SET APSId = null
UPDATE dbo.AgencyLicensedState SET APSId = null
UPDATE dbo.AgencyLineOfBusiness SET APSId = null
UPDATE [dbo].[AgencyLineOfBusinessPersonUnderwritingRolePerson] SET APSId = null

DELETE FROM [dbo].[APSSync]
DELETE FROM [dbo].[APSXMLLog]