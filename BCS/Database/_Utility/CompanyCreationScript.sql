DECLARE @NewCompanyId int = null, @NewCompanyNumberId int = null
DECLARE @CompanyName varchar(50) = 'Berkley Program Specialists'
DECLARE @CompanyAbbreviation varchar(50) = 'BPS'
DECLARE @CompanyNumber varchar(5) = '999'
DECLARE @CompanyAbbreviationToCopyFrom varchar(50) = 'AIC'
DECLARE @CompanyIdToCopyFrom int = 3
DECLARE @CompanyNumberIdToCopyFrom int = 6

SELECT @NewCompanyId = Id FROM dbo.Company WHERE [CompanyInitials] = @CompanyAbbreviation

IF @NewCompanyId IS NULL
BEGIN
	INSERT INTO [dbo].[Company]
			   ([CompanyName],[CompanyInitials],[AllowSubmissionsOnError],[ClientSystemTypeId],[DSIK],[ClientCoreEndPointUrl],[ClientCoreSystemCd],[ClientCoreUsername],[ClientCorePassword],[ClientCoreLocalId],[ClientCoreCorpId],[ClientCoreCorpCode],[ClientCoreOwner],[ClientCoreClientType],[UnderwriterRoleId],[AnalystRoleId],[TechnicianRoleId],[LogoUrl],[ThemeStyleSheet],[JavascriptFile],[SearchFieldOrder],[SubmissionScreenFieldOrder],[SubmissionScreenCodeTypeOrder],[SubmissionScreenAttributeOrder],[ClientScreenFieldOrder],[ClientScreenCodeTypeOrder],[ClientScreenAttributeOrder],[SupportsIndividualClients],[SupportsInternationalAddresses],[SupportsClassCodes],[SupportsDuplicates],[EnforceAgencyLicensedState],[LOBGridUpdateStatuses],[LOBGridUpdateExcludedStatuses],[SupportsMultipleLineOfBusinesses],[SupportsAgentUnspecifiedReasons],[RequiresAddressType],[RequiresNameType],[DesiresInsuredAutoSelectWhenOnlyOne],[DesiresDBAAutoSelectWhenOnlyOne],[SearchOnPolicyNumberOrSubmissonNumber],[FillAllUnderwriters],[DesiresDefaultStatusOnSubmissionCopy],[RequiresPolicyNumber],[GenerateOnlyPolicyNumber],[FreehandOnlyPolicyNumber],[SupportsShellSubmission],[UsesAPS],[APSURL],[APSDSIK],[AgenciesInGrid],[AutoSelectNamesOnSubmissionAdd],[PolicyNumberRegex],[LoadClientBeforeStep],[AgencyPortMatchLength],[AllowClientPrimaryChanges],Active)
		 SELECT
			   @CompanyName
			   ,@CompanyAbbreviation
			   ,AllowSubmissionsOnError,ClientSystemTypeId,DSIK
			   ,REPLACE(ClientCoreEndPointUrl COLLATE Latin1_General_BIN, LOWER(CompanyInitials), LOWER(@CompanyAbbreviation))
			   ,ClientCoreSystemCd,ClientCoreUsername,ClientCorePassword,ClientCoreLocalId,ClientCoreCorpId,ClientCoreCorpCode,ClientCoreOwner,ClientCoreClientType,UnderwriterRoleId,AnalystRoleId,TechnicianRoleId,LogoUrl
			   ,REPLACE(LOWER(ThemeStyleSheet COLLATE Latin1_General_BIN), LOWER(CompanyInitials), @CompanyAbbreviation)
			   ,JavascriptFile,SearchFieldOrder,SubmissionScreenFieldOrder,SubmissionScreenCodeTypeOrder,SubmissionScreenAttributeOrder,ClientScreenFieldOrder,ClientScreenCodeTypeOrder,ClientScreenAttributeOrder,SupportsIndividualClients,SupportsInternationalAddresses,SupportsClassCodes,SupportsDuplicates,EnforceAgencyLicensedState,LOBGridUpdateStatuses,LOBGridUpdateExcludedStatuses,SupportsMultipleLineOfBusinesses,SupportsAgentUnspecifiedReasons,RequiresAddressType,RequiresNameType,DesiresInsuredAutoSelectWhenOnlyOne,DesiresDBAAutoSelectWhenOnlyOne,SearchOnPolicyNumberOrSubmissonNumber,FillAllUnderwriters,DesiresDefaultStatusOnSubmissionCopy,RequiresPolicyNumber,GenerateOnlyPolicyNumber,FreehandOnlyPolicyNumber,SupportsShellSubmission,UsesAPS,APSURL,APSDSIK,AgenciesInGrid,AutoSelectNamesOnSubmissionAdd,PolicyNumberRegex,LoadClientBeforeStep,AgencyPortMatchLength,AllowClientPrimaryChanges,Active
	FROM
		dbo.Company
	WHERE
		Id = @CompanyIdToCopyFrom

	SET @NewCompanyId = @@Identity
END

IF NOT EXISTS(SELECT * FROM [dbo].[CompanyParameter] WHERE CompanyId = @NewCompanyId)
	INSERT INTO [dbo].[CompanyParameter]
			   ([CompanyID],[SearchFilter],[SearchFilterService],[SearchFilterServiceDescription],[FuzzySearchEnabled],[PolicyValidation],[AlwaysDisplayPortfolio],[SubmissionDisplayYears],[DietSearch],[DietSearchOptions],[UWReferralAddressIndicator],[RequiresUW],[RequiresUWAssistant],[ClearUWOnLOBChange],[FilterPerformClientSearch],[FilterGetClientInfo],[PrimaryUWRole],[PrimaryANRole],[UseAPSForAssignments],[FillAllAssignments],[RequiresInsuredOrDBA],[APSService],[MQName],[MQHostName],[MQChannel],[MQPort],[FilterInactiveStatusReasons],[CopySubmissionStatusOption],[SubmissionStatusDateTBD],[EnsureDefaultStatusOnAdd],[UsesFAQ],[LinkAfterSubStatus],[ClientTypeForSearch],[SegmentWithGetClientInfo],[OptionToViewDeletedSubmissions],[UseAPSNewBusinessCancelDate],[UseAPSRenewalCancelDate],[DefaultAPSAssignments], MQManager, APSAgencyUrl, DuplicateSubmissionQuery)
		SELECT
			   @NewCompanyId
			   ,SearchFilter,SearchFilterService,SearchFilterServiceDescription,FuzzySearchEnabled,PolicyValidation,AlwaysDisplayPortfolio,SubmissionDisplayYears,DietSearch,DietSearchOptions,UWReferralAddressIndicator,RequiresUW,RequiresUWAssistant,ClearUWOnLOBChange,FilterPerformClientSearch,FilterGetClientInfo,PrimaryUWRole,PrimaryANRole,UseAPSForAssignments,FillAllAssignments,RequiresInsuredOrDBA
			   ,REPLACE(APSService COLLATE Latin1_General_BIN, @CompanyAbbreviationToCopyFrom, @CompanyAbbreviation)
			   ,REPLACE(UPPER(MQName COLLATE Latin1_General_BIN), UPPER(@CompanyAbbreviationToCopyFrom), @CompanyAbbreviation)
			   ,MQHostName,MQChannel,MQPort,FilterInactiveStatusReasons,CopySubmissionStatusOption,SubmissionStatusDateTBD,EnsureDefaultStatusOnAdd,UsesFAQ,LinkAfterSubStatus,ClientTypeForSearch,SegmentWithGetClientInfo,OptionToViewDeletedSubmissions,UseAPSNewBusinessCancelDate,UseAPSRenewalCancelDate,DefaultAPSAssignments,MQManager
			   ,REPLACE(LOWER(APSAgencyUrl COLLATE Latin1_General_BIN), LOWER(@CompanyAbbreviationToCopyFrom), LOWER(@CompanyAbbreviation))
			   ,DuplicateSubmissionQuery
		FROM
			dbo.CompanyParameter
		WHERE
			CompanyId = @CompanyIdToCopyFrom

IF NOT EXISTS(SELECT * FROM [dbo].[CompanyClientAdvancedSearch] WHERE CompanyId = @NewCompanyId)
	INSERT INTO [dbo].[CompanyClientAdvancedSearch]
			   ([CompanyId],[DSIK],[ClientAdvSearchEndPointURL],[UsesReversePhoneLookup],[UsesOfacCheck],[IsPhoneRequiredField],[Username],[Password],[PopulateBusinessName],[PopulateCity],[PopulateState],[UsesOfacSubmissionComment], [ClientExperianSearchEndPointUrl])
		SELECT
			   @NewCompanyId,DSIK,ClientAdvSearchEndPointURL,UsesReversePhoneLookup,UsesOfacCheck,IsPhoneRequiredField,Username,[Password],PopulateBusinessName,PopulateCity,PopulateState,UsesOfacSubmissionComment,ClientExperianSearchEndPointUrl
		FROM
			dbo.[CompanyClientAdvancedSearch]
		WHERE
			CompanyId = @CompanyIdToCopyFrom

--Legacy table from old system
IF NOT EXISTS(SELECT * FROM [dbo].[SubmissionPage] WHERE CompanyId = @NewCompanyId)
	INSERT INTO [dbo].[SubmissionPage]
			   ([CompanyId],[AddInitialFocusField],[EditInitialFocusField],[AddInvisibleFields],[EditInvisibleFields],[AddDisabledFields],[EditDisabledFields],[CopyExcludedFields],[DefaultText],[SnapShotEditable],[RequiresInsuredName])
		SELECT
			@NewCompanyId
			,AddInitialFocusField
			,EditInitialFocusField
			,AddInvisibleFields
			,EditInvisibleFields
			,AddDisabledFields
			,EditDisabledFields
			,CopyExcludedFields
			,DefaultText
			,SnapShotEditable
			,RequiresInsuredName
		FROM
			[dbo].[SubmissionPage]
		WHERE
			CompanyId = @CompanyIdToCopyFrom

SELECT @NewCompanyNumberId = Id FROM dbo.[CompanyNumber] WHERE [CompanyNumber] = @CompanyNumber
IF @NewCompanyNumberId IS NULL
BEGIN
	INSERT INTO [dbo].[CompanyNumber]
			   ([CompanyId],[CompanyNumber])
		 VALUES
			   (@NewCompanyId, @CompanyNumber)

	SET @NewCompanyNumberId = @@Identity
END

IF NOT EXISTS(SELECT * FROM dbo.CompanyNumberSubmissionField WHERE [CompanyNumberId] = @NewCompanyNumberId)
	INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId)
	SELECT [Enabled], @NewCompanyNumberId, SubmissionFieldId FROM dbo.CompanyNumberSubmissionField WHERE CompanyNumberId = @CompanyNumberIdToCopyFrom

IF NOT EXISTS(SELECT * FROM [dbo].[CompanyNumberSubmissionType] WHERE CompanyNumberId = @NewCompanyNumberId)
	INSERT INTO [dbo].[CompanyNumberSubmissionType] ([CompanyNumberId],[SubmissionTypeId],[Order],[DefaultSelection],SummarySubmissionFieldId)
	SELECT @NewCompanyNumberId,[SubmissionTypeId],[Order],[DefaultSelection],SummarySubmissionFieldId FROM [dbo].[CompanyNumberSubmissionType] WHERE CompanyNumberId = @CompanyNumberIdToCopyFrom

IF NOT EXISTS(SELECT * FROM [dbo].[CompanyNumberSubmissionStatus] WHERE CompanyNumberId = @NewCompanyNumberId)
BEGIN
	DECLARE @StatusTypes TABLE (StatusId int null, Code varchar(50), [Order] int, DefaultSelection bit, SubReasonCode varchar(50))
	INSERT INTO @StatusTypes
	SELECT
		NULL
		,ss.StatusCode
		,c.[Order]
		,c.DefaultSelection
		,ssr.ReasonCode
	FROM
		dbo.CompanyNumberSubmissionStatus c
		INNER JOIN dbo.SubmissionStatus ss
			ON c.SubmissionStatusId = ss.Id
		LEFT JOIN [dbo].[SubmissionStatusSubmissionStatusReason] ssssr
			ON ssssr.SubmissionStatusId = ss.Id
			AND ssssr.Active = 1
		LEFT JOIN dbo.SubmissionStatusReason ssr
			ON ssssr.SubmissionStatusReasonId = ssr.Id
	WHERE
		c.CompanyNumberId = @CompanyNumberIdToCopyFrom

	DECLARE @StatusId int, @Code varchar(50), @Order int, @DefaultSelection bit, @SubReasonCode varchar(50)
	DECLARE db_cursor CURSOR FOR  
	select * from @StatusTypes
	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @StatusId, @Code, @Order, @DefaultSelection, @SubReasonCode
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		IF @StatusId IS NULL
		BEGIN
			SELECT @StatusId = s.Id FROM dbo.CompanyNumberSubmissionStatus c INNER JOIN dbo.SubmissionStatus s ON c.SubmissionStatusId = s.Id WHERE c.CompanyNumberId = @NewCompanyNumberId AND s.StatusCode = @Code
		
			IF @StatusId IS NULL
			BEGIN
				INSERT INTO [dbo].[SubmissionStatus] ([StatusCode]) VALUES (@Code)
				SET @StatusId = @@IDENTITY

				INSERT INTO [dbo].[CompanyNumberSubmissionStatus] ([CompanyNumberId],[SubmissionStatusId],[Order],[DefaultSelection],[HierarchicalOrder],[IsClearable])
					VALUES (@NewCompanyNumberId,@StatusId,@Order,@DefaultSelection,0,1)
			END

			UPDATE @StatusTypes SET StatusId = @StatusId WHERE Code = @Code
		END

		IF(@SubReasonCode IS NOT NULL)
			IF NOT EXISTS(SELECT * FROM [dbo].[SubmissionStatusSubmissionStatusReason] sr INNER JOIN [SubmissionStatusReason] r ON sr.SubmissionStatusReasonId = r.Id WHERE sr.SubmissionStatusId = @StatusId AND r.ReasonCode = @SubReasonCode)
			BEGIN
				INSERT INTO [dbo].[SubmissionStatusReason] ([ReasonCode])
					VALUES (@SubReasonCode)
				DECLARE @ReasonId INT = @@IDENTITY

				INSERT INTO [dbo].[SubmissionStatusSubmissionStatusReason] ([SubmissionStatusId],[SubmissionStatusReasonId],[Active])
					VALUES (@StatusId,@ReasonId,1)
			END

		FETCH NEXT FROM db_cursor INTO @StatusId, @Code, @Order, @DefaultSelection, @SubReasonCode
	END   
	CLOSE db_cursor   
	DEALLOCATE db_cursor
END


IF NOT EXISTS(SELECT * FROM [dbo].[SubmissionNumberControl] WHERE [CompanyNumberId] = @NewCompanyNumberId)
	INSERT INTO [dbo].[SubmissionNumberControl] ([CompanyNumberId] ,[ControlNumber])
		 VALUES (@NewCompanyNumberId, 10000)

IF NOT EXISTS(SELECT * FROM [dbo].[NumberControl] WHERE [CompanyNumberId] = @NewCompanyNumberId)
	INSERT INTO [dbo].[NumberControl] ([CompanyNumberId],[NumberControlTypeId],[ControlNumber],[PaddingChar],[PadHead],[TotalLength])
     VALUES (@NewCompanyNumberId,1,1000000,NULL,0,0)

IF NOT EXISTS(SELECT * FROM [dbo].[CompanyNumberUnderwritingRole] WHERE [CompanyNumberId] = @NewCompanyNumberId)
	INSERT INTO [dbo].[CompanyNumberUnderwritingRole]
	SELECT
		@NewCompanyNumberId
		,UnderwritingRoleId
	FROM
		[dbo].[CompanyNumberUnderwritingRole]
	WHERE
		CompanyNumberId = @CompanyNumberIdToCopyFrom

--Legacy Table
IF NOT EXISTS(SELECT * FROM [dbo].[SubmissionDisplayGrid] WHERE [CompanyNumberId] = @NewCompanyNumberId)
	INSERT INTO [dbo].[SubmissionDisplayGrid] ([CompanyNumberId],[AttributeDataTypeId],[FieldName],[HeaderText],[SupportField],[DisplayOrder],[SortOrder],[SortDirection],[RemoteAgencyVisible],[DisplayOnClientSearchScreen],[DisplayOnSubmissionSearchScreen],[DisplayOnSubmissionScreen])
		 SELECT
			   @NewCompanyNumberId
			   ,AttributeDataTypeId
			   ,FieldName
			   ,HeaderText
			   ,SupportField
			   ,DisplayOrder
			   ,SortOrder
			   ,SortDirection
			   ,RemoteAgencyVisible
			   ,DisplayOnClientSearchScreen
			   ,DisplayOnSubmissionSearchScreen
			   ,DisplayOnSubmissionScreen
		FROM
			[dbo].[SubmissionDisplayGrid]
		WHERE
			CompanyNumberId = @CompanyNumberIdToCopyFrom

IF NOT EXISTS(SELECT * FROM dbo.SubmissionViewField WHERE [CompanyNumberId] = @NewCompanyNumberId)
	INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT @NewCompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence FROM dbo.SubmissionViewField WHERE CompanyNumberId = @CompanyNumberIdToCopyFrom

IF NOT EXISTS(SELECT * FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE [CompanyNumberId] = @NewCompanyNumberId)
	INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd)
	SELECT SubmissionTypeId, @NewCompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE CompanyNumberId = @CompanyNumberIdToCopyFrom

IF NOT EXISTS(SELECT * FROM dbo.CompanyNumberClientField WHERE [CompanyNumberId] = @NewCompanyNumberId)
	INSERT INTO dbo.CompanyNumberClientField (CompanyNumberId, ClientFieldId, [Required], VisibleOnAdd)
	SELECT @NewCompanyNumberId, ClientFieldId, [Required], VisibleOnAdd FROM dbo.CompanyNumberClientField WHERE CompanyNumberId = @CompanyNumberIdToCopyFrom