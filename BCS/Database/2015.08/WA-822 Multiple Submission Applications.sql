IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = 'D2386053-4423-428F-A828-81481008440D')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('D2386053-4423-428F-A828-81481008440D', 'AllowMultipleSubmissions', 'AllowMultipleSubmissions', 'AllowMultipleSubmissions')

	--Disabling for BNPG & CSM for now
	INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) 
	SELECT CASE WHEN Id IN (35,20) THEN 0 ELSE 1 END, Id, 'D2386053-4423-428F-A828-81481008440D' FROM dbo.CompanyNumber

	INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) 
	SELECT 
		cnst.SubmissionTypeId
		,cnst.CompanyNumberId
		,'D2386053-4423-428F-A828-81481008440D'
		,1
		,1 
	FROM 
		dbo.CompanyNumberSubmissionField cnsf
		INNER JOIN [dbo].[CompanyNumberSubmissionType] cnst
			ON cnsf.CompanyNumberId = cnst.CompanyNumberId
	WHERE
		cnsf.SubmissionFieldId = 'D2386053-4423-428F-A828-81481008440D'
		AND cnsf.[Enabled] = 1
END

UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET [Required] = 0 WHERE SubmissionFieldId = 'D2386053-4423-428F-A828-81481008440D'