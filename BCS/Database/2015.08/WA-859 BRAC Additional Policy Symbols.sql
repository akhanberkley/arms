DECLARE @CompanyId int = 28
DECLARE @CompanyNumberId int = 40

--Setup New Policy Symbols
IF NOT EXISTS(SELECT * FROM [dbo].[PolicySymbol] WHERE CompanyNumberId = @CompanyNumberId AND Code = 'PCA')
BEGIN
	DECLARE @NumberControlTypeId INT;
	SELECT @NumberControlTypeId = Id FROM [dbo].[NumberControlType] WHERE TypeName = 'Policy Number by Policy Symbol'
	INSERT INTO [dbo].[NumberControl] ([CompanyNumberId],[NumberControlTypeId],[ControlNumber],[PaddingChar],[PadHead],[TotalLength])
		 VALUES (@CompanyNumberId, @NumberControlTypeId, 9500000, NULL, 0, 0)

	DECLARE @NumberControlId INT
	SELECT @NumberControlId = @@IDENTITY

	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('8AF66002-0F1F-42CF-85BA-AB6A8AE996A0', @CompanyNumberId, 'PCA', 'Commercial Auto', NULL, 1, 0, @NumberControlId)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('88FEB721-C604-4681-843E-FDF4D5361B64', @CompanyNumberId, 'PCF', 'Commercial Property', NULL, 1, 0, @NumberControlId)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('DACC5837-C761-49BD-AA72-84EB4C7E2361', @CompanyNumberId, 'PCO', 'Commercial Output', NULL, 1, 0, @NumberControlId)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('E4B70E51-A383-470A-9D0E-09577242BCBE', @CompanyNumberId, 'PCR', 'Commercial Crime', NULL, 1, 0, @NumberControlId)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('C5D75236-D633-4B5F-9646-2D2630FA05C8', @CompanyNumberId, 'PGL', 'General Liability', NULL, 1, 0, @NumberControlId)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('EDB46994-CE6A-4740-A19A-3EF1EB7D0D3E', @CompanyNumberId, 'PIM', 'Inland Marine', NULL, 1, 0, @NumberControlId)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('5DA75F9F-7734-4FEC-A55E-FD016872F6C6', @CompanyNumberId, 'PUM', 'Umbrella', NULL, 1, 0, @NumberControlId)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('FE9E9A7B-B215-4D69-8501-6C36EBD20708', @CompanyNumberId, 'PWC', 'Workers'' Compensation', NULL, 1, 0, @NumberControlId)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('01BD77A0-8B75-4DAC-ABBA-1DA2031AD1EB', @CompanyNumberId, 'PPK', 'Commercial Package', NULL, 1, 0, @NumberControlId)
END