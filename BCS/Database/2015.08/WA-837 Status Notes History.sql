IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'PreviousSubmissionStatusReasonOther' AND Object_ID = Object_ID(N'SubmissionStatusHistory'))
BEGIN
	ALTER TABLE [dbo].[SubmissionStatusHistory] ADD PreviousSubmissionStatusReasonId int NULL
	ALTER TABLE [dbo].[SubmissionStatusHistory] ADD PreviousSubmissionStatusReasonOther varchar(255) NULL

	ALTER TABLE [dbo].[SubmissionStatusHistory] ADD CONSTRAINT FK_SubmissionStatusHistory_SubmissionStatusReason FOREIGN KEY (PreviousSubmissionStatusReasonId) REFERENCES dbo.SubmissionStatusReason (Id)
END