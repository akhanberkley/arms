--BNPG
--Reword Package Policy Symbols Field
UPDATE [dbo].[CompanyNumberSubmissionField] SET DisplayLabel = 'Lines of Business' WHERE SubmissionFieldId = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D' and companynumberid = 35

--BLS
--Agent should no longer be required
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET [Required] = 0 WHERE SubmissionFieldId = 'B9AF602C-8E96-475F-833E-FD8890DB2A10' AND CompanyNumberId = 30
