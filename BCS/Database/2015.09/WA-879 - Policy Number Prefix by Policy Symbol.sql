IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'PolicyNumberPrefix' AND Object_ID = Object_ID(N'PolicySymbol'))
BEGIN
	ALTER TABLE [dbo].[PolicySymbol] ADD PolicyNumberPrefix varchar(10) NULL
END