IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'VisibleOnBPM' AND Object_ID = Object_ID(N'CompanyNumberAttribute'))
BEGIN
	ALTER TABLE [dbo].[CompanyNumberAttribute] ADD VisibleOnBPM bit NOT NULL default(0)
	ALTER TABLE [dbo].[CompanyNumberCodeType] ADD VisibleOnBPM bit NOT NULL default(0)
	ALTER TABLE [dbo].[CompanyNumberSubmissionFieldSubmissionType] ADD VisibleOnBPM bit NOT NULL default(0)
END
GO

IF NOT EXISTS(SELECT * FROM [dbo].[CompanyNumberAttribute] WHERE VisibleOnBPM = 1)
BEGIN
	UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnBPM = 1 WHERE ClientSpecific = 0
	UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnBPM = 1 WHERE ClientSpecific = 0 AND ClassCodeSpecific = 0
END

UPDATE 
	[dbo].[CompanyNumberSubmissionFieldSubmissionType]
SET
	VisibleOnBPM = 1
WHERE
	[SubmissionFieldId] IN 
	(
		'71A16357-3F9C-4B07-AE2F-ABA712B55E07'--effective date
		,'A65D45E4-3B8B-4A27-A6A9-918B46AA7D1D'--expiration date
	)