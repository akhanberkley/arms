DECLARE @CompanyId int = 29, @CompanyNumberId int = 41

UPDATE [dbo].[CompanyNumber] SET [CompanyNumber] = '105' WHERE CompanyId = @CompanyId

UPDATE [dbo].[CompanyNumberSubmissionType] SET SummarySubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' WHERE CompanyNumberId = @CompanyNumberId

UPDATE dbo.Company SET 
	ClientCoreEndPointUrl = REPLACE(ClientCoreEndPointUrl COLLATE Latin1_General_BIN, 'bps', 'ssp')
	,ClientCoreUsername = 'bpsclearance'
	,ClientCorePassword = 'welcome' 
	,ClientCoreCorpCode = 'WRBC'
	,ClientCoreOwner = 'BPSALL'
	,PolicyNumberRegex = '.*'
	,AnalystRoleID = null
WHERE Id = @CompanyId

UPDATE [dbo].[CompanyClientAdvancedSearch] SET
	DSIK = ''
	,ClientExperianSearchEndPointUrl = (SELECT TOP 1 ClientExperianSearchEndPointUrl FROM [dbo].[CompanyClientAdvancedSearch] WHERE CompanyId = 1)
WHERE CompanyId = @CompanyId

IF NOT EXISTS(SELECT * FROM [dbo].[CompanyNumberClientCoreSegment] WHERE CompanyNumberId = @CompanyNumberId)
	INSERT INTO [dbo].[CompanyNumberClientCoreSegment] ([CompanyNumberId],[Segment],[Description],[IsDefault])
     VALUES (@CompanyNumberId, 'BPSALL', 'All Clients', 1)

UPDATE dbo.CompanyParameter SET 
	SearchFilter = 'filterDuplicateNames|filterDuplicateAddresses|keepLowestNameSequence|keepLowestAddressSequence'
	,MQName = 'BPS.APS.BCS.REAL' 
	,DuplicateSubmissionQuery = '(SubmissionDt >= DbFunctions.AddDays(_Today, -120) || (EffectiveDate <= _Today && _Today <= ExpirationDate && SubmissionStatus.StatusCode == "Issued")) && SubmissionStatus.StatusCode != "Expired" && ((_PolicySymbolCode == null || _AgencyCode == null) || (Agency.AgencyNumber != _AgencyCode || PolicySymbol.Code == _PolicySymbolCode))'
WHERE CompanyId = @CompanyId

--Turn on policy symbol
IF NOT EXISTS(SELECT * FROM dbo.CompanyNumberSubmissionField WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId AND [Enabled] = 1)
BEGIN
	UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 1 WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
	
	INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd)
	SELECT SubmissionTypeId, CompanyNumberId, 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5', 1, 1 FROM dbo.CompanyNumberSubmissionType WHERE CompanyNumberId = @CompanyNumberId
END
--Turn off customization Policy Symbol
UPDATE [dbo].[SubmissionViewField] SET SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' WHERE SubmissionFieldId = '1B94F03C-C3B6-44C6-98CB-DA6FE3CEAE58' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionField  WHERE SubmissionFieldId = '1B94F03C-C3B6-44C6-98CB-DA6FE3CEAE58' AND CompanyNumberId = @CompanyNumberId

--Populate policy symbols
DECLARE @NumberControlTypeId INT;
DECLARE @NumberControlId INT;
SELECT @NumberControlTypeId = Id FROM [dbo].[NumberControlType] WHERE TypeName = 'Policy Number by Policy Symbol'
IF NOT EXISTS(SELECT * FROM [dbo].[PolicySymbol] WHERE CompanyNumberId = @CompanyNumberId)
BEGIN
	INSERT INTO [dbo].[NumberControl] ([CompanyNumberId],[NumberControlTypeId],[ControlNumber],[PaddingChar],[PadHead],[TotalLength])
		 VALUES (@CompanyNumberId, @NumberControlTypeId, 1, '0', 1, 7)
	SELECT @NumberControlId = @@IDENTITY

	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('7D051B51-2625-48D6-A6A7-72778958CBF3',@CompanyNumberId,'QCA', 'Commercial Auto', null, 1, 0, @NumberControlId)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('502340C7-DA88-4E3F-A7DB-848C82C00747',@CompanyNumberId,'QGL', 'General Liability', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('462289D5-B7F6-47A1-A5C8-A1BD9E822415',@CompanyNumberId,'QCP', 'Commercial Property', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('E8B4646D-A099-4729-B278-A8972F8377B8',@CompanyNumberId,'QCR', 'Commercial Crime', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('6FA28218-13C9-4B4D-8F79-6694209BF549',@CompanyNumberId,'QIM', 'Inland Marine', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('1FEE9186-40FB-44B3-9DB7-BFDC7EFD94C7',@CompanyNumberId,'QUM', 'Commercial Umbrella', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('FC93EC42-89D1-4492-9C54-792A988F803C',@CompanyNumberId,'QWC', 'Workers Compensation', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('9B3B6626-C5E8-4865-B757-16256A8158F1',@CompanyNumberId,'QPK', 'Commercial Package', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('50F51180-1345-4E11-81C2-45D03C141701',@CompanyNumberId,'QRW', 'Redwoods Program', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('DCEC2194-7EDB-49DB-8734-126474DBC48E',@CompanyNumberId,'QPA', 'BUP Public Auto', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('6FD22EB6-8665-418F-AD6D-90267782BBBE',@CompanyNumberId,'QLF', 'LIPCA Real Property', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('C2214797-3BB8-4213-9882-287867AE5C91',@CompanyNumberId,'QAM', 'Animal Mortality', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('F8F1FC28-A600-4571-AF30-AF31D16EB8F4',@CompanyNumberId,'QFO', 'Farm Owner', null, 1, 0, null)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('D7A4174C-6243-4801-B584-B1540C86A736',@CompanyNumberId,'QEL', 'Equine Liability', null, 1, 0, null)

	INSERT INTO [dbo].[NumberControl] ([CompanyNumberId],[NumberControlTypeId],[ControlNumber],[PaddingChar],[PadHead],[TotalLength])
		 VALUES (@CompanyNumberId, @NumberControlTypeId, 1, '0', 1, 7)
	SELECT @NumberControlId = @@IDENTITY

	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('1BDC5665-15C4-43D3-9219-E80E02B951B1',@CompanyNumberId,'QYP', 'Yacht Program', null, 1, 0, @NumberControlId)
END

UPDATE dbo.PolicySymbol SET PolicyNumberPrefix = '4AD' WHERE Id = '1BDC5665-15C4-43D3-9219-E80E02B951B1'

IF NOT EXISTS(SELECT * FROM [dbo].[PolicySymbol] WHERE Id = '9A4F9B8B-3BFD-41F7-967D-B4342D5D913A')
BEGIN
	SELECT @NumberControlId = NumberControlId FROM dbo.PolicySymbol WHERE Id = '7D051B51-2625-48D6-A6A7-72778958CBF3'

	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('9A4F9B8B-3BFD-41F7-967D-B4342D5D913A',@CompanyNumberId,'QCC', 'Collector Car', null, 1, 0, @NumberControlId)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId]) VALUES ('2ED91516-E75B-493E-9113-C4BA0A75E90D',@CompanyNumberId,'QC2', 'Collector Car - Direct Market', null, 1, 0, @NumberControlId)

	UPDATE dbo.PolicySymbol SET PolicyNumberPrefix = '499' WHERE Id IN ('9A4F9B8B-3BFD-41F7-967D-B4342D5D913A', '2ED91516-E75B-493E-9113-C4BA0A75E90D')
	UPDATE dbo.PolicySymbol SET PolicyNumberPrefix = NULL, NumberControlId = NULL WHERE Id = '7D051B51-2625-48D6-A6A7-72778958CBF3'
END

IF (SELECT DB_NAME()) = 'bcs_prd' 
	UPDATE [dbo].[CompanyClientAdvancedSearch] SET [DSIK] = 'a0801331-d136-ae7b-d468-e4b09e0f435c' WHERE CompanyId = @CompanyId
ELSE
	UPDATE [dbo].[CompanyClientAdvancedSearch] SET [DSIK] = 'cafab6f0-72f7-2276-3255-e801044639e8' WHERE CompanyId = @CompanyId

--Submission # should start at 1, not 10000
--IF NOT EXISTS(SELECT * FROM [dbo].[SubmissionNumberControl] WHERE [CompanyNumberId] = @CompanyNumberId AND [ControlNumber] >= 10000)
--	UPDATE [dbo].[SubmissionNumberControl] SET ControlNumber = 1 WHERE [CompanyNumberId] = @CompanyNumberId