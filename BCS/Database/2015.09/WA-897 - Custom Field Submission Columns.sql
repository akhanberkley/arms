IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'CompanyNumberAttributeId' AND Object_ID = Object_ID(N'SubmissionField'))
BEGIN
	ALTER TABLE [dbo].[SubmissionField] ADD CompanyNumberAttributeId int NULL
	ALTER TABLE [dbo].[SubmissionField] ADD CompanyNumberCodeTypeId int NULL

	ALTER TABLE [dbo].[SubmissionField] ADD CONSTRAINT FK_SubmissionField_CompanyNumberAttribute FOREIGN KEY (CompanyNumberAttributeId) REFERENCES dbo.CompanyNumberAttribute (Id)
	ALTER TABLE [dbo].[SubmissionField] ADD CONSTRAINT FK_SubmissionField_CompanyNumberCodeType FOREIGN KEY (CompanyNumberCodeTypeId) REFERENCES dbo.CompanyNumberCodeType (Id)
END

GO

--Update existing fields
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 3 AND CodeName = 'Policy Symbol') WHERE Id = '29163709-B4A6-4FCA-8EA1-7AD5C4BFAF83' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 6 AND CodeName = 'Policy Symbol') WHERE Id = '1B94F03C-C3B6-44C6-98CB-DA6FE3CEAE58' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 8 AND CodeName = 'Policy Symbol') WHERE Id = 'FC636F0A-1F4D-4010-A00D-34C1F2E95640' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 15 AND CodeName = 'Product') WHERE Id = '1EC5A717-5367-452D-922A-B0A1E97BF520' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 16 AND CodeName = 'Policy Type') WHERE Id = '0BE21E31-396D-4218-BE65-0CD460B8E4B4' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 20 AND CodeName = 'Policy Symbol') WHERE Id = '7A4CC8FC-F25B-4548-AA15-06D46D4A1101' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 25 AND CodeName = 'Policy Symbol') WHERE Id = 'F41C6012-6575-4756-B663-99FEFFB97173' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 29 AND CodeName = 'Policy Type') WHERE Id = '491B5CCD-D09D-4BC6-8B16-9E02FC78192C' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 30 AND CodeName = 'Policy Type') WHERE Id = '0E219703-25DC-4867-87D5-7FF42FED5074' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 32 AND CodeName = 'Policy Type') WHERE Id = 'EB48A5DC-1E27-4F11-A10B-6C8585F3AF38' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 36 AND CodeName = 'Policy Symbol') WHERE Id = '007AE062-AE31-4CF3-8804-7D68AAFB8D92' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 37 AND CodeName = 'Policy Symbol') WHERE Id = 'FBFBFC4D-D183-492F-AF29-8942934ED85E' AND CompanyNumberCodeTypeId IS NULL
UPDATE [dbo].[SubmissionField] SET CompanyNumberCodeTypeId = (SELECT TOP 1 Id FROM [dbo].[CompanyNumberCodeType] WHERE CompanyNumberId = 39 AND CodeName = 'Policy Symbol') WHERE Id = '988D3448-DD80-44CD-BE8A-3F666ECEC40A' AND CompanyNumberCodeTypeId IS NULL

--Insert missing fields
INSERT INTO [dbo].[SubmissionField] ([Id],[PropertyName],[DisplayLabel],[ColumnHeading],[CompanyNumberAttributeId],[CompanyNumberCodeTypeId])
SELECT 
	NEWID()
	,cna.Label
	,cna.Label
	,cna.Label
	,cna.Id
	,NULL
FROM 
	[dbo].[CompanyNumberAttribute] cna
WHERE 
	cna.ClientSpecific = 0
	AND cna.Id NOT IN (SELECT CompanyNumberAttributeId FROM [dbo].[SubmissionField] WHERE CompanyNumberAttributeId IS NOT NULL)

INSERT INTO [dbo].[SubmissionField] ([Id],[PropertyName],[DisplayLabel],[ColumnHeading],[CompanyNumberAttributeId],[CompanyNumberCodeTypeId])
SELECT 
	NEWID()
	,cnct.CodeName
	,cnct.CodeName
	,cnct.CodeName
	,NULL
	,cnct.Id
FROM 
	[dbo].[CompanyNumberCodeType] cnct
WHERE 
	cnct.ClientSpecific = 0 
	AND cnct.ClassCodeSpecific = 0
	AND cnct.Id NOT IN (SELECT CompanyNumberCodeTypeId FROM [dbo].[SubmissionField] WHERE CompanyNumberCodeTypeId IS NOT NULL)

INSERT INTO [dbo].[CompanyNumberSubmissionField] ([CompanyNumberId],[SubmissionFieldId],[Enabled],[DisplayLabel])
SELECT
	cna.CompanyNumberId
	,sf.Id
	,1
	,NULL
FROM
	dbo.SubmissionField sf
	INNER JOIN [dbo].[CompanyNumberAttribute] cna
		ON sf.[CompanyNumberAttributeId] = cna.Id
WHERE
	sf.CompanyNumberAttributeId IS NOT NULL
	AND NOT EXISTS(SELECT * FROM [dbo].[CompanyNumberSubmissionField] WHERE [SubmissionFieldId] = sf.Id)

INSERT INTO [dbo].[CompanyNumberSubmissionField] ([CompanyNumberId],[SubmissionFieldId],[Enabled],[DisplayLabel])
SELECT
	cnct.CompanyNumberId
	,sf.Id
	,1
	,NULL
FROM
	dbo.SubmissionField sf
	INNER JOIN [dbo].[CompanyNumberCodeType] cnct
		ON sf.[CompanyNumberCodeTypeId] = cnct.Id
WHERE
	sf.[CompanyNumberCodeTypeId] IS NOT NULL
	AND NOT EXISTS(SELECT * FROM [dbo].[CompanyNumberSubmissionField] WHERE [SubmissionFieldId] = sf.Id)