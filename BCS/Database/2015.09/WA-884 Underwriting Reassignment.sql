IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Action - Switch Underwriting Personnel')
BEGIN
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Action - Switch Underwriting Personnel', 0, 0, 'Ability to update the underwriting personnel tied to a set of submissions (on the Advanced Submission Search page)')
	DECLARE @RoleId INT
	SELECT @RoleId = @@IDENTITY

	INSERT INTO [dbo].[UserRole] ([UserId],[RoldId])
	SELECT
		ur.UserId
		,@RoleId
	FROM
		[dbo].[Role] r
		INNER JOIN [dbo].[UserRole] ur
			ON ur.RoldId = r.Id
			AND r.RoleName = 'ReAssignment Administrator'
END