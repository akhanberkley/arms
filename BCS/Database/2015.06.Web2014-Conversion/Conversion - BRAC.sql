DECLARE @CompanyId int = 28
DECLARE @CompanyNumberId int = 40

--Set 'New Business' as default submission type
UPDATE [dbo].[CompanyNumberSubmissionType] SET DefaultSelection = 1 WHERE CompanyNumberId = @CompanyNumberId AND SubmissionTypeId = 1
UPDATE [dbo].[CompanyNumberSubmissionType] SET [Order] = 2, DefaultSelection = 0 WHERE CompanyNumberId = @CompanyNumberId AND SubmissionTypeId = 2

--Agent not required
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET [Required] = 0 WHERE SubmissionFieldId = 'B9AF602C-8E96-475F-833E-FD8890DB2A10' AND CompanyNumberId = @CompanyNumberId
--Remove Client Portfolio
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = '8FBFB4CC-6682-4BFC-ADF7-3A9FEB597F34' AND CompanyNumberId = @CompanyNumberId
--Remove UnderwritingTechnician
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrierPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
--Remove AcquiringCarrierPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'B5D40301-24AB-4694-915B-51DFFAB51221' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'B5D40301-24AB-4694-915B-51DFFAB51221' AND CompanyNumberId = @CompanyNumberId
--Remove PackagePolicySymbols
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D' AND CompanyNumberId = @CompanyNumberId
--Remove PolicySystem
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
--Remove Client Core Segment
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = 'F88E6069-8912-4B4D-835B-BC2C076B1E5A' AND CompanyNumberId = @CompanyNumberId
--Remove Notes
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9' AND CompanyNumberId = @CompanyNumberId
DELETE FROM [dbo].[SubmissionViewField] WHERE SubmissionFieldId = '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9' AND CompanyNumberId = @CompanyNumberId
--Add PolicyMod
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 1 WHERE SubmissionFieldId = '36F65F08-6278-4A9A-B690-E8A04FDA445D' AND CompanyNumberId = @CompanyNumberId
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) VALUES(1, @CompanyNumberId, '36F65F08-6278-4A9A-B690-E8A04FDA445D', 0, 1)
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) VALUES(2, @CompanyNumberId, '36F65F08-6278-4A9A-B690-E8A04FDA445D', 0, 1)

--Setup Policy Symbols
DECLARE @NumberControlTypeId INT;
SELECT @NumberControlTypeId = Id FROM [dbo].[NumberControlType] WHERE TypeName = 'Policy Number by Policy Symbol'

IF NOT EXISTS(SELECT * FROM [dbo].[PolicySymbol] WHERE CompanyNumberId = @CompanyNumberId)
BEGIN
	INSERT INTO [dbo].[NumberControl] ([CompanyNumberId],[NumberControlTypeId],[ControlNumber],[PaddingChar],[PadHead],[TotalLength])
		 VALUES (@CompanyNumberId, @NumberControlTypeId, 1000000, NULL, 0, 0)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('3104D926-0081-4404-8212-CCA2EFF49C31', @CompanyNumberId, 'LMC', 'LMCIT', NULL, 1, 0, @@IDENTITY)

	INSERT INTO [dbo].[NumberControl] ([CompanyNumberId],[NumberControlTypeId],[ControlNumber],[PaddingChar],[PadHead],[TotalLength])
		 VALUES (@CompanyNumberId, @NumberControlTypeId, 2000000, NULL, 0, 0)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('4318F883-B5C3-43D4-976B-9CDF0037283E', @CompanyNumberId, 'AMR', 'AMRRP', NULL, 1, 0, @@IDENTITY)

	INSERT INTO [dbo].[NumberControl] ([CompanyNumberId],[NumberControlTypeId],[ControlNumber],[PaddingChar],[PadHead],[TotalLength])
		 VALUES (@CompanyNumberId, @NumberControlTypeId, 3000000, NULL, 0, 0)
	INSERT INTO [dbo].[PolicySymbol] ([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption],[NumberControlId])
		 VALUES ('8A0B6357-4F00-4DED-8CFB-715628DA4364', @CompanyNumberId, 'SSC', 'SSCIP', NULL, 1, 0, @@IDENTITY)
END