DECLARE @CompanyId int = 1
DECLARE @CompanyNumberId int = 3

--Need to make the class codes tab on client visible in the old system, otherwise it'll crash on clients that get class codes tied to them via the submission class code trigger
UPDATE dbo.Company SET SupportsClassCodes = 1 WHERE Id = @CompanyId

--Sort Hazard Grades
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 0 WHERE HazardGradeTypeId = 1 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 1 WHERE HazardGradeTypeId = 2 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 2 WHERE HazardGradeTypeId = 3 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 3 WHERE HazardGradeTypeId = 4 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 4 WHERE HazardGradeTypeId = 5 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 5 WHERE HazardGradeTypeId = 9 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 6 WHERE HazardGradeTypeId = 10 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 7 WHERE HazardGradeTypeId = 11 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 8 WHERE HazardGradeTypeId = 12 AND CompanyNumberId = @CompanyNumberId

--CWG Shouldn't have an analyst role
UPDATE dbo.company SET AnalystRoleId = NULL WHERE id = @CompanyId

--Update visibility of CWG Attributes/CodeTypes 
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 1, ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Manual Rated Policy Premium'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 1, ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'BUS Cat Code'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0, ExpirationDate = NULL WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Policy Premium'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 1, ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Policy Program Code'

--Data Conversion Required

--Keeping this for now
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 1, ExpirationDate = NULL WHERE CompanyNumberId = @CompanyNumberId AND Label = 'GL Class Code'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 1, ExpirationDate = NULL WHERE CompanyNumberId = @CompanyNumberId AND Label = 'SIC Code'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 1, ExpirationDate = NULL WHERE CompanyNumberId = @CompanyNumberId AND Label = 'NAICS Code'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 1, ExpirationDate = NULL, ExcludeValueOnCopy = 1 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Policy Symbol'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 1, OtherCarrierSpecific = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Prior Carrier Premium'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 1, OtherCarrierSpecific = 0, LineOfBusinessSpecific = 1 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Prior Carrier'
UPDATE [dbo].[CompanyNumberCodeType] SET ExpirationDate = '1/1/2014', OtherCarrierSpecific = 0, LineOfBusinessSpecific = 1 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Rollover'

-- Submission Field Customizations
DECLARE @PolicySymbolAttrSubmissionField uniqueidentifier = '29163709-B4A6-4FCA-8EA1-7AD5C4BFAF83'
IF NOT EXISTS (SELECT * FROM dbo.submissionfield WHERE Id = @PolicySymbolAttrSubmissionField)
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES (@PolicySymbolAttrSubmissionField, 'Policy Symbol', 'Policy Symbol', 'Symbol')
END
IF NOT EXISTS(SELECT * FROM dbo.CompanyNumberSubmissionField WHERE CompanyNumberId = @CompanyNumberId AND SubmissionFieldId = @PolicySymbolAttrSubmissionField)
BEGIN
	INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) VALUES (1, @CompanyNumberId, @PolicySymbolAttrSubmissionField)
END
UPDATE dbo.SubmissionViewField SET SubmissionFieldId = @PolicySymbolAttrSubmissionField WHERE CompanyNumberId = @CompanyNumberId AND SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5'

UPDATE 
	[dbo].[CompanyNumberSubmissionType]
SET 
	SummarySubmissionFieldId = @PolicySymbolAttrSubmissionField --policy symbol
WHERE
	CompanyNumberId = @CompanyNumberId

--PolicySymbol
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
--PolicyPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'F2CB0BAC-5492-4C7B-838F-C6C50A22C70B' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'F2CB0BAC-5492-4C7B-838F-C6C50A22C70B' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = 'B1713A27-05B7-4B95-A36F-2788DAE81233' AND CompanyNumberId = @CompanyNumberId
--Remove UnderwritingAnalyst
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'D0845F70-535C-41B7-A1F8-0A96574F43A6' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'D0845F70-535C-41B7-A1F8-0A96574F43A6' AND CompanyNumberId = @CompanyNumberId
--Remove UnderwritingTechnician
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrier
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrierPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
--Remove PackagePolicySymbols
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D' AND CompanyNumberId = @CompanyNumberId
--Remove PolicySystem
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
--Remove Client Core Segment
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = 'F88E6069-8912-4B4D-835B-BC2C076B1E5A' AND CompanyNumberId = @CompanyNumberId

--Fix the duplicate class codes
DECLARE @Updates TABLE (Id int, CodeValue varchar(50))
INSERT INTO @Updates (Id, CodeValue)
SELECT
	c.Id
	,c.CodeValue + CHAR(96 + ROW_NUMBER() OVER (partition by c.CodeValue order by c.codevalue))
FROM			
	dbo.ClassCode c		
	LEFT JOIN dbo.SicCodeList sic		
		ON c.SicCodeId = sic.Id	
WHERE			
	c.CompanyNumberId = 3		
	AND CodeValue IN (		
		SELECT 	
			CodeValue
		FROM	
			[dbo].[ClassCode]
		WHERE	
			CompanyNumberId = 3
		GROUP BY	
			CompanyNumberId
			,CodeValue
		HAVING	
			COUNT(*) > 1
	)
UPDATE
	dbo.ClassCode
SET
	CodeValue = u.CodeValue
FROM
	dbo.ClassCode c
	INNER JOIN @Updates u
		ON c.Id = u.Id

--Adjust submission duplicate table
DECLARE @SubmissionDuplicateViewId uniqueidentifier = 'A9A7352F-B7BC-4C3B-8E31-32597C39E1C9'
UPDATE dbo.SubmissionViewField SET Sequence = Sequence + 1 WHERE CompanyNumberId = @CompanyNumberId AND SubmissionViewTypeId = @SubmissionDuplicateViewId AND Sequence > 1
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	VALUES(@CompanyNumberId, @SubmissionDuplicateViewId, '96361B4B-2B1B-4932-861C-17852FF572DA', 2)--Company

DECLARE @RelatedSubmissionsViewId uniqueidentifier = '7E11EEAD-8149-4298-8F35-6901EDAF91E9'
UPDATE dbo.SubmissionViewField SET Sequence = Sequence + 1 WHERE CompanyNumberId = @CompanyNumberId AND SubmissionViewTypeId = @RelatedSubmissionsViewId AND Sequence > 1
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	VALUES(@CompanyNumberId, @RelatedSubmissionsViewId, '96361B4B-2B1B-4932-861C-17852FF572DA', 2)--Company