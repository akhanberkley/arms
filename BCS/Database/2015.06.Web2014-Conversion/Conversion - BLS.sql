DECLARE @CompanyId int = 18
DECLARE @CompanyNumberId int = 30

--Set 'New Business' as default submission type
UPDATE [dbo].[CompanyNumberSubmissionType] SET DefaultSelection = 1 WHERE CompanyNumberId = @CompanyNumberId AND SubmissionTypeId = 1

--Update visibility of Attributes/CodeTypes 
UPDATE [dbo].[CompanyNumberAttribute] SET ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Completed Submission Rec''d'

UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Prospect Indicator'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Retail Producer'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Indicated Premium'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Quoted Premium'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Bound Premium'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Issued Premium'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Working UA'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Working UW'
UPDATE [dbo].[CompanyNumberCodeType] SET ExcludeValueOnCopy = 1 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Policy Type'
UPDATE [dbo].[CompanyNumberAttribute] SET [ReadOnlyProperty] = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Current Carrier'

-- Submission Field Customizations
DECLARE @PolicySymbolAttrSubmissionField uniqueidentifier = '0E219703-25DC-4867-87D5-7FF42FED5074'
IF NOT EXISTS (SELECT * FROM dbo.submissionfield WHERE Id = @PolicySymbolAttrSubmissionField)
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES (@PolicySymbolAttrSubmissionField, 'Policy Type', 'Policy Type', 'Policy Type')
END
IF NOT EXISTS(SELECT * FROM dbo.CompanyNumberSubmissionField WHERE CompanyNumberId = @CompanyNumberId AND SubmissionFieldId = @PolicySymbolAttrSubmissionField)
BEGIN
	INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) VALUES (1, @CompanyNumberId, @PolicySymbolAttrSubmissionField)
END

UPDATE dbo.SubmissionViewField SET SubmissionFieldId = @PolicySymbolAttrSubmissionField WHERE CompanyNumberId = @CompanyNumberId AND SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5'
UPDATE 
	[dbo].[CompanyNumberSubmissionType]
SET 
	SummarySubmissionFieldId = @PolicySymbolAttrSubmissionField --policy symbol
WHERE
	CompanyNumberId = @CompanyNumberId
	
--Remove Client Portfolio
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = '8FBFB4CC-6682-4BFC-ADF7-3A9FEB597F34' AND CompanyNumberId = @CompanyNumberId
--PolicySymbol
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
--Policy # is not required
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET [Required] = 0 WHERE SubmissionFieldId = '7BC42623-1667-473C-AA99-16630327A75D' AND CompanyNumberId = @CompanyNumberId
--Remove UnderwritingTechnician
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrier
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrierPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
--Remove AcquiringCarrier
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'ABBDD884-BAAD-4164-9814-80D631B6E001' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'ABBDD884-BAAD-4164-9814-80D631B6E001' AND CompanyNumberId = @CompanyNumberId
--Remove AcquiringCarrierPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'B5D40301-24AB-4694-915B-51DFFAB51221' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'B5D40301-24AB-4694-915B-51DFFAB51221' AND CompanyNumberId = @CompanyNumberId
--Remove PackagePolicySymbols
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D' AND CompanyNumberId = @CompanyNumberId
--Remove PolicySystem
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
--Remove Client Core Segment
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = 'F88E6069-8912-4B4D-835B-BC2C076B1E5A' AND CompanyNumberId = @CompanyNumberId