ALTER TABLE dbo.Submission ALTER COLUMN AgencyId INT NULL
ALTER TABLE dbo.SubmissionHistory ALTER COLUMN AgencyId INT NULL


IF NOT EXISTS(SELECT *  FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_Submission_Agency')
BEGIN
	ALTER TABLE dbo.Submission ADD CONSTRAINT FK_Submission_Agency FOREIGN KEY (AgencyId) REFERENCES dbo.Agency (Id)
	ALTER TABLE dbo.Submission ADD CONSTRAINT FK_Submission_SubmissionType FOREIGN KEY (SubmissionTypeId) REFERENCES dbo.SubmissionType (Id)
END

IF EXISTS (select * from sysobjects where name = 'Web2014SubmissionAssociationSubmissionId')
DROP TRIGGER [dbo].[Web2014SubmissionAssociationSubmissionId]
GO
CREATE TRIGGER [dbo].[Web2014SubmissionAssociationSubmissionId] ON [dbo].[Submission] AFTER INSERT, UPDATE
AS
	IF EXISTS(SELECT * FROM INSERTED WHERE AssociationSubmissionId IS NULL)
		UPDATE 
			s
		SET
			s.AssociationSubmissionId = s.Id
		FROM
			dbo.Submission s
			INNER JOIN INSERTED i
				ON s.Id = i.Id
		WHERE
			i.AssociationSubmissionId IS NULL
GO

IF EXISTS (select * from sysobjects where name = 'Update_SubmissionPolicyNumberDigitsOnly')
DROP TRIGGER [dbo].[Update_SubmissionPolicyNumberDigitsOnly]
GO

CREATE TRIGGER [dbo].[Update_SubmissionPolicyNumberDigitsOnly] ON [dbo].[Submission] FOR INSERT, UPDATE
AS

SET NOCOUNT ON
DECLARE @Values TABLE (Id int, PolicyNumber varchar(50), CurrentPolicyNumberDigitsOnly bigint, NewPolicyNumberDigitsOnly bigint)
INSERT INTO @Values
SELECT
	i.Id
	,i.PolicyNumber
	,s.PolicyNumberDigitsOnly
	,dbo.RemoveChars(NULLIF(RTRIM(LTRIM(i.PolicyNumber)), ''))
FROM
	Inserted i
	LEFT JOIN Deleted s
	ON i.Id = s.Id

SET NOCOUNT OFF

IF EXISTS(SELECT * FROM @Values WHERE ISNULL(NewPolicyNumberDigitsOnly, -1) <> ISNULL(CurrentPolicyNumberDigitsOnly, -1))
	UPDATE
		s
	SET
		s.PolicyNumberDigitsOnly = v.NewPolicyNumberDigitsOnly
	FROM
		dbo.Submission s
		INNER JOIN @Values v
			ON s.Id = v.Id
		WHERE
			ISNULL(v.NewPolicyNumberDigitsOnly, -1) <> ISNULL(v.CurrentPolicyNumberDigitsOnly, -1)
GO

--AIC Spreadsheet Review Adjustments
DECLARE @CompanyNumberId int = 6
UPDATE dbo.CompanyParameter SET SubmissionStatusDateTBD = 60 WHERE CompanyId = 3

UPDATE dbo.CompanyNumberClientField SET VisibleOnAdd = 1 WHERE CompanyNumberId = @CompanyNumberId AND ClientFieldId = '6737EC59-7532-4806-A62C-1F78B3040DAB'--SICCode
UPDATE dbo.CompanyNumberClientField SET VisibleOnAdd = 1 WHERE CompanyNumberId = @CompanyNumberId AND ClientFieldId = 'E343A002-1BEE-4094-94FC-E1750FCCAF73'--NAICSCode
DELETE FROM dbo.CompanyNumberClientField WHERE CompanyNumberId = @CompanyNumberId AND ClientFieldId = 'FD8DB0A2-3BB1-41B7-A692-9FE20647C9DB'--ContactInformation

UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 1 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Designator'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 1 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Premium Range'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 1 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Target Group'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 1 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Year Est'
UPDATE [dbo].[CompanyNumberAttribute] SET ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Mod'
