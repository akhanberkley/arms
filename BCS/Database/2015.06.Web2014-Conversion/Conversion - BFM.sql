DECLARE @CompanyId int = 26
DECLARE @CompanyNumberId int = 38

IF NOT EXISTS(SELECT * FROM dbo.PolicySymbol WHERE CompanyNumberId = @CompanyNumberId)
BEGIN
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('C9C79265-EEBA-4CE0-8FDF-DB9BFB8A30CF',@CompanyNumberId,'BL','Bailees', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('423199EA-C0B5-4D93-BD78-D892B1F005A8',@CompanyNumberId,'BR','Builders Risk', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('89AD723F-7A51-46BF-AAF5-3E059990B620',@CompanyNumberId,'CE','Contractors Equipment', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('2226F12F-6921-4AB5-9534-960DFBDE01EB',@CompanyNumberId,'ED','Electronic Data Processing', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('E6B0A000-B32A-49ED-BC87-00C56CEA9704',@CompanyNumberId,'ES','Equipment Sales and Rental', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('4CCB56B3-A95E-480D-B343-54045E4C51FB',@CompanyNumberId,'FA','Fine Arts Dealer', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('6AB99C81-B50E-412B-BB9E-86E63A897686',@CompanyNumberId,'IF','Installation Floater', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('9827D78D-ACAF-4B20-A96E-5137FCC1D85B',@CompanyNumberId,'SP','Scheduled Property Floater', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('EC796D5B-119D-47F3-A0C8-B4512390925B',@CompanyNumberId,'ME','Mobile Medical Equipment', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('980CA4F2-DCF1-4393-9D22-2EDF13E5FDB1',@CompanyNumberId,'RS','Rolling Stock', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('2970E171-FFBA-4509-9A7F-97954AF7163B',@CompanyNumberId,'MC','Motor Truck Cargo', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('9627F133-9BD5-4B21-9FD3-C653E651AC4F',@CompanyNumberId,'TV','Radio TV Towers Equipment', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('5076484B-A67F-4D33-B268-B66DC2FF1833',@CompanyNumberId,'RL','Riggers Liability', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('C08CBBBE-1E75-40B3-A3B7-51A687391645',@CompanyNumberId,'TR','Transit', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('96083154-3031-4203-926C-EB2F7D60C83A',@CompanyNumberId,'WL','Warehouse Legal Liability', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('145460F7-68EE-44DF-9A6D-2E64FAAAFBD0',@CompanyNumberId,'CO','Commercial Output Program', NULL,0,1)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate],[PrimaryOption],[PackageOption]) VALUES ('9D875C10-774C-41F2-A6E4-2C2F01E533ED',@CompanyNumberId,'PR','Property', NULL,0,1)
END
UPDATE [PolicySymbol] SET PackageOption = 1 WHERE CompanyNumberId = @CompanyNumberId

DECLARE @PolicySymbolAttrSubmissionField uniqueidentifier = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D'
IF NOT EXISTS(SELECT * FROM dbo.CompanyNumberSubmissionField WHERE CompanyNumberId = @CompanyNumberId AND SubmissionFieldId = @PolicySymbolAttrSubmissionField)
BEGIN
	INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) VALUES (1, @CompanyNumberId, @PolicySymbolAttrSubmissionField)
END

UPDATE dbo.SubmissionViewField SET SubmissionFieldId = @PolicySymbolAttrSubmissionField WHERE CompanyNumberId = @CompanyNumberId AND SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5'
UPDATE 
	[dbo].[CompanyNumberSubmissionType]
SET 
	SummarySubmissionFieldId = @PolicySymbolAttrSubmissionField --policy symbol
WHERE
	CompanyNumberId = @CompanyNumberId

--Status not shown initially
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET VisibleOnAdd = 0 WHERE SubmissionFieldId = '4C4DDDF5-045F-46D6-83FC-151E8C70F30A' AND CompanyNumberId = @CompanyNumberId
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET VisibleOnAdd = 0 WHERE SubmissionFieldId = '266AD587-7D7D-42C7-BF10-F990EF46063C' AND CompanyNumberId = @CompanyNumberId
--PolicySymbol
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
--Remove Agent
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'B9AF602C-8E96-475F-833E-FD8890DB2A10' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'B9AF602C-8E96-475F-833E-FD8890DB2A10' AND CompanyNumberId = @CompanyNumberId
--Remove UnderwritingTechnician
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrier
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrierPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
--Remove AcquiringCarrier
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'ABBDD884-BAAD-4164-9814-80D631B6E001' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'ABBDD884-BAAD-4164-9814-80D631B6E001' AND CompanyNumberId = @CompanyNumberId
--Remove AcquiringCarrierPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'B5D40301-24AB-4694-915B-51DFFAB51221' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'B5D40301-24AB-4694-915B-51DFFAB51221' AND CompanyNumberId = @CompanyNumberId
--PolicyPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'F2CB0BAC-5492-4C7B-838F-C6C50A22C70B' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'F2CB0BAC-5492-4C7B-838F-C6C50A22C70B' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = 'B1713A27-05B7-4B95-A36F-2788DAE81233' AND CompanyNumberId = @CompanyNumberId
--Remove PolicySystem
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
--Remove Client Core Segment
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = 'F88E6069-8912-4B4D-835B-BC2C076B1E5A' AND CompanyNumberId = @CompanyNumberId