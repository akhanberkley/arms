IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = '7BC42623-1667-473C-AA99-16630327A75D')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('7BC42623-1667-473C-AA99-16630327A75D', 'PolicyNumber', 'Policy Number', 'Policy #')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('CDBFF102-D672-4F76-AF3C-678AD7C8E5BD', 'SubmissionType', 'Business Type', 'Type')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('9939ED4E-9F79-4017-92B7-B86A3988466A', 'SubmittedDate', 'Submitted Date', 'Submitted')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('9BCD2298-4625-4677-B8A7-7B0D34810B98', 'EffectivePeriod', 'Effective Period', 'Effective Period')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('7231DF59-2459-46AF-A0C4-0A451C42D224', 'Agency', 'Agency', 'Agency')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('4A776A4B-4D75-4666-9A59-A8C1402039D6', 'UnderwritingUnit', 'Underwriting Unit', 'UW Unit')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('4C4DDDF5-045F-46D6-83FC-151E8C70F30A', 'Status', 'Status', 'Status')
END
IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = '71A16357-3F9C-4B07-AE2F-ABA712B55E07')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('71A16357-3F9C-4B07-AE2F-ABA712B55E07', 'EffectiveDate', 'Effective Date', 'Effective')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('A65D45E4-3B8B-4A27-A6A9-918B46AA7D1D', 'ExpirationDate', 'Expiration Date', 'Expiration')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('B9AF602C-8E96-475F-833E-FD8890DB2A10', 'Agent', 'Agent', 'Agent')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('CDC4CA63-505E-4211-870C-B3849620C8AA', 'Underwriter', 'Underwriter', 'Underwriter')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('D0845F70-535C-41B7-A1F8-0A96574F43A6', 'UnderwritingAnalyst', 'Assistant/Analyst', 'Assistant')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('69377AC1-D496-460D-88E5-B40A29034775', 'UnderwritingTechnician', 'Technician', 'Technician')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('E88FA757-4B2A-4B37-B5BA-2B7B61D769F5', 'PolicySymbol', 'Policy Symbol', 'Symbol')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('36F65F08-6278-4A9A-B690-E8A04FDA445D', 'PolicyMod', 'Mod', 'Mod')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('FF0807F4-3009-43D3-8A3A-C24811F9B648', 'PriorCarrier', 'Prior Carrier', 'Prior Carrier')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('76888306-BE7B-4F44-BA99-C860D3DEADFB', 'PriorCarrierPremium', 'Prior Carrier Premium', 'Prior Carrier Premium')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('ABBDD884-BAAD-4164-9814-80D631B6E001', 'AcquiringCarrier', 'Acquiring Carrier', 'Acquiring Carrier')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('B5D40301-24AB-4694-915B-51DFFAB51221', 'AcquiringCarrierPremium', 'Acquiring Carrier Premium', 'Acquiring Carrier Premium')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9', 'Notes', 'Notes', 'Notes')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('26DCA514-74C2-4E4E-9D35-EC120B468C32', 'Class Codes', 'Class Codes', 'Class Codes')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('F2CB0BAC-5492-4C7B-838F-C6C50A22C70B', 'PolicyPremium', 'Policy Premium', 'Premium')
END
UPDATE dbo.SubmissionField SET DisplayLabel = 'Assistant/Analyst', ColumnHeading = 'Assistant' WHERE id = 'D0845F70-535C-41B7-A1F8-0A96574F43A6'
IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = '37B26DF5-8C6C-4EC3-BD1C-D9C9E400087E')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('37B26DF5-8C6C-4EC3-BD1C-D9C9E400087E', 'InsuredName', 'Insured Name', 'Insured')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('96361B4B-2B1B-4932-861C-17852FF572DA', 'Company', 'Company', 'Company')
END
UPDATE dbo.SubmissionField SET DisplayLabel = 'Client', ColumnHeading = 'Client' WHERE Id = '37B26DF5-8C6C-4EC3-BD1C-D9C9E400087E'

--IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = 'B189CD2F-806D-47B8-B997-E2DC6BCD0354')
--BEGIN
--	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('B189CD2F-806D-47B8-B997-E2DC6BCD0354', 'AdditionalInterests', 'Additional Interests', 'Additional Interests')
--END

IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('D4540EDC-0D67-4B26-88EF-0C9F2BD7A038', 'SequenceNumber', 'Sequence', 'Seq')
END
UPDATE dbo.SubmissionField SET DisplayLabel = 'Sequence Number' WHERE Id = 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038'
IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = 'D2BFB905-B328-4882-BBD4-C00E52CE1041')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('D2BFB905-B328-4882-BBD4-C00E52CE1041', 'SubmissionNumber', 'Submission Number', 'Sub #')
END
IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = '52A87238-D2BB-4CA0-8AC4-A3EB3ACA6C10')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('52A87238-D2BB-4CA0-8AC4-A3EB3ACA6C10', 'ClientCoreId', 'Client Id', 'Client Id')
	--INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('69A0ACD4-1CF5-4FF2-970F-C273AFE491F6', 'SubmissionView', 'View Submission Link', '')
END
--RIP Submission View Link
DELETE FROM dbo.SubmissionViewField WHERE SubmissionFieldId = '69A0ACD4-1CF5-4FF2-970F-C273AFE491F6'

IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = 'BAF0BD35-3965-430B-8928-303FB249A11B')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('BAF0BD35-3965-430B-8928-303FB249A11B', 'AgencyCode', 'Agency Code', 'Agency Code')
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('D851B70F-0965-4F47-AEE9-B9C4A85EF38B', 'StatusDate', 'Status Date', 'Status Date')
END
IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('BBBAF5B0-7072-440C-90D2-058DD3A79C8D', 'PackagePolicySymbols', 'Policy Symbols', 'Symbols')
END
IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = '575D0B44-E33F-40BC-8A61-DE0D4449587F')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('575D0B44-E33F-40BC-8A61-DE0D4449587F', 'PolicySystem', 'Policy System', 'Policy System')
END
IF NOT EXISTS(SELECT * FROM dbo.SubmissionField WHERE ID = '266AD587-7D7D-42C7-BF10-F990EF46063C')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('266AD587-7D7D-42C7-BF10-F990EF46063C', 'StatusNotes', 'Status Notes', 'Status Notes')
END

--Links to APS Agencies
DECLARE @apsEnvironment varchar(50)
SELECT @apsEnvironment = CASE LOWER(DB_NAME())
							WHEN 'bcs_dev' THEN 'int'
							WHEN 'bcs_int' THEN 'int'
							WHEN 'bcs_test' THEN 'tst'
							WHEN 'bcs_prd' THEN 'prd'
						END
IF @apsEnvironment <> 'prd'
BEGIN
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://cwg-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 1
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://aic-' + @apsEnvironment + '/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 3
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://bmg-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 4
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://usg-' + @apsEnvironment + '/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 6
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://csm-' + @apsEnvironment + '/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 9
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://brs-' + @apsEnvironment + '/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 13
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://bls-' + @apsEnvironment + '/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 18
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://ric-' + @apsEnvironment + '/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 21
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://bnp-' + @apsEnvironment + '/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 23
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://btu-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 25
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://bfm-' + @apsEnvironment + '/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 26
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://fin-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 27
END
ELSE
BEGIN
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://cwg-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 1
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://aic-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 3
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://bmg-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 4
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://usg-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 6
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://csm-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 9
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://brs-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 13
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://bls-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 18
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://ric-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 21
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://bnp-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 23
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://btu-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 25
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://bfm-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 26
	UPDATE [dbo].[CompanyParameter] SET APSAgencyUrl = 'https://fin-' + @apsEnvironment + '.wrbts.ads.wrberkley.com/aps/agency.faces?agencyCode={0}' WHERE CompanyId = 27
END

IF NOT EXISTS(SELECT * FROM dbo.ClientField WHERE ID = 'FD8DB0A2-3BB1-41B7-A692-9FE20647C9DB')
BEGIN
	INSERT INTO dbo.ClientField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('FD8DB0A2-3BB1-41B7-A692-9FE20647C9DB', 'ContactInformation', 'Contact Information', 'Contact')
	INSERT INTO dbo.ClientField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('552893D4-C6EF-44C9-B295-307692BC0490', 'FEIN', 'FEIN', 'FEIN')
	INSERT INTO dbo.ClientField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('6737EC59-7532-4806-A62C-1F78B3040DAB', 'SICCode', 'SIC Code', 'SIC')
	INSERT INTO dbo.ClientField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('E343A002-1BEE-4094-94FC-E1750FCCAF73', 'NAICSCode', 'NAICS Code', 'NAICS')
	INSERT INTO dbo.ClientField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('B1713A27-05B7-4B95-A36F-2788DAE81233', 'AccountPremium', 'Account Premium', 'Account Premium')
	INSERT INTO dbo.ClientField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('A236ADD0-728A-4741-81FA-FA17EBA22907', 'BusinessDescription', 'Business Description', 'Description')
END
IF NOT EXISTS(SELECT * FROM dbo.ClientField WHERE ID = '8FBFB4CC-6682-4BFC-ADF7-3A9FEB597F34')
BEGIN
	INSERT INTO dbo.ClientField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('8FBFB4CC-6682-4BFC-ADF7-3A9FEB597F34', 'PortfolioInformation', 'Portfolio Information', 'Portfolio')
END
IF NOT EXISTS(SELECT * FROM dbo.ClientField WHERE ID = 'F88E6069-8912-4B4D-835B-BC2C076B1E5A')
BEGIN
	INSERT INTO dbo.ClientField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('F88E6069-8912-4B4D-835B-BC2C076B1E5A', 'ClientCoreSegment', 'Segment', 'Segment')
END

IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Administrator - Class Codes')
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Administrator - Class Codes', 0, 0, 'Maintain the list of class codes that can be tied to submissions')
IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Administrator - Custom Fields')
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Administrator - Custom Fields', 0, 0, 'Maintain custom fields on submissions, clients, and class codes')
IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Administrator - Users')
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Administrator - Users', 0, 0, 'Maintain who has access to the system along with their roles')
IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Administrator - Other Carriers')
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Administrator - Other Carriers', 0, 0, 'Maintain list of carriers available as Prior and Acquiring options')
IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Tools - Policy Number Generator')
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Tools - Policy Number Generator', 0, 0, 'Ability to generate policy numbers outside of the clearance process')
IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Action - Delete Submission')
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Action - Delete Submission', 0, 0, 'Ability to mark submissions as deleted (visible on submission screen)')
IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Action - Renumber Submission')
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Action - Renumber Submission', 0, 0, 'Ability to update the number and sequence of submissions (visible on submission screen)')
IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Action - Switch Client')
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Action - Switch Client', 0, 0, 'Ability to change which client a submission is tied to (viewable on both the submission and client screens)')
IF EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Action - Cross Clearance')
BEGIN
	DELETE FROM dbo.userrole where roldid in (select id from dbo.role where RoleName = 'Action - Cross Clearance')
	DELETE FROM dbo.[Role] WHERE RoleName = 'Action - Cross Clearance'
END
IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Administrator - Submission Tables')
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Administrator - Submission Tables', 0, 0, 'Ability to set which columns (and the column order) of submission tables')
IF NOT EXISTS(SELECT * FROM dbo.[Role] WHERE RoleName = 'Action - Set Client Segment')
	INSERT INTO dbo.Role (RoleName, ReadOnly, UniversalAccess, [Description]) VALUES ('Action - Set Client Segment', 0, 0, 'Ability to set a client''s client core segment on client creation')

--turn off inactive companies
UPDATE
	dbo.Company
SET
	Active = 0
WHERE
	Id IN (2,5,8,10,11,12,14,15,16,20,22)


--SubmissionViewFields: Clean
DELETE FROM dbo.SubmissionViewField
DELETE FROM dbo.SubmissionField WHERE Id = '2F26BC5E-C94A-40C1-8B1E-E9A52DADB4BE'--dup field
DELETE FROM dbo.SubmissionField WHERE Id = '69A0ACD4-1CF5-4FF2-970F-C273AFE491F6'
DELETE FROM dbo.SubmissionField WHERE Id = 'B189CD2F-806D-47B8-B997-E2DC6BCD0354'

---- SubmissionViewFields: Client Details: Submission columns
DECLARE @ClientDetailId uniqueidentifier = '88E19811-2A49-428E-84ED-1CA65BC9835E'
IF NOT EXISTS (SELECT * FROM dbo.SubmissionViewType WHERE Id = @ClientDetailId)
	INSERT INTO dbo.SubmissionViewType (Id, Name) VALUES (@ClientDetailId, 'client-detail')
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ClientDetailId, 'D2BFB905-B328-4882-BBD4-C00E52CE1041', 0 FROM dbo.CompanyNumber--SubmissionNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ClientDetailId, 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038', 1 FROM dbo.CompanyNumber--Sequence
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ClientDetailId, 'CDBFF102-D672-4F76-AF3C-678AD7C8E5BD', 2 FROM dbo.CompanyNumber--Type
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ClientDetailId, '7BC42623-1667-473C-AA99-16630327A75D', 3 FROM dbo.CompanyNumber--PolicyNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ClientDetailId, '9939ED4E-9F79-4017-92B7-B86A3988466A', 4 FROM dbo.CompanyNumber--Submitted
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ClientDetailId, '9BCD2298-4625-4677-B8A7-7B0D34810B98', 5 FROM dbo.CompanyNumber--Effective Period
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ClientDetailId, '7231DF59-2459-46AF-A0C4-0A451C42D224', 6 FROM dbo.CompanyNumber--Agency
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ClientDetailId, '4A776A4B-4D75-4666-9A59-A8C1402039D6', 7 FROM dbo.CompanyNumber--UnderwritingUnit	
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ClientDetailId, '4C4DDDF5-045F-46D6-83FC-151E8C70F30A', 8 FROM dbo.CompanyNumber--Status
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ClientDetailId, '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9', 9 FROM dbo.CompanyNumber--Notes

---- SubmissionViewFields: Duplicate Submissions
DECLARE @SubmissionDuplicateViewId uniqueidentifier = 'A9A7352F-B7BC-4C3B-8E31-32597C39E1C9'
IF NOT EXISTS (SELECT * FROM dbo.SubmissionViewType WHERE Id = @SubmissionDuplicateViewId)
	INSERT INTO dbo.SubmissionViewType (Id, Name) VALUES (@SubmissionDuplicateViewId, 'clearance-duplicate')
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, 'D2BFB905-B328-4882-BBD4-C00E52CE1041', 0 FROM dbo.CompanyNumber--SubmissionNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038', 1 FROM dbo.CompanyNumber--Sequence
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, '37B26DF5-8C6C-4EC3-BD1C-D9C9E400087E', 2 FROM dbo.CompanyNumber--InsuredName
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, '7BC42623-1667-473C-AA99-16630327A75D', 3 FROM dbo.CompanyNumber--PolicyNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5', 4 FROM dbo.CompanyNumber--Policy Symbol
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, '9939ED4E-9F79-4017-92B7-B86A3988466A', 5 FROM dbo.CompanyNumber--SubmittedDate
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, '71A16357-3F9C-4B07-AE2F-ABA712B55E07', 6 FROM dbo.CompanyNumber--EffectiveDate
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, '7231DF59-2459-46AF-A0C4-0A451C42D224', 7 FROM dbo.CompanyNumber--Agency
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, 'CDC4CA63-505E-4211-870C-B3849620C8AA', 8 FROM dbo.CompanyNumber--Underwriter
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, '4C4DDDF5-045F-46D6-83FC-151E8C70F30A', 9 FROM dbo.CompanyNumber--Status
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionDuplicateViewId, '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9', 10 FROM dbo.CompanyNumber--Notes

---- SubmissionViewFields: Switch Client Table
DECLARE @SwitchClientViewId uniqueidentifier = '972128A9-B6AB-4641-85DC-715F4EB2F79D'
IF NOT EXISTS (SELECT * FROM dbo.SubmissionViewType WHERE Id = @SwitchClientViewId)
	INSERT INTO dbo.SubmissionViewType (Id, Name) VALUES (@SwitchClientViewId, 'switch-client')
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SwitchClientViewId, 'D2BFB905-B328-4882-BBD4-C00E52CE1041', 0 FROM dbo.CompanyNumber--SubmissionNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SwitchClientViewId, 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038', 1 FROM dbo.CompanyNumber--Sequence
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SwitchClientViewId, 'CDBFF102-D672-4F76-AF3C-678AD7C8E5BD', 2 FROM dbo.CompanyNumber--Type
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SwitchClientViewId, '7BC42623-1667-473C-AA99-16630327A75D', 3 FROM dbo.CompanyNumber--PolicyNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SwitchClientViewId, 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5', 4 FROM dbo.CompanyNumber--Policy Symbol
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SwitchClientViewId, '9939ED4E-9F79-4017-92B7-B86A3988466A', 5 FROM dbo.CompanyNumber--SubmittedDate
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SwitchClientViewId, '9BCD2298-4625-4677-B8A7-7B0D34810B98', 6 FROM dbo.CompanyNumber--Effective Period
	INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SwitchClientViewId, '7231DF59-2459-46AF-A0C4-0A451C42D224', 7 FROM dbo.CompanyNumber--Agency
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SwitchClientViewId, '4C4DDDF5-045F-46D6-83FC-151E8C70F30A', 8 FROM dbo.CompanyNumber--Status
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SwitchClientViewId, '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9', 9 FROM dbo.CompanyNumber--Notes

---- SubmissionViewFields: Agency Detail
DECLARE @AgencyDetailViewId uniqueidentifier = 'A666FA34-568C-4C35-8467-208C1DD395CD'
IF NOT EXISTS (SELECT * FROM dbo.SubmissionViewType WHERE Id = @AgencyDetailViewId)
	INSERT INTO dbo.SubmissionViewType (Id, Name) VALUES (@AgencyDetailViewId, 'agency-detail')
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @AgencyDetailViewId, 'D2BFB905-B328-4882-BBD4-C00E52CE1041', 0 FROM dbo.CompanyNumber--SubmissionNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @AgencyDetailViewId, 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038', 1 FROM dbo.CompanyNumber--Sequence
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @AgencyDetailViewId, 'CDBFF102-D672-4F76-AF3C-678AD7C8E5BD', 2 FROM dbo.CompanyNumber--Type
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @AgencyDetailViewId, '37B26DF5-8C6C-4EC3-BD1C-D9C9E400087E', 3 FROM dbo.CompanyNumber--InsuredName
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @AgencyDetailViewId, '7BC42623-1667-473C-AA99-16630327A75D', 4 FROM dbo.CompanyNumber--PolicyNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @AgencyDetailViewId, 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5', 5 FROM dbo.CompanyNumber--Policy Symbol
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @AgencyDetailViewId, '9939ED4E-9F79-4017-92B7-B86A3988466A', 6 FROM dbo.CompanyNumber--SubmittedDate
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @AgencyDetailViewId, '9BCD2298-4625-4677-B8A7-7B0D34810B98', 7 FROM dbo.CompanyNumber--Effective Period
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @AgencyDetailViewId, '4C4DDDF5-045F-46D6-83FC-151E8C70F30A', 8 FROM dbo.CompanyNumber--Status
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @AgencyDetailViewId, '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9', 9 FROM dbo.CompanyNumber--Notes

---- SubmissionViewFields: Submission Search Results Table
DECLARE @SubmissionSearchResultsViewId uniqueidentifier = '2A629CFF-DA82-4F95-9B69-3D3A4C881755'
IF NOT EXISTS (SELECT * FROM dbo.SubmissionViewType WHERE Id = @SubmissionSearchResultsViewId)
	INSERT INTO dbo.SubmissionViewType (Id, Name) VALUES (@SubmissionSearchResultsViewId, 'submission-search-results')
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, 'D2BFB905-B328-4882-BBD4-C00E52CE1041', 0 FROM dbo.CompanyNumber--SubmissionNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038', 1 FROM dbo.CompanyNumber--Sequence
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, 'CDBFF102-D672-4F76-AF3C-678AD7C8E5BD', 2 FROM dbo.CompanyNumber--Type
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, '37B26DF5-8C6C-4EC3-BD1C-D9C9E400087E', 3 FROM dbo.CompanyNumber--InsuredName
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, '7BC42623-1667-473C-AA99-16630327A75D', 4 FROM dbo.CompanyNumber--PolicyNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, '9939ED4E-9F79-4017-92B7-B86A3988466A', 5 FROM dbo.CompanyNumber--SubmittedDate
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, '71A16357-3F9C-4B07-AE2F-ABA712B55E07', 6 FROM dbo.CompanyNumber--EffectiveDate
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, '7231DF59-2459-46AF-A0C4-0A451C42D224', 7 FROM dbo.CompanyNumber--Agency
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, '4A776A4B-4D75-4666-9A59-A8C1402039D6', 8 FROM dbo.CompanyNumber--UnderwritingUnit
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, 'CDC4CA63-505E-4211-870C-B3849620C8AA', 9 FROM dbo.CompanyNumber--Underwriter
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, '4C4DDDF5-045F-46D6-83FC-151E8C70F30A', 10 FROM dbo.CompanyNumber--Status
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @SubmissionSearchResultsViewId, '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9', 11 FROM dbo.CompanyNumber--Notes

---- SubmissionViewFields: Submission Application View Table
DECLARE @ApplicationViewId uniqueidentifier = '36A495DE-4BE9-493B-813C-F3210E86BAC2'
IF NOT EXISTS (SELECT * FROM dbo.SubmissionViewType WHERE Id = @ApplicationViewId)
	INSERT INTO dbo.SubmissionViewType (Id, Name) VALUES (@ApplicationViewId, 'application-view')
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ApplicationViewId, 'D2BFB905-B328-4882-BBD4-C00E52CE1041', 1 FROM dbo.CompanyNumber--SubmissionNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ApplicationViewId, 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038', 2 FROM dbo.CompanyNumber--Sequence
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ApplicationViewId, '7BC42623-1667-473C-AA99-16630327A75D', 3 FROM dbo.CompanyNumber--PolicyNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ApplicationViewId, 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5', 4 FROM dbo.CompanyNumber--Policy Symbol
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ApplicationViewId, '4A776A4B-4D75-4666-9A59-A8C1402039D6', 5 FROM dbo.CompanyNumber--UnderwritingUnit
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ApplicationViewId, 'CDC4CA63-505E-4211-870C-B3849620C8AA', 6 FROM dbo.CompanyNumber--Underwriter
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ApplicationViewId, '4C4DDDF5-045F-46D6-83FC-151E8C70F30A', 7 FROM dbo.CompanyNumber--Status
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @ApplicationViewId, '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9', 8 FROM dbo.CompanyNumber--Notes

---- SubmissionViewFields: Related Submissions Tab View Table
DECLARE @RelatedSubmissionsViewId uniqueidentifier = '7E11EEAD-8149-4298-8F35-6901EDAF91E9'
IF NOT EXISTS (SELECT * FROM dbo.SubmissionViewType WHERE Id = @RelatedSubmissionsViewId)
	INSERT INTO dbo.SubmissionViewType (Id, Name) VALUES (@RelatedSubmissionsViewId, 'related-submissions')
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @RelatedSubmissionsViewId, 'D2BFB905-B328-4882-BBD4-C00E52CE1041', 0 FROM dbo.CompanyNumber--SubmissionNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @RelatedSubmissionsViewId, 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038', 1 FROM dbo.CompanyNumber--Sequence
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @RelatedSubmissionsViewId, '7BC42623-1667-473C-AA99-16630327A75D', 2 FROM dbo.CompanyNumber--PolicyNumber
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @RelatedSubmissionsViewId, 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5', 3 FROM dbo.CompanyNumber--Policy Symbol
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @RelatedSubmissionsViewId, '4A776A4B-4D75-4666-9A59-A8C1402039D6', 4 FROM dbo.CompanyNumber--UnderwritingUnit
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @RelatedSubmissionsViewId, 'CDC4CA63-505E-4211-870C-B3849620C8AA', 5 FROM dbo.CompanyNumber--Underwriter
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @RelatedSubmissionsViewId, '4C4DDDF5-045F-46D6-83FC-151E8C70F30A', 6 FROM dbo.CompanyNumber--Status
INSERT INTO dbo.SubmissionViewField (CompanyNumberId,SubmissionViewTypeId,SubmissionFieldId, Sequence)
	SELECT Id, @RelatedSubmissionsViewId, '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9', 7 FROM dbo.CompanyNumber--Notes

---- SubmissionViewFields: Client Search Page/Application Page (other submissions)
DECLARE @ClientSearchViewId uniqueidentifier = 'B863FFDB-247E-4F27-92A1-56E187B891FF'
DELETE FROM dbo.SubmissionViewType WHERE Id = @ClientSearchViewId --using client-detail now

--Submission Fields: Clean
UPDATE [dbo].[CompanyNumberSubmissionType] SET SummarySubmissionFieldId = NULL
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType
DELETE FROM dbo.CompanyNumberSubmissionField

--Setup enabled fields
--Submission Table Fields
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '9BCD2298-4625-4677-B8A7-7B0D34810B98' FROM dbo.CompanyNumber--EffectivePeriod
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '37B26DF5-8C6C-4EC3-BD1C-D9C9E400087E' FROM dbo.CompanyNumber--Client
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '96361B4B-2B1B-4932-861C-17852FF572DA' FROM dbo.CompanyNumber--Company
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'D2BFB905-B328-4882-BBD4-C00E52CE1041' FROM dbo.CompanyNumber--SubmissionNumber
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '52A87238-D2BB-4CA0-8AC4-A3EB3ACA6C10' FROM dbo.CompanyNumber--ClientCoreId
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'BAF0BD35-3965-430B-8928-303FB249A11B' FROM dbo.CompanyNumber--AgencyCode
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'D851B70F-0965-4F47-AEE9-B9C4A85EF38B' FROM dbo.CompanyNumber--StatusDate

--Usable Fields
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'CDBFF102-D672-4F76-AF3C-678AD7C8E5BD' FROM dbo.CompanyNumber--SubmissionType
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038' FROM dbo.CompanyNumber--SequenceNumber
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '9939ED4E-9F79-4017-92B7-B86A3988466A' FROM dbo.CompanyNumber--SubmittedDate
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '71A16357-3F9C-4B07-AE2F-ABA712B55E07' FROM dbo.CompanyNumber--EffectiveDate
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'A65D45E4-3B8B-4A27-A6A9-918B46AA7D1D' FROM dbo.CompanyNumber--ExpirationDate
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '7231DF59-2459-46AF-A0C4-0A451C42D224' FROM dbo.CompanyNumber--Agency
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'B9AF602C-8E96-475F-833E-FD8890DB2A10' FROM dbo.CompanyNumber--Agent
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '4A776A4B-4D75-4666-9A59-A8C1402039D6' FROM dbo.CompanyNumber--UnderwritingUnit
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'CDC4CA63-505E-4211-870C-B3849620C8AA' FROM dbo.CompanyNumber--Underwriter
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'D0845F70-535C-41B7-A1F8-0A96574F43A6' FROM dbo.CompanyNumber--UnderwritingAnalyst
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '69377AC1-D496-460D-88E5-B40A29034775' FROM dbo.CompanyNumber--UnderwritingTechnician
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '7BC42623-1667-473C-AA99-16630327A75D' FROM dbo.CompanyNumber--PolicyNumber
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '36F65F08-6278-4A9A-B690-E8A04FDA445D' FROM dbo.CompanyNumber--PolicyMod
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' FROM dbo.CompanyNumber--PolicySymbol
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D' FROM dbo.CompanyNumber--PackagePolicySymbols
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '4C4DDDF5-045F-46D6-83FC-151E8C70F30A' FROM dbo.CompanyNumber--Status
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'FF0807F4-3009-43D3-8A3A-C24811F9B648' FROM dbo.CompanyNumber--PriorCarrier
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '76888306-BE7B-4F44-BA99-C860D3DEADFB' FROM dbo.CompanyNumber--PriorCarrierPremium
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'ABBDD884-BAAD-4164-9814-80D631B6E001' FROM dbo.CompanyNumber--AcquiringCarrier
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'B5D40301-24AB-4694-915B-51DFFAB51221' FROM dbo.CompanyNumber--AcquiringCarrierPremium
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9' FROM dbo.CompanyNumber--Notes
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '26DCA514-74C2-4E4E-9D35-EC120B468C32' FROM dbo.CompanyNumber--ClassCodes
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, 'F2CB0BAC-5492-4C7B-838F-C6C50A22C70B' FROM dbo.CompanyNumber--PolicyPremium
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '575D0B44-E33F-40BC-8A61-DE0D4449587F' FROM dbo.CompanyNumber--PolicySystem
INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) SELECT 1, Id, '266AD587-7D7D-42C7-BF10-F990EF46063C' FROM dbo.CompanyNumber--StatusNotes

--Submission Fields:  Full Submission
DECLARE @FullSubmissionTypeIds table (CompanyNumberId int, SubmissionTypeId int)
INSERT INTO @FullSubmissionTypeIds VALUES (3,2)
INSERT INTO @FullSubmissionTypeIds VALUES (3,19)
INSERT INTO @FullSubmissionTypeIds VALUES (6,1)
INSERT INTO @FullSubmissionTypeIds VALUES (6,2)
INSERT INTO @FullSubmissionTypeIds VALUES (6,22)
INSERT INTO @FullSubmissionTypeIds VALUES (8,1)
INSERT INTO @FullSubmissionTypeIds VALUES (8,2)
INSERT INTO @FullSubmissionTypeIds VALUES (8,8)
INSERT INTO @FullSubmissionTypeIds VALUES (8,9)
INSERT INTO @FullSubmissionTypeIds VALUES (15,1)
INSERT INTO @FullSubmissionTypeIds VALUES (15,2)
INSERT INTO @FullSubmissionTypeIds VALUES (16,1)
INSERT INTO @FullSubmissionTypeIds VALUES (16,2)
INSERT INTO @FullSubmissionTypeIds VALUES (16,21)
INSERT INTO @FullSubmissionTypeIds VALUES (20,1)
INSERT INTO @FullSubmissionTypeIds VALUES (20,2)
INSERT INTO @FullSubmissionTypeIds VALUES (20,9)
INSERT INTO @FullSubmissionTypeIds VALUES (25,1)
INSERT INTO @FullSubmissionTypeIds VALUES (25,2)
INSERT INTO @FullSubmissionTypeIds VALUES (29,1)
INSERT INTO @FullSubmissionTypeIds VALUES (29,2)
INSERT INTO @FullSubmissionTypeIds VALUES (29,9)
INSERT INTO @FullSubmissionTypeIds VALUES (30,1)
INSERT INTO @FullSubmissionTypeIds VALUES (30,2)
INSERT INTO @FullSubmissionTypeIds VALUES (30,9)
INSERT INTO @FullSubmissionTypeIds VALUES (32,1)
INSERT INTO @FullSubmissionTypeIds VALUES (32,2)
INSERT INTO @FullSubmissionTypeIds VALUES (35,2)
INSERT INTO @FullSubmissionTypeIds VALUES (35,19)
INSERT INTO @FullSubmissionTypeIds VALUES (36,1)
INSERT INTO @FullSubmissionTypeIds VALUES (36,2)
INSERT INTO @FullSubmissionTypeIds VALUES (37,1)
INSERT INTO @FullSubmissionTypeIds VALUES (37,2)
INSERT INTO @FullSubmissionTypeIds VALUES (38,1)
INSERT INTO @FullSubmissionTypeIds VALUES (38,2)
INSERT INTO @FullSubmissionTypeIds VALUES (39,1)
INSERT INTO @FullSubmissionTypeIds VALUES (39,2)
INSERT INTO @FullSubmissionTypeIds VALUES (40,1)
INSERT INTO @FullSubmissionTypeIds VALUES (40,2)

INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'CDBFF102-D672-4F76-AF3C-678AD7C8E5BD', 1, 1 FROM @FullSubmissionTypeIds--SubmissionType
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038', 1, 1 FROM @FullSubmissionTypeIds--SequenceNumber
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '9939ED4E-9F79-4017-92B7-B86A3988466A', 1, 0 FROM @FullSubmissionTypeIds--SubmittedDate
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '71A16357-3F9C-4B07-AE2F-ABA712B55E07', 1, 1 FROM @FullSubmissionTypeIds--EffectiveDate
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'A65D45E4-3B8B-4A27-A6A9-918B46AA7D1D', 0, 1 FROM @FullSubmissionTypeIds--ExpirationDate
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '7231DF59-2459-46AF-A0C4-0A451C42D224', 1, 1 FROM @FullSubmissionTypeIds--Agency
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'B9AF602C-8E96-475F-833E-FD8890DB2A10', 1, 1 FROM @FullSubmissionTypeIds--Agent
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '4A776A4B-4D75-4666-9A59-A8C1402039D6', 1, 1 FROM @FullSubmissionTypeIds--UnderwritingUnit
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'CDC4CA63-505E-4211-870C-B3849620C8AA', 1, 1 FROM @FullSubmissionTypeIds--Underwriter
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'D0845F70-535C-41B7-A1F8-0A96574F43A6', 0, 1 FROM @FullSubmissionTypeIds--UnderwritingAnalyst
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '69377AC1-D496-460D-88E5-B40A29034775', 0, 1 FROM @FullSubmissionTypeIds--UnderwritingTechnician
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '7BC42623-1667-473C-AA99-16630327A75D', 1, 1 FROM @FullSubmissionTypeIds--PolicyNumber
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '36F65F08-6278-4A9A-B690-E8A04FDA445D', 1, 1 FROM @FullSubmissionTypeIds--PolicyMod
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5', 1, 1 FROM @FullSubmissionTypeIds--PolicySymbol
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D', 1, 1 FROM @FullSubmissionTypeIds--PackagePolicySymbols
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '4C4DDDF5-045F-46D6-83FC-151E8C70F30A', 1, 1 FROM @FullSubmissionTypeIds--Status
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'FF0807F4-3009-43D3-8A3A-C24811F9B648', 0, 1 FROM @FullSubmissionTypeIds--PriorCarrier
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '76888306-BE7B-4F44-BA99-C860D3DEADFB', 0, 1 FROM @FullSubmissionTypeIds--PriorCarrierPremium
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'ABBDD884-BAAD-4164-9814-80D631B6E001', 0, 0 FROM @FullSubmissionTypeIds--AcquiringCarrier
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'B5D40301-24AB-4694-915B-51DFFAB51221', 0, 0 FROM @FullSubmissionTypeIds--AcquiringCarrierPremium
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9', 0, 1 FROM @FullSubmissionTypeIds--Notes
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '26DCA514-74C2-4E4E-9D35-EC120B468C32', 0, 1 FROM @FullSubmissionTypeIds--ClassCodes
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'F2CB0BAC-5492-4C7B-838F-C6C50A22C70B', 0, 0 FROM @FullSubmissionTypeIds--PolicyPremium
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '575D0B44-E33F-40BC-8A61-DE0D4449587F', 0, 1 FROM @FullSubmissionTypeIds--PolicySystem
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '266AD587-7D7D-42C7-BF10-F990EF46063C', 0, 1 FROM @FullSubmissionTypeIds--StatusNotes

UPDATE 
	st
SET 
	SummarySubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5'--policy symbol
FROM
	[dbo].[CompanyNumberSubmissionType] st
	INNER JOIN @FullSubmissionTypeIds t
		ON st.CompanyNumberId = t.CompanyNumberId
		AND st.SubmissionTypeId = t.SubmissionTypeId
	
--Submission Fields: Prospect Submission
DECLARE @ProspectSubmissionTypeIds table (CompanyNumberId int, SubmissionTypeId int)
INSERT INTO @ProspectSubmissionTypeIds VALUES (3,22)

INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'CDBFF102-D672-4F76-AF3C-678AD7C8E5BD', 1, 1 FROM @ProspectSubmissionTypeIds--SubmissionType
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'D4540EDC-0D67-4B26-88EF-0C9F2BD7A038', 1, 1 FROM @ProspectSubmissionTypeIds--SequenceNumber
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '9939ED4E-9F79-4017-92B7-B86A3988466A', 1, 0 FROM @ProspectSubmissionTypeIds--SubmittedDate
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '71A16357-3F9C-4B07-AE2F-ABA712B55E07', 1, 1 FROM @ProspectSubmissionTypeIds--EffectiveDate
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'A65D45E4-3B8B-4A27-A6A9-918B46AA7D1D', 0, 1 FROM @ProspectSubmissionTypeIds--ExpirationDate
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '4A776A4B-4D75-4666-9A59-A8C1402039D6', 0, 1 FROM @ProspectSubmissionTypeIds--UnderwritingUnit
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5', 0, 1 FROM @ProspectSubmissionTypeIds--PolicySymbol
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D', 0, 1 FROM @ProspectSubmissionTypeIds--PackagePolicySymbols
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '4C4DDDF5-045F-46D6-83FC-151E8C70F30A', 1, 0 FROM @ProspectSubmissionTypeIds--Status
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, 'FF0807F4-3009-43D3-8A3A-C24811F9B648', 0, 1 FROM @ProspectSubmissionTypeIds--PriorCarrier
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '76888306-BE7B-4F44-BA99-C860D3DEADFB', 0, 1 FROM @ProspectSubmissionTypeIds--PriorCarrierPremium
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9', 0, 1 FROM @ProspectSubmissionTypeIds--Notes
INSERT INTO dbo.CompanyNumberSubmissionFieldSubmissionType (SubmissionTypeId, CompanyNumberId, SubmissionFieldId, [Required], VisibleOnAdd) SELECT SubmissionTypeId, CompanyNumberId, '266AD587-7D7D-42C7-BF10-F990EF46063C', 0, 0 FROM @ProspectSubmissionTypeIds--StatusNotes

--Set the summary field for submissions (setting all companies to the standard policy symbol)
UPDATE 
	st
SET 
	SummarySubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5'--policy symbol
FROM
	[dbo].[CompanyNumberSubmissionType] st
	INNER JOIN @ProspectSubmissionTypeIds t
		ON st.CompanyNumberId = t.CompanyNumberId
		AND st.SubmissionTypeId = t.SubmissionTypeId

--Submission Fields: All Company Customizations
--PolicyMod
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '36F65F08-6278-4A9A-B690-E8A04FDA445D'
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '36F65F08-6278-4A9A-B690-E8A04FDA445D'

--Client Fields
DELETE FROM dbo.CompanyNumberClientField
INSERT INTO dbo.CompanyNumberClientField (CompanyNumberId, ClientFieldId, [Required], VisibleOnAdd) SELECT Id, 'FD8DB0A2-3BB1-41B7-A692-9FE20647C9DB', 0, 1 FROM dbo.CompanyNumber--ContactInformation
INSERT INTO dbo.CompanyNumberClientField (CompanyNumberId, ClientFieldId, [Required], VisibleOnAdd) SELECT Id, '552893D4-C6EF-44C9-B295-307692BC0490', 0, 1 FROM dbo.CompanyNumber--FEIN
INSERT INTO dbo.CompanyNumberClientField (CompanyNumberId, ClientFieldId, [Required], VisibleOnAdd) SELECT Id, '6737EC59-7532-4806-A62C-1F78B3040DAB', 0, 0 FROM dbo.CompanyNumber--SICCode
INSERT INTO dbo.CompanyNumberClientField (CompanyNumberId, ClientFieldId, [Required], VisibleOnAdd) SELECT Id, 'E343A002-1BEE-4094-94FC-E1750FCCAF73', 0, 0 FROM dbo.CompanyNumber--NAICSCode
INSERT INTO dbo.CompanyNumberClientField (CompanyNumberId, ClientFieldId, [Required], VisibleOnAdd) SELECT Id, 'B1713A27-05B7-4B95-A36F-2788DAE81233', 0, 0 FROM dbo.CompanyNumber--AccountPremium
INSERT INTO dbo.CompanyNumberClientField (CompanyNumberId, ClientFieldId, [Required], VisibleOnAdd) SELECT Id, 'A236ADD0-728A-4741-81FA-FA17EBA22907', 0, 1 FROM dbo.CompanyNumber--BusinessDescription
INSERT INTO dbo.CompanyNumberClientField (CompanyNumberId, ClientFieldId, [Required], VisibleOnAdd) SELECT Id, '8FBFB4CC-6682-4BFC-ADF7-3A9FEB597F34', 0, 0 FROM dbo.CompanyNumber--PortfolioInformation
INSERT INTO dbo.CompanyNumberClientField (CompanyNumberId, ClientFieldId, [Required], VisibleOnAdd) SELECT Id, 'F88E6069-8912-4B4D-835B-BC2C076B1E5A', 0, 1 FROM dbo.CompanyNumber--ClientCoreSegment

--Update User Types
UPDATE dbo.[User]
SET UserTypeId = 'B01E48A7-929A-4B98-A9FB-E74DE5660DC3'--Read Only
WHERE Id IN (SELECT ur.UserID FROM dbo.UserRole ur INNER JOIN dbo.[Role] r 
									ON ur.RoldId = r.Id 
							  WHERE r.RoleName IN ('Auditor','Partial User','Remote Agency'))
UPDATE dbo.[User]
SET UserTypeId = 'DC840769-BD1B-415F-A760-8539285ED1E0'--System Admin
WHERE Id IN (SELECT ur.UserID FROM dbo.UserRole ur INNER JOIN dbo.[Role] r 
									ON ur.RoldId = r.Id 
							  WHERE r.RoleName = 'System Administrator')

--Submission Table Descriptions
UPDATE [dbo].[SubmissionViewType] SET [Description] = 'Shown on Client Search Results Page, Application Page (Clients Other Submissions), and the View Client Page' WHERE Id = '88E19811-2A49-428E-84ED-1CA65BC9835E'
UPDATE [dbo].[SubmissionViewType] SET [Description] = 'Shown on the View Agency Page' WHERE Id = 'A666FA34-568C-4C35-8467-208C1DD395CD'
UPDATE [dbo].[SubmissionViewType] SET [Description] = 'Shown on the Duplicates Step of the clearance process' WHERE Id = 'A9A7352F-B7BC-4C3B-8E31-32597C39E1C9'
UPDATE [dbo].[SubmissionViewType] SET [Description] = 'Shown on Submission Search Results Page' WHERE Id = '2A629CFF-DA82-4F95-9B69-3D3A4C881755'
UPDATE [dbo].[SubmissionViewType] SET [Description] = 'Shown on View Submission Page (Related Submissions Tab - both tables)' WHERE Id = '7E11EEAD-8149-4298-8F35-6901EDAF91E9'
UPDATE [dbo].[SubmissionViewType] SET [Description] = 'Shown on Switch Client process (via View Submission or View Client)' WHERE Id = '972128A9-B6AB-4641-85DC-715F4EB2F79D'
UPDATE [dbo].[SubmissionViewType] SET [Description] = 'Shown on View Application Page (Application Submissions)' WHERE Id = '36A495DE-4BE9-493B-813C-F3210E86BAC2'

--Client core segments
IF NOT EXISTS(SELECT * FROM dbo.CompanyNumberClientCoreSegment)
BEGIN
	INSERT INTO dbo.CompanyNumberClientCoreSegment (CompanyNumberId, Segment, [Description], IsDefault)
	SELECT
		cn.Id
		,c.ClientCoreOwner 
		,'All Clients'
		,1
	FROM 
		dbo.company c
		INNER JOIN dbo.CompanyNumber cn
			ON c.Id = cn.CompanyId
	WHERE
		c.active = 1

	INSERT INTO dbo.CompanyNumberClientCoreSegment (CompanyNumberId, Segment, [Description], IsDefault) VALUES (39, 'HOOFFICE', 'Home Office', 0)
	INSERT INTO dbo.CompanyNumberClientCoreSegment (CompanyNumberId, Segment, [Description], IsDefault) VALUES (35, 'PAYNE', 'Payne', 0)
	INSERT INTO dbo.CompanyNumberClientCoreSegment (CompanyNumberId, Segment, [Description], IsDefault) VALUES (3, 'PSBLISS', 'PSBLISS', 0)
	INSERT INTO dbo.CompanyNumberClientCoreSegment (CompanyNumberId, Segment, [Description], IsDefault) VALUES (3, 'PSTIS', 'PSTIS', 0)
	INSERT INTO dbo.CompanyNumberClientCoreSegment (CompanyNumberId, Segment, [Description], IsDefault) VALUES (36, 'HOOFFICE', 'Home Office', 0)
	INSERT INTO dbo.CompanyNumberClientCoreSegment (CompanyNumberId, Segment, [Description], IsDefault) VALUES (38, 'HOOFFICE', 'Home Office', 0)
	INSERT INTO dbo.CompanyNumberClientCoreSegment (CompanyNumberId, Segment, [Description], IsDefault) VALUES (30, 'HOOFFICE', 'Home Office', 0)
	INSERT INTO dbo.CompanyNumberClientCoreSegment (CompanyNumberId, Segment, [Description], IsDefault) VALUES (37, 'HOOFFICE', 'Home Office', 0)
END
UPDATE dbo.CompanyNumberClientCoreSegment SET Segment = 'BMAGALL' WHERE CompanyNumberId = 8 AND IsDefault = 1

--Cross Clearance
DELETE FROM dbo.CompanyNumberCrossClearance
INSERT INTO dbo.CompanyNumberCrossClearance (CompanyNumberId, ClearsCompanyNumberId) VALUES (3, 36)
INSERT INTO dbo.CompanyNumberCrossClearance (CompanyNumberId, ClearsCompanyNumberId) VALUES (36, 3)

--Policy Symbol number generator type
IF NOT EXISTS(SELECT * FROM [dbo].[NumberControlType] WHERE TypeName = 'Policy Number by Policy Symbol')
	INSERT INTO [dbo].[NumberControlType] ([TypeName]) VALUES ('Policy Number by Policy Symbol')




