DECLARE @CompanyId int = 23
DECLARE @CompanyNumberId int = 35

IF NOT EXISTS(SELECT * FROM dbo.PolicySymbol WHERE CompanyNumberId = @CompanyNumberId)
BEGIN
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('69B18D04-0AF4-488F-88F8-34784B3BE033',@CompanyNumberId,'BP','BUSINESSOWNERS', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('F373E8B8-CD30-4511-A62F-A2DD83BCBE8E',@CompanyNumberId,'CA','COMMERCIAL AUTO', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('F9DD3368-1DCC-4D00-AE5E-717F96FB9B19',@CompanyNumberId,'CE','CONTRACTOR E&O', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('C1C0A752-F0CA-4A5D-BB2D-77CF0F9F4DF0',@CompanyNumberId,'CR','CRIME', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('82334814-23D6-4321-A194-71CE65E747DD',@CompanyNumberId,'DO','DIRECTORS & OFFICERS', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('FBEDB041-865A-4519-8129-880EA302BA27',@CompanyNumberId,'GA','GARAGE', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('2080C783-31D0-4550-A2A6-23A12BC364AF',@CompanyNumberId,'GL','GENERAL LIABILITY', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('FC8F04AF-8DD6-47B5-999F-DFDBBCFD21EB',@CompanyNumberId,'IM','INLAND MARINE', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('28361A67-60A1-46F1-BB89-934045CDE06E',@CompanyNumberId,'MC','MOTOR CARRIER', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('2CD2A1EF-76DB-4802-8668-FA523CA87B2B',@CompanyNumberId,'PR','PROPERTY', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('AD8CA698-668F-45CA-9AA4-5E983F0802D3',@CompanyNumberId,'UM','UMBRELLA', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('C1488B72-9615-4488-AD94-51E667DDE0FA',@CompanyNumberId,'WC','WORKERS COMP', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('B584AD53-754E-48A9-A170-661CA7E3BE74',@CompanyNumberId,'FO','FARMOWNER', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('CBF37BE0-8BDB-435D-A9DD-A71478FBA268',@CompanyNumberId,'FA','FARM AUTO', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('F5ABC5E6-31B9-4377-B47A-68A36DD6B0DD',@CompanyNumberId,'FM','FARM UMBRELLA', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('BC9DE1C1-1D84-4418-9147-F71141653F2E',@CompanyNumberId,'ZMIN','Z - MINICOMPUTER', NULL)
	INSERT INTO [dbo].[PolicySymbol]([Id],[CompanyNumberId],[Code],[Description],[RetirementDate]) VALUES ('37ED6334-8E46-42FA-BC77-C8D5F24FA281',@CompanyNumberId,'ZUNK','Z - UNKNOWN', NULL)
END
UPDATE [PolicySymbol] SET PackageOption = 1 WHERE CompanyNumberId = @CompanyNumberId

--Update visibility of Attributes/CodeTypes 
UPDATE [dbo].[CompanyNumberAttribute] SET ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Manual Rated Policy Premium'
UPDATE [dbo].[CompanyNumberCodeType] SET ExpirationDate = '1/1/2014', [Required] = 0 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Policy Symbol'
UPDATE [dbo].[CompanyNumberAttribute] SET ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Prior Carrier Premium'
UPDATE [dbo].[CompanyNumberAttribute] SET ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'NAICS Code'
UPDATE [dbo].[CompanyNumberAttribute] SET ExpirationDate = '1/1/2014', OtherCarrierSpecific = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Prior Carrier Premium'
UPDATE [dbo].[CompanyNumberCodeType] SET ExpirationDate = '1/1/2014', OtherCarrierSpecific = 0, LineOfBusinessSpecific = 1 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Prior Carrier'
UPDATE [dbo].[CompanyNumberCodeType] SET ExpirationDate = '1/1/2014', OtherCarrierSpecific = 0, LineOfBusinessSpecific = 1 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Rollover'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Policy Premium'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'SIC Code'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0 WHERE CompanyNumberId = @CompanyNumberId AND Label = 'BUS Cat Code'

DECLARE @PolicySymbolAttrSubmissionField uniqueidentifier = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D'
IF NOT EXISTS(SELECT * FROM dbo.CompanyNumberSubmissionField WHERE CompanyNumberId = @CompanyNumberId AND SubmissionFieldId = @PolicySymbolAttrSubmissionField)
BEGIN
	INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) VALUES (1, @CompanyNumberId, @PolicySymbolAttrSubmissionField)
END

UPDATE dbo.SubmissionViewField SET SubmissionFieldId = @PolicySymbolAttrSubmissionField WHERE CompanyNumberId = @CompanyNumberId AND SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5'
UPDATE 
	[dbo].[CompanyNumberSubmissionType]
SET 
	SummarySubmissionFieldId = @PolicySymbolAttrSubmissionField --policy symbol
WHERE
	CompanyNumberId = @CompanyNumberId
	
--Remove Client Portfolio
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = '8FBFB4CC-6682-4BFC-ADF7-3A9FEB597F34' AND CompanyNumberId = @CompanyNumberId
--Status not shown initially
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET VisibleOnAdd = 0 WHERE SubmissionFieldId = '4C4DDDF5-045F-46D6-83FC-151E8C70F30A' AND CompanyNumberId = @CompanyNumberId
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET VisibleOnAdd = 0 WHERE SubmissionFieldId = '266AD587-7D7D-42C7-BF10-F990EF46063C' AND CompanyNumberId = @CompanyNumberId
--PolicySymbol
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
--Remove UnderwritingTechnician
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrier
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrierPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
--Remove PolicyPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'F2CB0BAC-5492-4C7B-838F-C6C50A22C70B' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'F2CB0BAC-5492-4C7B-838F-C6C50A22C70B' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = 'B1713A27-05B7-4B95-A36F-2788DAE81233' AND CompanyNumberId = @CompanyNumberId
--Remove PolicySystem
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId