DECLARE @CompanyId int = 3
DECLARE @CompanyNumberId int = 6

--AIC Should have analyst role
UPDATE dbo.company SET AnalystRoleId = 3 WHERE id = @CompanyId

--Sort Hazard Grades
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 0 WHERE HazardGradeTypeId = 1 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 1 WHERE HazardGradeTypeId = 2 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 2 WHERE HazardGradeTypeId = 3 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 3 WHERE HazardGradeTypeId = 4 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 4 WHERE HazardGradeTypeId = 5 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 5 WHERE HazardGradeTypeId = 6 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 6 WHERE HazardGradeTypeId = 7 AND CompanyNumberId = @CompanyNumberId
UPDATE [dbo].[CompanyNumberHazardGradeType] SET DisplayOrder = 7 WHERE HazardGradeTypeId = 8 AND CompanyNumberId = @CompanyNumberId

--Populate AIC's prior carrier options based on their code type
IF NOT EXISTS(select * from dbo.CompanyNumberOtherCarrier WHERE CompanyNumberId = @CompanyNumberId)
BEGIN
	INSERT INTO [dbo].[CompanyNumberOtherCarrier] ([CompanyNumberId],[OtherCarrierId],[Order],[DefaultSelection])
     SELECT
           @CompanyNumberId
		   ,oc.Id
		   ,0
		   ,0
	FROM 
		[dbo].[CompanyNumberCode] code
		INNER JOIN [dbo].[CompanyNumberCodeType] codeType
			ON code.[CompanyCodeTypeId] = codeType.Id
			AND codeType.CompanyNumberId = @CompanyNumberId 
			AND codeType.CodeName = 'Prior Carrier'
		INNER JOIN dbo.OtherCarrier oc
			ON code.Code = oc.[Description] COLLATE DATABASE_DEFAULT
END

--Update visibility of AIC Attributes/CodeTypes
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 1, ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Target Date'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 1, ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Application Due Date'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0, ExpirationDate = null WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Agency Bound?'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0, ExpirationDate = null WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Year Est.'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0, ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Contact Info'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0, ExpirationDate = '1/1/2014' WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Contact Info Line 2'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 0, ExpirationDate = null WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Designator'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 0, ExpirationDate = null WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Premium Range'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 0, ExpirationDate = null WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Target Group'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 0, ExpirationDate = null WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'New Business Lost'

--Data Conversion Required

--Keeping these for now
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 0, ExpirationDate = null WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Acquiring Carrier'
UPDATE [dbo].[CompanyNumberAttribute] SET VisibleOnAdd = 0, ExpirationDate = NULL WHERE CompanyNumberId = @CompanyNumberId AND Label = 'Mod'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 1, ExpirationDate = NULL, ExcludeValueOnCopy = 1 WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Policy Symbol'
UPDATE [dbo].[CompanyNumberCodeType] SET VisibleOnAdd = 1, ExpirationDate = NULL WHERE CompanyNumberId = @CompanyNumberId AND [CodeName] = 'Prior Carrier'

--Remove the unused AIC SubmissionType (Other)
DELETE FROM [dbo].[CompanyNumberSubmissionType] WHERE CompanyNumberId = @CompanyNumberId AND SubmissionTypeId = 18

--Profit Center Customization
IF NOT EXISTS (SELECT * FROM dbo.submissionfield WHERE Id = '5AD1F1F2-D959-4D12-BF04-B15514B0E2D4')
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('5AD1F1F2-D959-4D12-BF04-B15514B0E2D4', 'Profit Center', 'Profit Center', 'Profit Center')
	INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) VALUES (1, @CompanyNumberId, '5AD1F1F2-D959-4D12-BF04-B15514B0E2D4')
END

-- Submission Field Customizations
DECLARE @PolicySymbolAttrSubmissionField uniqueidentifier = '1B94F03C-C3B6-44C6-98CB-DA6FE3CEAE58'
IF NOT EXISTS (SELECT * FROM dbo.submissionfield WHERE Id = @PolicySymbolAttrSubmissionField)
BEGIN
	INSERT INTO dbo.SubmissionField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES (@PolicySymbolAttrSubmissionField, 'Policy Symbol', 'Policy Symbol', 'Symbol')
END
IF NOT EXISTS(SELECT * FROM dbo.CompanyNumberSubmissionField WHERE CompanyNumberId = @CompanyNumberId AND SubmissionFieldId = @PolicySymbolAttrSubmissionField)
BEGIN
	INSERT INTO dbo.CompanyNumberSubmissionField ([Enabled], CompanyNumberId, SubmissionFieldId) VALUES (1, @CompanyNumberId, @PolicySymbolAttrSubmissionField)
END

UPDATE dbo.SubmissionViewField SET SubmissionFieldId = @PolicySymbolAttrSubmissionField WHERE CompanyNumberId = @CompanyNumberId AND SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5'
UPDATE 
	[dbo].[CompanyNumberSubmissionType]
SET 
	SummarySubmissionFieldId = @PolicySymbolAttrSubmissionField --policy symbol
WHERE
	CompanyNumberId = @CompanyNumberId

--PolicySymbol
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'E88FA757-4B2A-4B37-B5BA-2B7B61D769F5' AND CompanyNumberId = @CompanyNumberId
--Underwriter not required
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET [Required] = 0 WHERE SubmissionFieldId = 'CDC4CA63-505E-4211-870C-B3849620C8AA' AND CompanyNumberId = @CompanyNumberId
--Status not shown initially
UPDATE dbo.CompanyNumberSubmissionFieldSubmissionType SET VisibleOnAdd = 0 WHERE SubmissionFieldId = '4C4DDDF5-045F-46D6-83FC-151E8C70F30A' AND CompanyNumberId = @CompanyNumberId
--Remove Status Notes
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '266AD587-7D7D-42C7-BF10-F990EF46063C' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '266AD587-7D7D-42C7-BF10-F990EF46063C' AND CompanyNumberId = @CompanyNumberId
--Remove Agent
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'B9AF602C-8E96-475F-833E-FD8890DB2A10' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'B9AF602C-8E96-475F-833E-FD8890DB2A10' AND CompanyNumberId = @CompanyNumberId
--Remove UnderwritingAnalyst
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'D0845F70-535C-41B7-A1F8-0A96574F43A6' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'D0845F70-535C-41B7-A1F8-0A96574F43A6' AND CompanyNumberId = @CompanyNumberId
--Remove UnderwritingTechnician
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '69377AC1-D496-460D-88E5-B40A29034775' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrier
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'FF0807F4-3009-43D3-8A3A-C24811F9B648' AND CompanyNumberId = @CompanyNumberId
--Remove PriorCarrierPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '76888306-BE7B-4F44-BA99-C860D3DEADFB' AND CompanyNumberId = @CompanyNumberId
--Remove AcquiringCarrier
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'ABBDD884-BAAD-4164-9814-80D631B6E001' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'ABBDD884-BAAD-4164-9814-80D631B6E001' AND CompanyNumberId = @CompanyNumberId
--Remove AcquiringCarrierPremium
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'B5D40301-24AB-4694-915B-51DFFAB51221' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'B5D40301-24AB-4694-915B-51DFFAB51221' AND CompanyNumberId = @CompanyNumberId
--Remove Notes
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9' AND CompanyNumberId = @CompanyNumberId
DELETE FROM [dbo].[SubmissionViewField] WHERE SubmissionFieldId = '2A4E0DD1-45B3-49F9-BCE7-E6D5949AF3C9' AND CompanyNumberId = @CompanyNumberId
--Remove PackagePolicySymbols
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = 'BBBAF5B0-7072-440C-90D2-058DD3A79C8D' AND CompanyNumberId = @CompanyNumberId
--Remove PolicySystem
UPDATE dbo.CompanyNumberSubmissionField SET [Enabled] = 0 WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
DELETE FROM dbo.CompanyNumberSubmissionFieldSubmissionType WHERE SubmissionFieldId = '575D0B44-E33F-40BC-8A61-DE0D4449587F' AND CompanyNumberId = @CompanyNumberId
--Remove Client Core Segment
DELETE FROM dbo.CompanyNumberClientField WHERE ClientFieldId = 'F88E6069-8912-4B4D-835B-BC2C076B1E5A' AND CompanyNumberId = @CompanyNumberId