IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'ExperianId' AND Object_ID = Object_ID(N'Client'))
BEGIN
	ALTER TABLE [dbo].[Client] ADD ExperianId bigint NULL
END

IF NOT EXISTS(SELECT * FROM dbo.ClientField WHERE ID = 'D9C97462-C3C2-4116-9612-F635E5CEFEFE')
BEGIN
	INSERT INTO dbo.ClientField (Id, PropertyName, DisplayLabel, ColumnHeading) VALUES ('D9C97462-C3C2-4116-9612-F635E5CEFEFE', 'ExperianId', 'Experian', 'Experian')
	--Turning it on for every company
	INSERT INTO dbo.CompanyNumberClientField (CompanyNumberId, ClientFieldId, [Required], VisibleOnAdd) SELECT Id, 'D9C97462-C3C2-4116-9612-F635E5CEFEFE', 0, 1 FROM dbo.CompanyNumber
END

