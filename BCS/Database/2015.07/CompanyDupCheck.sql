IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'DuplicateSubmissionQuery' AND Object_ID = Object_ID(N'CompanyParameter'))
BEGIN
	ALTER TABLE [dbo].[CompanyParameter] ADD DuplicateSubmissionQuery varchar(2000) NULL
END
GO
UPDATE [dbo].[CompanyParameter] SET DuplicateSubmissionQuery = 'EffectiveDate >= DbFunctions.AddMonths(_EffectiveDate, -1) && EffectiveDate <= DbFunctions.AddMonths(_EffectiveDate, 1)'
UPDATE [dbo].[CompanyParameter] SET DuplicateSubmissionQuery = 'SubmissionDt >= DbFunctions.AddDays(_Today, -120) && SubmissionStatus.StatusCode != "Expired" && (_PolicySymbolCode == null || CompanyNumberCode.Any(Code == _PolicySymbolCode && CompanyNumberCodeType.CodeName == "Policy Symbol"))' WHERE CompanyId  IN (1,3,4,13,23,24)
UPDATE [dbo].[CompanyParameter] SET DuplicateSubmissionQuery = 'SubmissionDt >= DbFunctions.AddDays(_Today, -120) && SubmissionStatus.StatusCode != "Expired" && (_PolicySymbolCode == null || CompanyNumberCode.Any(Code == _PolicySymbolCode && CompanyNumberCodeType.CodeName == "Product"))' WHERE CompanyId = 6
--UPDATE [dbo].[CompanyParameter] SET DuplicateSubmissionQuery = 'SubmissionDt >= DbFunctions.AddDays(_Today, -120) && SubmissionStatus.StatusCode != "Expired" && (_PolicySymbolCode == null || PackagePolicySymbols.Any(Code == _PolicySymbolCode))' WHERE CompanyId = 23--BNP has both a customization and package: not sure how to handle that - doing standard above for now
UPDATE [dbo].[CompanyParameter] SET DuplicateSubmissionQuery = 'SubmissionDt >= DbFunctions.AddDays(_Today, -120) && SubmissionStatus.StatusCode != "Expired" && (_PolicySymbolCode == null || (PackagePolicySymbols.Any(Code == _PolicySymbolCode) && !(_AgencyCode == Agency.AgencyNumber && (_PolicySymbolCode == "BR" || _PolicySymbolCode == "IF"))))' WHERE CompanyId = 26