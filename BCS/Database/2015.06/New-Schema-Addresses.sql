--Populate SubmissionAddress table with base data
IF NOT EXISTS(SELECT TOP 1 * FROM dbo.[SubmissionAddress])
BEGIN
	INSERT INTO [dbo].[SubmissionAddress] ([Id],[SubmissionId],[ClientCoreAddressId],[EntryBy],[EntryDt],[ModifiedBy],[ModifiedDt])
	SELECT 
		newid()
		,cca.SubmissionId
		,cca.ClientCoreClientAddressId 
		,'Web2014Conversion'
		,getdate()
		,'Web2014Conversion'
		,getdate()
	FROM 
		dbo.Submission s 
		INNER JOIN [dbo].[SubmissionClientCoreClientAddress] cca 
			ON s.Id = cca.SubmissionId
END

--Address Triggers
IF EXISTS(select * from sys.triggers where name = 'Web2013AddressAdd')
	DROP TRIGGER dbo.Web2013AddressAdd
GO
CREATE TRIGGER dbo.Web2013AddressAdd ON [dbo].[SubmissionClientCoreClientAddress] AFTER INSERT 
AS
	IF EXISTS(SELECT * FROM INSERTED)
		INSERT INTO [dbo].[SubmissionAddress] ([Id],[SubmissionId],[ClientCoreAddressId],[EntryBy],[EntryDt],[ModifiedBy],[ModifiedDt])
		SELECT
			NEWID()
			,i.SubmissionId
			,i.ClientCoreClientAddressId
			,'Web2014Conversion'
			,getdate()
			,'Web2014Conversion'
			,getdate()
		FROM
			INSERTED i
		WHERE
			NOT EXISTS(SELECT * FROM dbo.SubmissionAddress WHERE SubmissionId = i.SubmissionId AND ClientCoreAddressId = i.ClientCoreClientAddressId)
GO

IF EXISTS(select * from sys.triggers where name = 'Web2013AddressRemove')
	DROP TRIGGER dbo.Web2013AddressRemove
GO
CREATE TRIGGER dbo.Web2013AddressRemove ON [dbo].[SubmissionClientCoreClientAddress] AFTER DELETE 
AS
	IF EXISTS(SELECT * FROM DELETED)
		DELETE [dbo].[SubmissionAddress]
		FROM
			DELETED d
			INNER JOIN [dbo].[SubmissionAddress] sa
				ON d.SubmissionId = sa.SubmissionId
				AND d.ClientCoreClientAddressId = sa.ClientCoreAddressId
GO

IF EXISTS(select * from sys.triggers where name = 'Web2014AddressAdd')
	DROP TRIGGER dbo.Web2014AddressAdd
GO
CREATE TRIGGER dbo.Web2014AddressAdd ON [dbo].[SubmissionAddress] AFTER INSERT 
AS
	IF EXISTS(SELECT * FROM INSERTED)
		INSERT INTO [dbo].[SubmissionClientCoreClientAddress] ([SubmissionId],[ClientCoreClientAddressId])
		SELECT
			i.SubmissionId
			,i.ClientCoreAddressId
		FROM
			INSERTED i
		WHERE
			i.ClientCoreAddressId IS NOT NULL
			AND NOT EXISTS(SELECT * FROM dbo.[SubmissionClientCoreClientAddress] WHERE SubmissionId = i.SubmissionId AND [ClientCoreClientAddressId] = i.ClientCoreAddressId)
GO

IF EXISTS(select * from sys.triggers where name = 'Web2014AddressRemove')
	DROP TRIGGER dbo.Web2014AddressRemove
GO
CREATE TRIGGER dbo.Web2014AddressRemove ON [dbo].[SubmissionAddress] AFTER DELETE 
AS
	IF EXISTS(SELECT * FROM DELETED)
		DELETE [dbo].[SubmissionClientCoreClientAddress]
		FROM
			DELETED d
			INNER JOIN [dbo].[SubmissionClientCoreClientAddress] sa
				ON d.SubmissionId = sa.SubmissionId
				AND d.ClientCoreAddressId = sa.ClientCoreClientAddressId
GO