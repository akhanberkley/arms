--Populate new table with existing data
INSERT INTO [dbo].[SubmissionPackagePolicySymbol] ([SubmissionId],[PolicySymbolId])
SELECT DISTINCT
	slob.SubmissionId
	,ps.Id
FROM
	[dbo].[Submission] s
	INNER JOIN  [dbo].[SubmissionLineOfBusinessChildren] slob
		ON slob.SubmissionId = s.Id
	INNER JOIN [dbo].[CompanyNumberLOBGrid] lob
		ON slob.[CompanyNumberLOBGridId] = lob.Id
	INNER JOIN [dbo].[PolicySymbol] ps
		ON ps.CompanyNumberId = s.CompanyNumberId
		AND lob.MulitLOBCode = ps.Code 

--Triggers
IF EXISTS(select * from sys.triggers where name = 'Web2013PackagePolicySymbolsAdd')
	DROP TRIGGER dbo.Web2013PackagePolicySymbolsAdd
GO
CREATE TRIGGER dbo.Web2013PackagePolicySymbolsAdd ON [dbo].[SubmissionLineOfBusinessChildren] AFTER INSERT 
AS
	IF EXISTS(SELECT * FROM INSERTED)
		INSERT INTO [dbo].[SubmissionPackagePolicySymbol] ([SubmissionId],[PolicySymbolId])
		SELECT
			i.SubmissionId
			,ps.Id
		FROM
			[dbo].[Submission] s
			INNER JOIN INSERTED i
				ON i.SubmissionId = s.Id
			INNER JOIN [dbo].[CompanyNumberLOBGrid] lob
				ON i.[CompanyNumberLOBGridId] = lob.Id
			INNER JOIN [dbo].[PolicySymbol] ps
				ON ps.CompanyNumberId = s.CompanyNumberId
				AND lob.MulitLOBCode = ps.Code 
		WHERE
			NOT EXISTS(SELECT * FROM [dbo].[SubmissionPackagePolicySymbol] WHERE SubmissionId = i.SubmissionId AND [PolicySymbolId] = ps.Id)
GO
IF EXISTS(select * from sys.triggers where name = 'Web2013PackagePolicySymbolsUpdate')
	DROP TRIGGER dbo.Web2013PackagePolicySymbolsUpdate
GO
CREATE TRIGGER dbo.Web2013PackagePolicySymbolsUpdate ON [dbo].[SubmissionLineOfBusinessChildren] AFTER UPDATE 
AS
	DECLARE @LobChanges TABLE (SubmissionId int, OldLOBGridId int, NewLobGridId int)
	INSERT INTO @LobChanges
	SELECT
		i.SubmissionId
		,d.CompanyNumberLOBGridId
		,i.CompanyNumberLOBGridId 
	FROM 
		Inserted i 
		INNER JOIN Deleted d 
			ON i.Id = d.Id 
			AND i.CompanyNumberLOBGridId <> d.CompanyNumberLOBGridId

	IF EXISTS(SELECT * FROM @LobChanges)
		UPDATE 
			spps
		SET
			[PolicySymbolId] = newPs.Id
		FROM
			[dbo].[Submission] s
			INNER JOIN [dbo].[SubmissionPackagePolicySymbol] spps
				ON spps.SubmissionId = s.Id
			INNER JOIN @LobChanges c
				ON c.SubmissionId = spps.SubmissionId
			INNER JOIN [dbo].[CompanyNumberLOBGrid] oldLob
				ON c.OldLOBGridId = oldLob.Id
			INNER JOIN [dbo].[PolicySymbol] oldPs
				ON oldPs.CompanyNumberId = s.CompanyNumberId
				AND oldLob.MulitLOBCode = oldPs.Code 
			INNER JOIN [dbo].[CompanyNumberLOBGrid] newLob
				ON c.NewLobGridId = newLob.Id
			INNER JOIN [dbo].[PolicySymbol] newPs
				ON newPs.CompanyNumberId = s.CompanyNumberId
				AND newLob.MulitLOBCode = newPs.Code 
		WHERE
			spps.PolicySymbolId = oldPs.Id
GO
IF EXISTS(select * from sys.triggers where name = 'Web2013PackagePolicySymbolsRemove')
	DROP TRIGGER dbo.Web2013PackagePolicySymbolsRemove
GO
CREATE TRIGGER dbo.Web2013PackagePolicySymbolsRemove ON [dbo].[SubmissionLineOfBusinessChildren] AFTER DELETE 
AS
	IF EXISTS(SELECT * FROM DELETED)
		DELETE [dbo].[SubmissionPackagePolicySymbol]
		FROM
			[dbo].[Submission] s
			INNER JOIN DELETED d
				ON d.SubmissionId = s.Id
			INNER JOIN [dbo].[CompanyNumberLOBGrid] lob
				ON d.[CompanyNumberLOBGridId] = lob.Id
			INNER JOIN [dbo].[PolicySymbol] ps
				ON ps.CompanyNumberId = s.CompanyNumberId
				AND lob.MulitLOBCode = ps.Code 
			INNER JOIN [dbo].[SubmissionPackagePolicySymbol] spps
				ON spps.SubmissionId = s.Id
				AND spps.PolicySymbolId = ps.Id
GO

IF EXISTS(select * from sys.triggers where name = 'Web2014SubmissionPackagePolicySymbolAdd')
	DROP TRIGGER dbo.Web2014SubmissionPackagePolicySymbolAdd
GO
CREATE TRIGGER dbo.Web2014SubmissionPackagePolicySymbolAdd ON [dbo].[SubmissionPackagePolicySymbol] AFTER INSERT 
AS
	IF EXISTS(SELECT * FROM INSERTED)
		INSERT INTO [dbo].[SubmissionLineOfBusinessChildren] ([SubmissionId],[CompanyNumberLOBGridId])
		SELECT
			i.SubmissionId
			,lob.Id
		FROM
			[dbo].[Submission] s
			INNER JOIN INSERTED i
				ON i.SubmissionId = s.Id
			INNER JOIN [dbo].[PolicySymbol] ps
				ON i.PolicySymbolId = ps.Id
			INNER JOIN [dbo].[CompanyNumberLOBGrid] lob
				ON lob.CompanyNumberId = s.CompanyNumberId
				AND lob.MulitLOBCode = ps.Code 
		WHERE
			NOT EXISTS(SELECT * FROM dbo.[SubmissionLineOfBusinessChildren] WHERE SubmissionId = i.SubmissionId AND [CompanyNumberLOBGridId] = lob.Id)
GO

IF EXISTS(select * from sys.triggers where name = 'Web2014SubmissionPackagePolicySymbolRemove')
	DROP TRIGGER dbo.Web2014SubmissionPackagePolicySymbolRemove
GO
CREATE TRIGGER dbo.Web2014SubmissionPackagePolicySymbolRemove ON [dbo].[SubmissionPackagePolicySymbol] AFTER DELETE 
AS
	IF EXISTS(SELECT * FROM DELETED)
		DELETE [dbo].[SubmissionLineOfBusinessChildren]
		FROM
			[dbo].[Submission] s
			INNER JOIN DELETED d
				ON d.SubmissionId = s.Id
			INNER JOIN [dbo].[PolicySymbol] ps
				ON d.PolicySymbolId = ps.Id
			INNER JOIN [dbo].[CompanyNumberLOBGrid] lob
				ON lob.CompanyNumberId = s.CompanyNumberId
				AND lob.MulitLOBCode = ps.Code 
			INNER JOIN [dbo].[SubmissionLineOfBusinessChildren] slob
				ON slob.SubmissionId = s.Id
				AND slob.CompanyNumberLOBGridId = lob.Id
GO