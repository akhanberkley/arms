IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'SubmissionViewType')
BEGIN
	CREATE TABLE dbo.SubmissionField
		(
		Id uniqueidentifier NOT NULL,
		PropertyName varchar(50) NOT NULL,
		DisplayLabel varchar(50) NOT NULL,
		ColumnHeading varchar(50) NOT NULL
	  CONSTRAINT [PK_SubmissionField] PRIMARY KEY CLUSTERED 
		(
			[Id]
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	CREATE TABLE dbo.SubmissionViewType
		(
		Id uniqueidentifier NOT NULL,
		Name varchar(50) NOT NULL
	  CONSTRAINT [PK_SubmissionViewType] PRIMARY KEY CLUSTERED 
		(
			[Id]
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	CREATE TABLE dbo.SubmissionViewField
		(
		CompanyNumberId int NOT NULL,
		SubmissionViewTypeId uniqueidentifier NOT NULL,
		SubmissionFieldId uniqueidentifier NOT NULL,
		Sequence smallint NOT NULL,
		ColumnHeading varchar(50) NULL
	  CONSTRAINT [PK_SubmissionViewField] PRIMARY KEY CLUSTERED 
		(
			CompanyNumberId,
			SubmissionViewTypeId,
			SubmissionFieldId
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.SubmissionViewField ADD CONSTRAINT FK_SubmissionViewField_CompanyNumber FOREIGN KEY (CompanyNumberId) REFERENCES dbo.CompanyNumber (Id)
	ALTER TABLE dbo.SubmissionViewField ADD CONSTRAINT FK_SubmissionViewField_SubmissionViewType FOREIGN KEY (SubmissionViewTypeId) REFERENCES dbo.SubmissionViewType (Id)
	ALTER TABLE dbo.SubmissionViewField ADD CONSTRAINT FK_SubmissionViewField_SubmissionField FOREIGN KEY (SubmissionFieldId) REFERENCES dbo.SubmissionField (Id)
END
--This was the original attempt at setting up the schema - this table is not needed anymore (being replaced by the two below)
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'CompanyNumberSubmissionTypeSubmissionField')
BEGIN
	ALTER TABLE dbo.CompanyNumberSubmissionTypeSubmissionField DROP CONSTRAINT FK_CompanyNumberSubmissionTypeSubmissionField_CompanyNumber
	ALTER TABLE dbo.CompanyNumberSubmissionTypeSubmissionField DROP CONSTRAINT FK_CompanyNumberSubmissionTypeSubmissionField_SubmissionType
	ALTER TABLE dbo.CompanyNumberSubmissionTypeSubmissionField DROP CONSTRAINT FK_CompanyNumberSubmissionTypeSubmissionField_SubmissionField
	DROP TABLE dbo.CompanyNumberSubmissionTypeSubmissionField
END
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'CompanyNumberSubmissionField')
BEGIN
	CREATE TABLE dbo.CompanyNumberSubmissionField
		(
		CompanyNumberId int NOT NULL,
		SubmissionFieldId uniqueidentifier NOT NULL,
		[Enabled] bit NOT NULL,
		DisplayLabel varchar(50) NULL
	  CONSTRAINT [PK_CompanyNumberSubmissionField] PRIMARY KEY CLUSTERED 
		(
			CompanyNumberId,
			SubmissionFieldId
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.CompanyNumberSubmissionField ADD CONSTRAINT FK_CompanyNumberSubmissionField_CompanyNumber FOREIGN KEY (CompanyNumberId) REFERENCES dbo.CompanyNumber (Id)
	ALTER TABLE dbo.CompanyNumberSubmissionField ADD CONSTRAINT FK_CompanyNumberSubmissionField_SubmissionField FOREIGN KEY (SubmissionFieldId) REFERENCES dbo.SubmissionField (Id)
END
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'CompanyNumberSubmissionFieldSubmissionType')
BEGIN
	CREATE TABLE dbo.CompanyNumberSubmissionFieldSubmissionType
		(
		CompanyNumberId int NOT NULL,
		SubmissionFieldId uniqueidentifier NOT NULL,
		SubmissionTypeId int NOT NULL,
		[Required] bit NOT NULL,
		VisibleOnAdd bit NOT NULL
	  CONSTRAINT [PK_CompanyNumberSubmissionFieldSubmissionType] PRIMARY KEY CLUSTERED 
		(
			CompanyNumberId,
			SubmissionFieldId,
			SubmissionTypeId
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.CompanyNumberSubmissionFieldSubmissionType ADD CONSTRAINT FK_CompanyNumberSubmissionFieldSubmissionType_CompanyNumberSubmissionField FOREIGN KEY (CompanyNumberId, SubmissionFieldId) REFERENCES dbo.CompanyNumberSubmissionField (CompanyNumberId, SubmissionFieldId)
	ALTER TABLE dbo.CompanyNumberSubmissionFieldSubmissionType ADD CONSTRAINT FK_CompanyNumberSubmissionFieldSubmissionType_SubmissionType FOREIGN KEY (SubmissionTypeId) REFERENCES dbo.SubmissionType (Id)
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'CompanyNumberSubmissionTypeSubmissionField')
BEGIN
	CREATE TABLE dbo.CompanyNumberSubmissionTypeSubmissionField
		(
		CompanyNumberId int NOT NULL,
		SubmissionTypeId int NOT NULL,
		SubmissionFieldId uniqueidentifier NOT NULL,
		[Required] bit NOT NULL,
		VisibleOnAdd bit NOT NULL,
		DisplayLabel varchar(50) NULL
	  CONSTRAINT [PK_CompanyNumberSubmissionTypeSubmissionField] PRIMARY KEY CLUSTERED 
		(
			CompanyNumberId,
			SubmissionTypeId,
			SubmissionFieldId
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.CompanyNumberSubmissionTypeSubmissionField ADD CONSTRAINT FK_CompanyNumberSubmissionTypeSubmissionField_CompanyNumber FOREIGN KEY (CompanyNumberId) REFERENCES dbo.CompanyNumber (Id)
	ALTER TABLE dbo.CompanyNumberSubmissionTypeSubmissionField ADD CONSTRAINT FK_CompanyNumberSubmissionTypeSubmissionField_SubmissionType FOREIGN KEY (SubmissionTypeId) REFERENCES dbo.SubmissionType (Id)
	ALTER TABLE dbo.CompanyNumberSubmissionTypeSubmissionField ADD CONSTRAINT FK_CompanyNumberSubmissionTypeSubmissionField_SubmissionField FOREIGN KEY (SubmissionFieldId) REFERENCES dbo.SubmissionField (Id)
END

--IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'SubmissionStatusType')
--BEGIN
--	CREATE TABLE dbo.SubmissionStatusType
--		(
--		Id uniqueidentifier NOT NULL,
--		Name varchar(50) NOT NULL
--	  CONSTRAINT [PK_SubmissionStatusType] PRIMARY KEY CLUSTERED 
--		(
--			[Id]
--		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--		) ON [PRIMARY]

--	ALTER TABLE [dbo].[CompanyNumberSubmissionStatus] 
--		ADD SubmissionStatusTypeId uniqueidentifier NULL
--		CONSTRAINT FK_CompanyNumberSubmissionStatus_SubmissionStatusType FOREIGN KEY (SubmissionStatusTypeId) REFERENCES dbo.SubmissionStatusType (Id)
--END
--Removing this functionality
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'SubmissionStatusType')
BEGIN
	ALTER TABLE [dbo].[CompanyNumberSubmissionStatus] DROP CONSTRAINT FK_CompanyNumberSubmissionStatus_SubmissionStatusType
	ALTER TABLE [dbo].[CompanyNumberSubmissionStatus] DROP COLUMN SubmissionStatusTypeId
	DROP TABLE dbo.SubmissionStatusType
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'SubmissionClassCode')
BEGIN
	CREATE TABLE dbo.SubmissionClassCode
		(
		SubmissionId int NOT NULL,
		ClassCodeId int NOT NULL,
	  CONSTRAINT [PK_SubmissionClassCode] PRIMARY KEY CLUSTERED 
		(
			SubmissionId,
			ClassCodeId
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.SubmissionClassCode  ADD CONSTRAINT [FK_SubmissionClassCode_ClassCode] FOREIGN KEY([ClassCodeId]) REFERENCES [dbo].[ClassCode] ([Id])
	ALTER TABLE dbo.SubmissionClassCode  ADD CONSTRAINT [FK_SubmissionClassCode_Submission] FOREIGN KEY([SubmissionId]) REFERENCES [dbo].[Submission] ([Id])
END


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'PersonUnderwritingRole')
BEGIN
	CREATE TABLE dbo.PersonUnderwritingRole
		(
		PersonId int NOT NULL,
		UnderwritingRoleId int NOT NULL,
	  CONSTRAINT [PK_PersonUnderwritingRole] PRIMARY KEY CLUSTERED 
		(
			PersonId,
			UnderwritingRoleId
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.PersonUnderwritingRole  ADD CONSTRAINT [FK_PersonUnderwritingRole_Person] FOREIGN KEY([PersonId]) REFERENCES [dbo].[Person] ([Id])
	ALTER TABLE dbo.PersonUnderwritingRole  ADD CONSTRAINT [FK_PersonUnderwritingRole_UnderwritingRole] FOREIGN KEY([UnderwritingRoleId]) REFERENCES [dbo].[UnderwritingRole] ([Id])
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'PolicySymbol')
BEGIN
	CREATE TABLE dbo.PolicySymbol
		(
		Id uniqueidentifier NOT NULL,
		CompanyNumberId int NOT NULL,
		Code varchar(25) NOT NULL,
		[Description] varchar(100) NOT NULL,
		RetirementDate datetime NULL
	  CONSTRAINT [PK_PolicySymbol] PRIMARY KEY CLUSTERED 
		(
			Id
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.PolicySymbol ADD CONSTRAINT FK_PolicySymbol_CompanyNumber FOREIGN KEY (CompanyNumberId) REFERENCES dbo.CompanyNumber (Id)
END
IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'PrimaryOption' AND Object_ID = Object_ID(N'PolicySymbol'))
BEGIN
	ALTER TABLE [dbo].[PolicySymbol] ADD PrimaryOption bit NOT NULL DEFAULT(0)
	ALTER TABLE [dbo].[PolicySymbol] ADD PackageOption bit NOT NULL DEFAULT(0)
END

--Agency query improvements
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_AgencyLineOfBusinessPersonUnderwritingRolePerson_UnderwritingRoleId' AND object_id = OBJECT_ID('AgencyLineOfBusinessPersonUnderwritingRolePerson'))
	CREATE NONCLUSTERED INDEX [IX_AgencyLineOfBusinessPersonUnderwritingRolePerson_UnderwritingRoleId]
		ON [dbo].[AgencyLineOfBusinessPersonUnderwritingRolePerson] ([UnderwritingRoleId]) INCLUDE ([AgencyId],[LineOfBusinessId],[PersonId],[Primary])

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'ExpirationDate' AND Object_ID = Object_ID(N'CompanyNumberAttribute'))
BEGIN
	ALTER TABLE [dbo].[CompanyNumberAttribute] ADD VisibleOnAdd bit NOT NULL DEFAULT(1)
	ALTER TABLE [dbo].[CompanyNumberAttribute] ADD ExpirationDate datetime NULL
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'ExpirationDate' AND Object_ID = Object_ID(N'CompanyNumberCodeType'))
BEGIN
	ALTER TABLE [dbo].[CompanyNumberCodeType] ADD VisibleOnAdd bit NOT NULL DEFAULT(1)
	ALTER TABLE [dbo].[CompanyNumberCodeType] ADD ExpirationDate datetime NULL
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'DuplicateOfSubmissionId' AND Object_ID = Object_ID(N'Submission'))
BEGIN
	ALTER TABLE [dbo].[Submission] ADD DuplicateOfSubmissionId int NULL
	ALTER TABLE [dbo].[Submission] ADD CONSTRAINT [FK_Submission_Submission_Duplicate] FOREIGN KEY([DuplicateOfSubmissionId]) REFERENCES [dbo].[Submission] ([Id])
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'PolicySymbolId' AND Object_ID = Object_ID(N'Submission'))
BEGIN
	ALTER TABLE [dbo].[Submission] ADD PolicySymbolId uniqueidentifier NULL
	ALTER TABLE [dbo].[Submission] ADD CONSTRAINT [FK_Submission_PolicySymbol] FOREIGN KEY(PolicySymbolId) REFERENCES [dbo].[PolicySymbol] ([Id])
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'PriorCarrierId' AND Object_ID = Object_ID(N'Submission'))
BEGIN
	ALTER TABLE [dbo].[Submission] ADD PriorCarrierId int NULL
	ALTER TABLE [dbo].[Submission] ADD CONSTRAINT [FK_Submission_OtherCarrier_PriorCarrier] FOREIGN KEY(PriorCarrierId) REFERENCES [dbo].[OtherCarrier] ([Id])

	ALTER TABLE [dbo].[Submission] ADD PriorCarrierPremium money NULL
END

--IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'AdditionalInterestType')
--BEGIN
--	CREATE TABLE dbo.AdditionalInterestType
--		(
--		Id uniqueidentifier NOT NULL,
--		Name varchar(50) NOT NULL
--	  CONSTRAINT [PK_AdditionalInterestType] PRIMARY KEY CLUSTERED 
--		(
--			[Id]
--		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--		) ON [PRIMARY]

--	CREATE TABLE dbo.CompanyNumberAdditionalInterestType
--		(
--		CompanyNumberId int NOT NULL,
--		AdditionalInterestTypeId uniqueidentifier NOT NULL
--	  CONSTRAINT [PK_CompanyNumberAdditionalInterestType] PRIMARY KEY CLUSTERED 
--		(
--			CompanyNumberId,
--			AdditionalInterestTypeId
--		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--		) ON [PRIMARY]

--	ALTER TABLE dbo.CompanyNumberAdditionalInterestType ADD CONSTRAINT FK_CompanyNumberAdditionalInterestType_CompanyNumber FOREIGN KEY (CompanyNumberId) REFERENCES dbo.CompanyNumber (Id)

--	CREATE TABLE dbo.SubmissionAdditionalInterest
--		(
--		Id uniqueidentifier NOT NULL,
--		SubmissionId int NOT NULL,
--		AdditionalInterestTypeId uniqueidentifier NOT NULL,
--		Name varchar(250) NOT NULL
--	  CONSTRAINT [PK_SubmissionAdditionalInterest] PRIMARY KEY CLUSTERED 
--		(
--			Id
--		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--		) ON [PRIMARY]
	
--	ALTER TABLE dbo.SubmissionAdditionalInterest ADD CONSTRAINT FK_SubmissionAdditionalInterest_Submission FOREIGN KEY (SubmissionId) REFERENCES dbo.Submission (Id)
--	ALTER TABLE dbo.SubmissionAdditionalInterest ADD CONSTRAINT FK_SubmissionAdditionalInterest_AdditionalInterestType FOREIGN KEY (AdditionalInterestTypeId) REFERENCES dbo.AdditionalInterestType (Id)
--END
--IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'CountryId' AND Object_ID = Object_ID(N'SubmissionAdditionalInterest'))
--BEGIN
--	ALTER TABLE [dbo].[SubmissionAdditionalInterest] ADD CountryId int NULL

--	ALTER TABLE dbo.[SubmissionAdditionalInterest] ADD CONSTRAINT FK_SubmissionAdditionalInterest_Country FOREIGN KEY (CountryId) REFERENCES dbo.Country (Id)
--END
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'AdditionalInterestType')
BEGIN
	--Taking out additional interests pieces (storing additional names in CMS)
	ALTER TABLE dbo.SubmissionAdditionalInterest DROP CONSTRAINT FK_SubmissionAdditionalInterest_Submission
	ALTER TABLE dbo.SubmissionAdditionalInterest DROP CONSTRAINT FK_SubmissionAdditionalInterest_AdditionalInterestType
	ALTER TABLE dbo.[SubmissionAdditionalInterest] DROP CONSTRAINT FK_SubmissionAdditionalInterest_Country
	DROP TABLE dbo.SubmissionAdditionalInterest
	DROP TABLE dbo.CompanyNumberAdditionalInterestType
	DROP TABLE dbo.AdditionalInterestType
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'APSAgencyUrl' AND Object_ID = Object_ID(N'CompanyParameter'))
BEGIN
	ALTER TABLE [dbo].[CompanyParameter] ADD APSAgencyUrl varchar(200) NULL
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'ClientField')
BEGIN
	CREATE TABLE dbo.ClientField
		(
		Id uniqueidentifier NOT NULL,
		PropertyName varchar(50) NOT NULL,
		DisplayLabel varchar(50) NOT NULL,
		ColumnHeading varchar(50) NOT NULL
	  CONSTRAINT [PK_ClientField] PRIMARY KEY CLUSTERED 
		(
			[Id]
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	CREATE TABLE dbo.CompanyNumberClientField
		(
		CompanyNumberId int NOT NULL,
		ClientFieldId uniqueidentifier NOT NULL,
		[Required] bit NOT NULL,
		VisibleOnAdd bit NOT NULL,
		DisplayLabel varchar(50) NULL
	  CONSTRAINT [PK_CompanyNumberClientField] PRIMARY KEY CLUSTERED 
		(
			CompanyNumberId,
			ClientFieldId
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.CompanyNumberClientField ADD CONSTRAINT FK_CompanyNumberClientField_CompanyNumber FOREIGN KEY (CompanyNumberId) REFERENCES dbo.CompanyNumber (Id)
	ALTER TABLE dbo.CompanyNumberClientField ADD CONSTRAINT FK_CompanyNumberClientField_ClientField FOREIGN KEY (ClientFieldId) REFERENCES dbo.ClientField (Id)
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'AssociationSubmissionId' AND Object_ID = Object_ID(N'Submission'))
BEGIN
	ALTER TABLE [dbo].[Submission] ADD AssociationSubmissionId int NULL

	ALTER TABLE dbo.Submission ADD CONSTRAINT FK_Submission_AssociationSubmission FOREIGN KEY (AssociationSubmissionId) REFERENCES dbo.Submission (Id)
END
IF EXISTS(SELECT * FROM sys.indexes WHERE name='IX_Submission_AssociationSubmission' AND object_id = OBJECT_ID('Submission'))
BEGIN
	DROP INDEX IX_Submission_AssociationSubmission ON dbo.Submission
END
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_Submission_AssociationSubmissionId' AND object_id = OBJECT_ID('Submission'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_Submission_AssociationSubmissionId ON [dbo].[Submission] ([AssociationSubmissionId]) INCLUDE([ClientCoreClientId])
END
IF EXISTS(select * from sys.triggers where name = 'Web2014SubmissionAssociationSubmissionId')
	DROP TRIGGER dbo.Web2014SubmissionAssociationSubmissionId
GO
DISABLE TRIGGER [Update_SubmissionHistory] ON dbo.Submission
GO
DISABLE TRIGGER [Update_SubmissionPolicyNumberDigitsOnly] ON dbo.Submission
GO
UPDATE dbo.Submission SET AssociationSubmissionId = Id WHERE AssociationSubmissionId IS NULL
GO
ENABLE TRIGGER [Update_SubmissionHistory] ON dbo.Submission
GO
ENABLE TRIGGER [Update_SubmissionPolicyNumberDigitsOnly] ON dbo.Submission
GO
CREATE TRIGGER dbo.Web2014SubmissionAssociationSubmissionId ON [dbo].[Submission] AFTER INSERT 
AS
	UPDATE 
		dbo.Submission
	SET
		AssociationSubmissionId = Id
	WHERE
		Id IN (SELECT Id FROM INSERTED WHERE AssociationSubmissionId IS NULL)
GO

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_Submission_DuplicateOfSubmissionId' AND object_id = OBJECT_ID('Submission'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Submission_DuplicateOfSubmissionId] ON [dbo].[Submission] ([DuplicateOfSubmissionId])
END
IF EXISTS(SELECT * FROM sys.indexes WHERE name='IX_Submission_SubmissionNumber' AND object_id = OBJECT_ID('Submission'))
BEGIN
	DROP INDEX IX_Submission_SubmissionNumber ON [dbo].[Submission]
END
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_Submission_SubmissionNumberWithAssociationSubmissionId' AND object_id = OBJECT_ID('Submission'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_Submission_SubmissionNumberWithAssociationSubmissionId ON [dbo].[Submission] ([CompanyNumberId],[SubmissionNumber],[Sequence]) INCLUDE ([AssociationSubmissionId])
END
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_Submission_PolicyNumber' AND object_id = OBJECT_ID('Submission'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_Submission_PolicyNumber ON [dbo].[Submission] ([CompanyNumberId],[PolicyNumber])
END
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_Submission_SubmissionNumberPolicyNumber' AND object_id = OBJECT_ID('Submission'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_Submission_SubmissionNumberPolicyNumber ON [dbo].[Submission] ([CompanyNumberId],[SubmissionNumber],[PolicyNumber])
END
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_Submission_AgencyId' AND object_id = OBJECT_ID('Submission'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Submission_AgencyId] ON [dbo].[Submission] ([CompanyNumberId], [AgencyId])
END
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'SubmissionAddress')
BEGIN
	CREATE TABLE dbo.SubmissionAddress
		(
		Id uniqueidentifier NOT NULL,
		SubmissionId int NOT NULL,
		ClientCoreAddressId int NULL,
		Address1 varchar(200) NULL,
		Address2 varchar(200) NULL,
		City varchar(100) NULL,
		[State] varchar(20) NULL,
		County varchar(100) NULL,
		PostalCode varchar(20) NULL,
		CountryId int NULL,
		[Latitude] [decimal](9, 6) NULL,
		[Longitude] [decimal](9, 6) NULL,
		EntryBy varchar(50) NOT NULL,
		EntryDt datetime NOT NULL,
		ModifiedBy varchar(50) NULL,
		ModifiedDt datetime NULL
	  CONSTRAINT [PK_SubmissionAddress] PRIMARY KEY CLUSTERED 
		(
			Id
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	
	ALTER TABLE dbo.SubmissionAddress ADD CONSTRAINT FK_SubmissionAddress_Submission FOREIGN KEY (SubmissionId) REFERENCES dbo.Submission (Id)
	ALTER TABLE dbo.SubmissionAddress ADD CONSTRAINT FK_SubmissionAddress_Country FOREIGN KEY (CountryId) REFERENCES dbo.Country (Id)
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'SubmissionAdditionalName')
BEGIN
	CREATE TABLE dbo.SubmissionAdditionalName
		(
		Id uniqueidentifier NOT NULL,
		SubmissionId int NOT NULL,
		ClientCoreSequenceNumber int NULL,
		BusinessName varchar(500) NULL,
		EntryBy varchar(50) NOT NULL,
		EntryDt datetime NOT NULL,
		ModifiedBy varchar(50) NULL,
		ModifiedDt datetime NULL
	  CONSTRAINT [PK_SubmissionAdditionalName] PRIMARY KEY CLUSTERED 
		(
			Id
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]
	
	ALTER TABLE dbo.SubmissionAdditionalName ADD CONSTRAINT FK_SubmissionAdditionalName_Submission FOREIGN KEY (SubmissionId) REFERENCES dbo.Submission (Id)
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'Description' AND Object_ID = Object_ID(N'Role'))
BEGIN
	ALTER TABLE [dbo].[Role] ADD [Description] varchar(200) NULL
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'UserType')
BEGIN
	CREATE TABLE dbo.UserType
		(
		Id uniqueidentifier NOT NULL,
		Name varchar(25) NOT NULL
	  CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED 
		(
			[Id]
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	INSERT INTO dbo.UserType VALUES ('6EFE622D-2D42-47D5-B8DC-42274A744869', 'User')
	INSERT INTO dbo.UserType VALUES ('B01E48A7-929A-4B98-A9FB-E74DE5660DC3', 'Read Only')
	INSERT INTO dbo.UserType VALUES ('DC840769-BD1B-415F-A760-8539285ED1E0', 'System Administrator')

	ALTER TABLE [dbo].[User] ADD UserTypeId uniqueidentifier NOT NULL CONSTRAINT DF_User_UserTypeId DEFAULT '6EFE622D-2D42-47D5-B8DC-42274A744869'
	ALTER TABLE [dbo].[User] ADD CONSTRAINT FK_User_UserType FOREIGN KEY (UserTypeId) REFERENCES dbo.UserType (Id)
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'Active' AND Object_ID = Object_ID(N'Company'))
BEGIN
	ALTER TABLE [dbo].Company ADD Active bit NOT NULL CONSTRAINT DF_Company_Active DEFAULT 1
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'ClientExperianSearchEndPointUrl' AND Object_ID = Object_ID(N'CompanyClientAdvancedSearch'))
BEGIN
	ALTER TABLE [dbo].CompanyClientAdvancedSearch ADD ClientExperianSearchEndPointUrl varchar(255) NULL

	EXEC('UPDATE [dbo].[CompanyClientAdvancedSearch] SET ClientExperianSearchEndPointUrl = REPLACE(ClientAdvSearchEndPointUrl, ''AdvCliSearchWS/'', ''creditreportsupplemental'')')
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'ExcludeValueOnCopy' AND Object_ID = Object_ID(N'CompanyNumberAttribute'))
BEGIN
	ALTER TABLE [dbo].[CompanyNumberAttribute] ADD ExcludeValueOnCopy bit NOT NULL DEFAULT(0)
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'ExcludeValueOnCopy' AND Object_ID = Object_ID(N'CompanyNumberCodeType'))
BEGIN
	ALTER TABLE [dbo].[CompanyNumberCodeType] ADD ExcludeValueOnCopy bit NOT NULL DEFAULT(0)
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_Agency_CompanyNumberAgencyNumber' AND object_id = OBJECT_ID('Agency'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_Agency_CompanyNumberAgencyNumber ON [dbo].[Agency] ([CompanyNumberId],[AgencyNumber])
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_Agency_CompanyNumberAgencyName' AND object_id = OBJECT_ID('Agency'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_Agency_CompanyNumberAgencyName ON [dbo].[Agency] ([CompanyNumberId],[AgencyName])
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'AgencyAgencyBranch')
BEGIN
	CREATE TABLE dbo.AgencyAgencyBranch
		(
		AgencyId int NOT NULL,
		AgencyBranchId int NOT NULL,
	  CONSTRAINT [PK_AgencyAgencyBranch] PRIMARY KEY CLUSTERED 
		(
			AgencyId,
			AgencyBranchId
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.AgencyAgencyBranch  ADD CONSTRAINT [FK_AgencyAgencyBranch_Agency] FOREIGN KEY([AgencyId]) REFERENCES [dbo].[Agency] ([Id])
	ALTER TABLE dbo.AgencyAgencyBranch  ADD CONSTRAINT [FK_AgencyAgencyBranch_AgencyBranch] FOREIGN KEY([AgencyBranchId]) REFERENCES [dbo].[AgencyBranch] ([Id])

	INSERT INTO dbo.AgencyAgencyBranch
		(AgencyId, AgencyBranchId)
	SELECT
		Id,
		AgencyBranchId
	FROM
		dbo.Agency
	WHERE
		AgencyBranchId IS NOT NULL
END
--Triggers to support old column until we can get rid of it
IF EXISTS(select * from sys.triggers where name = 'Web2014AgencyBranchAdd')
	DROP TRIGGER dbo.Web2014AgencyBranchAdd
GO
CREATE TRIGGER dbo.Web2014AgencyBranchAdd ON [dbo].AgencyAgencyBranch AFTER INSERT 
AS
	IF EXISTS(SELECT * FROM INSERTED)
		UPDATE
			dbo.Agency
		SET
			AgencyBranchId = (SELECT TOP 1 AgencyBranchId FROM dbo.AgencyAgencyBranch WHERE AgencyId = Id)
		WHERE
			Id IN (SELECT AgencyId FROM INSERTED)
GO

IF EXISTS(select * from sys.triggers where name = 'Web2014AgencyBranchRemove')
	DROP TRIGGER dbo.Web2014AgencyBranchRemove
GO
CREATE TRIGGER dbo.Web2014AgencyBranchRemove ON [dbo].AgencyAgencyBranch AFTER DELETE 
AS
	IF EXISTS(SELECT * FROM DELETED)
		UPDATE
			dbo.Agency
		SET
			AgencyBranchId = (SELECT TOP 1 AgencyBranchId FROM dbo.AgencyAgencyBranch WHERE AgencyId = Id)
		WHERE
			Id IN (SELECT AgencyId FROM DELETED)
GO

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'PolicyMod' AND Object_ID = Object_ID(N'Submission'))
BEGIN
	ALTER TABLE [dbo].[Submission] ADD PolicyMod smallint NULL
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_SubmissionAddress_SubmissionId' AND object_id = OBJECT_ID('SubmissionAddress'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_SubmissionAddress_SubmissionId ON [dbo].[SubmissionAddress] ([SubmissionId])
END
IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_SubmissionAdditionalName_SubmissionId' AND object_id = OBJECT_ID('SubmissionAdditionalName'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_SubmissionAdditionalName_SubmissionId ON [dbo].[SubmissionAdditionalName] ([SubmissionId])
END
--IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_SubmissionAdditionalInterest_SubmissionId' AND object_id = OBJECT_ID('SubmissionAdditionalInterest'))
--BEGIN
--	CREATE NONCLUSTERED INDEX IX_SubmissionAdditionalInterest_SubmissionId ON [dbo].[SubmissionAdditionalInterest] ([SubmissionId])
--END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'SummarySubmissionFieldId' AND Object_ID = Object_ID(N'CompanyNumberSubmissionType'))
BEGIN
	ALTER TABLE [dbo].[CompanyNumberSubmissionType] ADD SummarySubmissionFieldId uniqueidentifier NULL

	ALTER TABLE dbo.[CompanyNumberSubmissionType] ADD CONSTRAINT FK_CompanyNumberSubmissionType_CompanyNumberSubmissionField FOREIGN KEY (CompanyNumberId, SummarySubmissionFieldId) REFERENCES dbo.CompanyNumberSubmissionField (CompanyNumberId, SubmissionFieldId)
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'GLCodeValue' AND Object_ID = Object_ID(N'ClassCode'))
BEGIN
	ALTER TABLE [dbo].[ClassCode] ADD GLCodeValue varchar(50) NULL

	EXEC('UPDATE dbo.ClassCode SET GLCodeValue = CodeValue')
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name='IX_ClassCode_CompanyNumberCodeValue' AND object_id = OBJECT_ID('ClassCode'))
BEGIN
	CREATE NONCLUSTERED INDEX IX_ClassCode_CompanyNumberCodeValue ON [dbo].[ClassCode] ([CompanyNumberId],[CodeValue]) INCLUDE ([GLCodeValue],[Description],[SicCodeId],[NaicsCodeId])
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'Description' AND Object_ID = Object_ID(N'SubmissionViewType'))
BEGIN
	ALTER TABLE [dbo].[SubmissionViewType] ADD [Description] varchar(200) NULL
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'AgencyId' AND Object_ID = Object_ID(N'User') AND system_type_id = 56)
BEGIN
	ALTER TABLE [dbo].[User] ALTER COLUMN AgencyId int NULL
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'DisplayOrder' AND Object_ID = Object_ID(N'CompanyNumberHazardGradeType'))
BEGIN
	ALTER TABLE [dbo].[CompanyNumberHazardGradeType] ADD DisplayOrder smallint NOT NULL DEFAULT(0)
END
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'SubmissionPackagePolicySymbol')
BEGIN
	CREATE TABLE dbo.SubmissionPackagePolicySymbol
		(
		SubmissionId int NOT NULL,
		PolicySymbolId uniqueidentifier NOT NULL,
	  CONSTRAINT [PK_SubmissionPackagePolicySymbol] PRIMARY KEY CLUSTERED 
		(
			SubmissionId,
			PolicySymbolId
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.SubmissionPackagePolicySymbol  ADD CONSTRAINT [FK_SubmissionPackagePolicySymbol_Submission] FOREIGN KEY([SubmissionId]) REFERENCES [dbo].[Submission] ([Id])
	ALTER TABLE dbo.SubmissionPackagePolicySymbol  ADD CONSTRAINT [FK_SubmissionPackagePolicySymbol_PolicySymbol] FOREIGN KEY([PolicySymbolId]) REFERENCES [dbo].[PolicySymbol] ([Id])
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'CompanyNumberClientCoreSegment')
BEGIN
	CREATE TABLE dbo.CompanyNumberClientCoreSegment
		(
		CompanyNumberId int NOT NULL,
		Segment varchar(50) NOT NULL,
		[Description] varchar(50) NOT NULL,
		IsDefault bit NOT NULL
	  CONSTRAINT [PK_CompanyNumberClientCoreSegment] PRIMARY KEY CLUSTERED 
		(
			CompanyNumberId,
			Segment
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.CompanyNumberClientCoreSegment ADD CONSTRAINT FK_CompanyNumberClientCoreSegment_CompanyNumber FOREIGN KEY (CompanyNumberId) REFERENCES dbo.CompanyNumber (Id)
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'CompanyNumberCrossClearance')
BEGIN
	CREATE TABLE dbo.CompanyNumberCrossClearance
		(
		CompanyNumberId int NOT NULL,
		ClearsCompanyNumberId int NOT NULL
	  CONSTRAINT [PK_CompanyNumberCrossClearance] PRIMARY KEY CLUSTERED 
		(
			CompanyNumberId,
			ClearsCompanyNumberId
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		) ON [PRIMARY]

	ALTER TABLE dbo.CompanyNumberCrossClearance ADD CONSTRAINT FK_CompanyNumberCrossClearance_CompanyNumber FOREIGN KEY (CompanyNumberId) REFERENCES dbo.CompanyNumber (Id)
	ALTER TABLE dbo.CompanyNumberCrossClearance ADD CONSTRAINT FK_CompanyNumberCrossClearance_ClearsCompanyNumber FOREIGN KEY (ClearsCompanyNumberId) REFERENCES dbo.CompanyNumber (Id)
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'WfaExceptionLog')
BEGIN
	CREATE TABLE [dbo].[WfaExceptionLog]
		(
			[Id] [int] IDENTITY(1,1) NOT NULL,
			[LoggedDate] [datetime] NOT NULL,
			[ApplicationName] [varchar](100) NOT NULL,
			[ApplicationEnvironment] [varchar](100) NOT NULL,
			[ExceptionInformation] [varchar](max) NOT NULL,
			[StackTrace] [varchar](max) NULL,
			[ExceptionContext] [varchar](max) NULL,
			[WebRequestData] [varchar](max) NULL
	  CONSTRAINT [PK_WfaExceptionLog] PRIMARY KEY CLUSTERED 
		(
			[Id] ASC
		) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	    ) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX IX_WfaExceptionLog_LoggedDate ON [dbo].[WfaExceptionLog] ([LoggedDate] DESC)
END

IF NOT EXISTS(SELECT * FROM SYS.Columns WHERE Name = 'NumberControlId' AND Object_ID = Object_ID(N'PolicySymbol'))
BEGIN
	ALTER TABLE [dbo].[PolicySymbol] ADD NumberControlId int NULL
	ALTER TABLE [dbo].[PolicySymbol] ADD CONSTRAINT [FK_PolicySymbol_NumberControl] FOREIGN KEY([NumberControlId]) REFERENCES [dbo].[NumberControl] ([Id])

	--This index can no longer be unique
	DROP INDEX [IX_NumberControl] ON [dbo].[NumberControl] 
	CREATE NONCLUSTERED INDEX [IX_NumberControl] ON [dbo].[NumberControl] ([CompanyNumberId] ASC, [NumberControlTypeId] ASC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END