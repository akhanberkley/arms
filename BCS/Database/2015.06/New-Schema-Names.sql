--Populate [SubmissionAdditionalName] table with base data
IF NOT EXISTS(SELECT TOP 1 * FROM dbo.[SubmissionAdditionalName])
BEGIN
	INSERT INTO [dbo].[SubmissionAdditionalName] ([Id],[SubmissionId],[ClientCoreSequenceNumber],[EntryBy],[EntryDt],[ModifiedBy],[ModifiedDt])
	SELECT 
		newid()
		,ccn.SubmissionId
		,ccn.[ClientCoreNameId]
		,'Web2014Conversion'
		,getdate()
		,'Web2014Conversion'
		,getdate()
	FROM 
		dbo.Submission s 
		INNER JOIN [dbo].[SubmissionClientCoreClientName] ccn 
			ON s.Id = ccn.SubmissionId
END

--Triggers
IF EXISTS(select * from sys.triggers where name = 'Web2013AdditionalNameAdd')
	DROP TRIGGER dbo.Web2013AdditionalNameAdd
GO
CREATE TRIGGER dbo.Web2013AdditionalNameAdd ON [dbo].[SubmissionClientCoreClientName] AFTER INSERT 
AS
	IF EXISTS(SELECT * FROM INSERTED)
		INSERT INTO [dbo].[SubmissionAdditionalName] ([Id],[SubmissionId],[ClientCoreSequenceNumber],[EntryBy],[EntryDt],[ModifiedBy],[ModifiedDt])
		SELECT
			NEWID()
			,i.SubmissionId
			,i.ClientCoreNameId
			,'Web2014Conversion'
			,getdate()
			,'Web2014Conversion'
			,getdate()
		FROM
			INSERTED i
		WHERE
			NOT EXISTS(SELECT * FROM [dbo].[SubmissionAdditionalName] WHERE SubmissionId = i.SubmissionId AND [ClientCoreSequenceNumber] = i.ClientCoreNameId)
GO

IF EXISTS(select * from sys.triggers where name = 'Web2013AdditionalNameRemove')
	DROP TRIGGER dbo.Web2013AdditionalNameRemove
GO
CREATE TRIGGER dbo.Web2013AdditionalNameRemove ON [dbo].[SubmissionClientCoreClientName] AFTER DELETE 
AS
	IF EXISTS(SELECT * FROM DELETED)
		DELETE [dbo].[SubmissionAdditionalName]
		FROM
			DELETED d
			INNER JOIN [dbo].[SubmissionAdditionalName] sn
				ON d.SubmissionId = sn.SubmissionId
				AND d.ClientCoreNameId = sn.[ClientCoreSequenceNumber]
GO

IF EXISTS(select * from sys.triggers where name = 'Web2014AdditionalNameAdd')
	DROP TRIGGER dbo.Web2014AdditionalNameAdd
GO
CREATE TRIGGER dbo.Web2014AdditionalNameAdd ON [dbo].[SubmissionAdditionalName] AFTER INSERT 
AS
	IF EXISTS(SELECT * FROM INSERTED)
		INSERT INTO [dbo].[SubmissionClientCoreClientName] ([SubmissionId],[ClientCoreNameId])
		SELECT
			i.SubmissionId
			,i.[ClientCoreSequenceNumber]
		FROM
			INSERTED i
		WHERE
			i.[ClientCoreSequenceNumber] IS NOT NULL
			AND NOT EXISTS(SELECT * FROM dbo.[SubmissionClientCoreClientName] WHERE SubmissionId = i.SubmissionId AND [ClientCoreNameId] = i.[ClientCoreSequenceNumber])
GO

IF EXISTS(select * from sys.triggers where name = 'Web2014AdditionalNameRemove')
	DROP TRIGGER dbo.Web2014AdditionalNameRemove
GO
CREATE TRIGGER dbo.Web2014AdditionalNameRemove ON [dbo].[SubmissionAdditionalName] AFTER DELETE 
AS
	IF EXISTS(SELECT * FROM DELETED)
		DELETE [dbo].[SubmissionClientCoreClientName]
		FROM
			DELETED d
			INNER JOIN [dbo].[SubmissionClientCoreClientName] sn
				ON d.SubmissionId = sn.SubmissionId
				AND d.[ClientCoreSequenceNumber] = sn.[ClientCoreNameId]
GO