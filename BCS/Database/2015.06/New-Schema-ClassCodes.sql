--Data Conversion
--GO
--DISABLE TRIGGER dbo.Web2014ClassCodeAdd ON [dbo].[SubmissionClassCode]
--GO
INSERT INTO [dbo].[SubmissionClassCode] ([SubmissionId],[ClassCodeId])
	SELECT DISTINCT
		s.Id
		,ccc.ClassCodeId
	FROM
		dbo.Client c
		INNER JOIN [dbo].[ClientClassCode] ccc
			ON c.Id = ccc.ClientId
		INNER JOIN dbo.Submission s
			ON s.ClientCoreClientId = c.ClientCoreClientId
			AND s.CompanyNumberId = c.CompanyNumberId
	WHERE
		NOT EXISTS(SELECT * FROM [dbo].[SubmissionClassCode] WHERE SubmissionId = s.Id AND ClassCodeId = ccc.ClassCodeId)
GO
--ENABLE TRIGGER dbo.Web2014ClassCodeAdd ON [dbo].[SubmissionClassCode]
--GO

--Class Codes Triggers
IF EXISTS(select * from sys.triggers where name = 'Web2013ClassCodeAdd')
	DROP TRIGGER dbo.Web2013ClassCodeAdd
GO
CREATE TRIGGER dbo.Web2013ClassCodeAdd ON [dbo].[ClientClassCode] AFTER INSERT 
AS
	IF EXISTS(SELECT * FROM INSERTED)
		INSERT INTO [dbo].[SubmissionClassCode] ([SubmissionId],[ClassCodeId])
		SELECT
			s.Id
			,i.ClassCodeId
		FROM
			INSERTED i
			INNER JOIN dbo.Client c
				ON i.ClientId = c.Id
			INNER JOIN dbo.Submission s
				ON c.ClientCoreClientId = s.ClientCoreClientId
				AND c.CompanyNumberId = s.CompanyNumberId
		WHERE
			NOT EXISTS(SELECT * FROM [dbo].[SubmissionClassCode] sc1 INNER JOIN [dbo].[Submission] s1 ON sc1.SubmissionId = s1.Id AND sc1.ClassCodeId = i.ClassCodeId AND s1.ClientCoreClientId = c.ClientCoreClientId)
			AND NOT EXISTS(SELECT * FROM [dbo].[SubmissionClassCode] WHERE SubmissionId = s.Id AND ClassCodeId = i.ClassCodeId)
GO

IF EXISTS(select * from sys.triggers where name = 'Web2013ClassCodeRemove')
	DROP TRIGGER dbo.Web2013ClassCodeRemove
GO
CREATE TRIGGER dbo.Web2013ClassCodeRemove ON [dbo].[ClientClassCode] AFTER DELETE 
AS
	IF EXISTS(SELECT * FROM DELETED)
		DELETE [dbo].[SubmissionClassCode]
		FROM
			DELETED d
			INNER JOIN dbo.Client c
				ON d.ClientId = c.Id
			INNER JOIN dbo.Submission s
				ON c.ClientCoreClientId = s.ClientCoreClientId
				AND c.CompanyNumberId = s.CompanyNumberId
			INNER JOIN [dbo].[SubmissionClassCode] sc
				ON s.Id = sc.SubmissionId
				AND d.ClassCodeId = sc.ClassCodeId
GO

IF EXISTS(select * from sys.triggers where name = 'Web2014ClassCodeAdd')
	DROP TRIGGER dbo.Web2014ClassCodeAdd
GO
CREATE TRIGGER dbo.Web2014ClassCodeAdd ON [dbo].[SubmissionClassCode] AFTER INSERT 
AS
	IF EXISTS(SELECT * FROM INSERTED)
		INSERT INTO [dbo].[ClientClassCode] ([ClientId],[ClassCodeId])
		SELECT
			c.Id
			,i.ClassCodeId
		FROM
			INSERTED i
			INNER JOIN dbo.Submission s
				ON s.Id = i.SubmissionId
			INNER JOIN dbo.Client c
				ON c.ClientCoreClientId = s.ClientCoreClientId
				AND c.CompanyNumberId = s.CompanyNumberId
		WHERE
			NOT EXISTS(SELECT * FROM [dbo].[ClientClassCode] WHERE [ClientId] = c.Id AND [ClassCodeId] = i.ClassCodeId)
GO

IF EXISTS(select * from sys.triggers where name = 'Web2014ClassCodeRemove')
	DROP TRIGGER dbo.Web2014ClassCodeRemove
GO
CREATE TRIGGER dbo.Web2014ClassCodeRemove ON [dbo].[SubmissionClassCode] AFTER DELETE 
AS
	IF EXISTS(SELECT * FROM DELETED)
		DELETE [dbo].[ClientClassCode]
		FROM
			DELETED d
			INNER JOIN dbo.Submission s
				ON s.Id = d.SubmissionId
			INNER JOIN dbo.Client c
				ON c.ClientCoreClientId = s.ClientCoreClientId
				AND c.CompanyNumberId = s.CompanyNumberId
			INNER JOIN [dbo].[ClientClassCode] csc
				ON c.Id = csc.ClientId
				AND csc.ClassCodeId = d.ClassCodeId
		WHERE
			NOT EXISTS(SELECT * FROM [dbo].[SubmissionClassCode] WHERE [ClassCodeId] = d.ClassCodeId AND SubmissionId IN (SELECT Id FROM dbo.Submission WHERE CompanyNumberId = c.CompanyNumberId AND ClientCoreClientId = c.ClientCoreClientId))
GO