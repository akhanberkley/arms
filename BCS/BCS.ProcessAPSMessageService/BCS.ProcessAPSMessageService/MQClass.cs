﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBM.XMS;
using BCS.Biz;
using BCS.Core;
using BCS.Core.Clearance;
using QCI.ExceptionManagement;

namespace BCS.ProcessAPSMessage
{
    public class MQClass
    {
        private IConnection m_objConnection = null;
        private ISession m_objSession = null;
        private IMessageConsumer m_objConsumer = null;

        private string _uri, _hostName, _channel, _companyNumber;
        private int _port;

        public MQClass()
        {

        }

        public MQClass(string name, string hostName, string channel, string port, string companyNumber)
        {
            _uri = string.Format("queue://{0}", name);
            _hostName = hostName;
            _channel = channel;
            _companyNumber = companyNumber;
            Int32.TryParse(port, out _port);
        }

        public void Connect()
        {
            try
            {
                writeData("PolicyStatusSubscriberService Starting");
                writeData("WMQ_HOST_NAME: " + _hostName);
                //writeData("WMQ_QUEUE_MANAGER: " + ConfigurationSettings.Instance.WMQ_QUEUE_MANAGER);
                writeData("WMQ_CHANNEL: " + _channel);
                writeData("WMQ_PORT: " + _port);
                writeData("URI: " + _uri);

                XMSFactoryFactory objFactoryFactory = XMSFactoryFactory.GetInstance(XMSC.CT_WMQ);
                IConnectionFactory objConnectionFactory = objFactoryFactory.CreateConnectionFactory();
                if (_hostName.Length > 0)
                    objConnectionFactory.SetStringProperty(XMSC.WMQ_HOST_NAME, _hostName);
                //if (ConfigurationSettings.Instance.WMQ_QUEUE_MANAGER.Length > 0)
                //    objConnectionFactory.SetStringProperty(XMSC.WMQ_QUEUE_MANAGER, ConfigurationSettings.Instance.WMQ_QUEUE_MANAGER);
                if (_channel.Length > 0)
                    objConnectionFactory.SetStringProperty(XMSC.WMQ_CHANNEL, _channel);
                if (_port != -1)
                    objConnectionFactory.SetIntProperty(XMSC.WMQ_PORT, _port);
                objConnectionFactory.SetIntProperty(XMSC.WMQ_CONNECTION_MODE, XMSC.WMQ_CM_CLIENT);

                //*****************************************************************
                // Create the connection and register an exception listener
                //*****************************************************************
                //m_objConnection = objConnectionFactory.CreateConnection("bcsapp", "bcsapp0924");
                m_objConnection = objConnectionFactory.CreateConnection("bcsapp", "e@gle15");
                // onException is called if a problem occurs with the connection - not if a given message fails.
                m_objConnection.ExceptionListener = new ExceptionListener(this.OnException);

                // ClientAcknowledge will enable the service to only indicate that
                // a message should be removed from the queue after it has been
                // processed successfully.
                m_objSession = m_objConnection.CreateSession(true, AcknowledgeMode.AutoAcknowledge);

                IDestination objQueue = m_objSession.CreateQueue(_uri);

                //*****************************************************************
                // Create the consumer and register an async message listener
                //*****************************************************************
                m_objConsumer = m_objSession.CreateConsumer(objQueue);


                m_objConsumer.MessageListener = new MessageListener(this.OnMessage);
                m_objConnection.Start();
                writeData("PolicyStatusSubscriberService Started Successfully");

                //throw new Exception("Testing");
            }
            catch (Exception ex)
            {
                Exception wrapper = new Exception("Failed to connect to " + _hostName + " Queue", ex);
                //ExceptionManager.Publish(wrapper);
                QCI.ExceptionManagement.ExceptionManager.PublishEmail(wrapper);
                writeData("PolicyStatusSubscriberService Exception");
                writeData(ex.ToString());
            }
        }

        void OnException(Exception ex)
        {
            QCI.ExceptionManagement.ExceptionManager.PublishEmail(ex);
        }

        [System.Runtime.CompilerServices.MethodImplAttribute(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)]
        static void writeData(String data)
        {
            System.IO.StreamWriter outFile = System.IO.File.AppendText(@"c:\temp\APS_SubscriberProcessing.txt");
            outFile.WriteLine(System.DateTime.Now.ToString() + "\t" + data);
            outFile.Close();
        }

        void OnMessage(IMessage msg)
        {
            try
            {
                DataManager dm = Common.GetDataManager();
                Biz.APSXMLLog entry = null;

                try
                {
                    entry = dm.NewAPSXMLLog();
                    string message = ((ITextMessage)msg).Text;

                    entry.CompanyNumber = _companyNumber;
                    entry.XML = message;
                    entry.Errored = false;

                    dm.CommitAll();

                    ProcessManager apsChangeManager = new ProcessManager();
                    apsChangeManager.BeginProcess(message, _companyNumber, entry.Id.ToString());
                }
                catch (Exception ex)
                {
                    // Notify Someone!
                    writeData("PolicyStatusSubscriberService OnMessage Exception");
                    writeData("Exception: ");
                    writeData(ex.ToString());
                    writeData("Current Message: ");
                    writeData(((IBM.XMS.ITextMessage)msg).Text);
                    if (entry != null)
                        entry.Errored = true;
                }

                //This will remove the message from the queue.
                dm.CommitAll();
                m_objSession.Commit();
            }
            catch
            {

            }
        }



    }
}


