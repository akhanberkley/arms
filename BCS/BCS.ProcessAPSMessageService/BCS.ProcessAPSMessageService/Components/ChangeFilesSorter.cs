using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;

namespace BCS.ProcessAPSMessage
{
    // Calls CaseInsensitiveComparer.Compare with the parameters reversed.
    public class ChangeFilesSorter : IComparer
    {
        int IComparer.Compare(Object x, Object y)
        {
            FileInfo file1 = new FileInfo((string)x);
            FileInfo file2 = new FileInfo((string)y);

            return DateTime.Compare(file1.LastWriteTime, file2.LastWriteTime);
        }
    }
}
