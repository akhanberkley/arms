using System;

namespace BCS.ProcessAPSMessage
{
	/// <summary>
	/// Summary description for DefaultValues.
	/// </summary>
	public sealed class APSErrors
	{
		/// <summary>
		/// Private ctor
		/// </summary>
		private APSErrors() {}

        public const string eAgencyUWPersonRole = "BCS/APS Error: Agency U/W Person Role";
        public const string eCompanyPersonnel = "BCS/APS Error: Company Personnel";
        public const string eAgencyPersonnel = "BCS/APS Error: Agency Personnel";
        public const string eCompanyPersonnelInfo = "BCS/APS Error: Company Personnel Info";
        public const string eUnderwritingUnit = "BCS/APS Error: Underwriting Unit";
        public const string eAgency = "BCS/APS Error: Agency";
        public const string eAgencyName = "BCS/APS Error: Agency Name";
        public const string eAgencyUnderwritingUnit = "BCS/APS Error: Agency U/W Unit";
        public const string eAgencylicense = "BCS/APS Error: Agency License";
        public const string eLocation = "BCS/APS Error: Location";
        public const string eLocationCommunication = "BCS/APS Error: Location Communication";
        public const string eLocationAddress = "BCS/APS Error: Location Address";
        public const string eGenericError = "BCS/APS Error";

	}
}
