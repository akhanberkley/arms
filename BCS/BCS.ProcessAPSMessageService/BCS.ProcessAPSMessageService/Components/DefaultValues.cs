using System;

namespace BCS.ProcessAPSMessage
{
	/// <summary>
	/// Summary description for DefaultValues.
	/// </summary>
	public sealed class DefaultValues
	{
		/// <summary>
		/// Private ctor
		/// </summary>
		private DefaultValues() {}

		/// <summary>
		/// Check the web.config file for the DSN.
		/// </summary>
		public static string DSN
		{
			get{return Globals.GetConnectionString("DSN");}
		}

		/// <summary>
		/// Check the web.config file for the DSN.
		/// </summary>
		public static int DefaultObjectCacheTimeInMinutes
		{
			get{return Convert.ToInt32( Globals.GetApplicationSetting("DefaultObjectCacheTimeInMinutes") );}
		}

        /*
        <add key="MailServer" value="smtp.wrberkley.com"/>
		<add key="MailCc" value=""/>
		<add key="SystemIdentifier" value="APS Service"/>
		<add key="MailFrom" value="bcs_aps_subscriber@wrberkley.com"/>
		<add key="MailTo" value="sbulusu@wrberkley.com"/>
        */

        /// <summary>
        /// Check the web.config file for the MailServer.
        /// </summary>
        public static string MailServer
        {
            get { return Globals.GetApplicationSetting("MailServer"); }
        }
        
        /// <summary>
        /// Check the web.config file for the SystemIdentifier.
        /// </summary>
        public static string SystemIdentifier
        {
            get { return Globals.GetApplicationSetting("SystemIdentifier"); }
        }

        /// <summary>
        /// Check the web.config file for the MailFrom.
        /// </summary>
        public static string MailFrom
        {
            get { return Globals.GetApplicationSetting("MailFrom"); }
        }

        /// <summary>
        /// Check the web.config file for the MailTo.
        /// </summary>
        public static string MailTo
        {
            get { return Globals.GetApplicationSetting("MailTo"); }
        }
        
        /// <summary>
        /// Check the web.config file for the MailCc.
        /// </summary>
        public static string MailCC
        {
            get { return Globals.GetApplicationSetting("MailCc"); }
        }
        
        /// <summary>
        /// Check the web.config file for the UseOtherCriteriaIfIdFails.
        /// </summary>
        public static bool UseOtherCriteriaIfIdFails
        {
            get { return Convert.ToBoolean(Globals.GetApplicationSetting("UseOtherCriteriaIfIdFails")); }
        }

        /// <summary>
        /// Check the web.config file for the UseOtherCriteriaIfIdFails.
        /// </summary>
        public static string MappingFile
        {
            get { return string.Format("{0}{1}", Globals.GetApplicationSetting("APSServiceDirectory"), "mappings.xml"); }
        }

        public static string ConfigFile
        {
            get { return string.Format("{0}{1}", Globals.GetApplicationSetting("APSServiceDirectory"), "Companies.xml"); }
        }

        /// <summary>
        /// Check the web.config file for the UseOtherCriteriaIfIdFails.
        /// </summary>
        public static string XmlDataFilesDirectory
        {
            get { return Globals.GetApplicationSetting("XmlDataFiles"); }
        }

        /// <summary>
        /// Check the web.config file for the UseOtherCriteriaIfIdFails.
        /// </summary>
        public static string LogCacheFile
        {
            get { return string.Format("{0}{1}", Globals.GetApplicationSetting("APSServiceDirectory"), "LogCache.txt"); }
        }

        /// <summary>
        /// Check the app.config file for BCS Service URL.
        /// </summary>
        public static string BCSService1
        {
            get { return Globals.GetApplicationSetting("BCSService1"); }
        }

        /// <summary>
        /// Check the app.config file for BCS Service URL.
        /// </summary>
        public static string BCSService2
        {
            get { return Globals.GetApplicationSetting("BCSService2"); }
        }
 
	}
}
