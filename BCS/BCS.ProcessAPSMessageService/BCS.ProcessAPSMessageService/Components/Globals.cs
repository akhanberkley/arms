using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Timers;
using System.Xml.Serialization;
using Microsoft.Win32;


namespace BCS.ProcessAPSMessage
{
    /// <summary>
    /// Summary description for Globals.
    /// </summary>
    public sealed class Globals
    {
        /// <summary>
        /// Private ctor
        /// </summary>
        private Globals() { }

        /// <summary>
        /// Date Format
        /// </summary>
        public static string DateFormat = "yyyyMMdd";

        /// <summary>
        /// Retrieve appsettings string from the web.config file
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetApplicationSetting(string key)
        {
            if (key != null && key.Length != 0)
            {
                try
                {
                    if (ConfigurationManager.AppSettings[key] != null)
                    {
                        return ConfigurationManager.AppSettings[key].ToString();
                    }
                    else
                    {
                        throw new ArgumentException("ConfigurationManager.AppSettings[key] is null.", "key");
                    }
                }
                catch (Exception ex)
                {
                    string returnMsg = "Caught exception in Globals.GetApplicationSetting trying to retrieve the requested application key from the Web.config file";
                    BTS.LogFramework.LogCentral.Current.LogError(returnMsg, ex);

                    return returnMsg;
                }
            }
            else
            {
                throw new ArgumentException("key equals an empty string.", "key");
            }
        }

        /// <summary>
        /// Retrieve ConnectionString string from the web.config file
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConnectionString(string key)
        {
            if (key != null && key.Length != 0)
            {
                try
                {
                    if (ConfigurationManager.ConnectionStrings[key] != null)
                    {
                        return ConfigurationManager.ConnectionStrings[key].ToString();
                    }
                    else
                    {
                        throw new ArgumentException("ConfigurationManager.ConnectionStrings[key] is null.", "key");
                    }
                }
                catch (Exception ex)
                {
                    string returnMsg = "Caught exception in Globals.GetApplicationSetting trying to retrieve the requested connection string key from the Web.config file";
                    BTS.LogFramework.LogCentral.Current.LogError(returnMsg, ex);

                    return returnMsg;
                }
            }
            else
            {
                throw new ArgumentException("key equals an empty string.", "key");
            }
        }
    }
}
