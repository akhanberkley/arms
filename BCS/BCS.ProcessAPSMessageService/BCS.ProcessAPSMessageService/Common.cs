using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.SqlTypes;
using System.Xml;
using System.Xml.Xsl;

using BCS.Biz;
using BCS.Core.Security;
using structures = BTS.ClientCore.Wrapper.Structures;
using System.Reflection;

namespace BCS.ProcessAPSMessage
{
    /// <summary>
    /// Summary description for Common
    /// </summary>
    public class Common
    {
        private static Biz.DataManager dm = new DataManager(DefaultValues.DSN);

        public Common() { }
        
        public enum SubmissionOptions
        {
            SearchOnly = 1,
            SearchAndAdd,
            AddOnly
        }

        public static Biz.Company GetCompany(Biz.User user)
        {
            DataManager dm = new DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(JoinPath.UserCompany.Columns.UserId, user.Id,OrmLib.MatchType.Exact);
            Biz.UserCompany uc = dm.GetUserCompany();

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(JoinPath.Company.Columns.Id, uc.CompanyId);
            Biz.Company c = dm.GetCompany();
            return c;
        }
  
        #region Sql Data Type Helpers
        public static SqlInt32 ConvertToSqlInt32(object o)
        {
            try
            {
                SqlInt32 r = (SqlInt32)Convert.ToInt32(o);
                return r;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static SqlDateTime ConverToSqlDateTime(string p)
        {
            try
            {
                SqlDateTime r = (SqlDateTime)Convert.ToDateTime(p);
                return r;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        } 
        #endregion

        public static DataManager GetDataManager()
        {
            dm.QueryCriteria.Clear();
            return dm;
        }       

        internal static void VerifyCompanyExists(DataManager dm, Company ucompany)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(JoinPath.Company.Columns.Id, ucompany.Id);
            Company company = dm.GetCompany();
            if (company == null)
                throw new ApplicationException("Company does not exist");

        }

        internal static void verifySubmissionTypeExists(DataManager dm, SubmissionType type)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(JoinPath.SubmissionType.Columns.Id, type.Id);
            SubmissionType o = dm.GetSubmissionType();
            if (o == null)
                throw new ApplicationException("Submission Type does not exist");
        }

        internal static void VerifyAgencyExists(DataManager dm, Agency agency)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(JoinPath.Agency.Columns.Id, agency.Id);
            Agency o = dm.GetAgency();
            if (o == null)
                throw new ApplicationException("Agency does not exist");
        }

        internal static void VerifyAgentExists(DataManager dm, Agent agent)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(JoinPath.Agent.Columns.Id, agent.Id);
            Agent o = dm.GetAgent();
            if (o == null)
                throw new ApplicationException("Agent does not exist");
        }

        internal static void VerifySubmissionStatusExists(DataManager dm, SubmissionStatus status)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(JoinPath.SubmissionStatus.Columns.Id, status.Id);
            SubmissionStatus o = dm.GetSubmissionStatus();
            if (o == null)
                throw new ApplicationException("Submission Status does not exist");
        }

        internal static void VerifySubmissionTokenExists(DataManager dm, SubmissionToken token)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(JoinPath.SubmissionToken.Columns.Id, token.Id);
            SubmissionToken o = dm.GetSubmissionToken();
            if (o == null)
                throw new ApplicationException("Submission Status does not exist");
        }

        public static XmlNode RemoveTypeAttributes(XmlNode doc)
        {
            foreach (XmlNode node in doc.ChildNodes)
            {
                if (node.Attributes != null)
                {
                    node.Attributes.RemoveAll();
                    //foreach (XmlAttribute attr in node.Attributes)
                    //{
                    //    if (attr.Name == "xsi:type" || attr.Name == "xmlns:xsi" || attr.Name == "xmlns:xsd")
                    //    {
                    //        node.Attributes.Remove(attr);
                    //        //break;
                    //    }

                    //}
                    RemoveTypeAttributes(node);
                }
            }
            return doc;
        }

        internal static object GetFromCache(string p)
        {
            return null;
        }

        internal static int ConvertToClientCoreDateTime(object o)
        {
            return Convert.ToInt32(BCS.Core.Clearance.Common.ConvertToClientCoreDateTime(o));
        }

        internal static object GetConvertedObject(object src, Type sqlType)
        {
            //object dst = null;
            if (!sqlType.FullName.StartsWith("System.Data.SqlTypes"))
            {
                throw new ApplicationException("Use only to convert sql types to .NET types");
            }

            // observed all of these have a constructor with parameter type matching the associated .net type. This method is based on that observation
            ConstructorInfo[] constructors = sqlType.GetConstructors();
            foreach (ConstructorInfo var in constructors)
            {
                ParameterInfo[] parameters = var.GetParameters();
                if (parameters.Length == 1)
                {
                    Type returnType = parameters[0].ParameterType;
                    object intermediate = Convert.ChangeType(src, returnType);
                    object returnObj = Activator.CreateInstance(sqlType, intermediate);
                    return returnObj;
                }
            }
            return null;
        }


    }
}