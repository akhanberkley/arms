using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Configuration;
using NameValueCollection = System.Collections.Specialized.NameValueCollection;
using System.IO;

using BCS.Biz;
using BCS.Core;
using BCS.Core.Clearance;
using BTS.LogFramework;
using CacheEngine = BTS.CacheEngine.V2.CacheEngine;

using com.bts.aps.domain;
using com.bts.aps.domain.publish;
using System.Net.Mail;
using System.Text;
using System.Reflection;
using System.Threading;
using System.Security.Principal;

namespace BCS.ProcessAPSMessage
{
    public class ProcessAPSMessageService : System.ServiceProcess.ServiceBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private FileSystemWatcher myWatcher;

        public ProcessAPSMessageService()
        {
            // This call is required by the Windows.Forms Component Designer.
            InitializeComponent();

            // TODO: Add any initialization after the InitComponent call
        }

        // The main entry point for the process
        static void Main()
        {
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ProcessAPSMessageService() };
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // Service1
            // 
            this.ServiceName = "ProcessAPSMessage";

        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Set things in motion so your service can do its work.
        /// </summary>
        protected override void OnStart(string[] args)
        {
#if DEBUG
            System.Diagnostics.Debugger.Launch();
           Thread.Sleep(6000);// used for testing 
#endif
#if !DEBUG
            Thread.Sleep(20000);
#endif
            string filePath = APSXMLFiles;
            DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.Company.Columns.UsesAPS, true);
            CompanyCollection companies = dm.GetCompanyCollection(Biz.FetchPath.Company.CompanyNumber, Biz.FetchPath.Company.CompanyParameter);

            foreach (Biz.Company company in companies)
            {
                Biz.CompanyParameter param = company.CompanyParameters[0];
#if !DEBUG
                if (string.IsNullOrWhiteSpace(param.MQName) && myWatcher == null)
                   {
                        myWatcher = new FileSystemWatcher(filePath);
                        myWatcher.EnableRaisingEvents = true;
                        myWatcher.IncludeSubdirectories = true;
                        myWatcher.Created += new FileSystemEventHandler(myWatcher_Created);
                    }
                else
                    {
                        MQClass mq = new MQClass(param.MQName, param.MQHostName, param.MQChannel, param.MQPort, company.CompanyNumbers[0].PropertyCompanyNumber);
                        mq.Connect();
                    }
#endif

#if DEBUG
                
                    if (company.Id == 18) //for testing locally
                    {
                    myWatcher = new FileSystemWatcher(filePath);
                    myWatcher.EnableRaisingEvents = true;
                    myWatcher.IncludeSubdirectories = true;
                    myWatcher.Created += new FileSystemEventHandler(myWatcher_Created);
                        // uncomment the below code to test it locally
                        ProcessManager apsChangeManager = new ProcessManager();
                        apsChangeManager.Run();
                }
                
#endif

            }
       }
        

        /// <summary>
        /// Stop this service.
        /// </summary>
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }

        /// <summary>
        /// Returns the root path of the letters.
        /// </summary>
        private static string APSXMLFiles
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["XmlDataFiles"].ToString();
                }
                catch (Exception ex)
                {
                    string exceptionMsg = "Caught exception in ApplicationSetting trying to retrieve the requested application key from the Web.config file";
                    ExceptionManager.Notify(exceptionMsg, ExceptionManager.GetFullDetailsString(ex), MailPriority.High);
                    
                    return String.Empty;
                }            
            }
        }

        // Created Event
        protected void myWatcher_Created(object sender, FileSystemEventArgs e)
        {
            //write any exceptions to the event log
            try
            {
                FileInfo file = new FileInfo(e.FullPath);

                //check that we have a valid parent directory
                DirectoryInfo dir = new DirectoryInfo(file.DirectoryName);
                string parent = dir.Name.ToUpper();

                if (parent == "XML")
                {
                    ProcessManager apsChangeManager = new ProcessManager();
                    apsChangeManager.Run();
                }
            }
            catch (NullReferenceException)
            {
                //swallow this exception
            }
            catch (Exception ex)
            {
                string returnMsg = string.Format("An error occured with APS ProcessManager");
                ExceptionManager.Notify(ex.Message, ExceptionManager.GetFullDetailsString(ex), MailPriority.High);
            }
        }
    }
}
