using System;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Configuration;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Net.Mail;
using System.Data.SqlTypes;
using System.Collections.Specialized;
using System.Web;

using com.bts.aps.domain.publish;
using com.bts.aps.domain;
using BTS.LogFramework;
using BTS.CacheEngine.V2;
using BCS.Biz;
using CacheUtilityService = BCS.ProcessAPSMessageService.CacheUtilityService;
using CacheChangeType = BCS.ProcessAPSMessageService.CacheUtilityService.CacheChangeType;
using System.Data.SqlClient;
using System.Threading;

namespace BCS.ProcessAPSMessage
{
    /// <summary>
    /// Summary description for LetterManager.
    /// </summary>
    public class ProcessManager
    {
        FileInfo _xmlFile;
        private string _dirPath = string.Empty;
        private static BCS.Biz.DataManager dm = CommonGetDataManager();
        private static XmlDocument mappingDoc;
        private static XmlDocument configDoc;

        private CacheUtilityService.CacheUtility cacheUtility = new BCS.ProcessAPSMessageService.CacheUtilityService.CacheUtility();

        #region properties
        /// <summary>
        /// Gets or sets the directory path.
        /// </summary>
        public string DirPath
        {
            get
            {
                _dirPath = ConfigurationManager.AppSettings["XmlDataFiles"];
                return _dirPath;
            }

        }

        /// <summary>
        /// Gets or sets the xml file.
        /// </summary>
        public FileInfo XMLFile
        {
            get { return _xmlFile; }
            set { _xmlFile = value; }
        }

        /// <summary>
        /// Xml file name.
        /// </summary>
        public string XMLFileName
        {
            get { return XMLFile.Name; }
        }

        #endregion

        public virtual void Run()
        {
            BeginProcess();
        }

        /// <summary>
        /// Scan the specified directory in search of new letters.
        /// </summary>
        protected virtual void BeginProcess()
        {
            string[] files = Directory.GetFiles(DirPath);
            IComparer sortComparer = new ChangeFilesSorter();
            Array.Sort(files, sortComparer);

            int attempts = 0;
            foreach (string fullName in files)
            {
                XMLFile = new FileInfo(fullName);
                attempts = 0;
                if (IsValidXML(XMLFile))
                {
                    //find the associated letter's XML doc
                    try
                    {
                        XmlDocument _doc = new XmlDocument();

                        string apsMessage = string.Empty;
                        string fileName = XMLFile.Name;
                        string[] words = fileName.Split('_');
                        string companyNumber = words[0];

                        Thread.Sleep(2000);
                        apsMessage = File.ReadAllText(XMLFile.FullName);

                        ProcessAgencyMessage(companyNumber, apsMessage, XMLFile.Name);

                        CleanUp();
                    }
                    catch (Exception ex)
                    {
                        //By default always try to rename the files in question

                        string exceptionMsg = string.Format("An error occurred while processing APS Change '{0}'. See inner exception for details.", XMLFile.Name);
                        ExceptionManager.Notify(string.Format("{0}: {1}", APSErrors.eGenericError, exceptionMsg), ExceptionManager.GetFullDetailsString(ex), MailPriority.High);

                        //OnError();
                    }
                }
            }
        }

        public virtual void BeginProcess(string xml, string companyNumber, string messageId)
        {
            try
            {
                ProcessAgencyMessage(companyNumber, xml, messageId);
            }
            catch (Exception ex)
            {
                string exceptionMsg = string.Format("An error occurred while processing APS Change '{0}'. See inner exception for details.", messageId);
                ExceptionManager.Notify(string.Format("{0}: {1}", APSErrors.eGenericError, exceptionMsg), ExceptionManager.GetFullDetailsString(ex), MailPriority.High);

                throw ex;
            }

        }

        /// <summary>
        /// Moves the XML and PDF file to the archived directory.
        /// </summary>
        protected virtual void CleanUp()
        {
            try
            {
                //move the files to the archived folder
                if (XMLFile != null && XMLFile.Extension != ".error" && !File.Exists(string.Format("{0}\\Archive\\{1}", DirPath, XMLFile.Name)))
                    XMLFile.MoveTo(string.Format("{0}\\Archive\\{1}", DirPath, XMLFile.Name));
            }
            catch (Exception exCleanUp)
            {
                throw exCleanUp;
            }
        }

        /// <summary>
        /// Renames the files with an "error" extension
        /// </summary>
        protected virtual void OnError()
        {
            try
            {
                string errorDirectory = string.Format("{0}\\Errors", DirPath);
                if (!Directory.Exists(errorDirectory))
                    Directory.CreateDirectory(errorDirectory);
                //move the files to an error directory
                if (XMLFile != null && File.Exists(XMLFile.FullName) && XMLFile.Extension != ".error")
                    XMLFile.MoveTo(string.Format("{0}\\{1}", errorDirectory, XMLFile.Name));
            }
            catch (Exception exError)
            {
                throw exError;
            }
        }

        /// <summary>
        /// Detemines if valid PDF was read.
        /// </summary>
        /// <param name="file">The file.</param>
        public static bool IsValidXML(FileInfo file)
        {
            if (file == null)
                return false;

            return (file.Extension.ToUpper() == ".XML") ? true : false;
        }

        #region Processing APS Message

        public void ProcessAgencyMessage(String companyNumber, String apsChangeMessage, string messageFileName)
        {
            if (companyNumber.Length <= 0)
            {
                System.Diagnostics.EventLog.WriteEntry("APSMessageListener", "Error company number cannot be empty", System.Diagnostics.EventLogEntryType.Warning);
                ExceptionManager.Notify(string.Format("Error company number cannot be empty. Message logged at {0}", messageFileName),
                    null,
                    MailPriority.High);
                return;
            }
            else
            {
                APSChange objApsChange = null;
                try
                {
                    objApsChange = APSChange.getInstance(apsChangeMessage, true);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                if (objApsChange != null)
                {
                    ProcessChange(companyNumber, objApsChange, messageFileName);
                }
            }
        }

        #region Process Message
        private static ModificationType GetModeType(APSChangeDetail aPSChangeDetail)
        {
            return (ModificationType)Enum.Parse(typeof(ModificationType), aPSChangeDetail.modificationType.ToString());
        }

        public void ProcessChange(String companyNumber, APSChange aPSChange, string messageId)
        {
            //string companyNumber = APSChange.CompanyNumber; // TODO:1
            int forIndex = 0;
            foreach (APSChangeDetail var in aPSChange.details)
            {
                dm = CommonGetDataManager();
                ModificationType modificationType = GetModeType(var);
                //string lowerClassName = (aPSChange.detailDomainObjectInstance(forIndex)).GetType().Name.ToLower();

                string lowerClassName = aPSChange.details[forIndex].className;

                if (!IsCompanyChange(lowerClassName, companyNumber))
                {
                    forIndex++;
                    continue;
                }

                switch (lowerClassName)
                {
                    #region agency
                    case "com.bts.aps.domain.Agency":
                        {
                            //ExceptionManager.Notify("test", aPSChange.rootAgencyGroupType, MailPriority.High);

                            if (string.IsNullOrEmpty(aPSChange.rootAgencyGroupType) || (aPSChange.rootAgencyGroupType == "12"))
                            {
                                break;
                            }

                            com.bts.aps.domain.Agency changeObject = (com.bts.aps.domain.Agency)aPSChange.detailDomainObjectInstance(forIndex);
                            BCS.Biz.Agency bizAgency = GetBizAgency(companyNumber, changeObject.id, changeObject.agencyCode);

                            if (modificationType == ModificationType.Delete)
                            {
                                try
                                {
                                    if (bizAgency == null)
                                    {
                                        //HandleNotFound(changeObject, XMLFileName);
                                        break;
                                    }

                                    bizAgency.ModifiedBy = DefaultValues.SystemIdentifier;
                                    bizAgency.ModifiedDt = DateTime.Now;

                                    // dont delete, just expire
                                    // Read it from ChangeObject.RenewalCancellationDate.

                                    if (GetCompanySetting("UsesRenewalCancelDate", companyNumber))
                                    {
                                        if (changeObject.renewalCancelDateSpecified)
                                            bizAgency.RenewalCancelDate = SqlDateTime.Parse(changeObject.renewalCancelDate);
                                    }
                                    if (GetCompanySetting("UsesNewBusinessCancelDate", companyNumber))
                                    {
                                        if (changeObject.newCancelDateSpecified)
                                            bizAgency.CancelDate = SqlDateTime.Parse(changeObject.newCancelDate);
                                    }
                                    else if (changeObject.renewalCancelDateSpecified)
                                    {

                                        DateTime dtCancelDate = DateTime.Parse(changeObject.renewalCancelDate.Trim());
                                        SqlDateTime dtCancelDateSql = new SqlDateTime(dtCancelDate);
                                        bizAgency.CancelDate = dtCancelDateSql;
                                    }
                                    else
                                    {
                                        bizAgency.CancelDate = DateTime.Now;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string msg = string.Format("Error applying CancelDate {0} on Agency {1} for company# {2}, XMLFileName - {3}",
                                                    changeObject.renewalCancelDate, changeObject.agencyCode, companyNumber, XMLFileName);
                                    ExceptionManager.Notify(APSErrors.eAgency, msg, MailPriority.High);

                                    throw ex;
                                }
                                Commit();
                                AddAgencyToCache(companyNumber);

                                continue;
                            }
                            else // adding
                            {
                                if (bizAgency == null)
                                {
                                    bizAgency = GetNewAgency(companyNumber);
                                    bizAgency.EntryBy = DefaultValues.SystemIdentifier;
                                    bizAgency.EntryDt = DateTime.Now;
                                }
                            }
                            FillAgency(bizAgency, changeObject, companyNumber);
                            Commit();

                            AddAgencyToCache(companyNumber);
                            break;
                        }
                    #endregion

                    #region agencylicense
                    case "com.bts.aps.domain.AgencyLicense":
                        {
                            AgencyLicense changeObject = (AgencyLicense)aPSChange.detailDomainObjectInstance(forIndex);
                            BCS.Biz.Agency bizAgency = null;
                            if (aPSChange.isRootClassAgency)
                                bizAgency = GetBizAgency(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode);
                            else
                                bizAgency = GetBizAgency(companyNumber, changeObject.agency.id, changeObject.agency.agencyCode);
                            switch (modificationType)
                            {

                                case ModificationType.Insert:
                                    {
                                        if (bizAgency == null)
                                        {
                                            HandleNotFound(changeObject.agency, XMLFileName);
                                            break;
                                        }
                                        BCS.Biz.AgencyLicensedState newOne = bizAgency.NewAgencyLicensedState();
                                        newOne.State = GetBizState(changeObject.licenseState.label);
                                    }
                                    break;
                                case ModificationType.Update:
                                    {
                                        BCS.Biz.AgencyLicensedState bizAgencyLicense = null;

                                        if (aPSChange.isRootClassAgency)
                                            bizAgencyLicense = GetBizAgencyLicenseState(
                                                                                        companyNumber,
                                                                                        aPSChange.rootAgencyApsId,
                                                                                        aPSChange.rootAgencyCode,
                                                                                        changeObject.licenseState.label);
                                        else

                                            bizAgencyLicense = GetBizAgencyLicenseState(companyNumber,
                                                                                        changeObject.agency.id,
                                                                                        changeObject.agency.agencyCode,
                                                                                        changeObject.licenseState.label);


                                        // update type comes with a new state code, so we cannot query on other parameters, aps Id must be there
                                        if (bizAgencyLicense == null)
                                        {
                                            BCS.Biz.AgencyLicensedState newOne = bizAgency.NewAgencyLicensedState();
                                            newOne.State = GetBizState(changeObject.licenseState.label);
                                        }
                                        else
                                            bizAgencyLicense.State = GetBizState(changeObject.licenseState.label);
                                    }
                                    break;
                                case ModificationType.Delete:
                                    {
                                        BCS.Biz.AgencyLicensedState bizAgencyLicense = GetBizAgencyLicenseState(changeObject.id);
                                        if (bizAgencyLicense == null) // not found by id, try again by other parameters
                                        {
                                            if (aPSChange.isRootClassAgency)
                                                bizAgencyLicense = GetBizAgencyLicenseState(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode, changeObject.licenseState.label);
                                            else
                                                bizAgencyLicense = GetBizAgencyLicenseState(companyNumber, changeObject.agency.id, changeObject.agency.agencyCode, changeObject.licenseState.label);

                                        }
                                        if (bizAgencyLicense != null)
                                            bizAgencyLicense.Delete();
                                    }
                                    break;
                                default:
                                    break;

                            }
                            Commit();
                        }
                        break;
                    #endregion

                    #region agencyname
                    case "com.bts.aps.domain.AgencyName":
                        {
                            AgencyName changeObject = (com.bts.aps.domain.AgencyName)aPSChange.detailDomainObjectInstance(forIndex);
                            if (modificationType == ModificationType.Delete)
                            {
                                // ignore, we only store current agency name and APS does not allow deletion of current name
                                // ignore, insert of agencyname since they will be taken care of during Agency insert
                            }
                            else
                            {
                                //BCS.Biz.Agency bizAgency = GetBizAgency(companyNumber, changeObject.agency.id, changeObject.agency.agencyCode);
                                BCS.Biz.Agency bizAgency = null;
                                if (aPSChange.isRootClassAgency)
                                    bizAgency = GetBizAgency(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode);
                                else
                                    bizAgency = GetBizAgency(companyNumber, changeObject.agency.id, changeObject.agency.agencyCode);

                                //agencyAPSId,
                                //agencyCode);
                                if (bizAgency == null)
                                {
                                    //HandleNotFound(changeObject.agency, XMLFileName);
                                    break;
                                }

                                bizAgency.AgencyName = changeObject.dbaName;//DetermineAgencyName(companyNumber, changeObject);
                                bizAgency.ModifiedBy = DefaultValues.SystemIdentifier;
                                bizAgency.ModifiedDt = DateTime.Now;

                            }
                            Commit();
                            AddAgencyToCache(companyNumber);

                            break;
                        }
                    #endregion

                    #region agencynote
                    case "com.bts.aps.domain.AgencyNote":
                        {

                            AgencyNote changeObject = (com.bts.aps.domain.AgencyNote)aPSChange.detailDomainObjectInstance(forIndex);
                            if (IsUpdateableProperty(lowerClassName, changeObject))
                            {
                                BCS.Biz.Agency bizAgency = null;
                                if (aPSChange.isRootClassAgency)
                                    bizAgency = GetBizAgency(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode);
                                else
                                    bizAgency = GetBizAgency(companyNumber, changeObject.agency.id, changeObject.agency.agencyCode);

                                if (bizAgency == null)
                                {
                                    HandleNotFound(changeObject.agency, XMLFileName);
                                    break;
                                }
                                if (modificationType == ModificationType.Delete)
                                {
                                    bizAgency.Comments = string.Empty;
                                }
                                else
                                {
                                    if (changeObject.detail.Length > 0)
                                        bizAgency.Comments = changeObject.detail;
                                    else
                                        bizAgency.Comments = changeObject.summary;
                                }
                                bizAgency.ModifiedBy = DefaultValues.SystemIdentifier;
                                bizAgency.ModifiedDt = DateTime.Now;
                                Commit();

                                AddAgencyToCache(companyNumber);
                            }
                            break;
                        }
                    #endregion

                    #region agencypersonnel
                    case "com.bts.aps.domain.AgencyPersonnel": // agent or contact
                        {
                            AgencyPersonnel changeObject = (com.bts.aps.domain.AgencyPersonnel)aPSChange.detailDomainObjectInstance(forIndex);
                            BCS.Biz.Agency bizAgency = GetBizAgency(companyNumber, changeObject.agency.id, changeObject.agency.agencyCode);
                            if (bizAgency == null)
                            {
                                HandleNotFound(changeObject.agency, XMLFileName);
                                break;
                            }

                            bool deleteProducer = false;
                            bool deleteContact = false;

                            //search for the agency role
                            AgencyPersonnelRole apRole = null;
                            List<AgencyPersonnelRole> apRoleList = new List<AgencyPersonnelRole>();
                            for (int i = 0; i < aPSChange.details.Length; i++)
                            {
                                string internalClassName = aPSChange.details[i].className;
                                switch (internalClassName)
                                {
                                    case "com.bts.aps.domain.AgencyPersonnelRole":
                                        apRole = (com.bts.aps.domain.AgencyPersonnelRole)aPSChange.detailDomainObjectInstance(i);
                                        apRoleList.Add(apRole);

                                        if (GetModeType(aPSChange.details[i]) == ModificationType.Delete)
                                        {
                                            if (apRole.agencyPersonnelRoleType.label == "Producer")
                                                deleteProducer = true;
                                            else if (apRole.agencyPersonnelRoleType.label.Contains("Contact"))
                                                deleteContact = true;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            //string[] roles = ConfigurationSettings.Instance.GetMappedRoleType(changeObject.agencyPersonnelRoles);
                            string[] roles = ConfigurationSettings.Instance.GetMappedRoleType(apRoleList.ToArray());
                            if (roles != null && roles.Length > 0)
                            {
                                foreach (string role in roles)
                                {
                                    if (role == "Agent")
                                    {
                                        BCS.Biz.Agent bizAgent = GetBizAgent(changeObject.id, changeObject.agency.agencyCode, companyNumber);
                                        if (modificationType == ModificationType.Delete || deleteProducer)
                                        {
                                            if (bizAgent == null)
                                            {
                                                //HandleNotFound(changeObject, role, XMLFileName);
                                                break;
                                            }
                                            // dont delete, just expire
                                            bizAgent.CancelDate = DateTime.Now;
                                            bizAgent.ModifiedBy = DefaultValues.SystemIdentifier;
                                            bizAgent.ModifiedDt = DateTime.Now;
                                        }
                                        else
                                        {
                                            if (bizAgent == null)
                                            {
                                                bizAgent = bizAgency.NewAgent();
                                                bizAgent.EntryDt = DateTime.Now;
                                                bizAgent.EntryBy = "APS Process";
                                            }

                                            FillAgent(bizAgent, changeObject);
                                        }
                                    }
                                    if (role == "Contact")
                                    {
                                        BCS.Biz.Contact bizContact = GetBizContact(changeObject.id, bizAgency.Id, changeObject.personnel.firstName,
                                               changeObject.personnel.lastName);

                                        if (modificationType == ModificationType.Delete || deleteContact)
                                        {
                                            if (bizContact == null)
                                            {
                                                HandleNotFound(changeObject, role, XMLFileName);
                                                break;
                                            }

                                            // dont delete, just expire
                                            bizContact.RetirementDate = DateTime.Now;
                                        }
                                        else
                                        {
                                            if (bizContact == null)
                                            {
                                                bizContact = bizAgency.NewContact();
                                            }
                                            FillContact(bizContact, changeObject);
                                        }
                                    }
                                }
                                Commit();
                            }
                            //Delete if Role has not been explicitly defined and ModificationType = Delete.
                            else if (modificationType == ModificationType.Delete)
                            {
                                //Agent
                                if (changeObject.agentNumber != null)
                                {
                                    BCS.Biz.Agent bizAgent = GetBizAgent(changeObject.id, changeObject.agency.agencyCode, companyNumber);
                                    if (bizAgent != null)
                                    {
                                        bizAgent.CancelDate = DateTime.Now;
                                        bizAgent.ModifiedBy = DefaultValues.SystemIdentifier;
                                        bizAgent.ModifiedDt = DateTime.Now;
                                        Commit();
                                    }
                                }
                            }
                            break;
                        }
                    #endregion

                    #region personnelcommunication
                    case "com.bts.aps.domain.PersonnelCommunication":
                        {
                            var pc = (com.bts.aps.domain.PersonnelCommunication)aPSChange.detailDomainObjectInstance(forIndex);
                            var bizAgent = GetBizAgentByPersonnelId(pc.personnel.id, companyNumber);

                            if (bizAgent != null)
                            {
                                var communicationType = pc.communicationType.formatter ?? "";
                                var communicationValue = pc.communicationValue ?? "";

                                if (communicationType.IndexOf("PHONE", StringComparison.CurrentCultureIgnoreCase) >= 0 && communicationValue.Length == 10)
                                {
                                    bizAgent.PrimaryPhoneAreaCode = communicationValue.Substring(0, 3);
                                    bizAgent.PrimaryPhone = communicationValue.Substring(3);
                                    bizAgent.ModifiedBy = DefaultValues.SystemIdentifier;
                                    bizAgent.ModifiedDt = DateTime.Now;
                                }
                                else if (communicationType.IndexOf("EMAIL", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                {
                                    bizAgent.EmailAddress = pc.communicationValue;
                                    bizAgent.ModifiedBy = DefaultValues.SystemIdentifier;
                                    bizAgent.ModifiedDt = DateTime.Now;
                                }

                                bizAgent.ModifiedBy = DefaultValues.SystemIdentifier;
                                bizAgent.ModifiedDt = DateTime.Now;

                                Commit();
                            }
                            break;
                        }
                    #endregion

                    #region agencyunderwritingunit
                    case "com.bts.aps.domain.AgencyUnderwritingUnit":
                        {

                            AgencyUnderwritingUnit changeObject = (AgencyUnderwritingUnit)aPSChange.detailDomainObjectInstance(forIndex);

                            BCS.Biz.Agency bizAgency = null;
                            if (aPSChange.isRootClassAgency)
                                bizAgency = GetBizAgency(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode);
                            else
                                bizAgency = GetBizAgency(companyNumber, changeObject.agency.id, changeObject.agency.agencyCode);

                            if (bizAgency == null)
                            {
                                HandleNotFound(changeObject.agency, XMLFileName);
                                break;
                            }
                            if (changeObject.underwritingUnit != null)
                            {
                                BCS.Biz.LineOfBusinessCollection bizUWs = GetBizUnderwritingUnits(companyNumber);
                                BCS.Biz.LineOfBusiness bizUW = bizUWs.FindByCode(changeObject.underwritingUnit.code);
                                if (bizUW == null)
                                    bizUW = bizUWs.FindByDescription(changeObject.underwritingUnit.description);

                                if (bizUW != null)
                                {
                                    if (bizAgency.AgencyLineOfBusinesss != null)
                                    {
                                        BCS.Biz.AgencyLineOfBusiness bizAgencyUW = null;
                                        int oAgencyId, oBizUWId;
                                        long oApsId;
                                        bool agencyLOBExists;
                                        GetAgencyLineofBusiness(bizAgency.Id, bizUW.Id, out oAgencyId, out oBizUWId, out oApsId);

                                        agencyLOBExists = oAgencyId != 0 && oBizUWId != 0;

                                        //bizAgency.AgencyLineOfBusinesss.FindByLineOfBusinessId(bizUW.Id);
                                        if (modificationType == ModificationType.Delete)
                                        {
                                            if (agencyLOBExists)
                                            {
                                                //bizAgencyUW.Delete();
                                                UpdateAgencyLineOfBusiness(bizAgency.Id, bizUW.Id, changeObject.id, ModificationType.Delete);
                                            }
                                        }
                                        else
                                        {
                                            if (!agencyLOBExists)
                                            {
                                                bizAgencyUW = bizAgency.NewAgencyLineOfBusiness();
                                                bizAgencyUW.LineOfBusinessId = bizUW.Id;
                                                bizAgencyUW.APSId = changeObject.id;
                                            }
                                            else
                                            {
                                                UpdateAgencyLineOfBusiness(bizAgency.Id, bizUW.Id, changeObject.id, ModificationType.Update);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //HandleNotFound(changeObject.underwritingUnit, XMLFileName);
                                }
                            }
                            Commit();
                            break;
                        }
                    #endregion

                    #region agencyuwpersonrole
                    case "com.bts.aps.domain.AgencyUWPersonRole":
                        {
                            AgencyUWPersonRole changeObject = (AgencyUWPersonRole)aPSChange.detailDomainObjectInstance(forIndex);

                            #region Validate required information
                            // check if any required are null
                            if (changeObject.agencyUnderwritingUnit == null)
                            {
                                string message = string.Format("An Agency underwriting person role change was received but lacks the agency underwriting unit information." +
                                    "Please review the change XML file {0}", messageId);

                                ExceptionManager.Notify(APSErrors.eAgencyUWPersonRole, message, MailPriority.High);
                                break;
                            }
                            // check if any required are null
                            if (changeObject.agencyUnderwritingUnit.underwritingUnit == null)
                            {
                                string message = string.Format("An Agency underwriting person role change was received but lacks the 'underwriting unit' information for the agency." +
                                    "Please review the change XML file {0}", messageId);

                                ExceptionManager.Notify(APSErrors.eAgencyUWPersonRole, message, MailPriority.High);
                                break;
                            }

                            // check if any required are null
                            if (changeObject.companyPersonnelRole == null || changeObject.companyPersonnelRole.companyPersonnel == null)
                            {
                                string message = string.Format("An Agency underwriting person role change was received but lacks the 'company personnel' information." +
                                    "Please review the change XML file {0}", messageId);

                                ExceptionManager.Notify(APSErrors.eAgencyUWPersonRole, message, MailPriority.High);
                                break;
                            }
                            // check if any required are null
                            if (changeObject.companyPersonnelRole.companyPersonnelRoleType == null)
                            {
                                string message = string.Format("An Agency underwriting person role change was received but lacks the 'role information'." +
                                    "Please review the change XML file {0}", messageId);

                                ExceptionManager.Notify(APSErrors.eAgencyUWPersonRole, message, MailPriority.High);
                                break;
                            }

                            if (!aPSChange.isRootClassAgency)
                            {
                                // check if any required are null
                                if (changeObject.agencyUnderwritingUnit.agency == null ||
                                    changeObject.agencyUnderwritingUnit.agency.agencyCode == null ||
                                    changeObject.agencyUnderwritingUnit.agency.id == long.MinValue)
                                {
                                    string message = string.Format("An Agency underwriting person role change was received but it lacks any 'Agency' information'." +
                                        "Please review the change XML file {0}", messageId);

                                    ExceptionManager.Notify(APSErrors.eAgencyUWPersonRole, message, MailPriority.High);
                                    break;
                                }
                            }


                            #endregion

                            switch (modificationType)
                            {
                                case ModificationType.Insert:
                                    {
                                        #region Get BCS information

                                        BCS.Biz.Agency bizAgency = null;
                                        if (aPSChange.isRootClassAgency)
                                            bizAgency = GetBizAgency(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode);
                                        else
                                            bizAgency = GetBizAgency(companyNumber, changeObject.agencyUnderwritingUnit.agency.id,
                                                                 changeObject.agencyUnderwritingUnit.agency.agencyCode);

                                        if (bizAgency == null)
                                        {
                                            HandleNotFound(changeObject.agencyUnderwritingUnit.agency, XMLFileName);
                                            break;
                                        }
                                        BCS.Biz.LineOfBusiness bizUW = GetBizLineOfBusiness(changeObject.agencyUnderwritingUnit.underwritingUnit.id, companyNumber,
                                            changeObject.agencyUnderwritingUnit.underwritingUnit.description);
                                        if (bizUW == null)
                                        {
                                            HandleNotFound(changeObject.agencyUnderwritingUnit.underwritingUnit, XMLFileName);
                                            // TODO: 1
                                            break;
                                        }
                                        BCS.Biz.Person bizPerson = null;
                                        if (aPSChange.isRootClassCompanyPersonnel)
                                        {
                                            bizPerson = GetBizPerson(aPSChange.companyPersonnel_APSID, companyNumber, aPSChange.companyPersonnel_FirstName,
                                                aPSChange.companyPersonnel_LastName);
                                        }
                                        else
                                        {
                                            bizPerson = GetBizPerson(changeObject.companyPersonnelRole.companyPersonnel.id, companyNumber,
                                                changeObject.companyPersonnelRole.companyPersonnel.firstName, changeObject.companyPersonnelRole.companyPersonnel.lastName);

                                        }

                                        if (bizPerson == null)
                                        {
                                            HandleNotFound(changeObject.companyPersonnelRole.companyPersonnel, XMLFileName);
                                            // TODO: 1
                                            break;
                                        }
                                        BCS.Biz.UnderwritingRole bizRole = GetBizUnderwritingRole(companyNumber, changeObject.companyPersonnelRole.companyPersonnelRoleType.label);

                                        if (bizRole == null)
                                        {
                                            HandleNotFound(changeObject.companyPersonnelRole.companyPersonnelRoleType, XMLFileName);
                                            // TODO: 1
                                            break;
                                        }
                                        #endregion
                                        BCS.Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson bizAssignment = null;
                                        //    GetBizAssignment(bizAgency.Id, bizUW.Id, bizPerson.Id, bizRole.Id);
                                        int oAgencyId, oBizUWId, oBizPersonId, oBizRoleId = 0;
                                        bool oPrimary = false;
                                        GetBizAssignment(bizAgency.Id, bizUW.Id, bizPerson.Id, bizRole.Id, out oAgencyId, out oBizUWId, out oBizPersonId, out oBizRoleId, out oPrimary);

                                        bool bizAssignmentNotExists = oAgencyId == 0 || oBizRoleId == 0 || oBizUWId == 0 || oBizPersonId == 0;
                                        if (bizAssignmentNotExists) // new object
                                        {
                                            bizAssignment = bizAgency.NewAgencyLineOfBusinessPersonUnderwritingRolePerson();
                                            bizAssignment.AgencyId = bizAgency.Id;
                                            //bizAssignment.APSId = 
                                            bizAssignment.LineOfBusinessId = bizUW.Id;
                                            bizAssignment.PersonId = bizPerson.Id;
                                            bizAssignment.UnderwritingRoleId = bizRole.Id;
                                            bizAssignment.Primary = changeObject.primaryInd;
                                        }
                                        else // looks like the assignment already exists.
                                        {
                                            UpdatePrimaryBizAssignment(bizAgency.Id, bizUW.Id, bizPerson.Id,
                                                bizRole.Id, changeObject.primaryInd, ModificationType.Update);
                                            //bizAssignment.Primary = changeObject.primaryInd; // TODO: reset primaries, if this is primary

                                        }
                                    }
                                    break;
                                case ModificationType.Update:
                                    {
                                        #region Get BCS information
                                        BCS.Biz.Agency bizAgency = null;
                                        if (aPSChange.isRootClassAgency)
                                            bizAgency = GetBizAgency(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode);
                                        else
                                            bizAgency = GetBizAgency(companyNumber, changeObject.agencyUnderwritingUnit.agency.id,
                                                                 changeObject.agencyUnderwritingUnit.agency.agencyCode);

                                        //BCS.Biz.Agency bizAgency = GetBizAgency(companyNumber, changeObject.agencyUnderwritingUnit.agency.id,
                                        //                        changeObject.agencyUnderwritingUnit.agency.agencyCode);
                                        if (bizAgency == null)
                                        {
                                            HandleNotFound(changeObject.agencyUnderwritingUnit.agency, XMLFileName);
                                            break;
                                        }
                                        BCS.Biz.LineOfBusiness bizUW = GetBizLineOfBusiness(changeObject.agencyUnderwritingUnit.underwritingUnit.id, companyNumber,
                                            changeObject.agencyUnderwritingUnit.underwritingUnit.description);
                                        if (bizUW == null)
                                        {
                                            HandleNotFound(changeObject.agencyUnderwritingUnit.underwritingUnit, XMLFileName);
                                            // TODO: 1
                                            break;
                                        }
                                        BCS.Biz.Person bizPerson = null;
                                        if (aPSChange.isRootClassCompanyPersonnel)
                                        {
                                            bizPerson = GetBizPerson(aPSChange.companyPersonnel_APSID, companyNumber, aPSChange.companyPersonnel_FirstName,
                                                aPSChange.companyPersonnel_LastName);
                                        }
                                        else
                                        {
                                            bizPerson = GetBizPerson(changeObject.companyPersonnelRole.companyPersonnel.id, companyNumber,
                                                changeObject.companyPersonnelRole.companyPersonnel.firstName, changeObject.companyPersonnelRole.companyPersonnel.lastName);

                                        }

                                        //BCS.Biz.Person bizPerson = GetBizPerson(changeObject.companyPersonnelRole.companyPersonnel.id, companyNumber,
                                        //    changeObject.companyPersonnelRole.companyPersonnel.firstName, changeObject.companyPersonnelRole.companyPersonnel.lastName);

                                        if (bizPerson == null)
                                        {
                                            HandleNotFound(changeObject.companyPersonnelRole.companyPersonnel, XMLFileName);
                                            // TODO: 1
                                            break;
                                        }
                                        BCS.Biz.UnderwritingRole bizRole = GetBizUnderwritingRole(companyNumber, changeObject.companyPersonnelRole.companyPersonnelRoleType.label);

                                        if (bizRole == null)
                                        {
                                            HandleNotFound(changeObject.companyPersonnelRole.companyPersonnelRoleType, XMLFileName);
                                            // TODO: 1
                                            break;
                                        }
                                        #endregion
                                        // get by aps id
                                        //BCS.Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson bizAssignmentByKeysId =
                                        //    GetBizAssignment(bizAgency.Id, bizUW.Id, bizPerson.Id, bizRole.Id);

                                        int oAgencyId, oBizUWId, oBizPersonId, oBizRoleId = 0;
                                        bool oPrimary = false;
                                        GetBizAssignment(bizAgency.Id, bizUW.Id, bizPerson.Id, bizRole.Id, out oAgencyId, out oBizUWId, out oBizPersonId, out oBizRoleId, out oPrimary);

                                        bool bizAssignmentNotExists = oAgencyId == 0 || oBizRoleId == 0 || oBizUWId == 0 || oBizPersonId == 0;

                                        if (bizAssignmentNotExists)
                                        {
                                            #region Validate required information
                                            // check if any required are null
                                            if (changeObject.agencyUnderwritingUnit == null)
                                            {
                                                string message = string.Format("An Agency underwriting person role change was received but lacks the agency underwriting unit information." +
                                                    "Please review the change XML file {0}", messageId);

                                                ExceptionManager.Notify(message, string.Empty, MailPriority.High);
                                                break;
                                            }
                                            // check if any required are null
                                            if (changeObject.agencyUnderwritingUnit.underwritingUnit == null)
                                            {
                                                string message = string.Format("An Agency underwriting person role change was received but lacks the underwriting unit information for the agency." +
                                                    "Please review the change XML file {0}", messageId);

                                                ExceptionManager.Notify(message, string.Empty, MailPriority.High);
                                                break;
                                            }

                                            // check if any required are null
                                            if (changeObject.companyPersonnelRole == null || changeObject.companyPersonnelRole.companyPersonnel == null)
                                            {
                                                string message = string.Format("An Agency underwriting person role change was received but lacks the company personnel information." +
                                                    "Please review the change XML file {0}", messageId);

                                                ExceptionManager.Notify(message, string.Empty, MailPriority.High);
                                                break;
                                            }
                                            // check if any required are null
                                            if (changeObject.companyPersonnelRole.companyPersonnelRoleType == null)
                                            {
                                                string message = string.Format("An Agency underwriting person role change was received but lacks the role information." +
                                                    "Please review the change XML file {0}", messageId);

                                                ExceptionManager.Notify(message, string.Empty, MailPriority.High);
                                                break;
                                            }
                                            #endregion

                                            BCS.Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson bizAssignment =
                                                bizAgency.NewAgencyLineOfBusinessPersonUnderwritingRolePerson();
                                            bizAssignment.AgencyId = bizAgency.Id;
                                            bizAssignment.LineOfBusinessId = bizUW.Id;
                                            bizAssignment.PersonId = bizPerson.Id;
                                            bizAssignment.UnderwritingRoleId = bizRole.Id;
                                            bizAssignment.Primary = changeObject.primaryInd;
                                        }
                                        else
                                        {
                                            UpdatePrimaryBizAssignment(bizAgency.Id, bizUW.Id, bizPerson.Id,
                                                bizRole.Id, changeObject.primaryInd, ModificationType.Update);
                                        }
                                    }
                                    break;
                                case ModificationType.Delete:
                                    {
                                        #region Get BCS information
                                        BCS.Biz.Agency bizAgency = null;
                                        if (aPSChange.isRootClassAgency)
                                            bizAgency = GetBizAgency(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode);
                                        else
                                            bizAgency = GetBizAgency(companyNumber, changeObject.agencyUnderwritingUnit.agency.id,
                                                                 changeObject.agencyUnderwritingUnit.agency.agencyCode);

                                        if (bizAgency == null)
                                        {
                                            HandleNotFound(changeObject.agencyUnderwritingUnit.agency, XMLFileName);
                                            break;
                                        }
                                        BCS.Biz.LineOfBusiness bizUW = GetBizLineOfBusiness(changeObject.agencyUnderwritingUnit.underwritingUnit.id, companyNumber,
                                            changeObject.agencyUnderwritingUnit.underwritingUnit.description);
                                        if (bizUW == null)
                                        {
                                            HandleNotFound(changeObject.agencyUnderwritingUnit.underwritingUnit, XMLFileName);
                                            // TODO: 1
                                            break;
                                        }
                                        BCS.Biz.Person bizPerson = null;
                                        if (aPSChange.isRootClassCompanyPersonnel)
                                        {
                                            bizPerson = GetBizPerson(aPSChange.companyPersonnel_APSID, companyNumber, aPSChange.companyPersonnel_FirstName,
                                                aPSChange.companyPersonnel_LastName);
                                        }
                                        else
                                        {
                                            bizPerson = GetBizPerson(changeObject.companyPersonnelRole.companyPersonnel.id, companyNumber,
                                                changeObject.companyPersonnelRole.companyPersonnel.firstName, changeObject.companyPersonnelRole.companyPersonnel.lastName);

                                        }

                                        //BCS.Biz.Person bizPerson = GetBizPerson(changeObject.companyPersonnelRole.companyPersonnel.id, companyNumber,
                                        //    changeObject.companyPersonnelRole.companyPersonnel.firstName, changeObject.companyPersonnelRole.companyPersonnel.lastName);

                                        if (bizPerson == null)
                                        {
                                            HandleNotFound(changeObject.companyPersonnelRole.companyPersonnel, XMLFileName);
                                            // TODO: 1
                                            break;
                                        }
                                        BCS.Biz.UnderwritingRole bizRole = GetBizUnderwritingRole(companyNumber, changeObject.companyPersonnelRole.companyPersonnelRoleType.label);

                                        if (bizRole == null)
                                        {
                                            HandleNotFound(changeObject.companyPersonnelRole.companyPersonnelRoleType, XMLFileName);
                                            // TODO: 1
                                            break;
                                        }
                                        #endregion
                                        //BCS.Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson bizAssignment = null;
                                        //    GetBizAssignment(bizAgency.Id, bizUW.Id, bizPerson.Id, bizRole.Id);
                                        int oAgencyId, oBizUWId, oBizPersonId, oBizRoleId = 0;
                                        bool oPrimary = false;
                                        GetBizAssignment(bizAgency.Id, bizUW.Id, bizPerson.Id, bizRole.Id, out oAgencyId, out oBizUWId, out oBizPersonId, out oBizRoleId, out oPrimary);

                                        bool bizAssignmentNotExists = oAgencyId == 0 || oBizRoleId == 0 || oBizUWId == 0 || oBizPersonId == 0;

                                        if (bizAssignmentNotExists)
                                        {
                                            string message = string.Format("An agency underwriting person role delete message was received, but could not be found in BCS" +
                                                "Please review the XML {0}.", messageId);
                                            ExceptionManager.Notify(APSErrors.eAgencyUWPersonRole, message, MailPriority.High);
                                        }
                                        else
                                        {
                                            UpdatePrimaryBizAssignment(bizAgency.Id, bizUW.Id, bizPerson.Id,
                                                bizRole.Id, changeObject.primaryInd, ModificationType.Delete);
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }

                            Commit();
                            UpdatePersonnelCache(companyNumber);
                            break;
                        }
                    #endregion

                    #region companypersonnel
                    case "com.bts.aps.domain.CompanyPersonnel":
                        {
                            CompanyPersonnel changeObject = (CompanyPersonnel)aPSChange.detailDomainObjectInstance(forIndex);
                            BCS.Biz.Person bizPerson = GetBizPerson(changeObject.id, companyNumber, changeObject.firstName, changeObject.lastName);

                            if (modificationType == ModificationType.Delete)
                            {
                                if (bizPerson == null)
                                {
                                    //HandleNotFound(changeObject, XMLFileName);
                                    break;
                                }
                                //bizPerson.Delete(); 
                                if (changeObject.retirementDateSpecified)
                                {
                                    try
                                    {
                                        DateTime retireDate = DateTime.Parse(changeObject.retirementDate);
                                        SqlDateTime retireDateSql = new SqlDateTime(retireDate);
                                        bizPerson.RetirementDate = retireDateSql;
                                    }
                                    catch (Exception ex)
                                    {
                                        throw ex;
                                    }
                                }
                                else
                                    bizPerson.RetirementDate = DateTime.Now;
                            }
                            else
                            {
                                if (bizPerson == null)
                                {
                                    bizPerson = GetNewPerson(companyNumber);
                                }
                                bizPerson.APSId = changeObject.id;
                                bizPerson.UserName = changeObject.userId;
                                bizPerson.FullName = string.Format("{0} {1}", changeObject.firstName, changeObject.lastName);
                                bizPerson.Active = SqlBoolean.True;
                            }
                            Commit();
                            UpdatePersonnelCache(companyNumber);
                            break;
                        }
                    #endregion

                    #region companypersonnelinfo
                    case "com.bts.aps.domain.CompanyPersonnelInfo":
                        {
                            CompanyPersonnelInfo changeObject = (CompanyPersonnelInfo)aPSChange.detailDomainObjectInstance(forIndex);

                            // check if any required are null
                            if (changeObject.companyPersonnel == null || changeObject.companyPersonnel.userId == null)
                            {
                                string message = string.Format("An Company Personnel Info was received but lacks the corresponding" +
                                                "'company personnel' information. Please review the change XML file {0}", messageId);

                                ExceptionManager.Notify(APSErrors.eCompanyPersonnelInfo, message, MailPriority.High);
                                break;
                            }

                            BCS.Biz.Person bizPerson = GetBizPerson(changeObject.companyPersonnel.id, companyNumber, changeObject.companyPersonnel.firstName, changeObject.companyPersonnel.lastName);

                            if (modificationType == ModificationType.Delete)
                            {
                                if (bizPerson == null)
                                {
                                    //HandleNotFound(changeObject, XMLFileName);
                                    break;
                                }
                            }


                            bool validRetirementDate = changeObject.retirementDate != null && changeObject.retirementDate != string.Empty &&
                                    changeObject.retirementDate.Length > 0;

                            if (modificationType == ModificationType.Delete)
                            {
                                bizPerson.Delete();
                                if (validRetirementDate)
                                {
                                    SqlDateTime retireDateSql = ConvertToSQLDate(changeObject.retirementDate.Trim());
                                    bizPerson.RetirementDate = retireDateSql;
                                }
                                else if (changeObject.retirementDate == null)
                                    bizPerson.RetirementDate = SqlDateTime.Null;
                            }
                            else
                            {
                                if (bizPerson == null)
                                {
                                    bizPerson = GetNewPerson(companyNumber);
                                }
                                bizPerson.APSId = changeObject.companyPersonnel.id;
                                bizPerson.Initials = changeObject.userInitials;
                                bizPerson.UserName = changeObject.companyPersonnel.userId;
                                bizPerson.FullName = string.Format("{0} {1}", changeObject.companyPersonnel.firstName,
                                                        changeObject.companyPersonnel.lastName);

                                if (validRetirementDate)
                                {
                                    SqlDateTime retireDateSql = ConvertToSQLDate(changeObject.retirementDate.Trim());
                                    bizPerson.RetirementDate = retireDateSql;
                                }
                                else if (changeObject.retirementDate == null)
                                {
                                    bizPerson.RetirementDate = SqlDateTime.Null;
                                }
                                bizPerson.Active = SqlBoolean.True;
                            }
                            Commit();
                            UpdatePersonnelCache(companyNumber);
                            break;
                        }
                    #endregion

                    #region Personnel
                    case "com.bts.aps.domain.Personnel":
                        {
                            var personnel = (com.bts.aps.domain.Personnel)aPSChange.detailDomainObjectInstance(forIndex);
                            var bizAgent = GetBizAgentByPersonnelId(personnel.id, companyNumber);

                            if (bizAgent != null)
                            {
                                bizAgent.FirstName = personnel.firstName;
                                bizAgent.Middle = personnel.middleName;
                                bizAgent.Surname = personnel.lastName;

                                bizAgent.ModifiedBy = DefaultValues.SystemIdentifier;
                                bizAgent.ModifiedDt = DateTime.Now;

                                Commit();
                            }
                            break;
                        }
                    #endregion

                    #region underwritingunit
                    case "com.bts.aps.domain.UnderwritingUnit":
                        {
                            UnderwritingUnit changeObject = (UnderwritingUnit)aPSChange.detailDomainObjectInstance(forIndex);
                            BCS.Biz.LineOfBusiness bizUW = GetBizLineOfBusiness(changeObject.id, companyNumber, changeObject.description);
                            if (modificationType == ModificationType.Delete)
                            {
                                if (bizUW == null)
                                {
                                    //HandleNotFound(changeObject, XMLFileName);
                                    break;
                                }
                                bizUW.Delete();
                            }
                            else
                            {
                                if (bizUW != null)
                                {
                                    bizUW.APSId = changeObject.id;
                                    bizUW.Code = changeObject.code;
                                    bizUW.Description = changeObject.description;
                                }
                                else
                                {
                                    // TODO: Find the LOB.Line "CL"
                                    bizUW = dm.NewLineOfBusiness(changeObject.code, changeObject.description, "CL");
                                    BCS.Biz.CompanyNumberLineOfBusiness bizCnUW = bizUW.NewCompanyNumberLineOfBusiness();
                                    bizUW.APSId = changeObject.id;

                                    dm.QueryCriteria.Clear();
                                    dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                                    bizCnUW.CompanyNumber = dm.GetCompanyNumber();
                                }
                            }
                            Commit();
                            break;
                        }
                    #endregion

                    #region location
                    case "com.bts.aps.domain.Location":
                        {
                            Location changeObject = (Location)aPSChange.detailDomainObjectInstance(forIndex);
                            if (modificationType == ModificationType.Delete)
                            {
                                // there will always be a primary location. This case should never arise.
                                // ignore location insert taken care of during agency insert
                            }
                            else
                            {
                                if (changeObject.isPrimaryLocation)
                                {
                                    BCS.Biz.Agency bizAgency = null;
                                    if (aPSChange.isRootClassAgency)
                                        bizAgency = GetBizAgency(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode);
                                    else
                                        bizAgency = GetBizAgency(companyNumber, changeObject.agency.id, changeObject.agency.agencyCode);


                                    if (bizAgency == null)
                                    {
                                        //HandleNotFound(changeObject.agency, XMLFileName);
                                        break;
                                    }
                                    bizAgency.Address = changeObject.line1;
                                    bizAgency.Address2 = changeObject.line2;
                                    bizAgency.City = changeObject.city;
                                    bizAgency.State = changeObject.usState != null ? changeObject.usState.label : null;
                                    bizAgency.Zip = changeObject.zip;

                                    bizAgency.ModifiedBy = DefaultValues.SystemIdentifier;
                                    bizAgency.ModifiedDt = DateTime.Now;
                                }
                            }
                            Commit();
                            AddAgencyToCache(companyNumber);

                            break;
                        }
                    #endregion

                    #region locationaddress
                    case "com.bts.aps.domain.LocationAddress":
                        {
                            LocationAddress changeObject = (LocationAddress)aPSChange.detailDomainObjectInstance(forIndex);
                            if (modificationType == ModificationType.Delete)
                            {
                                // there will always be a primary location. This case should never arise.
                                // ignore location insert taken care of during agency insert
                            }
                            else
                            {
                                if (changeObject.location.isPrimaryLocation)
                                {
                                    BCS.Biz.Agency bizAgency = null;
                                    if (aPSChange.isRootClassAgency)
                                        bizAgency = GetBizAgency(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode);
                                    else
                                        bizAgency = GetBizAgency(companyNumber, changeObject.location.agency.id, changeObject.location.agency.agencyCode);


                                    if (bizAgency == null)
                                    {
                                        HandleNotFound(changeObject.location.agency, XMLFileName);
                                        break;
                                    }
                                    bizAgency.Address = changeObject.line1;
                                    bizAgency.Address2 = changeObject.line2;
                                    bizAgency.City = changeObject.city;
                                    bizAgency.State = changeObject.USState != null ? changeObject.USState.label : null;
                                    bizAgency.Zip = changeObject.zip;

                                    bizAgency.ModifiedBy = DefaultValues.SystemIdentifier;
                                    bizAgency.ModifiedDt = DateTime.Now;
                                }
                            }
                            Commit();

                            AddAgencyToCache(companyNumber);
                            break;
                        }
                    #endregion

                    #region locationcommunication
                    case "com.bts.aps.domain.LocationCommunication":
                        {
                            LocationCommunication changeObject = (LocationCommunication)aPSChange.detailDomainObjectInstance(forIndex);

                            if (changeObject.location.isPrimaryLocation)
                            {
                                BCS.Biz.Agency bizAgency = null;
                                if (aPSChange.isRootClassAgency)
                                    bizAgency = GetBizAgency(companyNumber, aPSChange.rootAgencyApsId, aPSChange.rootAgencyCode);
                                else
                                    bizAgency = GetBizAgency(companyNumber, changeObject.location.agency.id, changeObject.location.agency.agencyCode);

                                if (bizAgency == null)
                                {
                                    HandleNotFound(changeObject.location.agency, XMLFileName);
                                    break;
                                }
                                StringDictionary dictionary = ConfigurationSettings.Instance.GetCommunicationDictionary();

                                if (null != changeObject.communicationType)
                                {
                                    string bcsProperty = dictionary[changeObject.communicationType.label];
                                    if (bcsProperty != null)
                                    {
                                        PropertyInfo pi = bizAgency.GetType().GetProperty(bcsProperty);
                                        if (null != pi)
                                        {
                                            if (pi.PropertyType.FullName.StartsWith("System.Data.SqlTypes"))
                                            {
                                                object objConverted = Common.GetConvertedObject(changeObject.communicationValue, pi.PropertyType);
                                                pi.SetValue(bizAgency, objConverted, null);
                                            }
                                            else
                                            {
                                                object objConverted = Convert.ChangeType(changeObject.communicationValue, pi.PropertyType);
                                                pi.SetValue(bizAgency, objConverted, null);
                                            }
                                        }
                                    }
                                }
                            }
                            Commit();
                            break;
                        }
                    #endregion

                    #region agencyxref
                    case "com.bts.aps.domain.AgencyXRef":
                        {
                            // TODO: how do we handle multiple parents
                            AgencyXRef changeObject = (AgencyXRef)aPSChange.detailDomainObjectInstance(forIndex);
                            foreach (AgencyXRef parentAssoc in changeObject.childAgency.agencyXRefParents)
                            {
                                foreach (AgencyXRefAssociationType assType in parentAssoc.agencyXRefAssociationTypes)
                                {
                                    if (IsUpdateableProperty(lowerClassName, assType)) // TODO: Confirm what type of association type OR any association type
                                    {
                                        BCS.Biz.Agency bizChildAgency = GetBizAgency(companyNumber, changeObject.childAgency.id, changeObject.childAgency.agencyCode);
                                        if (bizChildAgency == null)
                                        {
                                            HandleNotFound(changeObject.childAgency, XMLFileName);
                                            break;
                                        }
                                        BCS.Biz.Agency bizParentAgency = GetBizAgency(companyNumber, changeObject.parentAgency.id, changeObject.parentAgency.agencyCode);
                                        if (bizParentAgency == null)
                                        {
                                            HandleNotFound(changeObject.parentAgency, XMLFileName);
                                            break;
                                        }
                                        switch (modificationType)
                                        {
                                            case ModificationType.Insert:
                                                {
                                                    bizChildAgency.MasterAgencyId = bizParentAgency.MasterAgencyId;
                                                }
                                                break;
                                            case ModificationType.Update:
                                                // TODO: 
                                                break;
                                            case ModificationType.Delete:
                                                {
                                                    bizChildAgency.MasterAgencyId = SqlInt32.Null;
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                            Commit();
                            break;
                        }
                    #endregion

                    default:
                        break;
                }
                forIndex++;
            }
        }

        #endregion

        #region Add/Remove Cached Agencies

        private void AddAgencyToCache(string companyNumber)
        {
            try
            {
                if (DefaultValues.BCSService1 != null && DefaultValues.BCSService1.Length > 0)
                {
                    cacheUtility.Url = DefaultValues.BCSService1;
                    cacheUtility.UseDefaultCredentials = true;
                    cacheUtility.ProcessAgencyCache(companyNumber, CacheChangeType.Insert);
                }

                if (DefaultValues.BCSService2 != null && DefaultValues.BCSService2.Length > 0)
                {
                    cacheUtility.Url = DefaultValues.BCSService2;
                    cacheUtility.ProcessAgencyCache(companyNumber, CacheChangeType.Insert);
                }
            }
            catch (Exception ex)
            {
                string message = "Exception adding to Cache."; ;
                ExceptionManager.Notify(message, ExceptionManager.GetFullDetailsString(ex), MailPriority.High);

            }
        }

        private void UpdatePersonnelCache(string companyNumber)
        {
            try
            {
                if (DefaultValues.BCSService1 != null && DefaultValues.BCSService1.Length > 0)
                {
                    cacheUtility.Url = DefaultValues.BCSService1;
                    cacheUtility.UseDefaultCredentials = true;
                    cacheUtility.ProcessUWCache(companyNumber);
                }

                if (DefaultValues.BCSService2 != null && DefaultValues.BCSService2.Length > 0)
                {
                    cacheUtility.Url = DefaultValues.BCSService2;
                    cacheUtility.UseDefaultCredentials = true;
                    cacheUtility.ProcessUWCache(companyNumber);
                }

            }
            catch (Exception ex)
            {
                string message = "Exception adding to Cache."; ;
                ExceptionManager.Notify(message, ExceptionManager.GetFullDetailsString(ex), MailPriority.High);

            }
        }

        #endregion

        #region DB Updates & Inserts
        private static void FillAgency(BCS.Biz.Agency bizAgency, com.bts.aps.domain.Agency changeObject, string companyNumber)
        {
            #region Location Address & Communication

            Location location = null;
            foreach (Location var in changeObject.locations)
            {
                if (IsUpdateableProperty("agencylocation", var))
                {
                    location = var;
                }
            }
            if (location != null)
            {
                bizAgency.Address = location.line1;
                bizAgency.Address2 = location.line2;
                bizAgency.City = location.city;
                bizAgency.Zip = location.zip;
                if (location.usState != null)
                {
                    bizAgency.State = location.usState.description;
                }
                else
                {
                    bizAgency.State = null;
                }

                if (null != location.locationCommunications && 0 < location.locationCommunications.Length)
                {
                    StringDictionary dictionary = ConfigurationSettings.Instance.GetCommunicationDictionary();
                    foreach (LocationCommunication locationCommunication in location.locationCommunications)
                    {
                        if (null != locationCommunication.communicationType)
                        {
                            string bcsProperty = dictionary[locationCommunication.communicationType.label];
                            if (bcsProperty != null)
                            {
                                PropertyInfo pi = bizAgency.GetType().GetProperty(bcsProperty);
                                SqlDecimal numValue = StripNonAlphaNumericCharacters(locationCommunication.communicationValue);

                                if (pi != null && numValue != SqlDecimal.Null &&
                                    numValue > SqlDecimal.MinValue)
                                {
                                    pi.SetValue(bizAgency, numValue, null);
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region Agency Name
            AgencyName agencyName = null;
            foreach (AgencyName var in changeObject.agencyNames)
            {
                // TODO: add/update only current agency name. waiting on what constitutes a current name

                // TODO: pending TODO: above, just grab the first name
                agencyName = var;
                if (!string.IsNullOrEmpty(agencyName.dbaName))
                {
                    bizAgency.AgencyName = agencyName.dbaName;
                    break;
                }
                else if (!string.IsNullOrEmpty(agencyName.legalName))
                {
                    bizAgency.AgencyName = agencyName.legalName;
                    break;
                }
                else if (!string.IsNullOrEmpty(agencyName.shortName))
                {
                    bizAgency.AgencyName = agencyName.shortName;
                    break;
                }

            }
            //if (agencyName != null && !string.IsNullOrEmpty(agencyName.legalName))
            //{
            //    bizAgency.AgencyName = DetermineAgencyName(companyNumber, agencyName);
            //}
            //else
            //{
            //    // TODO: sometimes the agency names come as null. pending investigation, resort to defaulting
            //    bizAgency.AgencyName = "BCS Unknown";
            //}
            #endregion

            bizAgency.AgencyNumber = changeObject.agencyCode;
            bizAgency.APSId = changeObject.id;
            if (changeObject.branch != null)
                bizAgency.BranchId = changeObject.branch.id.ToString(); // TODO: id?
            else
                bizAgency.BranchId = null;

            if (changeObject.agencyBranches != null && changeObject.agencyBranches.Length > 0)
            {
                var xmlBranch = changeObject.agencyBranches[0].branch;
                var branch = GetBizAgencyBranch(xmlBranch.id);
                if (branch == null)
                {
                    var branchDm = CommonGetDataManager();
                    var newBranch = branchDm.NewAgencyBranch(bizAgency.CompanyNumberId.Value);
                    newBranch.APSId = xmlBranch.id;
                    newBranch.Code = xmlBranch.code;
                    branchDm.CommitAll();

                    branch = GetBizAgencyBranch(xmlBranch.id);
                }

                branch.Code = xmlBranch.code;
                branch.Name = xmlBranch.name;

                bizAgency.AgencyBranchId = branch.Id;
            }
            else
            {
                bizAgency.AgencyBranchId = SqlInt32.Null;
            }

            #region Cancel Date
            if (GetCompanySetting("UsesRenewalCancelDate", companyNumber))
            {
                if (changeObject.renewalCancelDateSpecified)
                    bizAgency.RenewalCancelDate = SqlDateTime.Parse(changeObject.renewalCancelDate);
            }
            if (changeObject.newCancelDateSpecified && GetCompanySetting("UsesNewBusinessCancelDate", companyNumber)) // else cancel date
            {
                DateTime result = ConvertToDateTime(changeObject.newCancelDate);
                if (result != null && result > DateTime.MinValue)
                    bizAgency.CancelDate = result;
            }
            else if (changeObject.renewalCancelDateSpecified && !GetCompanySetting("UsesNewBusinessCancelDate", companyNumber)) // renewal cancel date specified, set that as cancel date
            {
                DateTime result = ConvertToDateTime(changeObject.renewalCancelDate);
                if (result != null && result > DateTime.MinValue)
                    bizAgency.CancelDate = result;
            }
            else // else null
            {
                bizAgency.CancelDate = SqlDateTime.Null;
            }
            #endregion

            if (changeObject.appointmentDateSpecified) // TODO: renewalDate?
            {
                DateTime result = ConvertToDateTime(changeObject.appointmentDate);
                if (result != null && result > DateTime.MinValue)
                    bizAgency.EffectiveDate = result;

            }
            //effective dates are removed when an agency has been cancelled.  we don't want to overwrite the original date to null.
            //else
            //{
            //    bizAgency.EffectiveDate = SqlDateTime.MinValue;
            //}

            bizAgency.FEIN = changeObject.taxId;

            #region referral
            if (changeObject.transferredToAgency != null)
            {
                //referral date
                if (!string.IsNullOrEmpty(changeObject.transferDate))
                    bizAgency.ReferralDate = ConvertToDateTime(changeObject.transferDate);
                else
                    bizAgency.ReferralDate = SqlDateTime.MinValue;

                //referral agency
                if (!string.IsNullOrEmpty(changeObject.transferredToAgency.agencyCode))
                    bizAgency.ReferralAgencyNumber = changeObject.transferredToAgency.agencyCode;
                else
                    bizAgency.ReferralAgencyNumber = string.Empty;
            }
            #endregion

        }

        private static void FillContact(BCS.Biz.Contact bizContact, AgencyPersonnel changeObject)
        {
            // note: all todos need to be clarified on the array properties
            bizContact.APSId = changeObject.id; // TODO: clarify it its changeObject.id or changeObject.personnel.id
            if (changeObject.personnel.dateOfBirthSpecified)
                bizContact.DateOfBirth = ConvertToDateTime(changeObject.personnel.dateOfBirth);
            //bizContact.Email = // TODO:
            //bizContact.Fax = // TODO: 
            bizContact.Name = string.Format("{0} {1}", changeObject.personnel.firstName, changeObject.personnel.lastName);
            //bizContact.PrimaryPhone = // TODO: 
            //bizContact.PrimaryPhoneExtension = // TODO: 
            if (changeObject.expirationDateSpecified)
                bizContact.RetirementDate = ConvertToDateTime(changeObject.expirationDate);
            //bizContact.SecondaryPhone = // TODO: 
            //bizContact.SecondaryPhoneExtension = // TODO:             
        }

        private static void FillAgent(BCS.Biz.Agent bizAgent, AgencyPersonnel changeObject)
        {
            bizAgent.APSId = changeObject.id;
            bizAgent.APSPersonnelId = changeObject.personnel.id;
            bizAgent.BrokerNo = changeObject.agentNumber;// changeObject.personnel.producerCode;
            if (changeObject.expirationDateSpecified)
                bizAgent.CancelDate = ConvertToDateTime(changeObject.expirationDate);

            if (changeObject.personnel.dateOfBirthSpecified)
                bizAgent.DOB = ConvertToDateTime(changeObject.personnel.dateOfBirth);
            if (changeObject.effectiveDateSpecified)
                bizAgent.EffectiveDate = ConvertToDateTime(changeObject.effectiveDate);
            bizAgent.FirstName = changeObject.personnel.firstName;

            bizAgent.Middle = changeObject.personnel.middleName;
            if (changeObject.personnel.namePrefix != null)
                bizAgent.Prefix = changeObject.personnel.namePrefix.label;

            if (changeObject.personnel.nameSuffix != null)
                bizAgent.SuffixTitle = changeObject.personnel.nameSuffix.label;
            bizAgent.Surname = changeObject.personnel.lastName;
            bizAgent.TaxId = changeObject.personnel.socialSecurityNumber;

            bizAgent.ModifiedBy = "APS Process";
            bizAgent.ModifiedDt = DateTime.Now;
        }
        #endregion

        #region Error Handles & Email Notification

        private static void HandleDuplicate(com.bts.aps.domain.Agency apsAgency, string strXMLFileName)
        {
            // TODO: Do something to the message in queue
            string message = string.Format("The Agency {0}, {1} was already found in BCS. Please review the XML {2}.",
                apsAgency.shortName, apsAgency.agencyCode, strXMLFileName);

            ExceptionManager.Notify(APSErrors.eAgency, message, MailPriority.High);
        }
        private static void HandleNotFound(com.bts.aps.domain.Agency apsAgency, string strXMLFileName)
        {
            // TODO: Do something to the message in queue
            string message = string.Format("The Agency {0}, {1} is not found in BCS. Please review the XML {2}.",
                apsAgency.shortName, apsAgency.agencyCode, strXMLFileName);

            ExceptionManager.Notify(APSErrors.eAgency, message, MailPriority.High);
        }
        private static void HandleNotFound(AgencyPersonnel apsAgencyPersonnel, string bcsRole, string strXMLFileName)
        {
            // TODO: Do something to the message in queue
            string message = string.Format("The {0} {1} is not found in BCS. Please review the XML {2}.", bcsRole, string.Format("{0} {1}",
                apsAgencyPersonnel.personnel.firstName, apsAgencyPersonnel.personnel.lastName), strXMLFileName);

            ExceptionManager.Notify(APSErrors.eAgencyPersonnel, message, MailPriority.High);
        }
        private static void HandleNotFound(AgencyLicense apsAgencyLicense, string strXMLFileName)
        {
            // TODO: Do something to the message in queue
            string message = string.Format("The Agency license {0} is not associated with agency {1} in BCS. Please review the XML {2}.",
                apsAgencyLicense.licenseState.label,
                apsAgencyLicense.agency.agencyCode,
                strXMLFileName);

            ExceptionManager.Notify(APSErrors.eAgencylicense, message, MailPriority.High);
        }
        private static void HandleNotFound(AgencyUnderwritingUnit apsAgencyUnderwritingUnit, string strXMLFileName)
        {
            // TODO: Do something to the message in queue
            string message = string.Format("The Agency underwriting unit {0} is not associated with agency {1} in BCS. Please review the XML {2}.",
                apsAgencyUnderwritingUnit.underwritingUnit.description,
                apsAgencyUnderwritingUnit.agency.agencyCode,
                strXMLFileName);

            ExceptionManager.Notify(APSErrors.eAgencyUnderwritingUnit, message, MailPriority.High);
        }
        private static void HandleNotFound(UnderwritingUnit apsUnderwritingUnit, string strXMLFileName)
        {
            // TODO: Do something to the message in queue
            string message = string.Format("The Underwriting unit {0} is not found in BCS. Please review the XML {1}.",
                apsUnderwritingUnit.description, strXMLFileName);

            ExceptionManager.Notify(APSErrors.eUnderwritingUnit, message, MailPriority.High);
        }
        private static void HandleNotFound(CompanyPersonnel apsCompanyPersonnel, string strXMLFileName)
        {
            // TODO: Do something to the message in queue
            string message = string.Format("The Person {0} {1} is not found in BCS. Please review the XML {2}.",
                apsCompanyPersonnel.firstName, apsCompanyPersonnel.lastName, strXMLFileName);

            ExceptionManager.Notify(APSErrors.eCompanyPersonnel, message, MailPriority.High);
        }
        private static void HandleNotFound(CompanyPersonnelInfo apsCompanyPersonnelInfo, string strXMLFileName)
        {
            // TODO: Do something to the message in queue
            string message = string.Format("The Person APSID - {0}, Initials- {1} is not found in BCS. Please review the XML {2}.",
                apsCompanyPersonnelInfo.id, apsCompanyPersonnelInfo.userInitials, strXMLFileName);

            ExceptionManager.Notify(APSErrors.eCompanyPersonnelInfo, message, MailPriority.High);
        }
        private static void HandleNotFound(CompanyPersonnelRoleType apsCompanyPersonnelRoleType, string strXMLFileName)
        {
            // TODO: Do something to the message in queue
            string message = string.Format("The Underwriting role {0} is not found in BCS. Please review the XML {1}.",
                apsCompanyPersonnelRoleType.description, strXMLFileName);

            ExceptionManager.Notify(APSErrors.eCompanyPersonnel, message, MailPriority.High);
        }
        private static void HandleNotFound(USState apsState, string strXMLFileName)
        {
            // TODO: Do something to the message in queue
            string message = string.Format("The State {0} is not found in BCS. Please review the XML {1}.",
                apsState.label, strXMLFileName);

            ExceptionManager.Notify(APSErrors.eAgency, message, MailPriority.High);
        }

        /// <summary>
        /// Notifying Failure Messages
        /// </summary>
        /// <param name="subject">string</param>
        /// <param name="body">string</param>
        /// <param name="priority">MailPriority</param>
        private static void Notify(string subject, string body, MailPriority priority)
        {
            MailAddress messageFrom = new MailAddress(DefaultValues.MailFrom);
            MailAddress messageTo = new MailAddress(DefaultValues.MailTo);

            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(messageFrom, messageTo);

            string ccAddresses = DefaultValues.MailCC;
            string[] ccAddrArray = ccAddresses.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string var in ccAddrArray)
            {
                string trimmedVar = var.Trim();
                if (OrmLib.Email.ValidateEmail(trimmedVar))
                    message.CC.Add(trimmedVar);
            }

            SmtpClient client = new SmtpClient(DefaultValues.MailServer);

            //message.Subject = string.Format(DefaultValues.MailSubject, emailType);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Priority = priority;

            message.Body = body;

            client.Send(message);
        }

        #endregion

        #region BCS Biz Things

        private static BCS.Biz.State GetBizState(string stateCode)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.State.Columns.Abbreviation, stateCode);
            return dm.GetState();
        }

        private static BCS.Biz.LineOfBusinessCollection GetBizUnderwritingUnits(string companyNumber)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            return dm.GetLineOfBusinessCollection();
        }

        private static BCS.Biz.Agency GetBizAgency(int companyNumberId, long apsAgencyId, string agencyCode)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Agency.CompanyNumber.Columns.Id, companyNumberId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Agency.Columns.APSId, apsAgencyId);
            BCS.Biz.Agency agency = dm.GetAgency(BCS.Biz.FetchPath.Agency.AgencyLicensedState, BCS.Biz.FetchPath.AgencyLineOfBusiness);

            if (agency == null)
            {
                if (DefaultValues.UseOtherCriteriaIfIdFails)
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Agency.CompanyNumber.Columns.Id, companyNumberId);
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Agency.Columns.AgencyNumber, agencyCode);
                    agency = dm.GetAgency(BCS.Biz.FetchPath.Agency.AgencyLicensedState, BCS.Biz.FetchPath.AgencyLineOfBusiness);
                    if (agency != null)
                        agency.APSId = apsAgencyId; // set the missing aps agency id
                }
            }
            return agency;
        }

        private static BCS.Biz.Agency GetBizAgency(string companyNumber, long apsAgencyId, string agencyCode)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Agency.Columns.APSId, apsAgencyId);
            BCS.Biz.Agency agency = dm.GetAgency(BCS.Biz.FetchPath.Agency.AgencyLicensedState, BCS.Biz.FetchPath.AgencyLineOfBusiness);

            if (agency == null)
            {
                if (DefaultValues.UseOtherCriteriaIfIdFails)
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Agency.Columns.AgencyNumber, agencyCode);
                    agency = dm.GetAgency(BCS.Biz.FetchPath.Agency.AgencyLicensedState, BCS.Biz.FetchPath.AgencyLineOfBusiness);
                    if (agency != null)
                        agency.APSId = apsAgencyId; // set the missing aps agency id
                }
            }
            return agency;
        }

        private static BCS.Biz.Agent GetBizAgent(long apsId, string agencyCode, string companyNumber)
        {
            List<string> agentNumbers = new List<string>();

            if (apsId == long.MinValue || string.IsNullOrEmpty(agencyCode) || string.IsNullOrEmpty(companyNumber))
                return null;

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Agent.Columns.APSId, apsId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Agent.Agency.Columns.AgencyNumber, agencyCode);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Agent.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            BCS.Biz.Agent agent = dm.GetAgent();

            return agent;
        }
        private static BCS.Biz.Agent GetBizAgentByPersonnelId(long personnelId, string companyNumber)
        {
            List<string> agentNumbers = new List<string>();

            if (personnelId == long.MinValue || string.IsNullOrEmpty(companyNumber))
                return null;

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Agent.Columns.APSPersonnelId, personnelId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Agent.Agency.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            BCS.Biz.Agent agent = dm.GetAgent();

            return agent;
        }


        private static BCS.Biz.LineOfBusiness GetBizLineOfBusiness(long apsLineOfBusinessId, string companyNumber, string uWDescription)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.LineOfBusiness.Columns.APSId, apsLineOfBusinessId);
            BCS.Biz.LineOfBusiness lineofbusiness = dm.GetLineOfBusiness();

            if (lineofbusiness == null)
            {
                if (DefaultValues.UseOtherCriteriaIfIdFails)
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.LineOfBusiness.Columns.Description, uWDescription);
                    lineofbusiness = dm.GetLineOfBusiness();
                    if (lineofbusiness != null)
                        lineofbusiness.APSId = apsLineOfBusinessId; // set the missing aps lineofbusiness id
                }
            }
            return lineofbusiness;
        }

        private static BCS.Biz.Person GetBizPerson(long apsPersonId, string companyNumber, string firstName, string lastName)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.Columns.APSId, apsPersonId);
            BCS.Biz.Person person = dm.GetPerson();

            if (person == null)
            {
                if (DefaultValues.UseOtherCriteriaIfIdFails)
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.Columns.FullName, string.Format("{0} {1}", firstName, lastName));
                    person = dm.GetPerson();
                    if (person != null)
                        person.APSId = apsPersonId; // set the missing aps person id
                }
            }
            return person;
        }

        private static BCS.Biz.Person GetBizPerson(long apsPersonId, string companyNumber, string userInitials)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.Columns.APSId, apsPersonId);
            BCS.Biz.Person person = dm.GetPerson();

            if (person == null)
            {
                if (DefaultValues.UseOtherCriteriaIfIdFails)
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.Columns.Initials, userInitials);
                    person = dm.GetPerson();
                    if (person != null)
                        person.APSId = apsPersonId; // set the missing aps person id
                }
            }
            return person;
        }


        private static BCS.Biz.UnderwritingRole GetBizUnderwritingRole(string companyNumber, string roleCode)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.UnderwritingRole.CompanyNumberUnderwritingRole.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.UnderwritingRole.Columns.RoleName, roleCode);
            BCS.Biz.UnderwritingRole underwritingRole = dm.GetUnderwritingRole();

            return underwritingRole;
        }


        private BCS.Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson GetBizAssignment(long apsId)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.APSId, apsId);
            return dm.GetAgencyLineOfBusinessPersonUnderwritingRolePerson();
        }
        private static BCS.Biz.AgencyLineOfBusinessPersonUnderwritingRolePerson GetBizAssignment(int agencyId, int uWId, int personId, int roleId)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agencyId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.LineOfBusinessId, uWId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, personId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, roleId);
            return dm.GetAgencyLineOfBusinessPersonUnderwritingRolePerson();
        }

        ///
        /// AgencyLineofBusinessUnderwritingRolePerson: Mapper issues resulted in developing sps for SELECT  Updates and Deletes
        /// 
        private static void GetBizAssignment(int iAgencyId, int iUWId, int iPersonId, int iRoleId,
            out int oAgencyId, out int oUWId, out int oPersonId, out int oRoleId, out bool oPrimary)
        {
            oAgencyId = 0;
            oUWId = 0;
            oPersonId = 0;
            oRoleId = 0;
            oPrimary = false;

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(DefaultValues.DSN))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("usp_GetAgencyLobUWPerson", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("@AgencyID", System.Data.SqlDbType.Int).Value = iAgencyId;
                    cmd.Parameters.Add("@UWID", System.Data.SqlDbType.Int).Value = iUWId;
                    cmd.Parameters.Add("@PersonID", System.Data.SqlDbType.Int).Value = iPersonId;
                    cmd.Parameters.Add("@RoleID", System.Data.SqlDbType.Int).Value = iRoleId;

                    cmd.Connection.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            Int32.TryParse(dr["AgencyID"].ToString(), out oAgencyId);
                            Int32.TryParse(dr["LineofBusinessId"].ToString(), out oUWId);
                            Int32.TryParse(dr["PersonId"].ToString(), out oPersonId);
                            Int32.TryParse(dr["UnderwritingRoleID"].ToString(), out oRoleId);
                            oPrimary = bool.Parse(dr["Primary"].ToString());
                        }
                    }
                }
            }
        }

        ///
        /// AgencyLineofBusinessUnderwritingRolePerson: Mapper issues resulted in developing sps for UPDATE and DELETE
        /// 
        private static void UpdatePrimaryBizAssignment(int iAgencyId, int iUWId, int iPersonId, int iRoleId, bool iPrimary, ModificationType modType)
        {
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(DefaultValues.DSN))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("usp_UpdateAgencyLobUWPerson", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("@AgencyID", System.Data.SqlDbType.Int).Value = iAgencyId;
                    cmd.Parameters.Add("@UWID", System.Data.SqlDbType.Int).Value = iUWId;
                    cmd.Parameters.Add("@PersonID", System.Data.SqlDbType.Int).Value = iPersonId;
                    cmd.Parameters.Add("@RoleID", System.Data.SqlDbType.Int).Value = iRoleId;
                    cmd.Parameters.Add("@Primary", System.Data.SqlDbType.Bit).Value = iPrimary;
                    cmd.Parameters.Add("@ModificationCode", System.Data.SqlDbType.Int).Value = modType;

                    cmd.Connection.Open();
                    SqlDataReader dr = cmd.ExecuteReader();

                }
            }
        }


        ///
        /// AgencyLineofBusiness: Mapper issues resulted in developing sps for SELECT  Updates and Deletes
        /// 
        private static void GetAgencyLineofBusiness(int iAgencyId, int iBizUWId,
                                                    out int oAgencyId, out int oBizUWId,
                                                    out long oApsId)
        {
            oAgencyId = 0;
            oBizUWId = 0;
            oApsId = 0;

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(DefaultValues.DSN))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("usp_GetAgencyLOB", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("@AgencyID", System.Data.SqlDbType.Int).Value = iAgencyId;
                    cmd.Parameters.Add("@UWID", System.Data.SqlDbType.Int).Value = iBizUWId;

                    cmd.Connection.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            Int32.TryParse(dr["AgencyID"].ToString(), out oAgencyId);
                            Int32.TryParse(dr["LineofBusinessId"].ToString(), out oBizUWId);
                            Int64.TryParse(dr["APSId"].ToString(), out oApsId);
                        }
                    }
                }
            }
        }

        ///
        /// AgencyLineofBusinessUnderwritingRolePerson: Mapper issues resulted in developing sps for UPDATE and DELETE
        /// 
        private static void UpdateAgencyLineOfBusiness(int iAgencyId, int iUWId, long iAPSId,
                                                       ModificationType modType)
        {
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(DefaultValues.DSN))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("usp_UpdateAgencyLOB", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("@AgencyID", System.Data.SqlDbType.Int).Value = iAgencyId;
                    cmd.Parameters.Add("@UWID", System.Data.SqlDbType.Int).Value = iUWId;
                    cmd.Parameters.Add("@APSID", System.Data.SqlDbType.BigInt).Value = iAPSId;
                    cmd.Parameters.Add("@ModificationCode", System.Data.SqlDbType.Int).Value = modType;

                    cmd.Connection.Open();
                    SqlDataReader dr = cmd.ExecuteReader();

                }
            }
        }


        private static BCS.Biz.Contact GetBizContact(long apsContactId, int bizAgencyId, string firstName, string lastName)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Contact.Columns.APSId, apsContactId);
            BCS.Biz.Contact contact = dm.GetContact();

            if (contact == null)
            {
                if (DefaultValues.UseOtherCriteriaIfIdFails)
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Contact.Columns.AgencyId, bizAgencyId);
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Contact.Columns.Name, string.Format("{0} {1}", firstName, lastName));
                    contact = dm.GetContact();
                    if (contact != null)
                        contact.APSId = apsContactId; // set the missing aps contact id
                }
            }
            return contact;
        }

        private static BCS.Biz.Agency GetNewAgency(string companyNumber)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            return dm.GetCompanyNumber().NewAgency();
        }
        private static BCS.Biz.Person GetNewPerson(string companyNumber)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            return dm.GetCompanyNumber().NewPerson();
        }

        private static BCS.Biz.CompanyNumberLineOfBusiness GetNewLineOfBusiness(string companyNumber)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            return dm.GetCompanyNumber().NewCompanyNumberLineOfBusiness();
        }
        private static BCS.Biz.AgencyBranch GetBizAgencyBranch(long apsId)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyBranch.Columns.APSId, apsId);
            return dm.GetAgencyBranch();
        }
        private static BCS.Biz.AgencyLicensedState GetBizAgencyLicenseState(long apsId)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLicensedState.Columns.APSId, apsId);
            return dm.GetAgencyLicensedState(BCS.Biz.FetchPath.AgencyLicensedState.Agency, BCS.Biz.FetchPath.AgencyLicensedState.State);
        }
        private static BCS.Biz.AgencyLicensedState GetBizAgencyLicenseState(string companyNumber, long apsAgencyId, string agencyCode, string stateCode)
        {
            BCS.Biz.Agency agency = GetBizAgency(companyNumber, apsAgencyId, agencyCode);
            if (agency == null)
                return null;
            BCS.Biz.State state = GetBizState(stateCode);

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLicensedState.Columns.AgencyId, agency.Id);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLicensedState.Columns.StateId, state.Id);
            return dm.GetAgencyLicensedState();
        }

        private BCS.Biz.AgencyLineOfBusiness GetBizAgencyLineOfBusiness(long apsId)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusiness.Columns.APSId, apsId);
            return dm.GetAgencyLineOfBusiness(BCS.Biz.FetchPath.AgencyLineOfBusiness.Agency, BCS.Biz.FetchPath.AgencyLineOfBusiness.LineOfBusiness);
        }

        private static BCS.Biz.DataManager CommonGetDataManager()
        {
            return new BCS.Biz.DataManager(DefaultValues.DSN);
        }
        private static void Commit()
        {
            dm.CommitAll();
            dm = CommonGetDataManager();

        }

        internal static bool IsUpdateableProperty(string fieldName, object varChangeObject)
        {
            bool updateable = false;

            try
            {
                if (mappingDoc == null)
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(@DefaultValues.MappingFile); // TODO:
                    mappingDoc = xDoc;
                }
                XmlElement root = mappingDoc.DocumentElement;
                string searchString = string.Format("/mappings/mapping[@id='{0}']", fieldName);
                XmlNode controlNode = root.SelectSingleNode(searchString);
                if (controlNode != null)
                {
                    string propertyName = controlNode.Attributes["name"].Value;
                    string propertyValue = controlNode.Attributes["value"].Value;
                    string[] propertyNames = propertyName.Split('.');
                    Type evaluatingType = varChangeObject.GetType();
                    object evaluatingObj = varChangeObject;
                    for (int i = 0; i < propertyNames.Length; i++)
                    {
                        string currentProperty = propertyNames[i];
                        PropertyInfo pi = evaluatingType.GetProperty(currentProperty);
                        if (pi == null)
                            return false;
                        evaluatingObj = pi.GetValue(evaluatingObj, null);
                        evaluatingType = pi.PropertyType;
                    }
                    updateable = (evaluatingObj.ToString() == propertyValue);
                }
                else
                    ExceptionManager.Notify(APSErrors.eGenericError, string.Format("Not Exists in Mapping file. This node {0} is not updateable in BCS", searchString), MailPriority.Normal);

            }
            catch (Exception ex)
            {
                ExceptionManager.Notify("Control Nodes incorrect in mappings.xml file", ExceptionManager.GetFullDetailsString(ex), MailPriority.High);
                throw ex;
            }

            return updateable;
        }

        internal static bool GetCompanySetting(string className, string companyNumber)
        {
            string company = string.Empty;
            string xPath = "/Companies/{0}/APSSetting";
            bool result = false;
            try
            {
                if (configDoc == null)
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(@DefaultValues.ConfigFile); // TODO:
                    configDoc = xDoc;
                }
                XmlElement root = configDoc.DocumentElement;
                string searchXPath = string.Empty;
                switch (companyNumber)
                {
                    case "52":
                        company = "AIC";
                        break;
                    case "80":
                        company = "USG";
                        break;
                    case "40":
                        company = "CWG";
                        break;
                    case "12":
                        company = "BMG";
                        break;
                    case "09":
                        company = "BNP";
                        break;
                    case "079":
                        company = "BTU";
                        break;
                    case "024":
                        company = "BRS";
                        break;
                    case "026":
                        company = "BFM";
                        break;
                    case "058":
                        company = "BLS";
                        break;
                    case "064":
                        company = "FIN";
                        break;
                    case "018":
                        company = "RIC";
                        break;
                    case "030":
                        company = "BRAC";
                        break;
                    case "021":
                        company = "BARS";
                        break;


                    default:
                        return result;

                }

                searchXPath = string.Format(xPath, company);

                XmlNodeList configNodes = root.SelectNodes(searchXPath);
                foreach (XmlNode controlNode in configNodes)
                {
                    string propertyID = controlNode.Attributes["id"].Value;
                    string propertyValue = controlNode.Attributes["value"].Value;
                    if (propertyID.Equals(className))
                    {
                        bool.TryParse(propertyValue, out result);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        internal static bool IsCompanyChange(string className, string companyNumber)
        {
            bool isCompanyChange = false;
            try
            {
                if (configDoc == null)
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(@DefaultValues.ConfigFile); // TODO:
                    configDoc = xDoc;
                }
                XmlElement root = configDoc.DocumentElement;
                string searchXPath = string.Empty;
                switch (companyNumber)
                {
                    case "52":
                        searchXPath = "/Companies/AIC/APSChange";
                        break;
                    case "80":
                        searchXPath = "/Companies/USG/APSChange";
                        break;
                    case "40":
                        searchXPath = "/Companies/CWG/APSChange";
                        break;
                    case "12":
                        searchXPath = "/Companies/BMG/APSChange";
                        break;
                    case "09":
                        searchXPath = "/Companies/BNP/APSChange";
                        break;
                    case "079":
                        searchXPath = "/Companies/BTU/APSChange";
                        break;
                    case "024":
                        searchXPath = "/Companies/BRS/APSChange";
                        break;
                    case "026":
                        searchXPath = "/Companies/BFM/APSChange";
                        break;
                    case "058":
                        searchXPath = "/Companies/BLS/APSChange";
                        break;
                    case "050":
                        searchXPath = "/Companies/CSM/APSChange";
                        break;
                    case "064":
                        searchXPath = "/Companies/FIN/APSChange";
                        break;
                    case "018":
                        searchXPath = "/Companies/RIC/APSChange";
                        break;
                    case "030":
                        searchXPath = "/Companies/BRAC/APSChange";
                        break;
                    case "021":
                        searchXPath = "/Companies/BARS/APSChange";
                        break;
                    default:
                        break;

                }

                XmlNodeList configNodes = root.SelectNodes(searchXPath);
                foreach (XmlNode controlNode in configNodes)
                {
                    string propertyID = controlNode.Attributes["id"].Value;
                    string propertyValue = controlNode.Attributes["value"].Value;
                    if (propertyID.Equals(className))
                    {
                        if (propertyValue.Equals("True"))
                        {
                            isCompanyChange = true;
                            break;
                        }
                        else
                        {
                            isCompanyChange = false;
                            break;
                        }
                    }
                }
                return isCompanyChange;
            }
            catch (Exception ex)
            {
                ExceptionManager.Notify("Control Nodes incorrect in companies.xml file", ExceptionManager.GetFullDetailsString(ex), MailPriority.High);
                throw ex;
            }
        }

        #endregion

        #region Utilities

        private SqlDateTime ConvertToSQLDate(string retDateinEST)
        {
            SqlDateTime retireDateSql = SqlDateTime.MinValue;
            string strRetireDate = retDateinEST;
            try
            {
                if (retDateinEST.Length > 0)
                {
                    string subStr = retDateinEST.Substring(retDateinEST.Length - 3, 3);
                    if (subStr.Equals("EST") || subStr.Equals("EDT"))
                    {
                        strRetireDate = retDateinEST.Substring(0, retDateinEST.Length - 3);
                    }
                    DateTime retireDate = DateTime.Parse(strRetireDate.Trim());
                    retireDateSql = new SqlDateTime(retireDate);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retireDateSql;

        }

        //not needed
        //private static string DetermineAgencyName(string companyNumber, AgencyName agencyNameObj)
        //{
        //    string result = string.Empty;
        //    switch (companyNumber)
        //    {
        //        case "52":
        //            result = agencyNameObj.shortName.Trim();
        //            break;
        //        case "80":
        //            result = agencyNameObj.dbaName.Trim();
        //            break;
        //        default:
        //            result = agencyNameObj.legalName.Trim();
        //            break;
        //    }
        //    return result;
        //}

        /// <summary>
        /// Returns a format provider useful for string parsing.
        /// </summary>
        protected static IFormatProvider FormatProvider
        {
            get
            {
                return System.Globalization.CultureInfo.InvariantCulture;
            }
        }

        /// <summary>
        /// Returns an value from the object specified.
        /// </summary>
        /// <param name="amount">The object from which to find the value.</param>
        /// <returns>The value.</returns>
        private static decimal SafeReadDecimal(string amount)
        {
            decimal result = decimal.MinValue;

            try
            {
                if (amount != null)
                    result = decimal.Parse(amount, FormatProvider);
            }
            catch (ArgumentNullException)
            {
                // return decimal.MinValue
            }
            catch (FormatException)
            {
                // return decimal.MinValue
            }
            catch (OverflowException)
            {
                // return decimal.MinValue
            }

            return result;
        }


        /// <summary>
        /// Strips all non-alphanumeric characters from a string.
        /// </summary>
        /// <param name="value">The string to parse.</param>
        /// <returns>A string with only alphanumeric characters.</returns>
        private static SqlDecimal StripNonAlphaNumericCharacters(string value)
        {
            string result = string.Empty;

            foreach (char character in value)
                if (char.IsDigit(character))
                    result += character;


            return (new SqlDecimal(SafeReadDecimal(result)));
        }

        /// <summary>
        /// Convert string to DateTime
        /// </summary>
        /// <param name="p">string</param>
        /// <returns>DateTime</returns>
        private static DateTime ConvertToDateTime(string p)
        {
            DateTime oldDate = new DateTime(1760, 1, 1);
            DateTime result = DateTime.MinValue;
            // replaces locale specific time zones
            p = p.Replace("PDT", "-3");
            p = p.Replace("PST", "-3");
            p = p.Replace("MDT", "-4");
            p = p.Replace("MST", "-4");
            p = p.Replace("CDT", "-5");
            p = p.Replace("CST", "-5");
            p = p.Replace("EDT", "-6");
            p = p.Replace("EST", "-6");

            DateTime converted = Convert.ToDateTime(p);
            if (converted > oldDate)
            {
                result = converted;
            }
            return result;
        }

        public enum ModificationType
        {
            Insert = 1,
            Update = 2,
            Delete = 3
        }

        /// <summary>
        /// Writing the APSChange message to Disk
        /// </summary>
        /// <param name="data">string</param>
        /// <param name="fileName">string</param>
        /// <returns>FileName -> String</returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Assert)]
        private string WriteFile(string data, string fileName)
        {
            try
            {
                string currentDirectory = DefaultValues.XmlDataFilesDirectory;
                //System.IO.Directory.GetCurrentDirectory();
                bool b = System.IO.Directory.Exists(currentDirectory + "\\XML");

                if (!b)
                    System.IO.Directory.CreateDirectory(currentDirectory + "\\XML");

                string actualFileName = string.Format("{0}\\{1}.{2}", currentDirectory + "\\XML",
                    string.IsNullOrEmpty(fileName) ? DateTime.Now.Ticks.ToString() : (fileName + "_" + DateTime.Now.Ticks.ToString()), "xml");

                System.IO.StreamWriter outFile = System.IO.File.CreateText(actualFileName);
                outFile.WriteLine(data);
                outFile.Close();

                return actualFileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Writing the APSChange message to Disk
        /// </summary>
        /// <param name="data">string</param>
        /// <param name="fileName">string</param>
        /// <returns>FileName -> String</returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Assert)]
        private string WriteFile(string companyNumber, string data, string fileName)
        {
            try
            {
                string currentDirectory = DirPath;
                //System.IO.Directory.GetCurrentDirectory();
                bool b = System.IO.Directory.Exists(currentDirectory + "\\XML");

                if (!b)
                    System.IO.Directory.CreateDirectory(currentDirectory + "\\XML");

                string actualFileName = string.Format("{0}\\{1}.{2}", currentDirectory + "\\XML",
                    string.IsNullOrEmpty(fileName) ? DateTime.Now.Ticks.ToString() : (companyNumber + "_" + fileName + "_" + DateTime.Now.Ticks.ToString()), "xml");

                System.IO.StreamWriter outFile = System.IO.File.CreateText(actualFileName);
                outFile.WriteLine(data);
                outFile.Close();

                return actualFileName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Logging

        [System.Runtime.CompilerServices.MethodImplAttribute(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)]

        static void LogCache(String data)
        {
            string logCacheFile = DefaultValues.LogCacheFile;
            System.IO.StreamWriter outFile = System.IO.File.AppendText(logCacheFile);
            outFile.WriteLine(DateTime.Now.ToString() + "\t" + data);
            outFile.Close();
        }

        #endregion

        #endregion

    }
}