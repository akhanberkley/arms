﻿using System;
using BCS.Biz;
using BCS.ProcessAPSMessage;
using com.bts.aps.domain.publish;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BCS.ProcessAPSMessageService_Tests
{
    [TestClass]
    public class ProcessQueueItem
    {
        [TestMethod]
        public void Process()
        {
            int logId = 1009963;

            var db = new DataManager(DefaultValues.DSN);
            db.QueryCriteria.Clear();
            db.QueryCriteria.And(BCS.Biz.JoinPath.APSXMLLog.Columns.Id, logId);
            var logItem = db.GetAPSXMLLog(FetchPath.APSXMLLog);

            var pm = new ProcessManager();
            var apsChange = APSChange.getInstance(logItem.XML, true);
            pm.ProcessChange(logItem.CompanyNumber, apsChange, "TestApp");
        }

        [TestMethod]
        public void TestQueue()
        {
            var queue = new MQClass("CWG.APS.BCS.REAL", "WMQTWS01", "CLIENT.FROM.DOTNET", "1414", "40");
            queue.Connect();

            while (true)
                System.Threading.Thread.Sleep(5000);
        }
    }
}
