using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace BCS.CWG.ClearSync.Database
{
    public class DBAccess
    {
        #region Private Properties
        private static string connectionString = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Connection String used to connect to the database
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                return connectionString;
            }
            set
            {
                connectionString = value;
            }
        }
        #endregion

        #region Helpers

        /// <summary>
        /// Helper method to convert the query parameters to where clause
        /// </summary>
        /// <param name="sd">Parameters to the query as name value pairs</param>
        /// <returns>Where clause that can be appended to a query</returns>
        private static string ConvertToWhereClause(StringDictionary sd)
        {
            string res = string.Empty;
            foreach (string key in sd.Keys)
            {
                res += string.Format(" {0} = '{1}' and ", key, sd[key]);
            }
            res = res.Remove(res.Length - 5);
            return res;
        }

        /// <summary>
        /// Helper method to convert the query parameters to insert clause
        /// </summary>
        /// <param name="val">Parameters to the query as name value pairs</param>
        /// <returns>Insert clause that can be appended to an insert query</returns>
        private static string ConvertToInsertIntoClause(StringDictionary val)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("( ");
            foreach (string key in val.Keys)
            {
                sb.Append(key);
                sb.Append(" , ");
            }
            sb.Remove(sb.Length - 2, 2);
            sb.Append(" ) VALUES ( ");
            foreach (string key in val.Keys)
            {
                sb.Append("'");
                sb.Append(val[key]);
                sb.Append("' , ");
            }
            sb.Remove(sb.Length - 2, 2);
            sb.Append(" )");

            return sb.ToString();
        }

        /// <summary>
        /// Helper method to convert the query parameters to set clause in an update query
        /// </summary>
        /// <param name="keyvalues">Parameters to the query as name value pairs</param>
        /// <returns>Insert clause that can be appended to an update query</returns>
        private static string ConvertToSetClause(StringDictionary keyvalues)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in keyvalues.Keys)
                sb.Append(string.Format(" {0} = '{1}' , ", key, keyvalues[key]));
            sb.Remove(sb.Length - 2, 2);
            return sb.ToString();
        }

        /// <summary>
        /// Helper method to convert the query parameters to a delete query
        /// </summary>
        /// <param name="values">Parameters to the query as name value pairs</param>
        /// <returns>where clause to a delete query</returns>
        private static string ConvertToDeleteClause(StringDictionary values)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in values.Keys)
                sb.Append(string.Format(" {0} = '{1}' AND ", key, values[key]));
            sb.Remove(sb.Length - 4, 4);
            return sb.ToString();
        }

        #endregion

        #region Query Based Methods

        /// <summary>
        /// Gets a record from the database
        /// </summary>
        /// <param name="tablename">from table</param>
        /// <param name="keys">where clause query parameters</param>
        /// <returns>A StringDictionary</returns>
        public static StringDictionary GetRecord(string tablename, StringDictionary keys)
        {
            string query = string.Format("SELECT * FROM {0} WHERE {1}", tablename, ConvertToWhereClause(keys));
            DataTable dtable = SQLExecutor.ExecuteQuery(query, ConnectionString);
            if (dtable == null)
                return null;

            StringDictionary d = new StringDictionary();
            foreach (DataColumn dcol in dtable.Columns)
            {
                d.Add(dcol.ColumnName.ToString(), dtable.Rows[0][dcol.ColumnName].ToString());
            }
            return d;
        }

        /// <summary>
        /// Adds a record to the database table
        /// </summary>
        /// <param name="tablename">to table</param>
        /// <param name="values">where clause query parameters</param>
        /// <returns>Affected rows</returns>
        public static int AddRecord(string tablename, StringDictionary values)
        {
            string query = string.Format("INSERT INTO {0} {1} ", tablename, ConvertToInsertIntoClause(values));
            DataTable dtable = SQLExecutor.ExecuteQuery(query, ConnectionString);
            if (dtable == null)
                return -1;

            return dtable.Rows.Count;
        }

        /// <summary>
        /// Deletes a record from the database
        /// </summary>
        /// <param name="tablename">from table</param>
        /// <param name="keyvalues">where clause query parameters</param>
        /// <returns>Affected rows</returns>
        public static int DeleteRecord(string tablename, StringDictionary keyvalues)
        {
            string query = string.Format("DELETE FROM {0} WHERE {1} ", tablename, ConvertToDeleteClause(keyvalues));
            DataTable dtable = SQLExecutor.ExecuteQuery(query, ConnectionString);
            if (dtable == null)
                return 0;

            return dtable.Rows.Count;
        }

        /// <summary>
        /// Updates a record to the database
        /// </summary>
        /// <param name="tablename">to table</param>
        /// <param name="keyvalues">where clause query parameters</param>
        /// <param name="newvalues">set clause query parameters</param>
        /// <returns>Affected rows</returns>
        public static int UpdateRecord(string tablename, StringDictionary keyvalues, StringDictionary newvalues)
        {
            string query = string.Format("UPDATE {0} SET {1} WHERE {2} ", tablename, ConvertToSetClause(newvalues), ConvertToWhereClause(keyvalues));
            DataTable dtable = SQLExecutor.ExecuteQuery(query, ConnectionString);
            if (dtable == null)
                return -1;

            return dtable.Rows.Count;
        }

        /// <summary>
        /// Gets a set of records from the database
        /// </summary>
        /// <param name="tablename">from table</param>
        /// <returns>A DataTable</returns>
        public static DataTable GetDataTable(string tablename)
        {
            return SQLExecutor.ExecuteQuery(string.Format("SELECT * FROM {0}", tablename), ConnectionString);
        }

        /// <summary>
        /// Gets a set of records from the database
        /// </summary>
        /// <param name="tablename">from table</param>
        /// <param name="args">Column names to be retrieved</param>
        /// <returns>A DataTable</returns>
        public static DataTable GetDataTable(string tablename, params string[] args)
        {
            string commandtext = string.Empty;
            try
            {
                commandtext = string.Format("SELECT DISTINCT {0} FROM {1}", string.Join(" , ", args), tablename);

                SqlDataAdapter adapter = new SqlDataAdapter(commandtext, ConnectionString);
                DataTable dtable = new DataTable();
                adapter.Fill(dtable);
                return dtable;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                s = ex.Source;
                s = ex.StackTrace;
                //throw new DataException("Problem with database access.");
                throw ex;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets a set of records from the database
        /// </summary>
        /// <param name="tablename">from table</param>
        /// <param name="where">where clause query parameters</param>
        /// <returns>A DataTable</returns>
        public static DataTable GetDataTable(string tablename, StringDictionary where)
        {
            string whereclause = ConvertToWhereClause(where);
            return SQLExecutor.ExecuteQuery(string.Format("SELECT * FROM {0} where {1}", tablename, whereclause), ConnectionString);
        }

        /// <summary>
        /// Method gets distinct rows with the column specified
        /// </summary>
        /// <param name="tablename">Name of the table</param>
        /// <param name="field">Name of the field</param>
        /// <returns>Distinct rows as a StringCollection</returns>
        public static StringCollection GetDistinctRows(string tablename, string field)
        {
            try
            {
                //TODO :: Call SQLExecutor Methods
                string commandtext = string.Format("SELECT DISTINCT {0} FROM {1}", field, tablename);
                SqlDataAdapter adapter = new SqlDataAdapter(commandtext, ConnectionString);
                DataTable dtable = new DataTable();
                adapter.Fill(dtable);
                StringCollection sc = new StringCollection();
                foreach (DataRow drow in dtable.Rows)
                    sc.Add(drow[field].ToString());
                return sc;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                s = ex.Source;
                s = ex.StackTrace;
                throw new DataException("Problem with database access.");
            }
            finally
            {
            }
        }


        /// <summary>
        /// Method returns a single value from the database
        /// </summary>
        /// <param name="tablename">Name of the database table</param>
        /// <param name="fieldname">Name of the field of the table</param>
        /// <param name="where">Where condition as a name value pairs</param>
        /// <returns>field value as a string</returns>
        public static string GetValue(string tablename, string fieldname, StringDictionary where)
        {
            try
            {
                string commandtext = string.Format("SELECT {0} FROM {1} WHERE {2}", fieldname, tablename, ConvertToWhereClause(where));
                SqlDataAdapter adapter = new SqlDataAdapter(commandtext, ConnectionString);
                DataTable dtable = new DataTable();
                adapter.Fill(dtable);
                return dtable.Rows[0][fieldname].ToString();
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                s = ex.Source;
                s = ex.StackTrace;
                throw new DataException("Problem with database access.");
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets Distinct rows with the fields specified
        /// </summary>
        /// <param name="tablename">Name of the table</param>
        /// <param name="fields">fields</param>
        /// <returns>Collection of DataRows</returns>
        public static DataRowCollection GetDistinctRows(string tablename, params string[] fields)
        {
            try
            {
                string commandtext = string.Format("SELECT DISTINCT {0} FROM {1}", string.Join(" , ", fields), tablename);
                SqlDataAdapter adapter = new SqlDataAdapter(commandtext, ConnectionString);
                DataTable dtable = new DataTable();
                adapter.Fill(dtable);
                return dtable.Rows;
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                s = ex.Source;
                s = ex.StackTrace;
                throw new DataException("Problem with database access.");
            }
            finally
            {
            }
        }


        #endregion

        #region SP Based Methods

        /// <summary>
        /// Method to extract a single record from a database table
        /// </summary>
        /// <param name="procName">Procedure name to execute, that returns a single record</param>
        /// <param name="keys">Parameters to the stored procedure as name value pairs</param>
        /// <returns>the record queried as StringDictionary with name as column names and value as column values</returns>
        public static StringDictionary GetRecordWSP(string procName, StringDictionary keys)
        {
            ArrayList parameters = new ArrayList();
            foreach (string key in keys.Keys)
                parameters.Add(new SqlParameter(key, keys[key]));

            DataTable dtable = SQLExecutor.ExecuteSP(procName, ConnectionString, parameters);
            if (dtable == null || dtable.Rows.Count == 0)
                return null;

            StringDictionary d = new StringDictionary();
            foreach (DataColumn dcol in dtable.Columns)
            {
                d.Add(dcol.ColumnName.ToString(), dtable.Rows[0][dcol.ColumnName].ToString());
            }
            return d;
        }

        /// <summary>
        /// Method to add a single record to a database table
        /// </summary>
        /// <param name="procName">Name of the stored procedure</param>
        /// <param name="values">Parameters to the stored procedure as name value pairs</param>
        /// <returns>A <see cref="DBResponse"/>DBResponse object</returns>
        public static DBResponse AddRecordWSP(string procName, StringDictionary values)
        {
            try
            {
                ArrayList parameters = new ArrayList();
                foreach (string key in values.Keys)
                {
                    parameters.Add(new SqlParameter(key, values[key]));
                }
                return DBResponse.GetSuccessResponse(SQLExecutor.ExecuteSPReturnScalar(procName, ConnectionString, parameters));
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2627)
                    return DBResponse.GetFailureResponse("Record with same primary key already exists.", ResponseCode.PrimaryKeyViolation);
            }
            return DBResponse.GetFailureResponse("Unkwown Error.");
        }

        /// <summary>
        /// Method to delete a single record to a database table
        /// </summary>
        /// <param name="procName">Name of the stored procedure</param>
        /// <param name="keyvalues">Parameters to the stored procedure as name value pairs</param>
        /// <returns>A <see cref="DBResponse"/>DBResponse object</returns>
        public static DBResponse DeleteRecordWSP(string procName, StringDictionary keyvalues)
        {
            try
            {
                ArrayList parameters = new ArrayList();
                foreach (string key in keyvalues.Keys)
                    parameters.Add(new SqlParameter(key, keyvalues[key]));
                return DBResponse.GetSuccessResponse(SQLExecutor.ExecuteSPReturnScalar(procName, ConnectionString, parameters));
            }
            catch (SqlException ex)
            {
                if (ex.Number == 547)
                    return DBResponse.GetFailureResponse("Record has a foreign key dependency.", ResponseCode.ForeignKeyViolation);
            }
            return DBResponse.GetFailureResponse("Unkwown Error.");
        }

        /// <summary>
        /// Method to update a single record to a database table
        /// </summary>
        /// <param name="procName">Name of the stored procedure</param>
        /// <param name="keyvalues">Input condition parameters to the stored procedure as name value pairs</param>
        /// <param name="newvalues">Input updatable parameters to the stored procedure as name value pairs</param>
        /// <returns>A <see cref="DBResponse"/>DBResponse object</returns>
        public static int UpdateRecordWSP(string procName, StringDictionary keyvalues, StringDictionary newvalues)
        {
            ArrayList parameters = new ArrayList();
            foreach (string key in keyvalues.Keys)
                parameters.Add(new SqlParameter(key, keyvalues[key]));
            foreach (string key in newvalues.Keys)
                parameters.Add(new SqlParameter(key, newvalues[key]));

            return SQLExecutor.ExecuteSPReturnScalar(procName, ConnectionString, parameters);
        }

        /// <summary>
        /// Method that retrives a set of records from the database
        /// </summary>
        /// <param name="procName">Name of the stored procedure</param>
        /// <returns>A DataTable</returns>
        public static DataTable GetDataTableWSP(string procName)
        {
            return SQLExecutor.ExecuteSP(procName, ConnectionString, null);
        }

        /// <summary>
        /// Method that retrives a set of records from the database
        /// </summary>
        /// <param name="procName">Name of the stored procedure</param>
        /// <param name="dict">Parameters to the stored procedure as name value pairs representing where conditions</param>
        /// <returns>A DataTable</returns>
        public static DataTable GetDataTableWSP(string procName, StringDictionary dict)
        {
            ArrayList al = new ArrayList();
            foreach (string key in dict.Keys)
                al.Add(new SqlParameter(key, dict[key]));
            return SQLExecutor.ExecuteSP(procName, ConnectionString, al);
        }

        /// <summary>
        /// Gets a distinct rows by executing a stored procedure
        /// </summary>
        /// <param name="tablename">Name of the table</param>
        /// <param name="field">Name of the field</param>
        /// <returns>the distinct fields as a StringCollection</returns>
        public static StringCollection GetDistinctRowsWSP(string tablename, string field)
        {
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("TABLE_NAME", tablename));
            parameters.Add(new SqlParameter("FIELD_NAME", field));
            DataTable dtable = SQLExecutor.ExecuteSP("GET_DISTINCT_ROWS", ConnectionString, parameters);
            StringCollection sc = new StringCollection();
            foreach (DataRow drow in dtable.Rows)
                sc.Add(drow[field].ToString());
            return sc;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public static SqlParameterCollection GetParameters(string procName)
        {
            using (SqlConnection conn = new SqlConnection(DBAccess.ConnectionString))
            {
                try
                {
                    SqlCommand command = new SqlCommand(procName, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    SqlCommandBuilder.DeriveParameters(command);

                    return command.Parameters;

                }
                catch (Exception ex)
                { throw ex; }
            }
        }
    }
}
