using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace BCS.CWG.ClearSync
{
    class Common
    {

        #region Email methods
        internal static void SendEmail(string subject, string body)
        {
            MailAddress messageFrom = new MailAddress(Properties.Settings.Default.MailFrom);
            MailAddress messageTo = new MailAddress(Properties.Settings.Default.MailTo);

            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(messageFrom, messageTo);

            string ccAddresses = Properties.Settings.Default.MailCc;
            string[] ccAddrArray = ccAddresses.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string var in ccAddrArray)
            {
                string trimmedVar = var.Trim();
                if (OrmLib.Email.ValidateEmail(trimmedVar))
                    message.CC.Add(trimmedVar);
            }

            SmtpClient client = new SmtpClient(Properties.Settings.Default.MailServer);

            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Priority = MailPriority.High;

            message.Body = body;

            client.Send(message);
        }

        internal static void SendEmail(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("An Exception occurred");
            sb.AppendLine("Message : " + ex.Message);
            sb.AppendLine("Source : " + ex.Source);
            if (ex.InnerException != null)
                sb.AppendLine("Inner Exception : " + ex.InnerException);

            SendEmail("Status: Failure", sb.ToString());
        }
        #endregion

        internal static BCS.Biz.DataManager GetDataManager()
        {
            return new BCS.Biz.DataManager(Properties.Settings.Default.TargetDSN);
        }
    }
}
