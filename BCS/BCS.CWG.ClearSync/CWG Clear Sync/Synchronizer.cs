using System;
using System.Collections.Generic;
using System.Text;
using BCS.CWG.ClearSync.Sync.Objects;
using BTS.LogFramework;
using System.Reflection;
using System.Collections.Specialized;
using System.Data.SqlClient;

namespace BCS.CWG.ClearSync
{
    class Synchronizer
    {
        public static DateTime Synchronize(DateTime lastRunDate)
        {
            //lastRunDate = DateTime.Parse("12/09/2010");
            DateTime thisRunDate = lastRunDate;
            BCS.Biz.DataManager dm = Common.GetDataManager();
            BCS.Biz.CompanyNumberAttributeCollection attributes = GetUsedAttributes(dm);
            BCS.Biz.CompanyNumberCodeTypeCollection codeTypes = GetUsedCodeTypes(dm);

            SyncEntries entries = BCS.CWG.ClearSync.Sync.DataManager.GetEntriesToSync(lastRunDate);
            //#region TEST small load
            //List<SyncEntry> items = new List<SyncEntry>();
            //foreach (SyncEntry var in entries.Items)
            //{
            //    if (var.Policy_Num == "402863718")
            //        items.Add(var);
            //}
            //entries.Items = items.ToArray();
            //items.AddRange(entries.Items);
            //entries.Items = items.GetRange(9000, items.Count - 9000).ToArray();
            //#endregion

            #region code to serialize source entries to xml for analysis
            if (Properties.Settings.Default.SaveEntries)
            {
                SyncEntries toSerialize = entries;
                System.Xml.Serialization.XmlSerializerNamespaces namespaces = new System.Xml.Serialization.XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);

                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(toSerialize.GetType());
                System.Xml.XmlWriterSettings settings = new System.Xml.XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                //settings.NewLineChars = string.Empty;
                settings.Encoding = Encoding.UTF8;

                StringBuilder sb = new StringBuilder();
                System.Xml.XmlWriter w = System.Xml.XmlWriter.Create(sb, settings);

                try
                {
                    x.Serialize(w, toSerialize, namespaces);

                    try
                    {
                        string dirName = "XML";
                        string baseDirPath = System.IO.Directory.GetCurrentDirectory();
                        string xmlDirPath = string.Format(@"{0}\{1}", baseDirPath, dirName);
                        Console.WriteLine(xmlDirPath);
                        System.IO.DirectoryInfo xmlDir = null;
                        if (!System.IO.Directory.Exists(xmlDirPath))
                        {
                            xmlDir = System.IO.Directory.CreateDirectory(xmlDirPath);
                        }
                        else
                        {
                            xmlDir = System.IO.Directory.CreateDirectory(xmlDirPath);
                        }

                        string fileName = string.Format("{0}.xml", DateTime.Now.ToString("MMddyyyyHHmmss"));
                        string fullFilePath = string.Format(@"{0}\{1}", xmlDirPath, fileName);
                        Console.WriteLine(fullFilePath);

                        System.IO.File.AppendAllText(string.Format(@"{0}\{1}", xmlDirPath, fileName), sb.ToString());
                    }
                    catch (System.IO.IOException ioex)
                    {
                        LogCentral.Current.LogWarn("Exception saving source entries", ioex);
                    }
                }
                catch (Exception ex)
                {
                    // we will not treat this as show stopper and just log
                    LogCentral.Current.LogWarn("Unable to serialize source entries", ex);
                }
            }
            #endregion

            Properties.Settings.Default.TotalCount = entries.Items.Length;
            Console.WriteLine("Retrived {0} entries to sync.", entries.Items.Length);
            int currentIndex = 0;
            int notFoundCount = 0;
            foreach (SyncEntry var in entries.Items)
            {
                currentIndex++;
                if (currentIndex % 1000 == 0)
                    Console.WriteLine(DateTime.Now);
                Console.WriteLine("Processing entry {0} of {1} Policy Number {2}.", currentIndex, entries.Items.Length, var.Policy_Num);
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.CompanyNumberId, Properties.Settings.Default.CompanyNumberId);
                // strip leading 40 and search
                dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.PolicyNumber, var.Policy_Num.Remove(0, 2));
                // including the effective date as a filter as well , since there will be renewals coming through
                if (var.Policy_Tran_Type == "RN")
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.EffectiveDate, var.Policy_Eff_Dt);

                BCS.Biz.SubmissionCollection submissions = dm.GetSubmissionCollection(
                    BCS.Biz.FetchPath.Submission.SubmissionCompanyNumberAttributeValue, BCS.Biz.FetchPath.Submission.SubmissionCompanyNumberCode);

                if (submissions == null || submissions.Count == 0)
                {
                    notFoundCount++;
                    // commenting due to too many entries generated. Saving xml will overcome this.
                    //LogCentral.Current.LogError(string.Format("Unable to find submission(s) with policy number {0}.", var.Policy_Num));
                    continue;
                }
                if (submissions.Count > 1)
                {
                    LogCentral.Current.LogWarn(string.Format("Multiple submissions were found with policy number {0}.", var.Policy_Num));
                }
                Console.WriteLine("\t\t # {0}", submissions.Count);
                foreach (BCS.Biz.Submission sVar in submissions)
                {
                    int bcsid;
                    BCS.Biz.SubmissionCompanyNumberAttributeValue varToUpdate;
                    BCS.Biz.SubmissionCompanyNumberCode codeToUpdate;
                    #region Policy Premium
                    if (var.TW_PremSpecified)
                    {
                        bcsid = attributes.FindByLabel("Policy Premium").Id;
                        varToUpdate = sVar.SubmissionCompanyNumberAttributeValues.FindByCompanyNumberAttributeId(bcsid);
                        if (varToUpdate == null)
                        {
                            varToUpdate = sVar.NewSubmissionCompanyNumberAttributeValue();
                            varToUpdate.CompanyNumberAttributeId = bcsid;
                        }
                        varToUpdate.AttributeValue = var.TW_Prem.ToString();
                    }
                    #endregion
                    #region SIC Code
                    if (var.SICSpecified)
                    {
                        bcsid = attributes.FindByLabel("SIC Code").Id;
                        varToUpdate = sVar.SubmissionCompanyNumberAttributeValues.FindByCompanyNumberAttributeId(bcsid);
                        if (varToUpdate == null)
                        {
                            varToUpdate = sVar.NewSubmissionCompanyNumberAttributeValue();
                            varToUpdate.CompanyNumberAttributeId = bcsid;
                        }
                        varToUpdate.AttributeValue = var.SIC.ToString();
                    }
                    #endregion
                    #region NAICS Code
                    if (var.NAICSSpecified)
                    {
                        bcsid = attributes.FindByLabel("NAICS Code").Id;
                        varToUpdate = sVar.SubmissionCompanyNumberAttributeValues.FindByCompanyNumberAttributeId(bcsid);
                        if (varToUpdate == null)
                        {
                            varToUpdate = sVar.NewSubmissionCompanyNumberAttributeValue();
                            varToUpdate.CompanyNumberAttributeId = bcsid;
                        }
                        varToUpdate.AttributeValue = var.NAICS.ToString();
                    }
                    #endregion
                    #region BUS Cat Code
                    bcsid = attributes.FindByLabel("BUS Cat Code").Id;
                    varToUpdate = sVar.SubmissionCompanyNumberAttributeValues.FindByCompanyNumberAttributeId(bcsid);
                    if (varToUpdate == null)
                    {
                        varToUpdate = sVar.NewSubmissionCompanyNumberAttributeValue();
                        varToUpdate.CompanyNumberAttributeId = bcsid;
                    }
                    varToUpdate.AttributeValue = var.Bus_Cat.ToString();
                    #endregion
                    #region Sub/Policy Status

                    // handling sub status updates if this is a Renewal , previously CWG sync process used to handle only New Business or Application types

                    if (!String.IsNullOrEmpty(var.Policy_Tran_Type.ToString()) && (var.Policy_Tran_Type.ToString() == "RN"))
                    {

                        sVar.SubmissionStatusId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get(var.Policy_Status.Trim()));
                    }


                    #endregion
                    #region prior carrier

                    string priorCarrier = var.Prior_Carrier.Trim();

                    try
                    {
                        bcsid = codeTypes.FindByCodeName("Prior Carrier").Id;

                        codeToUpdate = sVar.SubmissionCompanyNumberCodes.FindByCompanyNumberCodeId(GetCompanyNumberCode(sVar.Id, bcsid));

                        if (!string.IsNullOrWhiteSpace(priorCarrier))
                        {
                            dm.QueryCriteria.Clear();
                            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.CompanyCodeTypeId, bcsid);
                            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Code, priorCarrier);
                            Biz.CompanyNumberCode code = dm.GetCompanyNumberCode();

                            //Add the code if not found
                            if (code == null)
                            {
                                code = AddCompanyCode(bcsid, priorCarrier);
                            }

                            //Update the sumission code
                            if (code != null)
                            {
                                if (codeToUpdate == null)
                                    codeToUpdate = sVar.NewSubmissionCompanyNumberCode();

                                codeToUpdate.CompanyNumberCodeId = code.Id;
                            }
                            else
                            {
                                string body = string.Format("Error adding Policy Program Code, {0}, for Submission {1}", priorCarrier, sVar.SubmissionNumber.ToString());
                                Common.SendEmail("Error Adding Policy Program Code", body);
                                codeToUpdate = null;
                            }
                        }
                        else //delete the code
                        {
                            if (codeToUpdate != null)
                            {
                                codeToUpdate.Delete();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        //do nothing.
                    }
                    #endregion
                    #region Policy Program Code
                    string progCode = var.Prog_Code.ToString().Trim();

                    //Get the CodeTypeID
                    bcsid = codeTypes.FindByCodeName("Policy Program Code").Id;

                    //Get the code to update
                    codeToUpdate = sVar.SubmissionCompanyNumberCodes.FindByCompanyNumberCodeId(GetCompanyNumberCode(sVar.Id, bcsid));

                    if (!string.IsNullOrEmpty(progCode))
                    {
                        dm.QueryCriteria.Clear();
                        dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.CompanyCodeTypeId, bcsid);
                        dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.Code, progCode);
                        Biz.CompanyNumberCode code = dm.GetCompanyNumberCode();

                        //Add the code if not found
                        if (code == null)
                        {
                            code = AddCompanyCode(bcsid, progCode);
                        }

                        //Update the sumission code
                        if (code != null)
                        {
                            if (codeToUpdate == null)
                                codeToUpdate = sVar.NewSubmissionCompanyNumberCode();

                            codeToUpdate.CompanyNumberCodeId = code.Id;
                        }
                        else
                        {
                            string body = string.Format("Error adding Policy Program Code, {0}, for Submission {1}", progCode, sVar.SubmissionNumber.ToString());
                            Common.SendEmail("Error Adding Policy Program Code", body);
                            codeToUpdate = null;
                        }
                    }
                    else //delete the code
                    {
                        if (codeToUpdate != null)
                        {
                            codeToUpdate.Delete();
                        }
                    }
                    #endregion

                    sVar.UpdatedBy = Properties.Settings.Default.Identifier;
                    sVar.UpdatedDt = DateTime.Now;
                }
                dm.CommitAll();
                if (notFoundCount != currentIndex)
                {
                    if (currentIndex % Properties.Settings.Default.BatchOf == 0)
                    {
                        Console.WriteLine("Committing at index {0}.", currentIndex);
                        dm.CommitAll();
                    }
                }
                thisRunDate = var.Date_Run;
            }
            if (notFoundCount != currentIndex)
            {
                Console.WriteLine("Committing final.");
                dm.CommitAll();
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Nothing to commit");
            }
            Properties.Settings.Default.NotFoundCount = notFoundCount;
            return thisRunDate;
        }

        private static BCS.Biz.CompanyNumberAttributeCollection GetUsedAttributes(BCS.Biz.DataManager dm)
        {
            StringCollection mappedAttributes = Properties.Settings.Default.MappedAttributes;
            StringCollection labels = new StringCollection();
            foreach (string var in mappedAttributes)
            {
                string[] spl = var.Split(new string[] { "+-" }, StringSplitOptions.RemoveEmptyEntries);
                labels.Add(spl[1]);
            }
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberAttribute.Columns.CompanyNumberId, Properties.Settings.Default.CompanyNumberId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberAttribute.Columns.Label, labels, OrmLib.MatchType.In);
            BCS.Biz.CompanyNumberAttributeCollection wantedAttritbutes = dm.GetCompanyNumberAttributeCollection();

            if (wantedAttritbutes.Count != labels.Count)
            {
                throw new ApplicationException("Not all attributes to be updated were found in Clearance");
            }
            return wantedAttritbutes;
        }

        private static BCS.Biz.CompanyNumberCodeTypeCollection GetUsedCodeTypes(BCS.Biz.DataManager dm)
        {
            StringCollection mappedCodes = Properties.Settings.Default.MappedCodeTypes;
            StringCollection labels = new StringCollection();
            foreach (string var in mappedCodes)
            {
                string[] spl = var.Split(new string[] { "+-" }, StringSplitOptions.RemoveEmptyEntries);
                labels.Add(spl[1]);
            }
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCodeType.Columns.CompanyNumberId, Properties.Settings.Default.CompanyNumberId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCodeType.Columns.CodeName, labels, OrmLib.MatchType.In);
            BCS.Biz.CompanyNumberCodeTypeCollection wantedCodes = dm.GetCompanyNumberCodeTypeCollection();

            if (wantedCodes.Count != labels.Count)
            {
                throw new ApplicationException("Not all attributes to be updated were found in Clearance");
            }
            return wantedCodes;
        }

        /*
        private static StringCollection GetUsedCodes(BCS.Biz.DataManager dm,int codeTypeID)
        {
            StringCollection codes = new StringCollection();
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCode.Columns.CompanyCodeTypeId,codeTypeID);
            BCS.Biz.CompanyNumberCodeCollection wantedCodes = dm.GetCompanyNumberCodeCollection();

            foreach (Biz.CompanyNumberCode code in wantedCodes)
                codes.Add(code.Id.ToString());

            return codes;
        }
        */
        private static Biz.CompanyNumberCode AddCompanyCode(int codeTypeID, string strCode)
        {
            Biz.CompanyNumberCode code;
            Biz.DataManager dmCodeAdd = Common.GetDataManager();
            dmCodeAdd.QueryCriteria.Clear();
            dmCodeAdd.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.Id, codeTypeID);
            Biz.CompanyNumberCodeType codeType = dmCodeAdd.GetCompanyNumberCodeType();

            code = codeType.NewCompanyNumberCode();
            code.Code = strCode;
            code.ExpirationDate = DateTime.Parse("1/1/2000");
            code.CompanyNumberId = Int32.Parse(Properties.Settings.Default.CompanyNumberId);

            dmCodeAdd.CommitAll();

            return code;
        }

        private static int GetCompanyNumberCode(int submissionID, int codeTypeID)
        {
            int codeID = 0;
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(Properties.Settings.Default.TargetDSN))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("usp_GetSumissionCompanyNumberCode", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("@SubmissionID", System.Data.SqlDbType.Int).Value = submissionID;
                    cmd.Parameters.Add("@CompanyCodeTypeID", System.Data.SqlDbType.Int).Value = codeTypeID;
                    cmd.Connection.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            Int32.TryParse(dr["CompanyNumberCodeID"].ToString(), out codeID);
                        }
                    }
                }
            }
            return codeID;
        }
    }
}
