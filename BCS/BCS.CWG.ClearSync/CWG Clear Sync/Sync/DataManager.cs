using System;
using System.Collections.Generic;
using System.Text;
using BCS.CWG.ClearSync.Sync.Objects;
using System.Data;
using System.Collections.Specialized;
using System.Data.SqlTypes;

namespace BCS.CWG.ClearSync.Sync
{
    public class DataManager
    {
        private static DataTable FilterNew(DataTable dataTable, DateTime lastRunDate)
        {
            string expr = string.Format(" Date_Run > '{0}'", lastRunDate);
            DataRow[] selectedRows = dataTable.Select(expr);
            DataTable filterDataTable = dataTable.Clone();
            foreach (DataRow drow in selectedRows)
                filterDataTable.Rows.Add(drow.ItemArray);
            filterDataTable.AcceptChanges();
            return filterDataTable;
        }
        internal static SyncEntries GetEntriesToSync(DateTime lastRunDate)
        {
            List<SyncEntry> syncEntryList = new List<SyncEntry>();

            Database.DBAccess.ConnectionString = Properties.Settings.Default.SourceDSN;
            DataTable dataTable = FilterNew(Database.DBAccess.GetDataTable(TableNames.SyncTable), lastRunDate);
            foreach (DataRow syncEntryDataRow in dataTable.Rows)
            {
                //don't import Renewals. 
                if (GetSafeString(syncEntryDataRow, "Policy_Tran_Type") == "RN")
                {
                    continue;
                }

                SyncEntry syncEntry = new SyncEntry();

                syncEntry.Branch = GetSafeInt(syncEntryDataRow, "Branch", 0);

                syncEntry.BranchSpecified = syncEntry.Branch != 0;
                syncEntry.Bus_Cat = GetSafeString(syncEntryDataRow, "Bus_Cat");
                syncEntry.Cvg_Eff_Dt = GetSafeDateTime(syncEntryDataRow, "Cvg_Eff_Dt", SqlDateTime.MinValue.Value);
                syncEntry.Cvg_Eff_DtSpecified = syncEntry.Cvg_Eff_Dt != SqlDateTime.MinValue.Value;
                syncEntry.Date_Run = GetSafeDateTime(syncEntryDataRow, "Date_Run", SqlDateTime.MinValue.Value);
                syncEntry.Date_RunSpecified = syncEntry.Date_Run != SqlDateTime.MinValue.Value;
                syncEntry.FEIN = GetSafeString(syncEntryDataRow, "FEIN");

                syncEntry.Policy_Mod = GetSafeString(syncEntryDataRow, "Policy_Mod");
                syncEntry.Policy_Num = GetSafeString(syncEntryDataRow, "Policy_Num");
                syncEntry.Policy_Tran_Type = GetSafeString(syncEntryDataRow, "Policy_Tran_Type");
                syncEntry.Policy_Status = GetSafeString(syncEntryDataRow, "Policy_Status");
                syncEntry.Proc_ID = GetSafeString(syncEntryDataRow, "Proc_ID");
                syncEntry.Prog_Code = GetSafeString(syncEntryDataRow, "Prog_Code");
                syncEntry.SIC = GetSafeDecimal(syncEntryDataRow, "SIC", decimal.Zero);
                syncEntry.SICSpecified = syncEntry.SIC != decimal.Zero;
                syncEntry.NAICS = GetSafeDecimal(syncEntryDataRow, "NAICS", decimal.Zero);
                syncEntry.NAICSSpecified = syncEntry.NAICS != decimal.Zero;
                syncEntry.Policy_Eff_Dt = GetSafeDateTime(syncEntryDataRow, "Policy_Eff_Dt", SqlDateTime.MinValue.Value);
                syncEntry.Policy_Eff_DtSpecified = syncEntry.Policy_Eff_Dt != SqlDateTime.MinValue.Value;
                syncEntry.Sub_Status = GetSafeString(syncEntryDataRow, "Sub_Status");
                syncEntry.Tran_Code = GetSafeString(syncEntryDataRow, "Tran_Code");
                syncEntry.Tran_Desc = GetSafeString(syncEntryDataRow, "Tran_Desc");

                syncEntry.Prior_Carrier = GetSafeString(syncEntryDataRow, "PriorCarrier");

                syncEntry.Tran_Proc_Dt = GetSafeDateTime(syncEntryDataRow, "Tran_Proc_Dt", SqlDateTime.MinValue.Value);
                syncEntry.Tran_Proc_DtSpecified = syncEntry.Tran_Proc_Dt != SqlDateTime.MinValue.Value;
                syncEntry.TW_Prem = GetSafeDecimal(syncEntryDataRow, "TW_Prem", decimal.Zero);
                syncEntry.TW_PremSpecified = syncEntry.TW_Prem != decimal.Zero;
                syncEntry.Undw_ID = GetSafeString(syncEntryDataRow, "Undw_ID");

                syncEntryList.Add(syncEntry);
            }

            SyncEntries syncEntries = new SyncEntries();
            syncEntries.Items = syncEntryList.ToArray();
            return syncEntries;
        }

        private static decimal GetSafeDecimal(DataRow dataRow, string columnName, decimal defaultValue)
        {
            try
            {
                return Convert.ToDecimal(dataRow[columnName]);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        private static int GetSafeInt(DataRow dataRow, string columnName, int defaultValue)
        {
            try
            {
                return Convert.ToInt32(dataRow[columnName]);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        private static DateTime GetSafeDateTime(DataRow dataRow, string columnName, DateTime defaultValue)
        {
            try
            {
                return Convert.ToDateTime(dataRow[columnName]);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        private static string GetSafeString(DataRow dataRow, string columnName)
        {
            try
            {
                return Convert.ToString(dataRow[columnName]);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private static bool GetSafeBool(DataRow dataRow, string columnName, bool defaultValue)
        {
            try
            {
                return Convert.ToBoolean(dataRow[columnName]);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }
    }
}
