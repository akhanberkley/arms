using System;
using System.Collections.Generic;
using System.Text;

namespace BCS.CWG.ClearSync.Sync
{
    class TableNames
    {
        // if deploying for BNP remove _pdr for the table name 
        public static readonly string SyncTable = "Cl_Sys_Data_Pull_Aplus_PDR";
    }
}
