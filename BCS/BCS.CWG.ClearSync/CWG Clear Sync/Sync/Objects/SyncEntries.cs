using System;
using System.Collections.Generic;
using System.Text;
using BCS.CWG.ClearSync.Sync.GeneratedObjects;

namespace BCS.CWG.ClearSync.Sync.Objects
{
    public class SyncEntries
    {
        private SyncEntry[] itemsField;

        public SyncEntry[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }
    public class SyncEntry : SyncEntryGenerated
    {
      
    }
}
