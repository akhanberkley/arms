using System;
using System.Collections.Generic;
using System.Text;
using BTS.LogFramework;

namespace BCS.CWG.ClearSync
{
    public enum RunStatus
    {
        Success = 0,
        Fail = -1
    }
    public class Program
    {
        static DateTime startTime;
        static void Main(string[] args)
        {
            RegisterUnhandledException();
            LogSeparator();
            startTime = LogCurrentTime("Process Started at");

            #region Previous Sync Status
            BCS.Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.ClearSync.Columns.CompanyNumberId, Properties.Settings.Default.CompanyNumberId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.ClearSync.Columns.Name, Properties.Settings.Default.Identifier);
            BCS.Biz.ClearSync syncStatus = dm.GetClearSync();
            if (syncStatus == null)
            {
                throw new ApplicationException(string.Format("Sync not initialized for company number id {0}, Name {1}.",
                    Properties.Settings.Default.CompanyNumberId,
                    Properties.Settings.Default.Identifier));
            }
            if (syncStatus.LastRunDate.IsNull)
            {
                throw new ApplicationException(string.Format("Last run date cannot be null for company number id {0}, Name {1}.",
                    Properties.Settings.Default.CompanyNumberId,
                    Properties.Settings.Default.Identifier));
            }
            #endregion

            // call sync
            DateTime runDate = Synchronizer.Synchronize(syncStatus.LastRunDate.Value);
            
            // reached here without problems, success
            UpdateStatus(RunStatus.Success, runDate);

            DateTime endTime = LogCurrentTime("Process Ended at");
            LogTimeTaken(startTime, endTime);
            Common.SendEmail("Status: Success", string.Format("Total Entries : {0}, Not Found in BCS : {1}",
                Properties.Settings.Default.TotalCount, Properties.Settings.Default.NotFoundCount));
        }

        private static void UpdateStatus(RunStatus runStatus, DateTime runDate)
        {
            BCS.Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.ClearSync.Columns.CompanyNumberId, Properties.Settings.Default.CompanyNumberId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.ClearSync.Columns.Name, Properties.Settings.Default.Identifier);
            BCS.Biz.ClearSync syncStatus = dm.GetClearSync();
            if (runDate > DateTime.MinValue)
                syncStatus.LastRunDate = runDate;
            syncStatus.LastRunStatus = (int)runStatus;
            dm.CommitAll();
        }

        private static void LogSeparator()
        {
            try
            {
                string separator = "****************************************";
                LogCentral.Current.LogInfo(separator);
                LogCentral.Current.LogWarn(separator);
                LogCentral.Current.LogError(separator);
                LogCentral.Current.LogFatal(separator);
            }
            catch (Exception ex)
            {
            }
        }

        internal static void LogTimeTaken(DateTime startTime, DateTime endTime)
        {
            TimeSpan ts = endTime.Subtract(startTime);
            string ps = string.Format("Process Time Taken : {0} minutes OR {1} seconds", Math.Round(ts.TotalMinutes, 2), Math.Round(ts.TotalSeconds, 2));
            LogCentral.Current.LogInfo(ps);
            Console.WriteLine(ps);
        }

        internal static void LogTimeTaken(string p, DateTime startTime, DateTime endTime)
        {
            TimeSpan ts = endTime.Subtract(startTime);
            string ps = string.Format("{2} Time Taken : {0} minutes OR {1} seconds", Math.Round(ts.TotalMinutes, 2), Math.Round(ts.TotalSeconds, 2), p);
            LogCentral.Current.LogInfo(ps);
            Console.WriteLine(ps);
        }

        internal static DateTime LogCurrentTime(string p)
        {
            DateTime DateTimeNow = DateTime.Now;
            string ps = string.Format("{0} : {1}", p, DateTimeNow.ToString());
            LogCentral.Current.LogInfo(ps);
            Console.WriteLine(ps);
            return DateTimeNow;
        }

        #region Unhandled exception handler
        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand, Flags = System.Security.Permissions.SecurityPermissionFlag.ControlAppDomain)]
        public static void RegisterUnhandledException()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);
        }

        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Console.WriteLine("Exception from {0}", System.Threading.Thread.CurrentThread.Name);
            Exception exceptionInfo = (Exception)args.ExceptionObject;
            LogCentral.Current.LogFatal(exceptionInfo.ToString(), exceptionInfo);
            // log end time and time taken in case of exception
            DateTime endTime = LogCurrentTime("Process Ended at");
            LogTimeTaken(startTime, endTime);

            // update failed status
            UpdateStatus(RunStatus.Fail, DateTime.MinValue);

            Common.SendEmail(exceptionInfo);
        }

        #endregion
    }
}
