﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS;
using BTS.WFA.BCS.Data;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.CompanySync.ConfigSections;
using BTS.WFA.BCS.CompanySync.Models;

namespace BTS.WFA.BCS.CompanySync
{
    class Program
    {
        static void Main(string[] args)
        {
            var emailFromAddress = ConfigurationManager.AppSettings["EmailSender"];
            List<ProcessElement> processes = new List<ProcessElement>();
            bool setupSucceeded = true;

            try
            {
                var appHandlers = Application.CreateHandlers(ConfigurationManager.AppSettings["ApplicationHandlers"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                Application.Initialize<SqlDb>(appHandlers.ToArray());

                var configProcessTypes = ConfigurationManager.GetSection("ProcessSection") as ProcessConfigurationSection;

                if (args == null || args.Length == 0)
                {
                    Console.WriteLine("Automated Usage: Enter process ids separated by spaces on the command line");
                    Console.WriteLine();
                    Console.Write("Please enter the process id you'd like to run manually: ");

                    args = new[] { Console.ReadLine() };
                }

                foreach (var processId in args)
                {
                    var process = configProcessTypes.Processes[processId];
                    if (process != null)
                        processes.Add(process);
                    else
                        throw new ArgumentException(String.Format("The Process '{0}' is not defined", processId));
                }
            }
            catch (Exception ex)
            {
                Application.HandleException(ex, null);
            }

            if (setupSucceeded)
            {
                var processUserName = "Company_Sync";
                foreach (var process in processes)
                {
                    Console.WriteLine("Starting Process {0}", process.Name);
                    try
                    {
                        switch (process.Id)
                        {
                            case "CWGSubmissionSync":
                            case "BNPSubmissionSync":
                                var companyAbbreviation = process.Parameters["CompanyAbbreviation"].Value;
                                var CWGToAddress = process.Parameters["CompletionEmailAddress"].Value;

                                var cwgResults = Syncronization.CWGPolicy.Synchronize(Console.Out, Application.CompanyMap[companyAbbreviation], process.Parameters["SourceConnectionString"].Value, processUserName);

                                SendCompletionEmail(process, cwgResults, CWGToAddress, emailFromAddress);
                                break;

                            case "AICSubmissionSync":
                                var AICcompanyNumber = process.Parameters["CompanyNumber"].Value;
                                int AICcnId = int.Parse(process.Parameters["CompanyNumberId"].Value);
                                var AICToAddress = process.Parameters["CompletionEmailAddress"].Value;

                                var aicResults = Syncronization.PolicyStar.Synchronize(Console.Out, AICcompanyNumber, AICcnId, process.Parameters["PolicyStarWebServiceUrl"].Value, processUserName);

                                SendCompletionEmail(process, aicResults, AICToAddress, emailFromAddress);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Process Failed.");
                        Application.HandleException(ex, null);
                    }

                    Console.WriteLine();
                }
            }
        }

        private static void SendCompletionEmail(ProcessElement process, CompanySynchronizationResult results, string toAddress, string fromAddress)
        {
            if (!String.IsNullOrEmpty(toAddress))
            {
                if (!results.Succeeded)
                {
                    Console.WriteLine("Failed: " + results.FailureMessage);
                    Application.HandleException(new Exception(results.FailureMessage), process.Name);
                    (new SmtpClient()).Send(new MailMessage(fromAddress, toAddress, process.Name + " Failed", results.FailureMessage));
                }
                else if (results.RecordsUpdated == 0 || results.Notes.Count > 0)
                {
                    var issues = String.Join(Environment.NewLine, results.Notes);
                    StringBuilder message = new StringBuilder();
                    message.AppendLine(process.Name + " Completed with " + results.RecordsUpdated + " records updated");
                    message.AppendLine();
                    message.AppendLine("Issues:");
                    message.AppendLine(String.IsNullOrEmpty(issues) ? "None" : issues);

                    string subject = String.Format("{2} Succeeded - {0} Records updated, {1} Potential issues", results.RecordsUpdated, results.Notes.Count, process.Name);
                    (new SmtpClient()).Send(new MailMessage(fromAddress, toAddress, subject, message.ToString()));
                }
            }
        }
    }
}
