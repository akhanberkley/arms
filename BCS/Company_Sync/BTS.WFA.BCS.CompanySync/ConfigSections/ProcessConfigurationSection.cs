﻿using System;
using System.Collections;
using System.Text;
using System.Configuration;
using System.Xml;
using System.Collections.Generic;

namespace BTS.WFA.BCS.CompanySync.ConfigSections
{
    public class ProcessConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("processes")]
        [ConfigurationCollection(typeof(ProcessesCollection), AddItemName = "process")]
        public ProcessesCollection Processes { get { return base["processes"] as ProcessesCollection; } }
    }

    public class ProcessesCollection : ConfigurationElementCollection
    {
        public new ProcessElement this[string id] { get { return BaseGet(id) as ProcessElement; } }
        public void Add(ProcessElement serviceConfig) { BaseAdd(serviceConfig); }
        protected override ConfigurationElement CreateNewElement() { return new ProcessElement(); }
        protected override object GetElementKey(ConfigurationElement element) { return ((ProcessElement)element).Id; }
    }

    public class ProcessElement : ConfigurationElement
    {
        [ConfigurationProperty("id", IsRequired = true)]
        public string Id
        {
            get { return (string)this["id"]; }
            set { this["id"] = value; }
        }

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("parameters")]
        [ConfigurationCollection(typeof(ParametersCollection), AddItemName = "parameter")]
        public ParametersCollection Parameters { get { return base["parameters"] as ParametersCollection; } }
    }

    public class ParametersCollection : ConfigurationElementCollection
    {
        public new ParameterElement this[string name] { get { return BaseGet(name) as ParameterElement; } }
        public void Add(ParameterElement serviceConfig) { BaseAdd(serviceConfig); }
        protected override ConfigurationElement CreateNewElement() { return new ParameterElement(); }
        protected override object GetElementKey(ConfigurationElement element) { return ((ParameterElement)element).Name; }
    }

    public class ParameterElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value
        {
            get { return (string)this["value"]; }
            set { this["value"] = value; }
        }
    }
}