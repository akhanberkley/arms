﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.BCS.CompanySync.Models
{
    public class CompanySynchronizationResult
    {
        public CompanySynchronizationResult()
        {
            Notes = new List<string>();
        }

        public string FailureMessage { get; set; }
        public bool Succeeded { get { return String.IsNullOrEmpty(FailureMessage); } }

        public List<string> Notes { get; set; }
        public int RecordsUpdated { get; set; }
    }
}
