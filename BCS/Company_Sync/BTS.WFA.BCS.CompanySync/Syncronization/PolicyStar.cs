﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.Entity;
using BTS.WFA.BCS.CompanySync.Models;

namespace BTS.WFA.BCS.CompanySync.Syncronization
{
    public class PolicyStar
    {
        public static CompanySynchronizationResult Synchronize(TextWriter outputStream, string companyNumber, int cnId, string policyStarUrl, string updateUser)
        {
            var results = new CompanySynchronizationResult();
            Action<string> log = (s => { if (outputStream != null) outputStream.WriteLine(s); });

            using (var db = Application.GetDatabaseInstance())
            {
                Integration.Models.PolicyStarPolicyResults searchResults = null;

                log("Getting Last Run Date");
                var dbSyncLog = db.ClearSync.FirstOrDefault(cs => cs.CompanyNumberId == cnId && cs.Name.Contains("DSS Sync"));
                if (dbSyncLog == null)
                    results.FailureMessage = "Could not find 'DSS Sync' row for company number " + cnId;
                else if (dbSyncLog.LastRunDate == null)
                    results.FailureMessage = "Last Run Date can not be null";
                else
                {
                    DateTime lastRun = dbSyncLog.LastRunDate.Value;
                    log("Getting Policies - " + lastRun.ToString());

                    searchResults = Integration.Services.PolicyStarBatchService.GetPolicies(policyStarUrl, companyNumber, lastRun);
                    if (!searchResults.Succeeded)
                        results.FailureMessage = searchResults.Exception.Message;
                }

                if (results.Succeeded)
                {
                    var statuses = db.CompanyNumber.Where(cn => cn.Id == cnId).SelectMany(cn => cn.CompanyNumberSubmissionStatus.Select(s => s.SubmissionStatus)).ToArray();
                    var dbCancelledStatus = statuses.First(s => s.StatusCode == "Cancelled");
                    var dbIssuedStatus = statuses.First(s => s.StatusCode == "Issued");

                    log(searchResults.Policies.Count + " Policies to process");
                    int submissionIndex = 0;
                    foreach (var p in searchResults.Policies)
                    {
                        log("Processing #" + (++submissionIndex).ToString().PadRight(3) + " Policy #" + p.PolicyNumber);
                        var matchedSubmissions = db.Submission.Include(s => s.SubmissionCompanyNumberAttributeValue)
                                                        .Include(s => s.CompanyNumberCode)
                                                        .Where(s => s.CompanyNumberId == cnId && s.PolicyNumber == p.PolicyNumber && s.EffectiveDate == p.EffectiveDate)
                                                        .ToList();
                        if (!matchedSubmissions.Any())
                        {
                            var message = "Could not find a submission for Policy #" + p.PolicyNumber;
                            log(message);
                            results.Notes.Add(message);
                        }
                        else if (matchedSubmissions.Count > 1)
                        {
                            var message = "Found multiple submissions for Policy #" + p.PolicyNumber;
                            log(message);
                            results.Notes.Add(message);
                        }
                        else
                        {
                            var dbSubmission = matchedSubmissions.First();
                            dbSubmission.ExpirationDate = p.ExpirationDate;
                            dbSubmission.EstimatedWrittenPremium = p.Premium;

                            if (p.CancelPendingAudit || (p.CancelDate.HasValue && p.Status == "CANCEL"))
                            {
                                dbSubmission.SubmissionStatus = dbCancelledStatus;
                                dbSubmission.SubmissionStatusDate = p.LastModifiedDate;
                                dbSubmission.CancellationDate = (p.CancelPendingAudit) ? p.StatusEffectiveDate : p.CancelDate;

                                var history = db.SubmissionStatusHistory.Add(new Data.Models.SubmissionStatusHistory());
                                history.Submission = dbSubmission;
                                history.PreviousSubmissionStatusId = dbCancelledStatus.Id;
                                history.SubmissionStatusDate = DateTime.Now;
                                history.StatusChangeBy = updateUser;
                            }
                            else if (dbSubmission.SubmissionStatus.Id == dbCancelledStatus.Id && (!p.CancelPendingAudit || p.Status == "ACTIVE" || p.Status == "REINSTATED"))
                            {
                                dbSubmission.SubmissionStatus = dbIssuedStatus;
                                dbSubmission.SubmissionStatusDate = p.LastModifiedDate;
                                dbSubmission.CancellationDate = null;

                                var history = db.SubmissionStatusHistory.Add(new Data.Models.SubmissionStatusHistory());
                                history.Submission = dbSubmission;
                                history.PreviousSubmissionStatusId = dbIssuedStatus.Id;
                                history.SubmissionStatusDate = DateTime.Now;
                                history.StatusChangeBy = updateUser;
                            }

                            dbSubmission.UpdatedDt = DateTime.Now;
                            dbSubmission.UpdatedBy = updateUser;
                            db.SaveChanges();
                            results.RecordsUpdated++;
                        }
                    }

                    dbSyncLog.LastRunDate = DateTime.Now;
                    dbSyncLog.LastRunStatus = 0;
                    db.SaveChanges();
                }
            }

            return results;
        }
    }
}
