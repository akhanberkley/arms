﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.Entity;
using BTS.WFA.BCS.CompanySync.Models;

namespace BTS.WFA.BCS.CompanySync.Syncronization
{
    public class CWGPolicy
    {
        public static CompanySynchronizationResult Synchronize(TextWriter outputStream, CompanyDetail cd, string sourceConnectionString, string updateUser)
        {
            var results = new CompanySynchronizationResult();
            Action<string> log = (s => { if (outputStream != null) outputStream.WriteLine(s); });

            using (var db = Application.GetDatabaseInstance())
            {
                Integration.Models.CWGPolicyResults searchResults = null;

                log("Getting Last Run Date");
                var dbSyncLog = db.ClearSync.FirstOrDefault(cs => cs.CompanyNumberId == cd.CompanyNumberId && cs.Name.Contains("Clear Sync"));
                if (dbSyncLog == null)
                    results.FailureMessage = "Could not find 'Clear Sync' row for company number id " + cd.CompanyNumberId;
                else if (dbSyncLog.LastRunDate == null)
                    results.FailureMessage = "Last Run Date can not be null";
                else
                {
                    DateTime lastRun = dbSyncLog.LastRunDate.Value;
                    log("Getting Submissions - " + lastRun.ToString());

                    var company = (Integration.Models.CWGCompany)Enum.Parse(typeof(Integration.Models.CWGCompany), cd.Summary.Abbreviation);
                    searchResults = Integration.Services.CWGService.GetSubmissionsToSync(company, sourceConnectionString, lastRun);
                    if (!searchResults.Succeeded)
                        results.FailureMessage = searchResults.Exception.Message;
                }

                if (results.Succeeded)
                {
                    var companyAttributes = db.CompanyNumberAttribute.Where(a => a.CompanyNumberId == cd.CompanyNumberId).ToArray();
                    var premiumAttribute = companyAttributes.FirstOrDefault(a => a.Label == "Policy Premium");
                    var sicCodeAttribute = companyAttributes.FirstOrDefault(a => a.Label == "SIC Code");
                    var naicsCodeAttribute = companyAttributes.FirstOrDefault(a => a.Label == "NAICS Code");
                    var busCatCodeAttribute = companyAttributes.FirstOrDefault(a => a.Label == "BUS Cat Code");

                    var priorCarrierCodeType = db.CompanyNumberCodeType.Include(t => t.CompanyNumberCode).FirstOrDefault(t => t.CompanyNumberId == cd.CompanyNumberId && t.CodeName == "Prior Carrier");
                    var policyProgramCodeType = db.CompanyNumberCodeType.Include(t => t.CompanyNumberCode).FirstOrDefault(t => t.CompanyNumberId == cd.CompanyNumberId && t.CodeName == "Policy Program Code");

                    log(searchResults.Policies.Count + " Submissions to process");
                    int submissionIndex = 0;
                    foreach (var p in searchResults.Policies)
                    {
                        log("Processing #" + (++submissionIndex).ToString().PadRight(3) + " Policy #" + p.PolicyNumber);
                        var matchedSubmissions = db.Submission.Include(s => s.SubmissionCompanyNumberAttributeValue)
                                                        .Include(s => s.CompanyNumberCode)
                                                        .Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.PolicyNumber == p.PolicyNumber && !s.Deleted)
                                                        .ToList();
                        if (!matchedSubmissions.Any())
                        {
                            var message = "Could not find a submission for Policy #" + p.PolicyNumber;
                            log(message);
                            results.Notes.Add(message);
                        }
                        else if (matchedSubmissions.Count > 1)
                        {
                            var message = "Found multiple submissions for Policy #" + p.PolicyNumber;
                            log(message);
                            results.Notes.Add(message);
                        }
                        else
                        {
                            var dbSubmission = matchedSubmissions.First();
                            var attrsToUpdate = new List<Tuple<Data.Models.CompanyNumberAttribute, string>>();
                            if (premiumAttribute != null && p.Premium != 0)
                                attrsToUpdate.Add(new Tuple<Data.Models.CompanyNumberAttribute, string>(premiumAttribute, p.Premium.ToString()));
                            if (sicCodeAttribute != null && p.SICCode != "0")
                                attrsToUpdate.Add(new Tuple<Data.Models.CompanyNumberAttribute, string>(sicCodeAttribute, p.SICCode));
                            if (naicsCodeAttribute != null && p.NAICSCode != "0" && !String.IsNullOrEmpty(p.NAICSCode))
                                attrsToUpdate.Add(new Tuple<Data.Models.CompanyNumberAttribute, string>(naicsCodeAttribute, p.NAICSCode));
                            if (busCatCodeAttribute != null && !String.IsNullOrEmpty(p.BusCategoryCode))
                                attrsToUpdate.Add(new Tuple<Data.Models.CompanyNumberAttribute, string>(busCatCodeAttribute, p.BusCategoryCode));

                            foreach (var update in attrsToUpdate)
                            {
                                var dbAttr = dbSubmission.SubmissionCompanyNumberAttributeValue.FirstOrDefault(a => a.CompanyNumberAttributeId == update.Item1.Id);
                                if (dbAttr == null)
                                {
                                    dbAttr = new Data.Models.SubmissionCompanyNumberAttributeValue() { CompanyNumberAttribute = update.Item1, Submission = dbSubmission };
                                    dbSubmission.SubmissionCompanyNumberAttributeValue.Add(dbAttr);
                                }
                                dbAttr.AttributeValue = update.Item2;
                            }

                            var codesToUpdate = new List<Tuple<Data.Models.CompanyNumberCodeType, string>>();
                            if (priorCarrierCodeType != null && !String.IsNullOrEmpty(p.PriorCarrier))
                                codesToUpdate.Add(new Tuple<Data.Models.CompanyNumberCodeType, string>(priorCarrierCodeType, p.PriorCarrier));
                            if (policyProgramCodeType != null && !String.IsNullOrEmpty(p.ProgramCode))
                                codesToUpdate.Add(new Tuple<Data.Models.CompanyNumberCodeType, string>(policyProgramCodeType, p.ProgramCode));

                            foreach (var update in codesToUpdate)
                            {
                                var newSelection = update.Item1.CompanyNumberCode.FirstOrDefault(c => String.Equals(c.Code, update.Item2, StringComparison.CurrentCultureIgnoreCase));
                                if (newSelection == null)
                                {
                                    newSelection = new Data.Models.CompanyNumberCode() { CompanyNumberId = cd.CompanyNumberId, CompanyNumberCodeType = update.Item1, Code = update.Item2, ExpirationDate = new DateTime(2000, 1, 1) };
                                    update.Item1.CompanyNumberCode.Add(newSelection);
                                }

                                var existingSelection = dbSubmission.CompanyNumberCode.FirstOrDefault(c => c.CompanyCodeTypeId == update.Item1.Id);
                                if (existingSelection != null && existingSelection.Id != newSelection.Id)
                                    dbSubmission.CompanyNumberCode.Remove(existingSelection);
                                if (existingSelection == null || existingSelection.Id != newSelection.Id)
                                    dbSubmission.CompanyNumberCode.Add(newSelection);
                            }

                            dbSubmission.UpdatedDt = DateTime.Now;
                            dbSubmission.UpdatedBy = updateUser;

                            db.SaveChanges();
                            results.RecordsUpdated++;
                        }
                    }
                    if (searchResults.Policies.Any())
                    {
                        dbSyncLog.LastRunDate = searchResults.Policies.Max(s => s.RunDate);
                        dbSyncLog.LastRunStatus = 0;
                        db.SaveChanges();
                    }
                }
            }

            return results;
        }
    }
}
