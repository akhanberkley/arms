﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using System.Linq;
using System.Configuration;

namespace BTS.WFA.BCS.MessageService.Tests
{
    [TestClass]
    public class ApsTester
    {
#if DEBUG
        [TestMethod]
#endif
        public void RunService()
        {
            //The MQ expects the logged in user to be the same as the connection user (bcsapp), so in order to run this unit test, you need to do two things:
            // 1.  Create a local user called 'bcsapp' and run visual studio as that user
            // 2.  Make sure you have the 'dotNETClientsetup.exe' IBM XMS client library installed

            BCS.Application.Initialize<BCS.Data.SqlDb>(Application.GetStandardDebugHandlers());

            var service = new MessageService.Messaging.Aps();
            service.Start(ConfigurationManager.AppSettings["apsQueueUser"], ConfigurationManager.AppSettings["apsQueuePassword"], int.Parse(ConfigurationManager.AppSettings["apsUpdatePollingInterval"]));

            while (true)
                System.Threading.Thread.Sleep(5000);
        }

#if DEBUG
        [TestMethod]
#endif
        public void ProcessIndividualLogItem()
        {
            BCS.Application.Initialize<BCS.Data.SqlDb>(Application.GetStandardDebugHandlers());

            var log = BCS.Services.AgencyService.GetApsLogItem(2711756);
            var cn = BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber);

            var update = MessageService.Messaging.Aps.ParseUpdateInfo(cn, log.Xml);
            MessageService.Messaging.Aps.ProcessUpdate(cn, update);
        }

        [TestMethod]
        public void VerifyAllXmlTypesParse()
        {
            BCS.Application.Initialize<BCS.Data.SqlDb>(Application.GetStandardDebugHandlers());

            using (var db = BCS.Application.GetDatabaseInstance())
            {
                var log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.Agency\">"));
                var update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(!String.IsNullOrEmpty(update.AgencyCode));

                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.AgencyName\">"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(!String.IsNullOrEmpty(update.AgencyCode));

                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.AgencyPersonnel\">"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(!String.IsNullOrEmpty(update.AgencyCode));

                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.AgencyLicense\">"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(!String.IsNullOrEmpty(update.AgencyCode));

                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.AgencyNote\">"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(!String.IsNullOrEmpty(update.AgencyCode));

                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.Location\">"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(!String.IsNullOrEmpty(update.AgencyCode));

                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.LocationAddress\">"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(!String.IsNullOrEmpty(update.AgencyCode));

                //comm update for non-agent
                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.PersonnelCommunication\">") && !l.Xml.Contains("com.bts.aps.domain.AgencyPersonnelInfo"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(String.IsNullOrEmpty(update.AgencyCode));

                //comm update for agent
                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.PersonnelCommunication\">") && l.Xml.Contains("com.bts.aps.domain.AgencyPersonnelInfo"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(!String.IsNullOrEmpty(update.AgencyCode));

                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.AgencyXRef\">"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(!String.IsNullOrEmpty(update.AgencyCode));

                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.Personnel\">"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);

                log = db.ApsxmlLog.First(l => l.Xml.Contains("<rootDomainObject class=\"com.bts.aps.domain.CompanyPersonnel\">"));
                update = MessageService.Messaging.Aps.ParseUpdateInfo(BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == log.CompanyNumber), log.Xml);
                Assert.IsTrue(update.PersonnelUpdate != null);
            }

        }
    }
}
