﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.BCS.MessageService.Models
{
    public class ApsUpdate
    {
        public int? LogId { get; set; }
        public string AgencyCode { get; set; }
        public Integration.Models.Personnel PersonnelUpdate { get; set; }
    }
}
