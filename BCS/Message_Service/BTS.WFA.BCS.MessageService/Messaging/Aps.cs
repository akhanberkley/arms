﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using BTS.WFA.BCS.MessageService.Models;
using IBM.XMS;

namespace BTS.WFA.BCS.MessageService.Messaging
{
    public class Aps
    {
        private bool stopUpdateThread = false;
        private List<IConnection> connections = new List<IConnection>();
        private ConcurrentQueue<Tuple<CompanyDetail, ApsUpdate>> updatesQueue = new ConcurrentQueue<Tuple<CompanyDetail, ApsUpdate>>();

        public void Start(string queueUserName, string queuePassword, int pollingInterval)
        {
            var companies = BCS.Application.CompanyMap.Where(kvp => !String.IsNullOrEmpty(kvp.Value.SystemSettings.ApsQueueName)).Select(kvp => kvp.Value);

            foreach (var company in companies)
            {
                try
                {
                    var connector = XMSFactoryFactory.GetInstance(XMSC.CT_WMQ).CreateConnectionFactory();
                    connector.SetStringProperty(XMSC.WMQ_HOST_NAME, company.SystemSettings.ApsQueueHost);
                    connector.SetStringProperty(XMSC.WMQ_CHANNEL, company.SystemSettings.ApsQueueChannel);
                    if (!String.IsNullOrEmpty(company.SystemSettings.ApsQueueManager))
                        connector.SetStringProperty(XMSC.WMQ_QUEUE_MANAGER, company.SystemSettings.ApsQueueManager);
                    connector.SetIntProperty(XMSC.WMQ_PORT, company.SystemSettings.ApsQueuePort.Value);
                    connector.SetIntProperty(XMSC.WMQ_CONNECTION_MODE, XMSC.WMQ_CM_CLIENT);

                    var connection = connector.CreateConnection(queueUserName, queuePassword);
                    connection.ExceptionListener = new ExceptionListener((ex) =>
                    {
                        Application.HandleException(ex, new { Company = company });
                    });

                    var session = connection.CreateSession(true, AcknowledgeMode.AutoAcknowledge);
                    var queue = session.CreateQueue(String.Format("queue://{0}", company.SystemSettings.ApsQueueName));
                    var consumer = session.CreateConsumer(queue);

                    consumer.MessageListener = new MessageListener((message) =>
                    {
                        string xml = null;
                        int? logId = null;
                        try
                        {
                            xml = (message as ITextMessage).Text;
                            logId = BCS.Services.AgencyService.SaveApsLogItem(company, xml);

                            var update = ParseUpdateInfo(company, xml);
                            update.LogId = logId;

                            updatesQueue.Enqueue(new Tuple<CompanyDetail, ApsUpdate>(company, update));
                            session.Commit();
                        }
                        catch (Exception ex)
                        {
                            Application.HandleException(ex, new { Source = "Message Listener", LogId = logId, Xml = xml });
                        }
                    });

                    connection.Start();
                    connections.Add(connection);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Failed to connect to MQ for " + company.Summary.Abbreviation, ex);
                }
            }

            Task.Factory.StartNew(() => ProcessUpdates(pollingInterval))
                        .ContinueWith(tsk => Application.HandleException(tsk.Exception, "APS Sync Processor Thread Failed"), TaskContinuationOptions.OnlyOnFaulted);
        }
        public void Stop()
        {
            stopUpdateThread = true;
            foreach (var c in connections)
            {
                c.Stop();
                c.Dispose();
            }
        }

        public static ApsUpdate ParseUpdateInfo(CompanyDetail cd, string xml)
        {
            var update = new ApsUpdate();
            XDocument doc = XDocument.Parse(xml);
            var rootDomainObject = doc.Root.Element("rootDomainObject");

            switch (rootDomainObject.Attribute("class").Value)
            {
                case "com.bts.aps.domain.Agency":
                    update.AgencyCode = rootDomainObject.Element("agencyCode").Value;
                    break;
                case "com.bts.aps.domain.AgencyName":
                case "com.bts.aps.domain.AgencyLicense":
                case "com.bts.aps.domain.AgencyNote":
                case "com.bts.aps.domain.Location":
                    var agencyCodeElement = rootDomainObject.XPathSelectElement("agency/agencyCode");
                    if (agencyCodeElement != null)
                        update.AgencyCode = agencyCodeElement.Value;
                    break;
                case "com.bts.aps.domain.LocationAddress":
                    var locationAgencyNode = rootDomainObject.XPathSelectElement("location/agency/agencyCode");
                    if (locationAgencyNode != null)
                        update.AgencyCode = locationAgencyNode.Value;
                    break;
                case "com.bts.aps.domain.PersonnelCommunication":
                    //We're only updating these for Agents
                    var personnelInfoNode = rootDomainObject.Descendants("com.bts.aps.domain.AgencyPersonnelInfo").FirstOrDefault();
                    if (personnelInfoNode != null)
                        update.AgencyCode = personnelInfoNode.Element("agencyCode").Value;
                    break;
                case "com.bts.aps.domain.AgencyXRef":
                    update.AgencyCode = rootDomainObject.XPathSelectElement("childAgency/agencyCode").Value;
                    break;
                case "com.bts.aps.domain.AgencyPersonnel":
                case "com.bts.aps.domain.Personnel":
                    var agencyNode = rootDomainObject.Descendants("agency").FirstOrDefault();
                    if (agencyNode != null)
                    {
                        var apsId = long.Parse(agencyNode.Element("id").Value);
                        var knownAgency = BCS.Services.AgencyService.Get(cd, apsId, null);
                        if (knownAgency != null)
                            update.AgencyCode = knownAgency.Code;
                    }
                    break;
                case "com.bts.aps.domain.CompanyPersonnel":
                    update.PersonnelUpdate = new Integration.Models.Personnel();
                    update.PersonnelUpdate.Id = long.Parse(rootDomainObject.Element("id").Value);
                    update.PersonnelUpdate.UserName = rootDomainObject.Element("userId").Value;

                    var firstNameNode = rootDomainObject.Element("firstName");
                    var lastNameNode = rootDomainObject.Element("lastName");
                    update.PersonnelUpdate.FullName = ((firstNameNode != null) ? firstNameNode.Value + " " : "") + ((lastNameNode != null) ? lastNameNode.Value : "");

                    foreach (var cpr in rootDomainObject.Descendants("com.bts.aps.domain.CompanyPersonnelRole"))
                    {
                        var codeNode = cpr.XPathSelectElement("companyPersonnelRoleType/code");
                        update.PersonnelUpdate.RoleCodes.Add(codeNode.Value);
                    }

                    var changeType = doc.XPathSelectElement("//details/com.bts.aps.domain.publish.APSChangeDetail/modificationType").Value;
                    if (changeType == "3")
                    {
                        DateTime retirementDate;
                        if (rootDomainObject.Element("retirementDate") != null && DateTime.TryParse(rootDomainObject.Element("retirementDate").Value, out retirementDate))
                            update.PersonnelUpdate.RetirementDate = retirementDate;
                        else
                            update.PersonnelUpdate.RetirementDate = DateTime.Now;
                    }
                    break;
            }

            return update;
        }

        private void ProcessUpdates(int pollingInterval)
        {
            while (!stopUpdateThread)
            {
                var distinctUpdates = new List<Tuple<CompanyDetail, ApsUpdate>>();

                Tuple<CompanyDetail, ApsUpdate> update = null;
                while (updatesQueue.TryDequeue(out update))
                {
                    //skip this update if we already have one for this agency number
                    if (!String.IsNullOrEmpty(update.Item2.AgencyCode) && distinctUpdates.Any(a => a.Item1 == update.Item1 && a.Item2.AgencyCode == update.Item2.AgencyCode))
                        continue;

                    distinctUpdates.Add(update);
                }

                foreach (var a in distinctUpdates)
                {
                    try
                    {
                        Messaging.Aps.ProcessUpdate(a.Item1, a.Item2);
                    }
                    catch (Exception ex)
                    {
                        Application.HandleException(ex, new { Message = "Failed to Perform an APS Update", Company = a.Item1, Update = a.Item2 });

                        try { BCS.Services.AgencyService.SaveApsLogItemAsFailure(a.Item2.LogId.Value); }
                        catch (Exception) { }
                    }
                }

                System.Threading.Thread.Sleep(pollingInterval);
            }
        }
        public static void ProcessUpdate(CompanyDetail cd, ApsUpdate update)
        {
            if (!String.IsNullOrEmpty(update.AgencyCode))
            {
                Application.LogItem(String.Format("Syncing Agency {0} For Company {1} - {2}", update.AgencyCode, cd.Summary.CompanyNumber, cd.Summary.CompanyName));
                var syncResult = BCS.Services.AgencyService.SyncAgency(cd, update.AgencyCode, false);
                if (!syncResult.Succeeded)
                    throw syncResult.Exception ?? new Exception(syncResult.Message);
                else if (syncResult.Exception != null)
                    Application.HandleException(syncResult.Exception, new { Message = "Agency Partially Updated", Update = update, Company = cd });
            }
            else if (update.PersonnelUpdate != null)
            {
                Application.LogItem(String.Format("Syncing Person {0} ({1}) For Company {2} - {3}", update.PersonnelUpdate.Id, update.PersonnelUpdate.FullName, cd.Summary.CompanyNumber, cd.Summary.CompanyName));
                BCS.Services.UnderwritingService.SyncPersonnelChanges(cd, update.PersonnelUpdate);
            }
        }
    }
}
