﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS.MessageService.Models;
using IBM.XMS;

namespace BTS.WFA.BCS.MessageService
{
    public partial class ApsQueueListener : ServiceBase
    {
        Messaging.Aps service = null;

        public ApsQueueListener()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                service = new Messaging.Aps();
                service.Start(ConfigurationManager.AppSettings["apsQueueUser"], ConfigurationManager.AppSettings["apsQueuePassword"], int.Parse(ConfigurationManager.AppSettings["apsUpdatePollingInterval"]));
            }
            catch (Exception ex)
            {
                Application.HandleException(ex, "APS Queue Listener");
                Stop();
            }
        }

        protected override void OnStop()
        {
            service.Stop();
        }
    }
}
