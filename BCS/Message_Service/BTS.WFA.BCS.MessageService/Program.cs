﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace BTS.WFA.BCS.MessageService
{
    static class Program
    {
        static void Main()
        {
            var appHandlers = BCS.Application.CreateHandlers(ConfigurationManager.AppSettings["ApplicationHandlers"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            BCS.Application.Initialize<BCS.Data.SqlDb>(appHandlers.ToArray());

            ServiceBase.Run(new ServiceBase[] 
            { 
                new ApsQueueListener() 
            });
        }
    }
}
