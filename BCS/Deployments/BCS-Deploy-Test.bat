@echo off
echo Shutting Down Services
sc \\btsdetstwfa02 stop BCS_Message_Service

echo Waiting for service to stop
timeout /t 20

echo Deleting Existing Directories

rd "\\btsdetstwfa02\d$\BCS\APS_Sync" /s /q
rd "\\btsdetstwfa02\d$\BCS\Company_Sync" /s /q
rd "\\btsdetstwfa02\d$\BCS\Message_Service" /s /q
rd "\\btsdetstwfa02\d$\BCS\Web_2014" /s /q

echo Copying Directories

xcopy "APS_Sync" "\\btsdetstwfa02\d$\BCS\APS_Sync" /E /I
xcopy "Company_Sync" "\\btsdetstwfa02\d$\BCS\Company_Sync" /E /I
xcopy "Message_Service" "\\btsdetstwfa02\d$\BCS\Message_Service" /E /I
xcopy "Web_2014" "\\btsdetstwfa02\d$\BCS\Web_2014" /E /I

echo Running Scripts

SQLScriptRunner "Database\BCS" "Data Source=btsdedevsql21;Initial Catalog=bcs_test;password=bc5@pp;user id=bcsapp"

echo Setting up Service
sc \\btsdetstwfa02 delete BCS_Message_Service
sc \\btsdetstwfa02 create BCS_Message_Service binPath= "D:\BCS\Message_Service\Message_Service.exe" obj= ".\bcsapp" password= "f@lcon16" start= auto
sc \\btsdetstwfa02 description BCS_Message_Service "BCS Service to handle real time processing"
sc \\btsdetstwfa02 start BCS_Message_Service

echo Deployment Complete
pause