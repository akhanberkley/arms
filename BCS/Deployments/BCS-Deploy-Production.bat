@echo off
set backuplocation=\\USILG01-ISP001\d$\BCS-Backups\%date:~-4,4%%date:~-10,2%%date:~7,2%
IF EXIST %backuplocation% (
	echo A Backup already exists, skipping %backuplocation%
) ELSE (
	echo Creating Backup at %backuplocation%
	mkdir %backuplocation%
	xcopy "\\USILG01-ISP001\d$\BCS" %backuplocation% /E /I /Q
)

echo Shutting Down Services
sc \\USILG01-ISP001 stop BCS_Message_Service

echo Waiting for service to stop
timeout /t 20

echo Deleting Existing Directories

rd "\\USILG01-ISP001\d$\BCS\Company_Sync" /s /q
rd "\\USILG01-ISP002\d$\BCS\Company_Sync" /s /q
rd "\\USILG01-ISP001\d$\BCS\Message_Service" /s /q
rd "\\USILG01-ISP002\d$\BCS\Message_Service" /s /q
rd "\\USILG01-ISP001\d$\BCS\Web_2014" /s /q
rd "\\USILG01-ISP002\d$\BCS\Web_2014" /s /q

echo Copying Directories

xcopy "Company_Sync" "\\USILG01-ISP001\d$\BCS\Company_Sync" /E /I
xcopy "Company_Sync" "\\USILG01-ISP002\d$\BCS\Company_Sync" /E /I
xcopy "Message_Service" "\\USILG01-ISP001\d$\BCS\Message_Service" /E /I
xcopy "Message_Service" "\\USILG01-ISP002\d$\BCS\Message_Service" /E /I
xcopy "Web_2014" "\\USILG01-ISP001\d$\BCS\Web_2014" /E /I
xcopy "Web_2014" "\\USILG01-ISP002\d$\BCS\Web_2014" /E /I

echo Running Scripts
SQLScriptRunner "Database\BCS" "Data Source=btsdesql77;Initial Catalog=bcs_prd;password=14bcs2use;user id=bcsapp"

echo Setting up Service
sc \\USILG01-ISP001 delete BCS_Message_Service
sc \\USILG01-ISP001 create BCS_Message_Service binPath= "D:\BCS\Message_Service\Message_Service.exe" obj= ".\bcsapp" password= "f@lcon16" start= auto
sc \\USILG01-ISP001 description BCS_Message_Service "BCS Service to handle real time processing"
sc \\USILG01-ISP001 start BCS_Message_Service

echo Deployment Complete
pause