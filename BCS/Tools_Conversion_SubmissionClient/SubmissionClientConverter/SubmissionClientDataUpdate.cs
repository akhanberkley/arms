﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SubmissionClientConverter
{
    class SubmissionClientDataUpdate
    {
        public SubmissionClientDataUpdate()
        {
            AddressClientIdCombos = new List<SubmissionAddressRow>();
            NameClientIdCombos = new List<SubmissionNameRow>();
        }

        public int CompanyNumberId { get; set; }
        public int SubmissionId { get; set; }
        public long ClientCoreClientId { get; set; }
        public DateTime ClientDate { get; set; }
        public List<SubmissionAddressRow> AddressClientIdCombos { get; set; }
        public List<SubmissionNameRow> NameClientIdCombos { get; set; }
    }

    class SubmissionAddressRow
    {
        public Guid AddressRowId { get; set; }
        public int? ClientCoreId { get; set; }
    }
    class SubmissionNameRow
    {
        public Guid NameRowId { get; set; }
        public int? SequenceNumber { get; set; }
    }
}
