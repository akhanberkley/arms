﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.BCS;
using BTS.WFA.BCS.Data;
using BTS.WFA.BCS.ViewModels;

namespace SubmissionClientConverter
{
    class Program
    {
        private static int SubmissionNumber = 0;
        private static Dictionary<string, int> countryIdMap;
        private static Dictionary<int, CompanyDetail> companyMap;

        static void Main(string[] args)
        {
            var appHandlers = Application.CreateHandlers(ConfigurationManager.AppSettings["ApplicationHandlers"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            Application.Initialize<SqlDb>(appHandlers.ToArray());

            using (var db = Application.GetDatabaseInstance())
            {
                countryIdMap = db.Country.ToDictionary(c => c.CountryCode, c => c.Id);
                companyMap = Application.CompanyMap.Values.ToDictionary((c) => c.CompanyNumberId);

                var triggerUserName = "Web2014Conversion";

                var addrs = db.SubmissionAddress.Where(sa => sa.EntryBy == triggerUserName && sa.EntryDt == sa.ModifiedDt)
                                                .Select(sa => new
                                                {
                                                    CompanyNumberId = sa.Submission.CompanyNumberId,
                                                    SubmissionId = sa.Submission.Id,
                                                    ClientCoreClientId = sa.Submission.ClientCoreClientId,
                                                    ClientDate = sa.Submission.UpdatedDt ?? sa.Submission.SubmissionDt,
                                                    Update = new SubmissionAddressRow { AddressRowId = sa.Id, ClientCoreId = sa.ClientCoreAddressId }
                                                }).ToArray();
                var names = db.SubmissionAdditionalName.Where(sn => sn.EntryBy == triggerUserName && sn.EntryDt == sn.ModifiedDt)
                                                       .Select(sn => new
                                                       {
                                                           CompanyNumberId = sn.Submission.CompanyNumberId,
                                                           SubmissionId = sn.Submission.Id,
                                                           ClientCoreClientId = sn.Submission.ClientCoreClientId,
                                                           ClientDate = sn.Submission.UpdatedDt ?? sn.Submission.SubmissionDt,
                                                           Update = new SubmissionNameRow { NameRowId = sn.Id, SequenceNumber = sn.ClientCoreSequenceNumber }
                                                       })
                                                       .ToArray();

                var submissionDictionary = new Dictionary<int, SubmissionClientDataUpdate>();
                foreach (var a in addrs.Where(a => companyMap.ContainsKey(a.CompanyNumberId)))
                {
                    if (!submissionDictionary.ContainsKey(a.SubmissionId))
                        submissionDictionary.Add(a.SubmissionId, new SubmissionClientDataUpdate { CompanyNumberId = a.CompanyNumberId, SubmissionId = a.SubmissionId, ClientCoreClientId = a.ClientCoreClientId, ClientDate = a.ClientDate });
                    submissionDictionary[a.SubmissionId].AddressClientIdCombos.Add(a.Update);
                }
                foreach (var n in names.Where(n => companyMap.ContainsKey(n.CompanyNumberId)))
                {
                    if (!submissionDictionary.ContainsKey(n.SubmissionId))
                        submissionDictionary.Add(n.SubmissionId, new SubmissionClientDataUpdate { CompanyNumberId = n.CompanyNumberId, SubmissionId = n.SubmissionId, ClientCoreClientId = n.ClientCoreClientId, ClientDate = n.ClientDate });
                    submissionDictionary[n.SubmissionId].NameClientIdCombos.Add(n.Update);
                }

                var submissions = submissionDictionary.Values.Where(s => s.AddressClientIdCombos.Any() || s.NameClientIdCombos.Any());
                Console.WriteLine("Found {0} Submissions", submissions.Count());

                int threadCount = int.Parse(ConfigurationManager.AppSettings["ThreadCount"]);
                Console.WriteLine("Running against {0} threads", threadCount);
                Parallel.ForEach(submissions, new ParallelOptions() { MaxDegreeOfParallelism = threadCount }, s => ProcessUpdates(s));
            }
        }

        private static void ProcessUpdates(SubmissionClientDataUpdate u)
        {
            using (var db = Application.GetDatabaseInstance() as DbContext)
            {
                var i = ++SubmissionNumber;
                var cd = companyMap[u.CompanyNumberId];

                var ccClientIdCriteria = cd.SystemSettings.ToClientSearchCriteria();
                ccClientIdCriteria.ClientId = u.ClientCoreClientId.ToString();
                ccClientIdCriteria.EffectiveDate = u.ClientDate;
                var ccSettings = cd.SystemSettings.ToClientSystemSettings();
                var results = BTS.WFA.Integration.Services.ClientService.Search(ccSettings, ccClientIdCriteria, 60);

                if (!results.Succeeded)
                {
                    Console.WriteLine("#{0} - Id {1}:  Search Failed for client {2} as of {3}: {4}", i, u.SubmissionId, u.ClientCoreClientId, u.ClientDate, results.Exception.Message);
                    if (u.AddressClientIdCombos.Count > 0)
                        db.Database.ExecuteSqlCommand("UPDATE [dbo].[SubmissionAddress] SET [ModifiedDt] = GETDATE(), [ModifiedBy] = 'Web2014ConversionToolException' WHERE Id IN (" + String.Join(",", u.AddressClientIdCombos.Select(c => "'" + c.AddressRowId + "'")) + ")");
                    if (u.NameClientIdCombos.Count > 0)
                        db.Database.ExecuteSqlCommand("UPDATE [dbo].[SubmissionAdditionalName] SET [ModifiedDt] = GETDATE(), [ModifiedBy] = 'Web2014ConversionToolException' WHERE Id IN (" + String.Join(",", u.NameClientIdCombos.Select(n => "'" + n.NameRowId + "'")) + ")");
                }
                else
                {
                    if (results.Matches.Count != 1)
                    {
                        Console.WriteLine("#{0} - Id {1}:  Could not find client {2} as of {3}: {4}", i, u.SubmissionId, u.ClientCoreClientId, u.ClientDate, u.AddressClientIdCombos.Count);
                        if (u.AddressClientIdCombos.Count > 0)
                            db.Database.ExecuteSqlCommand("UPDATE [dbo].[SubmissionAddress] SET [ModifiedDt] = GETDATE(), [ModifiedBy] = 'Web2014ConversionToolNoClient' WHERE Id IN (" + String.Join(",", u.AddressClientIdCombos.Select(c => "'" + c.AddressRowId + "'")) + ")");
                        if (u.NameClientIdCombos.Count > 0)
                            db.Database.ExecuteSqlCommand("UPDATE [dbo].[SubmissionAdditionalName] SET [ModifiedDt] = GETDATE(), [ModifiedBy] = 'Web2014ConversionToolNoClient' WHERE Id IN (" + String.Join(",", u.NameClientIdCombos.Select(n => "'" + n.NameRowId + "'")) + ")");
                    }
                    else
                    {
                        foreach (var addr in u.AddressClientIdCombos)
                        {
                            var matchingAddr = results.Matches[0].Addresses.FirstOrDefault(a => a.Id == addr.ClientCoreId);
                            if (matchingAddr == null)
                            {
                                Console.WriteLine("#{0} - Id {1}:  Could not find address id {2}; updating as blank", i, u.SubmissionId, addr.ClientCoreId);
                                db.Database.ExecuteSqlCommand("UPDATE [dbo].[SubmissionAddress] SET [ModifiedDt] = GETDATE(), [ModifiedBy] = 'Web2014ConversionToolNoAddress' WHERE Id = @p0", addr.AddressRowId);
                            }
                            else
                            {
                                Console.WriteLine("#{0} - Id {1}:  Updating address id {2}", i, u.SubmissionId, addr.AddressRowId);
                                db.Database.ExecuteSqlCommand(@"UPDATE [dbo].[SubmissionAddress] SET Address1 = @p0, Address2 = @p1, City = @p2, State = @p3, PostalCode = @p4, County = @p5, CountryId = @p6, [ModifiedDt] = GETDATE(), [ModifiedBy] = 'Web2014AddressTool' WHERE Id = @p7",
                                    ((String.IsNullOrEmpty(matchingAddr.HouseNumber)) ? null : matchingAddr.HouseNumber + " ") + (String.IsNullOrEmpty(matchingAddr.Address1) ? null : matchingAddr.Address1),
                                    String.IsNullOrEmpty(matchingAddr.Address2) ? null : matchingAddr.Address2,
                                    matchingAddr.City,
                                    matchingAddr.State,
                                    matchingAddr.PostalCode,
                                    matchingAddr.County,
                                    (String.IsNullOrEmpty(matchingAddr.Country)) ? (int?)null : countryIdMap[matchingAddr.Country],
                                    addr.AddressRowId);
                            }
                        }

                        foreach (var name in u.NameClientIdCombos)
                        {
                            var matchingName = results.Matches[0].Names.FirstOrDefault(n => n.SequenceNumber == name.SequenceNumber);
                            if (matchingName == null)
                            {
                                Console.WriteLine("#{0} - Id {1}:  Could not find name sequence {2}; updating as blank", i, u.SubmissionId, name.SequenceNumber);
                                db.Database.ExecuteSqlCommand("UPDATE [dbo].[SubmissionAdditionalName] SET [ModifiedDt] = GETDATE(), [ModifiedBy] = 'Web2014ConversionToolNoAddress' WHERE Id = @p0", name.NameRowId);
                            }
                            else
                            {
                                Console.WriteLine("#{0} - Id {1}:  Updating name id {2}", i, u.SubmissionId, name.NameRowId);
                                db.Database.ExecuteSqlCommand(@"UPDATE [dbo].[SubmissionAdditionalName] SET BusinessName = @p0, [ModifiedDt] = GETDATE(), [ModifiedBy] = 'Web2014ConversionTool' WHERE Id = @p1", matchingName.BusinessName, name.NameRowId);
                            }
                        }
                    }
                }
            }
        }
    }
}
