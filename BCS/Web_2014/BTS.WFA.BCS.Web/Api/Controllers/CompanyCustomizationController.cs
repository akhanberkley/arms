﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/CompanyCustomization")]
    public class CompanyCustomizationController : BaseController
    {
        private const string administratorRoles = Domains.Roles.Administrator_CustomFields + "," + Domains.Roles.SystemAdministrator;

        [HttpGet, Route("")]
        [System.Web.Http.Description.ResponseType(typeof(List<CompanyCustomizationViewModel>))]
        public HttpResponseMessage Get(CompanyDetail company, ODataQueryOptions<CompanyCustomizationViewModel> query)
        {
            ValidateOdataQuery(query);
            var customizations = BCS.Services.CompanyCustomizationService.GetCustomizations(company);
            return CreateODataResponse(query.ApplyTo(customizations.AsQueryable()) as IQueryable<CompanyCustomizationViewModel>);
        }

        [HttpPut, Route("Reorder")]
        [Authorize(Roles = administratorRoles)]
        public void Reorder(CompanyDetail company, IEnumerable<string> items)
        {
            BCS.Services.CompanyCustomizationService.Reorder(company, items);
        }

        [HttpPost, Route("")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(typeof(CompanyCustomizationViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Insert(CompanyDetail company, CompanyCustomizationViewModel customization)
        {
            var result = BCS.Services.CompanyCustomizationService.SaveCustomization(company, customization, null);
            return CreateValidatedResponse(result);
        }

        [HttpPut, Route("{description}")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(typeof(CompanyCustomizationViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Update(CompanyDetail company, CompanyCustomizationViewModel customization, string description)
        {
            var result = BCS.Services.CompanyCustomizationService.SaveCustomization(company, customization, description);
            return CreateValidatedResponse(result);
        }

        [HttpPost, Route("{description}/Option")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(typeof(CompanyNumberCodeViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage InsertOption(CompanyDetail company, CompanyNumberCodeViewModel option, string description)
        {
            var result = BCS.Services.CompanyCustomizationService.SaveCustomizationOption(company, description, option, null);
            return CreateValidatedResponse(result);
        }

        [HttpPut, Route("{description}/Option/{code}")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(typeof(CompanyNumberCodeViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage UpdateOption(CompanyDetail company, CompanyNumberCodeViewModel option, string description, string code)
        {
            var result = BCS.Services.CompanyCustomizationService.SaveCustomizationOption(company, description, option, code);
            return CreateValidatedResponse(result);
        }
    }
}
