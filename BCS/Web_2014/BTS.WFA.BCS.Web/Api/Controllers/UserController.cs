﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api")]
    [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]//Hide in Docs - other systems shouldn't need this
    public class UserController : BaseController
    {
        private const string administratorRoles = Domains.Roles.Administrator_Users + "," + Domains.Roles.SystemAdministrator;

        [HttpGet, Route("User/Current")]
        public UserViewModel GetCurrentUser()
        {
            return (User.Identity as WebIdentity).UserViewModel;
        }

        [HttpPost, Route("User/Current/DefaultCompany/{company}")]
        public void UpdateCurrentUsersDefaultCompany(CompanyDetail company)
        {
            var user = (User.Identity as WebIdentity);
            BCS.Services.UserService.UpdateDefaultCompany(user.UserViewModel.Id.Value, company);
            Api.Handlers.AuthenticationHandler.ClearUserFromCache(user.Name);
        }

        [HttpGet, Route("{company}/UserSummary")]
        [System.Web.Http.Description.ResponseType(typeof(List<UserSummaryViewModel>))]
        public virtual HttpResponseMessage GetSummary(CompanyDetail company, ODataQueryOptions<Data.Models.User> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(BCS.Services.UserService.SearchSummary(company, query.ApplyTo, UserIsSystemAdministrator));
        }

        [HttpGet, Route("{company}/User")]
        [System.Web.Http.Description.ResponseType(typeof(List<UserViewModel>))]
        public virtual HttpResponseMessage GetFull(CompanyDetail company, ODataQueryOptions<Data.Models.User> query, bool userListReport = false)
        {
            ValidateOdataQuery(query);
            bool showEveryone = (UserIsSystemAdministrator && !userListReport);
            return CreateODataResponse(BCS.Services.UserService.SearchFull(company, query.ApplyTo, showEveryone));
        }

        [HttpGet, Route("{company}/User/{userId}")]
        [Authorize(Roles = administratorRoles)]
        public UserViewModel Get(CompanyDetail company, int userId)
        {
            return BCS.Services.UserService.GetUser(company, userId, UserIsSystemAdministrator);
        }

        [HttpPut, Route("{company}/User/{userId}")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(typeof(UserViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Update(CompanyDetail company, UserViewModel user, int userId)
        {
            var isSystemAdmin = UserIsSystemAdministrator;
            var result = BCS.Services.UserService.SaveUser(company, userId, user, isSystemAdmin, isSystemAdmin);

            if (result.ValidationResults.Count == 0)
                Api.Handlers.AuthenticationHandler.ClearUserFromCache(user.UserName);
            return CreateValidatedResponse(result);
        }

        [HttpPost, Route("{company}/User")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(typeof(UserViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Insert(CompanyDetail company, UserViewModel user)
        {
            var isSystemAdmin = UserIsSystemAdministrator;
            var result = BCS.Services.UserService.SaveUser(company, null, user, isSystemAdmin, isSystemAdmin);

            if (result.ValidationResults.Count == 0)
                Api.Handlers.AuthenticationHandler.ClearUserFromCache(user.UserName);
            return CreateValidatedResponse(result);
        }
    }
}
