﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/Domains")]
    public class DomainController : BaseController
    {
        [HttpGet, Route("State")]
        public List<StateViewModel> StateList()
        {
            return Services.DomainService.FullStateList();
        }

        [HttpGet, Route("Country")]
        public List<CodeListItemViewModel> CountryList()
        {
            return Services.DomainService.FullCountryList();
        }

        [HttpGet, Route("SicCode")]
        [System.Web.Http.Description.ResponseType(typeof(List<SicCodeViewModel>))]
        public HttpResponseMessage SicCodeSearch(ODataQueryOptions<Data.Models.SicCodeList> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(Services.DomainService.SicCodeList(query.ApplyTo));
        }

        [HttpGet, Route("NaicsCode")]
        [System.Web.Http.Description.ResponseType(typeof(List<NaicsCodeViewModel>))]
        public HttpResponseMessage NaicsCodeSearch(ODataQueryOptions<Data.Models.NaicsCodeList> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(Services.DomainService.NaicsCodeList(query.ApplyTo));
        }

        [HttpGet, Route("AddressType")]
        public List<string> AddressTypeList()
        {
            return Services.DomainService.AddressTypeList();
        }

        [HttpGet, Route("LegacyAddressType")]
        public List<string> LegacyAddressTypeList()
        {
            return Services.DomainService.LegacyAddressTypeList();
        }

        [HttpGet, Route("ContactType")]
        public List<string> ContactTypeList()
        {
            return Services.DomainService.ContactTypeList();
        }

        [HttpGet, Route("ClientNameType")]
        public List<string> ClientNameTypeList()
        {
            return Services.DomainService.ClientNameTypeList();
        }

        [HttpGet, Route("UserType")]
        public List<string> UserTypeList()
        {
            return Services.DomainService.UserTypeList();
        }

        [HttpGet, Route("~/api/{company}/Domains/SubmissionStatus")]
        public List<SubmissionStatusViewModel> SubmissionStatuses(CompanyDetail company)
        {
            return Services.DomainService.SubmissionStatuses(company);
        }

        [HttpGet, Route("~/api/{company}/Domains/HazardGradeType")]
        public List<string> HazardGradeTypes(CompanyDetail company)
        {
            return Services.DomainService.HazardGradeTypes(company);
        }

        [HttpGet, Route("~/api/{company}/Domains/PolicySystem")]
        public List<CodeListItemViewModel> PolicySystemList(CompanyDetail company)
        {
            return Services.DomainService.PolicySystems(company);
        }

        [HttpGet, Route("~/api/{company}/Domains/UserRole")]
        public List<CodeListItemViewModel> UserRole(CompanyDetail company)
        {
            return Services.DomainService.UserRoles(company, User.IsInRole(Domains.Roles.SystemAdministrator));
        }

        [HttpGet, Route("~/api/{company}/Domains/NumberControl")]
        [System.Web.Http.Description.ResponseType(typeof(List<NumberControlViewModel>))]
        public HttpResponseMessage NumberControlSearch(CompanyDetail company, ODataQueryOptions<Data.Models.NumberControl> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(Services.DomainService.NumberControls(company, query.ApplyTo));
        }
    }
}
