﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Extensions;
using System.Web.OData.Query;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    public class BaseController : ApiController
    {
        public WebPrincipal UserPrincipal { get { return User as WebPrincipal; } }
        public WebIdentity UserIdentity { get { return User.Identity as WebIdentity; } }
        public bool UserIsSystemAdministrator { get { return UserIdentity.UserViewModel.UserType == Domains.UserTypes.SystemAdministrator; } }

        private static ODataValidationSettings odataSettings = new ODataValidationSettings { AllowedQueryOptions = AllowedQueryOptions.Filter | AllowedQueryOptions.OrderBy | AllowedQueryOptions.Skip | AllowedQueryOptions.Top | AllowedQueryOptions.Count };
        protected void ValidateOdataQuery(ODataQueryOptions query)
        {
            query.Validate(odataSettings);
        }
        protected HttpResponseMessage CreateODataResponse<T>(IEnumerable<T> data)
        {
            var message = Request.CreateResponse(System.Net.HttpStatusCode.OK, data);

            var odataProperties = Request.ODataProperties();
            if (odataProperties.TotalCount.HasValue)
                message.Headers.Add("odata-count", odataProperties.TotalCount.ToString());

            return message;
        }

        protected HttpResponseMessage CreateCachedResult(object data, TimeSpan? cacheLength = null)
        {
            var response = Request.CreateResponse(System.Net.HttpStatusCode.OK, data);
            response.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue() { MaxAge = cacheLength ?? new TimeSpan(0, 10, 0), Private = true };

            return response;
        }

        protected HttpResponseMessage CreateValidatedResponse(BTS.WFA.ViewModels.BaseViewModel validatedObject, bool returnVoidOnValid = false)
        {
            if (validatedObject.ItemNotFound)
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, "Item was not found");
            else if (validatedObject.ValidationResults.Count > 0)
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, validatedObject.ValidationResults);
            else
            {
                var returnCode = (validatedObject.ItemCreated) ? System.Net.HttpStatusCode.Created : System.Net.HttpStatusCode.OK;
                if (returnVoidOnValid)
                    return Request.CreateResponse(returnCode);
                else
                    return Request.CreateResponse(returnCode, validatedObject);
            }
        }

        protected HttpResponseMessage CreateEntityMissingResponse(string entityName)
        {
            return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, new[] { new BTS.WFA.ViewModels.ValidationResult(entityName, "Entity was not passed in or could not be parsed.") });
        }

        protected HttpResponseMessage CreateDeletedResponse(BTS.WFA.ViewModels.BaseViewModel validatedObject)
        {
            if (validatedObject.ItemNotFound)
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound);
            else if (validatedObject.ValidationResults.Count > 0)
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, validatedObject.ValidationResults);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.OK);
        }
    }
}
