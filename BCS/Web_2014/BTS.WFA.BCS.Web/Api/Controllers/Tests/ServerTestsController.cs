﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace BTS.WFA.BCS.Web.Api.Controllers.Tests
{
    /// <summary>
    /// This controller's sole purpose is for testing server web api exceptions and UI handling of exceptions
    /// </summary>
    [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]
    public class ServerTestsController : ApiController
    {
        [HttpGet, Route("api/Tests/ResponseCode/{code:int}")]
        public string ResponseCode(int code)
        {
            throw new HttpResponseException((System.Net.HttpStatusCode)code);
        }

        [HttpGet, Route("api/Tests/ServerException")]
        public string ServerException()
        {
            throw new NotImplementedException();
        }
        [HttpGet, Route("api/Tests/ServerExceptionWithArguments/{number}/{value}")]
        public string ServerExceptionWithArguments(int number, string value)
        {
            throw new Exception("Whoops!", new NotImplementedException());
        }

        [HttpGet, Route("api/Tests/Unauthorized")]
        [Authorize(Roles = "not-a-real-role")]
        public string Unauthorized()
        {
            return "";
        }
    }
}
