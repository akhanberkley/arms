﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Underwriting")]
    [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]//Hide in Docs - others should use APS
    public class UnderwritingController : BaseController
    {
        [HttpGet, Route("Unit")]
        public List<UnderwritingUnitViewModel> GetUnits(CompanyDetail company)
        {
            return BCS.Services.UnderwritingService.GetUnits(company);
        }

        [HttpGet, Route("Underwriter")]
        public List<PersonnelViewModel> GetUnderwriters(CompanyDetail company)
        {
            return BCS.Services.UnderwritingService.GetUnderwriters(company);
        }

        [HttpGet, Route("Analyst")]
        public List<PersonnelViewModel> GetUnderwritingAnalysts(CompanyDetail company)
        {
            return BCS.Services.UnderwritingService.GetUnderwritingAnalysts(company);
        }

        [HttpGet, Route("Technician")]
        public List<PersonnelViewModel> GetUnderwritingTechnicians(CompanyDetail company)
        {
            return BCS.Services.UnderwritingService.GetUnderwritingTechnicians(company);
        }

        [HttpPost, Route("Sync")]
        [Authorize(Roles = Domains.Roles.SystemAdministrator)]
        public List<string> Sync(CompanyDetail company)
        {
            List<string> log = new List<string>();
            BCS.Services.UnderwritingService.SyncAllPersonnel(company, (s => log.Add(s)));

            return log;
        }
    }
}
