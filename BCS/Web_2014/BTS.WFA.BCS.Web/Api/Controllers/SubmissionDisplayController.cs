﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/SubmissionDisplay")]
    [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]//This doesn't need to show up in the API docs
    public class SubmissionDisplayController : BaseController
    {
        private const string administratorRoles = Domains.Roles.Administrator_SubmissionTables + "," + Domains.Roles.SystemAdministrator;

        [HttpGet, Route("Table")]
        [Authorize(Roles = administratorRoles)]
        public List<CodeListItemViewModel> SubmissionSummaryTables(CompanyDetail company)
        {
            return Services.SubmissionQueryService.GetSummaryTables(company);
        }

        [HttpGet, Route("Field")]
        [Authorize(Roles = administratorRoles)]
        public List<SubmissionDisplayFieldViewModel> SubmissionSummaryFields(CompanyDetail company)
        {
            return Services.SubmissionQueryService.GetAllSummaryFields(company);
        }

        [HttpPut, Route("Table/{viewName}")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(null)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage SaveSubmissionSummaryTable(CompanyDetail company, string viewName, List<SubmissionDisplayFieldViewModel> fields)
        {
            return CreateValidatedResponse(Services.SubmissionQueryService.SaveSummaryTable(company, viewName, fields), true);
        }
        [HttpGet, Route("Table/{viewName}/Field")]
        public List<SubmissionDisplayFieldViewModel> SubmissionSummaryFields(CompanyDetail company, string viewName)
        {
            return Services.SubmissionQueryService.GetSummaryTableFields(company, viewName);
        }
    }
}
