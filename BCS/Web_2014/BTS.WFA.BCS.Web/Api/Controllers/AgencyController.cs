﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Agency")]
    [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]//Hide in Docs - others should use APS
    public class AgencyController : BaseController
    {
        [HttpGet, Route("~/api/{company}/AgencySummary")]
        [System.Web.Http.Description.ResponseType(typeof(List<AgencySummaryViewModel>))]
        public HttpResponseMessage SearchAutoComplete(CompanyDetail company, ODataQueryOptions<Data.Models.Agency> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(Services.AgencyService.SearchAutoComplete(company, query.ApplyTo, UserPrincipal.LimitingAgencyIds));
        }

        [HttpGet, Route("")]
        [System.Web.Http.Description.ResponseType(typeof(List<AgencyViewModel>))]
        public virtual HttpResponseMessage Get(CompanyDetail company, ODataQueryOptions<Data.Models.Agency> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(Services.AgencyService.Search(company, query.ApplyTo, UserPrincipal.LimitingAgencyIds));
        }

        [HttpGet, Route("{agencyCode}")]
        public AgencyViewModel Get(CompanyDetail company, string agencyCode)
        {
            return BCS.Services.AgencyService.Get(company, agencyCode, UserPrincipal.LimitingAgencyIds);
        }
        [HttpGet, Route("{agencyCode}/Agents")]
        public List<AgentViewModel> GetAgents(CompanyDetail company, string agencyCode)
        {
            return BCS.Services.AgencyService.GetAgents(company, agencyCode);
        }
        [HttpGet, Route("{agencyCode}/UnderwritingUnits")]
        public List<AgencyUnderwritingUnitViewModel> GetUnderwritingUnits(CompanyDetail company, string agencyCode)
        {
            return BCS.Services.AgencyService.GetUnderwritingUnits(company, agencyCode);
        }
        [HttpGet, Route("{agencyCode}/LicensedStates")]
        public List<StateViewModel> GetLicensedStates(CompanyDetail company, string agencyCode)
        {
            return BCS.Services.AgencyService.GetLicensedStates(company, agencyCode);
        }
        [HttpGet, Route("{agencyCode}/Submissions")]
        [System.Web.Http.Description.ResponseType(typeof(List<SubmissionViewModel>))]
        public HttpResponseMessage GetSubmissions(CompanyDetail company, string agencyCode, ODataQueryOptions<Data.Models.Submission> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(BCS.Services.SubmissionQueryService.GetAgencysSubmissions(company, agencyCode, query.ApplyTo, UserPrincipal.LimitingAgencyIds));
        }

        [HttpPost, Route("{agencyCode}/Sync")]
        [Authorize(Roles = Domains.Roles.SystemAdministrator)]
        public AgencySyncResultViewModel Sync(CompanyDetail company, string agencyCode)
        {
            return BCS.Services.AgencyService.SyncAgency(company, agencyCode, true);
        }
    }
}
