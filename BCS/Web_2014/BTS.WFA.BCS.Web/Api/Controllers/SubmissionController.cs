﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Submission")]
    public class SubmissionController : BaseController
    {
        #region Search
        [HttpPost, Route("~/api/{company}/DuplicateSubmission")]
        public List<SubmissionViewModel> DuplicateSearch(CompanyDetail company, SubmissionDuplicateSearchCriteriaViewModel criteria)
        {
            return Services.SubmissionQueryService.DuplicateSearch(company, criteria, UserPrincipal.LimitingAgencyIds);
        }

        [HttpGet, Route("")]
        [System.Web.Http.Description.ResponseType(typeof(List<SubmissionViewModel>))]
        public virtual HttpResponseMessage Get(CompanyDetail company, ODataQueryOptions<Data.Models.Submission> query, bool crossCompany = false, bool idsOnly = false)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(Services.SubmissionQueryService.Search(company, crossCompany, query.ApplyTo, UserPrincipal.LimitingAgencyIds, idsOnly));
        }
        [HttpGet, Route("~/api/{company}/SubmissionSummary")]
        [System.Web.Http.Description.ResponseType(typeof(List<SubmissionSummaryViewModel>))]
        public HttpResponseMessage SearchAutoComplete(CompanyDetail company, ODataQueryOptions<Data.Models.Submission> query, bool crossCompany = false)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(Services.SubmissionQueryService.SearchAutoComplete(company, crossCompany, query.ApplyTo, UserPrincipal.LimitingAgencyIds));
        }

        #endregion

        #region Specific Submission
        [HttpGet, Route("{submissionId}")]
        public SubmissionViewModel Get(CompanyDetail company, string submissionId)
        {
            var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
            return BCS.Services.SubmissionQueryService.GetByNumber(company, parsedNumber, UserPrincipal.LimitingAgencyIds);
        }

        [HttpPost, Route("")]
        [EditAccessAuthorization]
        [System.Web.Http.Description.ResponseType(typeof(SubmissionViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Insert(CompanyDetail company, SubmissionViewModel submission, bool generatePolicyNumber = false, bool returnFullSubmission = true, bool ignoreValidations = false)
        {
            if (submission == null)
                return CreateEntityMissingResponse("Submission");
            else
            {
                var result = BCS.Services.SubmissionService.Save(company, null, submission, User.Identity.Name, new BCS.Services.SubmissionSaveOptions() { GeneratePolicyNumber = generatePolicyNumber, ReturnFullSubmission = returnFullSubmission, IgnoreValidations = ignoreValidations }, UserPrincipal.LimitingAgencyIds);
                return CreateValidatedResponse(result);
            }
        }

        [HttpPut, Route("{submissionId}")]
        [EditAccessAuthorization]
        [System.Web.Http.Description.ResponseType(typeof(SubmissionViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Update(CompanyDetail company, SubmissionViewModel submission, string submissionId, bool returnFullSubmission = true, bool ignoreValidations = false)
        {
            if (submission == null)
                return CreateEntityMissingResponse("Submission");
            else
            {
                var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
                var result = BCS.Services.SubmissionService.Save(company, parsedNumber, submission, User.Identity.Name, new BCS.Services.SubmissionSaveOptions() { GeneratePolicyNumber = false, ReturnFullSubmission = returnFullSubmission, IgnoreValidations = ignoreValidations }, UserPrincipal.LimitingAgencyIds);
                return CreateValidatedResponse(result);
            }
        }

        [HttpDelete, Route("{submissionId}")]
        [Authorize(Roles = Domains.Roles.SystemAdministrator + "," + Domains.Roles.Action_DeleteSubmission)]
        [System.Web.Http.Description.ResponseType(null)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Delete(CompanyDetail company, string submissionId)
        {
            var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
            var results = BCS.Services.SubmissionService.Delete(company, parsedNumber, User.Identity.Name);
            return CreateValidatedResponse(results);
        }

        [HttpPost, Route("Validate")]
        [EditAccessAuthorization]
        [System.Web.Http.Description.ResponseType(null)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Validate(CompanyDetail company, SubmissionViewModel submission, bool generatePolicyNumber = false)
        {
            BCS.Services.SubmissionService.Validate(company, null, submission, generatePolicyNumber);
            return CreateValidatedResponse(submission, true);
        }
        [EditAccessAuthorization]
        [HttpPut, Route("{submissionId}/Validate")]
        [System.Web.Http.Description.ResponseType(null)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Validate(CompanyDetail company, SubmissionViewModel submission, string submissionId, bool generatePolicyNumber = false)
        {
            var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
            BCS.Services.SubmissionService.Validate(company, parsedNumber, submission, generatePolicyNumber);
            return CreateValidatedResponse(submission);
        }

        [Authorize(Roles = Domains.Roles.SystemAdministrator + "," + Domains.Roles.Tools_PolicyNumberGenerator)]
        [HttpPost, Route("NextPolicyNumber")]
        public string GetNextPolicyNumber(CompanyDetail company, SubmissionViewModel submission)
        {
            return BCS.Services.SubmissionService.GetNextPolicyNumber(company, submission ?? new SubmissionViewModel());
        }

        [Authorize(Roles = Domains.Roles.SystemAdministrator + "," + Domains.Roles.Action_SwitchUnderwritingPersonnel)]
        [HttpPut, Route("UnderwritingPersonnel")]
        [System.Web.Http.Description.ResponseType(null)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage ReassignUnderwritingPersonnel(CompanyDetail company, SubmissionUnderwritingPersonnelReassignmentViewModel reassignments)
        {
            return CreateValidatedResponse(Services.SubmissionService.ReassignUnderwritingPersonnel(company, reassignments), true);
        }

        [HttpPut, Route("{submissionId}/Status")]
        [EditAccessAuthorization]
        [System.Web.Http.Description.ResponseType(typeof(SubmissionStatusUpdateViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage UpdateStatus(CompanyDetail company, SubmissionStatusUpdateViewModel update, string submissionId)
        {
            var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
            var result = BCS.Services.SubmissionService.UpdateStatus(company, parsedNumber, update, User.Identity.Name);
            return CreateValidatedResponse(result, true);
        }

        [HttpPut, Route("{submissionId}/AssociatedSubmissions")]
        [EditAccessAuthorization]
        [System.Web.Http.Description.ResponseType(null)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage UpdateAssociatedSubmissions(CompanyDetail company, SubmissionSummaryViewModel[] submissions, string submissionId)
        {
            var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
            var result = BCS.Services.SubmissionService.UpdateAssociatedSubmissions(company, parsedNumber, submissions);
            return CreateValidatedResponse(result, true);
        }

        [HttpPut, Route("{submissionId}/StatusHistory/{historyId}/Revert")]
        [EditAccessAuthorization]
        [System.Web.Http.Description.ResponseType(null)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage RevertStatus(CompanyDetail company, SubmissionStatusUpdateViewModel update, string submissionId, int historyId)
        {
            var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
            var result = BCS.Services.SubmissionStatusHistoryService.RevertStatus(company, parsedNumber, new SubmissionStatusHistoryViewModel() { Id = historyId }, update, User.Identity.Name);
            return CreateValidatedResponse(result, true);
        }

        [HttpPut, Route("{submissionId}/ClientReassign")]
        [Authorize(Roles = Domains.Roles.SystemAdministrator + "," + Domains.Roles.Action_SwitchClient)]
        [System.Web.Http.Description.ResponseType(typeof(SubmissionClientReassignViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Update(CompanyDetail company, string submissionId, SubmissionClientReassignViewModel newInsured)
        {
            var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
            var result = BCS.Services.SubmissionService.ReassignClient(company, parsedNumber, newInsured, User.Identity.Name);
            return CreateValidatedResponse(result, true);
        }

        #endregion
    }
}
