﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Carrier")]
    public class CarrierController : BaseController
    {
        private const string administratorRoles = Domains.Roles.Administrator_OtherCarriers + "," + Domains.Roles.SystemAdministrator;

        [HttpGet, Route("~/api/Carrier")]
        [Authorize(Roles = administratorRoles)]
        public List<CodeListItemViewModel> GetAll()
        {
            return Services.CarrierService.AllOptions();
        }

        [HttpGet, Route("")]
        public List<CodeListItemViewModel> GetCompany(CompanyDetail company, ODataQueryOptions<Data.Models.OtherCarrier> query)
        {
            return Services.CarrierService.CompanyOptions(company, query.ApplyTo);
        }

        [HttpPut, Route("")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(null)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage SaveSubmissionSummaryTable(CompanyDetail company, List<CodeListItemViewModel> options)
        {
            return CreateValidatedResponse(Services.CarrierService.SaveOptions(company, options), true);
        }

    }
}
