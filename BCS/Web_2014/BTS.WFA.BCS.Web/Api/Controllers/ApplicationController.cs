﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Application")]
    public class ApplicationController : BaseController
    {
        [HttpGet, Route("{submissionId}")]
        public List<SubmissionViewModel> GetApplicationSubmissions(CompanyDetail company, string submissionId)
        {
            var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
            return BCS.Services.SubmissionQueryService.GetApplicationSubmissions(company, parsedNumber, UserPrincipal.LimitingAgencyIds);
        }

        [HttpGet, Route("{submissionId}/ClientsNonApplicationSubmissions")]
        [System.Web.Http.Description.ResponseType(typeof(List<SubmissionViewModel>))]
        public HttpResponseMessage GetClientsNonApplicationSubmissions(CompanyDetail company, string submissionId, ODataQueryOptions<Data.Models.Submission> query)
        {
            var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
            return CreateODataResponse(BCS.Services.SubmissionQueryService.GetClientsNonApplicationSubmissions(company, parsedNumber, UserPrincipal.LimitingAgencyIds, query.ApplyTo));
        }

        [HttpPut, Route("{submissionId}/Status")]
        [EditAccessAuthorization]
        [System.Web.Http.Description.ResponseType(typeof(List<SubmissionStatusUpdateViewModel>))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage UpdateApplicationStatus(CompanyDetail company, SubmissionStatusUpdateViewModel update, string submissionId)
        {
            var parsedNumber = SubmissionNumberViewModel.Parse(submissionId);
            var result = BCS.Services.SubmissionService.UpdateApplicationStatus(company, parsedNumber, update, User.Identity.Name);
            return CreateValidatedResponse(result);
        }
    }
}
