﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/Company")]
    public class CompanyController : BaseController
    {
        [HttpGet, Route("")]
        public IEnumerable<CompanySummaryViewModel> Get()
        {
            return Application.CompanyMap.Values.Select(c => c.Summary).OrderBy(c => c.Abbreviation);
        }

        [HttpGet, Route("{company}/Settings")]
        public CompanyConfiguration GetSettings(CompanyDetail company)
        {
            return company.Configuration;
        }

        [HttpGet, Route("Configuration")]
        public IEnumerable<object> GetConfigurations()
        {
            var includeSystemSettings = UserIsSystemAdministrator;
            return Application.CompanyMap.Values
                                        .OrderBy(c => c.Summary.Abbreviation)
                                        .Select(c => new
                                        {
                                            Summary = c.Summary,
                                            Configuration = c.Configuration,
                                            SystemSettings = (includeSystemSettings) ? c.SystemSettings : null
                                        });
        }
    }
}
