﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Address")]
    [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]//Hide in Docs - others should use service directly
    public class AddressController : BaseController
    {
        [HttpGet, Route("Lookup")]
        public IEnumerable<AddressViewModel> Lookup(CompanyDetail company, string address1 = null, string address2 = null, string city = null, string state = null, string postalCode = null, string county = null)
        {
            var criteria = new AddressLookupCriteriaViewModel() {
                Address1 = address1,
                Address2 = address2,
                City = city,
                State = state,
                PostalCode = postalCode,
                County = county
            };
            var results = Services.AddressService.Lookup(company, criteria);
            return results.Matches;
        }
    }
}
