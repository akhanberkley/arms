﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/PolicySymbol")]
    public class PolicySymbolController : BaseController
    {
        private const string administratorRoles = Domains.Roles.Administrator_PolicySymbols + "," + Domains.Roles.SystemAdministrator;

        [HttpGet, Route("")]
        public List<PolicySymbolViewModel> PrimaryPolicySymbols(CompanyDetail company, ODataQueryOptions<Data.Models.PolicySymbol> query)
        {
            return Services.PolicySymbolService.Search(company, query.ApplyTo);
        }

        [HttpPost, Route("")]
        [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(typeof(PolicySymbolViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Update(CompanyDetail company, PolicySymbolViewModel policySymbol)
        {
            var result = BCS.Services.PolicySymbolService.Save(company, null, policySymbol);
            return CreateValidatedResponse(result);
        }

        [HttpPut, Route("{code}")]
        [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(typeof(PolicySymbolViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Update(CompanyDetail company, string code, PolicySymbolViewModel policySymbol)
        {
            var result = BCS.Services.PolicySymbolService.Save(company, code, policySymbol);
            return CreateValidatedResponse(result);
        }

        [HttpDelete, Route("{code}")]
        [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(Roles = administratorRoles)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Update(CompanyDetail company, string code)
        {
            var result = BCS.Services.PolicySymbolService.Delete(company, code);
            return CreateDeletedResponse(result);
        }
    }
}
