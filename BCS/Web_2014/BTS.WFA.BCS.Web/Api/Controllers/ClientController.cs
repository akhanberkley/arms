﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;
using BTS.WFA.ViewModels;
using System.Web.Http.Description;
using Swashbuckle.Swagger.Annotations;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Client")]
    public class ClientController : BaseController
    {
        [HttpGet, Route("")]
        public ClientSearchResultsViewModel Search(CompanyDetail company, string businessName = null, string fein = null, string phoneNumber = null, string clientId = null, string address = null, string city = null, string state = null, string postalCode = null, long? portfolioId = null, string portfolioName = null)
        {
            var criteria = new ClientSearchCriteriaViewModel()
                {
                    BusinessName = businessName,
                    FEIN = fein,
                    PhoneNumber = phoneNumber,
                    ClientCoreId = clientId,
                    Address = address,
                    City = city,
                    State = state,
                    PostalCode = postalCode,
                    PortfolioId = portfolioId,
                    PortfolioName = portfolioName
                };

            return Services.ClientService.Search(company, criteria);
        }

        [HttpPost, Route("")]
        [EditAccessAuthorization]
        [ResponseType(typeof(List<ClientViewModel>))]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<ValidationResult>))]
        public HttpResponseMessage Insert(CompanyDetail company, ClientViewModel client, bool returnFullClient = false)
        {
            return SaveClient(company, null, client, returnFullClient, false);
        }

        [HttpPut, Route("{clientSystemId}")]
        [EditAccessAuthorization]
        [ResponseType(typeof(List<ClientViewModel>))]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<ValidationResult>))]
        public HttpResponseMessage Update(CompanyDetail company, ClientViewModel client, ClientSystemId clientSystemId, bool updatePortfolioName = false, bool returnFullClient = false)
        {
            return SaveClient(company, clientSystemId, client, returnFullClient, updatePortfolioName);
        }

        [HttpPost, Route("Validate")]
        [EditAccessAuthorization]
        [ResponseType(null)]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<ValidationResult>))]
        public HttpResponseMessage Validate(CompanyDetail company, ClientViewModel client)
        {
            var segmentOverride = DetermineClientSegment(company, client);
            BCS.Services.ClientService.Validate(company, client, segmentOverride);

            return CreateValidatedResponse(client, true);
        }

        private HttpResponseMessage SaveClient(CompanyDetail company, ClientSystemId clientSystemId, ClientViewModel client, bool returnFullClient, bool updatePortfolioName)
        {
            if (client == null)
                return CreateEntityMissingResponse("Client");
            else
            {
                var segmentOverride = DetermineClientSegment(company, client);
                var result = BCS.Services.ClientService.Save(company, clientSystemId, client, new Services.ClientSaveOptions { ReturnFullClient = returnFullClient, UpdatePorftolioName = updatePortfolioName, SegmentOverride = segmentOverride }, UserIdentity.Name);

                return CreateValidatedResponse(result);
            }
        }
        private CodeListItemViewModel DetermineClientSegment(CompanyDetail company, ClientViewModel client)
        {
            CodeListItemViewModel segmentOverride = null;
            if (UserPrincipal.IsInRole(Domains.Roles.Action_SetClientSegment) && company.Configuration.ClientFields.ClientCoreSegment.Enabled)
                segmentOverride = company.Configuration.ClientCoreSegments.FirstOrDefault(s => s.Code == client.ClientCoreSegment.Code);

            if (segmentOverride == null && !String.IsNullOrEmpty(UserIdentity.UserViewModel.ClientCoreSegmentCode))
                segmentOverride = company.Configuration.ClientCoreSegments.FirstOrDefault(s => s.Code == UserIdentity.UserViewModel.ClientCoreSegmentCode);

            return segmentOverride;
        }

        [HttpGet, Route("{clientSystemId}")]
        [ResponseType(typeof(ClientViewModel))]
        [SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Client Core Failure", typeof(ClientSearchFailureViewModel))]
        public HttpResponseMessage Get(CompanyDetail company, ClientSystemId clientSystemId)
        {
            var results = BCS.Services.ClientService.GetByClientSystemId(company, clientSystemId);

            if (results.Failures.Count > 0)
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, results.Failures[0]);
            else
                return CreateValidatedResponse((results.Matches.Count > 0) ? results.Matches[0] : new WFA.ViewModels.BaseViewModel() { ItemNotFound = true });
        }

        [HttpGet, Route("{clientSystemId}/Submission")]
        [ResponseType(typeof(List<SubmissionViewModel>))]
        public HttpResponseMessage GetSubmissions(CompanyDetail company, ClientSystemId clientSystemId, ODataQueryOptions<Data.Models.Submission> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(BCS.Services.SubmissionQueryService.GetClientsSubmissions(company, clientSystemId, query.ApplyTo, UserPrincipal.LimitingAgencyIds));
        }

        [HttpGet, Route("{clientSystemId}/ClassCode")]
        [ResponseType(typeof(List<ClassCodeViewModel>))]
        public HttpResponseMessage GetClassCodes(CompanyDetail company, ClientSystemId clientSystemId, ODataQueryOptions<Data.Models.ClassCode> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(BCS.Services.ClientService.GetClassCodes(company, clientSystemId, query.ApplyTo));
        }

        [HttpPost, Route("{clientSystemId}/ClassCode/{classCodeValue}")]
        [EditAccessAuthorization]
        public void AddClassCode(CompanyDetail company, ClientSystemId clientSystemId, string classCodeValue)
        {
            BCS.Services.ClientService.AddClassCode(company, clientSystemId, classCodeValue);
        }
        [HttpDelete, Route("{clientSystemId}/ClassCode/{classCodeValue}")]
        [EditAccessAuthorization]
        public void RemoveClassCode(CompanyDetail company, ClientSystemId clientSystemId, string classCodeValue)
        {
            BCS.Services.ClientService.DeleteClassCode(company, clientSystemId, classCodeValue);
        }

        [HttpGet, Route("Similar")]
        public IEnumerable<ClientCrossCompanySearchResultsViewModel> SimilarClients(CompanyDetail company, string insuredName, string state = null)
        {
            return BCS.Services.ClientService.GetSimilarClients(company, insuredName, state);
        }
    }
}
