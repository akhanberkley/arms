﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/ClassCode")]
    public class ClassCodeController : BaseController
    {
        private const string administratorRoles = Domains.Roles.Administrator_ClassCodes + "," + Domains.Roles.SystemAdministrator;

        [HttpGet, Route("")]
        [System.Web.Http.Description.ResponseType(typeof(List<ClassCodeViewModel>))]
        public virtual HttpResponseMessage Get(CompanyDetail company, ODataQueryOptions<Data.Models.ClassCode> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(Services.ClassCodeService.Search(company, query.ApplyTo));
        }

        [HttpPost, Route("")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(typeof(ClassCodeViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Insert(CompanyDetail company, ClassCodeViewModel classCode)
        {
            var result = BCS.Services.ClassCodeService.SaveClassCode(company, classCode, null);
            return CreateValidatedResponse(result);
        }

        [HttpPut, Route("{code}")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(typeof(ClassCodeViewModel))]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.BadRequest, "Validation Failed", typeof(List<BTS.WFA.ViewModels.ValidationResult>))]
        public HttpResponseMessage Update(CompanyDetail company, ClassCodeViewModel classCode, string code)
        {
            var result = BCS.Services.ClassCodeService.SaveClassCode(company, classCode, code);
            return CreateValidatedResponse(result);
        }

        [HttpDelete, Route("{code}")]
        [Authorize(Roles = administratorRoles)]
        [System.Web.Http.Description.ResponseType(null)]
        public HttpResponseMessage Delete(CompanyDetail company, string code)
        {
            var results = BCS.Services.ClassCodeService.DeleteClassCode(company, code);
            return CreateDeletedResponse(results);
        }
    }
}
