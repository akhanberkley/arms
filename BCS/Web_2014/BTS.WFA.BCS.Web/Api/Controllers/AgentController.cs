﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.BCS.ViewModels;
using BTS.WFA.BCS.Web.Api.Filters;
using BTS.WFA.ViewModels;

namespace BTS.WFA.BCS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Agent")]
    [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]//Hide in Docs - others should use APS
    public class AgentController : BaseController
    {
        [HttpGet, Route("")]
        [System.Web.Http.Description.ResponseType(typeof(List<AgentViewModel>))]
        public virtual HttpResponseMessage Get(CompanyDetail company, ODataQueryOptions<Data.Models.Agent> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(Services.AgentService.Search(company, query.ApplyTo));
        }

        [HttpGet, Route("UnspecifiedReason")]
        public virtual List<string> UnspecifiedReasons(CompanyDetail company)
        {
            return Services.AgentService.UnspecifiedReasons(company);
        }
    }
}
