﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace BTS.WFA.BCS.Web.Api.Filters
{
    public class WebExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            string controller = null;
            if (context.ActionContext.ControllerContext != null && context.ActionContext.ControllerContext.Controller != null)
                controller = context.ActionContext.ControllerContext.Controller.ToString();
            string action = null;
            if (context.ActionContext.ActionDescriptor != null)
                action = context.ActionContext.ActionDescriptor.ActionName;

            Application.HandleException(context.Exception, new
            {
                Controller = controller,
                Action = action,
                Parameters = context.ActionContext.ActionArguments
            });
        }
    }
}