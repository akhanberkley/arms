﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Net.Http;
using System.Threading;

namespace BTS.WFA.BCS.Web.Api.Filters
{
    /// <summary>
    /// Verifies the user is not "Read only"
    /// </summary>
    public class EditAccessAuthorizationAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var identity = Thread.CurrentPrincipal.Identity as WebIdentity;
            return (identity.UserViewModel.UserType != Domains.UserTypes.Read_Only);
        }
    }
}