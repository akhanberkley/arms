﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Net.Http;
using System.Threading;
using System.Web.Http.Filters;
using System.Net;
using BTS.WFA.BCS.ViewModels;

namespace BTS.WFA.BCS.Web.Api.Filters
{
    /// <summary>
    /// Responds unauthorized if user doesn't have access to that specific company
    /// </summary>
    public class CompanyAccessAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            foreach (var param in actionContext.ActionDescriptor.GetParameters())
            {
                if (param.ParameterType == typeof(CompanyDetail))
                {
                    var company = actionContext.ActionArguments[param.ParameterName] as CompanyDetail;
                    if (company == null)
                        actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, String.Format("Unknown Company: {0}", actionContext.ModelState[param.ParameterName].Value.AttemptedValue));
                    else
                    {
                        var user = Thread.CurrentPrincipal as WebPrincipal;
                        var identity = user.Identity as WebIdentity;

                        if ((identity.UserViewModel.UserType != Domains.UserTypes.SystemAdministrator && !user.HasAccessToCompany(company.Summary.Abbreviation)))
                            actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, String.Format("Unauthorized Company: {0}", company.Summary.Abbreviation));
                    }
                }
            }
        }
    }
}