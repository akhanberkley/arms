﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BTS.WFA.BCS.ViewModels;

namespace BTS.WFA.BCS.Web.Api
{
    public class TestUsers
    {
        public static UserViewModel CompanyAdmin(string companyKey)
        {
            return CreateUser("CompanyAdminUser", new[] { companyKey }, "User", new string[] { Domains.Roles.Action_DeleteSubmission, Domains.Roles.Action_RenumberSubmission, Domains.Roles.Action_SwitchClient, 
                                                                                                  Domains.Roles.Administrator_ClassCodes, Domains.Roles.Administrator_CustomFields, Domains.Roles.Administrator_Users, Domains.Roles.Tools_PolicyNumberGenerator });
        }
        public static UserViewModel StandardUser(string companyKey)
        {
            return CreateUser("StandardUser", new[] { companyKey }, "User", new string[] { });
        }
        public static UserViewModel Auditor(string companyKey)
        {
            return CreateUser("StandardUser", new[] { companyKey }, "Read Only", new string[] { });
        }

        public static UserViewModel CreateUser(string userName, IEnumerable<string> companyKeys, string userType, IEnumerable<string> roles)
        {
            var user = new UserViewModel() { Active = true, UserName = userName, UserType = userType };
            if (roles != null)
                foreach (var r in roles)
                    user.Roles.Add(r);

            if (companyKeys != null)
                foreach (var cn in companyKeys)
                    user.Companies.Add(new WFA.ViewModels.CodeListItemViewModel() { Code = cn, Description = Application.CompanyMap[cn].Summary.CompanyName });

            return user;
        }

    }
}