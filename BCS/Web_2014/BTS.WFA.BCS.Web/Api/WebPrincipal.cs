﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using BTS.WFA.BCS.ViewModels;

namespace BTS.WFA.BCS.Web.Api
{
    public class WebPrincipal : IPrincipal
    {
        private WebIdentity webIdentity;
        public IIdentity Identity { get { return webIdentity; } }
        public List<int> LimitingAgencyIds { get; private set; }

        public WebPrincipal(string userName, UserViewModel overrideUser)
        {
            LimitingAgencyIds = new List<int>();
            
            var user = overrideUser ?? Services.UserService.GetActiveUser(userName);
            if (user != null)
            {
                if (user.LimitingAgency != null)
                {
                    LimitingAgencyIds.Add(user.LimitingAgency.Id);
                    LimitingAgencyIds.AddRange(Services.AgencyService.GetChildrenAgencyIds(user.LimitingAgency.Id));
                }

                webIdentity = new WebIdentity(user);
            }
        }

        public bool IsInRole(string role)
        {
            return (webIdentity.UserViewModel.UserType == Domains.UserTypes.SystemAdministrator || webIdentity.UserViewModel.Roles.Contains(role));
        }
        public bool HasAccessToCompany(string companyKey)
        {
            return webIdentity.UserViewModel.Companies.Any(c => String.Equals(c.Code, companyKey, StringComparison.CurrentCultureIgnoreCase));
        }
    }

    public class WebIdentity : IIdentity
    {
        public WebIdentity(UserViewModel user)
        {
            IsAuthenticated = true;
            Name = user.UserName;
            Roles = new List<string>();
            UserViewModel = user;
        }

        public string AuthenticationType { get { return "BCS"; } }
        public bool IsAuthenticated { get; set; }
        public string Name { get; set; }
        public UserViewModel UserViewModel { get; set; }
        public List<string> Roles { get; set; }
    }
}