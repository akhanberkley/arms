﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.IdentityModel.Tokens;
using BTS.WFA.BCS.ViewModels;

namespace BTS.WFA.BCS.Web.Api.Handlers
{
    public class VersionHandler : DelegatingHandler
    {
        private string version = null;

        public VersionHandler()
        {
            string filePath = System.Reflection.Assembly.GetCallingAssembly().Location;

            var b = new byte[2048];
            using (var s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                s.Read(b, 0, 2048);
            var dt = new System.DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(System.BitConverter.ToInt32(b, System.BitConverter.ToInt32(b, 60) + 8));
            
            version = dt.AddHours(System.TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours).ToString("s");
        }


        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);
            response.Headers.Add("Version", version);
            return response;
        }
    }
}