﻿/// <reference path="app.ts" />
module bcs {
    'use strict';

    //Shared Controllers/Resolves
    var companyKeyController = function ($state: ng.ui.IStateService) {
        var key = <string>$state.params["companyKey"];
        var lowerKey = key.toLowerCase();
        if (key != lowerKey) {
            $state.go('.', { companyKey: lowerKey }, { location: 'replace' });
        }
    };
    var accessResolves = {
        User: function (UserService: UserService) { return UserService.UserPromise; },
        HasAccess: function ($q: ng.IQService, User: User, $stateParams: ng.ui.IStateParamsService) {
            return new $q((resolve, reject) => {
                var hasAccess = false;
                var companyKey = (<string>$stateParams['companyKey']).toUpperCase();
                for (var i = 0; i <= User.Companies.length - 1; i++) {
                    if (User.Companies[i].Code == companyKey) {
                        hasAccess = true;
                        User.SelectedCompany = User.Companies[i];

                        break;
                    }
                }

                if (hasAccess) {
                    resolve();
                } else {
                    reject('Company ' + companyKey + ' does not exist or you do not have access');
                }
            });
        }
    };

    app.config(function ($locationProvider: ng.ILocationProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
    });

    app.config(function ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        //Always remove trailing slash
        $urlRouterProvider.rule(function ($injector: ng.auto.IInjectorService, $location: ng.ILocationService) {
            var path = $location.path();
            if (path != '/' && path.slice(-1) === '/') {
                $location.replace().path(path.slice(0, -1));
            }
        });
        
        //Primary states
        $stateProvider
            .state('index', {
            url: "/",
            resolve: { User: function (UserService: UserService) { return UserService.UserPromise; } },
            controller: function ($state: ng.ui.IStateService, User: User) {
                $state.go('cn.search.clients', { companyKey: User.SelectedCompany.Code.toLowerCase() }, { location: 'replace' });
            }
        })
            .state('bpm', {
            url: "/bpm/{companyKey}",
            resolve: accessResolves,
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<bpm-view company-key="${$stateParams['companyKey']}"></bpm-view>`;
            },
            controller: companyKeyController
        })
            .state('cn', {
            url: "/{companyKey}",
            resolve: accessResolves,
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<company-view company-key="${$stateParams['companyKey']}"></company-view>`;
            },
            controller: companyKeyController
        });

        //If route names have a period in them, they are sub-routes in ui.router; they inherit the parent's info and are nested views
        $stateProvider.state('cn.search', {
            abstract: true,
            template: '<search></search>'
        });
        SearchAgency($stateProvider);
        SearchClient($stateProvider);
        SearchSubmission($stateProvider);

        Clearance($stateProvider);
        Application($stateProvider);
        Administration($stateProvider);
        Tools($stateProvider);
        Bpm($stateProvider);
    });

    function SearchAgency(provider: ng.ui.IStateProvider) {
        provider.state('cn.search.agencies', {
            url: "/agencies",
            analytics: 'Agency Search'
        })
            .state('cn.search.agency', {
            abstract: true,
            url: "/agency/{agencyCode}",
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                var agencyCode = $stateParams['agencyCode'];
                return `<agency agency-code="'${agencyCode}'"></agency>`;
            }
        })
            .state('cn.search.agency.linkedagencies', {
            url: "",
            analytics: 'View Agency',
            templateUrl: '/app/components/agency/tabs/linked-agencies.html'
        })
            .state('cn.search.agency.underwritingunits', {
            url: "/underwritingunits",
            templateUrl: '/app/components/agency/tabs/underwriting-units.html'
        })
            .state('cn.search.agency.licensedstates', {
            url: "/licensedstates",
            templateUrl: '/app/components/agency/tabs/licensed-states.html'
        })
            .state('cn.search.agency.agents', {
            url: "/agents",
            templateUrl: '/app/components/agency/tabs/agents.html'
        })
            .state('cn.search.agency.submissions', {
            url: "/submissions",
            templateUrl: '/app/components/agency/tabs/submissions.html'
        });
    }
    function SearchClient(provider: ng.ui.IStateProvider) {
        provider.state('cn.search.clients', {
            url: "/clients",
            analytics: 'Client Search'
        })
            .state('cn.search.client', {
            abstract: true,
            url: "/client/{clientCoreId}",
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                var clientCoreId = $stateParams['clientCoreId'];
                return `<client client-id="'${clientCoreId}'"></client>`;
            }
        })
            .state('cn.search.client.contactinfo', {
            url: "",
            analytics: 'View Client',
            templateUrl: '/app/components/client/tabs/contact-information.html'
        })
            .state('cn.search.client.names', {
            url: "/names",
            templateUrl: '/app/components/client/tabs/names.html'
        })
            .state('cn.search.client.classcodes', {
            url: "/classcodes",
            templateUrl: '/app/components/client/tabs/class-codes.html'
        })
            .state('cn.search.client.submissions', {
            url: "/submissions",
            templateUrl: '/app/components/client/tabs/submissions.html'
        })
            .state('cn.search.client.edit', {
            url: "/edit"
        });
    }
    function SearchSubmission(provider: ng.ui.IStateProvider) {
        provider.state('cn.search.submissions', {
            url: "/submissions",
            analytics: 'Submission Search'
        })
            .state('cn.search.submission', {
            abstract: true,
            url: "/submission/{submissionId}",
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<submission company-key="'${$stateParams['companyKey']}'" submission-id="'${$stateParams['submissionId']}'"></submission>`;
            }
        })
            .state('cn.search.submission.policyinformation', {
            url: "",
            analytics: 'View Submission',
            templateUrl: '/app/components/submission/tabs/policy-information.html'
        })
            .state('cn.search.submission.clientinformation', {
            url: "/clientinformation",
            templateUrl: '/app/components/submission/tabs/client.html'
        })
            .state('cn.search.submission.notes', {
            url: "/notes",
            templateUrl: '/app/components/submission/tabs/notes.html'
        })
            .state('cn.search.submission.relatedsubmissions', {
            url: "/relatedsubmissions",
            templateUrl: '/app/components/submission/tabs/related-submissions.html'
        })
            .state('cn.search.submission.edit', {
            url: "/edit"
        });
    }

    function Clearance(provider: ng.ui.IStateProvider) {
        provider.state('cn.clearance', {
            url: "/application?clientCoreId&applicationId&copyOf",
            analytics: 'New Application',
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return '<clearance ' +
                    (($stateParams['clientCoreId']) ? `client-id="${$stateParams['clientCoreId']}" ` : '') +
                    (($stateParams['applicationId']) ? `application-id="${$stateParams['applicationId']}" ` : '') +
                    (($stateParams['copyOf']) ? `copy-of="${$stateParams['copyOf']}"` : '') +
                    '></clearance>';
            }
        });
    }
    function Application(provider: ng.ui.IStateProvider) {
        provider.state('cn.viewapplication', {
            url: "/application/{submissionId}",
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<application submission-id="${$stateParams['submissionId']}"></application>`;
            },
            analytics: 'View Application'
        });
    }

    function Administration(provider: ng.ui.IStateProvider) {
        provider.state('cn.administration', {
            url: "/administration",
            template: '<administration></administration>',
            analytics: 'Administration'
        })
            .state('cn.administration.underwritingunits', {
            url: "/underwritingunits",
            template: '<underwriting-units></underwriting-units>',
            analytics: 'Underwriting Units Administration'
        })
            .state('cn.administration.classcodes', {
            url: "/classcodes",
            template: '<administration-class-codes></administration-class-codes>',
            analytics: 'Class Code Administration'
        })
            .state('cn.administration.classcodes.classcode', {
            url: "/{classCode}",
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<class-code code="${$stateParams['classCode']}"></class-code>`;
            }
        })
            .state('cn.administration.companycustomizations', {
            url: "/companycustomizations",
            template: '<company-customizations></company-customizations>',
            analytics: 'Company Customizations Administration'
        })
            .state('cn.administration.companycustomizations.sort', {
            url: "/sort",
            template: '<company-customizations-sort></company-customizations-sort>',
        })
            .state('cn.administration.companycustomizations.customization', {
            url: "/{customization}",
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<company-customization customization="${$stateParams['customization']}"></company-customization>`;
            }
        })
            .state('cn.administration.users', {
            url: "/users",
            template: '<users></users>',
            analytics: 'User Administration'
        })
            .state('cn.administration.users.user', {
            url: "/{userId}",
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<user id="${$stateParams['userId']}"></user>`;
            }
        })
            .state('cn.administration.apssync', {
            url: "/apssync",
            template: '<agency-sync></agency-sync>',
            analytics: 'APS Sync'
        })
            .state('cn.administration.testserver', {
            url: "/tests/server",
            template: '<server-tests></server-tests>',
            analytics: 'Server Tests'
        })
            .state('cn.administration.testvalidation', {
            url: "/tests/validation",
            template: '<validation-tests></validation-tests>',
            analytics: 'Validation Tests'
        })
            .state('cn.administration.testclientsystem', {
            url: "/tests/clientsystem",
            template: '<client-system-tests></client-system-tests>',
            analytics: 'Client System Tests'
        })
            .state('cn.administration.viewsettings', {
            url: "/company/viewsettings",
            template: '<company-settings></company-settings>',
            analytics: 'View Company Settings'
        })
            .state('cn.administration.viewfields', {
            url: "/company/viewfields",
            template: '<company-fields></company-fields>',
            analytics: 'View Submission/Client Fields'
        })
            .state('cn.administration.submissiontables', {
            url: "/submissiontable",
            template: '<submission-tables-administration></submission-tables-administration>',
            analytics: 'Submission Table Administration'
        })
            .state('cn.administration.othercarriers', {
            url: "/othercarriers",
            template: '<other-carriers-administration></other-carriers-administration>',
            analytics: 'Other Carriers Administration'
        })
            .state('cn.administration.policysymbols', {
            url: "/policysymbols",
            template: '<policy-symbols-administration></policy-symbols-administration>',
            analytics: 'Policy Symbols Administration'
        });
    }

    function Tools(provider: ng.ui.IStateProvider) {
        provider.state('cn.tools', {
            url: "/tools",
            template: '<tools></tools>',
            analytics: 'Tools'
        })
            .state('cn.tools.policynumbergenerator', {
            url: "/policynumbergenerator",
            template: '<policy-number-generator-tool></policy-number-generator-tool>',
            analytics: 'Policy Number Generator'
        })
            .state('cn.tools.fullsubmissionsearch', {
            url: "/submissionsearch",
            template: '<advanced-submission-search-tool></advanced-submission-search-tool>',
            analytics: 'Detailed Submission Search'
        })
            .state('cn.tools.classcodes', {
            url: "/classcodes",
            template: '<class-code-viewer-tool></class-code-viewer-tool>',
            analytics: 'Class Codes'
        });
    }

    function Bpm(provider: ng.ui.IStateProvider) {
        provider.state('bpm.submission', {
            url: "/submission/{submissionId}",
            analytics: 'BPM Submission View',
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<submission company-key="'${$stateParams['companyKey']}'" submission-id="'${$stateParams['submissionId']}'" view="bpm"></submission>`;
            }
        });
    }
}