﻿/// <reference path="app.ts" />
module bcs {
    'use strict';
    export class BaseController {
        protected companyKey: string;
        protected AppState: AppState;

        constructor(protected $scope: ng.IScope, protected $state: ng.ui.IStateService, protected UtilService: UtilService) {
            this.companyKey = $state.params['companyKey'];
            this.AppState = UtilService.AppState;
        }
    }

    export interface AppRootScope extends ng.IRootScopeService {
        appVersion: string;
    }

    export interface ValidatedForm extends ng.IFormController {
        ValidationErrors: ValidationResult[];
    }

    export interface CodeItem {
        Code: string;
        Description: string;
    }

    export interface State {
        StateName: string;
        Abbreviation: string;
    }

    export interface ValidationResult {
        Message: string;
        Property: string;
    }
    export interface ServerException {
        ExceptionMessage: string;
        StackTrace: string;
    }

    export interface CompanySummary {
        CompanyNumber: string;
        CompanyName: string;
        Abbreviation: string;
    }
    export interface CompanyConfiguration {
        Summary: CompanySummary;
        Configuration: CompanyConfiguration;
        SystemSettings: any;
    }
    export interface CompanyConfiguration {
        AgentsEnabled: boolean;
        AnalystEnabled: boolean;
        TechnicianEnabled: boolean;
        UsesAllUnderwritersForAllAssignments: boolean;
        FutureEffectiveDateDayCount: number;
        APSAgencyUrl: string;
        UsesCms: boolean;
        AllowClientPrimaryChanges: boolean;
        PolicyNumberRegex: string;
        PolicyNumberGeneratable: boolean;
        SubmissionEnforceAgencyLicensedState: boolean;
        ClientCoreSegments: ClientCoreSegment[];
        SubmissionTypes: SubmissionType[];
        ClientFields: {
            ContactInformation: ClientTypeField;
            FEIN: ClientTypeField;
            SicCode: ClientTypeField;
            NaicsCode: ClientTypeField;
            AccountPremium: ClientTypeField;
            BusinessDescription: ClientTypeField;
            PortfolioInformation: ClientTypeField;
            ClientCoreSegment: ClientTypeField;
        };
    }

    export interface ClientTypeField {
        Enabled: boolean;
        VisibleOnAdd: boolean;
        Required: boolean;
        Label?: string;
    }
    export interface ClientCoreSegment extends CodeItem {
        IsDefault: boolean;
    }
    export interface SubmissionType {
        TypeName: string;
        TypeIsDefault: boolean;
        TypeIsRenewal: boolean;
        SummaryField: SubmissionDisplayField;
        AllowMultipleSubmissions: SubmissionTypeField;
        PolicySymbol: SubmissionTypeField;
        PackagePolicySymbols: SubmissionTypeField;
        UnderwritingUnit: SubmissionTypeField;
        Underwriter: SubmissionTypeField;
        UnderwritingAnalyst: SubmissionTypeField;
        UnderwritingTechnician: SubmissionTypeField;
        EffectiveDate: SubmissionTypeField;
        ExpirationDate: SubmissionTypeField;
        Agency: SubmissionTypeField;
        Agent: SubmissionTypeField;
        PolicyPremium: SubmissionTypeField;
        PriorCarrier: SubmissionTypeField;
        PriorCarrierPremium: SubmissionTypeField;
        AcquiringCarrier: SubmissionTypeField;
        AcquiringCarrierPremium: SubmissionTypeField;
        StatusNotes: SubmissionTypeField;
        ClassCodes: SubmissionTypeField;
    }
    export interface SubmissionTypeField {
        Enabled: boolean;
        VisibleOnAdd: boolean;
        VisibleOnBPM: boolean;
        Required: boolean;
        Label?: string;
    }
    export interface SubmissionDisplayField {
        Header?: string;
        PropertyName?: string;
        DisplayName?: string;
        IsCustomization: boolean;
    }
    export interface SubmissionUnderwritingPersonnelReassignment {
        Submissions: SubmissionId[];
        Underwriter: UnderwritingPersonnel;
        UnderwritingAnalyst: UnderwritingPersonnel;
        UnderwritingTechnician: UnderwritingPersonnel;
    }

    export interface UserSummary {
        Id: number;
        UserName: string;
        DisplayName?: string;
        Active: boolean;
    }
    export interface User extends UserSummary {
        UserType: string;
        DefaultCompany: CodeItem;
        Companies: CodeItem[];
        Roles: string[];
        IsSystemAdministrator: boolean;
        IsReadOnly: boolean;
        LimitingAgency?: AgencySummary;
        ClientCoreSegmentCode?: string;
        Notes?: string;
        SelectedCompany?: CodeItem;
        ChangeSelectedCompany?: (newCompany: CodeItem) => void;
        AdministratorLinks?: AdministrationLink[];
        IsInRole?: (role: string) => boolean;
    }
    export interface UserSaveResult {
        User?: User;
        ValidationErrors?: ValidationResult[];
    }

    export interface Address {
        [property: string]: any;
        Address1: string;
        Address2: string;
        City: string;
        State: string;
        PostalCode: string;
        County: string;
        Country: string;
        Latitude: number;
        Longitude: number;
    }

    export interface AgencySyncResult {
        Succeeded: boolean;
        Message: string;
        Agency: Agency;
        ApsId: string;
    }
    export interface AgencySummary {
        Code: string;
        Name?: string;
        EffectiveDate?: any;
        CancelDate?: any;
        RenewalCancelDate?: any;
        ReferralDate?: any;
        Address?: Address;
        ReferralAgencyCode?: string;

        //Optionally manually populated:
        UnderwritingUnits?: UnderwritingUnit[];
        LicensedStates?: State[];
        Agents?: Agent[];
        AgencyUrl?: string;
    }
    export interface Agency extends AgencySummary {
        Branches: any[];
        Note: string;
        Parent: Agency;
        Children: Agency[];

        Expanded: boolean;
        Expandable: boolean;
        Expand: () => void;
        Dates: any[];
        BranchesAsString: string;
        AgencyReferralUrl: string;
        ParentAgencyUrl: string;
        ChildAgencyUrl: string;
    }
    export interface Agent {
        Agency?: Agency;
        FullName?: string;
        PrimaryPhone?: string;
        SecondaryPhone?: string;
        Fax?: string;
        EmailAddress?: string;
        RetirementDate?: any;

        AgencyUrl?: string;
        Contacts?: any[];
        Type?: string;
    }

    export interface Client {
        Company?: CompanySummary;
        ClientCoreId?: number;
        ClientCoreSegment?: ClientCoreSegment;
        CompanyNumber?: string;
        PrimaryName: ClientName;
        AdditionalNames: ClientName[];
        ExperianId?: number;
        FEIN?: string;

        Portfolio: {
            Id?: number;
            Name?: string;
            ClientCount?: number;
        };

        CompanyCustomizations: CompanyCustomizationValue[];
        CompanyCustomizationBindings: CompanyCustomizationValueBinding[];

        Contacts: ClientContact[];
        Addresses: ClientAddress[];
    }
    export interface ClientName {
        [property: string]: any;
        SequenceNumber?: number;
        NameType: string;
        BusinessName?: string;
        EffectiveDate?: Date;
    }
    export interface ClientContact {
        SequenceNumber?: number;
        Value: string;
        ContactType: string;
    }
    export interface ClientAddress extends Address {
        IsPrimary: boolean;
        AddressTypes: string[];
        ClientCoreAddressId?: number;

        IsInvalid: boolean;
        PostalCodeSearching: boolean;
    }
    export interface ClientSearchResult extends Client {
        MatchingNames: ClientName[];
        MatchingAddress: ClientAddress;
        MatchingAddresses: ClientAddress[];
        PhoneNumber: string;
    }
    export interface ClientSearchResults {
        Matches: ClientSearchResult[];
        CriteriaNotSpecific: boolean;
        Failures: ClientSearchResultFailure[];
    }
    export interface ClientSearchResultFailure {
        Status: string;
        Criteria: string;
        StatusMessage: string;
    }
    export interface CrossCompanyClientSearchResult {
        Company: CompanySummary;
        ClientId: number;
    }
    export interface ClientGetResult {
        Client?: Client;
        Failure?: any;
    }
    export interface ClientSaveResult {
        Client?: Client;
        ValidationErrors?: ValidationResult[];
    }

    export interface ClassCode {
        CodeValue?: string;
        GLCodeValue?: string;
        HazardGrades?: CodeItem[];
        CompanyCustomizations?: CompanyCustomizationValue[];
        CompanyCustomizationBindings?: CompanyCustomizationValueBinding[];
        IsDeleted?: boolean;
    }

    export interface ClientClassCode extends ClassCode {
        IsNew: boolean;
        Submissions: Submission[];
        VerticallyOrderedHazardGrades: any[];
    }

    export interface SubmissionId {
        Id?: string;
        SubmissionNumber?: number;
        SequenceNumber?: number;
    }

    export interface SubmissionSummary extends SubmissionId {
        Company?: CompanySummary;
        SubmissionType?: string;
        ClientCoreId?: number;
        ClientCorePortfolioId?: number;
        InsuredName?: string;
        InsuredDBA?: string;
        PolicyNumber?: string;
        PolicyMod?: any;
        SubmittedDate?: any;
        EffectiveDate?: any;
        ExpirationDate?: any;
        EffectivePeriod?: string;
    }
    export interface Submission extends SubmissionSummary, SubmissionStatusUpdate {
        [property: string]: any;
        ViewState?: {
            GeneratePolicyNumber: boolean;
            ReturnFullSubmission: boolean;
            Underwriters?: UnderwritingPersonnel[];
            UnderwritingAnalysts?: UnderwritingPersonnel[];
            UnderwritingTechnicians?: UnderwritingPersonnel[];
        };
        IsDeleted?: boolean;
        DeletedBy?: string;
        DeletedDate?: any;
        Agency?: AgencySummary;
        Agent?: Agent;
        AgentUnspecifiedReason?: string;
        PriorCarrier?: any;
        PriorCarrierPremium?: number;
        AcquiringCarrier?: any;
        AcquiringCarrierPremium?: number;
        Notes?: SubmissionNote[];
        CreatedWithOFACHit?: boolean;
        PolicyPremium?: number;
        PolicySymbol?: PolicySymbol;
        PackagePolicySymbols?: PolicySymbol[];
        UnderwritingUnit?: UnderwritingUnit;
        Underwriter?: UnderwritingPersonnel;
        UnderwritingAnalyst?: UnderwritingPersonnel;
        UnderwritingTechnician?: UnderwritingPersonnel;
        CompanyCustomizations?: CompanyCustomizationValue[];
        CompanyCustomizationBindings?: CompanyCustomizationValueBinding[];
        ClassCodes?: any;
        Addresses?: SubmissionAddress[];
        AdditionalNames?: SubmissionName[];
        DuplicateOfSubmission?: SubmissionSummary;
        AssociatedSubmissions?: SubmissionSummary[];

        StepSummary?: string;
    }
    export interface SubmissionAddress extends Address {
        ClientCoreAddressId?: number;
    }
    export interface SubmissionName extends ClientName {
        
    }
    export interface SubmissionNote {
        Id?: number;
        Text: string;
        Author?: string;
        Date?: any;
    }
    export interface SubmissionStatus {
        Code?: string;
        IsDefault: boolean;
        Reasons: SubmissionStatusReason[];
    }
    export interface SubmissionStatusReason {
        Code?: string;
        IsActive: boolean;
    }
    export interface SubmissionStatusHistory {
        Id: number;
        Date: any;
        Status: string;
        ChangeUser: string;
        ChangeDate: any;
        IsActive: boolean;
    }
    export interface SubmissionStatusUpdate {
        Id?: string;
        StatusAsObject?: SubmissionStatus;
        Status?: string;
        StatusReason?: string;
        StatusNotes?: string;
        StatusDate?: any;
        StatusHistory?: SubmissionStatusHistory[];
        RevertStatusTo?: SubmissionStatusHistory;
    }
    export interface SubmissionSaveResult {
        Submission?: Submission;
        ValidationErrors?: ValidationResult[];
    }

    export interface NumberControl {
        Id: number;
        Type: string;
        CurrentNumber: number;
    }

    export interface PolicySymbol extends CodeItem {
        RetirementDate?: any;
        IsPackageOption: boolean;
        IsPrimaryOption: boolean;
        NumberControlId?: number;
        PolicyNumberPrefix: string;
    }

    export interface UnderwritingUnit extends CodeItem {
        Line: string;

        Underwriters: UnderwritingPersonnel[];
        Analysts: UnderwritingPersonnel[];
        Technicians: UnderwritingPersonnel[];
    }
    export interface UnderwritingPersonnel {
        ApsId?: string;
        FullName?: string;
        UserName?: string;
        Initials?: string;
        RetirementDate?: any;

        IsPrimary: boolean;
        Assignment: string;
    }

    export interface CompanyCustomizationOption extends CodeItem {
        ExpirationDate?: any;
    }
    export interface CompanyCustomization {
        Description: string;
        Usage: string;
        DataType: string;
        Required: boolean;
        ReadOnly: boolean;
        VisibleOnAdd: boolean;
        VisibleOnBPM: boolean;
        ExcludeValueOnCopy: boolean;
        ExpirationDate: any;
        Options: CompanyCustomizationOption[];
        DefaultOption: CompanyCustomizationOption;
    }
    export interface CompanyCustomizationValue {
        Customization: string;
        TextValue: string;
        CodeValue: CompanyCustomizationOption;
    }
    export interface CompanyCustomizationValueBinding {
        Customization: CompanyCustomization;
        Value: CompanyCustomizationValue;
    }
}