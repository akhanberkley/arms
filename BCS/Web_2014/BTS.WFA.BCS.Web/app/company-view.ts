﻿/// <reference path="app.ts" />
module bcs {
    'use strict';
    app.directive('companyView',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/company-view.html',
            replace: true,
            scope: {
                companyKey: '@'
            },
            bindToController: true,
            controller: CompanyViewController,
            controllerAs: 'vm'
        };
    });

    class CompanyViewController {
        companyKey: string;
        User: User;
        Links: HeaderLink[];

        constructor(private $state: ng.ui.IStateService, $http: ng.IHttpService, UserService: UserService, UtilService: UtilService) {
            UserService.UserPromise.then((user) => {
                this.User = user;

                this.Links = [];
                this.Links.push(new HeaderLink('Search', 'cn.search.clients',() => UtilService.StateIsSearch($state.current.name)));
                if (user.LimitingAgency == null) {
                    this.Links.push(new HeaderLink('Tools', 'cn.tools',() => $state.includes('cn.tools')));
                }
                if (user.AdministratorLinks.length > 0 || user.IsSystemAdministrator) {
                    this.Links.push(new HeaderLink('Administration', 'cn.administration',() => $state.includes('cn.administration')));
                }
            });
        }

        ChangeCompany() {
            this.User.ChangeSelectedCompany(this.User.SelectedCompany);

            var newRoute = this.$state.current.name;
            if (newRoute.indexOf('cn.search.agency') >= 0) {
                newRoute = 'cn.search.agencies';
            } else if (newRoute.indexOf('cn.search.client') >= 0) {
                newRoute = 'cn.search.clients';
            } else if (newRoute.indexOf('cn.search.submission') >= 0 || newRoute.indexOf('cn.viewapplication') >= 0) {
                newRoute = 'cn.search.submissions';
            } else if (newRoute.indexOf('cn.clearance') >= 0) {
                newRoute = 'cn.search.clients';
            }

            this.$state.go(newRoute, { companyKey: this.User.SelectedCompany.Code.toLowerCase() });
        }
    }

    class HeaderLink {
        constructor(private Title: string, private RouteUrl: string, private IsActive?: () => boolean) { }
    }
}