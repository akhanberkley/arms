﻿/// <reference path="app.ts" />
module bcs {
    'use strict';
    app.directive('bpmView',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/bpm-view.html',
            replace: true,
            scope: {
                companyKey: '@'
            },
            bindToController: true,
            controller: BPMViewController,
            controllerAs: 'vm'
        };
    });

    class BPMViewController {
        companyKey: string;
        User: User;

        constructor(private $state: ng.ui.IStateService, $http: ng.IHttpService, UserService: UserService, UtilService: UtilService) {
            UserService.UserPromise.then((user) => {
                this.User = user;
            });
        }
    }
} 