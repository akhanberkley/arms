﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('clearanceNotesModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/clearance/notes-modal.html',
            replace: true,
            scope: {
                Note: '=note',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: ClearanceNotesController,
            controllerAs: 'vm'
        };
    });

    class ClearanceNotesController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;
        Note: string;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);

            UtilService.RaiseFocusTrigger('application-notes');
        }

        Save() {
            this.ModalInstance.close(this.Note);
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}