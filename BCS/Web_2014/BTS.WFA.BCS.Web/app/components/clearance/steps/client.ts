﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('clearanceClient',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/clearance/steps/client.html',
            replace: true,
            scope: {
                Client: '=client',
                CompanySettings: '=companySettings'
            },
            bindToController: true,
            controller: ClearanceClientController,
            controllerAs: 'vm'
        }
    });

    class ClearanceClientController extends BaseController {
        CompanySettings: CompanyConfiguration;
        Client: Client;
        ShowSegment: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, UserService: UserService, private SearchService: SearchService) {
            super($scope, $state, UtilService);

            UserService.UserPromise.then((user) => {
                this.ShowSegment = (user.IsInRole('Action - Set Client Segment') && this.CompanySettings.ClientFields.ClientCoreSegment.Enabled && this.Client.ClientCoreId == null);
            });
        }

        SicCodeSearch = (term: string) => {
            return this.SearchService.AutoComplete.SicCodes(term);
        }
        NaicsCodeSearch = (term: string) => {
            return this.SearchService.AutoComplete.NaicsCodes(term);
        }
    }
}