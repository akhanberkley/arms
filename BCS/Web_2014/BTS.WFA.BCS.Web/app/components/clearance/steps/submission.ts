﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('clearanceSubmission',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/clearance/steps/submission.html',
            replace: true,
            scope: {
                SubmissionWorkflow: '=submissionWorkflow',
                Submission: '=submission',
            },
            bindToController: true,
            controller: ClearanceSubmissionController,
            controllerAs: 'vm'
        }
    });

    class ClearanceSubmissionController extends BaseController {
        EditEffectiveDates: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private SearchService: SearchService, private AgencyService: AgencyService) {
            super($scope, $state, UtilService);
            this.EditEffectiveDates = false;
        }
    }
}