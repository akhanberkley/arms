﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('clearanceDuplicates',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/clearance/steps/duplicates.html',
            replace: true,
            scope: {
                SubmissionWorkflow: '=submissionWorkflow'
            },
            bindToController: true,
            controller: ClearanceDuplicatesController,
            controllerAs: 'vm'
        }
    });

    class ClearanceDuplicatesController extends BaseController {
        SubmissionWorkflow: SubmissionWorkflow;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private SearchService: SearchService, private AgencyService: AgencyService) {
            super($scope, $state, UtilService);
        }
    }


}