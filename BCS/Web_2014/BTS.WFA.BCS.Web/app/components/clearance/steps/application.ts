﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('clearanceApplication',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/clearance/steps/application.html',
            replace: true,
            scope: {
                SubmissionWorkflow: '=submissionWorkflow',
                client: '=',
            },
            bindToController: true,
            controller: ClearanceApplicationController,
            controllerAs: 'vm'
        }
    });

    class ClearanceApplicationController extends BaseController {
        client: Client;
        SubmissionWorkflow: SubmissionWorkflow;
        Submission: Submission;
        ClientAddresses: ClientAddress[];
        DoingBusinessAsNames: ClientName[];
        AdditionalNames: ClientName[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private SearchService: SearchService, private AgencyService: AgencyService) {
            super($scope, $state, UtilService);

            $scope.$watch(() => this.SubmissionWorkflow,(submissionWorkflow: SubmissionWorkflow) => {
                if (submissionWorkflow) {
                    this.Submission = submissionWorkflow.Submissions[0];
                }
            });
            $scope.$watch(() => this.client,(client: Client) => {
                if (client) {
                    this.ClientAddresses = client.Addresses;
                    this.DoingBusinessAsNames = client.AdditionalNames.filter((n) => n.NameType == 'DBA');
                    this.AdditionalNames = client.AdditionalNames.filter((n) => n.NameType != 'DBA');
                }
            });
        }
    }
}