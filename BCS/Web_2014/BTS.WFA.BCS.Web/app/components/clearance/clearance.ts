﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('clearance',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/clearance/clearance.html',
            replace: true,
            scope: {
                clientId: '@',
                applicationId: '@',
                copyOfId: '@copyOf'
            },
            bindToController: true,
            controller: ClearanceController,
            controllerAs: 'vm'
        }
    });

    class ClearanceController extends BaseController {
        CompanySettings: CompanyConfiguration;
        Client: Client;
        SubmissionWorkflow: SubmissionWorkflow;
        Submission: Submission;
        clientId: number;
        applicationId: string;
        copyOfId: string;

        private loadingPanelDelayMs = 300;
        private clientWasViewed = false;
        private applicationWasViewed = false;
        private allowStateTransition = false;
        private applicationSubmissions = <SubmissionSummary[]>[];
        ClientGetFailed = false;
        UpdatingApplication = false;
        CopyingSubmission = false;
        DisplayedClientId = <number>null;
        NonEditableSubmissionSummaries = <any[]>[];
        CurrentStep = '';
        SubmissionIndex = 0;
        HasPotentialDuplicates = false;
        Saving = false;
        SavingMessage = '';
        Forms = { Clearance: <ValidatedForm>{} };
        ValidationResults = { Client: <ValidationResult[]>[], Application: <ValidationResult[]>[], Submissions: <ValidationResult[][]>[] };

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, DomainService: DomainService, $modalStack: ng.ui.bootstrap.IModalStackService, private ClientService: ClientService, private SubmissionService: SubmissionService,
            private $filter: ng.IFilterService, private $window: ng.IWindowService, private $modal: ng.ui.bootstrap.IModalService, private $q: ng.IQService, SubmissionWorkflowService: SubmissionWorkflowService, private CacheService: CacheService, private AlertService: AlertService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('New Application');
            this.AppState.PageGenerating = true;
            var pageLoadTasks = <ng.IPromise<any>[]>[];

            pageLoadTasks.push(DomainService.CompanyInformation(this.companyKey).then((companySettings) => { this.CompanySettings = companySettings; }));

            pageLoadTasks.push(this.setClient(this.clientId));

            $q.all(pageLoadTasks).then(() => {
                var submissionPromise = <ng.IPromise<Submission[]>>new $q((resolve, reject) => {
                    if (this.copyOfId != null) {
                        SubmissionService.Get(this.companyKey, this.copyOfId).then((submission) => {
                            this.CopyingSubmission = true;
                            resolve([submission]);
                        });
                    } else if (this.applicationId != null) {
                        SubmissionService.GetApplicationSubmissions(this.companyKey, this.applicationId).then((submissions) => {
                            this.UpdatingApplication = true;
                            resolve(submissions || []);
                        });
                    } else {
                        resolve([]);
                    }
                });
                
                //At this point we have the client and submissions (if there were any)
                submissionPromise.then((submissions) => {
                    var submission = (submissions.length > 0) ? submissions[0] : null;
                    var workflowType = (this.UpdatingApplication) ? SubmissionWorkflowType.ExistingApplication : SubmissionWorkflowType.NewApplication;

                    SubmissionWorkflowService.GetWorkflow(this.companyKey, workflowType, submission).then((w) => {
                        if (submission != null) {
                            if (this.applicationId) {
                                for (var i = 0; i < submissions.length; i++) {
                                    var s = submissions[i];
                                    this.NonEditableSubmissionSummaries.push(this.getSubmissionStepSummary(i + 1, w, s));
                                    this.applicationSubmissions.push({ Id: s.Id });
                                }
                            }

                            if (w.InsuredDBA) {
                                var matchingDbas = this.Client.AdditionalNames.filter((name) => name.BusinessName == w.InsuredDBA);
                                if (matchingDbas.length > 0) {
                                    w.DbaAsObject = matchingDbas[0];
                                }
                            }

                            w.CleanSubmissionForCopy(submission, !this.CopyingSubmission);
                            this.refreshDupCheckMatches(w);
                            pageLoadTasks.push(w.SelectAgencyFromExistingSubmission(submission));
                        }

                        this.SubmissionWorkflow = w;
                        this.Submission = w.Submissions[0];
                        this.ValidationResults.Submissions.push([]);

                        //If this was an existing application, we need to wait until the agency is fully loaded, otherwise it'll mess up the dup check step
                        $q.all(pageLoadTasks).then(() => {
                            var initialStep = <ng.IPromise<any>>null;
                            if (this.Client.ClientCoreId == null) {
                                initialStep = this.TransitionToStep('Client');
                            } else if (this.UpdatingApplication) {
                                initialStep = this.TransitionToStep('Submission', 0);
                            } else {
                                initialStep = this.TransitionToStep('Application');
                            }

                            initialStep.then(() => {
                                this.syncAllSubmissionSummaries();
                                this.AppState.PageGenerating = false;
                            });
                        });
                    });
                });
            });

            $scope.$on('$stateChangeStart',(event, toState, toParams, fromState, fromParams) => {
                if (!this.allowStateTransition) {
                    event.preventDefault();

                    var modal = this.$modal.open({
                        template: '<unsaved-changes-modal modal="modal"></unsaved-changes-modal>',
                        size: 'sm',
                        controller: ($scope: ng.IScope) => {
                            $scope['modal'] = modal;
                        }
                    });
                    modal.result.then(() => {
                        this.allowStateTransition = true;
                        $modalStack.dismissAll();
                        $state.go(toState, toParams);
                    });
                }
            });
        }

        private createNewClient() {
            return new this.$q((resolve, reject) => {
                this.ClientService.GetNew(this.companyKey).then((client) => {
                    var searchCriteria = this.CacheService.Session.get('ClientSearchCriteria');
                    if (searchCriteria != null) {
                        client.PrimaryName.BusinessName = this.UtilService.ToTitleCase(searchCriteria.BusinessName);
                        client.FEIN = searchCriteria.FEIN;
                        client.Addresses[0].Address1 = this.UtilService.ToTitleCase(searchCriteria.Address);
                        client.Addresses[0].City = this.UtilService.ToTitleCase(searchCriteria.City);
                        client.Addresses[0].State = searchCriteria.State;
                        client.Addresses[0].PostalCode = searchCriteria.PostalCode;
                        if (searchCriteria.PhoneNumber) {
                            client.Contacts.push({ ContactType: 'Phone', Value: searchCriteria.PhoneNumber });
                        }
                    }

                    var defaultSegment = this.CompanySettings.ClientCoreSegments.filter((s) => s.IsDefault);
                    client.ClientCoreSegment = (defaultSegment.length > 0) ? defaultSegment[0] : null;

                    resolve(client);
                });
            });
        }
        private setClient(clientId: number) {
            return new this.$q((resolve, reject) => {
                if (clientId != null) {
                    this.ClientService.Get(this.companyKey, clientId).then((result) => {
                        var client = <Client>null;
                        if (result.Failure) {
                            this.ClientGetFailed = true;
                        } else if (result.Client != null) {
                            client = result.Client;
                            var clientName = (client.PrimaryName != null) ? client.PrimaryName.BusinessName : client.ClientCoreId;
                            this.DisplayedClientId = client.ClientCoreId;
                            this.UtilService.SetPageTitle('New Application (' + clientName + ')');
                        }

                        if (client) {
                            resolve(client);
                        } else {
                            this.createNewClient().then((newClient) => {
                                resolve(newClient);
                            });
                        }
                    });
                } else {
                    this.createNewClient().then((newClient) => {
                        resolve(newClient);
                    });
                }
            }).then((client) => {
                this.CacheService.Session.put('ClientSearchCriteria', {});//Clear client search criteria
                this.Client = client;
            });
        }


        private getSubmissionStepSummary(index: number, w: SubmissionWorkflow, submission: Submission) {
            var summaryItem = <any>null;
            if (w.SubmissionType != null && w.SubmissionType.SummaryField != null) {
                var f = w.SubmissionType.SummaryField;
                if (!f.IsCustomization) {
                    if (f.PropertyName == "PackagePolicySymbols") {
                        summaryItem = submission.PackagePolicySymbols.map((ps) => ps.Code).join(', ');
                    } else {
                        summaryItem = submission[f.PropertyName];
                    }
                } else {
                    //Existing submissions won't have bindings, so we'll build them for this first
                    if (!submission.CompanyCustomizationBindings) {
                        w.SetCustomizations(submission, false);
                    }
                    for (var i = 0; i < submission.CompanyCustomizationBindings.length; i++) {
                        var customization = submission.CompanyCustomizationBindings[i];
                        if (customization.Customization.Description == f.PropertyName) {
                            if (customization.Value.CodeValue != null) {
                                summaryItem = customization.Value.CodeValue;
                            } else {
                                summaryItem = customization.Value.TextValue;
                            }
                            break;
                        }
                    }
                }
            }

            if (!summaryItem) {
                summaryItem = 'Submission';
                if (w.SubmissionType.AllowMultipleSubmissions.VisibleOnAdd) {
                    summaryItem += ' #' + index;
                }
            }

            return (summaryItem);
        }
        private refreshVisibleForm() {
            var validations = <ValidationResult[]>[];
            switch (this.CurrentStep) {
                case 'Client':
                    validations = this.ValidationResults.Client;
                    break;
                case 'Application':
                    validations = this.ValidationResults.Application;
                    break;
                case 'Duplicates':
                    break;
                default:
                    if (this.ValidationResults.Submissions.length > this.SubmissionIndex) {
                        validations = this.ValidationResults.Submissions[this.SubmissionIndex];
                    }
                    break;
            }

            this.UtilService.SetValidationResults(this.Forms.Clearance, validations);
        }
        private refreshDupCheckMatches(w: SubmissionWorkflow) {
            var state = (this.Client.Addresses.length > 0) ? this.Client.Addresses[0].State : null;
            w.RefreshSimilarClientIds(this.Client, state);

            if (w.Submissions[0].EffectiveDate) {
                w.RefreshDuplicateMatches();
            }
        }

        CurrentStepIsSubmission() {
            return (this.CurrentStep.indexOf('Submission') == 0);
        }

        OpenNotes() {
            var modal = this.$modal.open({
                template: '<clearance-notes-modal note="note" modal="modal"></clearance-notes-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['note'] = this.SubmissionWorkflow.ClearanceNotes;
                    $scope['modal'] = modal;
                }
            });

            modal.result.then((note) => {
                this.SubmissionWorkflow.ClearanceNotes = note;
            });
        }

        RemoveSubmission(index: number) {
            this.SubmissionWorkflow.Submissions.splice(index, 1);
            this.ValidationResults.Submissions.splice(index, 1);

            this.syncAllSubmissionSummaries();
            if (this.CurrentStep.indexOf('Submission') >= 0) {
                if (index == this.SubmissionIndex) {
                    this.syncViewedSubmissionProperties(0);
                } else if (index < this.SubmissionIndex) {
                    this.syncViewedSubmissionProperties(this.SubmissionIndex - 1);
                }
            }

            this.refreshVisibleForm();
        }
        Cancel() {
            this.$state.go('cn.search.clients');
        }
        CanContinue() {
            if (this.CurrentStep == 'Duplicates') {
                return (this.SubmissionWorkflow.NoDuplicatesApplicable || this.SubmissionWorkflow.Duplicate != null);
            } else {
                return true;
            }
        }
        Continue() {
            switch (this.CurrentStep) {
                case 'Client':
                    this.TransitionToStep('Application');
                    break;
                case 'Application':
                case 'Duplicates':
                    this.TransitionToStep('Submission', 0);
                    break;
            }
        }

        private syncAllSubmissionSummaries() {
            var w = this.SubmissionWorkflow;
            for (var i = 0; i < w.Submissions.length; i++) {
                var s = w.Submissions[i];
                s.StepSummary = this.getSubmissionStepSummary(1 + this.applicationSubmissions.length + i, w, s);
            }
        }
        private syncViewedSubmissionProperties(index: number) {
            this.CurrentStep = 'Submission' + index;
            this.Submission = this.SubmissionWorkflow.Submissions[index];
            this.SubmissionIndex = index;
        }
        TransitionToStep(requestedStep: string, requestedStepIndex?: number) {
            var transitionPromise = new this.$q((resolveStep, rejectStep) => {
                var w = this.SubmissionWorkflow;
                var vr = this.ValidationResults;

                //Update insured name
                var nameChanged = (w.SimilarClientIdsPromise == null || w.InsuredName != this.Client.PrimaryName.BusinessName);
                var setupClientDetails = () => {
                    w.InsuredName = this.Client.PrimaryName.BusinessName;
                    if (w.DbaAsObject != null && w.DbaAsObject.NameType != 'DBA') {
                        w.DbaAsObject = null;
                    }

                    var additionalNames = this.Client.AdditionalNames.filter((n) => n.NameType != 'DBA');
                    for (var i = w.AdditionalNames.length - 1; i >= 0; i--) {
                        if (additionalNames.indexOf(w.AdditionalNames[i]) < 0) {
                            w.AdditionalNames.splice(i, 1);
                        }
                    }
                };

                //Handle moving to next step
                var originalStep = this.CurrentStep;
                var originalStepPromise = new this.$q((resolve, reject) => {
                    if (originalStep == '') {
                        setupClientDetails();
                        resolve();//if initial page load, ignore this current step stuff
                    } else {
                        switch (originalStep) {
                            case 'Client':
                                setupClientDetails();
                                this.ClientService.Validate(this.companyKey, this.Client).then(
                                    () => {
                                        if (nameChanged) {
                                            w.Cleared = false;
                                        }

                                        vr.Client = [];
                                        resolve();
                                    },(results) => {
                                        requestedStep = 'Client';

                                        vr.Client = results;
                                        resolve();
                                    });
                                break;
                            case 'Application':
                                //Only check validations if they're proceeding passed this step, or they're to the point where it's been cleared
                                if (!(requestedStep == 'Client' && !w.Cleared)) {
                                    vr.Application = [];
                                    if (w.SubmissionType.EffectiveDate.VisibleOnAdd && w.SubmissionType.EffectiveDate.Required && w.Submissions[0].EffectiveDate == null) {
                                        vr.Application.push({ Property: 'EffectiveDate', Message: 'Effective Date is required' });
                                    }
                                    if (w.SubmissionType.ExpirationDate.VisibleOnAdd && w.SubmissionType.ExpirationDate.Required && w.Submissions[0].ExpirationDate == null) {
                                        vr.Application.push({ Property: 'ExpirationDate', Message: 'Expiration Date is required' });
                                    }
                                    if (w.SubmissionType.Agency.VisibleOnAdd) {
                                        if (w.Agency == null) {
                                            if (w.SubmissionType.Agency.Required) {
                                                vr.Application.push({ Property: 'Agency', Message: 'Agency is required' });
                                            }
                                        } else if (w.Submissions[0].EffectiveDate != null) {
                                            if (w.Submissions[0].EffectiveDate < w.Agency.EffectiveDate) {
                                                vr.Application.push({ Property: 'Agency', Message: 'Application Effective Date is prior to Agency Effective Date (' + this.$filter('date')(w.Agency.EffectiveDate, 'MM/dd/yyyy') + ')' });
                                            } else {
                                                if (w.SubmissionType.TypeIsRenewal) {
                                                    var cancelDate = (w.Agency.RenewalCancelDate != null) ? w.Agency.RenewalCancelDate : w.Agency.CancelDate;
                                                    if (cancelDate != null && w.Submissions[0].EffectiveDate > cancelDate) {
                                                        vr.Application.push({ Property: 'Agency', Message: 'Application Effective Date is after Agency Renewal Cancel Date (' + this.$filter('date')(cancelDate, 'MM/dd/yyyy') + ')' });
                                                    }
                                                } else if (w.Agency.CancelDate != null && w.Submissions[0].EffectiveDate > w.Agency.CancelDate) {
                                                    vr.Application.push({ Property: 'Agency', Message: 'Application Effective Date is after Agency Cancel Date (' + this.$filter('date')(w.Agency.CancelDate, 'MM/dd/yyyy') + ')' });
                                                }
                                            }
                                        }
                                    }
                                    if (w.SubmissionType.Agent.VisibleOnAdd && w.SubmissionType.Agent.Required && w.Agent == null) {
                                        vr.Application.push({ Property: 'Agent', Message: 'Agent is required' });
                                    }
                                    if (w.Addresses.length == 0) {//This is always on for now
                                        vr.Application.push({ Property: 'Addresses', Message: 'An address is required' });
                                    }

                                    if (vr.Application.length > 0 && requestedStep == 'Submission') {
                                        requestedStep = 'Application';
                                    }
                                }

                                resolve();
                                break;
                            case 'Duplicates':
                                var resolveDuplicateStep = (clear: boolean) => {
                                    if (clear) {
                                        w.Clear();
                                    }
                                    resolve();
                                };
                                //Selected a duplicate (or n/a)
                                if (requestedStep == 'Submission' && requestedStepIndex == 0 && !w.Cleared) {
                                    //If this is a new submission, we'll copy the selected duplicate's info
                                    if (!this.UpdatingApplication) {
                                        if (w.Duplicate != null) {
                                            if (w.Duplicate.Company.Abbreviation == this.companyKey.toUpperCase()) {
                                                this.SubmissionService.GetApplicationSubmissions(this.companyKey, w.Duplicate.Id).then((submissions) => {
                                                    var newSubmissions = <Submission[]>[];
                                                    angular.forEach(submissions,(s) => {
                                                        var newSubmission = angular.copy(w.Submissions[0]);

                                                        w.CopyPolicyDetails(s, newSubmission);
                                                        newSubmission.DuplicateOfSubmission = { Id: s.Id };

                                                        newSubmissions.push(newSubmission);
                                                    });

                                                    w.Submissions = newSubmissions;
                                                    this.syncAllSubmissionSummaries();
                                                    resolveDuplicateStep(true);
                                                });
                                            } else {
                                                angular.forEach(w.Submissions,(s) => {
                                                    s.DuplicateOfSubmission = { Id: w.Duplicate.Id, Company: w.Duplicate.Company };
                                                });

                                                resolveDuplicateStep(true);
                                            }
                                        } else {
                                            angular.forEach(w.Submissions,(s) => {
                                                s.DuplicateOfSubmission = null;
                                            });
                                            resolveDuplicateStep(true);
                                        }
                                    } else {
                                        resolveDuplicateStep(true);
                                    }
                                } else {
                                    resolveDuplicateStep(false);
                                }
                                break;
                            default:
                                this.syncAllSubmissionSummaries();
                                resolve();
                                break;
                        }
                    }
                });

                originalStepPromise.then(() => {
                    //Update duplicates if name changed
                    if (nameChanged) {
                        this.refreshDupCheckMatches(w);
                    }

                    //Re-route to Duplicates step if they're trying to view a submission and we haven't cleared (or recleared) yet
                    var dupCheckPromise = new this.$q((resolve, reject) => {
                        if (!w.Cleared && (requestedStep == 'Submission' || requestedStep == 'Duplicates')) {
                            w.DuplicateMatchesPromise.then(() => {
                                this.HasPotentialDuplicates = (w.DuplicateMatches.length > 0);
                                if (this.HasPotentialDuplicates && w.Duplicate == null) {
                                    requestedStep = 'Duplicates';
                                } else if (requestedStep == 'Submission') {
                                    w.Clear();
                                }

                                resolve();
                            });
                        } else {
                            resolve();
                        }
                    });
                
                    //Move on to next step
                    dupCheckPromise.then(() => {
                        this.CurrentStep = requestedStep;
                        switch (this.CurrentStep) {
                            case 'Client':
                                this.clientWasViewed = true;
                                this.UtilService.RaiseFocusTrigger('BusinessName');
                                break;
                            case 'Application':
                                this.UtilService.RaiseFocusTrigger('SubmissionType');
                                if (!this.applicationWasViewed) {
                                    var newDbas = this.Client.AdditionalNames.filter((name) => { return name.NameType == 'DBA' && name.SequenceNumber == null; });
                                    if (newDbas.length > 0) {
                                        w.DbaAsObject = newDbas[0];
                                    }

                                    if (!this.UpdatingApplication && !this.CopyingSubmission) {
                                        for (var i = 0; i < this.Client.Addresses.length; i++) {
                                            w.Addresses.push(this.Client.Addresses[i]);
                                        }
                                    }

                                    this.applicationWasViewed = true;
                                }
                                break;
                            case 'Duplicates':
                                break;
                            default:
                                if (requestedStepIndex == w.Submissions.length) {
                                    var newSubmission = w.AddNewSubmission(w.Submissions[0]);
                                    vr.Submissions.push([]);
                                    this.syncAllSubmissionSummaries();
                                }

                                this.syncViewedSubmissionProperties(requestedStepIndex);
                                this.UtilService.RaiseFocusTrigger('btnAddClassCode');
                                break;
                        }

                        this.$window.scrollTo(0, 0);
                        this.refreshVisibleForm();
                        resolveStep();
                    });
                });
            });

            this.UtilService.SetLoadingFlagDelayed(transitionPromise, this.loadingPanelDelayMs);
            return transitionPromise;
        }

        private hasValidationResults() {
            if (this.ValidationResults.Client.length > 0 || this.ValidationResults.Application.length > 0) {
                return true;
            } else {
                for (var i = 0; i < this.ValidationResults.Submissions.length; i++) {
                    if (this.ValidationResults.Submissions[i].length > 0) {
                        return true;
                    }
                }
            }
            return false;
        }
        private saveSubmissions(submissionsComplete: ng.IQResolveReject<Submission>, submissions: Submission[], index: number) {
            var isLastSubmission = (index == submissions.length - 1);
            var submission = submissions[index];

            submission.ClientCoreId = this.Client.ClientCoreId;
            submission.ClientCorePortfolioId = this.Client.Portfolio.Id;
            if (isLastSubmission) {
                submission.AssociatedSubmissions = this.applicationSubmissions;
            }

            this.SubmissionService.Save(this.companyKey, null, submission, this.SubmissionWorkflow).then((submissionResults) => {
                if (submissionResults.ValidationErrors) {
                    this.ValidationResults.Submissions[index] = submissionResults.ValidationErrors;
                    submissionsComplete(null);
                } else if (isLastSubmission) {
                    submissionsComplete(submissionResults.Submission);
                } else {
                    this.applicationSubmissions.push({ Id: submissionResults.Submission.Id });
                    this.saveSubmissions(submissionsComplete, submissions, index + 1);
                }
            });
        }
        Submit() {
            this.syncAllSubmissionSummaries();

            var completionPromise = new this.$q((resolveCompletion, rejectCompletion) => {
                var w = this.SubmissionWorkflow;

                var validationPromises = <ng.IPromise<any>[]>[];
                validationPromises.push(this.ClientService.Validate(this.companyKey, this.Client).then(
                    () => { this.ValidationResults.Client = []; },
                    (validationResults) => { this.ValidationResults.Client = validationResults; }));
                for (var i = 0; i < w.Submissions.length; i++) {
                    //the following call is threaded, so var i could be something different by the time of callback
                    ((curIndex: number) => {
                        validationPromises.push(this.SubmissionService.Validate(this.companyKey, null, w.Submissions[curIndex], w).then(
                            () => { this.ValidationResults.Submissions[curIndex] = []; },
                            (validationResults) => { this.ValidationResults.Submissions[curIndex] = validationResults; }));
                    })(i);
                }

                this.$q.all(validationPromises).then(() => {
                    this.Saving = true;
                    this.SavingMessage = ((this.Client.ClientCoreId == null) ? 'Creating' : 'Saving') + ' Client';

                    var clientSavePromise = new this.$q((resolve, reject) => {
                        if (!this.clientWasViewed || this.hasValidationResults()) {
                            resolve();
                        } else {
                            this.ClientService.Save(this.companyKey, this.Client.ClientCoreId, this.Client, false, false).then((clientResults) => {
                                if (clientResults.ValidationErrors) {
                                    this.ValidationResults.Client = clientResults.ValidationErrors;
                                } else {
                                    this.Client = clientResults.Client;

                                    //Map any missing client Ids
                                    for (var i = 0; i < w.Addresses.length; i++) {
                                        var workflowAddress = w.Addresses[i];
                                        if (workflowAddress.ClientCoreAddressId == null) {
                                            var matchingAddresses = clientResults.Client.Addresses.filter((a) => this.UtilService.AddressesAreEqual(a, workflowAddress));
                                            if (matchingAddresses.length > 0) {
                                                workflowAddress.ClientCoreAddressId = matchingAddresses[0].ClientCoreAddressId;
                                                workflowAddress.Latitude = matchingAddresses[0].Latitude;
                                                workflowAddress.Longitude = matchingAddresses[0].Longitude;
                                            }
                                        }
                                    }
                                    for (var i = 0; i < w.AdditionalNames.length; i++) {
                                        var workflowName = w.AdditionalNames[i];
                                        if (workflowName.SequenceNumber == null) {
                                            var matchingNames = clientResults.Client.AdditionalNames.filter((n) => n.BusinessName == workflowName.BusinessName);
                                            if (matchingNames.length > 0) {
                                                workflowName.SequenceNumber = matchingNames[0].SequenceNumber;
                                            }
                                        }
                                    }
                                }
                                resolve();
                            });
                        }
                    });

                    clientSavePromise.then(() => {
                        this.$window.scrollTo(0, 0);
                        this.SavingMessage = ((this.UpdatingApplication) ? 'Updating' : 'Creating') + ' Application';

                        if (this.hasValidationResults()) {
                            resolveCompletion(null);
                        } else {
                            this.saveSubmissions(resolveCompletion, w.Submissions, 0);
                        }

                        completionPromise.then((submission: Submission) => {
                            if (this.hasValidationResults()) {
                                this.refreshVisibleForm();
                                this.Saving = false;
                            } else {
                                this.allowStateTransition = true;
                                this.AlertService.Notify(NotificationType.Success, `Application Successfully ${(this.UpdatingApplication) ? 'Updated' : 'Created'}`);
                                this.$state.go('cn.viewapplication', { submissionId: submission.Id });
                            }
                        });
                    });
                });
            });

            this.UtilService.SetLoadingFlagDelayed(completionPromise, this.loadingPanelDelayMs);
        }

    }
}