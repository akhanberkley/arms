﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('classCodeSubmissionsModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/class-code-submissions-modal.html',
            replace: true,
            scope: {
                ClassCode: '=classCode',
                Submissions: '=submissions',
                SubmissionFields: '=submissionFields',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: ClassCodeSubmissionsModalController,
            controllerAs: 'vm'
        };
    });

    class ClassCodeSubmissionsModalController extends BaseController {
        ClassCode: ClassCode;
        Submissions: PagedDataSource;
        SubmissionFields: SubmissionDisplayField[];
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $http: ng.IHttpService) {
            super($scope, $state, UtilService);
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}