﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('tools',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/tools/tools.html',
            replace: true,
            bindToController: true,
            controller: ToolsController,
            controllerAs: 'vm'
        };
    });

    class ToolsController extends BaseController {
        Menu: { name: string; state: string; }[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, $q: ng.IQService, UserService: UserService, DomainService: DomainService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Tools');

            this.Menu = [];
            var dependencies = <ng.IPromise<any>[]>[];
            var companySettings = <CompanyConfiguration>null;
            var user = <User>null;
            dependencies.push(DomainService.CompanyInformation(this.companyKey).then((item) => { companySettings = item; }));
            dependencies.push(UserService.UserPromise.then((u) => { user = u; }));

            if ($state.is('cn.tools')) {
                $state.go('.fullsubmissionsearch', null, { location: 'replace' });
            }

            $q.all(dependencies).then(() => {
                this.Menu.push({ name: 'Advanced Submission Search', state: 'cn.tools.fullsubmissionsearch' });

                if (companySettings.SubmissionTypes.filter(st => st.ClassCodes.Enabled).length > 0) {
                    this.Menu.push({ name: 'View Class Codes', state: 'cn.tools.classcodes' });
                }

                if (user.IsInRole('Tools - Policy Number Generator') && companySettings.PolicyNumberGeneratable) {
                    this.Menu.push({ name: 'Policy Number Generator', state: 'cn.tools.policynumbergenerator' });
                }
            });
        }
    }
}