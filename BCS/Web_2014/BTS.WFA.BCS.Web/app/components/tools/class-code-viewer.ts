﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('classCodeViewerTool',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/tools/class-code-viewer.html',
            replace: true,
            bindToController: true,
            controller: ClassCodeViewerToolController,
            controllerAs: 'vm'
        };
    });

    class ClassCodeViewerToolController extends BaseController {
        DataSource: PagedDataSource;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Class Codes');
            UtilService.RaiseFocusTrigger('SearchTerm');

            this.ClassCodeLookup(null);
        }

        ClassCodeLookup(term: string) {
            var filter = '';
            if (term) {
                var encodedTerm = this.UtilService.ODataString(term);
                filter = '&$filter=contains(CodeValue,' + encodedTerm + ') or contains(GLCodeValue,' + encodedTerm + ') or contains(Description,' + encodedTerm + ')';
            }
            var url = '/api/' + this.companyKey + '/ClassCode?$orderby=CodeValue' + filter;

            this.AppState.Loading = true;
            this.DataSource = this.UtilService.SetupPagedTableDataSource({ url: url, pageSize: 5, pageSizeOptions: [5, 10, 20], onComplete: () => this.AppState.Loading = false });
        }
    }
}