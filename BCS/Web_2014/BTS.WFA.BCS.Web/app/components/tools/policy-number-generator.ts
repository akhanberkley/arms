﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('policyNumberGeneratorTool',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/tools/policy-number-generator.html',
            replace: true,
            bindToController: true,
            controller: PolicyNumberGeneratorToolController,
            controllerAs: 'vm'
        };
    });

    class PolicyNumberGeneratorToolController extends BaseController {
        GeneratedPolicyNumber: string;

        PolicySymbols: PolicySymbol[];
        SelectedPolicySymbol: PolicySymbol;
        ShowPolicySymbol: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $http: ng.IHttpService, DomainService: DomainService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Policy Number Generator');

            DomainService.PolicySymbols(this.companyKey).then((policySymbols) => {
                this.PolicySymbols = angular.copy(policySymbols);
                this.ShowPolicySymbol = (policySymbols.filter((p) => p.NumberControlId != null || p.PolicyNumberPrefix != null).length > 0);
            });
        }

        GeneratePolicyNumber() {
            this.AppState.Loading = true;
            var submission = <Submission>{};
            submission.PolicySymbol = this.SelectedPolicySymbol;

            this.$http.post('/api/' + this.companyKey + '/Submission/NextPolicyNumber', submission).then((response: ng.IHttpPromiseCallbackArg<string>) => {
                this.GeneratedPolicyNumber = (response.data) ? response.data.replace(/"/g, '') : '';
                this.AppState.Loading = false;
            });
        }
    }
}