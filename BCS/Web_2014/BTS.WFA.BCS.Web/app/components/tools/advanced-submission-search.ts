﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('advancedSubmissionSearchTool',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/tools/advanced-submission-search.html',
            replace: true,
            bindToController: true,
            controller: AdvancedSubmissionSearchToolController,
            controllerAs: 'vm'
        };
    });

    class AdvancedSubmissionSearchToolController extends BaseController {
        Workflow: SearchWorkflow;
        CompanyUsers: UserSummary[];
        SubmissionFields: SubmissionDisplayField[];
        HasSearched: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, $q: ng.IQService, private SearchService: SearchService, $http: ng.IHttpService,
            DomainService: DomainService, SearchWorkflowService: SearchWorkflowService, CompanyCustomizationService: CompanyCustomizationService, private AlertService: AlertService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Advanced Submission Search');

            this.AppState.PageGenerating = true;
            this.Workflow = SearchWorkflowService.GetWorkflow(this.companyKey, false, false);

            var pagePromises = <ng.IPromise<any>[]>[];
            pagePromises.push(DomainService.SubmissionSummaryFields(this.companyKey, 'submission-search-results').then((items) => this.SubmissionFields = items));
            pagePromises.push(this.Workflow.SubmissionCompanyCustomizationsPromise);
            pagePromises.push($http.get(`/api/${this.companyKey}/UserSummary?$filter=Active eq true&$orderby=Username`).then((response: ng.IHttpPromiseCallbackArg<UserSummary[]>) => {
                angular.forEach(response.data,(u) => {
                    u.DisplayName = u.UserName.replace(/wrbts\\/ig, '');
                });
                
                this.CompanyUsers = response.data.sort((u1, u2) => u1.DisplayName.localeCompare(u2.DisplayName));
            }));

            $q.all(pagePromises).then(() => this.AppState.PageGenerating = false);
        }

        Search() {
            this.HasSearched = true;
            this.AppState.Loading = true;
            this.Workflow.SubmissionDataSource = this.SearchService.Search.Submissions(this.companyKey, true, false, this.Workflow.SubmissionSearchCriteria,() => this.AppState.Loading = false);
            if (this.Workflow.SubmissionDataSource == null) {
                this.AlertService.Notify(NotificationType.Info, "The search could not be performed because the criteria was not specific enough");
            }
        }
    }
}