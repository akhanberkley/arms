﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('classCodes',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: (elem: ng.IAugmentedJQuery, attrs: ng.IAttributes) => {
                return `/app/components/class-codes-${attrs['view']}.html`;
            },
            replace: true,
            scope: {
                ClientId: '=clientId',
                ClassCodes: '=classCodes',
                DeletedClassCodes: '=deletedClassCodes',
                SubmissionFields: '=submissionFields',
                Editing: '=editing'
            },
            bindToController: true,
            controller: ClassCodeController,
            controllerAs: 'vm'
        }
    });

    class ClassCodeController extends BaseController {
        ClientId: string;
        ClassCodes: ClassCode[];
        DeletedClassCodes: ClassCode[];
        ClassCodeCustomizations: CompanyCustomization[];
        SubmissionFields: SubmissionDisplayField[];
        HazardGradeTypes: string[];
        ShowInput: boolean;
        Editing: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, DomainService: DomainService, private $modal: ng.ui.bootstrap.IModalService, private $filter: ng.IFilterService, private SearchService: SearchService) {
            super($scope, $state, UtilService);

            DomainService.CompanyCustomizations(this.companyKey, 'ClassCode').then((customizations) => {
                this.ClassCodeCustomizations = customizations;
            });
            DomainService.HazardGradeTypes(this.companyKey).then((data) => {
                this.HazardGradeTypes = data;
            });

            $scope.$watchGroup([() => this.ClassCodes, () => this.DeletedClassCodes],() => {
                if (this.ClassCodes != null && this.DeletedClassCodes != null) {
                    this.ClassCodes.forEach((cc) => {
                        cc.IsDeleted = (this.DeletedClassCodes.filter((dcc) => dcc.CodeValue == cc.CodeValue).length > 0);
                    });
                }
            });

            this.ShowInput = false;
        }

        ClassCodeLookup = (term: string) => { return this.SearchService.AutoComplete.ClassCodes(this.companyKey, term); }
        GetTotalColumnCount() {
            return 4 + (this.ClassCodeCustomizations || []).length + ((this.Editing) ? 1 : 0);
        }
        InputBlurred = (event: ng.IAngularEvent) => {
            this.ShowInput = false;
        }

        GetHazardGradeValue(hazardGrade: string, classCodeGrades: CodeItem[]) {
            for (var i = 0; i < classCodeGrades.length; i++) {
                if (classCodeGrades[i].Code == hazardGrade) {
                    return classCodeGrades[i].Description;
                }
            }
            return null;
        }

        DisplayClassCodeCustomization(f: CompanyCustomization, cc: ClassCode) {
            for (var i = 0; i < cc.CompanyCustomizations.length; i++)
                if (cc.CompanyCustomizations[i].Customization == f.Description)
                    return this.$filter('codeItem')(cc.CompanyCustomizations[i].CodeValue);
            return '';
        }

        ShowClassCodeSubmissions(classCode: ClassCode) {
            var modal = this.$modal.open({
                windowClass: 'modal-wide',
                template: '<class-code-submissions-modal class-code="classCode" submissions="submissions" submission-fields="submissionFields" modal="modal"></class-code-submissions-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['classCode'] = classCode;
                    $scope['submissions'] = this.UtilService.SetupPagedTableDataSource({ url: `/api/${this.companyKey}/Client/${this.ClientId}/Submission?$filter=ClassCodes/any(cc:contains(cc/CodeValue,${this.UtilService.ODataString(classCode.CodeValue) }))&$orderby=EffectiveDate desc, PolicyNumber` });
                    $scope['submissionFields'] = this.SubmissionFields;
                    $scope['modal'] = modal;
                }
            });
        }

        Add() {
            this.ShowInput = true;
            this.UtilService.RaiseFocusTrigger('ClassCodeSearchInput');
        }
        AddClassCode(classCode: ClassCode) {
            if (this.ClassCodes.filter((cc) => cc.CodeValue == classCode.CodeValue).length == 0) {
                this.ClassCodes.push(classCode);
            }
        }
        Delete(cc: ClassCode) {
            this.ClassCodes.splice(this.ClassCodes.indexOf(cc), 1);
        }

        AddToClient(classCode: ClientClassCode) {
            if (this.ClassCodes.filter((cc) => cc.CodeValue == classCode.CodeValue).length == 0) {
                classCode.IsNew = true;
                this.ClassCodes.push(classCode);
            }
        }
        DeleteFromClient(cc: ClientClassCode) {
            if (cc.IsNew) {
                this.ClassCodes.splice(this.ClassCodes.indexOf(cc), 1);
            } else {
                if (confirm(`The class code ${cc.CodeValue} will be removed from all submissions for this client.`)) {
                    this.DeletedClassCodes.push(cc);
                    cc.IsDeleted = true;
                }
            }
            
        }
        UnDeleteFromClient(cc: ClientClassCode) {
            cc.IsDeleted = false;
            this.DeletedClassCodes.splice(this.DeletedClassCodes.indexOf(cc), 1);
        }
    }
} 