﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('updateStatusModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/update-status-modal.html',
            replace: true,
            scope: {
                Item: '=item',
                UpdateApplication: '=updateApplication',
                Revert: '=revert',
                ModalInstance: '=modal',
                SubmissionType: '=submissionType'
            },
            bindToController: true,
            controller: UpdateStatusController,
            controllerAs: 'vm'
        };
    });

    class UpdateStatusController extends BaseController {
        Item: SubmissionStatusUpdate;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;
        UpdateApplication: boolean;
        Revert: boolean;
        SubmissionType: SubmissionType;
        StatusForm: ValidatedForm;
        Loading: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private SubmissionService: SubmissionService, private SubmissionWorkflowService: SubmissionWorkflowService) {
            super($scope, $state, UtilService);
        }

        UpdateStatus() {
            this.Loading = true;
            this.SubmissionService.UpdateStatus(this.companyKey, this.UpdateApplication, this.Item.Id, this.Item).then((updateResults) => {
                if (updateResults.ValidationErrors) {
                    this.UtilService.SetValidationResults(this.StatusForm, updateResults.ValidationErrors);
                    this.Loading = false;
                } else {
                    if (this.UpdateApplication) {
                        this.ModalInstance.close();//we don't need to return anything; the data will be retreived on page refresh
                    } else {
                        this.SubmissionService.Get(this.companyKey, this.Item.Id).then((submission) => {
                            this.SubmissionWorkflowService.GetWorkflow(this.companyKey, SubmissionWorkflowType.ViewingSubmission, submission).then((workflow) => {
                                this.ModalInstance.close(workflow);
                            });
                        });
                    }
                }
            });
        }

        RevertStatus() {
            this.Loading = true;
            this.SubmissionService.RevertStatus(this.companyKey, this.Item.Id, this.Item.RevertStatusTo.Id, this.Item).then((updateResults) => {
                if (updateResults.ValidationErrors) {
                    this.UtilService.SetValidationResults(this.StatusForm, updateResults.ValidationErrors);
                    this.Loading = false;
                } else {
                    this.SubmissionService.Get(this.companyKey, this.Item.Id).then((submission) => {
                        this.SubmissionWorkflowService.GetWorkflow(this.companyKey, SubmissionWorkflowType.ViewingSubmission, submission).then((workflow) => {
                            this.ModalInstance.close(workflow);
                        });
                    });
                }
            });
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}