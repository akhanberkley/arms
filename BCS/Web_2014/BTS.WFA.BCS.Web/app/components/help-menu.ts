﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('helpMenu', function (): ng.IDirective {
        return {
            restrict: 'E',
            templateUrl: '/app/components/help-menu.html',
            replace: true,
            scope: {},
            bindToController: true,
            controller: HelpMenuController,
            controllerAs: 'vm'
        };
    });

    class HelpMenuController {
        constructor(private $window: ng.IWindowService, private $state: ng.ui.IStateService, private UtilService: UtilService) {
        }

        OpenHelp() {
            var schid = 1000;
            if (this.UtilService.StateIsSearch(this.$state.current.name)) {
                schid = 1009;
            } else if (this.$state.includes('cn.search.client')) {
                schid = 1006;
            } else if (this.$state.includes('cn.search.submission')) {
                schid = 1010;
            } else if (this.$state.includes('cn.search.agency')) {
                schid = 1002;
            } else if (this.$state.includes('cn.tools')) {
                schid = 1013;
            } else if (this.$state.includes('cn.clearance')) {
                schid = 1006;
            } else if (this.$state.includes('cn.viewapplication')) {
                schid = 1019;
            } else if (this.$state.includes('cn.administration.classcodes')) {
                schid = 1005;
            } else if (this.$state.includes('cn.administration.companycustomizations')) {
                schid = 1008;
            } else if (this.$state.includes('cn.administration.users')) {
                schid = 1015;
            }

            var windowConfig = ['height=719', 'width=555'];
            this.$window.open('/Help/Default.htm#cshid=' + schid, 'bcs-help-window', windowConfig.join(', '));
        }
    }
}