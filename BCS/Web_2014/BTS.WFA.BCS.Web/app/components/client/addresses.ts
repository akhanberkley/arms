﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('clientAddressesRead', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                addresses: '='
            },
            templateUrl: '/app/components/client/addresses-read.html',
            replace: true,
            controller: ClientAddressesController,
            controllerAs: 'vm'
        }
    });

    app.directive('clientAddressesWrite', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                addresses: '=',
                usesCms: '='
            },
            templateUrl: (elem: ng.IAugmentedJQuery, attrs: ng.IAttributes) => {
                return `/app/components/client/addresses-write-${(attrs['isNew'] == 'true') ? 'new' : 'existing'}.html`;
            },
            replace: true,
            require: '^form',
            link: function (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, form: ValidatedForm) {
                scope.$watch(() => { return form.ValidationErrors; },(validationErrors: ValidationResult[]) => {
                    scope['validationErrors'] = validationErrors;
                });
            },
            controller: ClientAddressesController,
            controllerAs: 'vm'
        }
    });

    class ClientAddressesController extends BaseController {
        UsesCms: boolean;
        EditIndex: number;
        Addresses: ClientAddress[];
        States: State[];
        Countries: CodeItem[];
        AddressTypes: string[];
        LegacyAddressTypes: string[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, DomainService: DomainService, private $http: ng.IHttpService,
            private $q: ng.IQService, private $filter: ng.IFilterService, private $modal: ng.ui.bootstrap.IModalService, private ClientService: ClientService, private AlertService: AlertService) {
            super($scope, $state, UtilService);
            this.EditIndex = 0;

            DomainService.States().then((data) => { this.States = data });
            DomainService.Countries().then((data) => { this.Countries = data });
            DomainService.AddressTypes().then((data) => { this.AddressTypes = data });
            DomainService.LegacyAddressTypes().then((data) => { this.LegacyAddressTypes = data });

            $scope.$watch('addresses',(addresses) => { this.Addresses = addresses; });
            $scope.$watch('usesCms',(usesCms) => { this.UsesCms = usesCms; });

            var propertyNameFormat = /Address\[(\d)\].*/;
            $scope.$watch('validationErrors',(validationErrors: ValidationResult[]) => {
                angular.forEach(this.Addresses,(a) => { a.IsInvalid = false; });

                if (validationErrors) {
                    for (var i = 0; i < validationErrors.length; i++) {
                        var matchDetails = propertyNameFormat.exec(validationErrors[i].Property);
                        if (matchDetails) {
                            this.Addresses[parseInt(matchDetails[1])].IsInvalid = true;
                        }
                    }
                }
            });
        }

        SetPrimaryAddress(address: ClientAddress) {
            angular.forEach(this.Addresses,(a) => { a.IsPrimary = (a == address); });
        }
        PrimaryAddressSelectionChanged(address: ClientAddress) {
            if (address.IsPrimary) {
                this.SetPrimaryAddress(address);
            } else if (this.Addresses.length > 0) {
                this.Addresses[0].IsPrimary = true;
            }
        }

        AddAddress() {
            this.Addresses.push(this.ClientService.GetNewAddress(this.Addresses.length == 0));

            var lastIndex = this.Addresses.length - 1;
            this.EditIndex = lastIndex;
            this.UtilService.RaiseFocusTrigger('AddressTypeSection[' + lastIndex + ']');
        }
        EditAddress(index: number) {
            if (this.EditIndex != index) {
                this.EditIndex = index;
                this.UtilService.RaiseFocusTrigger('AddressTypeSection[' + index + ']');
            }
        }
        DeleteAddress(index: number) {
            var address = this.Addresses[index];
            if (!this.UsesCms && address.ClientCoreAddressId != null) {
                this.AlertService.OpenWarningModal({ title: this.$filter('address')(address), message: 'This address can not be deleted because the current client system does not allow it' });
            } else {
                if (this.EditIndex == index) {
                    this.EditIndex = 0;
                } else if (this.EditIndex > index) {
                    this.EditIndex -= 1;
                }

                this.Addresses.splice(index, 1);
                if (address.IsPrimary && this.Addresses.length > 0) {
                    this.Addresses[0].IsPrimary = true;
                }
            }
        }

        AddressHasType(address: ClientAddress, typeName: string) {
            for (var i = 0; i < address.AddressTypes.length; i++) {
                if (address.AddressTypes[i] == typeName) {
                    return true;
                }
            }
            return false;
        }
        ToggleAddressType(address: ClientAddress, typeName: string) {
            var index = address.AddressTypes.indexOf(typeName);
            if (index > -1) {
                address.AddressTypes.splice(index, 1);
            } else {
                //Only allow 1 type selected if they aren't on CMS
                if (!this.UsesCms) {
                    address.AddressTypes = [];
                }
                address.AddressTypes.push(typeName);
            }
        }

        private openAddressSearchModal(address: ClientAddress, matchesPromise: ng.IPromise<Address>) {
            var modal = this.$modal.open({
                template: '<address-lookup-modal address="address" matches-promise="matchesPromise" modal="modal"></address-lookup-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['modal'] = modal;
                    $scope['address'] = address;
                    $scope['matchesPromise'] = matchesPromise;
                }
            });
        }
        PostalCodeSearch(address: ClientAddress) {
            if (address.PostalCode != null && address.PostalCode.length == 5) {
                address.PostalCodeSearching = true;
                this.$http.get('/api/' + this.companyKey + '/Address/Lookup', { params: { PostalCode: address.PostalCode } }).then((response) => {
                    var data = <Address[]>response.data;
                    if (data != null) {
                        if (data.length == 1) {
                            address.City = data[0].City;
                            address.State = data[0].State;
                            address.County = data[0].County;
                        } else if (data.length > 1) {
                            var matchesPromise = new this.$q((resolve, reject) => { resolve(data); });
                            this.openAddressSearchModal(address, matchesPromise);
                        }
                    }
                    address.PostalCodeSearching = false;
                });
            }
        }
        AdvancedAddressSearch(address: ClientAddress) {
            var searchCriteria = { Address1: address.Address1, Address2: address.Address2, City: address.City, State: address.State, PostalCode: address.PostalCode, County: address.County };
            var promise = this.$http.get('/api/' + this.companyKey + '/Address/Lookup', { params: searchCriteria }).then((response) => {
                return response.data;
            });
            this.openAddressSearchModal(address, promise);
        }
    }
} 