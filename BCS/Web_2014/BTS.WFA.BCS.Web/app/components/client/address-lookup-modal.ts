﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('addressLookupModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/client/address-lookup-modal.html',
            replace: true,
            scope: {
                AddressToPopulate: '=address',
                MatchesPromise: '=matchesPromise',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: AddressLookupController,
            controllerAs: 'vm'
        };
    });

    class AddressLookupController extends BaseController {
        AddressToPopulate: Address;
        MatchesPromise: ng.IPromise<Address[]>;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;
        SelectedAddress: Address;
        LoadingMatches: boolean;
        Matches: Address[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private SubmissionService: SubmissionService, private SubmissionWorkflowService: SubmissionWorkflowService) {
            super($scope, $state, UtilService);

            this.LoadingMatches = true;
            this.MatchesPromise.then((matches) => {
                this.Matches = matches;
                this.LoadingMatches = false;
            });
        }

        SetSelectedAddress (selected: Address) {
            this.SelectedAddress = selected;
        }
        PopulateAddress (m: Address) {
            this.AddressToPopulate.Address1 = m.Address1 || this.AddressToPopulate.Address1;
            this.AddressToPopulate.Address2 = m.Address2 || this.AddressToPopulate.Address2;
            this.AddressToPopulate.City = m.City;
            this.AddressToPopulate.State = m.State;
            this.AddressToPopulate.PostalCode = m.PostalCode;
            this.AddressToPopulate.County = m.County;
            this.AddressToPopulate.Latitude = m.Latitude;
            this.AddressToPopulate.Longitude = m.Longitude;

            this.ModalInstance.close();
        }
        Close() {
            this.ModalInstance.dismiss();
        }
    }
}