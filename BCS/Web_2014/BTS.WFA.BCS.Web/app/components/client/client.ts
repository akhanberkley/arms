﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('client',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/client/client.html',
            replace: true,
            scope: {
                clientId: '='
            },
            bindToController: true,
            controller: ClientController,
            controllerAs: 'vm'
        }
    });

    class ClientController extends BaseController {
        private cleanClientViewModel: Client;

        clientId: number;
        Client: Client;
        Editing: boolean;
        ShowActions: boolean;
        ShowAdvancedActions: boolean;
        ShowSegment: boolean;
        CompanySettings: CompanyConfiguration;
        Submissions: PagedDataSource[];
        SubmissionFields: SubmissionDisplayField[];
        ClassCodes: ClientClassCode[];
        DeletedClassCodes: ClientClassCode[];
        ColumnClasses: string;
        SicCodeSearch: (term: string) => ng.IPromise<any>;
        NaicsCodeSearch: (term: string) => ng.IPromise<any>;

        PerformingPortfolioSearch: boolean;
        OriginalPortfolioName: string;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, $modalStack: ng.ui.bootstrap.IModalStackService, private $modal: ng.ui.bootstrap.IModalService, private SearchService: SearchService, private $q: ng.IQService, private $http: ng.IHttpService,
            private ClientService: ClientService, UserService: UserService, DomainService: DomainService, private AlertService: AlertService) {
            super($scope, $state, UtilService);

            this.SicCodeSearch = (term: string) => {
                return this.SearchService.AutoComplete.SicCodes(term);
            }
            this.NaicsCodeSearch = (term: string) => {
                return this.SearchService.AutoComplete.NaicsCodes(term);
            }

            this.AppState.PageGenerating = true;
            var domains = <ng.IPromise<any>[]>[];
            var user = <User>null;
            domains.push(UserService.UserPromise.then((User) => { user = User; }));
            domains.push(DomainService.CompanyInformation(this.companyKey).then((companySettings) => { this.CompanySettings = companySettings; }));
            domains.push(DomainService.SubmissionSummaryFields(this.companyKey, 'client-detail').then((data) => { this.SubmissionFields = data; }));

            $q.all(domains).then(() => {
                this.ShowActions = (!user.IsReadOnly);
                this.ShowAdvancedActions = user.IsInRole('Action - Switch Client');
                this.ShowSegment = (user.IsInRole('Action - Set Client Segment') && this.CompanySettings.ClientFields.ClientCoreSegment.Enabled);

                ClientService.Get(this.companyKey, this.clientId).then((result) => {
                    if (result.Failure) {
                        AlertService.OpenStandardModal({ title: 'Client ' + this.clientId, message: 'Unable to retreive client from Client System.' });
                    } else {
                        var client = this.Client = this.cleanClientViewModel = result.Client;
                        this.OriginalPortfolioName = client.Portfolio.Name;

                        if (client.CompanyCustomizationBindings.length == 0) {
                            this.ColumnClasses = 'tri-column-grid column-1';
                        } else if (client.CompanyCustomizationBindings.length > 4) {
                            this.ColumnClasses = 'tri-column-grid';
                        } else {
                            this.ColumnClasses = 'tri-column-grid column-2';
                        }

                        UtilService.SetPageTitle((client.PrimaryName != null) ? client.PrimaryName.BusinessName : client.ClientCoreId + '');

                        if ($state.is('cn.search.client.edit')) {
                            this.Edit();
                            $state.go('^.contactinfo', null, { location: 'replace' });
                        }
                    }

                    this.AppState.PageGenerating = false;
                });

                this.refreshClassCodes();
                ClientService.GetSubmissions(this.companyKey, this.clientId, true).then((source) => {
                    this.Submissions = source;
                });
            });

            //close class code modal window if they left the page from there
            $scope.$on('$stateChangeStart',(event, toState, toParams, fromState, fromParams) => {
                $modalStack.dismissAll();
            });
        }

        refreshClassCodes() {
            this.DeletedClassCodes = [];
            this.ClientService.GetClassCodes(this.companyKey, this.clientId).then((classCodes) => {
                this.ClassCodes = classCodes;
            });
        }

        PortfolioIdChanged() {
            this.PerformingPortfolioSearch = true;
            var httpDetails = this.SearchService.Search.GetClientHttpInfo(this.companyKey, { PortfolioId: this.Client.Portfolio.Id });

            this.$http.get(httpDetails.url, { params: httpDetails.params }).then((response) => {
                var results = <ClientSearchResults>response.data;

                if (results != null && results.Matches.length > 0 && results.Matches[0].Portfolio != null && (results.Matches[0].Portfolio.Name || '') != '') {
                    this.Client.Portfolio.Name = results.Matches[0].Portfolio.Name;
                    this.OriginalPortfolioName = this.Client.Portfolio.Name;
                }

                this.PerformingPortfolioSearch = false;
            });
        }

        Edit() {
            this.Editing = true;
            this.Client = angular.copy(this.cleanClientViewModel);

            //These tabs won't be visible; so if we're on one go to the first
            if (this.$state.is('cn.search.client.submissions')) {
                this.$state.go('cn.search.client.contactinfo');
            }
        }
        Cancel() {
            this.Editing = false;
            this.UtilService.SetValidationResults(this.$scope['ClientForm']);
            this.Client = angular.copy(this.cleanClientViewModel);
            angular.forEach(this.ClassCodes.filter(cc => cc.IsNew),(cc) => {
                this.ClassCodes.splice(this.ClassCodes.indexOf(cc), 1);
            });
            this.DeletedClassCodes = [];
        }
        NewSubmission() {
            this.UtilService.NavigateToNewSubmission(this.Client.ClientCoreId);
        }
        Save() {
            this.AppState.Loading = true;
            var portfolioNameChanged = (this.OriginalPortfolioName != this.Client.Portfolio.Name);
            this.ClientService.Save(this.companyKey, this.clientId, this.Client, true, portfolioNameChanged).then((results) => {
                if (results.ValidationErrors) {
                    this.UtilService.SetValidationResults(this.$scope['ClientForm'], results.ValidationErrors);
                    this.AppState.Loading = false;
                } else {
                    this.UtilService.SetValidationResults(this.$scope['ClientForm']);

                    var alteredClassCodePromises = <ng.IPromise<any>[]>[];
                    angular.forEach(this.DeletedClassCodes,(cc) => {
                        alteredClassCodePromises.push(this.ClientService.RemoveClassCode(this.companyKey, this.clientId, cc));
                    });
                    angular.forEach(this.ClassCodes.filter(cc => cc.IsNew),(cc) => {
                        alteredClassCodePromises.push(this.ClientService.AddClassCode(this.companyKey, this.clientId, cc));
                    });

                    this.$q.all(alteredClassCodePromises).then(() => {
                        this.cleanClientViewModel = results.Client;
                        this.OriginalPortfolioName = results.Client.Portfolio.Name;
                        this.refreshClassCodes();
                        this.Cancel();
                        this.AppState.Loading = false;
                        this.AlertService.Notify(NotificationType.Success, `${this.Client.PrimaryName.BusinessName} was updated`);
                    });
                }
            });
        }
        SwitchClient() {
            var modal = this.$modal.open({
                size: 'lg',
                template: '<switch-client-modal client-id="clientId" client-name="clientName" submissions-promise="submissionsPromise" modal="modal"></switch-client-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['clientId'] = this.clientId;
                    $scope['clientName'] = this.Client.PrimaryName.BusinessName;
                    $scope['submissionsPromise'] = this.ClientService.GetSubmissions(this.companyKey, this.clientId, false);
                    $scope['modal'] = modal;
                }
            });

            modal.result.then((completed) => {
                if (completed) {
                    this.$state.transitionTo(this.$state.current.name, this.$state.params, { reload: true, inherit: false, notify: true });
                }
            });
        }
    }
}