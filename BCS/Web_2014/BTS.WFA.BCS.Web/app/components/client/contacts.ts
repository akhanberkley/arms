﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('clientContactsRead', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                contacts: '=',
                usesCms: '='
            },
            templateUrl: '/app/components/client/contacts-read.html',
            replace: true,
            controller: ClientContactsController,
            controllerAs: 'vm'
        }
    });
    app.directive('clientContactsWrite', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                contacts: '=',
                usesCms: '='
            },
            templateUrl: '/app/components/client/contacts-write.html',
            replace: true,
            controller: ClientContactsController,
            controllerAs: 'vm'
        }
    });

    class ClientContactsController extends BaseController {
        UsesCms: boolean;
        ContactTypes: string[];
        Contacts: ClientContact[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, DomainService: DomainService) {
            super($scope, $state, UtilService);

            $scope.$watch('usesCms',(usesCms) => {
                if (usesCms != undefined) {
                    if (this.UsesCms = usesCms) {
                        DomainService.ContactTypes().then((data) => { this.ContactTypes = data });
                    } else {
                        this.ContactTypes = ['Phone'];
                    }
                }
            });

            $scope.$watch('contacts',(contacts) => { this.Contacts = contacts; });
        }

        AddContact() {
            this.Contacts.push({ ContactType: this.ContactTypes[0], Value: null });
            this.UtilService.RaiseFocusTrigger('Contact[' + (this.Contacts.length - 1) + ']-ContactType');
        }
        DeleteContact(contact: ClientContact) {
            this.Contacts.splice(this.Contacts.indexOf(contact), 1);
        }
    }
} 