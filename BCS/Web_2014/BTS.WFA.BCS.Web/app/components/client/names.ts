﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('clientNamesRead',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/client/names-read.html',
            replace: true,
            scope: { additionalNames: '=' },
            controller: ClientNamesController,
            controllerAs: 'vm'
        }
    });
    app.directive('clientNamesWrite',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/client/names-write.html',
            replace: true,
            scope: { additionalNames: '=' },
            controller: ClientNamesController,
            controllerAs: 'vm'
        }
    });

    class ClientNamesController extends BaseController {
        NameTypes: string[];
        Names: ClientName[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private AlertService: AlertService, DomainService: DomainService) {
            super($scope, $state, UtilService);

            $scope.$watch('additionalNames',(names) => { this.Names = names; });

            DomainService.ClientNameTypes().then((items) => {
                this.NameTypes = items;
            });
        }

        AddName() {
            this.Names.push({ NameType: 'DBA' });
            this.UtilService.RaiseFocusTrigger('AdditionalNames[' + (this.Names.length - 1) + ']-NameType');
        }
        DeleteName(name: ClientName) {
            if (name.SequenceNumber != null) {
                this.AlertService.OpenWarningModal({ title: name.BusinessName, message: 'This name can not be deleted because the current client system does not allow it' });
            } else {
                this.Names.splice(this.Names.indexOf(name), 1);
            }
        }
    }
} 