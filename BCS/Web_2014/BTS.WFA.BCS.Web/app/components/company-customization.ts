﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('companyCustomizationRead', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                b: '=binding'
            },
            templateUrl: '/app/components/company-customization-read.html',
            replace: true
        }
    });
    app.directive('companyCustomizationWrite', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                b: '=binding',
                booleanAsDropdown: '='
            },
            templateUrl: '/app/components/company-customization-write.html',
            replace: true
        }
    });
} 