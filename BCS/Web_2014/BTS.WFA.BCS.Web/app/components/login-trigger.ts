﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('loginTrigger', function ($rootScope: ng.IRootScopeService, $timeout: ng.ITimeoutService): ng.IDirective {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: '/app/components/login-trigger.html',
            scope: true,
            link: (scope: any, element: ng.IAugmentedJQuery, attrs: ng.IAttributes) => {
                var iframe = element.find('iframe');
                scope.LoggingIn = false;

                var loginFlagTimeout = <ng.IPromise<any>>null;
                var login = () => {
                    iframe.attr('src', 'login');

                    //Putting a slight delay on this so it doesn't flicker on page refresh
                    loginFlagTimeout = $timeout(() => { scope.LoggingIn = true; }, 100);
                }
                $rootScope.$on('Login-Required',() => { login(); });

                iframe.load(() => {
                    try {
                        //This line will crash if this load was for another site (i.e. ADFS)
                        iframe.contents();

                        //Broadcast we're ok if we haven't crashed (and are therefore back on our own domain with /login loaded)
                        $timeout.cancel(loginFlagTimeout);
                        scope.LoggingIn = false;
                        $rootScope.$broadcast('Login-Completed');
                    } catch (e) { }
                });

                //Perform initial login
                login();
            }
        };
    });
} 