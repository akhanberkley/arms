﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('clientSearchResults',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/search/search-results-client.html',
            replace: true
        }
    });
    app.directive('submissionSearchResults',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/search/search-results-submission.html',
            replace: true
        }
    });
    app.directive('agencySearchResults',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/search/search-results-agency.html',
            replace: true
        }
    });
}  