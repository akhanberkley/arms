﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionSearchModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/search/submission-modal.html',
            replace: true,
            scope: {
                ModalInstance: '=modal',
                CrossCompany: '=crossCompany'
            },
            bindToController: true,
            controller: SubmissionSearchModalController,
            controllerAs: 'vm'
        };
    });

    class SubmissionSearchModalController extends BaseController {
        CrossCompany: boolean;
        UseRenewalCancelDate: boolean;
        EffectiveDate: any;
        Workflow: SearchWorkflow;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, SearchWorkflowService: SearchWorkflowService) {
            super($scope, $state, UtilService);

            this.Workflow = SearchWorkflowService.GetWorkflow(this.companyKey, this.CrossCompany, true);
            this.Workflow.SearchType = SearchType.Submission;

            this.Workflow.ModalSelectItem = () => {
                this.ModalInstance.close(this.Workflow.SelectedModalItem);
            };
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}