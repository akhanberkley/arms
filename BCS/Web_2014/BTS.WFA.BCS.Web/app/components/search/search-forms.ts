﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('clientSearchForm',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/search/search-forms-client.html',
            replace: true
        }
    });
    app.directive('submissionSearchForm',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/search/search-forms-submission.html',
            replace: true
        }
    });
    app.directive('agencySearchForm',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/search/search-forms-agency.html',
            replace: true
        }
    });
} 