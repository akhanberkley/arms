﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('search',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/search/search.html',
            replace: true,
            scope: {
                initialWorkflow: '=workflow',
                crossCompany: '=crossCompany'
            },
            bindToController: true,
            controller: SearchController,
            controllerAs: 'vm'
        };
    });

    class SearchController extends BaseController {
        initialWorkflow: SearchWorkflow;
        crossCompany: boolean;
        IsModal: boolean;
        ShowActions: boolean;
        ShowAgencySearch: boolean;
        Workflow: SearchWorkflow;
        NavigateToNewClientSubmission: (clientId: number) => void;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, UserService: UserService, SearchWorkflowService: SearchWorkflowService) {
            super($scope, $state, UtilService);

            UserService.UserPromise.then((user) => {
                this.ShowActions = (!user.IsReadOnly);
                this.ShowAgencySearch = (user.LimitingAgency == null);
            });

            if (this.initialWorkflow != null) {
                this.Workflow = this.initialWorkflow;
                this.IsModal = true;
                this.Workflow.OpenMenu();
            } else {//standard search page
                this.NavigateToNewClientSubmission = UtilService.NavigateToNewSubmission;
                this.Workflow = SearchWorkflowService.GetWorkflow(this.companyKey, false, false);
                this.stateChanged($state.current.name);

                $scope.$on('$stateChangeSuccess',(event, toState, toParams, fromState, fromParams) => {
                    this.stateChanged(toState.name);
                });
            }
        }

        private stateChanged(stateName: string) {
            if (this.$state.current.name.indexOf('cn.search.client') > -1) {
                this.Workflow.SearchType = SearchType.Client;
                if (this.$state.is('cn.search.clients')) {
                    this.UtilService.SetPageTitle('Client Search');
                }
            } else if (this.$state.current.name.indexOf('cn.search.submission') > -1) {
                this.Workflow.SearchType = SearchType.Submission;
                if (this.$state.is('cn.search.submissions')) {
                    this.UtilService.SetPageTitle('Submission Search');
                }
            } else if (this.$state.current.name.indexOf('cn.search.agenc') > -1) {
                this.Workflow.SearchType = SearchType.Agency;
                if (this.$state.is('cn.search.agencies')) {
                    this.UtilService.SetPageTitle('Agency Search');
                }
            }

            var onSearchScreen = this.UtilService.StateIsSearch(stateName);
            this.Workflow.DisplayPreSearchPane = (onSearchScreen && !this.Workflow.SearchTypesSearched[this.Workflow.SearchType]);

            if (onSearchScreen) {
                switch (this.Workflow.SearchStateChangeCause) {
                    case SearchMenuStateChangeReason.Results:
                        this.Workflow.MenuState = SearchMenuState.Open;
                        break;
                    default:
                        this.Workflow.OpenMenu();
                        break;
                }
            } else {
                this.Workflow.CloseMenu();
            }

            this.Workflow.SearchStateChangeCause = null;
            this.Workflow.MenuFixed = (this.Workflow.MenuState != 0);
        }
    }
}