﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('agencySearchModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/search/agency-modal.html',
            replace: true,
            scope: {
                UseRenewalCancelDate: '=useRenewalCancelDate',
                EffectiveDate: '=effectiveDate',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: AgencySearchModalController,
            controllerAs: 'vm'
        };
    });

    class AgencySearchModalController extends BaseController {
        UseRenewalCancelDate: boolean;
        EffectiveDate: any;
        Workflow: SearchWorkflow;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, SearchWorkflowService: SearchWorkflowService) {
            super($scope, $state, UtilService);

            this.Workflow = SearchWorkflowService.GetWorkflow(this.companyKey, false, true);
            this.Workflow.SearchType = SearchType.Agency;

            if (this.UseRenewalCancelDate) {
                this.Workflow.AgencySearchCriteria.RenewalCancelDate = this.EffectiveDate;
            } else {
                this.Workflow.AgencySearchCriteria.CancelDate = this.EffectiveDate;
            }

            this.Workflow.ModalSelectItem = () => {
                this.ModalInstance.close(this.Workflow.SelectedModalItem);
            };
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}