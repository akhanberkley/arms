﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('agencyModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/agency/agency-modal.html',
            replace: true,
            scope: {
                Agency: '=agency',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: AgencyModalController,
            controllerAs: 'vm'
        };
    });

    class AgencyModalController extends BaseController {
        Agency: Agency;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}