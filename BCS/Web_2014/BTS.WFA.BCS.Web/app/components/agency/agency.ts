﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('agency',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/agency/agency.html',
            replace: true,
            scope: {
                agencyCode: '=',
                SearchWorkflow: '=searchWorkflow',
                IsModal: '=isModal'
            },
            bindToController: true,
            controller: AgencyController,
            controllerAs: 'vm'
        }
    });

    export class AgencyController extends BaseController {
        agencyCode: string;
        IsModal: boolean;
        SearchWorkflow: SearchWorkflow;
        Agency: Agency;
        ReferralAgency: Agency;
        UnderwritersLabel: string;
        AgentsEnabled: boolean;
        AnalystEnabled: boolean;
        AnalystLabel: string;
        TechnicianEnabled: boolean;
        TechnicianLabel: string;
        APSAgencyUrl: string;
        ShowActions: boolean;
        VerticallyOrderedLicensedStates: any[];
        SubmissionFields: SubmissionDisplayField[];
        SubmissionDataSource: PagedDataSource;
        CurrentTab: number;
        CurrentTabTemplateUrl: string;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $modal: ng.ui.bootstrap.IModalService, $q: ng.IQService,
            AlertService: AlertService, AgencyService: AgencyService, DomainService: DomainService, UserService: UserService) {
            super($scope, $state, UtilService);

            this.AppState.Loading = true;
            if (!this.IsModal) {
                this.AppState.PageGenerating = true;
            }

            DomainService.CompanyInformation(this.companyKey).then((companySettings) => {
                this.AgentsEnabled = companySettings.AgentsEnabled;
                this.UnderwritersLabel = companySettings.SubmissionTypes[0].Underwriter.Label;
                this.AnalystEnabled = companySettings.AnalystEnabled;
                this.AnalystLabel = companySettings.SubmissionTypes[0].UnderwritingAnalyst.Label;
                this.TechnicianEnabled = companySettings.TechnicianEnabled;
                this.TechnicianLabel = companySettings.SubmissionTypes[0].UnderwritingTechnician.Label;

                if (companySettings.APSAgencyUrl) {
                    this.APSAgencyUrl = companySettings.APSAgencyUrl.replace('{0}', '');
                }
            });
            UserService.UserPromise.then((User) => {
                this.ShowActions = (!User.IsReadOnly);
            });

            $scope.$watch(() => this.agencyCode,(agencyCode: string) => {
                this.ReferralAgency = null;

                AgencyService.GetAgency(this.companyKey, agencyCode, !this.IsModal).then((agency) => {
                    if (agency == null) {
                        AlertService.OpenStandardModal({ title: 'Agency ' + agencyCode, message: 'Agency could not be found.' });
                    } else {
                        this.Agency = agency;

                        DomainService.SubmissionSummaryFields(this.companyKey, 'agency-detail').then((fields) => {
                            this.SubmissionFields = fields;
                        });
                        this.SubmissionDataSource = UtilService.SetupPagedTableDataSource({ url: '/api/' + this.companyKey + '/Agency/' + agency.Code + '/Submissions?$orderby=EffectiveDate desc, PolicyNumber' });

                        if (agency.ReferralAgencyCode) {
                            AgencyService.GetAgency(this.companyKey, agency.ReferralAgencyCode, false).then((referralAgency) => {
                                this.ReferralAgency = referralAgency;
                            });
                        }

                        var requests = <ng.IPromise<any>[]>[];
                        requests.push(AgencyService.GetUnderwritingUnits(this.companyKey, agency.Code).then((data) => {
                            agency.UnderwritingUnits = data;
                        }));
                        requests.push(AgencyService.GetAgencyAgents(this.companyKey, agency.Code).then((data) => {
                            angular.forEach(data,(a) => { UtilService.SetupAgentForUI(a, false, this.companyKey); });
                            agency.Agents = data;
                        }));
                        requests.push(AgencyService.GetLicensedStates(this.companyKey, agency.Code).then((data) => {
                            agency.LicensedStates = data;
                            this.VerticallyOrderedLicensedStates = UtilService.CreateVerticalTable(data, Math.min(data.length, 4));
                        }));

                        $q.all(requests).then(() => {
                            if (this.IsModal) {
                                this.SetTab(0);
                            } else {
                                UtilService.SetPageTitle(agency.Name);
                                this.AppState.PageGenerating = false;
                            }

                            this.AppState.Loading = false;
                        });
                    }
                });
            });
        }

        ShowPersonnel(underwritingUnit: UnderwritingUnit, personnel: UnderwritingPersonnel[], description: string) {
            var modal = this.$modal.open({
                size: 'sm',
                windowClass: 'modal-tall max-height-500px',
                template: '<agency-underwriting-unit-personnel-modal description="description" personnel="personnel" modal="modal"></agency-underwriting-unit-personnel-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['description'] = underwritingUnit.Description + ' ' + description;
                    $scope['personnel'] = personnel;
                    $scope['modal'] = modal;
                }
            });
        }

        SetTab(index: number) {
            this.CurrentTab = index;
            switch (index) {
                case 0: this.CurrentTabTemplateUrl = '/app/components/agency/tabs/linked-agencies.html'; break;
                case 1: this.CurrentTabTemplateUrl = '/app/components/agency/tabs/underwriting-units.html'; break;
                case 2: this.CurrentTabTemplateUrl = '/app/components/agency/tabs/licensed-states.html'; break;
                case 3: this.CurrentTabTemplateUrl = '/app/components/agency/tabs/agents.html'; break;
                case 4: this.CurrentTabTemplateUrl = '/app/components/agency/tabs/submissions.html'; break;
            }
        }
    }
} 