﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('agencyUnderwritingUnitPersonnelModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/agency/underwriting-unit-personnel-modal.html',
            replace: true,
            scope: {
                Description: '=description',
                Personnel: '=personnel',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: AgencyUnderwritingUnitPersonnelModalController,
            controllerAs: 'vm'
        };
    });

    class AgencyUnderwritingUnitPersonnelModalController extends BaseController {
        Description: string;
        Personnel: UnderwritingPersonnel[];
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}