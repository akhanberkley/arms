﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('switchClientModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/switch-client-modal.html',
            replace: true,
            scope: {
                ModalInstance: '=modal',
                ClientId: '=clientId',
                ClientName: '=clientName',
                SubmissionsPromise: '=submissionsPromise'
            },
            bindToController: true,
            controller: SwitchClientModalController,
            controllerAs: 'vm'
        };
    });

    class SwitchClientModalController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;
        ClientId: number;
        ClientName: string;
        SubmissionsPromise: ng.IPromise<Submission[]>;
        LoadingSubmissions: boolean;
        IsComplete: boolean;
        CurrentStep: number;
        Submissions: Submission[];
        SubmissionFields: SubmissionDisplayField[];

        ClientSearch: {
            Searching: boolean;
            Id: number;
            Match: Client;
            DoingBusinessAsNames: any[];
            DbaName: string;
            ClientNotFound: boolean;
            IsDuplicateId: boolean;
        }

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, DomainService: DomainService,
            private $http: ng.IHttpService, private $q: ng.IQService, private SearchService: SearchService) {
            super($scope, $state, UtilService);

            this.CurrentStep = 1;
            this.IsComplete = false;
            this.ClientSearch = { Searching: false, Id: null, Match: null, DoingBusinessAsNames: [], ClientNotFound: false, IsDuplicateId: false, DbaName: null };

            this.LoadingSubmissions = true;
            this.SubmissionsPromise.then((submissions) => {
                this.Submissions = submissions;
                this.LoadingSubmissions = false;
            });

            DomainService.SubmissionSummaryFields(this.companyKey, 'switch-client').then((items) => {
                this.SubmissionFields = items;
            });
        }

        ClearSearchResults() {
            this.ClientSearch.ClientNotFound = false;
            this.ClientSearch.IsDuplicateId = false;
            this.ClientSearch.Match = null;
        }

        Continue() {
            if (this.CurrentStep == 1) {
                this.CurrentStep++;
                this.UtilService.RaiseFocusTrigger('ClientId');
            } else if (this.CurrentStep == 2) {
                this.ClearSearchResults();

                if (this.ClientId == this.ClientSearch.Id) {
                    this.ClientSearch.IsDuplicateId = true;
                } else {
                    this.ClientSearch.Searching = true;
                    var httpDetails = this.SearchService.Search.GetClientHttpInfo(this.companyKey, { ClientId: this.ClientSearch.Id });
                    this.$http.get(httpDetails.url, { params: httpDetails.params }).then((response) => {
                        var results = <ClientSearchResults>response.data;

                        this.ClientSearch.Match = (results != null && results.Matches != null && results.Matches.length > 0) ? results.Matches[0] : null;
                        this.ClientSearch.DoingBusinessAsNames = (this.ClientSearch.Match != null) ? (<ClientName[]>this.ClientSearch.Match.AdditionalNames).filter((n) => { return n.NameType == 'DBA'; }) : [];
                        this.ClientSearch.ClientNotFound = (this.ClientSearch.Match == null);
                        if (!this.ClientSearch.ClientNotFound) {
                            this.CurrentStep++;
                        }

                        this.ClientSearch.Searching = false;
                    });
                }
            }
        }

        Complete() {
            var match = this.ClientSearch.Match;
            var newInsured = { ClientCoreId: match.ClientCoreId, ClientCorePortfolioId: match.Portfolio.Id, InsuredName: match.PrimaryName.BusinessName, InsuredDBA: this.ClientSearch.DbaName };

            var updateRequests = <ng.IPromise<any>[]>[];
            angular.forEach(this.Submissions,(submission) => {
                updateRequests.push(this.$http.put('/api/' + this.companyKey + '/Submission/' + submission.Id + '/ClientReassign/', newInsured));
            });

            this.$q.all(updateRequests).then(() => {
                this.IsComplete = true;
            });
        }

        Close() {
            this.ModalInstance.close(this.IsComplete);
        }
    }
}