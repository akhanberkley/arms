﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissions', function (): ng.IDirective {
        return {
            restrict: 'E',
            templateUrl: '/app/components/submissions/submissions.html',
            replace: true,
            scope: {
                submissions: '=',
                source: '=',
                fields: '=',
                copyAll: '=',
                disableLinks: '=',
                searchWorkflow: '=',
                duplicatesSubmissionWorkflow: '=',
                includeActions: '='
            },
            bindToController: true,
            controller: SubmissionsController,
            controllerAs: 'vm'
        }
    });

    class SubmissionsController extends BaseController {
        submissions: Submission[];
        fields: SubmissionDisplayField[];
        source: PagedDataSource;
        copyAll: boolean;
        disableLinks: boolean;
        searchWorkflow: SearchWorkflow;
        duplicatesSubmissionWorkflow: SubmissionWorkflow;
        includeActions: boolean;
        actionInProcess: boolean;
        actionsAvailable: { Description: string; Action: any; }[];

        currencyFields = ['Premium'];
        dateFields = ['EffectiveDate', 'ExpirationDate', 'StatusDate', 'SubmittedDate'];
        duplicateCheckBoxes = <JQuery[]>[];
        noDuplicateCheckBox: JQuery;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $element: ng.IAugmentedJQuery, private $filter: ng.IFilterService, private $http: ng.IHttpService,
            private $modal: ng.ui.bootstrap.IModalService, AgencyService: AgencyService, private SubmissionService: SubmissionService, UserService: UserService) {
            super($scope, $state, UtilService);
            //Render Table Header
            var thead = $element.find('table thead');

            //Render Submissions
            var viewSubmissions = <Submission[]>null;
            var tbody = $element.find('table tbody');
            var tfoot = $element.find('table tfoot');

            this.actionsAvailable = [];
            UserService.UserPromise.then((user) => {
                if (user.IsInRole('Action - Switch Underwriting Personnel')) {
                    this.actionsAvailable.push({ Description: 'Switch Underwriting Personnel', Action: () => this.OpenSwitchUnderwritingModal() });
                }
            });

            //Watch Fields
            $scope.$watch(() => (this.source) ? this.source.items : null,(submissions: Submission[]) => {//paged source
                if (submissions) {
                    viewSubmissions = submissions;
                    this.renderTableSubmissions(tbody, viewSubmissions, tfoot);
                }
            });
            $scope.$watch(() => this.submissions,(submissions: Submission[]) => {//array source
                if (submissions) {
                    viewSubmissions = submissions;
                    this.renderTableSubmissions(tbody, viewSubmissions, tfoot);
                }
            });
            $scope.$watch(() => this.fields,(fields: SubmissionDisplayField[]) => {//build header and rebuild submissions
                if (fields) {
                    this.renderTableHeader(thead, fields);
                    this.renderTableSubmissions(tbody, viewSubmissions, tfoot);
                }
            });
            $scope.$watch(() => (this.duplicatesSubmissionWorkflow) ? this.duplicatesSubmissionWorkflow.Duplicate : null,(duplicate: Submission) => {
                if (this.duplicatesSubmissionWorkflow != null) {
                    for (var i = 0; i < this.duplicateCheckBoxes.length; i++) {
                        this.duplicateCheckBoxes[i].prop('checked',(this.duplicateCheckBoxes[i].data("Submission") == duplicate));
                    }

                    this.noDuplicateCheckBox.prop('checked',(this.duplicatesSubmissionWorkflow != null && this.duplicatesSubmissionWorkflow.NoDuplicatesApplicable));
                }
            });
        }

        private renderTableHeader(container: JQuery, fields: SubmissionDisplayField[]) {
            var row = <JQuery>null;
            if (fields) {
                container.empty();
                row = angular.element('<tr>');
                if (this.duplicatesSubmissionWorkflow) {
                    row.append(angular.element('<th>').addClass('checkbox-column'));
                }
                angular.forEach(fields,(f) => {
                    var th = angular.element('<th>').attr('title', f.DisplayName).addClass(f.PropertyName);
                    switch (f.PropertyName) {
                        case "Notes":
                            th.append(angular.element('<i>').addClass('fa fa-file-o'));
                            break;
                        default:
                            th.text(f.Header);
                            break;
                    }
                    row.append(th);
                });
                container.append(row);
            }
        }
        private renderTableSubmissions(container: JQuery, submissions: Submission[], footerContainer: JQuery) {
            var row = <JQuery>null;
            var td = <JQuery>null;
            if (submissions != null && this.fields != null) {
                container.empty();
                if (submissions.length == 0) {
                    row = angular.element('<tr>');
                    row.append(angular.element('<td>N/A</td>').attr('colspan', this.fields.length + ((this.duplicatesSubmissionWorkflow) ? 1 : 0)).addClass('text-muted'));
                    container.append(row);
                } else if (this.fields.length > 0) {
                    angular.forEach(submissions,(s) => {
                        var checkBox = <JQuery>null;
                        row = angular.element('<tr>');
                        td = null;

                        if (this.duplicatesSubmissionWorkflow) {
                            td = angular.element('<td>').addClass('checkbox-column');
                            checkBox = angular.element('<input type="checkbox">')
                                .addClass('checkbox-inline')
                                .click(() => {
                                this.$scope.$apply(() => {
                                    this.duplicatesSubmissionWorkflow.SelectDuplicate(s);
                                });
                            })
                                .prop('checked',(this.duplicatesSubmissionWorkflow.Duplicate != null && this.duplicatesSubmissionWorkflow.Duplicate.Id == s.Id))
                                .data('Submission', s);

                            this.duplicateCheckBoxes.push(checkBox);
                            td.append(checkBox);
                            row.append(td);
                        }

                        for (var j = 0; j < this.fields.length; j++) {
                            var f = this.fields[j];

                            var elementText = <string>null;
                            td = angular.element('<td>').addClass(f.PropertyName);
                            switch (f.PropertyName) {
                                case "SubmissionNumber":
                                    var contentElm = <JQuery>null;
                                    if (this.disableLinks) {
                                        if (this.duplicatesSubmissionWorkflow) {
                                            contentElm = angular.element('<a click-event>').click(() => { this.SubmissionService.OpenSubmissionModal(s, true); });
                                        } else if (this.searchWorkflow) {
                                            contentElm = angular.element('<a click-event>').click(() => { this.searchWorkflow.ModalViewItem(s); });
                                        } else {
                                            contentElm = angular.element('<span>');
                                        }

                                        if (this.copyAll) {
                                            this.UtilService.SetupCopyableElement(this.$scope, contentElm);
                                        }

                                        td.append(contentElm);
                                    } else {
                                        contentElm = angular.element('<a>').attr('href', this.UtilService.CreateSubmissionLink(s.Id, s.Company.Abbreviation));
                                        td.append(contentElm);

                                        if (this.copyAll) {
                                            var copyIcon = angular.element('<i class="fa fa-files-o"></i>');
                                            this.UtilService.SetupCopyableElement(this.$scope, copyIcon, s.SubmissionNumber + '');
                                            td.append(copyIcon);
                                        }
                                    }
                                    contentElm.text(s.SubmissionNumber).attr('title', this.$filter('submissionTooltip')(s));
                                    break;
                                case "Agency":
                                    if (s.Agency) {
                                        td.attr('title', this.$filter('agencyTooltip')(s.Agency));
                                        if (this.disableLinks) {
                                            elementText = s.Agency.Name;
                                        } else {
                                            td.append(angular.element('<a>').attr('href', this.UtilService.CreateAgencyLink(s.Agency.Code, s.Company.Abbreviation)).text(s.Agency.Name));

                                            if (this.copyAll) {
                                                var copyIcon = angular.element('<i class="fa fa-files-o"></i>');
                                                this.UtilService.SetupCopyableElement(this.$scope, copyIcon, s.Agency.Name + '');
                                                td.append(copyIcon);
                                            }
                                        }
                                    }
                                    break;
                                case "Company":
                                    elementText = s.Company.Abbreviation;
                                    td.attr('title', s.Company.CompanyName);
                                    break;
                                case "AgencyCode":
                                    if (s.Agency) {
                                        elementText = s.Agency.Code;
                                    }
                                    break;
                                case "InsuredName":
                                    td.attr('title', this.$filter('clientTooltip')({ ClientCoreId: s.ClientCoreId }));
                                    if (this.disableLinks) {
                                        elementText = s.InsuredName;
                                    } else {
                                        td.append(angular.element('<a>').attr('href', this.UtilService.CreateClientLink(s.ClientCoreId)).text(s.InsuredName));
                                    }
                                    break;
                                case "PolicySymbol":
                                    elementText = this.$filter('codeItem')(s.PolicySymbol);
                                    break;
                                case "PackagePolicySymbols":
                                    elementText = s.PackagePolicySymbols.map((ps) => this.$filter('codeItem')(ps)).join(', ');
                                    break;
                                case "UnderwritingUnit":
                                    elementText = this.$filter('codeItem')(s.UnderwritingUnit);
                                    break;
                                case "Underwriter":
                                    if (s.Underwriter != null) {
                                        elementText = s.Underwriter.FullName;
                                    }
                                    break;
                                case "UnderwritingAnalyst":
                                    if (s.UnderwritingAnalyst != null) {
                                        elementText = s.UnderwritingAnalyst.FullName;
                                    }
                                    break;
                                case "UnderwritingTechnician":
                                    if (s.UnderwritingTechnician != null) {
                                        elementText = s.UnderwritingTechnician.FullName;
                                    }
                                    break;
                                case "Status":
                                    if (!s.IsDeleted) {
                                        var statusElm = angular.element('<div>').text(s.Status);
                                        var statusReasonElm = angular.element('<div>').text(s.StatusReason).addClass('text-muted');
                                        td.append(statusElm);
                                        td.append(statusReasonElm);
                                        if (this.copyAll) {
                                            this.UtilService.SetupCopyableElement(this.$scope, statusElm);
                                            this.UtilService.SetupCopyableElement(this.$scope, statusReasonElm);
                                        }
                                    } else {
                                        td.addClass('text-danger');
                                        td.append(angular.element('<i>').addClass('fa fa-times'));

                                        var deletedStatusElm = angular.element('<span>').text(' Deleted');
                                        td.append(deletedStatusElm);
                                        if (this.copyAll) {
                                            this.UtilService.SetupCopyableElement(this.$scope, deletedStatusElm);
                                        }
                                    }
                                    break;
                                case "Notes":
                                    if (s.Notes.length > 0) {
                                        var notesIcon = angular.element('<a click-event>').append(angular.element('<i>').addClass('fa fa-file-o').attr('title', 'View Notes'));
                                        notesIcon.bind('click',() => {
                                            var modal = this.$modal.open({
                                                template: '<submissions-notes-modal submission-number="submissionNumber" notes="notes" copyable="copyable" modal="modal"></submissions-notes-modal>',
                                                controller: ($scope: ng.IScope) => {
                                                    $scope['submissionNumber'] = s.SubmissionNumber;
                                                    $scope['notes'] = s.Notes;
                                                    $scope['copyable'] = this.copyAll;
                                                    $scope['modal'] = modal;
                                                }
                                            });
                                        });

                                        td.append(notesIcon);
                                    }
                                    break;
                                default:
                                    if (this.dateFields.indexOf(f.PropertyName) >= 0) {
                                        elementText = this.$filter('date')(s[f.PropertyName], 'MM/dd/yyyy');
                                        if (f.PropertyName == 'SubmittedDate') {
                                            td.attr('title', this.$filter('date')(s[f.PropertyName], 'MM/dd/yyyy h:mma'));
                                        }
                                    } else if (this.currencyFields.indexOf(f.PropertyName) >= 0) {
                                        elementText = this.$filter('currency')(s[f.PropertyName], '$');
                                    } else if (!f.IsCustomization) {
                                        elementText = s[f.PropertyName];
                                    } else {
                                        for (var i = 0; i < s.CompanyCustomizations.length; i++) {
                                            if (s.CompanyCustomizations[i].Customization == f.PropertyName) {
                                                if (s.CompanyCustomizations[i].CodeValue != null) {
                                                    elementText = this.$filter('codeItem')(s.CompanyCustomizations[i].CodeValue);
                                                } else {
                                                    elementText = s.CompanyCustomizations[i].TextValue;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    break;
                            }

                            if (elementText != null) {
                                if (this.copyAll) {
                                    var contentElm = angular.element('<span>').text(elementText);
                                    td.append(contentElm);
                                    this.UtilService.SetupCopyableElement(this.$scope, contentElm);
                                } else {
                                    td.text(elementText);
                                }

                                if (this.duplicatesSubmissionWorkflow) {
                                    td.addClass('cursor-pointer');
                                    td.on('click',() => {
                                        checkBox.click();
                                    });
                                }
                            }

                            row.append(td);
                        }

                        container.append(row);
                    });

                    if (this.duplicatesSubmissionWorkflow && footerContainer != null) {
                        row = angular.element('<tr>').addClass('no-duplicates-row');
                        td = angular.element('<td>').addClass('checkbox-column');
                        this.noDuplicateCheckBox = angular.element('<input type="checkbox" id="no-duplicate-applicable-checkbox">')
                            .addClass('checkbox-inline')
                            .click(() => {
                            this.$scope.$apply(() => {
                                this.duplicatesSubmissionWorkflow.NoDuplicatesApplicableChanged();
                            });
                        });

                        td.append(this.noDuplicateCheckBox);
                        row.append(td);

                        td = angular.element('<td>').attr('colspan', this.fields.length);
                        td.append(angular.element('<label for="no-duplicate-applicable-checkbox" class="cursor-pointer">This application is not a duplicate of any of these</label>'));
                        row.append(td);

                        footerContainer.empty();
                        footerContainer.append(row);
                    }
                }
            }
        }

        ExportToCsv() {
            this.actionInProcess = true;
            this.$http.get(this.source.url).then((response: ng.IHttpPromiseCallbackArg<Submission[]>) => {
                this.actionInProcess = false;
                var table = angular.element('<table><thead></thead><tbody></tbody></table>');
                this.renderTableHeader(table.find('thead'), this.fields);
                this.renderTableSubmissions(table.find('tbody'), response.data, null);

                //Break status into two columns
                if (this.fields.filter(f => f.PropertyName == "Status").length > 0) {
                    table.find('thead tr th.Status').after(angular.element('<th>Status Reason</th>'));

                    var tableRows = table.find('tbody tr');
                    for (var i = 0; i < tableRows.length; i++) {
                        var tdStatus = tableRows.eq(i).find('.Status');
                        tdStatus.text(response.data[i].Status);
                        tdStatus.after(angular.element('<td></td>').text(response.data[i].StatusReason));
                    }
                }

                this.tableToCsv(table);
            });
        }
        private tableToCsv(table: JQuery) {
            var rows = <string[]>[];
            angular.forEach(table.find('tr'),(row) => {
                var cols = $(row).find('th,td').toArray().map((col) => this.UtilService.CSVEncode($(col).text()));
                rows.push(cols.join(','));
            });

            var fileName = 'submissions-' + moment().format('YYYY-MM-DD-HH-mm-ss') + '.csv';
            saveAs(new Blob([rows.join('\r\n')], { type: "text/csv" }), fileName);
        }

        OpenSwitchUnderwritingModal() {
            var modal = this.$modal.open({
                template: '<submissions-underwriting-assignment-modal submissions-source="submissionsSource" modal="modal"></submissions-underwriting-assignment-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['submissionsSource'] = this.source;
                    $scope['modal'] = modal;
                }
            });
        }
    }
} 