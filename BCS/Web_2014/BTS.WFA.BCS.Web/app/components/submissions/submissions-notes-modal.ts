﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionsNotesModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/submissions/submissions-notes-modal.html',
            replace: true,
            scope: {
                SubmissionNumber: '=submissionNumber',
                Notes: '=notes',
                Copyable: '=copyable',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: SubmissionsNotesModalController,
            controllerAs: 'vm'
        };
    });

    class SubmissionsNotesModalController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}