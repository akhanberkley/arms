﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionsUnderwritingAssignmentModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/submissions/submissions-underwriting-assignment-modal.html',
            replace: true,
            scope: {
                SubmissionsSource: '=submissionsSource',
                Notes: '=notes',
                Copyable: '=copyable',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: SubmissionsUnderwritingAssignmentModalController,
            controllerAs: 'vm'
        };
    });

    class SubmissionsUnderwritingAssignmentModalController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;
        SubmissionsSource: PagedDataSource;
        ReassignmentForm: ValidatedForm;

        ReassignInProgress: boolean;
        Submissions: SubmissionId[];
        SubmissionFields: SubmissionDisplayField[];
        SearchWorkflow: SearchWorkflow;
        ReassignUnderwriter: boolean;
        SelectedUnderwriter: UnderwritingPersonnel;
        ReassignUnderwritingAnalyst: boolean;
        SelectedUnderwritingAnalyst: UnderwritingPersonnel;
        ReassignUnderwritingTechnician: boolean;
        SelectedUnderwritingTechnician: UnderwritingPersonnel;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private AlertService: AlertService, private $http: ng.IHttpService, DomainService: DomainService, SearchWorkflowService: SearchWorkflowService) {
            super($scope, $state, UtilService);

            this.SearchWorkflow = SearchWorkflowService.GetWorkflow(this.companyKey, false, false);
            this.ReassignUnderwriter = true;
            this.SubmissionFields = [
                { DisplayName: "Submission Number", Header: "Sub #", PropertyName: "SubmissionNumber", IsCustomization: false },
                { DisplayName: "Sequence Number", Header: "Seq", PropertyName: "SequenceNumber", IsCustomization: false }
            ];

            //Not returning any data, just getting the count
            $http.get(this.SubmissionsSource.url + '&idsOnly=true').then((response: ng.IHttpPromiseCallbackArg<SubmissionId[]>) => {
                this.Submissions = response.data;
            });
        }

        Reassign() {
            var reassignment = <SubmissionUnderwritingPersonnelReassignment>{ Submissions: this.Submissions };
            this.UtilService.SetValidationResults(this.ReassignmentForm);
            if (!this.ReassignUnderwriter && !this.ReassignUnderwritingAnalyst && !this.ReassignUnderwritingTechnician) {
                this.ReassignmentForm.ValidationErrors.push({ Property: 'Role', Message: 'Please select at least one role to reassign' });
            } else {
                var confirmationMessageParts = <string[]>[];
                if (this.ReassignUnderwriter) {
                    if (this.SelectedUnderwriter == null) {
                        this.ReassignmentForm.ValidationErrors.push({ Property: 'Underwriter', Message: `Please select the ${this.SearchWorkflow.UnderwritersLabel} to reassign` });
                    } else {
                        reassignment.Underwriter = this.SelectedUnderwriter;
                        confirmationMessageParts.push(`${this.SearchWorkflow.UnderwritersLabel} to ${reassignment.Underwriter.FullName}`);
                    }
                }
                if (this.ReassignUnderwritingAnalyst) {
                    if (this.SelectedUnderwritingAnalyst == null) {
                        this.ReassignmentForm.ValidationErrors.push({ Property: 'UnderwritingAnalyst', Message: `Please select the ${this.SearchWorkflow.UnderwritingAnalystLabel} to reassign` });
                    } else {
                        reassignment.UnderwritingAnalyst = this.SelectedUnderwritingAnalyst;
                        confirmationMessageParts.push(`${this.SearchWorkflow.UnderwritingAnalystLabel} to ${reassignment.UnderwritingAnalyst.FullName}`);
                    }
                }
                if (this.ReassignUnderwritingTechnician) {
                    if (this.SelectedUnderwritingTechnician == null) {
                        this.ReassignmentForm.ValidationErrors.push({ Property: 'UnderwritingTechnician', Message: `Please select the ${this.SearchWorkflow.UnderwritingTechnicianLabel} to reassign` });
                    } else {
                        reassignment.UnderwritingTechnician = this.SelectedUnderwritingTechnician;
                        confirmationMessageParts.push(`${this.SearchWorkflow.UnderwritingTechnicianLabel} to ${reassignment.UnderwritingTechnician.FullName}`);
                    }
                }

                if (this.ReassignmentForm.ValidationErrors.length == 0) {
                    if (confirm(`Click OK to update ${reassignment.Submissions.length} submissions to assign ` + confirmationMessageParts.join(', '))) {
                        this.ReassignInProgress = true;
                        this.$http.put(`/api/${this.companyKey}/Submission/UnderwritingPersonnel`, reassignment).then((result) => {
                            this.AlertService.Notify(NotificationType.Success, 'Underwriting personnel assignments have been updated');
                            this.Close();
                            this.SubmissionsSource.changePage(this.SubmissionsSource.page);
                        },(result) => {
                                this.UtilService.SetValidationResults(this.ReassignmentForm, <ValidationResult[]>result.data);
                            })
                            .finally(() => { this.ReassignInProgress = false; });
                    }
                }
            }
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}