﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('companyFields',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/company-fields.html',
            replace: true,
            controller: CompanyFieldsController,
            controllerAs: 'vm'
        }
    });

    class CompanyFieldsController extends BaseController {
        companies: CompanyConfiguration[];
        clientFields: string[];
        clientCompanies: any[];
        submissionFields: string[];
        submissionCompanies: any[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, $http: ng.IHttpService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Company Fields');
            this.AppState.PageGenerating = true;

            $http.get('/api/Company/Configuration').then((response: ng.IHttpPromiseCallbackArg<CompanyConfiguration[]>) => {
                this.companies = response.data;

                this.clientFields = Object.keys(this.companies[0].Configuration.ClientFields);
                this.clientCompanies = this.companies.map((c) => {
                    return {
                        Abbreviation: c.Summary.Abbreviation,
                        Description: c.Summary.CompanyNumber + ' - ' + c.Summary.CompanyName,
                        Type: c.Configuration.ClientFields
                    }
                });

                this.submissionFields = Object.keys(this.companies[0].Configuration.SubmissionTypes[0]).filter((f) => { return ['TypeName', 'TypeIsDefault', 'SummaryField', 'TypeIsRenewal'].indexOf(f) < 0; });
                this.submissionCompanies = [];
                var altCompanyRow = false;
                angular.forEach(this.companies,(c) => {
                    var firstForCompany = true;
                    angular.forEach(c.Configuration.SubmissionTypes, (t) => {
                        this.submissionCompanies.push({
                            FirstCompanyRow: firstForCompany,
                            AlternateRow: altCompanyRow,
                            RowCount: c.Configuration.SubmissionTypes.length,
                            Abbreviation: c.Summary.Abbreviation,
                            Description: c.Summary.CompanyNumber + ' - ' + c.Summary.CompanyName,
                            Type: t
                        });
                        firstForCompany = false;
                    });

                    altCompanyRow = !altCompanyRow;
                });

                this.AppState.PageGenerating = false;
            });
        }
    }
}
