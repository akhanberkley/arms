﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('administrationClassCodes',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/class-code/class-codes.html',
            replace: true,
            controller: AdministrationClassCodesController,
            controllerAs: 'vm'
        }
    });

    class AdministrationClassCodesController extends BaseController {
        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private SearchService: SearchService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Class Code Administration');
            if (!$state.params['classCode']) {
                UtilService.RaiseFocusTrigger('ExistingClassCodesContainer');
            }
        }

        ClassCodeSearch = (term: string) => {
            return this.SearchService.AutoComplete.ClassCodes(this.companyKey, term);
        }

        SelectClassCode(classCode: ClassCode) {
            this.$state.go('cn.administration.classcodes.classcode', { classCode: (classCode) ? classCode.CodeValue : 'new' }, { reload: true });
        }
    }
}