﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('classCode',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/class-code/class-code.html',
            replace: true,
            scope: {
                codeName: '@code'
            },
            bindToController: true,
            controller: ClassCodeController,
            controllerAs: 'vm'
        }
    });

    class ClassCodeController extends BaseController {
        codeName: string;
        ClassCodeForm: ValidatedForm;
        Item: ClassCode;
        itemsOriginalCodeValue: string;
        IsNew = false;
        HazardGradeTypes: string[];
        CompanyCustomizations: CompanyCustomization[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $http: ng.IHttpService, DomainService: DomainService,
            private SearchService: SearchService, private CompanyCustomizationService: CompanyCustomizationService, $q: ng.IQService, private AlertService: AlertService, $timeout: ng.ITimeoutService) {
            super($scope, $state, UtilService);

            var domainPromises = <ng.IPromise<any>[]>[];
            domainPromises.push(DomainService.HazardGradeTypes(this.companyKey).then((data) => {
                this.HazardGradeTypes = data;
            }));
            domainPromises.push(DomainService.CompanyCustomizations(this.companyKey, 'ClassCode').then((data) => {
                this.CompanyCustomizations = data;
            }));

            $scope.$watch(() => this.codeName,(codeName) => {
                this.AppState.PageGenerating = true;
                $q.all(domainPromises).then(() => {
                    //wrapping this in a $timeout since it seems to be having display issues in 1.4 w/ the initial load
                    $timeout(() => {
                        this.AppState.PageGenerating = false;
                        if (codeName) {
                            this.IsNew = (codeName == 'new');
                            if (this.IsNew) {
                                this.prepAndBindClassCode({ HazardGrades: [], CompanyCustomizations: [] }, true);
                                UtilService.RaiseFocusTrigger('CodeValue');
                            } else {
                                this.AppState.Loading = true;
                                $http.get('/api/' + this.companyKey + '/ClassCode?$filter=CodeValue eq ' + UtilService.ODataString(codeName)).then((response: ng.IHttpPromiseCallbackArg<ClassCode[]>) => {
                                    if (response.data != null && response.data.length > 0) {
                                        this.prepAndBindClassCode(response.data[0], false);
                                    }

                                    UtilService.RaiseFocusTrigger('CodeValue');
                                    this.AppState.Loading = false;
                                });
                            }
                        } else {
                            this.IsNew = false;
                        }
                    });
                });
            });
        }

        private prepAndBindClassCode(classCode: ClassCode, setDefaults: boolean) {
            var hazardGrades = <CodeItem[]>[];
            angular.forEach(this.HazardGradeTypes,(h) => {
                var value = <string>null;
                for (var i = 0; i < classCode.HazardGrades.length; i++) {
                    if (classCode.HazardGrades[i].Code == h) {
                        value = classCode.HazardGrades[i].Description;
                        break;
                    }
                }
                hazardGrades.push({ Code: h, Description: value });
            });

            classCode.HazardGrades = hazardGrades;
            classCode.CompanyCustomizationBindings = this.CompanyCustomizationService.CreateBindings(this.CompanyCustomizations, classCode.CompanyCustomizations, setDefaults);

            this.itemsOriginalCodeValue = classCode.CodeValue;
            this.Item = classCode;
        }

        SicCodeSearch = (term: string) => {
            return this.SearchService.AutoComplete.SicCodes(term);
        }
        NaicsCodeSearch = (term: string) => {
            return this.SearchService.AutoComplete.NaicsCodes(term);
        }
        Save() {
            this.Item.CompanyCustomizations = this.CompanyCustomizationService.GetValues(this.Item.CompanyCustomizationBindings);

            this.AppState.Loading = true;
            var request = <ng.IHttpPromise<ClassCode>>this.UtilService.CreateSaveRequest(this.Item, '/api/' + this.companyKey + '/ClassCode', this.itemsOriginalCodeValue);
            request.success((data, status, headers, config) => {
                this.AppState.Loading = false;
                this.$state.go('cn.administration.classcodes.classcode', { classCode: data.CodeValue });
                this.AlertService.Notify(NotificationType.Success, `Class Code "${data.CodeValue}" has been ${ this.IsNew ? 'added' : 'updated' }`);
            }).error((data, status, headers, config) => {
                this.UtilService.SetValidationResults(this.ClassCodeForm, data);
                this.AppState.Loading = false;
            });
        }
        Delete() {
            if (confirm('Are you sure you want to delete this class code?')) {
                this.AppState.Loading = true;
                this.$http.delete('/api/' + this.companyKey + '/ClassCode/' + this.itemsOriginalCodeValue).success((classCode, status, headers, config) => {
                    this.AppState.Loading = false;
                    this.$state.go('cn.administration.classcodes');
                }).error((data, status, headers, config) => {
                    this.UtilService.SetValidationResults(this.ClassCodeForm, data);
                    this.AppState.Loading = false;
                });
            };
        }
    }
}