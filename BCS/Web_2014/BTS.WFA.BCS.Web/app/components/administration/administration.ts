﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('administration',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/administration.html',
            replace: true,
            controller: AdministrationController,
            controllerAs: 'vm'
        }
    });

    class AdministrationController extends BaseController {
        Menu: AdministrationLink[];
        SystemAdminMenu: AdministrationLink[];
        private sysAdminStateNames: string[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, UserService: UserService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Administration');

            UserService.UserPromise.then((user) => {
                this.Menu = user.AdministratorLinks;
                if (user.IsSystemAdministrator) {
                    this.SystemAdminMenu =
                    [
                        { state: 'cn.administration.underwritingunits', name: 'Underwriting Units' },
                        { state: 'cn.administration.apssync', name: 'APS Sync' },
                        { state: 'cn.administration.viewsettings', name: 'Company Settings' },
                        { state: 'cn.administration.viewfields', name: 'Company Fields' },
                        { state: 'cn.administration.testclientsystem', name: 'Tests - Client System' },
                        { state: 'cn.administration.testserver', name: 'Tests - Exceptions' },
                        { state: 'cn.administration.testvalidation', name: 'Tests - Validation' }
                    ];

                    this.sysAdminStateNames = this.SystemAdminMenu.map((m) => { return m.state; });
                }

                if ($state.is('cn.administration')) {
                    var firstState = this.Menu.concat(this.SystemAdminMenu)[0].state;
                    $state.go(firstState, null, { location: 'replace' });
                }
            });
        }

        OnSystemAdminState() {
            return (this.sysAdminStateNames.indexOf(this.$state.current.name) > -1);
        }
    }
}