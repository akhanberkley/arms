﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('agencySync',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/aps-sync.html',
            replace: true,
            controller: AgencySyncController,
            controllerAs: 'vm'
        }
    });

    class AgencySyncController extends BaseController {
        SyncAgencyCode: string;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $http: ng.IHttpService,
            private AlertService: AlertService, private AgencyService: AgencyService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Agency Sync');
        }

        SyncPersonnel() {
            this.AppState.Loading = true;
            this.$http.post('/api/' + this.companyKey + '/Underwriting/Sync', null).then((response: ng.IHttpPromiseCallbackArg<string[]>) => {
                this.AlertService.OpenStandardModal({ title: 'Personnel Sync', message: response.data.join("<br />") });
                this.AppState.Loading = false;
            });
        }

        SyncAgency() {
            this.AppState.Loading = true;
            this.AgencyService.SyncAgency(this.companyKey, this.SyncAgencyCode).then((results) => {
                this.AlertService.OpenStandardModal({ title: 'Agency Sync', message: results.Message });
                this.AppState.Loading = false;
            });
        }
    }
}