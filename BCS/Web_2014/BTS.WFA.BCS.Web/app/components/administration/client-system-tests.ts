﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('clientSystemTests',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/client-system-tests.html',
            replace: true,
            controller: ClientSystemTestsController,
            controllerAs: 'vm'
        }
    });

    class ClientSystemTestsController extends BaseController {
        TestClientName: string;
        TestClientState: string;
        States: State[];
        Companies: CompanyTest[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private DomainService: DomainService, public $http: ng.IHttpService, public SearchService: SearchService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Client System Tests');
            this.TestClientName = 'scottz';
            this.TestClientState = 'IA';

            DomainService.States().then((states) => this.States = states);
            this.ResetTests();
        }

        ResetTests() {
            this.DomainService.Companies().then((companies) => {
                this.Companies = [];
                angular.forEach(companies,(c) => {
                    this.Companies.push(new CompanyTest(this, c));
                });
            });
        }

        CanRunAllSearch() {
            if (this.Companies == null) {
                return false;
            } else {
                return (this.Companies.filter((c) => c.SearchSucceeded == null).length > 0);
            }
        }
        RunAllSearch() {
            angular.forEach(this.Companies,(c) => {
                if (c.SearchSucceeded == null && !c.SearchRunning) {
                    c.RunSearch();
                }
            });
        }

        CanRunAllGet() {
            if (this.Companies == null) {
                return false;
            } else {
                return (this.Companies.filter((c) => c.SearchSucceeded != null && c.GetSucceeded == null).length > 0);
            }
        }
        RunAllGet() {
            angular.forEach(this.Companies,(c) => {
                if (c.GetSucceeded == null && !c.GetRunning) {
                    c.RunGet();
                }
            });
        }

        CanRunAllSave() {
            if (this.Companies == null) {
                return false;
            } else {
                return (this.Companies.filter((c) => c.GetSucceeded != null && c.SaveSucceeded == null).length > 0);
            }
        }
        RunAllSave() {
            angular.forEach(this.Companies,(c) => {
                if (c.SaveSucceeded == null && !c.SaveRunning) {
                    c.RunSave();
                }
            });
        }
    }

    class CompanyTest {
        SearchRunning: boolean;
        SearchSucceeded: boolean;
        GetRunning: boolean;
        GetSucceeded: boolean;
        SaveRunning: boolean;
        SaveSucceeded: boolean;
        ClientId: number;
        Client: Client;

        constructor(private Controller: ClientSystemTestsController, private Summary: CompanySummary) {
            this.SearchRunning = false;
            this.GetRunning = false;
            this.SaveRunning = false;
        }

        RunSearch() {
            this.SearchRunning = true;

            var config = this.Controller.SearchService.Search.GetClientHttpInfo(this.Summary.Abbreviation, { BusinessName: this.Controller.TestClientName, State: this.Controller.TestClientState });
            this.Controller.$http.get(config.url, { params: config.params }).success((results: ClientSearchResults) => {
                if (results.Failures.length == 0) {
                    this.SearchSucceeded = true;
                    if (results.Matches.length > 0) {
                        this.ClientId = results.Matches[0].ClientCoreId;
                    }
                } else {
                    this.SearchSucceeded = false;
                }
            }).finally(() => {
                this.SearchRunning = false;
            });
        }

        RunGet() {
            if (this.ClientId == null) {
                this.GetSucceeded = false;
            } else {
                this.GetRunning = true;
                this.Controller.SearchService.ClientService.Get(this.Summary.Abbreviation, this.ClientId).then((result) => {
                    this.GetSucceeded = (result.Client != null);
                    this.Client = result.Client;
                    this.GetRunning = false;
                });
            }
        }

        RunSave() {
            if (this.Client == null) {
                this.SaveSucceeded = false;
            } else {
                this.SaveRunning = true;
                this.Controller.SearchService.ClientService.Save(this.Summary.Abbreviation, this.ClientId, this.Client, false, false).then((result) => {
                    this.SaveSucceeded = (result.Client != null);
                    this.SaveRunning = false;
                });
            }
        }
    }
}