﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('otherCarriersAdministration',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/other-carriers.html',
            replace: true,
            controller: OtherCarriersAdministrationController,
            controllerAs: 'vm'
        }
    });

    class OtherCarriersAdministrationController extends BaseController {
        CompanyOptions: CodeItem[];
        AvailableOptions: CodeItem[];
        OptionsFilter: string;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, $q: ng.IQService, private $http: ng.IHttpService, private DomainService: DomainService,
            private AlertService: AlertService, private $filter: ng.IFilterService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Other Carriers Administration');
            this.AppState.Loading = true;

            var allOptions = <CodeItem[]>null;
            var requests = <ng.IPromise<any>[]>[];

            requests.push($http.get(`/api/Carrier`).then((results: ng.IHttpPromiseCallbackArg<CodeItem[]>) => { allOptions = results.data; }));
            requests.push(DomainService.Carriers(this.companyKey).then((carriers) => this.CompanyOptions = angular.copy(carriers)));

            $q.all(requests).then(() => {
                this.AvailableOptions = [];
                allOptions.forEach((o) => {
                    if (this.CompanyOptions.filter((co) => co.Code == o.Code).length == 0) {
                        this.AvailableOptions.push(o);
                    }
                });

                this.AppState.Loading = false;
            });
        }

        RemoveCarrier(carrier: CodeItem) {
            this.CompanyOptions.splice(this.CompanyOptions.indexOf(carrier), 1);

            this.AvailableOptions.push(carrier);
            this.AvailableOptions = this.$filter('orderBy')(this.AvailableOptions, 'Description');
        }
        AddCarrier(carrier: CodeItem) {
            this.AvailableOptions.splice(this.AvailableOptions.indexOf(carrier), 1);

            this.CompanyOptions.push(carrier);
            this.CompanyOptions = this.$filter('orderBy')(this.CompanyOptions, 'Description');
        }

        Save() {
            this.$http.put(`/api/${this.companyKey}/Carrier`, this.CompanyOptions).then((results) => {
                this.AlertService.Notify(NotificationType.Success, "Carrier Options have been saved");
                this.DomainService.ResetCache();
                this.OptionsFilter = '';
            });
        }
    }
} 