﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('companyCustomizations',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/company-customization/company-customizations.html',
            replace: true,
            controller: CompanyCustomizationsController,
            controllerAs: 'vm'
        }
    });

    class CompanyCustomizationsController extends BaseController {
        CustomizationList: CompanyCustomization[];
        DropDownSelectedCustomization: CompanyCustomization;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, DomainService: DomainService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Custom Field Administration');
            this.AppState.PageGenerating = true;

            DomainService.CompanyCustomizations(this.companyKey, null).then((data) => {
                this.CustomizationList = data;
                this.AppState.PageGenerating = false;

                if ($state.is('cn.administration.companycustomizations')) {
                    UtilService.RaiseFocusTrigger('ExistingCustomizationsContainer');
                } else {
                    var selectedCustomization = $state.params['customization'];
                    if (selectedCustomization != null) {
                        var matchingCustomizations = data.filter((c) => c.Description == selectedCustomization);
                        if (matchingCustomizations.length > 0) {
                            this.DropDownSelectedCustomization = matchingCustomizations[0];
                        }
                    }
                }
            });
        }

        NavigateToSort() {
            this.$state.go('cn.administration.companycustomizations.sort');
        }

        SelectCustomization(description: string) {
            this.$state.go('cn.administration.companycustomizations.customization', { customization: description }, { reload: true });
        }
    }
}