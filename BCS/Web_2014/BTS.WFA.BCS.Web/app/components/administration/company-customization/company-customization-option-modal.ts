﻿/// <reference path="../../../app.ts" />

module bcs {
    'use strict';
    app.directive('administrationCustomizationOptionModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/company-customization/company-customization-option-modal.html',
            replace: true,
            scope: {
                CustomizationName: '=customizationName',
                Option: '=option',
                IsNew: '=isNew',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: AdministrationCustomizationOptionController,
            controllerAs: 'vm'
        };
    });

    class AdministrationCustomizationOptionController extends BaseController {
        OptionForm: ValidatedForm;
        Option: CompanyCustomizationOption;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;
        SavingOption: boolean;
        CustomizationName: string;
        OriginalCodeName: string;
        IsNew: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);

            this.OriginalCodeName = (this.IsNew) ? null : this.Option.Code;
            UtilService.RaiseFocusTrigger('Code');
        }

        SaveOption() {
            this.SavingOption = true;
            var request = <ng.IHttpPromise<CompanyCustomizationOption>>this.UtilService.CreateSaveRequest(this.Option, '/api/' + this.companyKey + '/CompanyCustomization/' + this.CustomizationName + '/Option', this.OriginalCodeName);

            request.success((data, status, headers, config) => {
                this.SavingOption = false;
                this.ModalInstance.close(this.Option);
            }).error((data, status, headers, config) => {
                this.UtilService.SetValidationResults(this.OptionForm, data);
                this.SavingOption = false;
            });
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}