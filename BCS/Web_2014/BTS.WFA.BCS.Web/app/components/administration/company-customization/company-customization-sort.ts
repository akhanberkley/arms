﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('companyCustomizationsSort',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/company-customization/company-customization-sort.html',
            replace: true,
            controller: CompanyCustomizationsSortController,
            controllerAs: 'vm'
        }
    });

    class CompanyCustomizationsSortController extends BaseController {
        SortClientList: CompanyCustomization[];
        SortClassCodeList: CompanyCustomization[];
        SortSubmissionList: CompanyCustomization[];
        SortSettings = <ng.ui.SortableEvents<CompanyCustomization>>{};

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, DomainService: DomainService, private $http: ng.IHttpService, private AlertService: AlertService) {
            super($scope, $state, UtilService);

            DomainService.CompanyCustomizations(this.companyKey, null).then((data) => {
                this.SortClientList = data.filter((c) => c.Usage == 'Client');
                this.SortClassCodeList = data.filter((c) => c.Usage == 'ClassCode');
                this.SortSubmissionList = data.filter((c) => c.Usage == 'Submission');
            });
        }

        SaveOrder(category: string, customizations: CompanyCustomization[]) {
            this.AppState.Loading = true;
            var descriptions = customizations.map((i) => i.Description);
            this.$http.put('/api/' + this.companyKey + '/CompanyCustomization/Reorder', descriptions).then(() => {
                this.AlertService.Notify(NotificationType.Success, category + " customization order has been saved");
                this.AppState.Loading = false;
            });
        }
    }
}