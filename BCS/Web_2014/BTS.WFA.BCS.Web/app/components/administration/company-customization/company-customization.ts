﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('companyCustomization',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/company-customization/company-customization.html',
            replace: true,
            scope: {
                customizationName: '@customization'
            },
            bindToController: true,
            controller: CompanyCustomizationController,
            controllerAs: 'vm'
        }
    });

    class CompanyCustomizationController extends BaseController {
        customizationName: string;
        CustomizationForm: ValidatedForm;
        SelectedCustomization: CompanyCustomization;
        OriginalDescription = <string>null;
        OriginalDataType: string;
        OptionsFilter = <string>null;
        IsNew = false;
        UsageTypes = [
            { Description: 'Client', Code: 'Client' },
            { Description: 'Submission', Code: 'Submission' },
            { Description: 'Class Code', Code: 'ClassCode' }
        ];
        DataTypes = [
            { Description: 'List', Code: 'List' },
            { Description: 'Text', Code: 'Text' },
            { Description: 'Date', Code: 'Date' },
            { Description: 'Boolean', Code: 'Boolean' },
            { Description: 'Money', Code: 'Money' }
        ];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private DomainService: DomainService, private $modal: ng.ui.bootstrap.IModalService, private AlertService: AlertService, $timeout: ng.ITimeoutService) {
            super($scope, $state, UtilService);

            $scope.$watch(() => this.customizationName,(customizationName) => {
                if (customizationName) {
                    UtilService.SetValidationResults(this.CustomizationForm);
                    this.IsNew = (customizationName == 'new');
                    this.OptionsFilter = '';

                    if (this.IsNew) {
                        this.OriginalDescription = null;
                        this.SelectedCustomization = { Description: null, ExpirationDate: null, Options: [], DefaultOption: null, Required: false, ReadOnly: false, Usage: 'Client', DataType: 'Text', VisibleOnAdd: true, ExcludeValueOnCopy: false, VisibleOnBPM: true };
                        UtilService.RaiseFocusTrigger('Description');
                    } else {
                        DomainService.CompanyCustomizations(this.companyKey, null).then((data) => {
                            //wrapping this in a $timeout since it seems to be having display issues in 1.4 w/ the initial load if it's a list customization (options list is hidden)
                            $timeout(() => {
                                var matchedCustomizations = data.filter((c) => c.Description == customizationName);
                                if (matchedCustomizations.length == 1) {
                                    this.SelectedCustomization = angular.copy(matchedCustomizations[0]);
                                    this.OriginalDataType = this.SelectedCustomization.DataType;
                                    this.OriginalDescription = this.SelectedCustomization.Description;
                                    UtilService.RaiseFocusTrigger('Description');
                                }
                            });
                        });
                    }
                } else {
                    this.IsNew = false;
                }
            });
        }

        SaveCustomization() {
            this.AppState.Loading = true;
            var request = <ng.IHttpPromise<CompanyCustomization>>this.UtilService.CreateSaveRequest(this.SelectedCustomization, '/api/' + this.companyKey + '/CompanyCustomization', this.OriginalDescription);

            request.success((data, status, headers, config) => {
                this.UtilService.SetValidationResults(this.CustomizationForm);
                this.AppState.Loading = false;
                this.DomainService.ResetCache();
                this.$state.go('cn.administration.companycustomizations.customization', { customization: data.Description }, { reload: true });
                this.AlertService.Notify(NotificationType.Success, `Custom Field "${data.Description}" has been ${ this.IsNew ? 'added' : 'updated' }`);
            }).error((data, status, headers, config) => {
                this.UtilService.SetValidationResults(this.CustomizationForm, data);
                this.AppState.Loading = false;
            });
        }

        OpenOptionModal(option: CompanyCustomizationOption) {
            var isNew = (option == null);
            if (isNew) {
                option = { Code: '', Description: '' };
            }

            var modal = this.$modal.open({
                template: '<administration-customization-option-modal customization-name="customizationName" option="option" is-new="isNew" modal="modal"></administration-customization-option-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['customizationName'] = this.OriginalDescription;
                    $scope['option'] = angular.copy(option);
                    $scope['isNew'] = isNew;
                    $scope['modal'] = modal;
                }
            });

            modal.result.then((updatedOption) => {
                this.AlertService.Notify(NotificationType.Success, `Option ${(isNew) ? 'created' : 'updated'}`);

                if (isNew) {
                    this.SelectedCustomization.Options.push(updatedOption);
                } else {
                    var optionIndex = this.SelectedCustomization.Options.indexOf(option);
                    this.SelectedCustomization.Options[optionIndex] = updatedOption;
                }
            });
        }
    }
}