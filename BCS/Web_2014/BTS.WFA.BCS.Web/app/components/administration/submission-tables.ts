﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionTablesAdministration',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/submission-tables.html',
            replace: true,
            controller: SubmissionTablesAdministrationController,
            controllerAs: 'vm'
        }
    });

    class SubmissionTablesAdministrationController extends BaseController {
        Tables: CodeItem[];
        SelectedTable: CodeItem;
        Fields: SubmissionDisplayField[];
        SelectedTableFields: SubmissionDisplayField[];
        SelectedTableAvailableFields: SubmissionDisplayField[];
        SortSettings = <ng.ui.SortableEvents<CompanyCustomization>>{};

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $http: ng.IHttpService, private DomainService: DomainService,
            private AlertService: AlertService, private $filter: ng.IFilterService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Submission Table Administration');

            $http.get(`/api/${this.companyKey}/SubmissionDisplay/Table`).then((results: ng.IHttpPromiseCallbackArg<CodeItem[]>) => {
                this.Tables = results.data;
            });

            $http.get(`/api/${this.companyKey}/SubmissionDisplay/Field`).then((results: ng.IHttpPromiseCallbackArg<SubmissionDisplayField[]>) => {
                this.Fields = results.data.filter(f => f.PropertyName != 'AllowMultipleSubmissions');
            });
        }

        SelectedTableChanged() {
            if (this.SelectedTable) {
                this.AppState.Loading = true;
                this.DomainService.SubmissionSummaryFields(this.companyKey, this.SelectedTable.Code).then((fields) => {
                    this.SelectedTableFields = angular.copy(fields);

                    this.SelectedTableAvailableFields = [];
                    angular.forEach(this.Fields,(f) => {
                        if (this.SelectedTableFields.filter((tf) => tf.Header == f.Header).length == 0) {
                            this.SelectedTableAvailableFields.push(f);
                        }
                    });

                    this.AppState.Loading = false;
                });
            }
        }

        RemoveField(field: SubmissionDisplayField) {
            this.SelectedTableFields.splice(this.SelectedTableFields.indexOf(field), 1);

            this.SelectedTableAvailableFields.push(field);
            this.SelectedTableAvailableFields = this.$filter('orderBy')(this.SelectedTableAvailableFields, 'Header');
        }
        AddField(field: SubmissionDisplayField) {
            this.SelectedTableAvailableFields.splice(this.SelectedTableAvailableFields.indexOf(field), 1);
            this.SelectedTableFields.push(field);
        }

        Save() {
            this.$http.put(`/api/${this.companyKey}/SubmissionDisplay/Table/${this.SelectedTable.Code}`, this.SelectedTableFields).then((results) => {
                this.AlertService.Notify(NotificationType.Success, "Table Fields have been saved");
                this.DomainService.ResetCache();
            });

        }
    }
} 