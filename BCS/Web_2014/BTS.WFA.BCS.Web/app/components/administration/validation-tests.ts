﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('validationTests',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/validation-tests.html',
            replace: true,
            controller: ValidationTestsController,
            controllerAs: 'vm'
        }
    });

    class ValidationTestsController extends BaseController {
        InvalidControlsForm: ValidatedForm;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Validation Tests');
        }

        ResetValidations () {
            this.UtilService.SetValidationResults(this.InvalidControlsForm);
        }

        InvalidControls () {
            var validations = [
                { Property: 'InputTextBox', Message: 'Error' },
                { Property: 'InputSelect', Message: 'Error' },
                { Property: 'InputWithIcon', Message: 'Icon Error Message' },
                { Property: 'InputWithMessage', Message: 'Displayed Error Message' }
            ];
            this.UtilService.SetValidationResults(this.InvalidControlsForm, validations);
        }
    }
}