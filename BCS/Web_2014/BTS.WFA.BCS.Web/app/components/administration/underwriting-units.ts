﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('underwritingUnits',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/underwriting-units.html',
            replace: true,
            controller: UnderwritingUnitsController,
            controllerAs: 'vm'
        }
    });

    class UnderwritingUnitsController extends BaseController {
        UnderwritingUnits: UnderwritingUnit[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, $http: ng.IHttpService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Underwriting Units');

            this.AppState.PageGenerating = true;
            $http.get('/api/' + this.companyKey + '/Underwriting/Unit').then((response: ng.IHttpPromiseCallbackArg<UnderwritingUnit[]>) => {
                this.UnderwritingUnits = response.data;
                this.AppState.PageGenerating = false;
            });
        }
    }
}