﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('users',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/user/users.html',
            replace: true,
            controller: UsersController,
            controllerAs: 'vm'
        }
    });

    class UsersController extends BaseController {
        Roles: CodeItem[];
        User: User;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private UserService: UserService,
            private SearchService: SearchService, DomainService: DomainService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('User Administration');
            DomainService.UserRoles(this.companyKey).then((data) => { this.Roles = data; });
            UserService.UserPromise.then((user) => { this.User = user; });

            if ($state.is('cn.administration.users')) {
                UtilService.RaiseFocusTrigger('UserSearchInput');
            }
        }

        UserSearch = (term: string) => {
            return this.SearchService.AutoComplete.Users(this.companyKey, this.User.UserName, this.User.IsSystemAdministrator, term);
        };

        SelectUser(user: User) {
            this.$state.go('cn.administration.users.user', { userId: (user) ? user.Id : 'new' }, { reload: true });
        }

        GenerateUsersReport() {
            this.UserService.GetAllActiveCompanyUsers(this.companyKey).then((users) => {
                var rows = <string[]>[];

                var headerRow = [
                    this.UtilService.CSVEncode('User Name'),
                    this.UtilService.CSVEncode('User Type'),
                    this.UtilService.CSVEncode('Limiting Agency'),
                    this.UtilService.CSVEncode('Notes')];
                angular.forEach(this.Roles,(r) => { headerRow.push(this.UtilService.CSVEncode(r.Code)); });

                rows.push(headerRow.join(','));
                angular.forEach(users,(user) => {
                    var userRow = [this.UtilService.CSVEncode(user.UserName), this.UtilService.CSVEncode(user.UserType)];

                    userRow.push(this.UtilService.CSVEncode((user.LimitingAgency) ? user.LimitingAgency.Code : null));
                    userRow.push(this.UtilService.CSVEncode(user.Notes));


                    angular.forEach(this.Roles,(r) => {
                        userRow.push((user.Roles.indexOf(r.Code) >= 0) ? 'Yes' : '');
                    });

                    rows.push(userRow.join(','));
                });

                var fileName = 'userlist-' + moment().format('YYYY-MM-DD-HH-mm-ss') + '.csv';
                saveAs(new Blob([rows.join('\r\n')], { type: "text/csv" }), fileName);
            });
        }
    }
}