﻿/// <reference path="../../../app.ts" />
module bcs {
    'use strict';
    app.directive('user',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/user/user.html',
            replace: true,
            scope: {
                id: '@'
            },
            bindToController: true,
            controller: UserController,
            controllerAs: 'vm'
        }
    });

    class UserController extends BaseController {
        id: string;
        UserForm: ValidatedForm;
        SelectedUser: User;
        IsNew: boolean;
        Roles: CodeItem[];
        UserTypes: string[];
        Companies: CompanySummary[];
        CompanyConfigurations: CompanyConfiguration[];
        ClientCoreSegments: ClientCoreSegment[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, DomainService: DomainService, $q: ng.IQService,
            private UserService: UserService, private AlertService: AlertService, private SearchService: SearchService) {
            super($scope, $state, UtilService);

            this.AppState.PageGenerating = true;
            var domains = <ng.IPromise<any>[]>[];
            domains.push(DomainService.UserTypes().then((data) => { this.UserTypes = data; }));
            domains.push(DomainService.UserRoles(this.companyKey).then((data) => { this.Roles = data; }));
            domains.push(DomainService.CompanyConfigurations().then((configurations) => { this.CompanyConfigurations = configurations; }));
            domains.push(UserService.UserPromise.then((user) => {
                if (user.IsSystemAdministrator) {
                    DomainService.Companies().then((data) => { this.Companies = data; });
                }
            }));

            $q.all(domains).then(() => { this.AppState.PageGenerating = false; });

            $scope.$watch(() => this.id,(id) => {
                $q.all(domains).then(() => {
                    if (id) {
                        UtilService.SetValidationResults(this.UserForm);
                        this.IsNew = (id == 'new');

                        if (this.IsNew) {
                            UserService.UserPromise.then((user) => {
                                var defaultCompany = angular.copy(user.SelectedCompany);
                                this.SelectedUser = { Id: null, UserName: null, IsSystemAdministrator: false, IsReadOnly: false, Active: true, UserType: 'User', Companies: [defaultCompany], Roles: [], DefaultCompany: defaultCompany };
                                this.DefaultCompanyChanged();
                                this.AppState.PageGenerating = false;
                                UtilService.RaiseFocusTrigger('UserName');
                            });
                        } else {
                            UserService.Get(this.companyKey, id).then((user) => {
                                this.SelectedUser = user;
                                this.DefaultCompanyChanged();
                                this.AppState.PageGenerating = false;
                            });
                        }
                    } else {
                        this.IsNew = false;
                    }
                });
            });
        }

        AgencyAutoCompleteSearch = (term: string) => this.SearchService.AutoComplete.Agencies(this.companyKey, { SearchTerm: term }, 10)

        DefaultCompanyChanged() {
            if (this.SelectedUser == null || this.SelectedUser.DefaultCompany == null) {
                this.ClientCoreSegments = [];
            } else {
                var selectedCompany = this.CompanyConfigurations.filter((c) => c.Summary.CompanyName == this.SelectedUser.DefaultCompany.Description);
                this.ClientCoreSegments = selectedCompany[0].Configuration.ClientCoreSegments;

                if (this.ClientCoreSegments.filter((s) => s.Code == this.SelectedUser.ClientCoreSegmentCode).length == 0) {
                    this.SelectedUser.ClientCoreSegmentCode = null;
                }
            }
        }

        ToggleRole(roleName: string) {
            var index = this.SelectedUser.Roles.indexOf(roleName);
            if (index > -1) {
                this.SelectedUser.Roles.splice(index, 1);
            } else {
                this.SelectedUser.Roles.push(roleName);
            }
        }
        CompanyChecked(company: CompanySummary) {
            return (this.SelectedUser != null && this.SelectedUser.Companies.filter((c) => c.Description == company.CompanyName).length > 0);
        }
        ToggleCompany(company: CompanySummary) {
            var index = -1;
            if (this.SelectedUser != null)
                for (var i = 0; i < this.SelectedUser.Companies.length; i++)
                    if (this.SelectedUser.Companies[i].Description == company.CompanyName) {
                        index = i;
                        break;
                    }

            if (index > -1) {
                this.SelectedUser.Companies.splice(index, 1);
                if (this.SelectedUser.DefaultCompany != null && this.SelectedUser.DefaultCompany.Description == company.CompanyName) {
                    this.SelectedUser.DefaultCompany = null;
                    this.DefaultCompanyChanged();
                }
            }
            else {
                this.SelectedUser.Companies.push({ Description: company.CompanyName, Code: company.Abbreviation });
            }
        }

        SaveUser() {
            this.AppState.Loading = true;
            this.UserService.Save(this.companyKey, this.SelectedUser.Id, this.SelectedUser).then((results) => {
                if (results.ValidationErrors) {
                    this.UtilService.SetValidationResults(this.UserForm, results.ValidationErrors);
                } else {
                    this.UtilService.SetValidationResults(this.UserForm);
                    this.SelectedUser = results.User;
                    this.AlertService.Notify(NotificationType.Success, `User "${results.User.UserName}" has been ${ this.IsNew ? 'added' : 'updated' }`);

                    if (this.IsNew) {
                        this.$state.go('cn.administration.users.user', { userId: results.User.Id }, { reload: true });
                    } else {
                        this.UserService.UserPromise.then((user) => {
                            if (user.UserName == results.User.UserName) {
                                this.UserService.RefreshCurrentUserPromise();
                            }
                        });
                    }
                }

                this.AppState.Loading = false;
            });
        }
    }
}