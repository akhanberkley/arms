﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('serverTests',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/server-tests.html',
            replace: true,
            controller: ServerTestsController,
            controllerAs: 'vm'
        }
    });

    class ServerTestsController extends BaseController {
        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $http: ng.IHttpService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Server Tests');
        }

        ServerException() {
            this.$http.get('/api/Tests/ServerExceptionWithArguments/1/test');
        }
        NotFound() {
            this.$http.get('/api/Tests/ResponseCode/404');
        }
        Unauthorized() {
            this.$http.get('/api/Tests/Unauthorized');
        }
        JavascriptError() {
            var x = <any>null;
            var y = x.nullreference;
        }
    }
}