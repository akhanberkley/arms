﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('companySettings',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/company-settings.html',
            replace: true,
            controller: CompanySettingsController,
            controllerAs: 'vm'
        }
    });

    class CompanySettingsController extends BaseController {
        companySettingsFields: string[];
        systemSettingsFields: string[];
        companies: CompanyConfiguration[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, $http: ng.IHttpService, DomainService: DomainService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Company Settings');
            this.AppState.PageGenerating = true;

            DomainService.CompanyConfigurations().then((configurations) => {
                this.companies = configurations;

                var settings = Object.keys(this.companies[0].Configuration);
                settings.splice(settings.indexOf('SubmissionTypes'), 1);
                settings.splice(settings.indexOf('ClientFields'), 1);
                this.companySettingsFields = settings;

                settings = Object.keys(this.companies[0].SystemSettings);
                this.systemSettingsFields = settings;

                this.AppState.PageGenerating = false;
            });
        }
    }
}