﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('policySymbolsAdministration',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/policy-symbols.html',
            replace: true,
            controller: PolicySymbolsAdministrationController,
            controllerAs: 'vm'
        }
    });

    class PolicySymbolsAdministrationController extends BaseController {
        PolicySymbols: EditablePolicySymbol[];
        NumberControls: NumberControl[];
        ValidationResults: ValidationResult[];

        ShowNumberControls: boolean;
        UsesPolicySymbol: boolean;
        UsesPackagePolicySymbol: boolean;
        ShowUsageOptions: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private DomainService: DomainService, private $http: ng.IHttpService, $q: ng.IQService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('Policy Symbols');
            this.AppState.PageGenerating = true;

            var domainData = <ng.IPromise<any>[]>[];
            domainData.push(DomainService.PolicySymbols(this.companyKey).then((policySymbols) => this.PolicySymbols = angular.copy(policySymbols)));
            domainData.push(DomainService.PolicySymbolNumberControls(this.companyKey).then((numberControls) => this.NumberControls = numberControls));
            domainData.push(DomainService.CompanyInformation(this.companyKey).then((config) => {
                this.UsesPolicySymbol = (config.SubmissionTypes.filter((st) => st.PolicySymbol.Enabled).length > 0);
                this.UsesPackagePolicySymbol = (config.SubmissionTypes.filter((st) => st.PackagePolicySymbols.Enabled).length > 0);
            }));

            $q.all(domainData).then(() => {
                this.ShowNumberControls = (this.NumberControls.length > 0);
                this.ShowUsageOptions = (this.UsesPolicySymbol && this.UsesPackagePolicySymbol);

                this.AppState.PageGenerating = false;
            });
        }

        Edit(ps: EditablePolicySymbol) {
            ps.Original = angular.copy(ps);
            ps.Editing = true;
            this.ValidationResults = [];
            this.UtilService.RaiseFocusTrigger('code-' + this.PolicySymbols.indexOf(ps));
        }
        Cancel(ps: EditablePolicySymbol) {
            var original = ps.Original;
            delete ps.Original;

            angular.copy(original, ps);
            ps.Editing = false;
            this.ValidationResults = [];
        }
        Add() {
            this.PolicySymbols.push({ Code: '', Description: '', IsPackageOption: this.UsesPackagePolicySymbol, IsPrimaryOption: this.UsesPolicySymbol, Editing: true, PolicyNumberPrefix: null });
            this.UtilService.RaiseFocusTrigger('code-' + (this.PolicySymbols.length - 1));
        }

        Delete(ps: EditablePolicySymbol) {
            if (ps.Code == null || ps.Code == '') {
                this.PolicySymbols.splice(this.PolicySymbols.indexOf(ps), 1);
            } else {
                if (confirm('Are you sure you want to delete this Policy Symbol?')) {
                    this.AppState.Loading = true;
                    this.$http.delete(`/api/${this.companyKey}/PolicySymbol/${ps.Code}`).success((client, status, headers, config) => {
                        this.PolicySymbols.splice(this.PolicySymbols.indexOf(ps), 1);
                        this.DomainService.ResetCache();
                        this.ValidationResults = [];
                    }).error((data, status, headers, config) => {
                        this.ValidationResults = data;
                    }).finally(() => {
                        this.AppState.Loading = false;
                    })
                }
            }
        }

        Save(ps: EditablePolicySymbol) {
            var originalCode = (ps.Original != null) ? ps.Original.Code : null;
            var original = ps.Original;
            delete ps.Original;

            this.AppState.Loading = true;
            this.UtilService.CreateSaveRequest(ps, `/api/${this.companyKey}/PolicySymbol`, originalCode).success((client, status, headers, config) => {
                ps.Editing = false;
                this.DomainService.ResetCache();
                this.ValidationResults = [];
            }).error((data, status, headers, config) => {
                this.ValidationResults = data;
                ps.Original = original;
            }).finally(() => {
                this.AppState.Loading = false;
            });
        }
    }

    interface EditablePolicySymbol extends PolicySymbol {
        Editing?: boolean;
        Original?: PolicySymbol
    }
}
