﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('packagePolicySymbolsRead', function (): ng.IDirective {
        return {
            restrict: 'E',
            templateUrl: '/app/components/submission/package-policy-symbols-read.html',
            replace: true,
            scope: {
                PolicySymbols: '=policySymbols'
            },
            bindToController: true,
            controller: function () { },
            controllerAs: 'vm'
        }
    });

    app.directive('packagePolicySymbolsWrite', function (): ng.IDirective {
        return {
            restrict: 'E',
            templateUrl: '/app/components/submission/package-policy-symbols-write.html',
            replace: true,
            scope: {
                PolicySymbols: '=policySymbols'
            },
            bindToController: true,
            controller: PackagePolicySymbolsController,
            controllerAs: 'vm'
        }
    });

    class PackagePolicySymbolsController extends BaseController {
        PolicySymbolOptionPromise: ng.IPromise<PolicySymbol[]>;
        PolicySymbols: PolicySymbol[];
        ShowInput: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, DomainService: DomainService, private $q: ng.IQService, private $filter: ng.IFilterService) {
            super($scope, $state, UtilService);

            this.PolicySymbolOptionPromise = DomainService.PackagePolicySymbols(this.companyKey);

            this.ShowInput = (this.PolicySymbols == null || this.PolicySymbols.length == 0);
        }

        PolicySymbolSearch = (term: string) => {
            return new this.$q((resolve, reject) => {
                this.PolicySymbolOptionPromise.then((policySymbols) => {
                    resolve(this.$filter('filter')(policySymbols.filter((ps) => this.PolicySymbols.filter((existing) => existing.Code == ps.Code).length == 0), term));
                });
            });
        };

        InputBlurred = (event: ng.IAngularEvent) => {
            this.ShowInput = false;
        }
        Add() {
            this.ShowInput = true;
            this.UtilService.RaiseFocusTrigger('PolicySymbolSearchInput');
        }
        AddPolicySymbol(policySymbol: PolicySymbol) {
            if (this.PolicySymbols.filter((ps) => ps.Code == policySymbol.Code).length == 0) {
                this.PolicySymbols.push(policySymbol);
                this.PolicySymbols.sort((a, b) => a.Code.localeCompare(b.Code));
            }
        }
        Delete(policySymbol: PolicySymbol) {
            this.PolicySymbols.splice(this.PolicySymbols.indexOf(policySymbol), 1);
        }
    }
}