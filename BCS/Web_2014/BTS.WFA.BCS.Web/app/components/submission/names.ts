﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionNamesRead', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                SubmissionNames: '=submissionNames'
            },
            templateUrl: '/app/components/submission/names-read.html',
            replace: true,
            bindToController: true,
            controller: SubmissionNamesController,
            controllerAs: 'vm'
        }
    });
    app.directive('submissionNamesWrite', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                ClientNames: '=clientNames',
                SubmissionNames: '=submissionNames',
                NameUpdatable: '=nameUpdatable'
            },
            templateUrl: '/app/components/submission/names-write.html',
            replace: true,
            bindToController: true,
            controller: SubmissionNamesController,
            controllerAs: 'vm'
        }
    });

    class NameGrouping {
        Submission: SubmissionName;
        Client: ClientName;
        IsUpToDate: boolean;
    }

    class SubmissionNamesController {
        NameGroupings: NameGrouping[];
        ClientNames: ClientName[];
        SubmissionNames: SubmissionName[];
        NameUpdatable: boolean;

        constructor($scope: ng.IScope, UtilService: UtilService) {
            this.NameGroupings = [];
            $scope.$watch(() => this.SubmissionNames,(submissionNames: SubmissionName[]) => {
                if (submissionNames && this.ClientNames) {
                    this.NameGroupings = [];
                    for (var i = 0; i < this.ClientNames.length; i++) {
                        var clientName = this.ClientNames[i];
                        var matchingSubmissionName = <SubmissionName>null;
                        for (var j = 0; j < submissionNames.length; j++) {
                            if (this.namesMatch(clientName, submissionNames[j])) {
                                matchingSubmissionName = submissionNames[j];
                                break;
                            }
                        }

                        var nameIsUpToDate = (matchingSubmissionName == null || clientName.BusinessName == matchingSubmissionName.BusinessName);
                        this.NameGroupings.push({
                            Client: clientName,
                            Submission: matchingSubmissionName,
                            Display: matchingSubmissionName || clientName,
                            IsUpToDate: nameIsUpToDate
                        });
                    }
                }
            });
        }

        RefreshName(grouping: NameGrouping) {
            for (var i = 0; i < this.SubmissionNames.length; i++) {
                if (this.SubmissionNames[i] == grouping.Submission) {
                    for (var prop in grouping.Client) {
                        grouping.Submission[prop] = grouping.Client[prop];
                    }
                    break;
                }
            }

            grouping.IsUpToDate = true;
        }

        private namesMatch(clientName: ClientName, submissionName: SubmissionName) {
            return (clientName == <any>submissionName
                || (submissionName.SequenceNumber != null && submissionName.SequenceNumber == clientName.SequenceNumber)
                || submissionName.BusinessName == clientName.BusinessName);
        }
        IsNameSelected(clientName: ClientName) {
            for (var i = 0; i < this.SubmissionNames.length; i++) {
                if (this.namesMatch(clientName, this.SubmissionNames[i])) {
                    return true;
                }
            }
            return false;
        }
        SelectName(clientName: ClientName) {
            var addrIndex = -1;
            for (var i = 0; i < this.SubmissionNames.length; i++) {
                if (this.namesMatch(clientName, this.SubmissionNames[i])) {
                    addrIndex = i;
                    break;
                }
            }

            if (addrIndex < 0) {
                this.SubmissionNames.push(clientName);
            } else {
                this.SubmissionNames.splice(addrIndex, 1);
            }
        }

        AllNamesSelected () {
            return (this.SubmissionNames.length == this.NameGroupings.length);
        }
        ToggleAllNames() {
            var allSelected = this.AllNamesSelected();
            this.SubmissionNames = [];
            if (!allSelected) {
                for (var i = 0; i < this.NameGroupings.length; i++) {
                    this.SubmissionNames.push(this.NameGroupings[i].Client);
                }
            }
        }
    }
} 