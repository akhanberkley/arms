﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionLinearForm',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/submission/submission-linear-form.html',
            replace: true,
            scope: {
                SubmissionWorkflow: '=submissionWorkflow',
                Submission: '=submission',
                VisibilityKey: '@visibilityKey'
            },
            bindToController: true,
            controller: SubmissionLinearFormController,
            controllerAs: 'vm'
        };
    });

    class SubmissionLinearFormController extends BaseController {
        VisibilityKey: string;
        SubmissionWorkflow: SubmissionWorkflow;
        Submission: Submission;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);
        }
    }
}