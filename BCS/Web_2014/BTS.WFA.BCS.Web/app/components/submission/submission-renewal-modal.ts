﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionRenewalModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/submission/submission-renewal-modal.html',
            replace: true,
            scope: {
                OriginalSubmission: '=submission',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: SubmissionRenewalModalController,
            controllerAs: 'vm'
        };
    });

    class SubmissionRenewalModalController extends BaseController {
        OriginalSubmission: Submission;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        SubmissionWorkflow: SubmissionWorkflow;
        Submission: Submission;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, SubmissionWorkflowService: SubmissionWorkflowService) {
            super($scope, $state, UtilService);

            SubmissionWorkflowService.GetWorkflow(this.OriginalSubmission.Company.Abbreviation, SubmissionWorkflowType.NewApplication, this.OriginalSubmission).then((workflow) => {
                this.SubmissionWorkflow = workflow;
                this.Submission = workflow.Submissions[0];
            });
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}