﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionStatusDetailsModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/submission/status-details-modal.html',
            replace: true,
            scope: {
                Submission: '=submission',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: StatusDetailsModalController,
            controllerAs: 'vm'
        };
    });

    class StatusDetailsModalController extends BaseController {
        Submission: Submission;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $http: ng.IHttpService) {
            super($scope, $state, UtilService);
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}