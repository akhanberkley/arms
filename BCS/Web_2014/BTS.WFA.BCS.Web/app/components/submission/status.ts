﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionStatus',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: (elem: ng.IAugmentedJQuery, attrs: ng.IAttributes) => {
                return `/app/components/submission/status-${attrs['view']}.html`;
            },
            replace: true,
            scope: {
                Item: '=item',
                Revert: '=revert',
                SubmissionType: '=submissionType'
            },
            bindToController: true,
            controller: SubmissionStatusController,
            controllerAs: 'vm'
        }
    });

    class SubmissionStatusController extends BaseController {
        Item: SubmissionStatusUpdate;
        Revert: boolean;
        SubmissionType: SubmissionType;
        StatusOptions: SubmissionStatus[];
        HistoryOptions: SubmissionStatusHistory[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, DomainService: DomainService) {
            super($scope, $state, UtilService);

            DomainService.SubmissionStatuses(this.companyKey).then((items) => {
                this.StatusOptions = items;

                if (this.Revert) {
                    this.HistoryOptions = this.Item.StatusHistory.filter((h) => h.IsActive);
                    if (this.HistoryOptions.length > 0) {
                        this.HistoryOptions.splice(0, 1);
                        if (this.HistoryOptions.length > 0) {
                            this.Item.RevertStatusTo = this.HistoryOptions[0];
                            this.RevertStatusChanged();
                        }
                    }
                } else if (this.Item != null && this.Item.StatusAsObject == null) {
                    this.Item.StatusAsObject = this.getStatusAsObject(this.Item.Status);
                }
            });
        }

        private getStatusAsObject(status: string) {
            var selectedStatus = this.StatusOptions.filter((s) => { return (s.Code == status); });
            if (selectedStatus.length > 0) {
                return selectedStatus[0];
            }

            return null;
        }

        RevertStatusChanged() {
            this.Item.StatusAsObject = this.getStatusAsObject(this.Item.RevertStatusTo.Status);
            this.Item.StatusReason = null;
            this.Item.StatusDate = this.Item.RevertStatusTo.Date;
        }
        StatusChanged() {
            this.Item.StatusReason = null;
            this.StatusElementChanged();
        }
        StatusElementChanged() {
            if (!this.Revert) {
                this.Item.StatusDate = this.UtilService.GetCurrentDate();
            }
        }
    }
}