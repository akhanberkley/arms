﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionAddressesRead', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                SubmissionAddresses: '=submissionAddresses'
            },
            templateUrl: '/app/components/submission/addresses-read.html',
            replace: true,
            bindToController: true,
            controller: SubmissionAddressesController,
            controllerAs: 'vm'
        }
    });
    app.directive('submissionAddressesWrite', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                ClientAddresses: '=clientAddresses',
                SubmissionAddresses: '=submissionAddresses',
                onSelectedAddressChange: '=onSelectedAddressChange',
                AddressUpdatable: '=addressUpdatable'
            },
            templateUrl: '/app/components/submission/addresses-write.html',
            replace: true,
            bindToController: true,
            controller: SubmissionAddressesController,
            controllerAs: 'vm'
        }
    });

    class AddressGrouping {
        Submission: SubmissionAddress;
        Client: ClientAddress;
        AddressIsUpToDate: boolean;
    }

    class SubmissionAddressesController {
        AddressGroupings: AddressGrouping[];
        ClientAddresses: ClientAddress[];
        SubmissionAddresses: SubmissionAddress[];
        AddressUpdatable: boolean;
        onSelectedAddressChange: () => void;

        constructor($scope: ng.IScope, UtilService: UtilService) {
            this.AddressGroupings = [];
            $scope.$watch(() => this.SubmissionAddresses,(submissionAddresses: SubmissionAddress[]) => {
                if (submissionAddresses && this.ClientAddresses) {
                    this.AddressGroupings = [];
                    for (var i = 0; i < this.ClientAddresses.length; i++) {
                        var clientAddress = this.ClientAddresses[i];
                        var matchingSubmissionAddr = <SubmissionAddress>null;
                        for (var j = 0; j < submissionAddresses.length; j++) {
                            if (this.addressesMatch(clientAddress, submissionAddresses[j])) {
                                matchingSubmissionAddr = submissionAddresses[j];
                                break;
                            }
                        }

                        var addressIsUpToDate = (matchingSubmissionAddr == null || UtilService.AddressesAreEqual(clientAddress, matchingSubmissionAddr));

                        this.AddressGroupings.push({
                            Client: clientAddress,
                            Submission: matchingSubmissionAddr,
                            Display: matchingSubmissionAddr || clientAddress,
                            AddressIsUpToDate: addressIsUpToDate
                        });
                    }
                }
            });
        }

        RefreshAddress(grouping: AddressGrouping) {
            for (var i = 0; i < this.SubmissionAddresses.length; i++) {
                if (this.SubmissionAddresses[i] == grouping.Submission) {
                    for (var prop in grouping.Client) {
                        grouping.Submission[prop] = grouping.Client[prop];
                    }
                    break;
                }
            }

            grouping.AddressIsUpToDate = true;
        }

        private addressesMatch(clientAddress: ClientAddress, submissionAddress: SubmissionAddress) {
            //New client addresses won't have Ids, but the object should be the same
            return (submissionAddress == <SubmissionAddress>clientAddress || (clientAddress.ClientCoreAddressId != null && submissionAddress.ClientCoreAddressId == clientAddress.ClientCoreAddressId));
        }
        IsAddressSelected(clientAddress: ClientAddress) {
            for (var i = 0; i < this.SubmissionAddresses.length; i++) {
                if (this.addressesMatch(clientAddress, this.SubmissionAddresses[i])) {
                    return true;
                }
            }
            return false;
        }
        SelectAddress(clientAddress: ClientAddress) {
            var addrIndex = -1;
            for (var i = 0; i < this.SubmissionAddresses.length; i++) {
                if (this.addressesMatch(clientAddress, this.SubmissionAddresses[i])) {
                    addrIndex = i;
                    break;
                }
            }

            if (addrIndex < 0) {
                this.SubmissionAddresses.push(clientAddress);
            } else {
                this.SubmissionAddresses.splice(addrIndex, 1);
            }

            if (this.onSelectedAddressChange) {
                this.onSelectedAddressChange();
            }
        }

        AllAddressesSelected () {
            return (this.SubmissionAddresses.length == this.AddressGroupings.length);
        }
        ToggleAllAddresses() {
            var allSelected = this.AllAddressesSelected();
            this.SubmissionAddresses = [];
            if (!allSelected) {
                for (var i = 0; i < this.AddressGroupings.length; i++) {
                    this.SubmissionAddresses.push(this.AddressGroupings[i].Client);
                }
            }
        }
    }
} 