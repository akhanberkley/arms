﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionNotesRead', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                notes: '=',
                isCopyable: '='
            },
            templateUrl: '/app/components/submission/notes-read.html',
            replace: true,
            controller: function ($scope: ng.IScope) {
                if ($scope['isCopyable'] == null) {
                    $scope['isCopyable'] = false;
                }
            }
        }
    });
    app.directive('submissionNotesWrite', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                notes: '='
            },
            templateUrl: '/app/components/submission/notes-write.html',
            replace: true,
            controller: function ($scope: any, UserService: UserService, UtilService: UtilService) {
                $scope.AppState = UtilService.AppState;
                $scope.AllowDelete = false;
                var userName = 'You';

                UserService.UserPromise.then(function (User) {
                    $scope.AllowDelete = User.IsSystemAdministrator;
                    userName = User.UserName;
                });

                $scope.AddNote = function () {
                    $scope.notes.push({ IsNew: true, Author: userName, Date: new Date() });
                    UtilService.RaiseFocusTrigger('Note[' + ($scope.notes.length - 1) + ']-Text');
                };
                $scope.DeleteNote = function (note: SubmissionNote) {
                    $scope.notes.splice($scope.notes.indexOf(note), 1);
                };
            }
        }
    });
} 