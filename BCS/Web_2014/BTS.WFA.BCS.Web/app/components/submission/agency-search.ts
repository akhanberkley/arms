﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionAgencySearch', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                SubmissionWorkflow: '=submissionWorkflow'
            },
            templateUrl: '/app/components/submission/agency-search.html',
            replace: true,
            bindToController: true,
            controller: AgencySearchController,
            controllerAs: 'vm'
        }
    });

    class AgencySearchController extends BaseController {
        SubmissionWorkflow: SubmissionWorkflow;
        AgencySelectOptions: ItemSelectOptions;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, UserService: UserService, SearchService: SearchService) {
            super($scope, $state, UtilService);
            this.AgencySelectOptions = {
                source: null,
                display: 'i.Code + \' - \' + i.Name',
                tooltip: 'i | agencyTooltip',
                matchTemplate: 'item-select-agency'
            };

            $scope.$watch(() => this.SubmissionWorkflow,(submissionWorkflow: SubmissionWorkflow) => {
                if (submissionWorkflow) {
                    this.AgencySelectOptions.source = submissionWorkflow.AgencyAutoCompleteSearch;
                    this.AgencySelectOptions.expand = submissionWorkflow.OpenAgencyViewModal;
                }
            });
        }
    }
} 