﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submissionModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/submission/submission-modal.html',
            replace: true,
            scope: {
                CompanyKey: '=companyKey',
                SubmissionId: '=submissionId',
                AllowModalEdit: '=allowModalEdit',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: AgencyModalController,
            controllerAs: 'vm'
        };
    });

    class AgencyModalController extends BaseController {
        CompanyKey: string;
        SubmissionId: number;
        AllowModalEdit: boolean;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}