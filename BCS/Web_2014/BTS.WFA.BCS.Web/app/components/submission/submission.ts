﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('submission',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: (elem: ng.IAugmentedJQuery, attrs: ng.IAttributes) => {
                var view = attrs['view'];
                view = (view) ? view + '-' : '';

                return `/app/components/submission/${view}submission.html`;
            },
            replace: true,
            scope: {
                submissionCompanyKey: '=companyKey',
                submissionId: '=submissionId',
                IsModal: '=isModal',
                SearchWorkflow: '=searchWorkflow',
                AllowModalEdit: '=allowModalEdit'
            },
            bindToController: true,
            controller: SubmissionController,
            controllerAs: 'vm'
        }
    });

    class SubmissionController extends BaseController {
        submissionCompanyKey: string;
        submissionId: string;
        SubmissionWorkflow: SubmissionWorkflow;
        Submission: Submission;
        ApplicationSubmissions: Submission[];
        RelatedDuplicates: Submission[];
        RelatedSubmissionFields: SubmissionDisplayField[];
        RelatedSubmissionCount: number;

        User: User;
        SubmissionForm: ValidatedForm;
        Editing: boolean;
        ShowActions: boolean;
        CanEditSubmissionNumber: boolean;
        CanDeleteSubmission: boolean;
        CanSwitchClient: boolean;
        ClientDetails: { Addresses: ClientAddress[]; AdditionalNames: ClientName[]; DoingBusinessAsNames: string[]; NewInsuredName: string; };
        cleanWorkflowViewModel: SubmissionWorkflow;

        IsModal: boolean;
        AllowModalEdit: boolean;
        CurrentTab: number;
        CurrentTabTemplateUrl: string;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, $modalStack: ng.ui.bootstrap.IModalStackService, private $window: ng.IWindowService, private $modal: ng.ui.bootstrap.IModalService, private SearchService: SearchService, $q: ng.IQService, private ClientService: ClientService,
            private $http: ng.IHttpService, private SubmissionService: SubmissionService, private SubmissionWorkflowService: SubmissionWorkflowService, private DomainService: DomainService, UserService: UserService, private AlertService: AlertService) {
            super($scope, $state, UtilService);

            if (this.IsModal) {
                this.SetTab(0);
            }

            DomainService.SubmissionSummaryFields(this.submissionCompanyKey, 'related-submissions').then((items) => { this.RelatedSubmissionFields = items; });
            UserService.UserPromise.then((User) => {
                this.User = User;
                this.ShowActions = (!User.IsReadOnly);
                this.CanEditSubmissionNumber = User.IsInRole('Action - Renumber Submission');
                this.CanDeleteSubmission = User.IsInRole('Action - Delete Submission');
                this.CanSwitchClient = User.IsInRole('Action - Switch Client');
            });

            this.AppState.PageGenerating = true;
            SubmissionService.GetApplicationSubmissions(this.submissionCompanyKey, this.submissionId).then((submissions) => {
                if (submissions == null || submissions.length == 0) {
                    AlertService.OpenStandardModal({ title: 'Submission ' + this.submissionId, message: 'Submission could not be found.' });
                } else {
                    var submission = submissions.filter((s) => s.Id == this.submissionId)[0];
                    this.ApplicationSubmissions = submissions.filter((s) => s != submission);

                    SubmissionWorkflowService.GetWorkflow(submission.Company.Abbreviation, SubmissionWorkflowType.ViewingSubmission, submission).then((workflow) => {
                        this.bindWorkflow(workflow, true);
                        this.setupRelatedSubmissions();

                        UtilService.SetPageTitle('Submission #' + submission.Id);

                        if ($state.is('cn.search.submission.edit')) {
                            $state.go('^.policyinformation', null, { location: 'replace' });
                            this.Edit();
                        } else if ($state.is('bpm.submission')) {
                            this.Edit();
                        }

                        this.AppState.PageGenerating = false;
                    });
                }
            });
        }

        private bindWorkflow(workflow: SubmissionWorkflow, updateCleanViewModel: boolean) {
            this.SubmissionWorkflow = workflow;
            this.Submission = workflow.Submissions[0];

            if (updateCleanViewModel) {
                this.cleanWorkflowViewModel = workflow;
            }
        }
        private setupRelatedSubmissions() {
            this.RelatedSubmissionCount = null;
            this.RelatedDuplicates = null;

            if (this.Submission.DuplicateOfSubmission) {//submissions that are duplicates of DuplicateOfSubmission
                var filter = "DuplicateOfSubmission/SubmissionNumber eq " + this.Submission.DuplicateOfSubmission.SubmissionNumber +
                    " and SubmissionNumber ne " + this.Submission.SubmissionNumber;
            } else {//submissions that are duplicates of this submission
                var filter = "DuplicateOfSubmission/SubmissionNumber eq " + this.Submission.SubmissionNumber;
            }
            var url = '/api/' + this.Submission.Company.Abbreviation + '/Submission?crossCompany=true&$filter=' + filter + '&$orderby=EffectiveDate desc, PolicyNumber';
            this.$http.get(url).then((response: ng.IHttpPromiseCallbackArg<Submission[]>) => {
                this.RelatedDuplicates = response.data;
                this.RelatedSubmissionCount = this.ApplicationSubmissions.length + this.RelatedDuplicates.length;
            });
        }

        SetTab(index: number) {
            this.CurrentTab = index;
            switch (index) {
                case 0: this.CurrentTabTemplateUrl = '/app/components/submission/tabs/policy-information.html'; break;
                case 1: this.CurrentTabTemplateUrl = '/app/components/submission/tabs/client.html'; break;
                case 2: this.CurrentTabTemplateUrl = '/app/components/submission/tabs/notes.html'; break;
                case 3: this.CurrentTabTemplateUrl = '/app/components/submission/tabs/related-submissions.html'; break;
            }
        }

        OpenSubmissionModal = (submission: Submission) => {
            this.SubmissionService.OpenSubmissionModal(submission, false);
        }

        PolicyInformationFieldCount() {
            var count = 0;
            if (this.SubmissionWorkflow != null) {
                var st = this.SubmissionWorkflow.SubmissionType;
                count += ((st.PolicySymbol.Enabled) ? 1 : 0);
                count += ((st.UnderwritingUnit.Enabled) ? 1 : 0);
                count += ((st.Underwriter.Enabled) ? 1 : 0);
                count += ((st.UnderwritingAnalyst.Enabled) ? 1 : 0);
                count += ((st.UnderwritingTechnician.Enabled) ? 1 : 0);
                count += ((st.PolicyPremium.Enabled) ? 1 : 0);
                count += ((st.PriorCarrier.Enabled) ? 1 : 0);
                count += ((st.PriorCarrierPremium.Enabled) ? 1 : 0);
                count += ((st.AcquiringCarrier.Enabled) ? 1 : 0);
                count += ((st.AcquiringCarrierPremium.Enabled) ? 1 : 0);

                count += this.Submission.CompanyCustomizationBindings.length;
            }

            return count;
        }

        Edit() {
            this.AppState.Loading = true;
            var copiedSubmission = angular.copy(this.cleanWorkflowViewModel.Submissions[0]);
            this.SubmissionWorkflowService.GetWorkflow(copiedSubmission.Company.Abbreviation, SubmissionWorkflowType.ViewingSubmission, copiedSubmission).then((workflow) => {
                this.bindWorkflow(workflow, false);

                workflow.SelectAgencyFromExistingSubmission(copiedSubmission);
                this.ClientService.Get(copiedSubmission.Company.Abbreviation, copiedSubmission.ClientCoreId).then((result) => {
                    if (result.Client != null) {
                        var client = result.Client;

                        this.ClientDetails = {
                            NewInsuredName: null,
                            Addresses: client.Addresses,
                            AdditionalNames: client.AdditionalNames.filter((n) => n.NameType != 'DBA'),
                            DoingBusinessAsNames: client.AdditionalNames.filter((n) => n.NameType == 'DBA').map((n) => n.BusinessName)
                        };

                        if (copiedSubmission.InsuredDBA && this.ClientDetails.DoingBusinessAsNames.indexOf(copiedSubmission.InsuredDBA) < 0) {
                            this.ClientDetails.DoingBusinessAsNames.push(copiedSubmission.InsuredDBA);
                        }

                        if (client.PrimaryName.BusinessName != copiedSubmission.InsuredName) {
                            this.ClientDetails.NewInsuredName = client.PrimaryName.BusinessName;
                        }
                    }

                    //Shift state if we're on an invisible tab
                    if (this.$state.is('cn.search.submission.relatedsubmissions')) {
                        this.$state.go('cn.search.submission.policyinformation');
                    }

                    this.Editing = true;
                    this.AppState.Loading = false;
                });
            });
        }
        RedirectToEdit() {
            this.$state.go('cn.search.submission.edit', { submissionId: this.Submission.Id });
        }

        Cancel() {
            this.bindWorkflow(this.cleanWorkflowViewModel, false);
            this.UtilService.SetValidationResults(this.SubmissionForm);
            this.Editing = false;
        }

        OpenStatusDetails() {
            var modal = this.$modal.open({
                template: '<submission-status-details-modal submission="submission" modal="modal"></submission-status-details-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['submission'] = this.Submission;
                    $scope['modal'] = modal;
                }
            });
        }

        RefreshInsuredName() {
            this.SubmissionWorkflow.InsuredName = this.ClientDetails.NewInsuredName;
            this.ClientDetails.NewInsuredName = null;
        }

        Copy() {
            this.$state.go('cn.clearance', { clientCoreId: this.Submission.ClientCoreId, copyOf: this.Submission.Id });
        }

        Renew() {
            var modal = this.$modal.open({
                template: '<submission-renewal-modal submission="submission" modal="modal"></submission-status-details-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['submission'] = angular.copy(this.Submission);
                    $scope['modal'] = modal;
                }
            });
        }

        Save() {
            var originalDuplicateOfId = (this.cleanWorkflowViewModel.Submissions[0].DuplicateOfSubmission) ? this.cleanWorkflowViewModel.Submissions[0].DuplicateOfSubmission.Id : null;
            this.AppState.Loading = true;
            this.$window.scrollTo(0, 0);

            this.SubmissionService.Save(this.Submission.Company.Abbreviation, this.Submission.Id, this.Submission, this.SubmissionWorkflow).then((submissionResults) => {
                if (submissionResults.ValidationErrors) {
                    this.UtilService.SetValidationResults(this.SubmissionForm, submissionResults.ValidationErrors);
                    this.AppState.Loading = false;
                } else {
                    if (this.$state.is('bpm.submission')) {
                        this.$window.location.reload();//BPM knows the save occured when this event happens
                    } else {
                        this.UtilService.SetValidationResults(this.SubmissionForm);
                        this.AppState.Loading = false;
                        this.Editing = false;

                        if (submissionResults.Submission.Id != this.Submission.Id) {
                            this.$state.go('.', { submissionId: submissionResults.Submission.Id });
                        } else {
                            this.SubmissionWorkflowService.GetWorkflow(this.Submission.Company.Abbreviation, SubmissionWorkflowType.ViewingSubmission, submissionResults.Submission).then((workflow) => {
                                this.bindWorkflow(workflow, true);

                                //Refresh duplicate table if the duplicate info changed
                                var newDuplicateOfId = (submissionResults.Submission.DuplicateOfSubmission) ? submissionResults.Submission.DuplicateOfSubmission.Id : null;
                                if (originalDuplicateOfId != newDuplicateOfId) {
                                    this.setupRelatedSubmissions();
                                }
                            });
                        }

                        this.AlertService.Notify(NotificationType.Success, `Submission #${this.Submission.SubmissionNumber} updated`);
                    }
                }
            });
        }

        UpdateStatus(revert: boolean) {
            var modal = this.$modal.open({
                template: '<update-status-modal item="submission" modal="modal" revert="revert" update-application="false" submission-type="submissionType"></update-status-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['submission'] = angular.copy(this.Submission);
                    $scope['revert'] = revert;
                    $scope['submissionType'] = this.SubmissionWorkflow.SubmissionType;
                    $scope['modal'] = modal;
                }
            });

            modal.result.then((workflow) => {
                this.bindWorkflow(workflow, true);
            });
        }

        Delete() {
            if (confirm("Click OK to confirm the deletion of this submission")) {
                this.SubmissionService.Delete(this.Submission.Company.Abbreviation, this.Submission.Id);
                this.Submission.IsDeleted = true;
                this.Submission.DeletedBy = this.User.UserName;
                this.Submission.DeletedDate = new Date();
            }
        }

        SwitchClient() {
            //we could potentially use the data we've already received, but since the workflow alters how objects look on the submission, it's easier to just handle it this way
            var submissionsPromise = this.SubmissionService.GetApplicationSubmissions(this.Submission.Company.Abbreviation, this.Submission.Id);

            var modal = this.$modal.open({
                size: 'lg',
                template: '<switch-client-modal client-id="clientId" client-name="clientName" submissions-promise="submissionsPromise" modal="modal"></switch-client-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['clientId'] = this.Submission.ClientCoreId;
                    $scope['clientName'] = this.Submission.InsuredName;
                    $scope['submissionsPromise'] = submissionsPromise;
                    $scope['modal'] = modal;
                }
            });

            modal.result.then((completed) => {
                if (completed) {
                    this.$state.transitionTo(this.$state.current.name, this.$state.params, { reload: true, inherit: false, notify: true });
                }
            });
        }
    }
}