﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('addressView', function () : ng.IDirective {
        return {
            restrict: 'E',
            replace: true,
            template: function (elem: ng.IAugmentedJQuery, attrs: ng.IAttributes) {
                var addrLine = '{{a.Address1}}{{(a.Address2) ? " " + a.Address2 : ""}}';
                var detailLine = '{{(a.City) ? a.City + "," : ""}} {{a.State}} {{a.PostalCode | postalCode}}';
                var divider = (attrs['singleLine'] != undefined) ? ', ' : '<br />';

                return '<div>' + addrLine + divider + detailLine + '</div>';
            },
            scope: {
                a: '=address'
            }
        };
    });
} 