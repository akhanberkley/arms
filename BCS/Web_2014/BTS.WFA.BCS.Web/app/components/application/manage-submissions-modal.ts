﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('manageSubmissionsModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/application/manage-submissions-modal.html',
            replace: true,
            scope: {
                Submissions: '=submissions',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: ManageSubmissionsController,
            controllerAs: 'vm'
        };
    });

    class ManageSubmissionsController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;
        Submissions: Submission[];
        SubmissionToAdd: Submission;
        Loading: boolean;
        ShowInput: boolean;
        AllClientSubmissions: ng.IPromise<Submission[]>;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $q: ng.IQService,
            private SubmissionService: SubmissionService, private ClientService: ClientService) {
            super($scope, $state, UtilService);

            this.ShowInput = false;
        }

        HideAddInput = () => {
            this.ShowInput = false;
        }
        ShowAddInput() {
            this.ShowInput = true;
            this.UtilService.RaiseFocusTrigger('SubmissionSearchInput');
        }

        SubmissionSearch = (term: string) => {
            if (this.AllClientSubmissions == null) {
                this.AllClientSubmissions = new this.$q((resolve, reject) => {
                    this.ClientService.GetSubmissions(this.companyKey, this.Submissions[0].ClientCoreId, false).then((submissions) => {
                        resolve(submissions);
                    });
                });
            }

            return new this.$q((resolve, reject) => {
                this.AllClientSubmissions.then((submissions) => {
                    var existingSubmissionIds = this.Submissions.map((s: Submission) => s.Id);
                    resolve(submissions.filter((s) => {
                        return existingSubmissionIds.indexOf(s.Id) < 0
                            && ((s.SubmissionNumber || '').toString().indexOf(term) >= 0
                                || (s.PolicyNumber || '').toString().indexOf(term) >= 0);
                    }));
                });
            });
        }

        Selected() {
            if (this.Submissions.filter((s: Submission) => s.Id == this.SubmissionToAdd.Id).length == 0) {
                this.Submissions.push(this.SubmissionToAdd);
            }

            this.SubmissionToAdd = null;
            this.UtilService.RaiseFocusTrigger('SubmissionSearchInput');
        }

        RemoveSubmission(s: Submission) {
            this.Submissions.splice(this.Submissions.indexOf(s), 1);
        }

        Update() {
            this.Loading = true;
            this.SubmissionService.UpdateAssociatedSubmissions(this.companyKey, this.Submissions).then(() => {
                this.ModalInstance.close();
            });
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}