﻿/// <reference path="../../app.ts" />
module bcs {
    'use strict';
    app.directive('application',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/application/application.html',
            replace: true,
            scope: {
                SubmissionId: '@submissionId'
            },
            bindToController: true,
            controller: ApplicationController,
            controllerAs: 'vm'
        }
    });

    class ApplicationController extends BaseController {
        SubmissionId: string;
        Submissions: Submission[];
        ApplicationSubmissionFields: SubmissionDisplayField[];
        ClientSubmissionFields: SubmissionDisplayField[];
        PrimarySubmissionType: SubmissionType;

        HasOFACHit: boolean;
        ShowAgency: boolean;
        ShowAgent: boolean;
        AllowsMultipleSubmissions: boolean;

        Clients: {
            ClientCoreId: number;
            Name: string;
            Url: string;
            Submissions?: PagedDataSource;
        }[];
        AdditionalNames: string[];
        Addresses: string[];
        SubmissionTypes: string[];
        EffectivePeriods: string[];
        Agencies: AgencySummary[];
        Agents: string[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $modal: ng.ui.bootstrap.IModalService, private SubmissionService: SubmissionService,
            private $q: ng.IQService, private ClientService: ClientService, $filter: ng.IFilterService, DomainService: DomainService, private AlertService: AlertService) {
            super($scope, $state, UtilService);

            UtilService.SetPageTitle('View Application');
            this.AppState.PageGenerating = true;

            var clientSubmissions = <Submission[]>[];
            var dependencies = <ng.IPromise<any>[]>[];
            var companySettings = <CompanyConfiguration>null;
            dependencies.push(DomainService.CompanyInformation(this.companyKey).then((item) => { companySettings = item; }));
            dependencies.push(DomainService.SubmissionSummaryFields(this.companyKey, 'application-view').then((items) => { this.ApplicationSubmissionFields = items; }));
            dependencies.push(DomainService.SubmissionSummaryFields(this.companyKey, 'client-detail').then((items) => { this.ClientSubmissionFields = items; }));
            dependencies.push(SubmissionService.GetApplicationSubmissions(this.companyKey, this.SubmissionId).then((submissions) => {
                if (submissions == null || submissions.length == 0) {
                    AlertService.OpenStandardModal({ title: 'Application', message: 'Unable to retreive submission ' + this.SubmissionId + ' or its application' });
                } else {
                    this.HasOFACHit = (submissions.filter((s) => s.CreatedWithOFACHit).length > 0);

                    this.Clients = [];
                    this.AdditionalNames = [];
                    this.Addresses = [];
                    this.SubmissionTypes = [];
                    this.EffectivePeriods = [];
                    this.Agencies = [];
                    this.Agents = [];
                    angular.forEach(submissions,(s) => {
                        if (this.Clients.filter((i) => i.ClientCoreId == s.ClientCoreId).length == 0) {
                            this.Clients.push({ ClientCoreId: s.ClientCoreId, Name: (s.InsuredName + (((s.InsuredDBA || '') == '') ? '' : ' dba ' + s.InsuredDBA)), Url: UtilService.CreateClientLink(s.ClientCoreId) });
                        }
                        angular.forEach(s.AdditionalNames.map((n) => n.BusinessName),(n) => {
                            if (this.AdditionalNames.indexOf(n) < 0) {
                                this.AdditionalNames.push(n);
                            }
                        });
                        angular.forEach(s.Addresses.map((a) => $filter('address')(a)),(a) => {
                            if (this.Addresses.indexOf(a) < 0) {
                                this.Addresses.push(a);
                            }
                        });
                        if (this.SubmissionTypes.indexOf(s.SubmissionType) < 0) {
                            this.SubmissionTypes.push(s.SubmissionType);
                        }
                        if (this.EffectivePeriods.indexOf(s.EffectivePeriod) < 0) {
                            this.EffectivePeriods.push(s.EffectivePeriod);
                        }
                        if (s.Agency != null && this.Agencies.filter((i) => i.Code == s.Agency.Code).length == 0) {
                            s.Agency.AgencyUrl = UtilService.CreateAgencyLink(s.Agency.Code, s.Company.Abbreviation);
                            this.Agencies.push(s.Agency);
                        }
                        var agent = (s.Agent != null) ? s.Agent.FullName : (s.AgentUnspecifiedReason) ? '(' + s.AgentUnspecifiedReason + ')' : null;
                        if (agent != null && this.Agents.indexOf(agent) < 0) {
                            this.Agents.push(agent);
                        }
                    });

                    this.Submissions = submissions;
                }
            }));

            var clientsOtherSubmissions = <PagedDataSource>null;
            dependencies.push(new $q((resolve, reject) => {
                clientsOtherSubmissions = SubmissionService.GetClientsNonApplicationSubmissions(this.companyKey, this.SubmissionId,() => { resolve(); });
            }));

            $q.all(dependencies).then(() => {
                if (this.Submissions) {
                    var submissionTypeSettings = companySettings.SubmissionTypes.filter((t) => (this.SubmissionTypes.indexOf(t.TypeName) != -1));
                    this.PrimarySubmissionType = submissionTypeSettings[0];
                    angular.forEach(submissionTypeSettings,(t) => {
                        if (t.Agency.Enabled) {
                            this.ShowAgency = true;
                        }
                        if (t.Agent.Enabled) {
                            this.ShowAgent = true;
                        }
                        if (t.AllowMultipleSubmissions.Enabled) {
                            this.AllowsMultipleSubmissions = true;
                        }
                    });

                    angular.forEach(this.Clients,(c) => {
                        c.Submissions = clientsOtherSubmissions;
                    });
                }

                this.AppState.PageGenerating = false;
            });
        }

        AddNewSubmission() {
            this.$state.go('cn.clearance', { clientCoreId: this.Submissions[0].ClientCoreId, applicationId: this.SubmissionId });
        }

        UpdateStatus() {
            var modal = this.$modal.open({
                template: '<update-status-modal item="submission" modal="modal" update-application="true" submission-type="submissionType"></update-status-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['submission'] = angular.copy(this.Submissions[0]);
                    $scope['submissionType'] = this.PrimarySubmissionType;
                    $scope['modal'] = modal;
                }
            });

            modal.result.then(() => {
                this.AlertService.Notify(NotificationType.Success, "Application Updated");
                this.$state.go('.', { submissionId: this.Submissions[0].Id }, { reload: true });
            });
        }

        ManageSubmissions() {
            var modal = this.$modal.open({
                template: '<manage-submissions-modal submissions="submissions" modal="modal"></manage-submissions-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['submissions'] = angular.copy(this.Submissions);
                    $scope['modal'] = modal;
                }
            });

            modal.result.then(() => {
                this.AlertService.Notify(NotificationType.Success, "Application Updated");
                this.$state.go('.', { submissionId: this.Submissions[0].Id }, { reload: true });
            });
        }
    }
}