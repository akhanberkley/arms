﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('address', function (postalCodeFilter: any) {
        return function (a: Address) {
            if (!a) {
                return '';
            } else if (a.Address1 || a.State || a.City || a.PostalCode) {
                return a.Address1 + " "
                    + ((a.Address2) ? a.Address2 + " " : "")
                    + ((a.City) ? a.City + ", " : "")
                    + a.State + " " + postalCodeFilter(a.PostalCode);
            } else {
                return '';
            }
        };
    });
}