﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('codeItemEdit', function () {
        return function (c: CodeItem) {
            if (!c)
                return '';
            else {
                if ((c.Description || '') == '' || c.Description == c.Code) {
                    return c.Code;
                } else {
                    return c.Code + ' - ' + c.Description;
                }
            }
        };
    });
} 