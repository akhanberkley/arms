﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('postalCode', function () {
        return function (value: string) {
            return (value != null && value.length == 9) ? value.substr(0, 5) + '-' + value.substr(5, 4) : value;
        };
    });
} 