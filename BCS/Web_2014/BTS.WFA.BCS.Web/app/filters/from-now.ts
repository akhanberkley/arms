﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('fromNow', function () {
        return function (date: any) {
            return moment(date).fromNow();
        };
    });
} 