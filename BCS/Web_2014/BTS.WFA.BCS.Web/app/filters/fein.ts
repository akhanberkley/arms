﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('FEIN', function () {
        return function (fein: string) {
            if (!fein)
                return '';
            else {
                var value = fein.toString().trim();
                if (value.length == 9)
                    return value.slice(0, 2) + '-' + value.slice(2);
                else
                    return value;
            }
        };
    });
} 