﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('NA', function () {
        return function (value: any) {
            if (!value || value.toString().trim() == '')
                return '-';
            else
                return value;
        };
    });
} 