﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('submissionTooltip', function (addressFilter: any) {
        return function (s: Submission) {
            if (!s)
                return '';
            else {
                return ((s.Company) ? s.Company.Abbreviation + ' ' : '') +
                    (((s.PolicyNumber) ? ' - Policy #' + s.PolicyNumber : '') +
                        ((s.InsuredName) ? ' - ' + s.InsuredName : '') +
                        ((s.InsuredDBA) ? ' dba ' + s.InsuredDBA : '') +
                        ' (' + s.SubmissionType +
                        ((s.EffectivePeriod) ? ' effective ' + s.EffectivePeriod : '') +
                        ')').substr(3);
            }
        };
    });
} 