﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('YesNo', function () {
        return function (value: boolean) {
            return (value) ? 'Yes' : 'No';
        };
    });
} 