﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('codeItem', function () {
        return function (c: CodeItem) {
            if (!c)
                return '';
            else {
                var desc = c.Description || (<any>c).Name;
                if ((desc || '') == '' || desc == c.Code) {
                    return c.Code;
                } else {
                    return desc + ((c.Code) ? ' (' + c.Code + ')' : '');
                }
            }
        };
    });
} 