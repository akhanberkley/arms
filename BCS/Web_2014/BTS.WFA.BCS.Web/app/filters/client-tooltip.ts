﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('clientTooltip', function () {
        return function (c: Client) {
            if (!c)
                return '';
            else {
                return 'Client ' + c.ClientCoreId;
            }
        };
    });
} 