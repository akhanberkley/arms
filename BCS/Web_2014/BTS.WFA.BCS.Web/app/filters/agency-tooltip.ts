﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.filter('agencyTooltip', function (addressFilter: any) {
        return function (a: Agency) {
            if (!a)
                return '';
            else {
                var address = addressFilter(a.Address);
                return a.Code + ' - ' + a.Name + ((address) ? ' (' + address + ')' : '');
            }
        };
    }); 
}