﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('loadingIndicator', function ($rootScope: ng.IRootScopeService, $timeout: ng.ITimeoutService): ng.IDirective {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: '/app/common/loading-indicator.html',
            scope: true,
            controller: LoadingIndicatorController,
            controllerAs: 'vm'
        };
    });

    class LoadingIndicatorController {
        PendingRequests: any;

        constructor($http: ng.IHttpService) {
            this.PendingRequests = $http.pendingRequests;
        }
    }
} 