﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('validatedField', function ($timeout: ng.ITimeoutService): ng.IDirective {
        return {
            restrict: 'A',
            require: ['^form', '?ngModel'],
            link: (scope: any, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, ctrls: any[]) => {
                //Setup variables
                var options = (attrs['validatedField']) ? scope.$eval(attrs['validatedField']) : {};
                var form = <ValidatedForm>ctrls[0];
                var ngModelCtrl = <ng.INgModelController>ctrls[1];
                var iconOnly = (options.iconOnly == true);
                var iconElem = <ng.IAugmentedJQuery>null;
                var errors = <ValidationResult[]>[];

                if (options.icon) {
                    element.wrap('<div class="validation-icon-wrapper"></div>');
                }

                options.names = options.names || [];
                if (attrs['name']) {
                    options.names.push(attrs['name']);
                }

                //Actions
                var handleErrors = (validationErrors: ValidationResult[]) => {
                    errors = [];
                    for (var i = 0; i < validationErrors.length; i++) {
                        if (options.names.indexOf(validationErrors[i].Property) >= 0) {
                            errors.push(validationErrors[i]);
                        }
                    }

                    if (!iconOnly) {
                        if (ngModelCtrl != null) {
                            ngModelCtrl.$setValidity('servererror',(errors.length == 0));
                        } else {
                            //delaying this because the form input might not exist initially
                            $timeout(function () {
                                for (var i = 0; i < options.names.length; i++) {
                                    var formElem = form[options.names[i]];
                                    if (formElem) {
                                        formElem.$setValidity('servererror',(errors.length == 0));
                                    }
                                }
                            });
                        }
                    }

                    if (options.icon) {
                        if (errors.length > 0) {
                            iconElem = iconElem || angular.element('<i class="fa fa-exclamation-circle validation-icon"></i>');
                            iconElem.attr('title', errors.map((e) => e.Message).join(',')).tooltip({ container: 'body' });
                            element.after(iconElem);
                        } else if (iconElem != null) {
                            iconElem.remove();
                        }
                    }
                };

                if (ngModelCtrl != null) {
                    scope.$watch(() => ngModelCtrl.$viewValue,(val: any, oldVal: any) => {
                        if (val != oldVal && errors.length > 0) {
                            for (var i = 0; i < errors.length; i++) {
                                form.ValidationErrors.splice(form.ValidationErrors.indexOf(errors[i]), 1);
                            }
                            errors = [];
                        }
                    });
                }

                scope.$watchCollection(() => form.ValidationErrors,(validationErrors: ValidationResult[]) => {
                    if (validationErrors) {
                        handleErrors(validationErrors);
                    }
                });
            }
        };
    });
} 