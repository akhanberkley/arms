﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('copyable', function (UtilService: UtilService): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                text: '=',
                enabled: '='
            },
            link: function (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes) {
                if (scope['enabled'] == null || scope['enabled'] == true) {
                    UtilService.SetupCopyableElement(scope, element, scope['text']);
                }
            }
        };
    });
}  