﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('warningMessage', function ($compile): ng.IDirective {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                message: '@',
                regex: '@'
            },
            templateUrl: '/app/common/warning-message.html'
        };
    });
} 