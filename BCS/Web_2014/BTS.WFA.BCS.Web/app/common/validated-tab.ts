﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('validatedTab', function ($timeout: ng.ITimeoutService): ng.IDirective {
        return {
            restrict: 'A',
            require: '^form',
            link: function (scope: any, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, form: ValidatedForm) {
                //Setup variables
                var options = (attrs['validatedTab']) ? scope.$eval(attrs['validatedTab']) : {};

                var propertyNames = (options.names) ? options.names : [];
                var regexs = (options.regexs) ? options.regexs : [];
                for (var i = 0; i < regexs.length; i++) {//in angular 1.3 I think we can just pass in regexs inline
                    regexs[i] = new RegExp(regexs[i]);
                }

                //Actions
                var handleErrors = (validationErrors: ValidationResult[]) => {
                    var hasErrors = false;
                    for (var i = 0; i < validationErrors.length; i++) {
                        if (propertyNames.indexOf(validationErrors[i].Property) >= 0) {
                            hasErrors = true;
                            break;
                        }
                        for (var j = 0; j < regexs.length; j++) {
                            if (regexs[j].test(validationErrors[i].Property)) {
                                hasErrors = true;
                                break;
                            }
                        }
                    }

                    if (hasErrors) {
                        element.addClass('invalid-step');
                    } else {
                        element.removeClass('invalid-step');
                    }
                };

                //Page Load
                if (form.ValidationErrors) {
                    handleErrors(form.ValidationErrors);
                }
                scope.$watchCollection(() => form.ValidationErrors,(validationErrors: ValidationResult[]) => {
                    if (validationErrors) {
                        handleErrors(validationErrors);
                    }
                });
            }
        };
    });
} 