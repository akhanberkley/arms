﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('focusTrigger', function ($timeout: ng.ITimeoutService): ng.IDirective {
        return {
            restrict: 'E',
            link: function (scope: ng.IScope, element: ng.IRootElementService) {
                scope.$on('focusTrigger', function (e, item) {
                    var focusedElement = function () {
                        var elm = item;
                        if (!(elm instanceof jQuery)) {
                            elm = $('[name="' + item + '"]');
                        }

                        if (elm.is(':visible') && elm.css('visibility') != 'hidden') {
                            if (!elm.is(':focusable')) {
                                elm = elm.find('[tabindex!="-1"]:focusable:first');
                            }
                            elm.focus();
                            return true;
                        }
                        return false;
                    };

                    var recursiveTry = function (attemptNumber: number) {
                        if (attemptNumber < 5) {
                            $timeout(function () {
                                if (!focusedElement())
                                    recursiveTry(++attemptNumber);
                            }, 100);
                        }
                    };

                    recursiveTry(0);
                });
            }
        };
    });
}  