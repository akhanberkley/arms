﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('loadingWell', function (): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
                text: '@',
                subText: '@',
                onCancel: '&',
                layout: '@'
            },
            templateUrl: '/app/common/loading-well.html',
            link: function (scope: any, element: ng.IAugmentedJQuery, attrs: ng.IAttributes) {
                scope.HasCancel = (attrs['onCancel'] != null);
            }
        };
    });
}  