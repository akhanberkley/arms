﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('validationMessage', function ($compile: ng.ICompileService): ng.IDirective {
        return {
            restrict: 'E',
            replace: true,
            require: '?^form',
            scope: {
                property: '@',
                regex: '@'
            },
            templateUrl: '/app/common/validation-message.html',
            link: function (scope: any, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, form: ValidatedForm) {
                scope.Message = null;
                var prop = scope.property;
                var regex = (scope.regex) ? new RegExp(scope.regex) : null;

                scope.$watchCollection(() => form.ValidationErrors,(validationErrors: ValidationResult[]) => {
                    if (validationErrors) {
                        scope.Message = null;
                        for (var i = 0; i < validationErrors.length; i++) {
                            if ((prop && prop == validationErrors[i].Property)
                                || (regex && regex.test(validationErrors[i].Property))) {

                                scope.Message = validationErrors[i].Message;
                                break;
                            }
                        }
                    }
                });
            }
        };
    });
} 