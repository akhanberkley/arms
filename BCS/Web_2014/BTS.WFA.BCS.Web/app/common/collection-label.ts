﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('collectionLabel', function (): ng.IDirective {
        return {
            restrict: 'E',
            replace: true,
            template: '<span>'
            + '<span>{{title}}</span> '
            + '<span class="badge" ng-class="{\'empty-collection\': count == undefined || count == 0, \'loading-collection fa\': count == undefined}">{{count}} </span>'
            + '</span>',
            scope: {
                count: '=',
                title: '@'
            }
        };
    });
}  