﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('alert', function (): ng.IDirective {
        return {
            restrict: 'EA',
            templateUrl: '/app/common/alert.html',
            transclude: true,
            replace: true,
            scope: {
                type: '=',
                close: '&'
            },
            controller: function ($scope: any, $element: ng.IAugmentedJQuery, $attrs: ng.IAttributes) {
                $scope.alertType = $scope.type || 'warning';
                $scope.closeable = ($attrs['close'] != null);
            }
        }
    });
}