﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('characterFilter', function (): ng.IDirective {
        return {
            restrict: 'A',
            require: ['ngModel'],
            link: function (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, ctrls: any[]) {
                var ngModel = <ng.INgModelController>ctrls[0];
                var regex = new RegExp(attrs['characterFilter'], 'g');

                ngModel.$parsers.push((val) => {
                    if (val != null) {
                        var numericValue = (val + '').replace(regex, '');
                        if (val != numericValue) {
                            ngModel.$setViewValue(numericValue);
                            ngModel.$render();
                        }
                        return numericValue;
                    } else {
                        return null;
                    }
                });
            }
        };
    });
}