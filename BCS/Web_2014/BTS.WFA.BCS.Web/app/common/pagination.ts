﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('pagination', function ($rootScope: ng.IRootScopeService, $timeout: ng.ITimeoutService): ng.IDirective {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: '/app/common/pagination.html',
            scope: {
                Source: '=source'
            },
            bindToController: true,
            controller: PaginationController,
            controllerAs: 'vm'
        };
    });

    class PaginationController {
        Source: PagedDataSource;
        Pages: number[];
        TotalPages: number;
        PageSizeChanged: boolean;

        constructor($scope: ng.IScope) {
            this.TotalPages = 1;
            this.Pages = [];
            this.PageSizeChanged = false;

            $scope.$watchGroup([() => (this.Source != null) ? this.Source.pageSize : null, () => (this.Source != null) ? this.Source.count : null],(values: any[]) => {
                if (this.Source != null && !isNaN(this.Source.pageSize) && !isNaN(this.Source.count)) {
                    var totalPages = (this.Source.pageSize < 1) ? 1 : Math.ceil(this.Source.count / this.Source.pageSize);
                    this.TotalPages = Math.max(totalPages || 0, 1);

                    this.CalculatePages();
                    if (this.PageSizeChanged) {
                        this.SelectPage(1);
                        this.PageSizeChanged = false;
                    }
                }
            });
        }

        CalculatePages() {
            var maxPages = 6;
            var pages = <number[]>[];
            var startPage = Math.max(this.Source.page - Math.floor(maxPages / 2), 1);
            var endPage = startPage + maxPages - 1;
            if (endPage > this.TotalPages) {
                endPage = this.TotalPages;
                startPage = Math.max(endPage - maxPages + 1, 1);
            }

            for (var i = startPage; i <= endPage; i++) {
                pages.push(i);
            }

            this.Pages = pages;
        }

        SelectPage(page: number) {
            if (this.PageSizeChanged || (this.Source.page !== page && page <= this.TotalPages)) {
                this.Source.changePage(page);
                this.CalculatePages();
            }
        }

        ChangePageSize(pageSize: number) {
            this.PageSizeChanged = true;
            this.Source.pageSize = pageSize;
        }
    }
} 