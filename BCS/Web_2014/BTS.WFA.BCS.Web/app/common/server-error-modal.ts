﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('serverErrorModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/common/server-error-modal.html',
            replace: true,
            scope: {
                RequestUrl: '=requestUrl',
                Message: '=message',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: ServerErrorModalController,
            controllerAs: 'vm'
        };
    });

    class ServerErrorModalController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}