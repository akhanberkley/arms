﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('versionErrorModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/common/version-error-modal.html',
            replace: true,
            bindToController: true,
            controller: VersionErrorModalController,
            controllerAs: 'vm'
        };
    });

    class VersionErrorModalController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService, private $window: ng.IWindowService) {
            super($scope, $state, UtilService);
        }

        RefreshPage() {
            this.$window.location.reload();
        }
    }
}