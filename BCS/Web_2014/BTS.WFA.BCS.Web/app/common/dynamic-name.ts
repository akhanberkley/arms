﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('dynamicName', function (): ng.IDirective {
        return {
            restrict: 'A',
            priority: 10000,
            controller: ($scope: ng.IScope, $element: ng.IAugmentedJQuery, $attrs: ng.IAttributes) => {
                $attrs.$set("name", $scope.$eval($attrs['dynamicName']));
                $element.removeAttr('dynamic-name');
            }
        };
    });
}  