﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('unsavedChangesModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/common/unsaved-changes-modal.html',
            replace: true,
            scope: {
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: UnsavedChangesController,
            controllerAs: 'vm'
        };
    });

    class UnsavedChangesController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);
        }

        Continue() {
            this.ModalInstance.close();
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}