﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('editableField', function (): ng.IDirective {
        return {
            restrict: 'E',
            compile: function ($element: ng.IAugmentedJQuery, $attrs: ng.IAttributes) {
                var readElem = $element.find('read');
                var editElem = $element.find('write');

                var restructured = $('<span>' +
                    '<span ng-if="!(' + $attrs['editing'] + ')">' + readElem.html() + '</span>' +
                    '<span ng-if="' + $attrs['editing'] + '">' + editElem.html() + '</span>' +
                    '</span>');

                $element.replaceWith(restructured);

                return {};
            }
        };
    });
}  