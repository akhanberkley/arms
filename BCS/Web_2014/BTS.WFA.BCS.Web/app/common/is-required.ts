﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('isRequired', function (): ng.IDirective {
        return {
            restrict: 'A',
            scope: {
                required: '=isRequired'
            },
            link: function (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes) {
                var asterisk = angular.element('<i ng-show="required" class="fa fa-asterisk icon-required"></i>');

                scope.$watch('required', function (required) {
                    if (required) {
                        element.prepend(asterisk);
                    } else {
                        asterisk.remove();
                    }
                });
            }
        };
    });
}  