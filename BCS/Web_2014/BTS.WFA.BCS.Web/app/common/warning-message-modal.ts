﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    app.directive('warningMessageModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/common/warning-message-modal.html',
            replace: true,
            scope: {
                Title: '=title',
                Message: '=message',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: WarningMessageModalController,
            controllerAs: 'vm'
        };
    });

    class WarningMessageModalController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilService: UtilService) {
            super($scope, $state, UtilService);
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}