﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export class ClientService {
        constructor(private $http: ng.IHttpService, private $q: ng.IQService, private $filter: ng.IFilterService, private CompanyCustomizationService: CompanyCustomizationService,
            private UtilService: UtilService, private DomainService: DomainService) { }

        private prepViewModel(companyKey: string, client: Client, isNew: boolean) {
            return new this.$q((resolve, reject) => {
                if (client) {
                    var today = this.UtilService.GetCurrentDate();
                    var domains = <ng.IPromise<any>[]>[];
                    domains.push(this.DomainService.CompanyCustomizations(companyKey, 'Client').then((customizationList) => {
                        var validClientCustomizations = customizationList.filter((c) => { return (c.ExpirationDate == null || c.ExpirationDate > today); });
                        client.CompanyCustomizationBindings = this.CompanyCustomizationService.CreateBindings(validClientCustomizations, client.CompanyCustomizations, isNew);
                    }));
                    domains.push(this.DomainService.CompanyInformation(companyKey).then((companySettings) => {
                        if (client.Contacts.length == 0 && !companySettings.UsesCms) {
                            client.Contacts.push({ ContactType: 'Phone', Value: null });
                        }
                    }));

                    this.$q.all(domains).then(() => {
                        resolve();
                    });
                } else {
                    resolve();
                }
            });
        }

        GetNewAddress(isPrimary: boolean) {
            return <ClientAddress>{ Country: 'USA', AddressTypes: ['Mailing'], IsPrimary: isPrimary };
        }

        GetNew(companyKey: string) : ng.IPromise<Client> {
            return new this.$q((resolve, reject) => {
                var client = <Client>{ PrimaryName: {}, AdditionalNames: [], Portfolio: {}, Addresses: [this.GetNewAddress(true)], Contacts: [], CompanyCustomizations: [] };
                this.prepViewModel(companyKey, client, true).then(() => {
                    resolve(client);
                });
            });
        }
        Get(companyKey: string, clientId: number): ng.IPromise<ClientGetResult> {
            return new this.$q((resolve, reject) => {
                this.$http.get('/api/' + companyKey + '/Client/' + clientId).success((data, status, headers, config) => {
                    var client = <Client>data;
                    this.prepViewModel(companyKey, client, false).then(() => {
                        resolve({ Client: client });
                    });
                }).error((data, status, headers, config) => {
                    resolve({ Failure: data });
                });
            });
        }
        GetSubmissions(companyKey: string, clientId: number, paged: boolean, pageOptions?: number[]) {
            return new this.$q((resolve, reject) => {
                var url = '/api/' + companyKey + '/Client/' + clientId + '/Submission?$orderby=EffectiveDate desc, PolicyNumber';
                if (paged) {
                    resolve(this.UtilService.SetupPagedTableDataSource({ url: url, pageSizeOptions: pageOptions }));
                } else {
                    this.$http.get(url).success((data, status, headers, config) => {
                        resolve(<Submission[]>data);
                    }).error((data, status, headers, config) => {
                        resolve([]);
                    });
                }
            });
        }
        GetClassCodes(companyKey: string, clientId: number) {
            return this.$http.get(`/api/${companyKey}/Client/${clientId}/ClassCode?$orderby=CodeValue`).then((result: ng.IHttpPromiseCallbackArg<ClientClassCode[]>) => {
                return result.data;
            });
        }

        Validate(companyKey: string, client: Client) {
            return new this.$q((resolve, reject) => {
                var copy = angular.copy(client);
                copy.CompanyCustomizations = this.CompanyCustomizationService.GetValues(client.CompanyCustomizationBindings);
                delete copy.CompanyCustomizationBindings;

                this.UtilService.CreateSaveRequest(copy, '/api/' + companyKey + '/Client/Validate').success((client, status, headers, config) => {
                    resolve();
                }).error((data, status, headers, config) => {
                    reject(<ValidationResult[]>data);
                });
            });
        }
        Save(companyKey: string, clientId: number, client: Client, returnFullClient: boolean, updatePortfolioName: boolean): ng.IPromise<ClientSaveResult> {
            return new this.$q((resolve, reject) => {
                var copy = angular.copy(client);
                copy.CompanyCustomizations = this.CompanyCustomizationService.GetValues(client.CompanyCustomizationBindings);
                delete copy.CompanyCustomizationBindings;

                var request = this.UtilService.CreateSaveRequest(copy, '/api/' + companyKey + '/Client', clientId, { updatePortfolioName: updatePortfolioName, returnFullClient: returnFullClient });
                request.success((data, status, headers, config) => {
                    var updatedClient = <Client>data;
                    this.prepViewModel(companyKey, updatedClient, false).then(() => {
                        resolve({ Client: updatedClient });
                    });
                }).error((data, status, headers, config) => {
                    resolve({ ValidationErrors: this.UtilService.CreateValidationResultsFromServerResponse(status, data) });
                });
            });
        }

        RemoveClassCode(companyKey: string, clientId: number, classCode: ClientClassCode) {
            return this.$http.delete(`/api/${companyKey}/Client/${clientId}/ClassCode/${classCode.CodeValue}`);
        }
        AddClassCode(companyKey: string, clientId: number, classCode: ClientClassCode) {
            return this.$http.post(`/api/${companyKey}/Client/${clientId}/ClassCode/${classCode.CodeValue}`, null);
        }
    }

    app.service('ClientService', ClientService);
}