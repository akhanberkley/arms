﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export class SubmissionWorkflowService {
        constructor(public $http: ng.IHttpService, public $q: ng.IQService, public $filter: ng.IFilterService, public DomainService: DomainService, public $modal: ng.ui.bootstrap.IModalService, public UserService: UserService,
            public UtilService: UtilService, public AgencyService: AgencyService, public CompanyCustomizationService: CompanyCustomizationService, public SearchService: SearchService, public SearchWorkflowService: SearchWorkflowService) { }

        GetWorkflow(companyKey: string, workflowType: SubmissionWorkflowType, initSubmission: Submission) {
            var w = new SubmissionWorkflow(companyKey, this, this.UtilService.GetCurrentDate(), workflowType, initSubmission);
            return w.WorkflowLoaded;
        }

        GetPotentialDuplicates(companyKey: string, clients: SimilarClient[], submissionType: string, effectiveDate: string, applicationId: string) {
            var criteria = { SubmissionType: submissionType, Clients: clients, EffectiveDate: effectiveDate, IgnoreApplicationId: applicationId };
            return this.$http.post('/api/' + companyKey + '/DuplicateSubmission', criteria).then((response) => {
                var submissions = <Submission[]>response.data;
                angular.forEach(submissions,(s) => { this.UtilService.SetupSubmissionForUI(s); });
                return submissions;
            });
        }
    }

    export enum SubmissionWorkflowType {
        NewApplication,
        ExistingApplication,
        ViewingSubmission
    }
    interface SimilarClient {
        Company: CompanySummary;
        ClientId: number;
    }
    export class SubmissionWorkflow {
        WorkflowLoaded: ng.IPromise<SubmissionWorkflow>;
        CompanySettings: CompanyConfiguration;
        processDuplicates: boolean;
        dupIgnoredAppId: string;

        AllCustomizations: CompanyCustomization[];
        AllPrimaryPolicySymbols: PolicySymbol[];
        AllPackagePolicySymbols: PolicySymbol[];
        AllUnderwritingUnits: UnderwritingUnit[];
        AllUnderwriters: UnderwritingPersonnel[];
        AllUnderwritingAnalysts: UnderwritingPersonnel[];
        AllUnderwritingTechnicians: UnderwritingPersonnel[];

        FutureEffectiveDate: Date;
        SubmissionTypes: SubmissionType[];
        SubmissionStatuses: SubmissionStatus[];
        Carriers: CodeItem[];
        PolicySystems: CodeItem[];
        UnderwritingUnits: UnderwritingUnit[];
        Underwriters: UnderwritingPersonnel[];
        UnderwritingAnalysts: UnderwritingPersonnel[];
        UnderwritingTechnicians: UnderwritingPersonnel[];
        PrimaryPolicySymbols: PolicySymbol[];
        PackagePolicySymbols: PolicySymbol[];
        Addresses: SubmissionAddress[];
        AdditionalNames: SubmissionName[];
        Agents: Agent[];
        AgentUnspecifiedReasons: Agent[];
        DuplicateMatches: Submission[];
        DuplicateSubmissionFields: SubmissionDisplayField[];

        Submissions: Submission[];
        SubmissionType: SubmissionType;
        InsuredName: string;
        InsuredDBA: string;
        DbaAsObject: ClientName;
        Agency: AgencySummary;
        TypeAheadAgency: AgencySummary;
        OriginalAgencySelection: AgencySummary;
        AgencyLicensedStatePromise: ng.IPromise<State[]>;
        AgencyLicensedStateMessage: string;
        AgencyInWarningState: boolean;
        AgencyShortWarningMessage: string;
        AgencyOptionsAreLimited: boolean;
        LimitedAgencyOptions: AgencySummary[];

        Agent: Agent;
        AgentPopulatePromise: ng.IPromise<void>;
        AgentUnspecifiedReasonLabel = 'Other';

        ClearanceNotes: string;

        Cleared: boolean;
        Clearable: boolean;
        NoDuplicatesApplicable: boolean;
        Duplicate: SubmissionSummary;
        DuplicateMatchesPromise: ng.IPromise<Submission[]>;
        SimilarClientIdsPromise: ng.IPromise<SimilarClient[]>;
        LoadingDuplicates: boolean;
        HasExactDuplicate: boolean;

        constructor(public companyKey: string, private service: SubmissionWorkflowService, private today: Date, private workflowType: SubmissionWorkflowType, private initSubmission: Submission = null) {
            this.Submissions = [];
            this.Addresses = [];
            this.AdditionalNames = [];
            this.processDuplicates = (workflowType != SubmissionWorkflowType.ViewingSubmission);
            this.dupIgnoredAppId = (initSubmission != null) ? initSubmission.Id : null;

            var domainPromises = <ng.IPromise<any>[]>[];
            domainPromises.push(service.UserService.UserPromise.then((user) => {
                if (user.LimitingAgency != null) {
                    this.AgencyOptionsAreLimited = true;
                    this.refreshLimitedAgencySelection(today);
                } else {
                    this.AgencyOptionsAreLimited = false;
                };
            }));
            domainPromises.push(service.DomainService.CompanyInformation(companyKey).then((companySettings) => {
                this.CompanySettings = companySettings;
                this.SubmissionTypes = companySettings.SubmissionTypes;
                if (companySettings.FutureEffectiveDateDayCount) {
                    this.FutureEffectiveDate = moment(this.today).add(companySettings.FutureEffectiveDateDayCount, 'day').toDate();
                }
            }));
            domainPromises.push(service.DomainService.SubmissionStatuses(companyKey).then((items) => { this.SubmissionStatuses = items; }));
            domainPromises.push(service.DomainService.CompanyCustomizations(companyKey, 'Submission').then((items) => { this.AllCustomizations = items; }));
            domainPromises.push(service.DomainService.PrimaryPolicySymbols(companyKey).then((items) => { this.AllPrimaryPolicySymbols = items; }));
            domainPromises.push(service.DomainService.PackagePolicySymbols(companyKey).then((items) => { this.AllPackagePolicySymbols = items; }));
            domainPromises.push(service.DomainService.Carriers(companyKey).then((items) => { this.Carriers = items; }));
            domainPromises.push(service.DomainService.PolicySystems(companyKey).then((items) => { this.PolicySystems = items; }));
            domainPromises.push(service.DomainService.SubmissionSummaryFields(companyKey, 'clearance-duplicate').then((items) => { this.DuplicateSubmissionFields = items; }));
            domainPromises.push(service.DomainService.UnderwritingUnits(companyKey).then((items) => { this.AllUnderwritingUnits = items; }));
            domainPromises.push(service.DomainService.Underwriters(companyKey).then((items) => { this.AllUnderwriters = items; }));
            domainPromises.push(service.DomainService.UnderwritingAnalysts(companyKey).then((items) => { this.AllUnderwritingAnalysts = items; }));
            domainPromises.push(service.DomainService.UnderwritingTechnicians(companyKey).then((items) => { this.AllUnderwritingTechnicians = items; }));
            domainPromises.push(service.DomainService.AgentUnspecifiedReasons(companyKey).then((items) => {
                this.AgentUnspecifiedReasons = [];
                angular.forEach(items,(i) => { this.AgentUnspecifiedReasons.push({ FullName: i, Type: this.AgentUnspecifiedReasonLabel }); });
            }));

            this.WorkflowLoaded = new service.$q((resolve, reject) => {
                service.$q.all(domainPromises).then(() => {
                    if (initSubmission) {
                        this.InsuredName = initSubmission.InsuredName;
                        this.InsuredDBA = initSubmission.InsuredDBA;
                        this.Addresses = initSubmission.Addresses;
                        this.AdditionalNames = initSubmission.AdditionalNames;
                        this.Duplicate = initSubmission.DuplicateOfSubmission;

                        this.SubmissionType = this.SubmissionTypes.filter((t) => { return (t.TypeName == initSubmission.SubmissionType); })[0];

                        var selectedStatus = this.SubmissionStatuses.filter((s) => { return (s.Code == initSubmission.Status); });
                        if (selectedStatus.length > 0) {
                            initSubmission.StatusAsObject = selectedStatus[0];
                        }

                        initSubmission.ViewState = this.getNewViewState();
                        this.SetCustomizations(initSubmission, false);
                        this.Submissions.push(initSubmission);
                    } else {
                        var newSubmission = this.AddNewSubmission(null);

                        var defaultStatus = this.SubmissionStatuses.filter((s) => { return s.IsDefault; });
                        if (defaultStatus.length > 0) {
                            newSubmission.StatusAsObject = defaultStatus[0];
                        }

                        var defaultType = this.SubmissionTypes.filter((t) => { return t.TypeIsDefault; });
                        if (defaultType.length > 0) {
                            this.SubmissionType = defaultType[0];
                        }
                    }

                    resolve(this);
                });
            });
        }

        private getValidPersonnel(submission: Submission, agencyPersonnelType: string, fullList: UnderwritingPersonnel[], populateAll: boolean) {
            var effectiveDate = submission.EffectiveDate;
            var validPersonnel = fullList.filter((p) => { return (effectiveDate == null || p.RetirementDate == null || p.RetirementDate >= effectiveDate); });
            var primary = <UnderwritingPersonnel>null;

            if (this.Agency != null && submission.UnderwritingUnit != null) {
                var agencyUnderwritingUnits = this.Agency.UnderwritingUnits.filter((uu) => { return uu.Code == submission.UnderwritingUnit.Code; });
                if (agencyUnderwritingUnits.length == 1) {
                    var agencyPersonnelList = <UnderwritingPersonnel[]>((<any>agencyUnderwritingUnits[0])[agencyPersonnelType]);
                    agencyPersonnelList = agencyPersonnelList.filter((p) => { return (effectiveDate == null || p.RetirementDate == null || p.RetirementDate >= effectiveDate); });

                    if (!populateAll) {
                        validPersonnel = agencyPersonnelList;
                        var primaryPersonnel = validPersonnel.filter((p) => p.IsPrimary);
                        if (primaryPersonnel.length > 0) {
                            primary = primaryPersonnel[0];
                        }
                    } else {
                        var personnelWithUnassigned = <UnderwritingPersonnel[]>[];
                        angular.forEach(validPersonnel,(p) => {
                            var cloned = angular.copy(p);
                            var agencyUnderwriterMatches = agencyPersonnelList.filter((u) => { return u.UserName == p.UserName; });
                            cloned.Assignment = (agencyUnderwriterMatches.length == 0) ? 'Unassigned' : 'Assigned';
                            if (agencyUnderwriterMatches.length > 0 && agencyUnderwriterMatches[0].IsPrimary) {
                                primary = cloned;
                            }
                            personnelWithUnassigned.push(cloned);
                        });
                        validPersonnel = this.service.$filter('orderBy')(personnelWithUnassigned, ['Assignment', 'FullName']);
                    }
                }
            }

            return { Personnel: validPersonnel, Primary: primary };
        }
        private refreshPersonnel(submission: Submission, setDefaults: boolean) {
            var results = this.getValidPersonnel(submission, 'Underwriters', this.AllUnderwriters, this.CompanySettings.UsesAllUnderwritersForAllAssignments);
            submission.ViewState.Underwriters = results.Personnel;
            if (setDefaults) {
                submission.Underwriter = results.Primary;
            }

            results = this.getValidPersonnel(submission, 'Analysts', this.AllUnderwritingAnalysts, this.CompanySettings.UsesAllUnderwritersForAllAssignments);
            submission.ViewState.UnderwritingAnalysts = results.Personnel;
            if (setDefaults) {
                submission.UnderwritingAnalyst = results.Primary;
            }

            results = this.getValidPersonnel(submission, 'Technicians', this.AllUnderwritingTechnicians, this.CompanySettings.UsesAllUnderwritersForAllAssignments);
            submission.ViewState.UnderwritingTechnicians = results.Personnel;
            if (setDefaults) {
                submission.UnderwritingTechnician = results.Primary;
            }
        }
        private refreshUnderwriting(submission: Submission, setDefaults: boolean) {
            this.PrimaryPolicySymbols = this.AllPrimaryPolicySymbols.filter((p) => { return (submission.EffectiveDate == null || p.RetirementDate == null || p.RetirementDate >= submission.EffectiveDate); });
            this.PackagePolicySymbols = this.AllPackagePolicySymbols.filter((p) => { return (submission.EffectiveDate == null || p.RetirementDate == null || p.RetirementDate >= submission.EffectiveDate); });
            this.UnderwritingUnits = (this.Agency == null) ? this.AllUnderwritingUnits : this.Agency.UnderwritingUnits;

            var submissionsToUpdate = (submission == null) ? this.Submissions : [submission];
            angular.forEach(submissionsToUpdate,(s) => {
                if (setDefaults && this.UnderwritingUnits.length == 1) {
                    s.UnderwritingUnit = this.UnderwritingUnits[0];
                }

                if (s.UnderwritingUnit != null) {
                    var match = this.UnderwritingUnits.filter((i) => i.Code == s.UnderwritingUnit.Code);
                    if (match.length == 0) {
                        s.UnderwritingUnit = null;
                    }
                }

                this.refreshPersonnel(s, setDefaults);
            });
        }
        private refreshLimitedAgencySelection(date: Date) {
            this.service.SearchService.AutoComplete.Agencies(this.companyKey, { SearchTerm: null, EffectiveDate: date }, null).then((agencies) => {
                this.LimitedAgencyOptions = agencies;
            });
        }

        AgencyAutoCompleteSearch = (term: string) => {
            var effectiveDate = this.Submissions[0].EffectiveDate || this.today;
            var criteria = <AgencyAutoCompleteSearchCriteria>{ SearchTerm: term, EffectiveDate: effectiveDate };
            if (this.SubmissionType.TypeIsRenewal) {
                criteria.RenewalCancelDate = effectiveDate;
            } else {
                criteria.CancelDate = effectiveDate;
            }

            return this.service.SearchService.AutoComplete.Agencies(this.companyKey, criteria, 10);
        }
        DuplicatesAutoCompleteSearch = (term: string) => {
            return this.service.SearchService.AutoComplete.Submissions(this.companyKey, term, 10, this.Submissions[0].Id, true);
        }
        OpenAgencyViewModal = (agency: Agency) => {
            this.service.AgencyService.OpenAgencyModal(agency);
        }

        TypeChanged(submission: Submission) {
            if (submission.EffectiveDate != null) {
                //We won't process duplicates if it's an existing submission
                if (this.processDuplicates && submission == this.Submissions[0]) {
                    this.RefreshDuplicateMatches();
                }
            }
        }

        EffectiveDateIsFutureDate(submission: Submission) {
            return this.service.UtilService.DatesAreEqual(submission.EffectiveDate, this.FutureEffectiveDate);
        }
        SetFutureEffectiveDate(submission: Submission) {
            submission.EffectiveDate = this.FutureEffectiveDate;
            this.EffectiveDateChanged(submission, true);
        }
        EffectiveDateChanged(submission: Submission, refreshDependencies: boolean) {
            if (submission.EffectiveDate instanceof Date) {
                submission.ExpirationDate = moment(submission.EffectiveDate).add(1, 'years').toDate();

                if (this.AgencyOptionsAreLimited) {
                    this.refreshLimitedAgencySelection(submission.EffectiveDate);
                }

                if (refreshDependencies) {
                    var agency = this.Agency;
                    if (submission.EffectiveDate != null) {
                        //We won't process duplicates if it's an existing submission
                        if (this.processDuplicates && submission == this.Submissions[0]) {
                            this.RefreshDuplicateMatches();
                        }

                        //Reselect incase its referral date is applicable now
                        if (agency != null && agency.ReferralDate != null && (submission.EffectiveDate == null || submission.EffectiveDate >= agency.ReferralDate)) {
                            this.SelectAgency(agency, refreshDependencies, null);
                        }
                    }

                    this.refreshUnderwriting(submission,(this.workflowType != SubmissionWorkflowType.ViewingSubmission));
                }
            }
        }

        SelectedAddressesChanged() {
            if (this.Agency != null) {
                this.AgencyLicensedStatePromise.then((states) => {
                    this.SetLicenseStateMessage(states);
                });
            }
        }
        SetLicenseStateMessage(licensedStates: State[]) {
            if (this.CompanySettings.SubmissionEnforceAgencyLicensedState) {
                var selectedStates = this.Addresses.map((a) => { return a.State; });
                if (licensedStates == null || selectedStates.length == 0) {
                    this.AgencyLicensedStateMessage = null;
                } else {
                    var agencyStates = licensedStates.map((s) => { return s.Abbreviation; });
                    var externalStates = <string[]>[];
                    for (var i = 0; i < selectedStates.length; i++) {
                        if (agencyStates.indexOf(selectedStates[i]) < 0 && externalStates.indexOf(selectedStates[i]) < 0) {
                            externalStates.push(selectedStates[i]);
                        }
                    }

                    if (externalStates.length > 0) {
                        this.AgencyLicensedStateMessage = 'The insured address' + ((externalStates.length == 1) ? '' : 'es') + ' in '
                        + externalStates.join(', ')
                        + ' is outside of the agency\'s licensed states ('
                        + (agencyStates.join(', ') || 'N/A')
                        + ').';
                    } else {
                        this.AgencyLicensedStateMessage = null;
                    }
                }
            }
        }
        CalculateAgencyWarnings() {
            this.AgencyInWarningState = (this.Agency != null && (this.OriginalAgencySelection != null || this.AgencyLicensedStateMessage != null));

            var messages = <string[]>[];
            if (this.Agency) {
                if (this.OriginalAgencySelection != null) {
                    messages.push('Agency updated by referral');
                }
                if (this.AgencyLicensedStateMessage != null) {
                    messages.push('Verify licensed states');
                }
            }

            this.AgencyShortWarningMessage = messages.join('. ');
        }

        SelectAgency(agency: AgencySummary, setDefaults: boolean, originalSelectionAgency: AgencySummary) {
            var submission = this.Submissions[0];
            this.OriginalAgencySelection = originalSelectionAgency;
            this.TypeAheadAgency = agency;//not setting this to null here since it could be changed a few times (referral) and would flicker on screen
            this.SetLicenseStateMessage(null);
            this.Agency = null;
            this.Agent = null;
            this.Agents = [];

            if (agency != null) {
                if (agency.ReferralDate != null && (submission.EffectiveDate == null || submission.EffectiveDate >= agency.ReferralDate)) {
                    this.service.AgencyService.GetAgency(this.companyKey, agency.ReferralAgencyCode, false).then((a) => {
                        this.SelectAgency(a, setDefaults, originalSelectionAgency || agency);
                    });
                } else {
                    this.Agency = agency;

                    this.Agent = null;
                    this.Agents = [];
                    this.AgentPopulatePromise = this.service.AgencyService.GetAgencyAgents(this.companyKey, agency.Code).then((agents) => {
                        agency.Agents = agents.filter((a) => { return (submission.EffectiveDate == null || a.RetirementDate == null || a.RetirementDate >= submission.EffectiveDate); });
                        if (this.AgentUnspecifiedReasons.length == 0) {
                            this.Agents = agency.Agents;
                        } else {
                            angular.forEach(agency.Agents,(a) => { a.Type = 'Agent'; });
                            this.Agents = agency.Agents.concat(this.AgentUnspecifiedReasons);
                        }
                    });
                    this.AgencyLicensedStatePromise = this.service.AgencyService.GetLicensedStates(this.companyKey, agency.Code).then((states) => {
                        this.SetLicenseStateMessage(states);
                        this.CalculateAgencyWarnings();
                        return states;
                    });

                    this.service.AgencyService.GetUnderwritingUnits(this.companyKey, agency.Code).then((underwritingUnits) => {
                        if (this.Agency != null && this.Agency.Code == agency.Code) {//user might have deselected prior to completion
                            this.Agency.UnderwritingUnits = underwritingUnits;
                            this.refreshUnderwriting(submission, setDefaults);
                        }
                    });

                    if (this.processDuplicates && submission.EffectiveDate) {
                        this.ProcessDuplicateMatches(submission.EffectiveDate);
                    }
                }
            } else {
                this.CalculateAgencyWarnings();
                this.refreshUnderwriting(submission, setDefaults);
            }
        }
        SelectAgencyFromModal(agency: Agency) {
            this.SelectAgency(agency, true, null);
        }
        SelectAgencyFromExistingSubmission(submission: Submission) {
            return new this.service.$q((resolve, reject) => {
                if (submission.Agency == null) {
                    this.SelectAgency(null, false, null);
                    resolve();
                } else {
                    this.SelectAgency(submission.Agency, false, null);

                    //If an agent was selected, make sure we reselect it
                    if (submission.Agent != null || submission.AgentUnspecifiedReason != null) {
                        this.AgentPopulatePromise.then(() => {
                            this.SelectAgent(submission.Agent, submission.AgentUnspecifiedReason);
                        });
                    }

                    resolve();
                }
            });
        }

        SelectAgent(agent: Agent, unspecifiedReason: Agent) {
            for (var i = 0; i < this.Agents.length; i++) {
                var item = this.Agents[i];
                if ((agent != null && item.Type != this.AgentUnspecifiedReasonLabel && agent.FullName == item.FullName) || (unspecifiedReason != null && item.Type == this.AgentUnspecifiedReasonLabel && unspecifiedReason == item.FullName)) {
                    this.Agent = item;
                    break;
                }
            }
        }

        OpenAgencySearchModal() {
            var modal = this.service.$modal.open({
                windowClass: 'modal-wide modal-tall-no-footer modal-no-padding',
                template: '<agency-search-modal use-renewal-cancel-date="useRenewalCancelDate" effective-date="effectiveDate" modal="modal"></agency-search-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['useRenewalCancelDate'] = this.SubmissionType.TypeIsRenewal;
                    $scope['effectiveDate'] = this.Submissions[0].EffectiveDate || this.today;
                    $scope['modal'] = modal;
                }
            });

            modal.result.then((selection) => {
                this.SelectAgencyFromModal(selection);
            });
        }
        OpenDuplicateSubmissionSearchModal() {
            var modal = this.service.$modal.open({
                windowClass: 'modal-wide modal-tall-no-footer modal-no-padding',
                template: '<submission-search-modal modal="modal" cross-company="true"></submission-search-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['modal'] = modal;
                }
            });

            modal.result.then((selection) => {
                this.Submissions[0].DuplicateOfSubmission = selection;
            });
        }

        PolicyNumberGenerationChanged(submission: Submission) {
            if (submission.ViewState.GeneratePolicyNumber) {
                submission.PolicyNumber = null;
                submission.PolicyMod = null;
            } else {
                this.service.UtilService.RaiseFocusTrigger('PolicyNumber');
            }
        }
        UnderwritingUnitChanged(submission: Submission) {
            this.refreshPersonnel(submission, true);
        }

        CarrierSearch = (term: string) => {
            return new this.service.$q((resolve, reject) => {
                resolve(this.service.$filter('filter')(this.Carriers, term));
            });
        }

        NoDuplicatesApplicableChanged() {
            this.NoDuplicatesApplicable = !this.NoDuplicatesApplicable;
            if (this.NoDuplicatesApplicable) {
                this.Duplicate = null;
            }
            this.Cleared = false;
        }
        SelectDuplicate(submission: Submission) {
            if (this.Duplicate == submission) {
                this.Duplicate = null;
            } else {
                this.Duplicate = submission;
                this.NoDuplicatesApplicable = false;
            }
            this.Cleared = false;
        }
        Clear() {
            this.Cleared = true;
        }

        RefreshSimilarClientIds(client: Client, state: string) {
            this.SimilarClientIdsPromise = new this.service.$q((resolve, reject) => {
                var similarClientParameters = { insuredName: this.InsuredName, state: state };

                var ids = <SimilarClient[]>[];
                if (client) {
                    ids.push({ Company: client.Company, ClientId: client.ClientCoreId });
                }
                if (this.InsuredName == null || this.InsuredName == '') {
                    resolve(ids);
                } else {
                    this.service.$http.get('/api/' + this.companyKey + '/Client/Similar', { params: similarClientParameters }).then((response) => {
                        var results = <CrossCompanyClientSearchResult[]>response.data;
                        if (results) {
                            var companyAbbr = this.companyKey.toUpperCase();
                            angular.forEach(results,(companyResult) => {
                                if (!(companyResult.Company.Abbreviation == companyAbbr && companyResult.ClientId == client.ClientCoreId)) {
                                    ids.push({ Company: companyResult.Company, ClientId: companyResult.ClientId });
                                }
                            });
                        }

                        resolve(ids);
                    });
                }
            });
        }
        RefreshDuplicateMatches() {
            this.NoDuplicatesApplicable = false;
            this.Cleared = false;

            var effectiveDate = this.Submissions[0].EffectiveDate;
            this.DuplicateMatchesPromise = new this.service.$q((resolve, reject) => {
                this.SimilarClientIdsPromise.then((similarClients) => {
                    if (effectiveDate == null) {
                        reject();
                    } else {
                        if (similarClients == null || similarClients.length == 0) {
                            resolve([]);
                        } else {
                            this.service.GetPotentialDuplicates(this.companyKey, similarClients, this.SubmissionType.TypeName, this.service.UtilService.DateToString(effectiveDate), this.dupIgnoredAppId).then((submissions) => {
                                resolve(submissions);
                            });
                        }
                    }
                });
            });

            this.ProcessDuplicateMatches(effectiveDate);
        }
        ProcessDuplicateMatches(effectiveDate: any) {
            this.LoadingDuplicates = true;
            this.DuplicateMatchesPromise.then((matches) => {
                this.DuplicateMatches = [];
                this.Cleared = false;
                this.Clearable = true;
                if (this.Duplicate != null) {
                    var matchingDups = matches.filter((s) => { return s.Id == this.Duplicate.Id; });
                    this.Duplicate = (matchingDups.length == 1) ? matchingDups[0] : null;
                }
                var exactMatches = <Submission[]>[], potentialMatches = <Submission[]>[];
                angular.forEach(matches,(m) => {
                    if (this.service.UtilService.DatesAreEqual(m.EffectiveDate, effectiveDate) && (this.Agency != null && m.Agency != null && m.Agency.Code == this.Agency.Code)) {
                        exactMatches.push(m);
                    } else {
                        potentialMatches.push(m);
                    }
                });

                this.HasExactDuplicate = (exactMatches.length > 0);
                this.DuplicateMatches = (exactMatches.length > 0) ? exactMatches : potentialMatches;
                this.LoadingDuplicates = false;
            });
        }

        GetClientUrl(submission: Submission) {
            return this.service.UtilService.CreateClientLink(submission.ClientCoreId, null);
        }
        GetAgencyUrl(submission: Submission) {
            return this.service.UtilService.CreateAgencyLink((submission.Agency) ? submission.Agency.Code : null, submission.Company.Abbreviation);
        }
        GetSubmissionUrl(submission: Submission) {
            return this.service.UtilService.CreateSubmissionLink(submission.Id, submission.Company.Abbreviation);
        }
        GetApplicationUrl(submission: Submission) {
            return this.service.UtilService.CreateApplicationLink(submission.Id, submission.Company.Abbreviation);
        }

        SetCustomizations(submission: Submission, setDefaults: boolean) {
            var customizations = this.AllCustomizations.filter((c) => { return (c.ExpirationDate == null || c.ExpirationDate > this.today) && (this.workflowType == SubmissionWorkflowType.ViewingSubmission || c.VisibleOnAdd); });
            submission.CompanyCustomizationBindings = this.service.CompanyCustomizationService.CreateBindings(customizations, submission.CompanyCustomizations, setDefaults);
        }
        CleanSubmissionForCopy(submission: Submission, clearValues: boolean) {
            submission.Id = null;
            submission.SubmissionNumber = null;
            submission.SequenceNumber = null;
            submission.StatusDate = this.today;
            angular.forEach(submission.Notes,(note) => {
                delete note.Id;
            });
            if (submission.StatusHistory != null && submission.StatusHistory.length > 0) {
                angular.forEach(submission.StatusHistory,(sh) => {
                    delete sh.Id;
                    delete sh.Date;
                    delete sh.ChangeDate;
                    delete sh.ChangeUser;
                });

                submission.StatusHistory.shift();//the first element is not needed (it's the current status)
            }

            if (clearValues) {
                submission.PolicyNumber = null;
                submission.PolicyMod = null;
                submission.PolicySymbol = null;
                submission.PackagePolicySymbols = [];

                angular.forEach(submission.CompanyCustomizationBindings,(b) => {
                    if (b.Customization.ExcludeValueOnCopy) {
                        b.Value.CodeValue = null;
                        b.Value.TextValue = null;
                    }
                });
            }
        }
        CopyPolicyDetails(original: Submission, destination: Submission) {
            destination.ClassCodes = original.ClassCodes;
            destination.PolicySymbol = original.PolicySymbol;
            destination.PackagePolicySymbols = original.PackagePolicySymbols;

            destination.UnderwritingUnit = original.UnderwritingUnit;
            destination.Underwriter = original.Underwriter;
            destination.UnderwritingAnalyst = original.UnderwritingAnalyst;
            destination.UnderwritingTechnician = original.UnderwritingTechnician;
            this.refreshUnderwriting(destination, false);

            destination.CompanyCustomizations = angular.copy(original.CompanyCustomizations);
            this.SetCustomizations(destination, false);

            destination.PriorCarrier = original.PriorCarrier;
            destination.PriorCarrierPremium = original.PriorCarrierPremium;
        }

        private getNewViewState() {
            return {
                GeneratePolicyNumber: (this.CompanySettings.PolicyNumberGeneratable && this.workflowType != SubmissionWorkflowType.ViewingSubmission),
                ReturnFullSubmission: (this.workflowType == SubmissionWorkflowType.ViewingSubmission)
            };
        }
        AddNewSubmission(submissionToDuplicate: Submission) {
            var newSubmission = <Submission>{ ViewState: this.getNewViewState(), CompanyCustomizations: [], ClassCodes: [], Notes: [], PackagePolicySymbols: [], StatusDate: this.today };
            if (submissionToDuplicate != null) {
                newSubmission = angular.copy(submissionToDuplicate);
                this.CleanSubmissionForCopy(newSubmission, true);
            } else {
                this.refreshUnderwriting(newSubmission, true);
                this.SetCustomizations(newSubmission, true);
            }

            this.Submissions.push(newSubmission);
            return newSubmission;
        }
    }

    app.service('SubmissionWorkflowService', SubmissionWorkflowService);
} 