﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export class AdministrationLink {
        constructor(public name: string,
            public state: string) {
        }
    }

    export class UserService {
        public UserPromise: ng.IPromise<User>;

        constructor(private $rootScope: AppRootScope, private $http: ng.IHttpService, private $q: ng.IQService, private UtilService: UtilService, private CacheService: CacheService) {

            this.UserPromise = new $q((resolve, reject) => {
                //Wait until we've been logged in before getting the user; we are doing this separately opposed to allowing the Index view to authenticate
                //because the ADFS redirect would cause us to lose the full link if a user just clicked on a link from another system and wasn't logged in
                var loginListener = $rootScope.$on('Login-Completed',() => {
                    this.getCurrentUserPromise().then((user) => {
                        resolve(user);

                        //Once we're logged in, calculate user's local time and current app version
                        $http.get('/api/WebClient/ServerTime').then((response) => {
                            if (response.data) {
                                //determine timezone offset
                                var hourDifference = moment(<string>response.data, 'YYYY-MM-DDTHH:mm:ss.SSS').diff(moment()) / 1000 / 3600;
                                UtilService.TimezoneOffset = Math.round(hourDifference) * -60;

                                //determine application version
                                $rootScope.appVersion = response.headers("Version");
                            }
                        });
                    });

                    loginListener();//remove the listener
                });
            });
        }

        private getCurrentUserPromise() {
            return new this.$q((resolve, reject) => {
                this.$http.get('/api/User/Current').then((response: ng.IHttpPromiseCallbackArg<User>) => {
                    var data = response.data;
                    if (data) {
                        if (data.DefaultCompany == null) {
                            data.DefaultCompany = data.Companies[0];
                        }

                        if (data.DefaultCompany != null) {
                            var matchedCompanies = data.Companies.filter((c) => c.Code == data.DefaultCompany.Code);
                            data.SelectedCompany = (matchedCompanies.length == 1) ? matchedCompanies[0] : null;
                        }

                        data.ChangeSelectedCompany = (newCompany) => {
                            //Clear cache (submission search, etc) since that data is no longer applicable
                            this.CacheService.Clear();
                            data.SelectedCompany = newCompany;

                            //Persist the selected company as the new default
                            this.$http.post('/api/User/Current/DefaultCompany/' + newCompany.Code, null);
                        }

                        data.IsSystemAdministrator = (data.UserType == 'System Administrator');
                        data.IsReadOnly = (data.UserType == 'Read Only');
                        data.IsInRole = (role: string) => (data.Roles.indexOf(role) >= 0 || data.IsSystemAdministrator);

                        data.AdministratorLinks = [];
                        if (data.IsInRole('Administrator - Class Codes'))
                            data.AdministratorLinks.push(new AdministrationLink('Class Codes', 'cn.administration.classcodes'));
                        if (data.IsInRole('Administrator - Custom Fields'))
                            data.AdministratorLinks.push(new AdministrationLink('Custom Fields', 'cn.administration.companycustomizations'));
                        if (data.IsInRole('Administrator - Users'))
                            data.AdministratorLinks.push(new AdministrationLink('Users', 'cn.administration.users'));
                        if (data.IsInRole('Administrator - Submission Tables'))
                            data.AdministratorLinks.push(new AdministrationLink('Submission Tables', 'cn.administration.submissiontables'));
                        if (data.IsInRole('Administrator - Other Carriers'))
                            data.AdministratorLinks.push(new AdministrationLink('Other Carriers', 'cn.administration.othercarriers'));
                        if (data.IsInRole('Administrator - Policy Symbols'))
                            data.AdministratorLinks.push(new AdministrationLink('Policy Symbols', 'cn.administration.policysymbols'));
                    }

                    resolve(data);
                });
            });
        }

        RefreshCurrentUserPromise() {
            this.UserPromise = this.getCurrentUserPromise();
        }

        Get(companyKey: string, userId: string): ng.IPromise<User> {
            return new this.$q((resolve, reject) => {
                this.$http.get('/api/' + companyKey + '/User/' + userId).success((user, status, headers, config) => {
                    resolve(user);
                });
            });
        }

        Save(companyKey: string, userId: number, user: User) {
            return new this.$q((resolve, reject) => {
                var request = <ng.IHttpPromise<User>>this.UtilService.CreateSaveRequest(user, '/api/' + companyKey + '/User', userId);
                request.success((data, status, headers, config) => {
                    resolve({ User: data });
                }).error((data, status, headers, config) => {
                    resolve({ ValidationErrors: this.UtilService.CreateValidationResultsFromServerResponse(status, data) });
                });
            });
        }

        GetAllActiveCompanyUsers(companyKey: string) {
            return this.$http.get('/api/' + companyKey + '/User?userListReport=true&$filter=Active eq true&$orderby=Username').then((response) => {
                return <User[]>response.data;
            });
        }
    }

    app.service('UserService', UserService);
}