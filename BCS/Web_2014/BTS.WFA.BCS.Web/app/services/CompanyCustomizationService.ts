﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export class CompanyCustomizationService {
        constructor(private $filter: ng.IFilterService, private UtilService: UtilService) { }

        CreateBindings(customizations: CompanyCustomization[], values: CompanyCustomizationValue[], setDefaults: boolean) {
            var copiedCustomizations = angular.copy(customizations);
            var today = this.UtilService.GetCurrentDate();
            var bindings = <CompanyCustomizationValueBinding[]>[];
            angular.forEach(copiedCustomizations,(c) => {
                var binding = <CompanyCustomizationValueBinding>{ Customization: c, Value: {} };

                for (var i = 0; i < values.length; i++) {
                    if (values[i].Customization == c.Description) {
                        binding.Value = values[i];
                        break;
                    }
                }

                if (c.DataType == 'List') {
                    c.Options = c.Options.filter((o) => {
                        return (binding.Value != null && binding.Value.CodeValue != null && binding.Value.CodeValue.Code == o.Code)
                            || (o.ExpirationDate == null || o.ExpirationDate > today);
                    });

                    if (setDefaults) {
                        binding.Value.CodeValue = c.DefaultOption;
                    }
                } else if (c.DataType == 'Money') {
                    if (binding.Value.TextValue != null && binding.Value.TextValue.indexOf('$') == 0) {
                        binding.Value.TextValue = binding.Value.TextValue.substr(1);
                    }
                } else if (c.DataType == 'Boolean') {
                    if (binding.Value.TextValue != 'True') {
                        binding.Value.TextValue = null;
                    }
                }

                bindings.push(binding);
            });

            return bindings;
        }

        GetValues(bindings: CompanyCustomizationValueBinding[]) {
            var values = <CompanyCustomizationValue[]>[];
            angular.forEach(bindings,(b) => {
                if ((b.Value.TextValue != null && b.Value.TextValue != '') || b.Value.CodeValue != null) {
                    var val = angular.copy(b.Value);
                    val.Customization = b.Customization.Description;
                    val.TextValue = this.FormatTextValue(val.TextValue, b.Customization.DataType);
                    values.push(val);
                }
            });

            return values;
        }

        FormatTextValue(value: string, dataType: string) {
            if (dataType == 'Date') {
                value = this.$filter('date')(value, 'MM/dd/yyyy');
            } else if (dataType == 'Money') {
                if (value.indexOf('$') < 0) {
                    value = '$' + value;
                }
            }

            return value;
        }
    }

    app.service('CompanyCustomizationService', CompanyCustomizationService);
}