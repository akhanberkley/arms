﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export class AgencyService {
        constructor(private $http: ng.IHttpService, private $q: ng.IQService, private $modal: ng.ui.bootstrap.IModalService,
            private CompanyCustomizationService: CompanyCustomizationService, private UtilService: UtilService,
            private DomainService: DomainService, private SearchWorkflowService: SearchWorkflowService, private UserService: UserService) { }

        GetAgency(companyKey: string, agencyCode: string, generateUrls: boolean): ng.IPromise<Agency> {
            return new this.$q((resolve, reject) => {
                if (agencyCode == null || agencyCode == '') {
                    resolve(null);
                } else {
                    this.$http.get('/api/' + companyKey + '/Agency/' + agencyCode).then((response) => {
                        var agency = <Agency>response.data;
                        if (agency) {
                            this.UtilService.SetupAgencyForUI(agency, generateUrls, companyKey);
                        }
                        resolve(agency);
                    });
                }
            });
        }

        private getChildrenHelper<T>(companyKey: string, agencyCode: string, subUrl: string): ng.IPromise<T> {
            return new this.$q((resolve, reject) => {
                if (agencyCode == null || agencyCode == '') {
                    resolve(null);
                } else {
                    this.$http.get('/api/' + companyKey + '/Agency/' + agencyCode + '/' + subUrl).then((response) => {
                        resolve(<T>response.data);
                    });
                }
            });
        }
        GetAgencyAgents(companyKey: string, agencyCode: string) {
            return this.getChildrenHelper<Agent[]>(companyKey, agencyCode, 'Agents').then((agents) => {
                angular.forEach(agents,(a) => { a.RetirementDate = this.UtilService.ParseServerDate(a.RetirementDate); });
                return agents;
            });
        }
        GetUnderwritingUnits(companyKey: string, agencyCode: string) {
            return this.getChildrenHelper<UnderwritingUnit[]>(companyKey, agencyCode, 'UnderwritingUnits');
        }
        GetLicensedStates(companyKey: string, agencyCode: string) {
            return this.getChildrenHelper<State[]>(companyKey, agencyCode, 'LicensedStates');
        }

        SyncAgency(companyKey: string, agencyCode: string) {
            return new this.$q((resolve, reject) => {
                this.$http.post('/api/' + companyKey + '/Agency/' + agencyCode + '/Sync', null).then((response) => {
                    var data = <AgencySyncResult>response.data;
                    if (data.Agency) {
                        this.UtilService.SetupAgencyForUI(data.Agency, true, companyKey);
                    }
                    resolve(data);
                });
            });
        }

        OpenAgencyModal(agency: AgencySummary) {
            var modal = this.$modal.open({
                windowClass: 'modal-wide modal-tall-no-footer',
                template: '<agency-modal agency="agency" modal="modal"></agency-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['agency'] = agency;
                    $scope['modal'] = modal;
                }
            });
        }
    }

    app.service('AgencyService', AgencyService);
}