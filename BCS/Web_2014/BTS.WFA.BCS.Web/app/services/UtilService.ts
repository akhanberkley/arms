﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';

    export interface AppState {
        Loading: boolean;
        PageGenerating: boolean;
        StillWaiting: boolean;
    }

    export interface PagedDataSourceConfig {
        url?: string;
        pageSize?: number;
        pageSizeOptions?: number[];
        fullLoad?: boolean;
        params?: any;
        onDataBind?: (item: any) => void;
        onComplete?: () => void;
        processResponse?: (source: PagedDataSource, results: any) => any[];
    }
    export class PagedDataSource {
        url: string;
        pageSize: number;
        pageSizeOptions: number[];
        count: number;
        page: number;
        items: any[];
        array: any[];
        abortSearch: () => void;
        changePage: (pageNumber: number) => void;
        updateItems: (items: any[]) => void;

        constructor(config: PagedDataSourceConfig) {
            this.url = config.url;
            this.pageSizeOptions = config.pageSizeOptions || [10, 25, 50];
            this.pageSize = config.pageSize || this.pageSizeOptions[0];
            this.count = null;
            this.page = 1;
            this.items = [];

            this.updateItems = (items) => {
                if (config.onDataBind) {
                    angular.forEach(items,(i) => { config.onDataBind(i); });
                }
                this.items = items;
            };
        }
    }

    export class UtilService {
        AppState: AppState;
        TimezoneOffset: number;

        constructor(private $filter: ng.IFilterService, private $timeout: ng.ITimeoutService, private $rootScope: ng.IRootScopeService,
            private $window: ng.IWindowService, private $state: ng.ui.IStateService, private $http: ng.IHttpService,
            private $q: ng.IQService, private $animate: ng.IAnimateService, private AlertService: AlertService) {

            this.AppState = { Loading: false, PageGenerating: false, StillWaiting: false };
            this.TimezoneOffset = 0;
        }

        BrowserIsInternetExplorer() {
            return (this.$window.navigator.userAgent.indexOf('MSIE ') >= 0 || this.$window.navigator.userAgent.indexOf('Trident/') >= 0);
        }

        SetPageTitle(title: string) {
            this.$window.document.title = ((title) ? title + ' - ' : '') + 'Berkley Clearance System';
        }
        RaiseFocusTrigger(trigger: any) {
            this.$rootScope.$broadcast('focusTrigger', trigger);
        }

        DateToString(date: any) { return this.$filter('date')(date, 'yyyy-MM-ddTHH:mm:ss'); }
        DatesAreEqual(a: Date, b: Date) {
            if (a == null || b == null)
                return false;
            return (a.getTime() == b.getTime());
        }
        ParseServerDate(dateString: string) {
            return (dateString) ? moment(dateString, 'YYYY-MM-DD').toDate() : null;
        }
        ParseServerDateWithTime(dateString: string) {
            return (dateString) ? moment(dateString, 'YYYY-MM-DDTHH:mm:ss.SSS').add(this.TimezoneOffset, 'minutes').toDate() : null;
        }
        GetCurrentDate() {
            var today = moment().toDate();
            today.setHours(0, 0, 0, 0);
            return today;
        }

        CreateClientLink(clientCoreId: number, altState?: string) {
            var stateName = 'cn.search.client' + ((altState) ? '.' + altState : '');
            return (clientCoreId == null) ? null : this.$state.href(stateName, { clientCoreId: clientCoreId });
        }
        CreateAgencyLink(agencyCode: string, companyKey: string) {
            return (agencyCode == null) ? null : this.$state.href('cn.search.agency', { agencyCode: agencyCode, companyKey: companyKey.toLowerCase() });
        }
        CreateSubmissionLink(submissionId: string, companyKey: string) {
            return (submissionId == null) ? null : this.$state.href('cn.search.submission', { submissionId: submissionId, companyKey: companyKey.toLowerCase() });
        }
        CreateApplicationLink(submissionId: string, companyKey: string) {
            return (submissionId == null) ? null : this.$state.href('cn.viewapplication', { submissionId: submissionId, companyKey: companyKey.toLowerCase() });
        }
        NavigateToNewSubmission(clientId: number) { this.$state.go('cn.clearance', { clientCoreId: clientId }); }

        SetupAgencyDates(a: AgencySummary) {
            a.EffectiveDate = this.ParseServerDate(a.EffectiveDate);
            a.CancelDate = this.ParseServerDate(a.CancelDate);
            a.RenewalCancelDate = this.ParseServerDate(a.RenewalCancelDate);
            a.ReferralDate = this.ParseServerDate(a.ReferralDate);
        }
        SetupAgencyForUI(a: Agency, generateUrls: boolean, companyKey: string) {
            this.SetupAgencyDates(a);

            a.Expanded = false;
            a.Expandable = (!(a.Note == null || a.Note.length == 0) || a.Parent != null || a.Children.length > 0);
            a.Expand = () => {
                if (a.Expandable) {
                    a.Expanded = !a.Expanded;
                }
            };

            a.Dates = [];
            if (a.CancelDate) { a.Dates.push({ Name: 'Cancel', Date: a.CancelDate }); }
            if (a.RenewalCancelDate) { a.Dates.push({ Name: 'Renewal Cancel', Date: a.RenewalCancelDate }); }
            if (a.ReferralDate) { a.Dates.push({ Name: 'Referral', Date: a.ReferralDate }); }

            a.BranchesAsString = a.Branches.map((b) => b.Name).join(', ');

            if (generateUrls) {
                a.AgencyUrl = this.CreateAgencyLink(a.Code, companyKey);
                a.AgencyReferralUrl = (a.ReferralAgencyCode != null) ? this.CreateAgencyLink(a.ReferralAgencyCode, companyKey) : '';
                a.ParentAgencyUrl = (a.Parent != null) ? this.CreateAgencyLink(a.Parent.Code, companyKey) : '';

                var service = this;
                angular.forEach(a.Children,(c) => {
                    c.ChildAgencyUrl = service.CreateAgencyLink(c.Code, companyKey);
                });
            }
        }
        SetupAgentForUI(a: Agent, generateUrls: boolean, companyKey: string) {
            if (generateUrls) {
                a.AgencyUrl = (a.Agency != null) ? this.CreateAgencyLink(a.Agency.Code, companyKey) : '';
            }

            a.Contacts = [];
            if (a.PrimaryPhone) { a.Contacts.push({ Name: 'Phone', Value: this.$filter('phoneNumber')(a.PrimaryPhone) }); }
            if (a.SecondaryPhone) { a.Contacts.push({ Name: 'Secondary', Value: this.$filter('phoneNumber')(a.SecondaryPhone) }); }
            if (a.Fax) { a.Contacts.push({ Name: 'Fax', Value: this.$filter('phoneNumber')(a.Fax) }); }
            if (a.EmailAddress) { a.Contacts.push({ Name: 'Email', Value: a.EmailAddress }); }
        }
        SetupSubmissionForUI(s: Submission) {
            var service = this;
            s.EffectiveDate = service.ParseServerDate(s.EffectiveDate);
            s.ExpirationDate = service.ParseServerDate(s.ExpirationDate);
            s.SubmittedDate = service.ParseServerDateWithTime(s.SubmittedDate);
            angular.forEach(s.Notes,(n) => {
                n.Date = service.ParseServerDateWithTime(n.Date);
            });
            angular.forEach(s.StatusHistory,(h) => {
                h.ChangeDate = service.ParseServerDateWithTime(h.ChangeDate);
            });
        }

        StateIsSearch(name: string) {
            return (['cn.search.agencies', 'cn.search.clients', 'cn.search.submissions'].indexOf(this.$state.current.name) >= 0);
        }
        LogSearchQuery(searchType: string, query: any) {
            ga('send', { hitType: 'event', eventCategory: 'search', eventAction: searchType, eventLabel: JSON.stringify(query) });
        }

        ODataString(value: string) { return "'" + value.replace(/'/g, "''").replace(/&/g, "%26").replace(/#/g, "%23") + "'"; }
        ODataDate(value: Date) { return this.$filter('date')(value, 'yyyy-MM-ddTHH:mm:ss') + 'Z'; }
        CreateSaveRequest(item: any, url: string, itemId: any = null, params: any = null) {
            var config = <ng.IRequestConfig>{};
            config.params = params;
            return (itemId == null) ? this.$http.post(url, item, config) : this.$http.put(url + '/' + itemId, item, config);
        }

        CreateVerticalTable(arr: any[], columns: number) {
            var table = <any[]>[];
            var numberPerColumn = arr.length / columns;
            if (numberPerColumn != Math.floor(numberPerColumn))
                numberPerColumn = Math.floor(numberPerColumn) + 1;
            for (var row = 0; row < numberPerColumn; row++) {
                var tableRow = <any[]>[];
                for (var i = 0; i < columns; i++) {
                    var originalIndex = (numberPerColumn * i) + row;
                    tableRow.push((arr.length > originalIndex) ? arr[originalIndex] : null);
                }
                table.push(tableRow);
            }

            return table;
        }
        ToTitleCase(str: string) {
            return (!str) ? null : str.replace(/\w\S*/g,(txt) => {
                return (txt == txt.toUpperCase()) ? txt : txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
        CSVEncode(str: string) { return '"' + (str || '').trim().replace('"', '""') + '"'; }

        SetValidationResults(form: ValidatedForm, errors?: ValidationResult[]) {
            errors = errors || [];
            if (errors.length == 0 && form.$setPristine) {
                form.$setPristine();
            }
            form.ValidationErrors = errors;
        }
        CreateValidationResultsFromServerResponse(statusCode: number, data: any) {
            var results = <ValidationResult[]>[];
            if (statusCode == 500) {
                var exception = <ServerException>data;
                var valResult = { Property: 'Server Error', Message: 'Server Error' };
                if (exception.ExceptionMessage) {
                    valResult.Message += ': ' + exception.ExceptionMessage;
                }
                results.push(valResult);
            } else if (statusCode == 400) {
                results = <ValidationResult[]>data;
            }

            return results;
        }

        AddressesAreEqual(a1: Address, a2: Address) {
            return ((a1.Address1 || '') == (a2.Address1 || '')
                && (a1.Address2 || '') == (a2.Address2 || '')
                && (a1.City || '') == (a2.City || '')
                && (a1.State || '') == (a2.State || '')
                && (a1.PostalCode || '') == (a2.PostalCode || ''));
        }

        SetLoadingFlagDelayed(promise: ng.IPromise<any>, delay: number) {
            var appState = this.AppState;
            var $timeout = this.$timeout;
            appState.Loading = true;

            var waitingTimeout = this.$timeout(() => {
                appState.StillWaiting = true;
            }, delay);

            promise.then(() => {
                $timeout.cancel(waitingTimeout);
                appState.Loading = false;
                appState.StillWaiting = false;
            });
        }

        SetupPagedTableDataSource(config: PagedDataSourceConfig) {
            var $http = this.$http;
            var source = new PagedDataSource(config);

            if (source.url) {
                var abortSearch = new this.$q((resolve, reject) => {
                    source.abortSearch = () => {
                        resolve();
                    };
                });

                if (config.fullLoad) {
                    source.changePage = (page) => {
                        source.page = page;

                        var startIndex = (source.page - 1) * source.pageSize;
                        source.updateItems(source.array.slice(startIndex, startIndex + source.pageSize));
                    };

                    $http.get(source.url, { params: config.params, timeout: abortSearch }).then((response) => {
                        source.array = config.processResponse(source, response.data);
                        source.count = source.array.length;
                        (config.onComplete || angular.noop)();

                        source.changePage(source.page);
                    });
                } else {
                    source.changePage = (page) => {
                        source.page = page;
                        var pageFilter = '&$top=' + source.pageSize + '&$skip=' + ((source.page - 1) * source.pageSize);
                        $http.get(source.url + pageFilter).then((response) => {
                            source.updateItems(<any[]>response.data);
                        });
                    };

                    $http.get(source.url + '&$top=' + source.pageSize + '&$count=true', { timeout: abortSearch }).then((response) => {
                        source.updateItems(<any[]>response.data);
                        source.count = parseInt(response.headers('odata-count'));
                        (config.onComplete || angular.noop)();
                    });
                }
            }

            return source;
        }

        SetupCopyableElement(scope: ng.IScope, element: JQuery, textToCopy?: string) {
            if (textToCopy == null) {
                textToCopy = element.text();
                scope.$watch(() => element.text(), function (value) {
                    textToCopy = value;
                });
            }

            var zeroClipboard = new ZeroClipboard(element);
            zeroClipboard.on('ready',(event: any) => {
                element.attr('title', `Click to copy "${textToCopy}"`).addClass('cursor-pointer copy-animation');
            });
            zeroClipboard.on('copy',(event: any) => {
                textToCopy = textToCopy || '';
                event.clipboardData.setData("text/plain", textToCopy.toString());

                var $animate = this.$animate;
                this.$timeout(() => {
                    $animate.addClass(element, 'copying').then(function () {
                        element.removeClass('copying');
                    });
                });

                this.AlertService.Notify(NotificationType.Info, `Copied "${textToCopy}"`);
            });

            //clicking on a flash object updates the page title to whatever's after the # in the url.. what the heck IE?
            if (this.BrowserIsInternetExplorer()) {
                var originalTitle = this.$window.document.title;

                zeroClipboard.on('copy',(event: any) => {
                    this.$window.document.title = originalTitle;
                });
            }
        }
    }

    app.service('UtilService', UtilService);
}