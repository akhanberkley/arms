﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export class DomainService {
        private cachedPromises: any;
        constructor(private $http: ng.IHttpService, private $q: ng.IQService, private UtilService: UtilService) {
            this.cachedPromises = {};
        }

        private getCachedPromise<T>(url: string, customLogic: (items: T) => void = null) {
            var lowerCaseUrl = url.toLowerCase();
            var promise = <ng.IPromise<T>>this.cachedPromises[lowerCaseUrl];
            if (promise == null) {
                promise = this.cachedPromises[lowerCaseUrl] = this.$http.get(url).then((response) => {
                    var items = <T>response.data;
                    if (customLogic) {
                        customLogic(items);
                    }
                    return items;
                });
            }

            return promise;
        }

        ResetCache() {
            this.cachedPromises = {};
        }
        States() {
            return this.getCachedPromise<State[]>('/api/Domains/State');
        }
        Countries() {
            return this.getCachedPromise<CodeItem[]>('/api/Domains/Country');
        }
        AddressTypes() {
            return this.getCachedPromise<string[]>('/api/Domains/AddressType');
        }
        LegacyAddressTypes() {
            return this.getCachedPromise<string[]>('/api/Domains/LegacyAddressType');
        }
        ContactTypes() {
            return this.getCachedPromise<string[]>('/api/Domains/ContactType');
        }
        ClientNameTypes() {
            return this.getCachedPromise<string[]>('/api/Domains/ClientNameType');
        }
        HazardGradeTypes(companyKey: string) {
            return this.getCachedPromise<string[]>('/api/' + companyKey + '/Domains/HazardGradeType');
        }
        PolicySystems(companyKey: string) {
            return this.getCachedPromise<CodeItem[]>('/api/' + companyKey + '/Domains/PolicySystem');
        }

        PolicySymbols(companyKey: string, filter?: string) {
            var queryString = '';
            if (filter) {
                queryString = '?$filter=' + filter;
            }
            return this.getCachedPromise<PolicySymbol[]>('/api/' + companyKey + '/PolicySymbol' + queryString,(items) => {
                angular.forEach(items,(item) => { item.RetirementDate = this.UtilService.ParseServerDate(item.RetirementDate); });
            });
        }
        PrimaryPolicySymbols(companyKey: string) {
            return this.PolicySymbols(companyKey, 'PrimaryOption eq true');
        }
        PackagePolicySymbols(companyKey: string) {
            return this.PolicySymbols(companyKey, 'PackageOption eq true');
        }

        PolicySymbolNumberControls(companyKey: string) {
            return this.getCachedPromise<NumberControl[]>('/api/' + companyKey + '/Domains/NumberControl?$filter=NumberControlType/TypeName eq ' + this.UtilService.ODataString('Policy Number by Policy Symbol'));
        }

        Companies() {
            return this.getCachedPromise<CompanySummary[]>('/api/Company');
        }
        CompanyInformation(companyKey: string) {
            return this.getCachedPromise<CompanyConfiguration>('/api/Company/' + companyKey + '/Settings');
        }
        CompanyConfigurations() {
            return this.getCachedPromise<CompanyConfiguration[]>('/api/Company/Configuration');
        }
        CompanyCustomizations(companyKey: string, usageType: string) {
            var usageFilter = (usageType) ? `?$filter=Usage eq BTS.WFA.BCS.ViewModels.CompanyCustomizationUsage'${usageType}'` : ``;
            return this.getCachedPromise<CompanyCustomization[]>('/api/' + companyKey + '/CompanyCustomization' + usageFilter,(items) => {
                angular.forEach(items,(item) => {
                    if (item.ExpirationDate) {
                        item.ExpirationDate = this.UtilService.ParseServerDate(item.ExpirationDate);
                    }

                    angular.forEach(item.Options,(o) => {
                        if (o.ExpirationDate) {
                            o.ExpirationDate = this.UtilService.ParseServerDate(o.ExpirationDate);
                        }
                    });
                });
            });
        }
        SubmissionSummaryFields(companyKey: string, viewName: string) {
            return this.getCachedPromise<SubmissionDisplayField[]>('/api/' + companyKey + '/SubmissionDisplay/Table/' + viewName + '/Field');
        }
        SubmissionStatuses(companyKey: string) {
            return this.getCachedPromise<SubmissionStatus[]>('/api/' + companyKey + '/Domains/SubmissionStatus');
        }
        Carriers(companyKey: string) {
            return this.getCachedPromise<CodeItem[]>('/api/' + companyKey + '/Carrier?$orderby=Description');
        }
        UnderwritingUnits(companyKey: string) {
            return this.getCachedPromise<UnderwritingUnit[]>('/api/' + companyKey + '/Underwriting/Unit');
        }
        Underwriters(companyKey: string) {
            return this.getCachedPromise<UnderwritingPersonnel[]>('/api/' + companyKey + '/Underwriting/Underwriter',(items) => {
                angular.forEach(items,(item) => { item.RetirementDate = this.UtilService.ParseServerDate(item.RetirementDate); });
            });
        }
        UnderwritingAnalysts(companyKey: string) {
            return this.getCachedPromise<UnderwritingPersonnel[]>('/api/' + companyKey + '/Underwriting/Analyst',(items) => {
                angular.forEach(items,(item) => { item.RetirementDate = this.UtilService.ParseServerDate(item.RetirementDate); });
            });
        }
        UnderwritingTechnicians(companyKey: string) {
            return this.getCachedPromise<UnderwritingPersonnel[]>('/api/' + companyKey + '/Underwriting/Technician',(items) => {
                angular.forEach(items,(item) => { item.RetirementDate = this.UtilService.ParseServerDate(item.RetirementDate); });
            });
        }
        AgentUnspecifiedReasons(companyKey: string) {
            return this.getCachedPromise<string[]>('/api/' + companyKey + '/Agent/UnspecifiedReason');
        }
        UserTypes() {
            return this.getCachedPromise<string[]>('/api/Domains/UserType');
        }
        UserRoles(companyKey: string) {
            return this.getCachedPromise<CodeItem[]>('/api/' + companyKey + '/Domains/UserRole');
        }
    }

    app.service('DomainService', DomainService);
}