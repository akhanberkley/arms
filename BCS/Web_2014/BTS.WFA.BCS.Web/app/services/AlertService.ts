﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export interface AlertOptions {
        title: string;
        message: string;
    }

    export enum NotificationType {
        Success,
        Info,
        Warning,
        Error
    }

    export class AlertService {
        constructor(private $modal: ng.ui.bootstrap.IModalService) { }

        Notify(nofiticationType: NotificationType, message: string) {
            toastr.clear();
            switch (nofiticationType) {
                case NotificationType.Success: toastr.success(message); break;
                case NotificationType.Info: toastr.info(message); break;
                case NotificationType.Warning: toastr.warning(message); break;
                case NotificationType.Error: toastr.error(message); break;
            }
        }

        OpenStandardModal(options: AlertOptions) {
            var modal = this.$modal.open({
                size: 'sm',
                template: '<standard-message-modal title="title" message="message" modal="modal"></standard-message-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['title'] = options.title;
                    $scope['message'] = options.message;
                    $scope['modal'] = modal;
                }
            });
        }

        OpenWarningModal(options: AlertOptions) {
            var modal = this.$modal.open({
                size: 'sm',
                template: '<warning-message-modal title="title" message="message" modal="modal"></warning-message-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['title'] = options.title;
                    $scope['message'] = options.message;
                    $scope['modal'] = modal;
                }
            });
        }
    }

    app.service('AlertService', AlertService);
}