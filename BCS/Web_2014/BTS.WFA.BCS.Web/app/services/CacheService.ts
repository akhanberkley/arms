﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export class CacheService {
        Session: ng.ICacheObject;
        Recent: ng.ICacheObject;

        constructor(private $cacheFactory: ng.ICacheFactoryService) {
            this.Session = $cacheFactory('Session');
            this.Recent = $cacheFactory('Recent', { capacity: 10 });
        }

        Clear() {
            this.Session.removeAll();
            this.Recent.removeAll();
        }
    }

    app.service('CacheService', CacheService);
}