﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export class SubmissionService {
        constructor(private $http: ng.IHttpService, private $state: ng.ui.IStateService, private $q: ng.IQService,
            private $modal: ng.ui.bootstrap.IModalService, private CompanyCustomizationService: CompanyCustomizationService,
            private UtilService: UtilService, private DomainService: DomainService, private SearchService: SearchService,
            private UserService: UserService, private SubmissionWorkflowService: SubmissionWorkflowService) { }

        private handleStatusSerialization(submissionOrStatusUpdate: any) {
            if (submissionOrStatusUpdate.StatusAsObject != null) {
                submissionOrStatusUpdate.Status = submissionOrStatusUpdate.StatusAsObject.Code;
                delete submissionOrStatusUpdate.StatusAsObject;
            }
            if (submissionOrStatusUpdate.StatusDate != null) {
                submissionOrStatusUpdate.StatusDate = this.UtilService.DateToString(submissionOrStatusUpdate.StatusDate);
            }
        }
        private getSerializableSubmission(workflow: SubmissionWorkflow, submission: Submission) {
            var copy = angular.copy(submission);
            delete copy.ViewState;
            delete copy.Id;
            delete copy.StepSummary;
            delete copy.EffectivePeriod;
            delete copy.SubmittedDate;

            if (copy.UnderwritingUnit != null) {
                delete copy.UnderwritingUnit.Underwriters;
                delete copy.UnderwritingUnit.Analysts;
                delete copy.UnderwritingUnit.Technicians;
            }

            copy.ClassCodes = [];
            angular.forEach(submission.ClassCodes,(cc) => { copy.ClassCodes.push({ CodeValue: cc.CodeValue }); });

            copy.SubmissionType = (workflow.SubmissionType == null) ? null : workflow.SubmissionType.TypeName;
            copy.InsuredName = workflow.InsuredName;
            if (workflow.DbaAsObject) {
                copy.InsuredDBA = workflow.DbaAsObject.BusinessName;
            } else {
                copy.InsuredDBA = workflow.InsuredDBA;
            }

            copy.Addresses = workflow.Addresses;
            copy.AdditionalNames = workflow.AdditionalNames;
            copy.EffectiveDate = this.UtilService.DateToString(copy.EffectiveDate);
            copy.ExpirationDate = this.UtilService.DateToString(copy.ExpirationDate);

            copy.Agency = (workflow.Agency) ? { Code: workflow.Agency.Code } : null;
            copy.Agent = (workflow.Agent != null && workflow.Agent.Type != workflow.AgentUnspecifiedReasonLabel) ? workflow.Agent : null;
            copy.AgentUnspecifiedReason = (workflow.Agent != null && workflow.Agent.Type == workflow.AgentUnspecifiedReasonLabel) ? workflow.Agent.FullName : null;

            if (workflow.ClearanceNotes != null && workflow.ClearanceNotes != '') {
                copy.Notes.push({ Text: workflow.ClearanceNotes });
            }

            copy.DuplicateOfSubmission = (copy.DuplicateOfSubmission) ? { Id: copy.DuplicateOfSubmission.Id, Company: copy.DuplicateOfSubmission.Company } : null;

            this.handleStatusSerialization(copy);

            copy.CompanyCustomizations = this.CompanyCustomizationService.GetValues(submission.CompanyCustomizationBindings);
            delete copy.CompanyCustomizationBindings;

            return copy;
        }

        Get(companyKey: string, submissionId: string) {
            return this.$http.get('/api/' + companyKey + '/Submission/' + submissionId).then((response) => {
                var submission = <Submission>response.data;
                if (submission) {
                    this.UtilService.SetupSubmissionForUI(submission);
                }

                return submission;
            });
        }
        GetApplicationSubmissions(companyKey: string, submissionId: string) {
            return this.$http.get('/api/' + companyKey + '/Application/' + submissionId).then((response) => {
                var submissions = <Submission[]>response.data;
                angular.forEach(submissions,(s) => {
                    this.UtilService.SetupSubmissionForUI(s);
                });

                return submissions;
            });
        }
        GetClientsNonApplicationSubmissions(companyKey: string, submissionId: string, onComplete: () => void) {
            return this.UtilService.SetupPagedTableDataSource({ url: '/api/' + companyKey + '/Application/' + submissionId + '/ClientsNonApplicationSubmissions?$orderby=EffectiveDate desc, PolicyNumber', onComplete: onComplete });
        }

        OpenSubmissionModal(submission: Submission, allowEdit: boolean) {
            var modal = this.$modal.open({
                windowClass: 'modal-wide modal-tall-no-footer',
                template: '<submission-modal submission-id="submissionId" company-key="companyKey" allow-modal-edit="allowEdit" modal="modal"></submission-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['companyKey'] = submission.Company.Abbreviation;
                    $scope['submissionId'] = submission.Id;
                    $scope['allowEdit'] = allowEdit;
                    $scope['modal'] = modal;
                }
            });
        }

        Validate(companyKey: string, originalSubmissionId: string, submission: Submission, workflow: SubmissionWorkflow) {
            return new this.$q((resolve, reject) => {
                var copy = this.getSerializableSubmission(workflow, submission);
                this.UtilService.CreateSaveRequest(copy, '/api/' + companyKey + '/Submission/Validate', originalSubmissionId, { 'generatePolicyNumber': submission.ViewState.GeneratePolicyNumber }).success((submission, status, headers, config) => {
                    resolve();
                }).error((data, status, headers, config) => {
                    reject(data);
                });
            });
        }
        Save(companyKey: string, submissionId: string, submission: Submission, workflow: SubmissionWorkflow) {
            return new this.$q((resolve, reject) => {
                var copy = this.getSerializableSubmission(workflow, submission);
                var request = this.UtilService.CreateSaveRequest(copy, '/api/' + companyKey + '/Submission', submissionId, { 'generatePolicyNumber': submission.ViewState.GeneratePolicyNumber, 'returnFullSubmission': submission.ViewState.ReturnFullSubmission });
                request.success((data, status, headers, config) => {
                    var result = <Submission>data;
                    if (submission.ViewState.ReturnFullSubmission) {
                        this.UtilService.SetupSubmissionForUI(result);
                        workflow.SetCustomizations(result, false);
                    }

                    resolve({ Submission: result });
                }).error((data, status, headers, config) => {
                    resolve({ ValidationErrors: this.UtilService.CreateValidationResultsFromServerResponse(status, data) });
                });
            });
        }

        private condenseStatusUpdateObject(update: SubmissionStatusUpdate) {
            return <SubmissionStatusUpdate>{ StatusAsObject: update.StatusAsObject, Status: update.Status, StatusReason: update.StatusReason, StatusDate: update.StatusDate, StatusNotes: update.StatusNotes };
        }
        UpdateStatus(companyKey: string, updateApplication: boolean, submissionId: string, update: SubmissionStatusUpdate): ng.IPromise<{ ValidationErrors?: ValidationResult[] }> {
            return new this.$q((resolve, reject) => {
                var copy = this.condenseStatusUpdateObject(update);
                this.handleStatusSerialization(copy);
                var request = this.UtilService.CreateSaveRequest(copy, `/api/${companyKey}/${(updateApplication) ? 'Application' : 'Submission'}/${submissionId}`, 'Status');
                request.success((updateResult, status, headers, config) => {
                    resolve({});
                }).error((data, status, headers, config) => {
                    resolve({ ValidationErrors: this.UtilService.CreateValidationResultsFromServerResponse(status, data) });
                });
            });
        }
        RevertStatus(companyKey: string, submissionId: string, historyId: number, update: SubmissionStatusUpdate): ng.IPromise<{ ValidationErrors?: ValidationResult[] }> {
            return new this.$q((resolve, reject) => {
                var copy = this.condenseStatusUpdateObject(update);
                this.handleStatusSerialization(copy);
                var request = this.UtilService.CreateSaveRequest(copy, `/api/${companyKey}/Submission/${submissionId}/StatusHistory/${historyId}`, 'Revert');
                request.success((updateResult, status, headers, config) => {
                    resolve({});
                }).error((data, status, headers, config) => {
                    resolve({ ValidationErrors: this.UtilService.CreateValidationResultsFromServerResponse(status, data) });
                });
            });
        }

        Delete(companyKey: string, submissionId: string) {
            return this.$http.delete('/api/' + companyKey + '/Submission/' + submissionId).then((response) => {
                if (response.data) {
                    angular.forEach(response.data,(s) => { this.UtilService.SetupSubmissionForUI(s); });
                }

                return response.data;
            });
        }
        UpdateAssociatedSubmissions(companyKey: string, submissions: SubmissionSummary[]) {
            var associatedSubmissions = <Submission[]>[];
            for (var i = 1; i < submissions.length; i++) {
                associatedSubmissions.push({ Id: submissions[i].Id });
            }

            return this.$http.put('/api/' + companyKey + '/Submission/' + submissions[0].Id + '/AssociatedSubmissions', associatedSubmissions).then((response) => {
                return response.data;
            });
        }
    }

    app.service('SubmissionService', SubmissionService);
}