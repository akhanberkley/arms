﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export class SearchService {
        Search: SearchServiceSearches;
        AutoComplete: SearchServiceAutoCompletes;

        constructor(public $http: ng.IHttpService, public $q: ng.IQService, public $state: ng.ui.IStateService, public $filter: ng.IFilterService,
            public CacheService: CacheService, public DomainService: DomainService, public UtilService: UtilService, public CompanyCustomizationService: CompanyCustomizationService, public ClientService: ClientService) {
            this.Search = new SearchServiceSearches(this);
            this.AutoComplete = new SearchServiceAutoCompletes(this);
        }
    }

    export class SearchServiceSearches {
        constructor(private service: SearchService) { }

        Agencies(companyKey: string, criteria: AgencySearchCriteria, generateUrls: boolean, onComplete: () => void) {
            var UtilService = this.service.UtilService;
            var query = <string[]>[];

            if (criteria.AgencyName) {
                query.push("contains(AgencyName," + UtilService.ODataString(criteria.AgencyName) + ")");
            }
            if (criteria.AgencyCode) {
                query.push("contains(AgencyNumber," + UtilService.ODataString(criteria.AgencyCode) + ")");
            }
            if (criteria.AgencyCity) {
                query.push("contains(City," + UtilService.ODataString(criteria.AgencyCity) + ")");
            }
            if (criteria.AgencyState) {
                query.push("State eq " + UtilService.ODataString(criteria.AgencyState));
            }
            if (criteria.EffectiveDate) {
                query.push("(EffectiveDate le " + UtilService.ODataDate(criteria.EffectiveDate) + ")");
            }
            if (criteria.CancelDate) {
                query.push("(CancelDate eq null or CancelDate ge " + UtilService.ODataDate(criteria.CancelDate) + ")");
            }
            if (criteria.RenewalCancelDate) {
                query.push(`((RenewalCancelDate eq null and CancelDate eq null) or (RenewalCancelDate ne null and RenewalCancelDate ge ${UtilService.ODataDate(criteria.RenewalCancelDate) }) or CancelDate ge ${UtilService.ODataDate(criteria.RenewalCancelDate) })`);
            }
            if (criteria.AgentFirstName) {
                query.push("Agent/any(agent:contains(agent/FirstName," + UtilService.ODataString(criteria.AgentFirstName) + "))");
            }
            if (criteria.AgentLastName) {
                query.push("Agent/any(agent:contains(agent/Surname," + UtilService.ODataString(criteria.AgentLastName) + "))");
            }
            if (criteria.AgentNumber) {
                query.push("Agent/any(agent:contains(agent/BrokerNo," + UtilService.ODataString(criteria.AgentNumber) + "))");
            }

            if (query.length == 0) {
                (onComplete || angular.noop)();
                return null;
            } else {
                this.service.UtilService.LogSearchQuery('Agency', criteria);
                var url = '/api/' + companyKey + '/Agency?$filter=' + query.join(' and ') + '&$orderby=AgencyName,AgencyNumber';
                return UtilService.SetupPagedTableDataSource({ url: url, pageSize: 10, onComplete: onComplete, onDataBind: (a) => { UtilService.SetupAgencyForUI(a, generateUrls, companyKey); } });
            }
        }
        Agents(companyKey: string, criteria: AgencySearchCriteria, generateUrls: boolean, onComplete: () => void) {
            var UtilService = this.service.UtilService;
            var query = <string[]>[];

            if (criteria.AgentFirstName) {
                query.push("contains(FirstName," + UtilService.ODataString(criteria.AgentFirstName) + ")");
            }
            if (criteria.AgentLastName) {
                query.push("contains(Surname," + UtilService.ODataString(criteria.AgentLastName) + ")");
            }
            if (criteria.AgentNumber) {
                query.push("contains(BrokerNo," + UtilService.ODataString(criteria.AgentNumber) + ")");
            }

            if (query.length == 0) {
                (onComplete || angular.noop)();
                return null;
            } else {
                this.service.UtilService.LogSearchQuery('Agent', query);
                var url = '/api/' + companyKey + '/Agent?$filter=' + query.join(' and ') + '&$orderby=Surname,FirstName';
                return UtilService.SetupPagedTableDataSource({ url: url, pageSize: 15, onComplete: onComplete, onDataBind: (a) => { UtilService.SetupAgentForUI(a, generateUrls, companyKey); } });
            }
        }

        GetClientHttpInfo(companyKey: string, criteria: ClientSearchCriteria) {
            return {
                url: '/api/' + companyKey + '/Client',
                params: {//passing blank strings as null, so the parameter doesn't get sent to the server
                    businessName: criteria.BusinessName || null,
                    fein: criteria.FEIN || null,
                    phoneNumber: criteria.PhoneNumber || null,
                    clientId: criteria.ClientId || null,
                    address: criteria.Address || null,
                    city: criteria.City || null,
                    state: criteria.State || null,
                    postalCode: criteria.PostalCode || null,
                    portfolioId: (!isNaN(Number(criteria.PortfolioId))) ? criteria.PortfolioId : null,
                    portfolioName: criteria.PortfolioName || null
                }
            };
        }
        Clients(companyKey: string, criteria: ClientSearchCriteria, onComplete: () => void) {
            var $filter = this.service.$filter;
            var $http = this.service.$http;
            var UtilService = this.service.UtilService;
            var httpDetails = this.GetClientHttpInfo(companyKey, criteria);

            this.service.UtilService.LogSearchQuery('Client', criteria);
            return UtilService.SetupPagedTableDataSource(
                {
                    url: httpDetails.url,
                    fullLoad: true,
                    params: httpDetails.params,
                    pageSize: 10,
                    onComplete: onComplete,
                    processResponse: (source: any, results: ClientSearchResults) => {
                        source.CriteriaNotSpecific = results.CriteriaNotSpecific;
                        if (results.Failures.length == 0) {
                            source.SearchFailure = null;
                        } else {
                            if (results.Failures.filter((f) => f.Status == 'TooManyResults').length > 0) {
                                source.SearchFailure = 'TooManyResults';
                            } else if (results.Failures.filter((f) => f.Status == 'TimedOut').length > 0) {
                                source.SearchFailure = 'TimedOut';
                            } else {
                                source.SearchFailure = 'Failed';
                            }
                        }

                        angular.forEach(results.Matches,(m) => {
                            m.MatchingNames = [];
                            for (var i = 0; i != m.AdditionalNames.length;) {
                                var n = m.AdditionalNames[i];
                                if (criteria.BusinessName && n.BusinessName.toLowerCase().indexOf(criteria.BusinessName.toLowerCase()) >= 0) {
                                    m.AdditionalNames.splice(i, 1);
                                    m.MatchingNames.push(n);
                                } else {
                                    i++;
                                }
                            }

                            if (m.Addresses.length > 0) {
                                var primaryAddress = <ClientAddress>null;
                                m.MatchingAddresses = [];
                                for (var i = 0; i < m.Addresses.length; i++) {
                                    var a = m.Addresses[i];
                                    if (a.IsPrimary) {
                                        primaryAddress = a;
                                    }

                                    var isMatch = ((criteria.Address || criteria.City || criteria.State || criteria.PostalCode || '').length > 0);
                                    var addr1 = a.Address1;

                                    if (criteria.Address && (a.Address1 == null || a.Address1.toLowerCase().indexOf(criteria.Address.toLowerCase()) < 0)) {
                                        isMatch = false;
                                    } else if (criteria.City && (a.City == null || a.City.toLowerCase().indexOf(criteria.City.toLowerCase()) < 0)) {
                                        isMatch = false;
                                    } else if (criteria.State && (a.State == null || a.State.toLowerCase().indexOf(criteria.State.toLowerCase()) < 0)) {
                                        isMatch = false;
                                    } else if (criteria.PostalCode && (a.PostalCode == null || a.PostalCode.toLowerCase().indexOf(criteria.PostalCode.toLowerCase()) < 0)) {
                                        isMatch = false;
                                    }

                                    if (isMatch) {
                                        m.MatchingAddress = a;
                                        if (a.IsPrimary)
                                            break;
                                    }
                                }

                                if (m.MatchingAddress == null)
                                    m.MatchingAddress = primaryAddress || m.Addresses[0];

                                m.Addresses.splice(m.Addresses.indexOf(m.MatchingAddress), 1);

                                var phoneNumbers = <string[]>[];
                                angular.forEach(m.Contacts,(c) => {
                                    if (c.ContactType == 'Phone')
                                        phoneNumbers.push($filter('phoneNumber')(c.Value));
                                });
                                m.PhoneNumber = phoneNumbers.join(', ');
                            }
                        });

                        return results.Matches;
                    },
                    onDataBind: (m) => {
                        m.ClientUrl = UtilService.CreateClientLink(m.ClientCoreId, null);
                        m.ClientNamesUrl = UtilService.CreateClientLink(m.ClientCoreId, 'names');
                        m.NavigateToClientSubmission = () => { UtilService.NavigateToNewSubmission(m.ClientCoreId); };

                        m.Expanded = false;
                        m.Expand = () => {
                            m.Expanded = !m.Expanded;

                            if (m.Expanded && m.Submissions == null) {
                                this.service.ClientService.GetSubmissions(companyKey, m.ClientCoreId, true, [5, 10, 20]).then((source) => {
                                    m.Submissions = source;
                                });
                            }
                        };
                    }
                });
        }

        Submissions(companyKey: string, advancedSearch: boolean, crossCompany: boolean, criteria: SubmissionSearchCriteria, onComplete: () => void) {
            var UtilService = this.service.UtilService;
            var query = <string[]>[];

            if (criteria.EntityNumber) {
                var encodedEntityNumber = UtilService.ODataString(criteria.EntityNumber);
                var numberQuery = `startswith(PolicyNumber,${encodedEntityNumber})`;
                if (!isNaN(Number(criteria.EntityNumber))) {
                    numberQuery = `(${numberQuery} or startswith(cast(SubmissionNumber,Edm.String),${encodedEntityNumber}))`;
                }

                query.push(numberQuery);
            }
            if (criteria.SubmittedBy) {
                query.push("SubmissionBy eq " + UtilService.ODataString(criteria.SubmittedBy));
            }
            if (criteria.SubmittedStart) {
                query.push("SubmissionDt ge " + UtilService.ODataDate(criteria.SubmittedStart));
            }
            if (criteria.SubmittedEnd) {
                query.push("SubmissionDt lt " + UtilService.ODataDate(moment(criteria.SubmittedEnd).add(1, 'day').toDate()));
            }
            if (criteria.EffectiveStart) {
                query.push("EffectiveDate ge " + UtilService.ODataDate(criteria.EffectiveStart));
            }
            if (criteria.EffectiveEnd) {
                query.push("EffectiveDate lt " + UtilService.ODataDate(moment(criteria.EffectiveEnd).add(1, 'day').toDate()));
            }
            if (criteria.SubmissionType) {
                query.push("SubmissionType/TypeName eq " + UtilService.ODataString(criteria.SubmissionType));
            }
            if (criteria.SubmissionStatus) {
                query.push("SubmissionStatus/StatusCode eq " + UtilService.ODataString(criteria.SubmissionStatus));
            }
            if (criteria.UnderwritingUnit) {
                query.push("LineOfBusiness/any(lob: lob/Description eq " + UtilService.ODataString(criteria.UnderwritingUnit) + ")");
            }
            if (criteria.Underwriter) {
                query.push("UnderwriterPerson/FullName eq " + UtilService.ODataString(criteria.Underwriter));
            }
            if (criteria.PolicySymbol) {
                query.push("PolicySymbol/Code eq " + UtilService.ODataString(criteria.PolicySymbol));
            }
            if (criteria.Agency) {
                query.push("Agency/AgencyNumber eq " + UtilService.ODataString(criteria.Agency.Code));
            }

            if (advancedSearch) {
                if (criteria.ClientId) {
                    query.push("ClientCoreClientId eq " + criteria.ClientId);
                }
                if (criteria.ExpirationStart) {
                    query.push("ExpirationDate ge " + UtilService.ODataDate(criteria.ExpirationStart));
                }
                if (criteria.ExpirationEnd) {
                    query.push("ExpirationDate lt " + UtilService.ODataDate(moment(criteria.ExpirationEnd).add(1, 'day').toDate()));
                }
                if (criteria.UpdatedStart) {
                    query.push("UpdatedDt ge " + UtilService.ODataDate(criteria.UpdatedStart));
                }
                if (criteria.UpdatedEnd) {
                    query.push("UpdatedDt lt " + UtilService.ODataDate(moment(criteria.UpdatedEnd).add(1, 'day').toDate()));
                }
                if (criteria.UnderwritingAnalyst) {
                    query.push("AnalystPerson/FullName eq " + UtilService.ODataString(criteria.UnderwritingAnalyst));
                }
                if (criteria.UnderwritingTechnician) {
                    query.push("TechnicianPerson/FullName eq " + UtilService.ODataString(criteria.UnderwritingTechnician));
                }

                if (criteria.CompanyCustomizationBindings != null && criteria.CompanyCustomizationBindings.length > 0) {
                    angular.forEach(criteria.CompanyCustomizationBindings,(binding) => {
                        if (binding.Value.CodeValue != null) {
                            query.push("CompanyNumberCode/any(cnc: cnc/Code eq " + UtilService.ODataString(binding.Value.CodeValue.Code) + " and cnc/CompanyNumberCodeType/CodeName eq " + UtilService.ODataString(binding.Customization.Description) + ")");
                        } else if (binding.Value.TextValue != null && binding.Value.TextValue != '') {
                            var queryValue = this.service.CompanyCustomizationService.FormatTextValue(binding.Value.TextValue, binding.Customization.DataType);
                            query.push("SubmissionCompanyNumberAttributeValue/any(cna: cna/AttributeValue eq " + UtilService.ODataString(queryValue) + " and cna/CompanyNumberAttribute/Label eq " + UtilService.ODataString(binding.Customization.Description) + ")");
                        }
                    });
                }
            }

            if (query.length == 0) {
                (onComplete || angular.noop)();
                return null;
            } else {
                this.service.UtilService.LogSearchQuery('Submission', query);
                var url = '/api/' + companyKey + '/Submission?';
                if (crossCompany) {
                    url += 'crossCompany=true&';
                }
                url += '$filter=' + query.join(' and ') + '&$orderby=EffectiveDate desc, PolicyNumber';
                return UtilService.SetupPagedTableDataSource({ url: url, onComplete: onComplete });
            }
        }
    }
    export class SearchServiceAutoCompletes {
        constructor(private service: SearchService) { }

        Agencies(companyKey: string, criteria: AgencyAutoCompleteSearchCriteria, maxResults: number) {
            var $http = this.service.$http;
            var UtilService = this.service.UtilService;
            var query = <string[]>[];

            if (criteria.SearchTerm) {
                query.push('(contains(AgencyName,' + UtilService.ODataString(criteria.SearchTerm) + ') or contains(AgencyNumber,' + UtilService.ODataString(criteria.SearchTerm) + '))');
            }
            if (criteria.EffectiveDate) {
                query.push("(EffectiveDate le " + UtilService.ODataDate(criteria.EffectiveDate) + ")");
            }
            if (criteria.CancelDate) {
                query.push("(CancelDate eq null or CancelDate ge " + UtilService.ODataDate(criteria.CancelDate) + ")");
            }
            if (criteria.RenewalCancelDate) {
                query.push(`((RenewalCancelDate eq null and CancelDate eq null) or (RenewalCancelDate ne null and RenewalCancelDate ge ${UtilService.ODataDate(criteria.RenewalCancelDate) }) or CancelDate ge ${UtilService.ODataDate(criteria.RenewalCancelDate) })`);
            }

            if (query.length > 0) {
                var url = '/api/' + companyKey + '/AgencySummary?$filter=' + query.join(' and ') + '&$orderby=AgencyName,AgencyNumber';
                if (maxResults != null) {
                    url += '&$top=' + maxResults;
                }

                return $http.get(url).then((response: ng.IHttpPromiseCallbackArg<AgencySummary[]>) => {
                    angular.forEach(response.data,(a) => UtilService.SetupAgencyDates(a));
                    return response.data;
                });
            } else {
                return null;
            }
        }
        Submissions(companyKey: string, term: string, maxResults: number, excludingSubmissionNumber: string, crossCompany: boolean) {
            var $http = this.service.$http;
            var encodedTerm = this.service.UtilService.ODataString(term);
            if (term) {
                var filter = `startswith(PolicyNumber,${encodedTerm})`;
                if (!isNaN(Number(term))) {
                    filter = `(${filter} or startswith(cast(SubmissionNumber,Edm.String),${encodedTerm}))`;
                }
                if (excludingSubmissionNumber) {
                    filter = filter + " and SubmissionNumber ne " + excludingSubmissionNumber;
                }

                var url = '/api/' + companyKey + '/SubmissionSummary?';
                if (crossCompany) {
                    url += 'crossCompany=true&';
                }
                url += '$filter=' + filter + '&$orderby=EffectiveDate desc, PolicyNumber&$top=' + maxResults;

                return $http.get(url).then((response) => {
                    return response.data;
                });
            } else {
                return null;
            }
        }
        ClassCodes(companyKey: string, term: string) {
            var $http = this.service.$http;
            var UtilService = this.service.UtilService;
            var query = <string[]>[];
            query.push('contains(CodeValue,' + UtilService.ODataString(term) + ')');
            query.push('contains(GLCodeValue,' + UtilService.ODataString(term) + ')');
            query.push('contains(Description,' + UtilService.ODataString(term) + ')');
            return $http.get('/api/' + companyKey + '/ClassCode?$filter=' + query.join(' or ') + '&$orderby=CodeValue&$top=10').then((response) => {
                return response.data;
            });
        }
        SicCodes(term: string) {
            var $http = this.service.$http;
            var UtilService = this.service.UtilService;
            var query = <string[]>[];
            query.push('contains(Code,' + UtilService.ODataString(term) + ')');
            query.push('contains(CodeDescription,' + UtilService.ODataString(term) + ')');
            query.push('contains(SicGroup/GroupDescription,' + UtilService.ODataString(term) + ')');
            query.push('contains(SicGroup/SicDivision/DivisionDescription,' + UtilService.ODataString(term) + ')');
            return $http.get('/api/Domains/SicCode?$filter=' + query.join(' or ') + '&$orderby=Code&$top=10').then((response) => {
                return response.data;
            });
        }
        NaicsCodes(term: string) {
            var $http = this.service.$http;
            var UtilService = this.service.UtilService;
            var query = <string[]>[];
            query.push('contains(Code,' + UtilService.ODataString(term) + ')');
            query.push('contains(CodeDescription,' + UtilService.ODataString(term) + ')');
            query.push('contains(NaicsGroup/GroupDescription,' + UtilService.ODataString(term) + ')');
            query.push('contains(NaicsGroup/NaicsDivision/DivisionDescription,' + UtilService.ODataString(term) + ')');
            return $http.get('/api/Domains/NaicsCode?$filter=' + query.join(' or ') + '&$orderby=Code&$top=10').then((response) => {
                return response.data;
            });
        }
        Users(companyKey: string, userName: string, isSystemAdministrator: boolean, term: string) {
            var $http = this.service.$http;
            var UtilService = this.service.UtilService;
            var query = <string[]>[];
            query.push('contains(Username,' + UtilService.ODataString(term) + ')');
            //Don't let them view themselves if they aren't a system admin
            if (!isSystemAdministrator) {
                query.push('Username ne ' + UtilService.ODataString(userName));
            }

            return $http.get('/api/' + companyKey + '/UserSummary?$filter=' + query.join(' and ') + '&$orderby=Username&$top=10').then((response) => {
                return response.data;
            });
        }
    }

    export interface SubmissionSearchCriteria {
        ClientId?: number;
        EntityNumber?: string;
        SubmittedBy?: string;
        SubmittedStart?: any;
        SubmittedEnd?: any;
        EffectiveStart?: any;
        EffectiveEnd?: any;
        ExpirationStart?: any;
        ExpirationEnd?: any;
        UpdatedStart?: any;
        UpdatedEnd?: any;
        SubmissionType?: string;
        SubmissionStatus?: string;
        UnderwritingUnit?: string;
        Underwriter?: string;
        UnderwritingAnalyst?: string;
        UnderwritingTechnician?: string;
        PolicySymbol?: string;
        Agency?: Agency;
        CompanyCustomizationBindings?: CompanyCustomizationValueBinding[];
    }
    export interface ClientSearchCriteria {
        BusinessName?: string;
        FEIN?: string;
        PhoneNumber?: string;
        ClientId?: number;
        Address?: string;
        City?: string;
        State?: string;
        PostalCode?: string;
        PortfolioId?: number;
        PortfolioName?: string;
    }
    export interface AgencySearchCriteria {
        AgencyName?: string;
        AgencyCode?: string;
        AgencyCity?: string;
        AgencyState?: string;
        CancelDate?: any;
        RenewalCancelDate?: any;
        EffectiveDate?: any;
        AgentFirstName?: string;
        AgentLastName?: string;
        AgentNumber?: string;
    }
    export interface AgencyAutoCompleteSearchCriteria {
        SearchTerm: string;
        CancelDate?: any;
        RenewalCancelDate?: any;
        EffectiveDate?: any;
    }

    app.service('SearchService', SearchService);
}