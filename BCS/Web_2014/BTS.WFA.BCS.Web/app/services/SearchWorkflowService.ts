﻿/// <reference path="../app.ts" />
module bcs {
    'use strict';
    export class SearchWorkflowService {
        constructor(public $state: ng.ui.IStateService, public $q: ng.IQService, public SearchService: SearchService, public DomainService: DomainService, public CacheService: CacheService, public UtilService: UtilService, public CompanyCustomizationService: CompanyCustomizationService, public UserService: UserService) { }

        GetWorkflow(companyKey: string, crossCompany: boolean, isModal: boolean) {
            return new SearchWorkflow(this, companyKey, crossCompany, isModal);
        }
    }

    export enum SearchMenuState {
        Closed,
        Open,
        Expanded
    }
    export enum SearchType {
        Client,
        Submission,
        Agency
    }
    export enum SearchMenuStateChangeReason {
        Search,
        Results
    }
    export class SearchWorkflow {
        Searching: boolean;
        DisplayPreSearchPane: boolean;
        SearchType: SearchType;
        LastSearchType: SearchType;
        SearchTypesSearched: any;
        MenuState: SearchMenuState;
        MenuFixed: boolean;
        SearchStateChangeCause: SearchMenuStateChangeReason;
        SelectedModalItem: any;

        ClientDataSource: PagedDataSource;
        ClientSearchCriteria: ClientSearchCriteria;
        ClientSearchSubmissionFields: SubmissionDisplayField[];
        ClientPortfolioEnabled: boolean;
        ClientSegmentEnabled: boolean;

        SubmissionDataSource: PagedDataSource;
        SubmissionSearchCriteria: SubmissionSearchCriteria;
        SubmissionSearchSubmissionFields: SubmissionDisplayField[];
        SubmissionCompanyCustomizationsPromise: ng.IPromise<CompanyCustomization[]>;

        AgencyDataSource: PagedDataSource;
        AgentDataSource: PagedDataSource;
        AgencySearchCriteria: AgencySearchCriteria;
        AgencySelected: (agency: Agency) => void;
        AgentsEnabled: boolean;

        SubmissionTypes: SubmissionType[];
        States: State[];
        SubmissionStatuses: SubmissionStatus[];
        UnderwritingUnits: UnderwritingUnit[];
        Underwriters: UnderwritingPersonnel[];
        UnderwritersLabel: string;
        UnderwritingAnalysts: UnderwritingPersonnel[];
        UsesUnderwritingAnalyst: boolean;
        UnderwritingAnalystLabel: string;
        UnderwritingTechnicians: UnderwritingPersonnel[];
        UsesUnderwritingTechnician: boolean;
        UnderwritingTechnicianLabel: string;
        PolicySymbols: PolicySymbol[];
        UsesPolicySymbol: boolean;
        PolicySymbolLabel: string;

        constructor(private service: SearchWorkflowService, private companyKey: string, private CrossCompany: boolean, private IsModal: boolean) {
            service.DomainService.CompanyInformation(companyKey).then((companySettings) => {
                service.UserService.UserPromise.then((user) => {
                    this.ClientSegmentEnabled = (user.IsInRole('Action - Set Client Segment') && companySettings.ClientFields.ClientCoreSegment.Enabled);
                });

                this.ClientPortfolioEnabled = companySettings.ClientFields.PortfolioInformation.Enabled;
                this.AgentsEnabled = companySettings.AgentsEnabled;
                this.SubmissionTypes = companySettings.SubmissionTypes;
                for (var i = 0; i < this.SubmissionTypes.length; i++) {
                    var submissionType = this.SubmissionTypes[i];
                    if (submissionType.PolicySymbol.Enabled) {
                        this.UsesPolicySymbol = true;
                        this.PolicySymbolLabel = submissionType.PolicySymbol.Label;
                    }
                    if (submissionType.Underwriter.Enabled) {
                        this.UnderwritersLabel = submissionType.Underwriter.Label;
                    }
                    if (submissionType.UnderwritingAnalyst.Enabled) {
                        this.UsesUnderwritingAnalyst = true;
                        this.UnderwritingAnalystLabel = submissionType.UnderwritingAnalyst.Label;
                    }
                    if (submissionType.UnderwritingTechnician.Enabled) {
                        this.UsesUnderwritingTechnician = true;
                        this.UnderwritingTechnicianLabel = submissionType.UnderwritingTechnician.Label;
                    }
                }
            });
            service.DomainService.States().then((items) => { this.States = items; });
            service.DomainService.SubmissionStatuses(companyKey).then((items) => { this.SubmissionStatuses = items; });
            service.DomainService.UnderwritingUnits(companyKey).then((items) => { this.UnderwritingUnits = items; });
            service.DomainService.Underwriters(companyKey).then((items) => { this.Underwriters = items; });
            service.DomainService.UnderwritingAnalysts(companyKey).then((items) => { this.UnderwritingAnalysts = items; });
            service.DomainService.UnderwritingTechnicians(companyKey).then((items) => { this.UnderwritingTechnicians = items; });
            service.DomainService.PrimaryPolicySymbols(companyKey).then((items) => { this.PolicySymbols = items; });

            this.SubmissionCompanyCustomizationsPromise = new service.$q((resolve, reject) => {
                service.DomainService.CompanyCustomizations(this.companyKey, 'Submission').then((items) => {
                    var today = this.service.UtilService.GetCurrentDate();
                    resolve(items.filter((c) => (c.ExpirationDate == null || c.ExpirationDate > today) && !c.ReadOnly));
                });
            });

            this.MenuFixed = true;
            this.DisplayPreSearchPane = true;
            this.SearchTypesSearched = {};
            this.MenuState = SearchMenuState.Open;
            this.ClientSearchCriteria = service.CacheService.Session.get('ClientSearchCriteria') || service.CacheService.Session.put('ClientSearchCriteria', <ClientSearchCriteria>{});
            this.ResetSubmissionSearchCriteria();
            this.ResetAgencySearchCriteria();
        }

        private startSearch(stateName: string) {
            this.Searching = true;
            this.DisplayPreSearchPane = false;

            this.LastSearchType = this.SearchType;
            this.SearchTypesSearched[this.SearchType] = true;

            this.MenuState = SearchMenuState.Open;
            if (this.IsModal) {
                this.SelectedModalItem = null;
                this.MenuFixed = true;
            } else {
                this.SearchStateChangeCause = SearchMenuStateChangeReason.Search;
                this.service.$state.go(stateName);
            }
        }
        private focusCurrentSearchForm() {
            switch (this.SearchType) {
                case 0: this.service.UtilService.RaiseFocusTrigger('businessName'); break;
                case 1: this.service.UtilService.RaiseFocusTrigger('submissionNumber'); break;
                case 2: this.service.UtilService.RaiseFocusTrigger('agencyName'); break;
            }
        }

        CloseMenu() {
            this.MenuState = SearchMenuState.Closed;
        }
        OpenMenu() {
            this.MenuState = SearchMenuState.Open;
            //Expand it out if there's hidden criteria
            if ((this.SearchType == SearchType.Submission && (this.SubmissionSearchCriteria.PolicySymbol || this.SubmissionSearchCriteria.Agency))
                || (this.SearchType == SearchType.Agency && (this.AgencySearchCriteria.AgencyCity || this.AgencySearchCriteria.AgencyState))) {
                this.MenuState = SearchMenuState.Expanded;
            }

            this.focusCurrentSearchForm();
        }
        ToggleAdvancedMenu() {
            this.MenuState = (this.MenuState == SearchMenuState.Open) ? SearchMenuState.Expanded : SearchMenuState.Open;
        }
        ChangeSearchType(newSearchType: SearchType) {
            if (this.Searching) {
                this.CancelSearch();
            }

            this.SearchType = newSearchType;
            switch (this.SearchType) {
                case 1: this.service.$state.go('cn.search.clients'); break;
                case 2: this.service.$state.go('cn.search.submissions'); break;
                case 3: this.service.$state.go('cn.search.agencies'); break;
            }
        }
        CancelSearch() {
            angular.forEach([this.ClientDataSource, this.SubmissionDataSource, this.AgencyDataSource, this.AgentDataSource],(s) => {
                if (s) {
                    s.abortSearch();
                }
            });
            this.SearchTypesSearched[this.SearchType] = false;
            this.DisplayPreSearchPane = true;
            this.Searching = false;

            this.focusCurrentSearchForm();
        }

        HasSearched() { return (this.LastSearchType != null); }
        NavigateToSearchResults() {
            if (this.IsModal) {
                this.SelectedModalItem = null;
                this.MenuFixed = true;
                this.OpenMenu();
            } else {
                this.SearchStateChangeCause = SearchMenuStateChangeReason.Results;
                switch (this.LastSearchType) {
                    case 0: this.service.$state.go('cn.search.clients'); break;
                    case 1: this.service.$state.go('cn.search.submissions'); break;
                    case 2: this.service.$state.go('cn.search.agencies'); break;
                }
            }
        }

        ResetClientSearchCriteria() {
            this.ClientSearchCriteria = this.service.CacheService.Session.put('ClientSearchCriteria', <ClientSearchCriteria>{});
            this.CancelSearch();
            this.ClientDataSource = null;
        }
        ClientSearch() {
            if (this.ClientSearchSubmissionFields == null) {
                this.service.DomainService.SubmissionSummaryFields(this.companyKey, 'client-detail').then((items) => { this.ClientSearchSubmissionFields = items; });
            }

            this.startSearch('cn.search.clients');
            this.ClientDataSource = this.service.SearchService.Search.Clients(this.companyKey, this.ClientSearchCriteria,() => { this.Searching = false; });
        }

        ResetSubmissionSearchCriteria() {
            this.SubmissionSearchCriteria = {};
            this.SubmissionCompanyCustomizationsPromise.then((customizations) => {
                this.SubmissionSearchCriteria.CompanyCustomizationBindings = this.service.CompanyCustomizationService.CreateBindings(customizations, [], false);
            });

            this.CancelSearch();
            this.SubmissionDataSource = null;
        }
        SubmissionSearch() {
            if (this.SubmissionSearchSubmissionFields == null) {
                this.service.DomainService.SubmissionSummaryFields(this.companyKey, 'submission-search-results').then((items) => { this.SubmissionSearchSubmissionFields = items; });
            }

            this.startSearch('cn.search.submissions');
            this.SubmissionDataSource = this.service.SearchService.Search.Submissions(this.companyKey, false, this.CrossCompany, this.SubmissionSearchCriteria,() => { this.Searching = false; });
        }

        ResetAgencySearchCriteria() {
            this.AgencySearchCriteria = {};
            this.CancelSearch();
            this.AgencyDataSource = null;
            this.AgentDataSource = null;
        }
        AgencySearch() {
            this.startSearch('cn.search.agencies');

            this.AgencyDataSource = this.AgentDataSource = null;
            var criteria = this.AgencySearchCriteria;
            if (criteria.AgencyName || criteria.AgencyCode || criteria.AgencyCity || criteria.AgencyState) {
                this.AgencyDataSource = this.service.SearchService.Search.Agencies(this.companyKey, criteria, !this.IsModal,() => { this.Searching = false; });
            } else {
                this.AgentDataSource = this.service.SearchService.Search.Agents(this.companyKey, criteria, !this.IsModal,() => { this.Searching = false; });
            }
        }

        ModalViewItem(item: any) {
            if (this.IsModal) {
                this.SelectedModalItem = item;
                this.MenuFixed = false;
                this.MenuState = SearchMenuState.Closed;
            }
        }
        ModalSelectItem: () => void;

        AgencyAutoCompleteSearch = (term: string) => this.service.SearchService.AutoComplete.Agencies(this.companyKey, { SearchTerm: term }, 10)

    }

    app.service('SearchWorkflowService', SearchWorkflowService);
}