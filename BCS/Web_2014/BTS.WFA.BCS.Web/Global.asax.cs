﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Swashbuckle.Application;
using BCS = BTS.WFA.BCS;
using System.Web.Http.Description;

namespace BTS.WFA.BCS.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            //MVC
            RouteTable.Routes.MapRoute("Index", "", new { controller = "Home", action = "Index" });
            RouteTable.Routes.MapRoute("UnsupportedBrowser", "UnsupportedBrowser", new { controller = "Home", action = "UnsupportedBrowser" });
            RouteTable.Routes.MapRoute("UnloadApplication", "A6E601BF-1387-42DB-93EF-46D51CEBBC9C", new { controller = "Home", action = "UnloadApplication" });
            RouteTable.Routes.MapRoute("Login", "login", new { controller = "Home", action = "Login" });
            //Web Api
            GlobalConfiguration.Configure(config =>
            {
                config.MapHttpAttributeRoutes();

                config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
                config.Filters.Add(new Api.Filters.WebExceptionFilterAttribute());
                config.Filters.Add(new Api.Filters.CompanyAccessAttribute());
                config.MessageHandlers.Add(new Api.Handlers.AuthenticationHandler());
                config.MessageHandlers.Add(new Api.Handlers.VersionHandler());

                config.EnableSwagger("swagger/api/{apiVersion}", c =>
                {
                    c.MapType<CompanyDetail>(() => new Swashbuckle.Swagger.Schema { type = "string", description = "Company Abbreviation" });
                    c.MapType<ClientSystemId>(() => new Swashbuckle.Swagger.Schema { type = "string", description = "Client Core Id or CMS Id" });
                    c.RootUrl((request) => ConfigurationManager.AppSettings["ApplicationRootUrl"]);
                    c.SingleApiVersion("v1", "Clearance Web Api");
                    c.ApiKey("apiKey").Description("API JWT Authentication").Name("Authorization").In("header");
                    c.DescribeAllEnumsAsStrings();
                    //c.IncludeXmlComments(GetXmlCommentsPath());
                }).EnableSwaggerUi("swagger/{*assetPath}", c =>
                {
                    c.DisableValidator();//this won't work against internal pages anyway
                    c.EnableDiscoveryUrlSelector();
                });
            });

            var appHandlers = BCS.Application.CreateHandlers(ConfigurationManager.AppSettings["ApplicationHandlers"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            BCS.Application.Initialize<BCS.Data.SqlDb>(appHandlers.ToArray());
            //Api.Handlers.AuthenticationHandler.OverrideUser = Services.UserService.GetActiveUser(@"wrbts\UserName");//specific user
            //Api.Handlers.AuthenticationHandler.OverrideUser = Api.TestUsers.CompanyAdmin("CWG");
            //Api.Handlers.AuthenticationHandler.OverrideUser = Api.TestUsers.StandardUser("CWG");
            //Api.Handlers.AuthenticationHandler.OverrideUser = Api.TestUsers.Auditor("CWG");
            //Api.Handlers.AuthenticationHandler.OverrideUser = Api.TestUsers.CreateUser("CustomUser", new[] { "CWG", "AIC" }, "User", new[] { Domains.Roles.Tools_PolicyNumberGenerator });

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/framework-scripts").Include("~/scripts/analytics.js")
                                                                                   .Include("~/scripts/moment-2.8.3.js")
                                                                                   .Include("~/scripts/FileSaver-2014-08-29.js")
                                                                                   .Include("~/scripts/zeroclipboard-2.2.0/ZeroClipboard.js")
                                                                                   .Include("~/scripts/toastr-2.1.1/toastr.js")
                                                                                   .IncludeDirectory("~/scripts/angular-ui", "*.js", true));

            //We aren't minifying this one to help with debugging
            BundleTable.Bundles.Add(new Bundle("~/bundles/app-scripts").Include("~/app/models.js")
                                                                       .Include("~/app/app.js")
                                                                       .Include("~/app/states.js")
                                                                       .IncludeDirectory("~/app", "*.js", true));

            BundleTable.Bundles.Add(new StyleBundle("~/bundles/framework-styles").Include("~/styles/bts-bootstrap/bts-bootstrap.v1.5.0.css")
                                                                                 .Include("~/scripts/toastr-2.1.1/toastr.css"));

            BundleTable.Bundles.Add(new StyleBundle("~/bundles/app-styles").IncludeDirectory("~/styles", "*.css", false)
                                                                           .IncludeDirectory("~/app", "*.css", true));
        }
    }
}