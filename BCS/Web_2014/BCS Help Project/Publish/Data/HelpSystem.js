var xmlHelpSystemData = "";
xmlHelpSystemData += '<?xml version=\"1.0\" encoding=\"utf-8\"?>';
xmlHelpSystemData += '<WebHelpSystem DefaultUrl=\"Content/concept topics/berkly_clearance_system.htm\" Toc=\"Data/Toc.js\" Index=\"Data/Index.js\" Concepts=\"Data/Concepts.xml\" BrowseSequence=\"Data/BrowseSequence.js\" Glossary=\"Data/Glossary.js\" SearchDatabase=\"Data/Search.xml\" Alias=\"Data/Alias.xml\" Synonyms=\"Data/Synonyms.xml\" SkinName=\"HTML5\" Skins=\"HTML5\" BuildTime=\"5/19/2015 9:28:43 AM\" BuildVersion=\"10.2.2.0\" TargetType=\"WebHelp2\" SkinTemplateFolder=\"Skin/\" InPreviewMode=\"false\" MoveOutputContentToRoot=\"false\" MakeFileLowerCase=\"false\" UseCustomTopicFileExtension=\"false\">';
xmlHelpSystemData += '    <CatapultSkin Version=\"2\" SkinType=\"WebHelp2\" Comment=\"HTML5 skin\" Anchors=\"Left,Right,Top,Bottom,Width,Height\" Width=\"545px\" Height=\"698px\" Top=\"241px\" Left=\"719px\" Bottom=\"85px\" Right=\"16px\" Tabs=\"TOC,Index\" DefaultTab=\"TOC\" UseBrowserDefaultSize=\"false\" UseDefaultBrowserSetup=\"false\" Title=\"Berkley Clearance System Help\" BrowserSetup=\"Toolbar,Menu\" DisplaySearchBar=\"false\" Name=\"HTML5\">';
xmlHelpSystemData += '        <WebHelpOptions NavigationPaneWidth=\"160\" HideNavigationOnStartup=\"true\" />';
xmlHelpSystemData += '        <Toolbar EnableCustomLayout=\"true\" Buttons=\"Print|Separator|TopicRatings|Separator|EditUserProfile|Filler|PreviousTopic|CurrentTopicIndex|NextTopic\" />';
xmlHelpSystemData += '    </CatapultSkin>';
xmlHelpSystemData += '</WebHelpSystem>';
MadCap.Utilities.Xhr._FilePathToXmlStringMap.Add('HelpSystem', xmlHelpSystemData);
