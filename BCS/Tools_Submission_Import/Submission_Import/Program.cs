﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using BTS.WFA.BCS;
using BTS.WFA.BCS.Data;
using BTS.WFA.BCS.ViewModels;
using Services = BTS.WFA.BCS.Services;

namespace Submission_Import
{
    class Program
    {
        static void Main(string[] args)
        {
            var appHandlers = Application.CreateHandlers(ConfigurationManager.AppSettings["ApplicationHandlers"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            Application.Initialize<SqlDb>(appHandlers.ToArray());

            var cn = Application.CompanyMap["BMG"];

            //var batchId = Guid.NewGuid();
            //ParseCSVFile(cn, batchId, @"C:\TestSubmissionImport2015.csv");

            ImportSubmissions(cn, new Guid("0D943DD6-CA9E-4BCA-9E9D-CC93ED0EF3A1"));
        }

        private static void ParseCSVFile(CompanyDetail cd, Guid batchId, string filePath)
        {
            var fileRows = CSV.ParseText(System.IO.File.ReadAllText(filePath));
            if (fileRows.Length > 0)
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    var companyCustomizations = Services.CompanyCustomizationService.GetCustomizations(cd).Where(c => c.Usage == CompanyCustomizationUsage.Submission);
                    var headerRow = fileRows[0];
                    foreach (var row in fileRows.Skip(1).Where(r => r.Any(s => !String.IsNullOrEmpty(s))))
                    {
                        List<XElement> customizations = new List<XElement>();
                        var submission = db.ImportSubmission.Add(new BTS.WFA.BCS.Data.Models.ImportSubmission() { CompanyNumberId = cd.CompanyNumberId, BatchId = batchId });
                        for (int i = 0; i < row.Length; i++)
                        {
                            var value = (row[i] ?? "").Trim();
                            if (!String.IsNullOrEmpty(value))
                            {
                                switch (headerRow[i].ToLower())
                                {
                                    case "clientid":
                                        submission.ClientCoreClientId = long.Parse(value);
                                        break;
                                    case "policynumber":
                                        submission.PolicyNumber = value;
                                        break;
                                    case "effectivedate":
                                        submission.EffectiveDate = DateTime.Parse(value);
                                        break;
                                    case "expirationdate":
                                        submission.ExpirationDate = DateTime.Parse(value);
                                        break;
                                    case "agencynumber":
                                        submission.AgencyNumber = value;
                                        break;
                                    case "underwritingunit":
                                    case "uwunitcode":
                                        submission.UnderwritingUnit = value;
                                        break;
                                    case "underwriter":
                                        submission.Underwriter = value;
                                        break;
                                    case "insuredname":
                                        submission.InsuredName = value;
                                        break;
                                    case "submissionnumber":
                                        submission.SubmissionNumber = long.Parse(value);
                                        break;
                                    case "sequence":
                                    case "submissionsequence":
                                        submission.Sequence = int.Parse(value);
                                        break;
                                    case "submissiontype":
                                        submission.SubmissionType = value;
                                        break;
                                    case "status":
                                        submission.SubmissionStatus = value;
                                        break;
                                    default:
                                        var matchingCustomization = companyCustomizations.FirstOrDefault(c => c.Description.Replace(" ", "").Equals(headerRow[i].Replace(" ", ""), StringComparison.CurrentCultureIgnoreCase));
                                        if (matchingCustomization != null)
                                            customizations.Add(new XElement("Customization", new XAttribute("Name", matchingCustomization.Description), new XAttribute("Value", value)));
                                        break;
                                }
                            }
                        }

                        if (customizations.Count > 0)
                            submission.CompanyCustomizations = (new XElement("Customizations", customizations)).ToString();
                    }

                    db.SaveChanges();
                }
            }
        }

        private static void ImportSubmissions(CompanyDetail cd, Guid batchId)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var companyCustomizations = Services.CompanyCustomizationService.GetCustomizations(cd).Where(c => c.Usage == CompanyCustomizationUsage.Submission);
                var underwritingUnits = Services.UnderwritingService.GetUnits(cd);
                var underwriters = Services.UnderwritingService.GetUnderwriters(cd).OrderBy(u => u.RetirementDate.HasValue);

                foreach (var import in db.ImportSubmission.Where(s => s.CompanyNumberId == cd.CompanyNumberId && s.CreatedSubmissionNumber == null && s.BatchId == batchId).ToArray())
                {
                    var submission = new SubmissionViewModel();
                    if (import.SubmissionNumber != null)
                    {
                        submission.SubmissionNumber = import.SubmissionNumber.Value;
                        submission.SequenceNumber = import.Sequence ?? 1;
                    }

                    submission.ClientCoreId = import.ClientCoreClientId;
                    submission.InsuredName = import.InsuredName;
                    submission.SubmissionType = import.SubmissionType;
                    submission.EffectiveDate = import.EffectiveDate;
                    submission.ExpirationDate = import.ExpirationDate;
                    submission.PolicyNumber = import.PolicyNumber;
                    submission.Status = import.SubmissionStatus;

                    if (!String.IsNullOrEmpty(import.AgencyNumber))
                    {
                        submission.Agency = Services.AgencyService.Get(cd, import.AgencyNumber, null);
                        if (submission.Agency == null)
                            submission.AddValidation("Agency", "Could not find Agency '{0}'", import.AgencyNumber);
                    }

                    if (!String.IsNullOrEmpty(import.Agent))
                    {
                        if (submission.Agency != null)
                        {
                            var agents = Services.AgencyService.GetAgents(cd, submission.Agency.Code);
                            submission.Agent = agents.FirstOrDefault(a => a.FullName.Equals(import.Agent, StringComparison.CurrentCultureIgnoreCase));
                        }

                        if (submission.Agent == null)
                            submission.AgentUnspecifiedReason = import.Agent;
                    }

                    if (!String.IsNullOrEmpty(import.UnderwritingUnit))
                    {
                        submission.UnderwritingUnit = underwritingUnits.FirstOrDefault(u => u.Code.Equals(import.UnderwritingUnit, StringComparison.CurrentCultureIgnoreCase)
                                                                                         || u.Description.Equals(import.UnderwritingUnit, StringComparison.CurrentCultureIgnoreCase));
                        if (submission.UnderwritingUnit == null)
                            submission.AddValidation("UnderwritingUnit", "Could not find Undewriting Unit '{0}'", import.UnderwritingUnit);
                    }

                    if (!String.IsNullOrEmpty(import.Underwriter))
                    {
                        submission.Underwriter = underwriters.FirstOrDefault(u => (u.UserName ?? "").Equals(import.Underwriter, StringComparison.CurrentCultureIgnoreCase)
                                                                               || (u.Initials ?? "").Equals(import.Underwriter, StringComparison.CurrentCultureIgnoreCase)
                                                                               || (u.FullName ?? "").Equals(import.Underwriter, StringComparison.CurrentCultureIgnoreCase));
                        if (submission.Underwriter == null)
                            submission.AddValidation("Underwriter", "Could not find Underwriter '{0}'", import.Underwriter);
                    }

                    if (!String.IsNullOrEmpty(import.CompanyCustomizations))
                    {
                        foreach (var node in XElement.Parse(import.CompanyCustomizations).Descendants("Customization"))
                        {
                            var name = node.Attribute("Name").Value;
                            var value = node.Attribute("Value").Value;

                            var matchingCustomization = companyCustomizations.FirstOrDefault(c => c.Description.Equals(name, StringComparison.CurrentCultureIgnoreCase));
                            if (matchingCustomization == null)
                                submission.AddValidation("CompanyCustomization", "Could not find Customization '{0}'", name);
                            else
                            {
                                var submissionCustomization = new CompanyCustomizationValueViewModel() { Customization = matchingCustomization.Description };
                                submission.CompanyCustomizations.Add(submissionCustomization);

                                if (matchingCustomization.DataType == CompanyCustomizationDataType.List)
                                {
                                    var matchingCode = matchingCustomization.Options.FirstOrDefault(o => o.Code.Equals(value, StringComparison.CurrentCultureIgnoreCase) || (o.Description != null && o.Description.Equals(value, StringComparison.CurrentCultureIgnoreCase)));
                                    if (matchingCode == null)
                                        submission.AddValidation("CompanyCustomization", "Could not find value '{0}' for Company Customization '{1}'", value, name);
                                    else
                                        submissionCustomization.CodeValue = matchingCode;
                                }
                                else
                                    submissionCustomization.TextValue = value;
                            }
                        }
                    }

                    //TODO: bmag 2015 hack
                    submission.AgentUnspecifiedReason = "No Signature";

                    SubmissionIdViewModel result = submission;
                    if (submission.ValidationResults.Count == 0)
                        result = Services.SubmissionService.Save(cd, (submission.SubmissionNumber == null) ? null : SubmissionNumberViewModel.Parse(submission.Id), submission, "Submission Import", new Services.SubmissionSaveOptions() { IgnoreValidations = true }, null);

                    var hasErrors = (result.ValidationResults.Count > 0);
                    import.ErrorMessage = (hasErrors) ? String.Join(", ", result.ValidationResults.Select(vr => vr.Message)) : null;
                    import.CreatedSubmissionNumber = (hasErrors) ? null : result.Id;
                    db.SaveChanges();
                }
            }
        }
    }
}
