using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;

namespace Submission_Import
{
    public class CSV
    {
        public static string[][] ParseText(string fileText)
        {
            List<string[]> rows = new List<string[]>();
            List<string> curRow = new List<string>();
            StringBuilder curWord = new StringBuilder();
            bool inQuotes = false;

            char[] fileChars = fileText.ToCharArray();
            for (int i = 0; i < fileChars.Length; i++)
            {
                switch (fileChars[i])
                {
                    case '\r':
                        if (i + 1 < fileChars.Length && fileChars[i + 1] == '\n')
                        {   //actual new line
                            i++;
                            curRow.Add(curWord.ToString().Trim());

                            if (curRow.Count > 0)
                                rows.Add(curRow.ToArray());

                            curRow = new List<string>();
                            curWord = new StringBuilder();
                        }
                        break;
                    case ',':
                        if (inQuotes)
                            curWord.Append(fileChars[i]);
                        else
                        {
                            curRow.Add(curWord.ToString().Trim());
                            curWord = new StringBuilder();
                        }
                        break;
                    case '"':
                        if (inQuotes)
                        {
                            if (i + 1 < fileChars.Length && fileChars[i + 1] == '"')
                            {   //if we see "", it's a single " if we're in a quoted cell
                                curWord.Append('"');
                                i++;
                            }
                            else
                                inQuotes = false;
                        }
                        else
                            inQuotes = true;
                        break;
                    default:
                        curWord.Append(fileChars[i]);
                        break;
                }
            }

            if (curRow.Count > 0 || curWord.Length > 0)
            {
                curRow.Add(curWord.ToString().Trim());
                rows.Add(curRow.ToArray());
            }

            return rows.ToArray();
        }
    }
}
